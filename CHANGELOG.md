## Release [0.1] - 2020-03-12

### Added

- Base architecture to run
    - An ETL step, for partitioning data into a mount storage
    - Preprocessing, training and forecast Machine Learning steps 

- Json schema frames for defining ML pipeline configurations

- A mean of tracking and historizing each of the ETL and ML steps runs in a database.

- A set of services
    - Weather API
    - S3 connector
    - Athena connector
    - Database (postgres) connector
    
## Release [0.2] - 2020-05-14

### Added

- Evolution of the code backbone
    - more feature engineering and pipeline parameters 
    - dask multi processing
    - metric compute and formatting
    - better factorization

- Monitoring stack deployment
    - MLFlow for machine learning
    - Superset
    - Environment set up scripts

- Evolution of Data Preparations
    - logic evolution
    - data sources integration (promo, media, sellout...)
    - dask multi processing
    - first etl integration