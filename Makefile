install:
	@bash scripts/install.sh

setup:
	@bash scripts/setup.sh

cleanup:
	@bash scripts/cleanup.sh

reset: cleanup install setup

notebook:
	@venv/bin/jupyter-lab --no-browser --port=$(PORT) --ip=0.0.0.0

start:
	@docker-compose -f dockerfiles/docker-compose.local.yml up -d

stop:
	@docker-compose -f dockerfiles/docker-compose.local.yml down

user:
	@bash scripts/user.sh

test:
	@bash scripts/test.sh

migrate:
	@ENV=$(ENV) poetry run alembic upgrade head

seed:
	@ENV=$(ENV) poetry run python seeds/seeder.py

doc:
	poetry run sphinx-apidoc -f -o docs/source/ adf/
	poetry run sphinx-build docs/ docs/_build
