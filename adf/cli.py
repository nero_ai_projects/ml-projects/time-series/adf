import click
from adf.services.weather import cli as weather_cli
from adf.services.calendar import cli as calendar_cli
from adf.modelling import cli as modelling_cli
from adf.flows import cli as flow_cli
from adf.great_expectations import cli as great_expectations_cli


@click.group()
def cli():
    return


cli.add_command(weather_cli.commands)
cli.add_command(calendar_cli.commands)
cli.add_command(modelling_cli.commands)
cli.add_command(flow_cli.commands)
cli.add_command(great_expectations_cli.commands)


if __name__ == '__main__':
    cli()
