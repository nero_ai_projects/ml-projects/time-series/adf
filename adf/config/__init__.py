"""
The config sub-package allows us to provide static informations that are not secret.
"""

from typing import Any
import os
from os.path import dirname, abspath
import json
import logging

# LOGGING
LOGGING_LEVEL = os.environ.setdefault("LOGGING_LEVEL", "INFO")
logging.getLogger("numexpr").setLevel(logging.ERROR)
logging.getLogger("distributed.nanny").setLevel(logging.ERROR)
logging.getLogger("distributed.utils_perf").setLevel(logging.ERROR)
logging.basicConfig(
    level=LOGGING_LEVEL,
    format=(
        "%(asctime)s %(levelname)8s - "
        "%(message)s (%(filename)s:%(lineno)s)"),
    datefmt="%Y-%m-%d %H:%M:%S",
)
log = logging.getLogger("adf")

# ENVIRONMENT
ENV = os.environ.setdefault("ENV", "local")
assert ENV in ("test", "local", "dev", "prod")

# HDFS MOUNTS FOR OUTPUTS
if ENV not in ("local", "test"):
    MOUNT = "/mnt/produced-spc/spc/advanced_forecasting/data"
else:
    MOUNT = f"{dirname(dirname(dirname(abspath(__file__))))}/data"
# STORAGE FOLDERS FOR OUTPUTS
FOLDER_CHECKPOINTS = f"{ENV}/checkpoints"
FOLDER_REPORTS_DATA = f"{ENV}/reports/data"
FOLDER_REPORTS_NEW = f"{ENV}/reports/new"
FOLDER_DATAPREP = f"{ENV}/modelling/dataprep"
FOLDER_DATAPREP_INNO = f"{ENV}/modelling/inno_dataprep"
FOLDER_DATAPREP_PROMO = f"{ENV}/modelling/promo_dataprep"
FOLDER_PREPROCESSING = f"{ENV}/modelling/preprocessing"
FOLDER_TRAINING = f"{ENV}/modelling/training"
FOLDER_FORECAST = f"{ENV}/modelling/forecast"
FOLDER_BUILDING_BLOCKS = f"{ENV}/modelling/building_blocks"
FOLDER_IBP_EXPORT = f"{ENV}/modelling/ibp_export"
FOLDER_IBP_EXPORT_BENCHMARK = f"{ENV}/modelling/ibp_export/benchmark"
IBP_EXPORT_FORECAST_ST_SUFFIX = "_DAYS100DAYFCST"
IBP_EXPORT_SELLOUT_SUFFIX = "_SELLOUT"


# HDFS MOUNTS FOR IBP INTERFACE
if ENV not in ("local", "test"):
    IBP_INTERFACE_MOUNT = "/mnt/produced-spc/spc/advanced_forecasting_ibp_interface/forecasts"
else:
    IBP_INTERFACE_MOUNT = f"{dirname(dirname(dirname(abspath(__file__))))}/ibp_interface/forecasts"

# DATABASE
DB_POOL_SIZE = int(os.environ.setdefault("DB_POOL_SIZE", "32"))

# PROCESSING
DASK_SCHEDULER_ADDRESS = os.getenv("DASK_SCHEDULER_ADDRESS", None)
CORES = 14 if ENV == "dev" else 4

# WEATHER
WEATHER_URI = "https://api.darksky.net/forecast/{key}/{latitude},{longitude}"

# CALENDAR
SCHOOL_HOLIDAYS_URI = {
    "france": (
        "https://data.education.gouv.fr/api/v2/catalog/datasets/fr-en-"
        "calendrier-scolaire/exports/json?rows=-1&pretty=false&timezone=UTC"
    )
}

# EMAIL CONFIG
DEFAULT_EMAILS = ["danone-advanced-forecasting@danone.com"]

# LOCAL DATABASE DEFAULT CREDENTIALS (used for ENV=local and tests)
DEFAULT_CREDENTIALS = {
    "db_user": "postgres",
    "db_pass": "postgres",
    "db_host": "localhost",
    "db_port": 5432,
    "db_name": "danone",
}

# SPAIN DAIRY
SPAIN_PRIMARY_WAREHOUSES = [170, 171, 174, 218]

# MLFLOW
DATABRICKS_MLFLOW_PATH = (
    "/Advanced forecasting/experiments/" if ENV not in ("test", "local") else ""
)

# AZURE
KEY_VAULT_NAME = 'DAN-EU-T-KVT800-S-DB-OFC'
SNOWFLAKE_ACCOUNT = 'danone.west-europe.azure'
SNOWFLAKE_PWD_NAME = ''
SNOWFLAKE_USER = "unknown"
SNOWFLAKE_DATABASE = "unknown"
SNOWFLAKE_WAREHOUSE = "unknown"
SNOWFLAKE_ROLE = "unknown"
SNOWFLAKE_SCHEMA = "unknown"
if ENV == "dev":
    SNOWFLAKE_PWD_NAME = 'dev-dtb-ofc'
    SNOWFLAKE_USER = 'DEV_DTB_OFC'
    SNOWFLAKE_DATABASE = 'DEV_CIS'
    SNOWFLAKE_WAREHOUSE = 'DEV_CIS_ELT_WH'
    SNOWFLAKE_ROLE = 'DEV_CD2'
    SNOWFLAKE_SCHEMA = 'CIS_DSP_IBP'
elif ENV == "prod":
    SNOWFLAKE_PWD_NAME = 'PRD-AFC-DS-PWD'
    SNOWFLAKE_USER = 'PRD_AFC_DS'
    SNOWFLAKE_DATABASE = 'PRD_VCP'
    SNOWFLAKE_WAREHOUSE = 'PRD_VCP_ELT_WH'
    SNOWFLAKE_ROLE = 'PRD_AFC_DS'
    SNOWFLAKE_SCHEMA = 'VCP_EXP_AFC'

SNOWFLAKE_MAPPING_COUNTRIES = {
    "spain": "SPN",
    "canary": "CNR",
    "france": "FRC",
    "poland": "PLD",
    "dach": "DACH",
}
SNOWFLAKE_MAPPING_COUNTRIES_TABLE = {
    "spain": "SPN",
    "canary": "CNR",
    "france": "FRC",
    "poland": "POL",
    "dach": "DAC",
}
SNOWFLAKE_MAPPING_DIVISIONS = {
    "dairy": "EDP",
    "dairy_veg": "EDP",
    "dairy_reg": "EDP",
    "dairy_de": "EDP",
    "dairy_at": "EDP",
    "dairy_ch": "EDP",
    "waters": "WTR"
}


def load_config(filename: str) -> Any:
    filepath = os.path.join(os.path.dirname(__file__), filename)
    with open(filepath) as stream:
        config = json.load(stream)
    return config
