CONFIG_MAPPING = [
    {
        "splits": [],
        "config": "training.json",
        "filters": "filter_dach_de_nfa_with_preorders.json"
    },
    {
        "splits": [],
        "config": "training.json",
        "filters": "filter_dach_de_nfa_edeka.json"
    },
    {
        "splits": [],
        "config": "training.json",
        "filters": "filter_dach_de_nfa_others.json"
    },
    {
        "splits": [],
        "config": "training.json",
        "filters": "filter_dach_de_nfa_rewe.json"
    }
]
