

CONFIG_MAPPING = [
    {
        "splits": [],
        "config": "training.json",
        "filters": "filter_high.json"
    },
    {
        "splits": [],
        "config": "training.json",
        "filters": "filter_low.json"
    },
    {
        "splits": [],
        "config": "training_afh.json",
        "filters": "filter_afh.json"
    },
    {
        "splits": [],
        "config": "training.json",
        "filters": "filter_lot.json"
    }
]
