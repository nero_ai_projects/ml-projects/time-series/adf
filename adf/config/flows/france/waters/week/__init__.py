"""

The 'week' configurations contain configurations for preprocessing and training steps

This rollout can also compute a preprocessing step to prepare
the promotion data on a weekly schedule.

Configuring the PREREQUIRED_FLOW variable allows the flow to use forecasts
from another flow configuration to disaggregate.

"""


PREREQUIRED_FLOW = "day"
