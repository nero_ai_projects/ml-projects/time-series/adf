

CONFIG_MAPPING = [
    {
        "splits": [
            {
                "column": "ordered_volumes",
                "method": "mean",
                "operators": [">"],
                "comparisons": {
                    "nature": "dynamic",
                    "multipliers": [0.1],
                    "functions": ["max"]
                }
            }
        ],
        "config": "training_high.json"
    },
    {
        "splits": [
            {
                "column": "ordered_volumes",
                "method": "mean",
                "operators": ["<="],
                "comparisons": {
                    "nature": "dynamic",
                    "multipliers": [0.1],
                    "functions": ["max"]
                }
            }
        ],
        "config": "training_low.json"
    }
]
