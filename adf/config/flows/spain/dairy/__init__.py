"""

Configurations for Spain dairy rollout

The main forecasts that are used are the configurations from the 'week' folder.
The 'day' configurations are used to disaggregate forecasts from the weekly aggregation.

"""
