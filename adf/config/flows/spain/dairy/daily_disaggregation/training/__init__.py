

CONFIG_MAPPING = [
    {
        "splits": [
            {
                "index": "location_index",
                "method": "in",
                "list": [170]
            },
            {
                "column": "adjusted_units",
                "method": "mean",
                "operators": [">="],
                "comparisons": {
                    "nature": "dynamic",
                    "multipliers": [0.1],
                    "functions": ["max"]
                }
            }
        ],
        "config": "train.json"
    },
    {
        "splits": [
            {
                "index": "location_index",
                "method": "in",
                "list": [171]
            },
            {
                "column": "adjusted_units",
                "method": "mean",
                "operators": [">="],
                "comparisons": {
                    "nature": "dynamic",
                    "multipliers": [0.1],
                    "functions": ["max"]
                }
            }
        ],
        "config": "train.json"
    },
    {
        "splits": [
            {
                "index": "location_index",
                "method": "in",
                "list": [174]
            },
            {
                "column": "adjusted_units",
                "method": "mean",
                "operators": [">="],
                "comparisons": {
                    "nature": "dynamic",
                    "multipliers": [0.1],
                    "functions": ["max"]
                }
            }
        ],
        "config": "train.json"
    },
    {
        "splits": [
            {
                "index": "location_index",
                "method": "in",
                "list": [218]
            },
            {
                "column": "adjusted_units",
                "method": "mean",
                "operators": [">="],
                "comparisons": {
                    "nature": "dynamic",
                    "multipliers": [0.1],
                    "functions": ["max"]
                }
            }
        ],
        "config": "train.json"
    },
    {
        "splits": [
            {
                "index": "location_index",
                "method": "in",
                "list": [170]
            },
            {
                "column": "adjusted_units",
                "method": "mean",
                "operators": ["<"],
                "comparisons": {
                    "nature": "dynamic",
                    "multipliers": [0.1],
                    "functions": ["max"]
                }
            }
        ],
        "config": "train.json"
    },
    {
        "splits": [
            {
                "index": "location_index",
                "method": "in",
                "list": [171]
            },
            {
                "column": "adjusted_units",
                "method": "mean",
                "operators": ["<"],
                "comparisons": {
                    "nature": "dynamic",
                    "multipliers": [0.1],
                    "functions": ["max"]
                }
            }
        ],
        "config": "train.json"
    },
    {
        "splits": [
            {
                "index": "location_index",
                "method": "in",
                "list": [174]
            },
            {
                "column": "adjusted_units",
                "method": "mean",
                "operators": ["<"],
                "comparisons": {
                    "nature": "dynamic",
                    "multipliers": [0.1],
                    "functions": ["max"]
                }
            }
        ],
        "config": "train.json"
    },
    {
        "splits": [
            {
                "index": "location_index",
                "method": "in",
                "list": [218]
            },
            {
                "column": "adjusted_units",
                "method": "mean",
                "operators": ["<"],
                "comparisons": {
                    "nature": "dynamic",
                    "multipliers": [0.1],
                    "functions": ["max"]
                }
            }
        ],
        "config": "train.json"
    }
]
