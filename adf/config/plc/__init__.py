from adf import config


def get_files_configs():
    return {
        "reference_products": {
            "config": config.load_config("plc/reference_products.json"),
            "sales_org_col": "dimension2",
            "date_columns": []
        },
        "forecast_dates": {
            "config": config.load_config("plc/forecast_dates.json"),
            "sales_org_col": "dimension_value",
            "date_columns": ["fcst_start_date", "phasein_start_date",
                             "phasein_end_date", "phaseout_start_date",
                             "phaseout_end_date"]
        }
    }
