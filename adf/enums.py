from enum import Enum


class Index(Enum):

    product = "product_index"
    location = "location_index"
    customer = "customer_index"
    time = "time_index"


class Quantile(Enum):

    minimum = 0.0
    lower = 0.25
    median = 0.50
    upper = 0.75
    maximum = 1.0


Indexes = [Index.product.value, Index.location.value, Index.customer.value, Index.time.value]
