class BusinessUnitError(Exception):
    pass


class SkipError(Exception):
    pass


class DataprepError(Exception):
    pass


class PreprocessingError(Exception):
    pass


class TrainingError(Exception):
    pass


class ForecastError(Exception):
    pass


class IBPExportError(Exception):
    pass


class WeatherError(Exception):
    pass


class NoApiKeysLeftError(WeatherError):
    pass


class AlgorithmTypeNotFound(Exception):
    pass


class BuildingBlocksError(Exception):
    pass


class AzureKeyVaultSecretError(Exception):
    pass


class FlowsError(Exception):
    pass


class DatabricksAPIRequestError(Exception):
    pass
