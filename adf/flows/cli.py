import click
from adf.utils.imports import import_method


@click.group("flows")
def commands() -> None:
    """ Flows utils
    """
    return


@commands.command("run_flow")
@click.option("--country", required=True, type=str)
@click.option("--division", required=True, type=str)
@click.option("--product_level", required=True, type=str)
@click.option("--location_level", required=True, type=str)
@click.option(
    "--run_type", required=False,
    type=click.Choice(["core"]), default=None
)
@click.option("--pipeline_id", required=False, type=str, default=None)
@click.option(
    "--from_step", required=False,
    type=click.Choice(["preprocessing", "training", "ibp_export"]),
    default=None
)
@click.option(
    "--to_step", required=False,
    type=click.Choice(["dataprep", "preprocessing", "training"]),
    default=None
)
@click.option(
    "--algorithm_type", required=False,
    type=click.Choice(["day", "week", "month"]), default=None
)
@click.option(
    "--backtest/--no-backtest", required=False,
    default=False,
    help="whether to calculate Shap values on the backtest period")
def run_flow(
        country: str,
        division: str,
        product_level: str,
        location_level: str,
        run_type: str,
        pipeline_id: str,
        from_step: str,
        to_step: str,
        algorithm_type: str,
        backtest: bool
) -> None:
    """ Runs the flow that generates forecasts once
    """
    return import_method("adf.flows.flow", "run_adf_flow")(
        country,
        division,
        product_level,
        location_level,
        run_type,
        algorithm_type,
        backtest,
        pipeline_id,
        from_step,
        to_step
    )


@commands.command("run_flows")
@click.option("--country", required=True, type=str)
@click.option("--division", required=True, type=str)
@click.option("--product_level", required=True, type=str)
@click.option("--location_level", required=True, type=str)
@click.option(
    "--run_type", required=False,
    type=click.Choice(["core"]), default=None
)
def run_flows(
        country: str,
        division: str,
        product_level: str,
        location_level: str,
        run_type: str
) -> None:
    """ Runs one or multiple flows that generates forecasts depending on the rollout expected
    schedules
    """
    return import_method("adf.flows.flow", "run_multi_adf_flows")(
        country,
        division,
        product_level,
        location_level,
        run_type
    )


@commands.command("run_promo_flow")
@click.option("--country", required=True, type=str)
@click.option("--division", required=True, type=str)
@click.option("--product_level", required=True, type=str)
@click.option("--location_level", required=True, type=str)
@click.option("--pipeline_id", required=False, type=str, default=None)
@click.option(
    "--from_step", required=False,
    type=click.Choice(
        [
            "promo_preprocessing",
            "finalize_promo",
            "retailers_promo",
            "uplifts_promo"
        ]
    ),
    default=None
)
@click.option("--building_block", required=False, type=bool, default=False)
@click.option("--compute_market_shares", required=False, type=bool, default=False)
@click.option(
    "--run_type", required=False,
    type=click.Choice(["promo_core"]), default=None
)
@click.option("--cannib", required=False, type=bool, default=False)
def run_promo_flow(
        country: str,
        division: str,
        product_level: str,
        location_level: str,
        pipeline_id: str,
        from_step: str,
        building_block: bool,
        compute_market_shares: bool,
        run_type: str,
        cannib: bool
) -> None:
    """ Runs the promo flow once
    """
    return import_method("adf.flows.flow", "run_promo_flow")(
        country,
        division,
        product_level,
        location_level,
        pipeline_id,
        from_step,
        building_block,
        compute_market_shares,
        run_type,
        cannib
    )


@commands.command("run_calendar_flow")
@click.option("--country", required=True, type=str)
@click.option("--year_shift", required=False, type=int, default=0,
              help="indicates the number of years to shift from current year to perform request")
@click.option("--depth", required=False, type=int, default=1,
              help="number of months for which to extract data from current month, including "
                   "current month")
@click.option("--by_zones", required=False, type=bool, default=True,
              help="indicates whether extracts must be split by provinces or not")
@click.option(
    "--run_type", required=False,
    type=click.Choice(["calendar_core"]), default=None
)
def run_calendar_flow(
        country: str,
        year_shift: int,
        depth: int,
        by_zones: bool,
        run_type: str
) -> None:
    """ Runs the calendar flow once
    """
    return import_method("adf.flows.flow", "run_calendar_flow")(
        country,
        year_shift,
        depth,
        by_zones,
        run_type
    )


@commands.command("run_weather_flow")
@click.option("--country", required=True, type=str)
@click.option("--division", required=True, type=str)
@click.option(
    "--run_type", required=False,
    type=click.Choice(["weather_core"]), default=None
)
def run_weather_flow(
        country: str,
        division: str,
        run_type: str
) -> None:
    """ Runs the weather flow once
    """
    return import_method("adf.flows.flow", "run_weather_flow")(
        country,
        division,
        run_type
    )


@commands.command("run_plc_flow")
@click.option("--country", required=True, type=str)
@click.option("--division", required=True, type=str)
@click.option("--sales_org_cod", required=True, type=str)
@click.option(
    "--run_type", required=False,
    type=click.Choice(["plc_core"]), default=None
)
def run_plc_flow(
        country: str,
        division: str,
        sales_org_cod: str,
        run_type: str
) -> None:
    """ Runs the plc flow once
    """
    return import_method("adf.flows.flow", "run_plc_flow")(
        country,
        division,
        sales_org_cod,
        run_type
    )
