import logging
import pytz
from datetime import date, timedelta, datetime
from dateutil.relativedelta import relativedelta
import uuid
from adf.errors import FlowsError
from adf.flows.tasks import (
    validate_parameters, create_pipeline, fetch_algorithm_types, run_calendar, get_training_configs,
    validate_preprocessor, validate_trainer, validate_filters,
    run_prepare, run_preprocessing, run_training_and_forecast,
    run_ibp_export, mark_pipeline_as_success, run_uplifts_promo,
    run_weather, run_promo_dataprep, run_finalize_promo, run_retailers_promo,
    run_promo_means, run_prepare_promo
)
from adf.utils.imports import import_method
from adf.services.database import (
    get_session, PipelineReports, ForecastReports
)
from adf.services.database.query import get_report
from adf.modelling.ibp_export.tools.report import build_ibp_report

log = logging.getLogger("adf")


def get_pipeline(pipeline_id):
    return get_report(
        get_session(),
        pipeline_id,
        PipelineReports,
        fail_for_status=[]
    )


def run_multi_adf_flows(
        country, division, product_level, location_level, run_type
):
    """
    Runs several global modeling pipelines (preprocessing, training, forecast
    and IBP exports generation) from a single data preparation, for the given parameters and
    algorithm type specified for each one.
    """
    algorithm_types = fetch_algorithm_types(country, division)
    if not algorithm_types:
        build_ibp_report(
            country,
            division,
            description="No pipeline planned in the scheduling calendar",
            status="aborted",
            reason="no_pipeline_planned",
            pipeline_id=None,
            pipeline_run_type=None
        )
    for algorithm_type in algorithm_types:
        run_adf_flow(
            country, division, product_level, location_level, run_type, algorithm_type
        )


def run_adf_flow(
        country, division, product_level, location_level, run_type, algorithm_type,
        backtest=False, pipeline_id=None, from_step=None, to_step=None
):
    session = get_session()
    try:
        if pipeline_id:
            flow_run_id = uuid.UUID(str(pipeline_id))
        else:
            flow_run_id = uuid.uuid4()

        log.info(f"Flow ID : {str(flow_run_id)}")
        validate_parameters(
            country,
            division,
            product_level,
            location_level
        )

        if not pipeline_id:
            create_pipeline(
                session,
                flow_run_id=flow_run_id,
                algorithm_type=algorithm_type,
                run_type=run_type
            )

        if from_step is None:

            core_report, innovation_report = run_prepare(
                country,
                division,
                product_level,
                location_level,
                flow_run_id=flow_run_id
            )

        elif pipeline_id:
            core_reports = [
                report for report in get_pipeline(flow_run_id).dataprep_report
                if report.prep_type == "core"
            ]
            core_report = sorted(core_reports, key=lambda x: x.updated_at, reverse=True)[0]
            innovation_reports = [
                report for report in get_pipeline(flow_run_id).dataprep_report
                if report.prep_type == "innovation"
            ]
            innovation_report = (
                sorted(core_reports, key=lambda x: x.updated_at, reverse=True)[0]
                if innovation_reports else None
            )
            if not innovation_report:
                log.warning("Could not find any innovation report")

        else:
            raise FlowsError("Configuration error for dataprep...")

        validate_preprocessor(algorithm_type, country, division)
        validate_trainer(algorithm_type, country, division)
        validate_filters(algorithm_type, country, division, "training")

        prerequired_forecasts = None
        if from_step in ["preprocessing", None] and to_step not in ["dataprep"]:
            prerequired_forecasts = get_prerequired_forecasts(
                country,
                division,
                product_level,
                location_level,
                run_type,
                algorithm_type,
                pipeline_id=flow_run_id
            )
            preprocessing = run_preprocessing(
                algorithm_type,
                core_report,
                country, division,
                flow_run_id=flow_run_id,
                config_type="preprocessing"
            )
        elif pipeline_id:
            preprocessing_reports = [
                report for report in get_pipeline(flow_run_id).preprocessing_report
            ]
            preprocessing = sorted(
                preprocessing_reports,
                key=lambda x: (
                    pytz.UTC.localize(x.updated_at) if x.updated_at
                    else pytz.UTC.localize(datetime.min)
                ),
                reverse=True
            )[0]
        else:
            raise FlowsError("Configuration error for preprocessing...")

        if (
                from_step in ["training", "preprocessing", None]
        ) and (
                to_step not in ["dataprep", "preprocessing"]
        ):
            training_configs = get_training_configs(
                algorithm_type=algorithm_type,
                country=country,
                division=division
            )
            forecasts = []
            for training_config in training_configs:
                forecasts.append(run_training_and_forecast(
                    training_config,
                    preprocessing_result=preprocessing,
                    flow_run_id=flow_run_id,
                    backtest=backtest,
                    algorithm_type=algorithm_type,
                    kpis=True,
                ))
        elif pipeline_id:
            training_configs = get_training_configs(
                algorithm_type=algorithm_type,
                country=country,
                division=division
            )
            trainings = sorted(
                [
                    training
                    for training in get_pipeline(flow_run_id).training_report
                ],
                key=lambda x: (
                    pytz.UTC.localize(x.created_at) if x.created_at
                    else pytz.UTC.localize(datetime.min)
                ),
                reverse=True
            )
            trainings = [
                training
                for training in trainings
                if training.description["models"][0]["id"] != ""
            ][:len(training_configs)]
            forecasts = [
                forecast
                for forecast in get_pipeline(flow_run_id).forecast_report
                if forecast.training_report_id in [
                    training.id for training in trainings
                ]
            ]
        else:
            raise FlowsError("Configuration error for training and forecast...")

        if (
                from_step in ["ibp_export", "training", "preprocessing", None]
        ) and (
                to_step not in ["dataprep", "preprocessing", "training"]
        ):
            if (country, division) in [
                ("spain", "dairy"),
                ("poland", "waters"),
                ("poland", "dairy"),
                ("france", "waters"),
                ("france", "dairy_reg"),
                ("france", "dairy_veg"),
                ("dach", "dairy_de"),
                ("dach", "dairy_at"),
                ("dach", "dairy_ch"),
                ("canary", "dairy"),
            ]:
                run_ibp_export(
                    forecasts=forecasts,
                    prerequired_forecasts=prerequired_forecasts,
                    backtest=backtest,
                    flow_run_id=flow_run_id,
                    flow_run_type=run_type
                )

        mark_pipeline_as_success(flow_run_id=flow_run_id)

    except FlowsError as err:
        message = str(err.__class__.__name__) + ": " + str(err)
        build_ibp_report(
            country,
            division,
            description="",
            status="failed",
            reason=message,
            pipeline_id=None,
            pipeline_run_type=None
        )
        raise FlowsError(message)


def run_promo_flow(
    country, division, product_level, location_level,
    pipeline_id, from_step, building_block,
    compute_market_shares, run_type, cannib
):
    """
    Runs a dataprep specific to promotions then runs a preprocessing step with a specific config
    and finally, aggregates the data into another dataprep report that will be used
    to merge the promotions with the core model dataprep
    """
    session = get_session()
    if pipeline_id:
        flow_run_id = uuid.UUID(pipeline_id)
    else:
        flow_run_id = uuid.uuid4()
    log.info(f"Flow ID : {str(flow_run_id)}")
    if not pipeline_id:
        create_pipeline(
            session=session,
            flow_run_id=flow_run_id,
            algorithm_type="week",
            run_type=run_type
        )
    if from_step is None and not pipeline_id:
        orders_report, promo_report = run_prepare_promo(
            country,
            division,
            product_level,
            location_level,
            building_block,
            compute_market_shares,
            flow_run_id=flow_run_id,
            cannib=cannib
        )
    elif pipeline_id:
        promo_reports = [
            report for report in get_pipeline(flow_run_id).dataprep_report
            if report.scope == "promo_report"
        ]
        promo_reports = sorted(
            promo_reports, key=lambda x: x.created_at, reverse=True
        )
        promo_report = promo_reports[0] if len(promo_reports) else None
        orders_reports = [
            report for report in get_pipeline(flow_run_id).dataprep_report
            if report.scope == "orders_report"
        ]
        orders_reports = sorted(
            orders_reports, key=lambda x: x.created_at, reverse=True
        )
        orders_report = orders_reports[0] if len(orders_reports) else None
    else:
        raise FlowsError("Configuration error for dataprep...")
    if from_step is None:
        promo_dataprep_report, _ = run_promo_dataprep(
            country,
            division,
            product_level,
            location_level,
            building_block,
            compute_market_shares,
            flow_run_id=flow_run_id,
            promo_report=promo_report,
            orders_report=orders_report,
            cannib=cannib
        )
    elif pipeline_id:
        promo_dataprep_reports = [
            report for report in get_pipeline(flow_run_id).dataprep_report
            if report.scope == "promo"
        ]
        promo_dataprep_report = sorted(
            promo_dataprep_reports, key=lambda x: x.updated_at, reverse=True
        )[0]
    else:
        raise FlowsError("Configuration error for dataprep...")

    if country in ['spain']:
        if from_step in ["promo_preprocessing", None]:
            promo_preprocessing_report = run_preprocessing(
                "week",
                promo_dataprep_report,
                country, division,
                flow_run_id=flow_run_id,
                config_type="promo_preprocessing",
                distributed=False
            )
        elif pipeline_id:
            promo_preprocessing_reports = [
                report for report in get_pipeline(flow_run_id).preprocessing_report
                if report.pipeline_id == flow_run_id and report.updated_at is not None
            ]
            promo_preprocessing_report = sorted(
                promo_preprocessing_reports, key=lambda x: x.updated_at, reverse=True
            )[0]
        else:
            raise FlowsError("Configuration error for preprocessing...")

    if from_step in ["finalize_promo", "promo_preprocessing", None]:
        if country == 'france' and division in ['dairy_reg', 'dairy_veg']:
            run_promo_means(
                country, division, product_level,
                location_level, flow_run_id=flow_run_id,
                orders_report=orders_report,
                promo_report=promo_report
            )

            if cannib:
                run_uplifts_promo(
                    country,
                    division,
                    product_level,
                    location_level,
                    True,
                    promo_dataprep_report,
                    flow_run_id=flow_run_id
                )
        else:
            run_finalize_promo(
                country, division,
                product_level, location_level,
                promo_preprocessing_report,
                flow_run_id=flow_run_id
            )
    elif pipeline_id:
        log.info("Nothing to do for finalize_promo...")
    else:
        raise FlowsError("Configuration error for finalize_promo...")

    if country in ['spain']:
        if from_step in ["retailers_promo", "finalize_promo", "promo_preprocessing", None]:
            run_retailers_promo(
                country, division,
                product_level, location_level,
                promo_dataprep_report,
                flow_run_id=flow_run_id
            )
        elif pipeline_id:
            log.info("Nothing to do for retailers_promo...")
        else:
            raise Exception("Configuration error for retailers_promo...")

    if from_step in ["uplifts_promo",
                     "retailers_promo",
                     "finalize_promo",
                     "promo_preprocessing",
                     None]:
        run_uplifts_promo(
            country,
            division,
            product_level,
            location_level,
            False,
            promo_dataprep_report,
            flow_run_id=flow_run_id
        )
    elif pipeline_id:
        log.info("Nothing to do for uplifts_promo...")
    else:
        raise FlowsError("Configuration error for uplifts_promo...")

    mark_pipeline_as_success(flow_run_id=flow_run_id)


def run_calendar_flow(country, year_shift, depth, by_zones, run_type):
    """
    Extracts calendar data for a given country, splitting by zones or not, from first day of current
    month (shifted or not based on year_shift), for a number of months specified by depth input
    """
    session = get_session()
    flow_run_id = uuid.uuid4()
    create_pipeline(
        session,
        flow_run_id=flow_run_id,
        algorithm_type="any",
        run_type=run_type
    )
    today = date.today()
    start_date = date(today.year + year_shift, today.month, 1)
    end_date = start_date + relativedelta(months=depth) - timedelta(days=1)
    run_calendar(
        country,
        start_date.strftime("%Y-%m-%d"),
        end_date.strftime("%Y-%m-%d"),
        add_zones=by_zones,
        flow_run_id=flow_run_id
    )
    mark_pipeline_as_success(flow_run_id=flow_run_id)


def run_weather_flow(country, division, run_type):
    """
    Extracts weather data for previous day on a given country and division
    """
    session = get_session()
    flow_run_id = uuid.uuid4()
    create_pipeline(
        session,
        flow_run_id=flow_run_id,
        algorithm_type="any",
        run_type=run_type
    )
    run_weather(
        country,
        division,
        None,
        None,
        flow_run_id=flow_run_id
    )
    mark_pipeline_as_success(flow_run_id=flow_run_id)


def get_prerequired_forecasts(
        country,
        division,
        product_level,
        location_level,
        run_type,
        algorithm_type,
        pipeline_id
):
    prerequired_algorithm_type = None
    try:
        prerequired_algorithm_type = import_method(
            f"adf.config.flows.{country}.{division}.{algorithm_type}.__init__",
            "PREREQUIRED_FLOW"
        )
        log.info("prerequired_algorithm_type : {}".format(prerequired_algorithm_type))
    except (ModuleNotFoundError, AttributeError):
        return None
    if prerequired_algorithm_type and prerequired_algorithm_type != algorithm_type:
        pipeline_id = str(pipeline_id)
        run_adf_flow(
            country, division, product_level, location_level, run_type, prerequired_algorithm_type,
            pipeline_id=pipeline_id, from_step="preprocessing", to_step="training"
        )
        forecasts = get_session().query(ForecastReports) \
            .filter(ForecastReports.pipeline_id == pipeline_id) \
            .all()
        return [str(forecast.id) for forecast in forecasts]
