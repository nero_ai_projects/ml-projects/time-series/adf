import traceback
from typing import List, Optional, Tuple, Union
from functools import partial
import os
import smtplib
from uuid import UUID
from datetime import datetime, date
import logging
from jinja2 import Environment, PackageLoader  # type: ignore
from timeout_decorator import timeout, TimeoutError  # type: ignore
from sqlalchemy.orm import Session  # type: ignore
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from adf.flows.tools.scheduling import get_algorithm_types
from adf.flows.tools.pipeline_configs import get_pipeline_configs
from adf.flows.tools.report import update_pipeline_report
from adf.utils.provider import Provider
from adf.services.calendar.main import get_holidays
from adf.services.database import (
    PreprocessingReports, DataprepReports,
    ForecastReports, PipelineReports
)
from adf.services.database.query import (
    build_report, get_business_unit
)
from adf.flows.tools.pipeline_configs import PipelineConfigs
from adf.services.weather.main import fetch_historical_for_dates_or_last_day
from adf.modelling.pipeline import prepare
from adf.modelling.preprocessing.tools.preprocessor import Preprocessor
from adf.modelling.training.tools.trainer import Trainer
from adf.modelling.forecast.tools.forecaster import Forecaster
from adf.modelling import preprocessing
from adf.modelling import training
from adf.modelling import forecast as forecasting
from adf.modelling.ibp_export import main as export_to_ibp
from adf.utils.filters import Filters
from adf.errors import AlgorithmTypeNotFound
from adf.config import DEFAULT_EMAILS, ENV
from adf.utils.imports import import_method

jinja_env = Environment(
    loader=PackageLoader('adf', 'config/mails'),
)

Provide = partial(Provider, mlrun=True, session=True, cluster=True)
log = logging.getLogger("adf")


@timeout(60)
def send_email(email_template, pipeline_id, title, message, emails_to):
    mailserver = smtplib.SMTP('mail.nead.danet', 25)
    from_addr = os.environ.get("EMAIL_USERNAME")
    msg = MIMEMultipart()
    msg['From'] = from_addr
    msg['To'] = ", ".join(emails_to)
    msg['Subject'] = title
    msg.attach(
        MIMEText(
            email_template.render(
                pipeline_id=pipeline_id,
                message=message,
                logs_url=f"https://eu-west-3.console.aws.amazon.com/cloudwatch/home"
                         f"?region=eu-west-3#logsV2:log-groups/log-group/"
                         f"artefact/log-events/{ENV}$252F{pipeline_id}"
            ),
            'html'
        )
    )
    mailserver.ehlo()
    mailserver.starttls()
    mailserver.login(os.environ.get("EMAIL_USERNAME"), os.environ.get("EMAIL_PASSWORD"))
    mailserver.sendmail(from_addr, emails_to, msg.as_string())
    mailserver.quit()


def check_for_failure(fn):
    def wrapper(*args, **kwargs):
        try:
            return fn(*args, **kwargs)
        except Exception as e:
            update_pipeline_report(
                kwargs.get("flow_run_id"),
                "failure",
                f"The flow {kwargs.get('flow_run_id')} ended in a failure : "
                f"Error message : {e.message if hasattr(e, 'message') else e}"
            )
            template = jinja_env.get_template('error_template.html')
            try:
                send_email(
                    template,
                    kwargs.get("flow_run_id"),
                    title="An error occurred during pipeline run.",
                    message=e.message if hasattr(e, 'message') else e,
                    emails_to=DEFAULT_EMAILS
                )
            except TimeoutError as err:
                log.error(f"Failed to send email: {err}")
            except Exception:
                log.error(f'Unhandled : Failed to send email : {traceback.format_exc()}')
            raise e

    return wrapper


@check_for_failure
def validate_parameters(
        country: str,
        division: str,
        product_level: str,
        location_level: str
):
    if any(
            value is None for value in [country, division, product_level, location_level]
    ):
        raise Exception(
            "country, division, product_leveland location_level "
            "are required parameters"
        )
    if location_level not in ["national", "regional", "departmental", "warehouse"]:
        raise Exception(
            f"Location level : {location_level} is not valid."
            f"Choose between : national, regional, departmental and warehouse"
        )
    if product_level not in ["fu", "sku"]:
        raise Exception(
            f"Product level : {product_level} is not valid."
            f"Choose between : fu and sku"
        )
    return True


def create_pipeline(
        session: Session,
        flow_run_id: Union[str, UUID],
        algorithm_type: str,
        run_type: str
) -> PipelineReports:
    report_fields = {
        "id": flow_run_id,
        "algorithm_type": algorithm_type,
        "run_type": run_type,
        "reason": "Pipeline flow has been started..."
    }
    return build_report(session, PipelineReports, "running", **report_fields)


@check_for_failure
def fetch_algorithm_types(
        country: str,
        division: str
) -> Union[List[str], str]:
    status, output = get_algorithm_types(country, division)
    if status:
        return output
    raise AlgorithmTypeNotFound(
        f"{output} for country {country}, division {division} and date {date.today()} !"
    )


@check_for_failure
def validate_preprocessor(
        algorithm_type,
        country: str,
        division: str
) -> List[Preprocessor]:
    configs = get_pipeline_configs(country, division, algorithm_type, "preprocessing")
    result = []
    for config in configs:
        result.append(Preprocessor(config.config))
    return result


@check_for_failure
def validate_trainer(
        algorithm_type,
        country: str,
        division: str
) -> List[Trainer]:
    configs = get_pipeline_configs(country, division, algorithm_type, "training")
    result = []
    for config in configs:
        result.append(Trainer(config.config))
    return result


@check_for_failure
def validate_forecaster(
        algorithm_type,
        country: str,
        division: str
) -> List[Forecaster]:
    configs = get_pipeline_configs(country, division, algorithm_type, "forecast")
    result = []
    for config in configs:
        result.append(Forecaster(config.config))
    return result


@check_for_failure
def validate_filters(
        algorithm_type,
        country: str,
        division: str,
        step: str
) -> List[Filters]:
    configs = get_pipeline_configs(country, division, algorithm_type, step)
    result = []
    for config in configs:
        if config.filters:
            result.append(Filters(config.filters))
    return result


@check_for_failure
def run_calendar(
        country: str,
        start: str,
        end: str,
        add_zones: bool,
        **kwargs
) -> None:
    get_holidays(country, start, end, add_zones=add_zones)


@check_for_failure
def run_weather(
        country: str,
        division: str,
        start: Union[str, None],
        end: Union[str, None],
        **kwargs
) -> None:
    fetch_historical_for_dates_or_last_day(
        country,
        division,
        datetime.strptime(start, "%Y-%m-%d").date() if start else None,
        datetime.strptime(end, "%Y-%m-%d").date() if end else None
    )


@check_for_failure
def get_etl_tasks(
        session: Session,
        country: str,
        division: str,
        source: Union[str, None],
        **kwargs
):
    if source:
        return [{"country": country, "division": division, "source": source}]
    else:
        bu = get_business_unit(session, country, division)
        result = []
        for data_source in bu.data_sources:
            result.append({"country": country, "division": division, "source": data_source.source})
        return result


@check_for_failure
def run_prepare(
        country: str,
        division: str,
        product_level: str,
        location_level: str,
        flow_run_id: Union[str, UUID]
) -> Tuple[DataprepReports, DataprepReports]:
    core_report, inno_report = prepare(
        country,
        division,
        product_level,
        location_level,
        pipeline_id=flow_run_id
    )
    return core_report, inno_report


@check_for_failure
def run_preprocessing(
        algorithm_type: str,
        preparing: DataprepReports,
        country: str, division: str,
        flow_run_id: Union[str, UUID],
        config_type: str = "preprocessing",
        distributed: bool = False
) -> Optional[PreprocessingReports]:
    configs = get_pipeline_configs(country, division, algorithm_type, config_type)
    result = []
    for config in configs:
        result.append(
            Provide("preprocessing")(preprocessing.run)(
                config.config,
                input_mount=preparing.mount,
                input_blob=preparing.blob,
                pipeline_id=flow_run_id
            )
        )
    return next(iter(result), None)


@check_for_failure
def get_training_configs(
        algorithm_type: str,
        country: str,
        division: str
) -> List[PipelineConfigs]:
    return get_pipeline_configs(country, division, algorithm_type, "training")


@check_for_failure
def run_training_and_forecast(
        training_config: PipelineConfigs,
        preprocessing_result: PreprocessingReports,
        flow_run_id: Union[str, UUID],
        backtest: bool = False,
        algorithm_type: str = None,
        kpis: bool = False,
        distributed: bool = False
) -> ForecastReports:
    training_result = partial(
        Provider, mlrun=True, session=True, cluster=False
    )("training")(training.run)(
        training_config.config, preprocessing_result.id, training_config.filters,
        splits_config=training_config.splits, backtest=backtest,
        forecast=False, algorithm_type=algorithm_type, pipeline_id=flow_run_id
    )
    return Provide("forecasting")(forecasting.run)(
        training_config.config, training_result.id, preprocessing_result.mount,
        preprocessing_result.blob, training_config.filters, kpis,
        splits_config=training_config.splits, algorithm_type=algorithm_type,
        pipeline_id=flow_run_id
    )


@check_for_failure
def run_promo_dataprep(
        country: str,
        division: str,
        product_level: str,
        location_level: str,
        building_block: bool,
        compute_market_shares: bool,
        flow_run_id: Union[str, UUID],
        promo_report: DataprepReports = None,
        orders_report: DataprepReports = None,
        cannib: bool = False
) -> DataprepReports:
    if country == 'france' and division in ['dairy_reg', 'dairy_veg']:
        return Provide("dataprep")(import_method(
            f"adf.modelling.dataprep.{country}.dairy.promo", "run"
        ))(
            country,
            division,
            product_level,
            location_level,
            building_block=building_block,
            compute_market_shares=compute_market_shares,
            pipeline_id=flow_run_id,
            promo_report=promo_report,
            orders_report=orders_report
        )
    else:
        return Provide("dataprep")(import_method(
            f"adf.modelling.dataprep.{country}.{division}.promo", "run"
        ))(
            country,
            division,
            product_level,
            location_level,
            building_block=building_block,
            compute_market_shares=compute_market_shares,
            pipeline_id=flow_run_id
        )


@check_for_failure
def run_prepare_promo(
        country: str,
        division: str,
        product_level: str,
        location_level: str,
        building_block: bool,
        compute_market_shares: bool,
        flow_run_id: Union[str, UUID],
        cannib: bool = False
) -> Tuple[DataprepReports, DataprepReports]:
    if country == 'france' and division in ['dairy_reg', 'dairy_veg']:
        return Provide("dataprep")(import_method(
            f"adf.modelling.dataprep.{country}.dairy.promo", "prepare_promo"
        ))(
            country,
            division,
            product_level,
            location_level,
            building_block=building_block,
            compute_market_shares=compute_market_shares,
            pipeline_id=flow_run_id
        )
    else:
        return DataprepReports(), DataprepReports()


@check_for_failure
def run_finalize_promo(
        country: str, division: str,
        product_level: str, location_level: str,
        preprocessing_result: PreprocessingReports,
        flow_run_id: Union[str, UUID]
) -> DataprepReports:
    return import_method(f"adf.modelling.dataprep.{country}.{division}.promo", "finalize_promo")(
        product_level,
        location_level,
        preprocessing_result,
        pipeline_id=flow_run_id
    )


@check_for_failure
def run_retailers_promo(
        country: str, division: str,
        product_level: str, location_level: str,
        dataprep_report: DataprepReports,
        flow_run_id: Union[str, UUID]
) -> DataprepReports:
    return import_method(f"adf.modelling.dataprep.{country}.{division}.promo", "retailers_promo")(
        product_level,
        location_level,
        dataprep_report,
        pipeline_id=flow_run_id
    )


@check_for_failure
def run_promo_means(
        country: str, division: str,
        product_level: str, location_level: str,
        flow_run_id: Union[str, UUID],
        promo_report: DataprepReports = None,
        orders_report: DataprepReports = None
) -> DataprepReports:
    if country == 'france' and division in ['dairy_reg', 'dairy_veg']:
        return import_method(f"adf.modelling.dataprep.{country}.dairy.promo", "promo_means")(
            country,
            division,
            product_level,
            location_level,
            pipeline_id=flow_run_id,
            promo_report=promo_report,
            orders_report=orders_report
        )
        # return partial(
        #     Provider, mlrun=True, session=True, cluster=False
        # )("promo_means")(
        #     import_method(f"adf.modelling.dataprep.{country}.dairy.promo", "promo_means")
        # )(country,
        #   division,
        #   product_level,
        #   location_level,
        #   pipeline_id=flow_run_id
        #   )
    else:
        raise Exception("Method promo means not implemented for given country and division")


@check_for_failure
def run_uplifts_promo(
        country: str,
        division: str,
        product_level: str,
        location_level: str,
        cannib: bool,
        dataprep_report: DataprepReports,
        flow_run_id: Union[str, UUID]
) -> DataprepReports:
    if country == 'france' and division in ['dairy_reg', 'dairy_veg']:
        return import_method(f"adf.modelling.dataprep.{country}.dairy.promo", "uplifts_promo")(
            country,
            division,
            product_level,
            location_level,
            cannib,
            dataprep_report,
            pipeline_id=flow_run_id
        )
    else:
        return import_method(f"adf.modelling.dataprep.{country}.{division}.promo", "uplifts_promo")(
            product_level,
            location_level,
            dataprep_report,
            pipeline_id=flow_run_id
        )


@check_for_failure
def run_ibp_export(
        forecasts: List[ForecastReports],
        prerequired_forecasts: List[str],
        backtest: bool,
        flow_run_id: Union[str, UUID],
        flow_run_type: str
):
    return Provide("ibp_export")(export_to_ibp.run)(
        forecast_ids=[forecast.id for forecast in forecasts],
        pipeline_id=flow_run_id,
        pipeline_run_type=flow_run_type,
        prerequired_forecast_ids=prerequired_forecasts,
        backtest=backtest
    )


@check_for_failure
def mark_pipeline_as_success(
        flow_run_id: Union[str, UUID]
):
    update_pipeline_report(
        flow_run_id,
        "success",
        f"The flow {flow_run_id} has ended successfully !"
    )


@check_for_failure
def mark_pipeline_as_failure(
        flow_run_id: Union[str, UUID],
        reason: str
):
    update_pipeline_report(
        flow_run_id,
        "failure",
        reason=reason
    )
