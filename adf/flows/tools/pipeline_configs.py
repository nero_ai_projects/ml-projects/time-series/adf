import json
from pathlib import Path
from datetime import datetime, date
from jsonschema import validate  # type: ignore
from adf import config
from adf.utils.imports import import_method


class PipelineConfigs:

    splits = None
    filters = None

    FILTERS_SCHEMA = config.load_config("modelling/filters.json")

    def __init__(
            self,
            country: str,
            division: str,
            algorithm_type: str,
            step: str,
            **properties
    ):
        self.country = country
        self.division = division
        self.algorithm_type = algorithm_type
        self.step = step
        self.rotation = None
        self.split_location = None
        if 'config' not in properties:
            raise Exception("PipelineConfigs missing 'config' property!")
        for k, v in properties.items():
            if k not in ['config', 'filters']:
                setattr(self, k, v)
        self.config = json.load(
            open(
                Path(__file__).parent.parent.parent / (
                    f"config/flows/{country}/{division}/{algorithm_type}"
                    f"/{step}/{properties['config']}"
                ),
                "r"
            )
        )
        self.update_config(country, division, algorithm_type, step)
        if 'filters' in properties:
            self.filters = json.load(
                open(
                    Path(__file__).parent.parent.parent / (
                        f"config/flows/{country}/{division}/{algorithm_type}/"
                        f"{step}/{properties['filters']}"
                    ),
                    "r"
                )
            )
            validate(self.filters, self.FILTERS_SCHEMA)

        if "rotation" in properties:
            self.rotation = properties["rotation"]
        if "split_location" in properties:
            self.split_location = properties["split_location"]

    def update_config(
            self, country: str, division: str, algorithm_type: str, step: str
    ):
        """
        Updates training configuration in order to remove days in France Waters Daily models list.
        This enables to forecast on a fix period of time which does not depend on daily
        view_from day of week.
        """
        if country == "france":
            if division == "waters":
                if algorithm_type == "day":
                    if step == "training":
                        view_from_dates = self.config.get("predict_from_dates", None)
                        if view_from_dates is not None:
                            if len(view_from_dates) == 0:
                                modulo = date.today().isoweekday() % 7
                                # iso_weekday = 7 means Sundays
                                # If modulo is 0, it means we are on a Sunday
                                # And, we do not update config
                            else:
                                view_from_weekdays = [
                                    datetime.strptime(view_from, "%Y-%m-%d").date().isoweekday()
                                    for view_from in view_from_dates
                                ]
                                if len(set(view_from_weekdays)) > 1:
                                    # Weekdays are not all the same
                                    modulo = 0  # Do not update config
                                else:
                                    modulo = view_from_weekdays[0] % 7
                            if modulo > 0:
                                self.config["models"] = self.config["models"][:-modulo]


def get_pipeline_configs(
        country: str, division: str,
        algorithm_type: str, step: str
):
    configs = import_method(
        f"adf.config.flows.{country}.{division}.{algorithm_type}.{step}",
        "CONFIG_MAPPING"
    )
    return [
        PipelineConfigs(country, division, algorithm_type, step, **config) for config in configs
    ]
