from uuid import UUID
from typing import Union, List
from adf.services.database import PipelineReports
from adf.services.database import get_session


def get_pipeline_report(pipeline_id: str) -> List[PipelineReports]:
    session = get_session()
    return session.query(PipelineReports).filter(PipelineReports.id == pipeline_id).all()


def update_pipeline_report(pipeline_id: Union[str, UUID], status: str, reason: str):
    session = get_session()
    for report in session.query(PipelineReports).filter(PipelineReports.id == pipeline_id).all():
        report.status = status
        report.reason = reason
    session.commit()
