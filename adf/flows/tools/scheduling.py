from typing import Union, List, Tuple
from datetime import date, timedelta
from adf.modelling.tools import get_snowflake_df

algorithm_type_mapping = {
    "daily": ("day", 2),
    "weekly": ("week", 1),
    "monthly": ("month", 0)
}


def get_algorithm_types(country: str, division: str) -> Tuple[bool, Union[List[str], str]]:

    if country in ("spain", "canary"):

        from adf.config import (
            SNOWFLAKE_DATABASE,
            SNOWFLAKE_MAPPING_COUNTRIES,
            SNOWFLAKE_MAPPING_COUNTRIES_TABLE,
            SNOWFLAKE_MAPPING_DIVISIONS
        )

        schedules = get_snowflake_df(
            SNOWFLAKE_DATABASE,
            f"VCP_DSP_AFC_{SNOWFLAKE_MAPPING_DIVISIONS[division]}_"
            f"{SNOWFLAKE_MAPPING_COUNTRIES[country]}",
            f"R_MAN_{SNOWFLAKE_MAPPING_DIVISIONS[division]}_"
            f"{SNOWFLAKE_MAPPING_COUNTRIES_TABLE[country]}_CAL_JOB_SCD"
        )
        # In order to push export data on time and to align with business needs to use a
        # manual file for job scheduling we are running pipeline on the day
        # before the day specified in the scheduling file
        execution_dates = schedules[
            schedules.algorithm_output_day == (date.today() + timedelta(1)).strftime("%Y-%m-%d")
        ]

        if len(execution_dates) == 0:
            return False, "No algorithm type"

        execution_algos = execution_dates[execution_dates.execute_algorithm == "yes"].algorithm_type
        algorithm_types = execution_algos.values if len(execution_algos) > 0 else False

        if not isinstance(algorithm_types, bool):
            outputs = []
            for algorithm_type in algorithm_types:
                output = algorithm_type_mapping.get(algorithm_type, False)
                if isinstance(output, bool):
                    return False, "Unknown algorithm type"
                outputs.append(output)
            return True, [val[0] for val in sorted(outputs, key=lambda x: x[1])]
        else:
            no_execution = execution_dates[execution_dates.execute_algorithm == "no"]
            if len(no_execution) > 0:
                return False, "No execution scheduled"
    if country == "poland":
        if division == "waters":
            if date.today().weekday() >= 1:  # Tuesday
                return True, ["week"]
            else:
                return False, "Don't execute algorithm, on Mondays"
        if division == "dairy":
            if date.today().weekday() in [1, 2, 3, 4]:  # From Tuesday to Friday
                return True, ["week"]
            else:
                return False, "Don't execute algorithm on non working days or mondays"
    if country == "france":
        if division == "waters":
            if date.today().weekday() in [4, 6]:  # Friday and Sunday
                return True, ["week"]
            else:
                return False, "Don't execute algorithm, except on Fridays and Sundays"
        elif division == "dairy_reg" or division == "dairy_veg":
            if date.today().weekday() == 0:  # Monday
                return True, ["week"]
            else:
                return False, "Don't execute algorithm, except on Mondays"
    if country == "dach":
        if division in ("dairy_at", "dairy_de", "dairy_ch"):
            if date.today().weekday() == 1:  # Tuesday
                return True, ["week"]
            else:
                return False, "Don't execute algorithm, except on Tuesdays"
    else:
        if date.today().weekday() == 0:  # Monday
            return True, ["week"]

    return False, "No algorithm to execute, except on Mondays"
