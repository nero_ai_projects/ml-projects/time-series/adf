import click
from typing import List
import os
import json
import logging
import great_expectations as ge  # type: ignore
from dask import delayed, compute  # type: ignore

log = logging.getLogger("adf")


@click.group("great_expectations")
def commands() -> None:
    """ Data Quality
    """
    return


@commands.command("run")
@click.option("--datasource", required=True, type=str)
@click.option("--data_asset_name", required=False, type=str)
@click.option("--generator_name", required=False, type=str, default="pandas_s3_generator")
@click.option("--profiler_name", required=False, type=str, default="DatasetProfiler")
def run_on_missing(
    datasource, data_asset_name, generator_name, profiler_name
) -> List:
    """ Run validation on files that were not already validated
    """
    context = ge.data_context.DataContext(
        context_root_dir=os.path.dirname(os.path.abspath(__file__))
    )
    datasource_obj = context.get_datasource(datasource)
    results = []
    if not data_asset_name:
        data_asset_names = [
            name[0]
            for name in datasource_obj.get_available_data_asset_names()[generator_name]['names']
        ]
        for data_asset_name in data_asset_names:
            results.append(run_for_data_asset(
                context, datasource_obj, data_asset_name, generator_name, profiler_name
            ))
    else:
        results.append(run_for_data_asset(
            context, datasource_obj, data_asset_name, generator_name, profiler_name
        ))

    log.info("Validation process completed")
    return results


def run_for_data_asset(context, datasource_obj, data_asset_name, generator_name, profiler_name):
    log.info(f"Running on {datasource_obj.name}.{data_asset_name}")
    batch_kwargs_template = context.build_batch_kwargs(
        datasource=datasource_obj.name, batch_kwargs_generator=generator_name,
        data_asset_name=data_asset_name
    )

    prefix = datasource_obj \
        .get_batch_kwargs_generator(generator_name) \
        .assets[data_asset_name]["prefix"]
    missing_data_info = get_files_missing_validation(
        context, datasource_obj,
        data_asset_name, prefix, generator_name
    )
    prefix = missing_data_info["prefix"]
    batches = []
    for file in missing_data_info["missing_keys"]:
        batch = batch_kwargs_template.copy()
        batch["s3"] = prefix.join([batch["s3"].split(prefix)[0], file])
        batches.append(
            (
                batch,
                '.'.join([datasource_obj.name, generator_name, data_asset_name, profiler_name])
            )
        )
    results = []
    for batch in batches:
        batch = context.get_batch(*batch)
        results.append(
            delayed(context.validation_operators["action_list_operator"].run)(
                assets_to_validate=[batch],
                run_name=f"{datasource_obj.name}_{data_asset_name}"
            )
        )

    return compute(*results)


def get_files_missing_validation(
    context, datasource, data_asset_name, prefix, generator_name="pandas_s3_generator"
):
    all_files = []
    for key in context.stores["validations_store"].store_backend.list_keys():
        if datasource.name in key[0] and (data_asset_name == key[2] or not data_asset_name):
            result = json.loads(context.stores["validations_store"].store_backend.get(key))
            datasource = context.get_datasource(result["meta"]["batch_kwargs"]["datasource"])
            data_asset_name = result["meta"]["batch_kwargs"]["data_asset_name"]
            prefix = datasource. \
                get_batch_kwargs_generator(generator_name). \
                assets[data_asset_name]["prefix"]
            file = result["meta"]["batch_kwargs"]["s3"].split(prefix)[1]
            all_files.append(file)
    existing_partitions = datasource. \
        get_batch_kwargs_generator(generator_name). \
        get_available_partition_ids(data_asset_name)
    return {
        "completed": len(list(set(all_files))) / len(existing_partitions),
        "prefix": prefix,
        "missing_keys": list(set(existing_partitions) - set(all_files))
    }
