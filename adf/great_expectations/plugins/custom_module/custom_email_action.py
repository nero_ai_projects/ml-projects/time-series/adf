import logging
import smtplib
import ssl
import textwrap
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from great_expectations.core.id_dict import BatchKwargs  # type: ignore
from great_expectations.render.renderer.renderer import Renderer  # type: ignore
from great_expectations.validation_operators.actions import ValidationAction  # type: ignore
from great_expectations.data_context.types.resource_identifiers import (  # type: ignore
    ValidationResultIdentifier
)
from great_expectations.data_context.util import instantiate_class_from_config  # type: ignore
from great_expectations.exceptions import ClassInstantiationError  # type: ignore


logger = logging.getLogger(__name__)


class EmailRenderer(Renderer):
    def __init__(self):
        super().__init__()

    def render(
        self,
        validation_result=None,
        data_docs_pages=None,
        notify_with=None
    ):
        default_text = (
            "No validation occurred. Please ensure you passed a validation_result."
        )
        status = "Failed ❌"

        title = default_text

        html = default_text

        if validation_result:
            expectation_suite_name = validation_result.meta.get(
                "expectation_suite_name", "__no_expectation_suite_name__"
            )

            if "batch_kwargs" in validation_result.meta:
                data_asset_name = validation_result.meta["batch_kwargs"].get(
                    "data_asset_name", "__no_data_asset_name__"
                )
            else:
                data_asset_name = "__no_data_asset_name__"

            n_checks_succeeded = validation_result.statistics["successful_expectations"]
            n_checks = validation_result.statistics["evaluated_expectations"]
            run_id = validation_result.meta.get("run_id", "__no_run_id__")
            batch_id = BatchKwargs(
                validation_result.meta.get("batch_kwargs", {})
            ).to_id()
            check_details_text = (
                f"<strong>{n_checks_succeeded}</strong> "
                f"of <strong>{n_checks}</strong> expectations were met"
            )

            if validation_result.success:
                status = "Success 🎉"

            title = f"{expectation_suite_name}: {status}"

            html = textwrap.dedent(
                f"""\
                <p><strong>Batch Validation Status</strong>: {status}</p>
                <p><strong>Expectation suite name</strong>: {expectation_suite_name}</p>
                <p><strong>Data asset name</strong>: {data_asset_name}</p>
                <p><strong>Run ID</strong>: {run_id}</p>
                <p><strong>Batch ID</strong>: {batch_id}</p>
                <p><strong>Summary</strong>: {check_details_text}</p>"""
            )
            if validation_result.meta.get("batch_kwargs", {}).get("s3", None):
                s3_path = validation_result.meta.get("batch_kwargs", {}).get("s3")
                s3_uri = f"s3://{s3_path[6:]}"
                s3_splits = s3_path[6:].split('/')
                s3_bucket = s3_splits[0]
                s3_prefix = '/'.join(s3_splits[1:])
                s3_object_url = (
                    f"https://s3.console.aws.amazon.com/"
                    f"s3/object/{s3_bucket}?prefix={s3_prefix}"
                )
                html += f"<p><strong>S3 URI</strong>: {s3_uri}</p>"
                html += (
                    f'<p><strong>S3 Object URL</strong>: <a href="{s3_object_url}">here</a></p>'
                )
            if data_docs_pages:
                if notify_with is not None:
                    for docs_link_key in notify_with:
                        if docs_link_key in data_docs_pages.keys():
                            docs_link = data_docs_pages[docs_link_key]
                            report_element = self._get_report_element(docs_link)
                        else:
                            report_element = (
                                f"<strong>ERROR</strong>: The email is trying to provide a link "
                                "to the following DataDocs: "
                                f"`{str(docs_link_key)}`, but it is not configured "
                                "under data_docs_sites in the great_expectations.yml</br>"
                            )
                            logger.critical(
                                report_element
                            )
                        if report_element:
                            html += report_element
                else:
                    for docs_link_key in data_docs_pages.keys():
                        if docs_link_key == "class":
                            continue
                        docs_link = data_docs_pages[docs_link_key]
                        report_element = self._get_report_element(docs_link)
                        if report_element:
                            html += report_element

            if "result_reference" in validation_result.meta:
                result_reference = validation_result.meta["result_reference"]
                report_element = f"- <strong>Validation Report</strong>: {result_reference}</br>"
                html += report_element

            if "dataset_reference" in validation_result.meta:
                dataset_reference = validation_result.meta["dataset_reference"]
                report_element = (
                    f"- <strong>Validation data asset</strong>: {dataset_reference}</br>"
                )
                html += report_element

        documentation_url = (
            "https://docs.greatexpectations.io/en/latest/guides/tutorials/"
            "getting_started/set_up_data_docs.html"
        )
        footer_section = (
            f'<p>Learn <a href="{documentation_url}">here</a> '
            'how to review validation results in Data Docs</p>'
        )
        html += footer_section
        return title, html

    def _get_report_element(self, docs_link):
        report_element = None
        if docs_link:
            try:
                if "file://" in docs_link:
                    # handle special case since the email does not render these links
                    report_element = (
                        '<p><strong>DataDocs</strong> can be found here: '
                        '<a href="{docs_link}">{docs_link}</a>.</br>'
                        "(Please copy and paste link into a browser to view)</p>",
                    )
                else:
                    report_element = (
                        '<p><strong>DataDocs</strong> can be found here: '
                        '<a href="{docs_link}">{docs_link}</a>.</p>'
                    )
            except Exception as e:
                logger.warning(
                    f"EmailRenderer had a problem with generating the docs link."
                    f"link used to generate the docs link is: {docs_link} "
                    "and is of type: {type(docs_link)}."
                    f"Error: {e}"
                )
                return
        else:
            logger.warning(
                "No docs link found. Skipping data docs link in the email message."
            )
        return report_element


class EmailNotificationAction(ValidationAction):
    """
    EmailNotificationAction sends an email to a given list of email addresses.

    **Configuration**

    .. code-block:: yaml

        name: send_email_on_validation_result
        action:
          class_name: StoreValidationResultAction
          # put the actual following informations in the uncommitted/config_variables.yml file
          # or pass in as environment variable
          smtp_address: ${smtp_address}
          smtp_port: ${smtp_port}
          sender_email_address: ${email_address}
          sender_email_password: ${sender_email_password}
          # receiver_emails = string containing email addresses separated by commas
          receiver_emails: ${receiver_emails}
          notify_on: all # possible values: "all", "failure", "success"
          # notify_with = optional list of DataDocs site names to display in the email message.
          # Defaults to showing all
          notify_with:
          renderer:
            # the class that implements the message to be sent
            # this is the default implementation, but you can
            # implement a custom one
            module_name: great_expectations.render.renderer.email_renderer
            class_name: EmailRenderer

    """

    def __init__(
        self,
        data_context,
        renderer,
        smtp_address,
        smtp_port,
        sender_email_address,
        sender_email_password,
        receiver_emails,
        notify_on="all",
        notify_with=None,
    ):
        """Construct an EmailNotificationAction
        Args:
            data_context:
            renderer: dictionary specifying the renderer used to generate an email, for example:
                {
                   "module_name": "great_expectations.render.renderer.email_renderer",
                   "class_name": "EmailRenderer",
               }
            smtp_address: address of the SMTP server used to send the email
            smtp_address: port of the SMTP server used to send the email
            sender_email_address: email address used to send the email
            sender_email_password: email address password used to send the email
            receiver_emails: email addresses that will be receive the email
              (separated by commas)
            notify_on: "all", "failure", "success"
              specifies validation status that will trigger notification
            notify_with: optional list of DataDocs site names to display in the email message
        """
        super().__init__(data_context)
        self.renderer = instantiate_class_from_config(
            config=renderer,
            runtime_environment={},
            config_defaults={},
        )
        module_name = renderer["module_name"]
        if not self.renderer:
            raise ClassInstantiationError(
                module_name=module_name,
                package_name=None,
                class_name=renderer["class_name"],
            )
        self.smtp_address = smtp_address
        self.smtp_port = smtp_port
        self.sender_email_address = sender_email_address
        self.sender_email_password = sender_email_password
        self.receiver_emails_list = list(map(lambda x: x.strip(), receiver_emails.split(',')))
        assert smtp_address, "No SMTP server address found in action config."
        assert smtp_port, "No SMTP server port found in action config."
        assert sender_email_address, (
            "No email address found for sending the email in action config."
        )
        assert sender_email_address, (
            "No email address password found for sending the email in action config."
        )
        assert receiver_emails, (
            "No email addresses to send the email to in action config."
        )
        self.notify_on = notify_on
        self.notify_with = notify_with

    def _run(
        self,
        validation_result_suite,
        validation_result_suite_identifier,
        data_asset=None,
        payload=None,
    ):
        logger.debug("EmailNotificationAction.run")

        if validation_result_suite is None:
            return

        if not isinstance(
            validation_result_suite_identifier, ValidationResultIdentifier
        ):
            raise TypeError(
                "validation_result_suite_id must be of "
                "type ValidationResultIdentifier, not {}".format(
                    type(validation_result_suite_identifier)
                )
            )

        validation_success = validation_result_suite.success
        data_docs_pages = None

        if payload:
            # process the payload
            for action_names in payload.keys():
                if payload[action_names]["class"] == "UpdateDataDocsAction":
                    data_docs_pages = payload[action_names]

        if (
            self.notify_on == "all"
        ) or (
            self.notify_on == "success" and validation_success
        ) or (
            self.notify_on == "failure" and not validation_success
        ):
            title, html = self.renderer.render(
                validation_result_suite, data_docs_pages, self.notify_with
            )
            # this will actually send the email
            email_result = send_email(
                title,
                html,
                self.smtp_address,
                self.smtp_port,
                self.sender_email_address,
                self.sender_email_password,
                self.receiver_emails_list
            )

            # sending payload back as dictionary
            return {"email_notification_result": email_result}
        else:
            return {"email_notification_result": ""}


def send_email(
    title,
    html,
    smtp_address,
    smtp_port,
    sender_email_address,
    sender_email_password,
    receiver_emails_list
):
    mailserver = smtplib.SMTP(smtp_address, smtp_port)
    msg = MIMEMultipart()
    msg['From'] = sender_email_address
    msg['To'] = ", ".join(receiver_emails_list)
    msg['Subject'] = title
    msg.attach(
        MIMEText(
            html,
            'html'
        )
    )
    context = ssl.create_default_context()
    mailserver.starttls(context=context)
    mailserver.login(sender_email_address, sender_email_password)
    errors = mailserver.sendmail(sender_email_address, receiver_emails_list, msg.as_string())
    mailserver.quit()
    return errors
