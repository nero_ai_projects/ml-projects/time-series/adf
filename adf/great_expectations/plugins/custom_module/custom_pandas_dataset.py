import great_expectations as ge  # type: ignore
import pandas as pd  # type: ignore


class CustomPandasDataSet(ge.dataset.PandasDataset):

    _data_asset_type = "CustomPandasDataset"

    @ge.dataset.MetaPandasDataset.column_pair_map_expectation
    def expect_column_pair_map_values_to_have_nto1_relationship(
        self,
        column_A,
        column_B,
        mostly=None,
        ignore_row_if="both_values_are_missing",
        result_format=None,
        row_condition=None,
        condition_parser=None,
        *args,
        **kwargs
    ):
        df = pd.concat([column_A, column_B], join='outer', axis=1)
        relationship = df.groupby(column_A.name)[column_B.name].apply(
            lambda x: x.nunique() == 1
        )
        result = pd.DataFrame(column_A).merge(relationship, on=column_A.name)
        return result[column_B.name]
