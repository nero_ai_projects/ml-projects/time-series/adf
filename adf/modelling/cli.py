import click
from adf.utils.imports import import_method


@click.group("modelling")
def commands() -> None:
    """ ML modelling
    """
    return


@commands.command("dataprep")
@click.option("--country", required=True, type=str)
@click.option("--division", required=True, type=str)
@click.option(
    "--product_level",
    required=True,
    type=click.Choice(["sku", "fu", "brand"])
)
@click.option(
    "--location_level",
    required=True,
    type=click.Choice(["national", "regional", "departmental", "warehouse"])
)
def prepare(
    country: str,
    division: str,
    product_level: str,
    location_level: str,
) -> None:
    """ Generate panel from ingested data
    """
    import_method("adf.modelling.pipeline", "prepare")(
        country, division, product_level, location_level
    )


@commands.command("preprocessing")
@click.option(
    "--config_path", required=True,
    type=click.Path(exists=True, readable=True),
    help="path to preprocessing configuration file")
@click.option(
    "--input_mount", required=False,
    help="mount of the input file")
@click.option(
    "--input_blob", required=False,
    help="blob of the input file")
def preprocess(config_path: str, input_mount: str, input_blob: str) -> None:
    """ Generate features and normalize data for training
    """
    import_method("adf.modelling.pipeline", "preprocess")(
        config_path, input_mount, input_blob
    )


@commands.command("training")
@click.option(
    "--config_path", required=True,
    type=click.Path(exists=True, readable=True),
    help="path to training configuration file")
@click.option(
    "--preprocessing_id", required=True,
    help="id of preprocessing that contains data for training")
@click.option(
    "--filters_path", required=False,
    type=click.Path(exists=True),
    help="an optional path to filter file used on training/forecast")
@click.option(
    "--splits_path", required=False,
    type=click.Path(exists=True),
    default=None,
    help="an optional path used for splitting preprocessing data")
@click.option(
    "--forecast/--no-forecast", required=False,
    default=False,
    help="whether to perform the forecasting step or not"
)
@click.option(
    "--kpis/--no-kpis", required=False,
    default=True,
    help="whether to calculate performance KPIs for forecasting step")
@click.option(
    "--backtest/--no-backtest", required=False,
    default=False,
    help="whether to calculate Shap values on the backtest period")
def train(
    config_path: str,
    preprocessing_id: str,
    filters_path: str,
    splits_path: str,
    forecast: bool,
    kpis: bool,
    backtest: bool
) -> None:
    """ Train model using preprocessed data
    """
    import_method("adf.modelling.pipeline", "train")(
        config_path, preprocessing_id, filters_path,
        splits_path=splits_path,
        forecast=forecast, kpis=kpis, backtest=backtest
    )


@commands.command("forecast")
@click.option(
    "--config_path",
    type=click.Path(exists=True, readable=True),
    help="path to forecast configuration file")
@click.option(
    "--training_id", required=True,
    help="id of training that contains model for forecast")
@click.option(
    "--input_mount", required=True,
    help="mount of the input file")
@click.option(
    "--input_blob", required=True,
    help="blob of the input file")
@click.option(
    "--filters_path", required=False,
    type=click.Path(exists=True),
    help="an optional path to filter file")
@click.option(
    "--kpis/--no-kpis", required=False,
    default=True,
    help="whether to calculate performance KPIs for forecasting step")
def forecast(
    config_path: str,
    training_id: str,
    input_mount: str,
    input_blob: str,
    filters_path: str,
    kpis: bool
) -> None:
    """ Run forecast using trained model
    """
    import_method("adf.modelling.pipeline", "forecast")(
        config_path, training_id, input_mount, input_blob, filters_path, kpis
    )


@commands.command("ibp_export")
@click.option(
    "--forecast_ids", required=True, multiple=True,
    help="id of forecast(s) results to be exported")
@click.option(
    "--pipeline_id", required=False, type=str, default=None,
    help="If IBP Export comes from a pipeline, please specify its ID"
)
@click.option(
    "--dataprep_id", required=False, type=str, default=None,
    help="If IBP Export doesn't come from a pipeline, please specify the dataprep ID"
)
@click.option(
    "--prediction_date", required=False, type=str, default=None,
    help="If forecasts contain several prediction dates, please specify a prediction date to keep"
)
@click.option(
    "--prerequired_forecast_ids", required=False, multiple=True, default=None,
    help="id of prerequired forecast(s) needed for disaggregation"
)
@click.option(
    "--backtest/--no-backtest", required=False, multiple=False, default=False,
    help="Whether to test a disaggregation on the backtest period"
)
def ibp_export(
    forecast_ids, pipeline_id, dataprep_id, prediction_date, prerequired_forecast_ids, backtest
):
    """ Export forecast data to ibp standard
    """
    import_method("adf.modelling.pipeline", "ibp_export")(
        forecast_ids, pipeline_id=pipeline_id,
        dataprep_id=dataprep_id, prediction_date=prediction_date,
        prerequired_forecast_ids=prerequired_forecast_ids,
        backtest=backtest
    )


@commands.command("inno_forecasting")
@click.option(
    "--config_path",
    type=click.Path(exists=True, readable=True),
    help="path to innovation configuration file")
@click.option(
    "--training_id", required=True,
    help="id of training of the classification model")
def launch_forecasting(
    config_path: str,
    training_id: str
):
    import_method("adf.modelling.pipeline", "inno_forecaster")(
        config_path, training_id
    )


@commands.command("inno_training")
@click.option(
    "--config_path",
    type=click.Path(exists=True, readable=True),
    help="path to innovation configuration file")
@click.option(
    "--preprocessing_id", required=True,
    help="id of preprocessing that contains data for training")
def train_inno(
    config_path: str,
    preprocessing_id: str
):
    import_method("adf.modelling.pipeline", "inno_trainer")(
        config_path, preprocessing_id
    )
