# from adf.errors import DataprepError
# from adf.modelling.dataprep.france.waters import panel as france_waters
# from adf.modelling.dataprep.france.dairy import panel as france_dairy
# from adf.modelling.dataprep.spain.dairy import panel as spain_dairy
# from adf.modelling.dataprep.dach.dairy import panel as dach_dairy
# from adf.modelling.dataprep.poland.waters import panel as poland_waters
# from adf.modelling.dataprep.poland.dairy import panel as poland_dairy
# from adf.modelling.dataprep.canary.dairy import panel as canary_dairy


# DATAPREP = {
#     ("france", "waters"): france_waters,
#     ("france", "dairy_reg"): france_dairy,
#     ("france", "dairy_veg"): france_dairy,
#     ("spain", "dairy"): spain_dairy,
#     ("dach", "dairy_de"): dach_dairy,
#     ("dach", "dairy_at"): dach_dairy,
#     ("dach", "dairy_ch"): dach_dairy,
#     ("poland", "waters"): poland_waters,
#     ("poland", "dairy"): poland_dairy,
#     ("canary", "dairy"): canary_dairy,
# }


# def get_dataprep(country: str, division: str):
#     key = (country, division)
#     if key not in DATAPREP:
#         raise DataprepError("No setup found for %s" % str(key))
#     return DATAPREP[key]
