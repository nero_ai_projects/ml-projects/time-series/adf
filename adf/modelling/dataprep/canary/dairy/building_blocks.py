import traceback
import logging
import pandas as pd  # type: ignore
import numpy as np  # type: ignore
from adf.errors import BuildingBlocksError
from adf.utils.decorators import timeit


log = logging.getLogger("adf")


@timeit
def build_bb(promo_df):
    try:
        promo_df = scope_promo(promo_df)
        promo_df = cast_and_sort_promo(promo_df)

        uplift_df = add_uplift(promo_df)
        uplift_cols = [col for col in uplift_df.columns if "_uplift" in col]
        uplift_df = uplift_df.dropna(subset=uplift_cols, how="all")
        uplift_df = uplift_df.fillna(0)
        uplift_df = aggregate_uplifts(uplift_df)

        return uplift_df

    except Exception as err:
        traceback.print_exc()
        message = str(err.__class__.__name__) + ": " + str(err)
        raise BuildingBlocksError(message)


# TODO: Parameterize this
def scope_promo(promo_df):
    promo_df = promo_df.set_index(["mat_ava_dat"])
    promo_df = promo_df.dropna(subset=["ord_sku_qty"])
    promo_df = promo_df.reset_index()
    return promo_df


def cast_and_sort_promo(promo_df):
    promo_df = promo_df.astype(
        {"mat_cod": "int32", "cus_superchain_cod": "int32", "ord_sku_qty": "int32"}
    )
    return promo_df.sort_values(by=["mat_ava_dat", "mat_cod", "cus_superchain_cod"])


def add_uplift(promo_df):
    uplift_df = create_uplift_table(promo_df)
    uplift_df = regularize_volumes(uplift_df)
    uplift_df = add_annual_means(uplift_df)
    uplift_df = compute_uplifts(uplift_df)

    return uplift_df


def create_uplift_table(promo_df):
    uplift_df = pd.pivot_table(
        promo_df,
        index=["cus_superchain_cod", "mat_ava_dat", "mat_cod"],
        columns=["promotion_mechanics_cod"],
        values="ord_sku_qty",
        aggfunc=np.sum,
    )
    uplift_df = uplift_df.fillna(0)
    return uplift_df


def regularize_volumes(uplift_df):
    """
    Regularizes volumes if there are sales for non-promo and promo the same time
    """
    reg_cols = uplift_df.columns.tolist()
    reg_cols.remove("0")
    for col in reg_cols:
        uplift_df.loc[(uplift_df[col] > uplift_df["0"]), col] = uplift_df["0"] + uplift_df[col]
        uplift_df.loc[(uplift_df[col] > uplift_df["0"]), "0"] = 0

        uplift_df.loc[(uplift_df["0"] > uplift_df[col]), "0"] = uplift_df["0"] + uplift_df[col]
        uplift_df.loc[(uplift_df["0"] > uplift_df[col]), col] = 0

    return uplift_df


def add_annual_means(uplift_df):
    for column in uplift_df.columns:
        mean_df = uplift_df.loc[uplift_df[column] > 0]
        mean_df = mean_df.groupby(["mat_cod", "cus_superchain_cod"]).agg(
            **{f"{column}_annual_mean": (column, "mean")}
        )
        uplift_df = uplift_df.join(mean_df, on=["mat_cod", "cus_superchain_cod"], how="left")

    return uplift_df


def compute_uplifts(uplift_df):
    promo_cod = [col for col in uplift_df.columns if "_annual_mean" not in col]
    promo_cod.remove("0")
    for col in promo_cod:
        uplift_df[f"{col}_uplift"] = uplift_df[f"{col}_annual_mean"] / uplift_df["0_annual_mean"]

    uplift_cols = [col for col in uplift_df.columns if "_uplift" in col]
    uplift_df = uplift_df.sort_values(by=["mat_ava_dat", "mat_cod"])
    uplift_df[uplift_cols] = uplift_df[uplift_cols] - 1
    uplift_df[uplift_cols] = uplift_df[uplift_cols].clip(0)
    return uplift_df


def aggregate_uplifts(uplift_df):
    uplift_cols = [col for col in uplift_df.columns if "_uplift" in col]
    agg = {col: "mean" for col in uplift_cols}
    uplift_df = uplift_df.groupby(["mat_cod", "cus_superchain_cod"]).agg(agg).reset_index()
    return uplift_df
