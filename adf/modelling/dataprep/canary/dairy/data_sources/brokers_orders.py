import pandas as pd  # type: ignore
from adf.modelling.tools import get_snowflake_df_spark  # type: ignore
import pyspark.sql.functions as sf  # type: ignore
import pyspark.sql.types as st  # type: ignore
from adf.config import SNOWFLAKE_DATABASE


def fetch(sku_replacer=None, mixed_trays_replacer=None):
    brokers_orders = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_CNR", "F_AFC_EDP_CNR_BRK_ORD", "spark"
    )

    decimal_cols = [
        f.name for f in brokers_orders.schema.fields if isinstance(f.dataType, st.DecimalType)
    ]

    for column in decimal_cols:
        brokers_orders = brokers_orders.withColumn(column, sf.col(column).cast(st.FloatType()))

    brokers_orders = brokers_orders.drop_duplicates().toPandas()

    if sku_replacer:
        brokers_orders = sku_replacer(brokers_orders)
    if mixed_trays_replacer:
        brokers_orders = mixed_trays_replacer(brokers_orders)
    brokers_orders = brokers_orders.drop_duplicates()
    brokers_orders = brokers_orders.rename(columns={"shp_cus_cod": "cus_cod"})
    brokers_orders = brokers_orders[brokers_orders.plt_cod.notnull()]
    brokers_orders = brokers_orders[brokers_orders.plt_cod != 350001855]
    brokers_orders["plt_cod"] = brokers_orders["plt_cod"].replace([350006384], 350001693)
    last_cus_ord = (
        brokers_orders.groupby(["cus_cod"])
        .sal_cre_dat.max()
        .reset_index()
        .rename(columns={"sal_cre_dat": "last_ord_dat"})
    )

    last_cus_ord.last_ord_dat = pd.to_datetime(
        last_cus_ord.last_ord_dat, format="%Y-%m-%d", errors="coerce"
    )

    real_cus = last_cus_ord[
        (pd.to_datetime("today", format="%Y-%m-%d", errors="coerce") - last_cus_ord.last_ord_dat) <
        pd.to_timedelta(120, "days")
    ][["cus_cod"]]
    brokers_orders = brokers_orders.merge(
        real_cus,
        how="inner",
        on="cus_cod",
    )
    brokers_orders = (
        brokers_orders.groupby(
            [
                "sales_org_cod",
                "plt_cod",
                "sal_cre_dat",
                "cus_cod",
                "mat_cod",
            ]
        )
        .ord_sku_qty.sum()
        .reset_index()
    )
    brokers_orders = brokers_orders[
        [
            "sales_org_cod",
            "plt_cod",
            "sal_cre_dat",
            "cus_cod",
            "mat_cod",
            "ord_sku_qty",
        ]
    ].drop_duplicates()

    return brokers_orders
