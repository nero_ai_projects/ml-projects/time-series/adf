from adf.modelling.tools import get_snowflake_df_spark  # type: ignore
import pyspark.sql.functions as sf  # type: ignore
import pyspark.sql.types as st  # type: ignore
from adf.config import SNOWFLAKE_DATABASE


def fetch():
    customers = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_CNR", "R_MAN_EDP_CNR_CUS_SAL", "spark"
    )

    decimal_cols = [
        f.name for f in customers.schema.fields if isinstance(f.dataType, st.DecimalType)
    ]

    for column in decimal_cols:
        customers = customers.withColumn(column, sf.col(column).cast(st.FloatType()))

    customers = customers.toPandas()

    customers = customers.groupby(["cus_cod"]).cus_superchain_cod.first().reset_index()
    customers = customers[
        [
            "cus_cod",
            "cus_superchain_cod",
        ]
    ].drop_duplicates()

    return customers
