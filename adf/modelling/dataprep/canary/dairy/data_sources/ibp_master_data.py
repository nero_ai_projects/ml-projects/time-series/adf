from adf.modelling.tools import get_snowflake_df_spark  # type: ignore
import pyspark.sql.functions as sf  # type: ignore
import pyspark.sql.types as st  # type: ignore
from adf.config import SNOWFLAKE_DATABASE


def fetch():
    ibp_master_data = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_CNR", "R_IBP_EDP_CNR_MAT_STS", "spark"
    )

    decimal_cols = [
        f.name for f in ibp_master_data.schema.fields if isinstance(f.dataType, st.DecimalType)
    ]

    for column in decimal_cols:
        ibp_master_data = ibp_master_data.withColumn(column, sf.col(column).cast(st.FloatType()))

    ibp_master_data = ibp_master_data.drop_duplicates().toPandas()

    ibp_master_data = ibp_master_data.drop_duplicates()
    ibp_master_data = ibp_master_data[ibp_master_data.product_activation_cod != "Z4"]
    ibp_master_data = ibp_master_data[ibp_master_data.product_aggregation != "STOP ALGORITHM"]
    ibp_master_data = ibp_master_data.groupby(["mat_cod"]).first().reset_index()
    ibp_master_data = ibp_master_data[["mat_cod"]].drop_duplicates()

    return ibp_master_data
