import numpy as np  # type: ignore
import pandas as pd  # type: ignore
from adf.modelling.tools import get_snowflake_df_spark  # type: ignore
import pyspark.sql.functions as sf  # type: ignore
import pyspark.sql.types as st  # type: ignore
from adf.config import SNOWFLAKE_DATABASE


def fetch():
    mixed_trays = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_CNR", "R_MAN_EDP_CNR_MAT_MIX", "spark"
    )

    decimal_cols = [
        f.name for f in mixed_trays.schema.fields if isinstance(f.dataType, st.DecimalType)
    ]

    for column in decimal_cols:
        mixed_trays = mixed_trays.withColumn(column, sf.col(column).cast(st.FloatType()))

    mixed_trays = mixed_trays.drop_duplicates().toPandas()

    mixed_trays = mixed_trays.drop_duplicates()
    mixed_trays = (
        mixed_trays.assign(
            comp=mixed_trays[["comp1", "comp2"]].agg(np.array, axis=1),
            cant=mixed_trays[["cant1", "cant2"]].agg(np.array, axis=1),
        )
        .set_index(["material", "material_description"])
        .drop(columns=["comp1", "comp2", "desc1", "desc2", "cant1", "cant2"])
        .apply(pd.Series.explode)
        .dropna()
        .reset_index()
    )
    mixed_trays = mixed_trays[
        [
            "material",
            "material_description",
            "comp",
            "cant",
        ]
    ].drop_duplicates()

    return mixed_trays
