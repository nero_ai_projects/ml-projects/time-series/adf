import pandas as pd  # type: ignore
from adf.modelling.tools import get_snowflake_df_spark  # type: ignore
import pyspark.sql.functions as sf  # type: ignore
import pyspark.sql.types as st  # type: ignore
from adf.config import SNOWFLAKE_DATABASE


def fetch(sku_replacer=None, mixed_trays_replacer=None):
    orders = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_CNR", "F_AFC_EDP_CNR_ORD", "spark"
    )

    decimal_cols = [f.name for f in orders.schema.fields if isinstance(f.dataType, st.DecimalType)]

    for column in decimal_cols:
        orders = orders.withColumn(column, sf.col(column).cast(st.FloatType()))

    orders = orders.drop_duplicates().toPandas()
    if sku_replacer:
        orders = sku_replacer(orders)
    if mixed_trays_replacer:
        orders = mixed_trays_replacer(orders)
    orders = orders.rename(columns={"shp_cus_cod": "cus_cod"})
    orders["cus_cod"] = orders["cus_cod"].replace([350006384], 350001693)
    last_cus_ord = (
        orders.groupby(["cus_cod"])
        .mat_ava_dat.max()
        .reset_index()
        .rename(columns={"mat_ava_dat": "last_ord_dat"})
    )

    last_cus_ord.last_ord_dat = pd.to_datetime(
        last_cus_ord.last_ord_dat, format="%Y-%m-%d", errors="coerce"
    )
    real_cus = last_cus_ord[
        (pd.to_datetime("today", format="%Y-%m-%d", errors="coerce") - last_cus_ord.last_ord_dat) <
        pd.to_timedelta(120, "days")
    ][["cus_cod"]]
    orders = orders.merge(
        real_cus,
        how="inner",
        on="cus_cod",
    )
    orders = (
        orders.groupby(
            [
                "sales_org_cod",
                "plt_cod",
                "mat_ava_dat",
                "cus_cod",
                "mat_cod",
            ]
        )
        .ord_sku_qty.sum()
        .reset_index()
    )
    orders = orders[
        [
            "sales_org_cod",
            "plt_cod",
            "mat_ava_dat",
            "cus_cod",
            "mat_cod",
            "ord_sku_qty",
        ]
    ].drop_duplicates()

    return orders
