from . import plc_forecast_dates
from . import plc_product_assignment


def fetch():
    plc_forecast_dates_data = plc_forecast_dates.fetch()
    plc_product_assignment_data = plc_product_assignment.fetch()

    plc = (
        plc_forecast_dates_data.set_index("mat_cod")
        .join(plc_product_assignment_data.set_index("mat_cod"))
        .reset_index()
        .rename(
            columns={
                "mat_cod": "before",
                "mat_cod_reference": "after",
                "phaseout_end_date": "date",
            }
        )
        .drop_duplicates()
    )
    plc = (
        plc.groupby(
            [
                "before",
                "after",
                "date",
            ]
        )
        .first()
        .reset_index()
    )
    plc = plc[
        [
            "before",
            "after",
            "date",
        ]
    ].drop_duplicates()

    return plc
