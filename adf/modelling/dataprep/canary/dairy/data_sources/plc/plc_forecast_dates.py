from adf.modelling.tools import get_snowflake_df_spark  # type: ignore
import pyspark.sql.functions as sf  # type: ignore
import pyspark.sql.types as st  # type: ignore
from adf.config import SNOWFLAKE_DATABASE


def fetch():
    plc_forecast_dates = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_CNR", "R_IBP_EDP_CNR_PRO_LFC_FOR_DAT", "spark"
    )

    decimal_cols = [
        f.name for f in plc_forecast_dates.schema.fields if isinstance(f.dataType, st.DecimalType)
    ]

    for column in decimal_cols:
        plc_forecast_dates = plc_forecast_dates.withColumn(
            column, sf.col(column).cast(st.FloatType())
        )

    plc_forecast_dates = plc_forecast_dates.drop_duplicates().toPandas()

    plc_forecast_dates = plc_forecast_dates.loc[~plc_forecast_dates.phaseout_curve_name.isna()]
    plc_forecast_dates = (
        plc_forecast_dates.groupby(["mat_cod", "phaseout_end_date"]).first().reset_index()
    )
    plc_forecast_dates = plc_forecast_dates[["mat_cod", "phaseout_end_date"]].drop_duplicates()

    return plc_forecast_dates
