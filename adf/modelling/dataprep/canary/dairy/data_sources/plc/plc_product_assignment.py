from adf.modelling.tools import get_snowflake_df_spark  # type: ignore
import pyspark.sql.functions as sf  # type: ignore
import pyspark.sql.types as st  # type: ignore
from adf.config import SNOWFLAKE_DATABASE


def fetch():
    plc_product_assignment = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_CNR", "R_IBP_EDP_CNR_PRO_LFC_REF_PRO", "spark"
    )

    decimal_cols = [
        f.name
        for f in plc_product_assignment.schema.fields
        if isinstance(f.dataType, st.DecimalType)
    ]

    for column in decimal_cols:
        plc_product_assignment = plc_product_assignment.withColumn(
            column, sf.col(column).cast(st.FloatType())
        )

    plc_product_assignment = plc_product_assignment.drop_duplicates().toPandas()

    plc_product_assignment = (
        plc_product_assignment.groupby(
            [
                "mat_cod",
                "mat_cod_reference",
            ]
        )
        .first()
        .reset_index()
    )
    plc_product_assignment = plc_product_assignment[
        [
            "mat_cod",
            "mat_cod_reference",
        ]
    ].drop_duplicates()

    return plc_product_assignment
