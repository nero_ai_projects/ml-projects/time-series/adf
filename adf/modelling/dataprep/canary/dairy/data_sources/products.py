from adf.modelling.tools import get_snowflake_df_spark  # type: ignore
import pyspark.sql.functions as sf  # type: ignore
import pyspark.sql.types as st  # type: ignore
from adf.config import SNOWFLAKE_DATABASE


def fetch():
    products = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_CNR", "R_MAN_EDP_CNR_MAT_SAL", "spark"
    )

    decimal_cols = [
        f.name for f in products.schema.fields if isinstance(f.dataType, st.DecimalType)
    ]

    for column in decimal_cols:
        products = products.withColumn(column, sf.col(column).cast(st.FloatType()))

    products = products.drop_duplicates().toPandas()

    products = products.drop_duplicates()
    products["mat_volume"] = (
        products.mat_length_value * products.mat_width_value * products.mat_height_value / 1000
    )
    products.mat_weight = products.mat_weight * 1000
    products = (
        products.groupby(
            [
                "mat_cod",
            ]
        )
        .first()
        .reset_index()
    )
    products = products[
        [
            "mat_cod",
            "mat_length_value",
            "mat_width_value",
            "mat_height_value",
            "mat_volume",
            "mat_weight",
            "mat_type",
            "total_shelf_life",
            "units_number_per_mat",
            "mat_tax_type",
            "mat_active_flag",
            "sku_signature",
            "flavour_cod",
            "product_type_cod",
            "mat_umbrella_cod",
            "mat_prodbrand_cod",
            "mat_subfamily_cod",
        ]
    ].drop_duplicates()

    return products
