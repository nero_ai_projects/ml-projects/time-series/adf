import numpy as np  # type: ignore
import pandas as pd  # type: ignore

from . import promotions_header
from . import promotions_transactionnal


def fetch(sku_replacer=None):
    promotions_header_data = promotions_header.fetch()
    promotions_transactionnal_data = promotions_transactionnal.fetch(sku_replacer)

    promotions = pd.merge(
        left=promotions_transactionnal_data,
        right=promotions_header_data,
        how="left",
        on="promotion_cod",
    )
    promotions = promotions.drop_duplicates().dropna()
    promotions["promo_date_range"] = list(
        map(
            pd.date_range,
            promotions.promotion_start_date_sellin,
            promotions.promotion_end_date_sellin,
        )
    )
    promotions["promo_len"] = promotions.promo_date_range.apply(len)
    promotions = promotions[promotions["promo_len"] > 0]
    promotions = promotions.explode("promo_date_range")
    promotions = promotions.rename(columns={"promo_date_range": "promo_date"})
    promotions["promo_date"] = pd.DatetimeIndex(promotions["promo_date"])
    promotions["promotion_start_date_sellin"] = pd.DatetimeIndex(
        promotions["promotion_start_date_sellin"]
    )
    promotions["promo_elapsed_days"] = (
        promotions.promo_date - promotions.promotion_start_date_sellin
    ).dt.days
    promotions["promo_quantile"] = promotions.promo_elapsed_days % np.ceil(
        promotions.promo_len / promotions.promo_len.apply(_get_quantile)
    )
    promotions["promo_time_category"] = promotions.promo_len.apply(_get_time_category)
    promotions = (
        promotions.groupby(
            [
                "promotion_cod",
                "cus_superchain_cod",
                "mat_cod",
                "promotion_type_cod",
                "promotion_mechanics_cod",
                "promo_date",
                "promo_len",
                "promo_time_category",
                "promo_quantile",
                "promotion_start_date_sellin",
                "promotion_end_date_sellin",
                "promo_elapsed_days",
            ]
        )
        .first()
        .reset_index()
    )
    promotions = promotions[
        [
            "promotion_cod",
            "cus_superchain_cod",
            "mat_cod",
            "promotion_type_cod",
            "promotion_mechanics_cod",
            "promo_date",
            "promo_len",
            "promo_time_category",
            "promo_quantile",
            "promotion_start_date_sellin",
            "promotion_end_date_sellin",
            "promo_elapsed_days",
        ]
    ].drop_duplicates()

    return promotions


def _get_time_category(x):
    if x < 7:
        return 0
    elif x < 14:
        return 1
    elif x < 21:
        return 2
    elif x < 42:
        return 3
    elif x < 84:
        return 4
    else:
        return 5


def _get_quantile(x):
    if x < 7:
        return 7
    elif x < 14:
        return 7
    elif x < 21:
        return 3
    elif x < 42:
        return 6
    elif x < 84:
        return 4
    else:
        return 4
