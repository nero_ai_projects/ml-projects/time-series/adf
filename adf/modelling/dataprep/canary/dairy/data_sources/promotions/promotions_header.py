from adf.modelling.tools import get_snowflake_df_spark  # type: ignore
import pyspark.sql.functions as sf  # type: ignore
import pyspark.sql.types as st  # type: ignore
from adf.config import SNOWFLAKE_DATABASE


def fetch():
    promotions_header = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_CNR", "F_IBP_EDP_CNR_PMN_HDR", "spark"
    )

    decimal_cols = [
        f.name for f in promotions_header.schema.fields if isinstance(f.dataType, st.DecimalType)
    ]

    for column in decimal_cols:
        promotions_header = promotions_header.withColumn(
            column, sf.col(column).cast(st.FloatType())
        )

    promotions_header = promotions_header.drop_duplicates().toPandas()

    promotions_header = (
        promotions_header.groupby(
            [
                "promotion_cod",
                "promotion_type_cod",
                "promotion_mechanics_cod",
                "promotion_start_date_sellin",
                "promotion_end_date_sellin",
            ]
        )
        .first()
        .reset_index()
    )
    promotions_header = promotions_header[
        [
            "promotion_cod",
            "promotion_type_cod",
            "promotion_mechanics_cod",
            "promotion_start_date_sellin",
            "promotion_end_date_sellin",
        ]
    ].drop_duplicates()

    return promotions_header
