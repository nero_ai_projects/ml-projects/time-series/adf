from adf.modelling.tools import get_snowflake_df_spark  # type: ignore
import pyspark.sql.functions as sf  # type: ignore
import pyspark.sql.types as st  # type: ignore
from adf.config import SNOWFLAKE_DATABASE


def fetch(sku_replacer=None):
    promotions_transactional = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_CNR", "F_IBP_EDP_CNR_PMN_TRS", "spark"
    )

    decimal_cols = [
        f.name
        for f in promotions_transactional.schema.fields
        if isinstance(f.dataType, st.DecimalType)
    ]

    for column in decimal_cols:
        promotions_transactional = promotions_transactional.withColumn(
            column, sf.col(column).cast(st.FloatType())
        )

    promotions_transactional = promotions_transactional.drop_duplicates().toPandas()

    if sku_replacer:
        promotions_transactional = sku_replacer(promotions_transactional)
    promotions_transactional = (
        promotions_transactional.groupby(["promotion_cod", "cus_superchain_cod", "mat_cod"])
        .first()
        .reset_index()
    )
    promotions_transactional = promotions_transactional[
        ["promotion_cod", "cus_superchain_cod", "mat_cod"]
    ].drop_duplicates()

    return promotions_transactional
