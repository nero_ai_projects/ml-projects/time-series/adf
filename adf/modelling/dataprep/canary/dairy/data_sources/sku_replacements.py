import networkx as nx  # type: ignore
from adf.modelling.tools import get_snowflake_df_spark  # type: ignore
import pyspark.sql.functions as sf  # type: ignore
import pyspark.sql.types as st  # type: ignore
from adf.config import SNOWFLAKE_DATABASE


def fetch():
    sku_replacements = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_CNR", "R_MAN_EDP_CNR_MAT_OLD_NEW", "spark"
    )

    decimal_cols = [
        f.name for f in sku_replacements.schema.fields if isinstance(f.dataType, st.DecimalType)
    ]

    for column in decimal_cols:
        sku_replacements = sku_replacements.withColumn(column, sf.col(column).cast(st.FloatType()))

    sku_replacements = sku_replacements.drop_duplicates().toPandas()

    sku_replacements = sku_replacements.drop_duplicates()
    sku_replacements = sku_replacements[~sku_replacements.mat_cod_before.isnull()]
    sku_replacements = sku_replacements[~sku_replacements.mat_cod_after.isnull()]

    G = nx.from_pandas_edgelist(
        sku_replacements, "mat_cod_before", "mat_cod_after", create_using=nx.DiGraph
    )
    tree = nx.tree.greedy_branching(G)

    def find_terminal_node(node, tree=tree):
        while node:
            terminal_node = node
            node = next(iter(tree[node].keys()), None)
        else:
            return terminal_node

    sku_replacements["mat_cod_after"] = sku_replacements.mat_cod_before.apply(find_terminal_node)
    sku_replacements = sku_replacements[
        sku_replacements.mat_cod_after != sku_replacements.mat_cod_before
    ]
    sku_replacements = (
        sku_replacements.groupby(
            [
                "mat_cod_before",
                "mat_cod_after",
            ]
        )
        .first()
        .reset_index()
    )
    sku_replacements = sku_replacements[
        [
            "mat_cod_before",
            "mat_cod_after",
        ]
    ].drop_duplicates()

    return sku_replacements
