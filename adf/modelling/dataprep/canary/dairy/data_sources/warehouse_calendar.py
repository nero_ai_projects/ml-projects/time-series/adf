from adf.modelling.tools import get_snowflake_df_spark  # type: ignore
import pyspark.sql.functions as sf  # type: ignore
import pyspark.sql.types as st  # type: ignore
from adf.config import SNOWFLAKE_DATABASE


def fetch():
    warehouse_calendar = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_CNR", "R_MAN_EDP_CNR_CAL_WRH", "spark"
    )

    decimal_cols = [
        f.name for f in warehouse_calendar.schema.fields if isinstance(f.dataType, st.DecimalType)
    ]

    for column in decimal_cols:
        warehouse_calendar = warehouse_calendar.withColumn(
            column, sf.col(column).cast(st.FloatType())
        )

    warehouse_calendar = warehouse_calendar.drop_duplicates().toPandas()

    warehouse_calendar = warehouse_calendar.assign(
        mat_ava_dat=warehouse_calendar.to_dt,
        plt_cod=warehouse_calendar.warehouse_cod,
        sales_org_cod=warehouse_calendar.sales_organization_cod,
    )
    warehouse_calendar = warehouse_calendar[
        [
            "sales_org_cod",
            "plt_cod",
            "mat_ava_dat",
            "warehouse_opening",
        ]
    ].drop_duplicates()

    return warehouse_calendar
