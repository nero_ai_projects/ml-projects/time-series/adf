import logging
import numpy as np  # type: ignore
import pandas as pd  # type: ignore
import adf.modelling.dataprep.canary.dairy.data_sources as data_sources
from datetime import timedelta
from adf.modelling import mllogs
from adf.errors import DataprepError
from adf.modelling.dataprep.utils.core import build_dataprep_report
from adf.modelling.dataprep.utils.holidays import (
    add_holidays_count_in_week,
    add_weekday_of_holidays,
    get_holidays_df,
)
from adf.services.database.query.reports import update_report
from adf.modelling.dataprep.canary.dairy.promo import (
    compute_all_promo_means,
    compute_all_promo_uplifts,
)
from adf.modelling.dataprep.canary.dairy.utils import (
    SkuReplacer,  # type: ignore
    MixedTraysReplacer,  # type: ignore
)


log = logging.getLogger("adf")


def run(country: str, division: str, product_level: str, location_level: str, **context):
    """Build panel data for Spain dairy given product_level and location_level
    Done for core and inno model both

    :product_level: sku
    :location_level: warehouse
    """
    assert product_level in ("sku",)
    assert location_level in ("warehouse",)

    session = context["session"]
    pipeline_id = context.get("pipeline_id", None)

    report = build_dataprep_report(
        session,
        country,
        division,
        product_level,
        location_level,
        scope="panel",
        split_type="",
        split_code=None,
        prep_type="core",
        pipeline_id=pipeline_id,
    )
    mllogs.log_dataprep_report(report)

    try:
        log.info("Building panel")
        panel = build_panel_data()
        mllogs.log_shape(panel)

        log.info("Saving")
        report.upload(panel)
        update_report(session, report, "success")
        mllogs.tag_report_output(report)

        return report, None

    except Exception as err:
        message = str(err.__class__.__name__) + ": " + str(err)
        update_report(session, report, "failed", message)
        raise DataprepError(message) from err

    finally:
        mllogs.tag_report_output(report)


def build_panel_data() -> pd.DataFrame:
    log.info("Fetching data sources")
    products = data_sources.products.fetch()
    log.info({"products.shape": products.shape})

    customers = data_sources.customers.fetch()
    log.info({"customers.shape": customers.shape})

    ibp_master_data = data_sources.ibp_master_data.fetch()
    log.info({"ibp_master_data.shape": ibp_master_data.shape})

    sku_replacements = data_sources.sku_replacements.fetch()
    log.info({"sku_replacements.shape": sku_replacements.shape})

    warehouse_calendar = data_sources.warehouse_calendar.fetch()
    sku_replacer = SkuReplacer(sku_replacements)
    mixed_trays = data_sources.mixed_trays.fetch()
    log.info({"mixed_trays.shape": mixed_trays.shape})

    mixed_trays_replacer = MixedTraysReplacer(mixed_trays)
    orders = data_sources.orders.fetch(sku_replacer, mixed_trays_replacer)
    log.info({"orders.shape": orders.shape})

    brokers_orders = data_sources.brokers_orders.fetch(sku_replacer, mixed_trays_replacer)
    log.info({"brokers_orders.shape": brokers_orders.shape})

    promotions = data_sources.promotions.fetch(sku_replacer)
    log.info({"promotions.shape": promotions.shape})

    log.info("Filtering products with IBP master data")
    products = pd.merge(left=products, right=ibp_master_data, how="inner", on="mat_cod")
    log.info({"products.shape": products.shape})

    log.info("Filtering orders with IBP master data")
    orders = pd.merge(left=orders, right=ibp_master_data, how="inner", on="mat_cod")
    log.info({"orders.shape": orders.shape})

    log.info("Filtering brokers orders with IBP master data")
    brokers_orders = pd.merge(left=brokers_orders, right=ibp_master_data, how="inner", on="mat_cod")
    log.info({"brokers_orders.shape": brokers_orders.shape})

    log.info("Computing unique brokers")
    brokers = brokers_orders.plt_cod.unique()

    log.info("Initializing panel with orders")
    panel = orders
    log.info({"panel.shape": panel.shape})

    log.info("Removing brokers orders from general orders")
    # as they only represent 5% of total volume and break customer granularity
    panel = panel[~panel.cus_cod.isin(brokers)]
    log.info({"panel.shape": panel.shape})

    log.info("Adding customer data to panel")
    panel = pd.merge(left=panel, right=customers, how="left", on="cus_cod")
    log.info({"panel.shape": panel.shape})

    log.info("Dispatching brokers orders to customers")
    panel = apply_brokers_orders(panel, orders, brokers_orders, customers)
    log.info({"panel.shape": panel.shape})

    log.info("Aggregating at customer lv2 granularity")
    panel = panel.groupby(
        [
            "sales_org_cod",
            "plt_cod",
            "mat_ava_dat",
            "cus_superchain_cod",
            "mat_cod",
        ]
    ).agg(ord_sku_qty=("ord_sku_qty", sum))
    panel = panel.reset_index()
    log.info({"panel.shape": panel.shape})

    log.info("Adding future rows to forecast")
    today = pd.Timestamp(pd.Timestamp.today().date())
    future_rows = (
        panel.reset_index()[["sales_org_cod", "plt_cod", "cus_superchain_cod", "mat_cod"]]
        .drop_duplicates()
        .pipe(
            lambda df: df.assign(
                mat_ava_dat=[pd.date_range(today, today + timedelta(days=7 * 15))] * len(df)
            )
        )
        .explode("mat_ava_dat")
    )
    panel = panel[panel.mat_ava_dat < today]
    panel = panel.append(future_rows)
    log.info({"panel.shape": panel.shape})

    log.info("Merging panel with product data")
    panel = pd.merge(left=panel, right=products, how="inner", on="mat_cod")
    log.info({"panel.shape": panel.shape})

    log.info("Replacing NaN flavour_cod")
    panel["flavour_cod"] = panel["flavour_cod"].replace([np.NaN], -1)
    log.info({"panel.shape": panel.shape})

    log.info("Replacing NaN mat_subfamily_cod")
    panel["mat_subfamily_cod"] = panel["mat_subfamily_cod"].replace([np.NaN], -1)
    log.info({"panel.shape": panel.shape})

    log.info("Computing promotions means")
    promotions_means = compute_all_promo_means(promotions, panel)
    log.info({"promotions_means.shape": promotions_means.shape})

    log.info("Computing promotions uplifts")
    promotions_uplifts = compute_all_promo_uplifts(promotions, panel)
    log.info({"promotions_uplifts.shape": promotions_uplifts.shape})

    promotions_means["mat_ava_dat"] = pd.to_datetime(promotions_means["mat_ava_dat"])
    panel["mat_ava_dat"] = pd.to_datetime(panel["mat_ava_dat"])
    log.info("Merging promotions means into panel")
    panel = pd.merge(
        left=panel,
        right=promotions_means,
        how="left",
        on=["mat_cod", "mat_ava_dat", "cus_superchain_cod"],
    )
    log.info({"panel.shape": panel.shape})

    log.info("Merging promotions uplifts into panel")
    panel = pd.merge(
        left=panel,
        right=promotions_uplifts,
        how="left",
        on=["mat_cod", "mat_ava_dat", "cus_superchain_cod"],
    )
    log.info({"panel.shape": panel.shape})

    log.info("Enriching panel data with holidays")
    holidays_df = get_holidays_df("spain_CN", "dayoff")
    holidays_df = add_weekday_of_holidays(holidays_df, "date")
    holidays_df = add_holidays_count_in_week(holidays_df, "date")
    holidays_df = holidays_df.rename({"date": "mat_ava_dat"}, axis=1)
    panel = pd.merge(
        left=panel,
        right=holidays_df,
        on="mat_ava_dat",
        how="left",
    )
    log.info({"panel.shape": panel.shape})

    log.info("Merging warehouse calendar into panel")
    warehouse_calendar["mat_ava_dat"] = pd.to_datetime(warehouse_calendar["mat_ava_dat"])
    panel = pd.merge(
        left=panel,
        right=warehouse_calendar,
        how="left",
        on=[
            "sales_org_cod",
            "plt_cod",
            "mat_ava_dat",
        ],
    )
    log.info({"panel.shape": panel.shape})

    log.info("Adding covid feature to panel")
    is_covid = (panel["mat_ava_dat"] >= pd.Timestamp("2020-03-02")) & (
        panel["mat_ava_dat"] < pd.Timestamp("2020-05-04")
    )
    panel.loc[is_covid, "is_covid"] = 1
    panel.loc[~is_covid, "is_covid"] = 0
    log.info({"panel.shape": panel.shape})

    return panel


def apply_brokers_orders(panel, orders, brokers_orders, customers):
    orders_to_brokers = brokers_orders
    orders_to_brokers = orders_to_brokers.merge(
        customers,
        how="left",
        on="cus_cod",
    )

    orders_to_brokers_volumes = orders_to_brokers.groupby(
        [
            "plt_cod",
            "cus_superchain_cod",
            "mat_cod",
        ]
    ).agg(ord_sku_qty=("ord_sku_qty", "sum"))
    orders_to_brokers_volumes = orders_to_brokers_volumes.reset_index().merge(
        orders_to_brokers_volumes.groupby(
            [
                "plt_cod",
                "mat_cod",
            ]
        ).agg(ord_sku_qty=("ord_sku_qty", "sum")),
        how="left",
        on=[
            "plt_cod",
            "mat_cod",
        ],
    )

    brokers_ratios = orders_to_brokers_volumes.assign(
        ratio=(orders_to_brokers_volumes.ord_sku_qty_x / orders_to_brokers_volumes.ord_sku_qty_y)
    )[["plt_cod", "cus_superchain_cod", "mat_cod", "ratio"]]

    orders_from_brokers = (
        orders_to_brokers[["plt_cod"]]
        .drop_duplicates()
        .merge(
            orders,
            how="left",
            left_on="plt_cod",
            right_on="cus_cod",
        )
        .drop(columns=["plt_cod_x", "plt_cod_y"])
    )
    orders_from_brokers = orders_from_brokers.merge(
        brokers_ratios,
        how="inner",
        left_on=["cus_cod", "mat_cod"],
        right_on=["plt_cod", "mat_cod"],
    )
    orders_from_brokers["ord_sku_qty_scaled"] = np.floor(
        orders_from_brokers["ord_sku_qty"] * orders_from_brokers["ratio"]
    )
    orders_from_brokers = orders_from_brokers.query("ord_sku_qty_scaled > 0")

    missing_orders = orders_from_brokers.groupby(["mat_ava_dat", "plt_cod", "mat_cod"]).agg(
        ord_sku_qty=("ord_sku_qty", "first"),
        ord_sku_qty_scaled=("ord_sku_qty_scaled", "sum"),
    )
    missing_orders["missing"] = missing_orders.ord_sku_qty - missing_orders.ord_sku_qty_scaled
    missing_orders = missing_orders.query("missing > 0")

    orders_from_brokers["ord_sku_qty"] = orders_from_brokers["ord_sku_qty_scaled"]

    top_orderers = (
        orders_from_brokers.groupby(
            [
                "mat_ava_dat",
                "plt_cod",
                "mat_cod",
                "cus_superchain_cod",
            ]
        )
        .agg(ord_sku_qty=("ord_sku_qty", "sum"))
        .reset_index()
    )
    top_orderers = top_orderers.merge(
        top_orderers.groupby(
            [
                "mat_ava_dat",
                "plt_cod",
                "mat_cod",
            ]
        ).agg(ord_sku_qty=("ord_sku_qty", "max")),
        how="right",
        on=["mat_ava_dat", "plt_cod", "mat_cod", "ord_sku_qty"],
    )
    top_orderers = top_orderers[
        [
            "mat_ava_dat",
            "plt_cod",
            "mat_cod",
        ]
    ].drop_duplicates()

    missing_orders = missing_orders.merge(
        top_orderers, how="left", on=["mat_ava_dat", "plt_cod", "mat_cod"]
    )
    missing_orders["ord_sku_qty"] = missing_orders["missing"]

    orders_from_brokers["plt_cod"] = 164
    missing_orders["plt_cod"] = 164

    panel = pd.concat(
        [
            panel,
            orders_from_brokers.drop(
                columns=[
                    "ord_sku_qty_scaled",
                    "ratio",
                ]
            ),
            missing_orders.drop(
                columns=[
                    "ord_sku_qty_scaled",
                    "missing",
                ]
            ),
        ]
    )
    return panel
