from datetime import timedelta
import numpy as np  # type: ignore
import pandas as pd  # type: ignore
import adf.modelling.dataprep.canary.dairy.building_blocks as building_blocks


HORIZONS = [0, 7, 14, 21, 28, 35, 42, 49, 56, 63, 70, 77, 84, 91, 98]


def compute_all_promo_means(promotions, panel):
    promotions = pd.merge(
        left=promotions,
        right=panel.groupby(["cus_superchain_cod", "mat_cod", "mat_ava_dat"])
        .ord_sku_qty.sum()
        .reset_index(),
        how="left",
        left_on=["cus_superchain_cod", "mat_cod", "promo_date"],
        right_on=["cus_superchain_cod", "mat_cod", "mat_ava_dat"],
    )

    promotions["mat_cod"] = promotions["mat_cod"].astype("int64")
    for horizon in HORIZONS:
        promotions = compute_promo_means(promotions, horizon)

    promotions = promotions[
        ["mat_cod", "mat_ava_dat", "cus_superchain_cod"] +
        [f"promo_mean_horizon_{i}" for i in HORIZONS]
    ]
    promotions = (
        promotions.groupby(["mat_cod", "mat_ava_dat", "cus_superchain_cod"])[
            [f"promo_mean_horizon_{i}" for i in HORIZONS]
        ]
        .max()
        .reset_index()
    )

    return promotions


AGG_LEVELS = [
    [
        "mat_cod",
        "cus_superchain_cod",
        "promotion_mechanics_cod",
        "promo_time_category",
        "promo_quantile",
    ],
    ["mat_cod", "cus_superchain_cod", "promotion_mechanics_cod", "promo_time_category"],
    ["mat_cod", "cus_superchain_cod", "promo_time_category"],
    ["mat_cod", "cus_superchain_cod", "promotion_mechanics_cod"],
    ["mat_cod", "promotion_mechanics_cod", "promo_time_category", "promo_quantile"],
    ["mat_cod", "promotion_mechanics_cod", "promo_time_category"],
    ["mat_cod", "promo_time_category"],
    ["mat_cod", "promotion_mechanics_cod"],
    ["mat_cod"],
]


def compute_promo_means(promotions, horizon):
    promo_mean_column = f"promo_mean_horizon_{horizon}"
    columns_to_keep = promotions.columns.to_list() + [promo_mean_column]
    promotions[promo_mean_column] = np.nan
    promotions["horizon_promo_date"] = promotions.promotion_start_date_sellin - timedelta(
        days=horizon
    )
    for agg_level, agg_level_columns in enumerate(AGG_LEVELS):
        if promotions[promo_mean_column].isna().sum() == 0:
            break
        promos_agg_at_lvl = promotions.groupby(["promotion_cod"] + agg_level_columns)
        promos_agg_at_lvl = promos_agg_at_lvl.agg(
            {"ord_sku_qty": "mean", "promotion_start_date_sellin": "min"}
        )
        promos_agg_at_lvl = promos_agg_at_lvl.reset_index()
        promos_agg_at_lvl = promos_agg_at_lvl.dropna(subset=["ord_sku_qty"])

        promos_agg_at_lvl["ord_sku_qty"] = promos_agg_at_lvl.groupby(
            agg_level_columns
        ).ord_sku_qty.transform(lambda x: x.rolling(5, 1).mean())

        promotions = pd.merge_asof(
            promotions.sort_values("horizon_promo_date"),
            promos_agg_at_lvl.sort_values("promotion_start_date_sellin"),
            by=agg_level_columns,
            left_on="horizon_promo_date",
            right_on="promotion_start_date_sellin",
            direction="backward",
            suffixes=(None, f"_{agg_level}"),
            allow_exact_matches=False,
        )
        promotions[promo_mean_column] = promotions[promo_mean_column].fillna(
            promotions[f"ord_sku_qty_{agg_level}"]
        )
    return promotions[columns_to_keep]


def compute_all_promo_uplifts(promotions, panel):
    promo_dates = promotions.promo_date.copy()
    promotions = pd.merge(
        left=panel.groupby(["cus_superchain_cod", "mat_cod", "mat_ava_dat"])
        .ord_sku_qty.sum()
        .reset_index(),
        right=promotions,
        how="left",
        right_on=["cus_superchain_cod", "mat_cod", "promo_date"],
        left_on=["cus_superchain_cod", "mat_cod", "mat_ava_dat"],
    )
    promotions["promotion_mechanics_cod"] = promotions["promotion_mechanics_cod"].fillna("0")

    bb = building_blocks.build_bb(promotions)

    promotions = pd.merge(
        left=promotions, right=bb, on=["mat_cod", "cus_superchain_cod"], how="left"
    )
    promotions["total_uplifts"] = promotions.filter(regex="uplift").sum(axis=1)
    promotions.loc[promotions.promo_date.isnull(), "total_uplifts"] = np.NaN
    promotions = (
        promotions.groupby(["mat_cod", "mat_ava_dat", "cus_superchain_cod"])
        .agg({"total_uplifts": "sum"})
        .reset_index()
    )

    promotions = promotions[promotions.mat_ava_dat.isin(promo_dates)]

    return promotions
