import pandas as pd  # type: ignore


class SkuReplacer:
    def __init__(self, sku_replacements: pd.DataFrame) -> None:
        self.sku_replacements = sku_replacements

    def __call__(self, df: pd.DataFrame):
        df_old = pd.merge(
            left=df,
            right=self.sku_replacements,
            how="inner",
            left_on="mat_cod",
            right_on="mat_cod_before",
        )
        df_old["mat_cod"] = df_old["mat_cod_after"]
        df_old = df_old[df.columns]
        df_new = pd.merge(
            left=df,
            right=self.sku_replacements,
            how="left",
            left_on="mat_cod",
            right_on="mat_cod_before",
        )
        df_new = df_new[df_new.mat_cod_before.isnull()]
        df_new = df_new[df.columns]
        df = pd.concat([df_old, df_new])
        return df


class MixedTraysReplacer:
    def __init__(self, mixed_trays: pd.DataFrame) -> None:
        self.mixed_trays = mixed_trays

    def __call__(self, df: pd.DataFrame):
        mixed_trays_df = df[df.mat_cod.isin(self.mixed_trays.material)]
        mixed_trays_df = mixed_trays_df.merge(
            self.mixed_trays, how="left", left_on="mat_cod", right_on="material"
        )
        mixed_trays_df = mixed_trays_df.assign(
            mat_cod=mixed_trays_df.comp.astype("float64"),
            ord_sku_qty=(mixed_trays_df.ord_sku_qty * mixed_trays_df.cant).astype("float64"),
        )
        mixed_trays_df = mixed_trays_df[df.columns]

        normal_df = df[~df.mat_cod.isin(self.mixed_trays.material)]

        df = pd.concat([mixed_trays_df, normal_df])
        return df
