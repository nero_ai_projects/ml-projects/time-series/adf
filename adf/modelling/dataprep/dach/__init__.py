"""

-----------
**Context**
-----------

The goal here is to deliver:

The goal here is to deliver short-term and mid-term forecasts on a 14 weeks horizon
and send it to the IBP environment.

Results are sent to the IBP environment to be shared to dach dairy Demand Planners.

Forecast granularities:

* predict volumes per ``SKU / NFA / Week``

NB : forecast is made on a weekly basis and must be disaggregated
to the daily level before being sent to IBP (98 days total).

----------------
**Data Sources**
----------------
Data sources for dairy Germany

.. figure:: ./../_static/data_sources_germany.png
  :width: 500
  :align: center

Data sources for dairy CHAT

.. figure:: ./../_static/data_sources_chat.png
  :width: 500
  :align: center

NB: There are 3 sales organization code in Dach Dairy: 0006 for Germany,
0013 for Austria and 0071 for switzerland. Although we generate
3 forecasts reports, the data sources are common. We will specify it when the
differentiating is needed.

-----------------
**Mapping Files**
-----------------

*in_out_promo_nfa_mapping, 1 file per country (per customer for DE and at national
 level for CH and AT)
*region_code_mapping (only)
*sku_replacement_mapping
*school_holidays_at_mapping

NB: These mapping files have been provided by demand planners
and are update at low frequency if needed.

------------------------------
**Data Preparation Pipelines**
------------------------------

Data preparation overview

.. figure:: ./../_static/data_preparation_dach.png
  :width: 500
  :align: center

Please refer to the `business documentation <https://danone-my.sharepoint.com/:p:/r/personal/
anne-laure_cebile_danone_com/_layouts/15/
Doc.aspx?sourcedoc=%7B9AC967FD-825C-46B1-9002-2D1ABEC039F8%7D&file=%5BADF%5D%20DACH%20EDP%20-%20Business%20Documentation.pptx&action=edit&mobileredirect=true>`_
for more details.

------------------------------------
**Promotion Preparation Pipeline**
------------------------------------

1. Promotion headers & history
------------------------------

* Gather promotion headers and promotion headers' history
* Remove duplicates with key [nfa_cod, mat_cod, promotion_start_date_store,
  promotion_end_date_store, status_code]
* Apply sku replacements
* Merge the two tables

2. Promotion transactions & history
-----------------------------------
* Gather the promotion transactions and transactions' history
* Clean the promotion
    - Drop duplicates using key ['mat_cod', 'promotion_cod', 'monday_date']
    - Aggregate weekly KPIs using max for uplift and min for downlift
    - Apply sku replacements
* Merge the two tables

*First preparations on promotions*
==================================

After the previous preparations, we get two tables ``promo_df`` containing
the promotion headers and ``promo_volume_df`` containing the promotion transactions
coming from BluePlanner and containing the predicted downlift expected after a
promotion (a promotion dip), the weekly forecasted uplift as well as the weekly
cannibalization downlift.

The first step concerns the promotion file for Germany only and not Austria or
Switzerland.
It consists in shifting the promotion start and end dates per NFA for the
promo_df table.

1. Promotion means
------------------

This step is performed using the ``promo_df`` and ``orders_df`` table.
The objective is that for a given day, taking into account the customers
(NFAs) in promotion and the type of promotion, to retroactively calculate
the rolling average of the orders carried out at different horizons
(last 7 occurrences, last 14, etc.).

Before computing the promotion means, many steps are performed:

Product processing for each product in the orders data set:
    * Clean promotions
    * Shift promotion dates
    * Expand promos to daily (ex: if the promotion lasts 4 days,
      we will have 4 lines)
    * Remove all promotions on sunday
    * For each product, date (requested delivery date) and NFA:

        - Remove promotion mechanic code duplicates (keep the first one).
        - Sum KPIs in the orders data
        - Add these KPIs to the product promotion table (merge promotions
          with the orders aggregated in the step before)
        - Compute quantiles and horizons: horizons represent the last 7,
          14, 21, etc. occurrences of the promotion
        - Compute the promotion rolling means per horizon
        - Select the maximum rolling mean for each product, date and NFA

2. Promotion uplifts
--------------------

This step uses the ``promo_df`` and ``orders_df`` tables and is performed
in two steps:

    a- Building the promotion panel

* Aggregate the orders' volumes per product, customer (NFA) and date
* Process promotion data just like before per product x NFA x date

    - Clean table
    - Expand promotions to a daily view
    - Remove all promotions on sunday
    - Remove promotion mechanic code duplicates (keep the first one).
    - Sum KPIs in the orders data per product, customer (NFA) and date.

* Perform a left merge between the product orders data and
  the cleaned promotions' table.
* All lines in the merged table not containing a promotion will have a
  mechanic code of 0.

At the end of this first step, we get two tables, one is the panel which is the
result of the merge above (with mechanic_cod = 0 when there is no promotion)
and a table with promotions only.

    b- Creating the building blocks

To create the building blocks, we use the panel generated in the step before.

* We take a period of 2 years from the dataset, starting from the current date,
  backward
* For each product and each location, we compute the volumes associated with
  each promotion type (mechanic code) then we aggregate these volumes across all
  promotions (sum per product, NFA and date).
* We regularize volumes if there are sales for non-promo and promo at the
  same time.
* We create new columns with the mean of volume quantities sold for each
  mechanic code including O which is the no promotion.
  This mean will the same for each product and NFA for any date as it is
  computed over the two year period.
* We compute the uplifts for each promotion type (excluding 0):
  ``uplift = (promo_period_mean / no_promo_period_mean) - 1``
  the period here is two years. Uplift created here is for one product and one
  NFA.

Then, we have an uplift on base 100, and which is clipped to 0 if
the value is below 0.

3. Features from promotion transactions
---------------------------------------

In this last step, we will create some features using the promotion transactions'
table from BluePlanner.

* First we use the ``promo_df`` table to get the NFAs and promotion codes
* We then merge this table with the promotion transactions in order to add
  the NFA information
* We aggregate the predicted uplift and downlift for cannibalization and promotion
  dip per product, NFA and date. We use the sum for the uplift and the minimum for
  the downlift.

-----------------
**Preprocessing**
-----------------

Preprocessing config files for dairy AT, CH and DE are in the config/flows/dach/ folder.

-------------------------
**Training and Forecast**
-------------------------

Training config files for dairy AT, CH and DE are in the config/flows/dach/ folder.

Training is done using filters on customers:
* Dach DE:
  * NFAs of the group Edeka
  * NFAs of the group Rewe
  * NFAs that make pre-orders
  * Others

* Dach AT
  * Discounter
  * Non discounter

* Dach CH (no filters is applied because we don't have enough data per group)

----------------------------------
** IBP export and Business rules**
----------------------------------

We generate 1 forecast file per country.

The following business rules are applied:
* remove SKUs that are in stop algo from the ibp master data
* compute building blocks for promotions
* remove old products
* apply :
  * remove dead NFAs
  * force predictions to 0 on weekends and bank holidays
  * force prediction to 0 outside promo for IO (in and out) products
  * cap negative predictions to 0
* update warehouse codes
"""


def get_country_name(division: str) -> str:
    dach_country_mapping = {
        "dairy_de": "germany",
        "dairy_ch": "switzerland",
        "dairy_at": "austria"
    }
    return dach_country_mapping[division]


def get_country_name_promo(division: str) -> str:
    dach_country_mapping = {
        "dairy_de": "GE",
        "dairy_ch": "CH",
        "dairy_at": "AT"
    }
    return dach_country_mapping[division]


def get_sales_organization_cod(division: str) -> str:
    dach_country_mapping = {
        "dairy_de": '0006',
        "dairy_ch": '0071',
        "dairy_at": '0013'
    }
    return dach_country_mapping[division]


def get_iso_cod_calendar():
    return {
        "BW": "Baden-Wurttemberg",
        "BY": "Bavaria",
        "BE": "Berlin",
        "BB": "Brandenburg",
        "HB": "Bremen",
        "HH": "Hamburg",
        "HE": "Hesse",
        "NI": "Lower Saxony",
        "MV": "Mecklenburg-Vorpomm.",
        "NW": "Nrth Rhine Westfalia",
        "RP": "Rhineland Palatinate",
        "SL": "Saarland",
        "SN": "Saxony",
        "ST": "Saxony-Anhalt",
        "SH": "Schleswig-Holstein",
        "TH": "Thuringia"
    }


def get_shift_mapping() -> dict:
    return {
        10001: [1, 0],
        10005: [0, 0],
        10009: [1, 0],
        10012: [-2, 0],
        10199: [-1, 0],
        10202: [-2, 0],
        10203: [0, -6],
        10204: [-1, -1],
        10205: [0, 0],
        10211: [-2, 0],
        10212: [-1, -1],
        10213: [0, 0],
        10214: [0, 0],
        10216: [1, -1],
        10218: [2, -1],
        10219: [0, -1],
        10226: [0, 0],
        10228: [0, 0],
        10231: [-2, 0],
        10234: [0, -1],
        10235: [0, -1],
        10239: [0, 0],
        10241: [0, 0],
        12456: [0, 0],
        12529: [1, 0]
    }
