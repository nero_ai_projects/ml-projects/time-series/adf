import traceback
import logging
import pandas as pd  # type: ignore
import numpy as np  # type: ignore
from adf.errors import BuildingBlocksError
from adf.utils.decorators import timeit
from datetime import date
from dateutil.relativedelta import relativedelta

from adf.modelling.dataprep.utils.promos import add_period_means

log = logging.getLogger("adf")


@timeit
def build_bb(promo_df):
    try:

        promos_type_cols = list(set(
            promo_df['promotion_mechanics_cod'].astype('int64').to_numpy()
        ))
        promos_uplifts_cols = [
            f"{promo_cod}_uplift" for promo_cod in promos_type_cols if promo_cod != 0
        ]

        # promo_df = promo_df.pipe(scope_promo, '2017-01-01', '2019-03-01').compute()
        promo_df = promo_df.pipe(scope_promo)
        promo_df = cast_and_sort_promo(promo_df)
        promo_df = promo_df.reset_index(drop=True)

        uplift_df = promo_df.groupby(
            ["mat_cod", "nfa_cod"], as_index=False
        ).apply(add_uplift(promos_uplifts_cols, promos_type_cols))

        uplift_df = uplift_df.dropna(subset=promos_uplifts_cols, how='all')
        float_cols = list(uplift_df.select_dtypes(include=['float64', 'float32']).columns)
        int_cols = list(uplift_df.select_dtypes(include=['int']).columns)
        uplift_df[float_cols] = uplift_df[float_cols].fillna(0.0)
        uplift_df[int_cols] = uplift_df[int_cols].fillna(0)
        uplift_df = aggregate_uplifts(uplift_df, promos_uplifts_cols)

        return uplift_df.to_pandas()

    except Exception as err:
        traceback.print_exc()
        message = str(err.__class__.__name__) + ": " + str(err)
        raise BuildingBlocksError(message)


def cast_and_sort_promo(promo_df):
    promo_df = promo_df[~promo_df['nfa_cod'].isnull()]
    promo_df = promo_df.astype(
        {
            "mat_cod": 'int64',
            "nfa_cod": 'int64',
            "ordered_units": 'int64'
        })
    return promo_df.sort_values(by=["to_dt", "mat_cod", "nfa_cod"])


def scope_promo(promo_df):
    """Filter promo dataframe on observation dataset

    Parameters
    ----------
    promo_df : pd.DataFrame
        input promo dataframe at a daily level

    Returns
    -------
    pd.DataFrame
        scoped promotion dataframe
    """
    today = date.today()
    start_date = (today + relativedelta(months=-24)).strftime("%Y-%m-%d")
    end_date = today.strftime("%Y-%m-%d")

    promo_df = promo_df.loc[promo_df.to_dt.between(start_date, end_date)]
    return promo_df


def add_uplift(promos_uplifts_cols, promos_type_cols):
    def add_uplift_per_location(
        promo_df
    ):
        return promo_df \
            .pipe(create_uplift_table, promos_uplifts_cols, promos_type_cols) \
            .pipe(regularize_volumes, promos_type_cols) \
            .pipe(add_period_means, 'mat_cod', 'nfa_cod', promos_type_cols) \
            .pipe(compute_uplifts, promos_uplifts_cols)
    return add_uplift_per_location


def create_uplift_table(df_location: str, promos_uplifts_cols, promos_type_cols) -> pd.DataFrame:
    """Build columns containing volumes associated to each promo types
    Parameters
    ----------
    df_location: pd.DataFrame
        promo dataframe for one location
    promos_uplifts_cols: List[str]
    promos_type_cols: List[str]
    Returns
    -------
    pd.DataFrame
        uplift dataframe containing volumes associated to each promo types
    """
    uplift_df = pd.pivot_table(
        df_location,
        values="ordered_units",
        index=["nfa_cod", "to_dt", "mat_cod"],
        columns=["promotion_mechanics_cod"],
        aggfunc=np.sum
    ).reset_index()
    uplift_df = uplift_df.fillna(0)
    for col in promos_uplifts_cols:
        if col not in uplift_df.columns:
            uplift_df[col] = 0
    reindex_cols = ["nfa_cod", "to_dt", "mat_cod"] + promos_type_cols
    uplift_df = uplift_df.reindex(columns=reindex_cols)

    uplift_df = uplift_df.groupby(
        ["nfa_cod", "to_dt", "mat_cod"]
    ).agg({el: "sum" for el in promos_type_cols}).reset_index()
    return uplift_df


def regularize_volumes(uplift_df: pd.DataFrame, promos_type_cols) -> pd.DataFrame:
    """
    Regularizes volumes if there are sales for non-promo and promo the same time
    """
    reg_cols = promos_type_cols.copy()
    reg_cols.remove(0)
    for col in promos_type_cols:
        uplift_df.loc[(uplift_df[col] > uplift_df[0]), col] = \
            uplift_df[0] + uplift_df[col]
        uplift_df.loc[(uplift_df[col] > uplift_df[0]), 0] = 0

        uplift_df.loc[(uplift_df[0] > uplift_df[col]), 0] = \
            uplift_df[0] + uplift_df[col]
        uplift_df.loc[(uplift_df[0] > uplift_df[col]), col] = 0

    return uplift_df


def compute_uplifts(uplift_df: pd.DataFrame, promos_uplifts_cols) -> pd.DataFrame:
    """Calculate increment or decrement of promo effect due compared to a baseline

    Parameters
    ----------
    uplift_df: pd.DataFrame
        promotion dataframe with enriched columns
    promos_uplifts_cols: List[str]
    Returns
    -------
    pd.DataFrame
        promotion dataframe containing uplifts cols
    """
    for col in promos_uplifts_cols:
        period_col = col.split("_", 1)[0] + "_period_mean"
        uplift_df[col] = uplift_df[period_col] / uplift_df["0_period_mean"]

    uplift_df = uplift_df.sort_values(by=["to_dt", "mat_cod"])
    uplift_df[promos_uplifts_cols] = uplift_df[promos_uplifts_cols] - 1
    uplift_df[promos_uplifts_cols] = uplift_df[promos_uplifts_cols].clip(0)
    return uplift_df


def aggregate_uplifts(uplift_df: pd.DataFrame, promos_uplifts_cols) -> pd.DataFrame:
    """ Aggregate uplift per 'mat_cod', 'nfa_cod', calculating the mean of each promo type
    """
    agg = {col: "mean" for col in promos_uplifts_cols}
    uplift_df = uplift_df.groupby(
        ["mat_cod", "nfa_cod"]
    ).agg(agg).reset_index()

    return uplift_df
