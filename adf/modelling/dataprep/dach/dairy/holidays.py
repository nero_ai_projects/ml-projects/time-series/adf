import pandas as pd  # type: ignore
from typing import List
from adf.utils.decorators import timeit
from adf.utils.dataframe import memory_reduction
from adf.modelling.dataprep.utils.holidays import (
    add_holidays_count_in_week,
    add_weekday_of_holidays,
)
from adf.modelling.tools import get_snowflake_df_spark
from adf.modelling.dataprep.dach import get_country_name, get_iso_cod_calendar
from adf.config import SNOWFLAKE_DATABASE


@timeit
def get_holidays_df_region(
        location: str, holiday_type, region: str = None
) -> pd.DataFrame:
    """
    Loads and concatenates all holidays S3 flat files into a single dataframe, for a given country
    and holiday type. This method also formats the date column to %Y-%m-%d.

    Parameters
    ----------
    location: str
        country for which to extract holidays data
    holiday_type: str
        value can be "dayoff" or "holidays&dayoff"
    region: str
        if not none, will get files for all regions and add column with the region's name
    Returns
    -------
    pandas.DataFrame
        holidays dataframe
    """
    if region is not None:
        reg = "_" + region
        holidays_df = get_snowflake_df_spark(
            SNOWFLAKE_DATABASE, "VCP_EXP_AFC", "CALENDAR_DATA", "koalas"
        )
        holidays_df = holidays_df.loc[holidays_df["location"] == location + reg]
        holidays_df = holidays_df.loc[holidays_df["holiday_type"] == holiday_type]
        holidays_df = holidays_df.to_pandas()
        holidays_df["date"] = pd.to_datetime(holidays_df["date"], format="%Y-%m-%d")
        holidays_df["region"] = region
    else:
        holidays_df = get_snowflake_df_spark(
            SNOWFLAKE_DATABASE, "VCP_EXP_AFC", "CALENDAR_DATA", "koalas"
        )
        holidays_df = holidays_df.loc[holidays_df["location"].str.startswith(location)]
        holidays_df = holidays_df.loc[holidays_df["holiday_type"] == holiday_type]
        holidays_df = holidays_df.to_pandas()
        holidays_df["date"] = pd.to_datetime(holidays_df["date"], format="%Y-%m-%d")
        holidays_df = holidays_df.groupby(["date"], as_index=False).agg({
            "is_school_holiday": "max",
            "days_before_next_school_holiday": "min",
            "daynum_school_holiday": "max",
            "is_holiday": "max",
            "holiday_name": get_max_holiday_name,
            "days_before_next_holiday": "min",
            "holiday_type": "first"
        })
    return holidays_df


def get_max_holiday_name(series):
    clean_series = [value for value in series if isinstance(value, str)]
    if len(clean_series):
        return max(clean_series)
    else:
        return None


def get_holidays_per_region(division: str, holiday_type: str) -> pd.DataFrame:
    """
    Concatenates all the holiday files loaded by region

    Parameters
    ----------
    division: str
        country for which to extract holidays data
    holiday_type: str
        value can be "dayoff" or "holidays&dayoff"
    Returns
    -------
    pandas.DataFrame
        holidays dataframe for all regions
    """
    all_holidays = pd.concat([
        get_holidays_df_region(
            get_country_name(division).lower(),
            holiday_type,
            reg
        )
        for reg in get_iso_cod_calendar().keys()
    ], axis=0)
    return all_holidays


def process_holidays(
        df: pd.DataFrame, customers_df: pd.DataFrame
) -> pd.DataFrame:
    """
    Gets the region/code mapping for Germany to add the region description to
    the holidays data frame that will allow the merge with customers to get the nfa code

    Parameters
    ----------
    df: pandas.DataFrame
        input dataframe with holidays per region
    customers_df: pandas.DataFrame
        customers database loaded in the panel
    Returns
    -------
    pandas.DataFrame
        holidays dataframe with nfa codes
    """
    mapping_region_cod = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_DACH", "R_MAN_EDP_DAC_GER_RGN_COD_MAP", "koalas"
    ).to_pandas()
    mapping_de = mapping_region_cod[mapping_region_cod.country_code == "DE"] \
        .pipe(format_to_2_digits, "region_code")
    if "region_description" not in df.columns:
        df["region_description"] = df["region"].map(get_iso_cod_calendar())
    df = df. \
        pipe(
            merge_dataframe,
            mapping_de[[
                "region_code",
                "region_description"]],
            on=["region_description"],
            how="left",
            validate="many_to_one") \
        .pipe(
            merge_dataframe,
            customers_df[[
                "nfa_cod",
                "region_code"
            ]].drop_duplicates(),
            on=["region_code"],
            how="left",
            validate="many_to_many")
    return df


def format_to_2_digits(df: pd.DataFrame, col_name: str) -> pd.DataFrame:
    """Transforms the given column into a 4 digit one
    Parameters
    ----------
    df: pandas.DataFrame
        input dataframe
    col_name: str
        column to transform
    Returns
    -------
    pandas.DataFrame
        input dataframe with transformed column
    """
    df[col_name] = df[col_name].apply(lambda x: str(x).zfill(2))
    return df


def prepare_dach_holidays_nfa(df: pd.DataFrame) -> pd.DataFrame:
    """
    Transforms holidays dataframe features to make them relevant for a nfa granularity
    """
    df = df.groupby(["nfa_cod", "date", "region"], as_index=False).first()
    results = []

    nfa_columns = ["nfa_cod",
                   "date"
                   ]
    region_columns = ["nfa_cod",
                      "date",
                      "is_holiday",
                      "days_before_next_holiday"]

    df_nfa = df[nfa_columns] \
        .copy() \
        .drop_duplicates()

    for region in df["region"].unique():
        df_region = df.loc[df["region"] == region].copy()
        df_region = df_region[region_columns]

        for col in region_columns:
            if col == "date" or col == "nfa_cod":
                continue
            df_region.rename(
                columns={
                    col: f"{region}_{col}"
                },
                inplace=True
            )
        results.append(df_region)

    for region in results:
        df_nfa = df_nfa.merge(region, on=["date", "nfa_cod"], how="left")

    return df_nfa


@timeit
def add_weekday_of_holidays_region(
        df: pd.DataFrame, date_column: str
) -> pd.DataFrame:
    """
    For each region, adds holiday's weekday as a new column into input holidays dataframe.
    For non-holiday days, value of this new column is set to -1.
    Parameters
    ----------
    df: pandas.DataFrame
        input holidays dataframe
    date_column: str
        date column in input dataframe

    Returns
    -------
    pandas.DataFrame
        enriched holidays dataframe
    """
    for reg in get_iso_cod_calendar():
        mask = df[reg + "_is_holiday"] == 1
        df.loc[mask, reg + "_holiday_day_of_week"] = df.loc[mask, date_column].dt.weekday
        df[reg + "_holiday_day_of_week"] = df[reg + "_holiday_day_of_week"].fillna(-1)
    return df


def clean_holidays(df: pd.DataFrame) -> pd.DataFrame:
    """
    Creates for each type of feature one holiday feature for all regions:
    sum for is_holiday, min for the days before next holiday and max for holiday
    day of week
    """
    is_holiday_cols = df.filter(regex="is_holiday*")
    days_before_next_holiday_cols = df.filter(regex="days_before_next_holiday*")
    holiday_day_of_week_cols = df.filter(regex="holiday_day_of_week*")
    df["is_holiday"] = is_holiday_cols.sum(axis=1)
    df["days_before_next_holiday"] = days_before_next_holiday_cols.min(axis=1)
    df["holiday_day_of_week"] = holiday_day_of_week_cols.max(axis=1)
    return df.drop((is_holiday_cols +
                    days_before_next_holiday_cols +
                    holiday_day_of_week_cols).columns, axis=1)


def clean_school_holidays(df: pd.DataFrame) -> pd.DataFrame:
    """
    Transforms the raw school holiday file and corrects the name
    of the regions with the ones in the mapping
    """
    df = df.melt(id_vars=["date"],
                 var_name="region_description",
                 value_name="is_school_holiday")
    return df.replace({"region_description": {
        "Baden-Württemberg": "Baden-Wurttemberg",
        "Hessen": "Hesse",
        "Mecklenburg-Vorpommern": "Mecklenburg-Vorpomm.",
        "Bayern": "Bavaria",
        "Nordrhein-Westfalen": "Nrth Rhine Westfalia",
        "Rheinland-Pfalz": "Rhineland Palatinate",
        "Sachsen-Anhalt": "Saxony-Anhalt",
        "Sachsen": "Saxony",
        "Thüringen": "Thuringia",
        "Niedersachsen": "Lower Saxony"
    }})


def groupby_location(df: pd.DataFrame) -> pd.DataFrame:
    """
    Aggregates school holidays per date and nfa code
    """
    df = df.rename({"date": "to_dt"}, axis=1)
    return df.groupby(["to_dt", "nfa_cod"], as_index=False)["is_school_holiday"].sum()


def get_holidays_austria_with_slovenia(nfa_codes: List[int]) -> pd.DataFrame:
    """Return holidays df for Austria and adapted bank holidays for slovenia nfas
    """
    slovenia_nfas = [12543, 12566, 12565, 12562, 12564, 12567]
    austria_nfas = list(set(nfa_codes) - set(slovenia_nfas))

    holidays_slovenia_df = get_holidays_slovenia(slovenia_nfas)
    holidays_austria_df = get_holidays_austria(austria_nfas)

    return pd.concat([holidays_slovenia_df, holidays_austria_df])


def get_holidays_austria(nfa_codes: List[int]) -> pd.DataFrame:
    """Get Austria holidays for concerned list of nfas
    """
    holidays_austria_df = get_holidays_df_region('austria', "dayoff") \
        .pipe(memory_reduction) \
        .pipe(add_weekday_of_holidays, "date") \
        .pipe(add_holidays_count_in_week, "date") \
        .rename({"date": "to_dt"}, axis=1)

    holidays_austria_df.loc[:, 'nfa_cod'] = holidays_austria_df.apply(lambda x: nfa_codes,
                                                                      axis=1)
    holidays_austria_df = holidays_austria_df.explode('nfa_cod')
    return holidays_austria_df


def get_holidays_slovenia(nfa_codes: List[int]) -> pd.DataFrame:
    """Get Slovenia holidays for concerned list of nfas
    """
    holidays_slovenia_df = get_holidays_df_region('slovenia', "dayoff") \
        .pipe(memory_reduction) \
        .pipe(add_weekday_of_holidays, "date") \
        .pipe(add_holidays_count_in_week, "date") \
        .rename({"date": "to_dt"}, axis=1)

    holidays_slovenia_df.loc[:, 'nfa_cod'] = holidays_slovenia_df.apply(lambda x: nfa_codes,
                                                                        axis=1)
    holidays_slovenia_df = holidays_slovenia_df.explode('nfa_cod')
    return holidays_slovenia_df


def merge_dataframe(
        df: pd.DataFrame, second_dataframe: pd.DataFrame, **kwargs
) -> pd.DataFrame:
    """
    Merges two tables
    """
    return df.merge(
        second_dataframe,
        on=kwargs["on"],
        how=kwargs["how"],
        validate=kwargs["validate"]
    )
