import datetime
import logging
import decimal
from typing import Union, Callable, Dict
import numpy as np  # type: ignore
import pandas as pd  # type: ignore
from dateutil.relativedelta import relativedelta
from adf.utils.decorators import timeit, shapeit, showdtypes
from adf.utils.dataframe import memory_reduction
from adf.modelling.dataprep.utils.orders import (
    add_future_orders_until_weekday_of_n_previous_weeks,
    add_future_orders_of_n_previous_days,
    remove_innovations,
    remove_products_not_in_last_min_depth_history
)
from adf.modelling.dataprep.dach.dairy.holidays import (
    get_holidays_austria_with_slovenia,
    get_holidays_per_region,
    process_holidays,
    prepare_dach_holidays_nfa,
    add_weekday_of_holidays_region,
    clean_holidays,
    clean_school_holidays,
    groupby_location,
    get_holidays_df_region
)
from adf.errors import DataprepError
from adf.modelling.tools import get_snowflake_df_spark
from adf.modelling import mllogs
from adf.modelling.dataprep.dach.dairy.promo import run as run_promo_dataprep
from adf.modelling.dataprep.dach.dairy.promo_means import run as run_promo_means
from adf.services.database.query import update_report
from adf.modelling.dataprep.utils.core import (
    build_dataprep_report
)
from adf.modelling.dataprep.utils.core import build_panel_indexes
from adf.modelling.preprocessing.tools.aggregator import most_frequent_value
from adf.modelling.dataprep.utils.holidays import (
    add_holidays_count_in_week,
    add_weekday_of_holidays
)
from adf.modelling.dataprep.dach import (
    get_shift_mapping,
    get_country_name_promo,
    get_country_name,
    get_sales_organization_cod)
from adf.modelling.dataprep.utils.plc import apply_plc
from adf.modelling.dataprep.dach.dairy.plc import apply_sku_replacements
from adf.config import SNOWFLAKE_DATABASE


log = logging.getLogger("adf")


def run(
        country: str, division: str, product_level: str,
        location_level: str, **context
):
    """ Builds panel data for Germany Dairy given product_level and location_level.
    :product_level: sku
    :location_level: departmental, national
    :division: "dairy_ch", "dairy_de", "dairy_at"
    """
    assert product_level == "sku"
    assert location_level in ("national", "departmental")
    assert division in ("dairy_ch", "dairy_de", "dairy_at")

    product_column = {
        "sku": "mat_cod",
    }[product_level]

    location_column = {
        "national": "country_cod",
        "departmental": "nfa_cod"
    }[location_level]

    session = context["session"]

    if division == 'dairy_de':
        split_type = 'dask'
    else:
        split_type = ''

    report = build_dataprep_report(
        session,
        country,
        division,
        product_level,
        location_level,
        scope="panel",
        split_type=split_type,
        split_code=None,
        prep_type="core",
        pipeline_id=context.get("pipeline_id", None)
    )

    mllogs.log_dataprep_report(report)

    try:
        log.info("Building panel")
        panel_df = build_panel_data(country, division, product_column, location_column)
        panel_df = convert_final_columns(division, panel_df)
        mllogs.log_shape(panel_df)

        log.info("Saving")
        if division == 'dairy_de':
            report.upload(panel_df, partition_on=["nfa_cod"])
        else:
            report.upload(panel_df)
        update_report(session, report, "success")
        mllogs.tag_report_output(report)

    except Exception as err:
        message = str(err.__class__.__name__) + ": " + str(err)
        update_report(session, report, "failed", message)
        raise DataprepError(message)

    finally:
        mllogs.tag_report_output(report)

    return report, None


def build_panel_data(
        country: str, division: str,
        product_column: str, location_column: str
) -> pd.DataFrame:

    products_df = (
        get_snowflake_df_spark(
            SNOWFLAKE_DATABASE, 'VCP_DSP_AFC_EDP_DACH', "R_SAP_EDP_DAC_PDT_HIE", "koalas"
        )
        .to_pandas()
        .pipe(cast_decimal_to_numeric)
        .pipe(process_products, division)
    )
    products_df["mat_cod"] = pd.to_numeric(products_df["mat_cod"])

    media = None
    sellout_df = None

    if division == 'dairy_de':
        sellout_minden = (
            get_snowflake_df_spark(
                SNOWFLAKE_DATABASE,
                'VCP_DSP_AFC_EDP_DACH',
                "F_MAN_EDP_DAC_SEL_OUT_MEN",
                "koalas"
            )
            .to_pandas()
            .pipe(cast_decimal_to_numeric)
            .pipe(add_nfa, "Edeka_minden")
        )
        sellout_sudwest = (
            get_snowflake_df_spark(
                SNOWFLAKE_DATABASE,
                'VCP_DSP_AFC_EDP_DACH',
                "F_MAN_EDP_DAC_SEL_OUT_SOU_WES",
                "koalas"
            )
            .to_pandas()
            .pipe(cast_decimal_to_numeric)
            .pipe(add_nfa, "Edeka_sudwest")
        )
        sellout_rewe = (
            get_snowflake_df_spark(
                SNOWFLAKE_DATABASE,
                'VCP_DSP_AFC_EDP_DACH',
                "F_MAN_EDP_DAC_SEL_OUT_REW",
                "koalas"
            )
            .to_pandas()
            .pipe(cast_decimal_to_numeric)
            .pipe(add_nfa, "REWE Gebiete")
            .pipe(resample_to_weekly)
        )
        sellout_penny = (
            get_snowflake_df_spark(
                SNOWFLAKE_DATABASE,
                'VCP_DSP_AFC_EDP_DACH',
                "F_MAN_EDP_DAC_SEL_OUT_PEN",
                "koalas"
            )
            .to_pandas()
            .pipe(cast_decimal_to_numeric)
            .pipe(add_nfa, "PENNY")
            .pipe(resample_to_weekly)
        )
        sellout_history = (
            get_snowflake_df_spark(
                SNOWFLAKE_DATABASE,
                'VCP_DSP_AFC_EDP_DACH',
                "F_MAN_EDP_DAC_SEL_OUT_HIS",
                "koalas"
            )
            .to_pandas()
        )
        sellout_df = (
            pd
            .concat(
                [
                    sellout_history,
                    sellout_minden,
                    sellout_sudwest,
                    sellout_rewe,
                    sellout_penny
                ]
            )
            .pipe(filter_nan)
            .pipe(update_datetime, "sellout_date")
            .pipe(process_sellout)
            .pipe(groupby_sellout)
        )
        media = (
            get_snowflake_df_spark(
                SNOWFLAKE_DATABASE,
                'VCP_DSP_AFC_EDP_DACH',
                "R_IBP_EDP_DAC_MRK",
                "koalas"
            )
            .to_pandas()
            .pipe(clean_media)
            .pipe(format_to_4_digits, "sales_organization_cod")
            .query(f'sales_organization_cod=="{get_sales_organization_cod(division)}"')
            .pipe(process_media)
        )

    customers_df = (
        get_snowflake_df_spark(
            SNOWFLAKE_DATABASE,
            'VCP_DSP_AFC_EDP_DACH',
            "R_SAP_EDP_DAC_CUS_HIE_NFA_RFA",
            "koalas"
        )
        .to_pandas()
        .pipe(cast_decimal_to_numeric)
        .pipe(format_customers, division)
        .pipe(filter_customers_features)
    )

    orders_df = (
        get_snowflake_df_spark(
            SNOWFLAKE_DATABASE,
            'VCP_DSP_AFC_EDP_DACH',
            "D_SAP_EDP_DAC_DRY_ORD",
            "koalas"
        )
        .to_pandas()
        .pipe(cast_decimal_to_numeric)
        .pipe(filter_orders_data, division)
        .drop_duplicates()
        .pipe(process_orders_data, division)
    )
    orders_df = merge_dataframe(
        orders_df,
        customers_df[
            [
                "cus_cod",
                "distribution_channel_cod",
                location_column
            ]
        ],
        on=["cus_cod", "distribution_channel_cod"],
        how="left",
        validate="many_to_one"
    )
    orders_df = remove_innovations(
        orders_df,
        product_column,
        "to_dt",
        min_depth_history=3,
        depth_unit="W",
        view_from_date=datetime.date.today().strftime("%Y-%m-%d")
    )
    orders_df = (
        remove_products_not_in_last_min_depth_history(
            orders_df,
            product_column,
            "to_dt",
            3
        )
        .pipe(add_non_delivered_sku_nfa)
        .pipe(add_non_delivered_nfa)
    )
    orders_df = merge_dataframe(
        orders_df,
        products_df[
            [
                "mat_cod",
                "mat_umbrella_cod"
            ]
        ],
        on=["mat_cod"],
        how="inner",
        validate="many_to_one"
    )

    end_date = build_end_date(future_months_number=6)

    log.info("Building SKU panels")

    orders_df = merge_dataframe(
        orders_df,
        customers_df,
        on=["sales_organization_cod", "cus_cod",
            "distribution_channel_cod", location_column],
        how="left",
        validate=None
    )

    orders_df = orders_df.pipe(aggregate_orders, location_column, product_column)
    panel_data = build_panel_indexes(
        orders_df=orders_df,
        product_column=product_column,
        location_column=location_column,
        customer_column="nfa_cod",
        sales_organization_column="sales_organization_cod",
        date_column="to_dt",
        end_date=end_date
    )
    panel_data = (
        merge_dataframe(
            panel_data, orders_df, on=list(panel_data.columns), how="left", validate="one_to_one"
        )
        .fillna(value={"ordered_units": 0})
    )
    panel_data = merge_products(panel_data, products_df, product_column)

    holidays_per_region, school_holidays, holidays_df, school_holidays_df = None, None, None, None
    if division == 'dairy_de':
        panel_data = panel_data.pipe(merge_media, media)
        panel_data = apply_plc("dach", division, panel_data, "snowflake")

        holidays_per_region = (
            get_holidays_per_region(division, "dayoff")
            .pipe(process_holidays, customers_df)
            .pipe(prepare_dach_holidays_nfa)
            .pipe(update_datetime, "date")
            .pipe(add_weekday_of_holidays_region, "to_dt")
            .pipe(clean_holidays)
            .pipe(add_holidays_count_in_week, "to_dt", "nfa_cod")
            .drop(["date"], axis=1)
        )

        school_holidays = (
            get_snowflake_df_spark(
                SNOWFLAKE_DATABASE,
                "VCP_DSP_AFC_EDP_DACH",
                "R_MAN_EDP_DAC_GER_SCO_HOL",
                "koalas"
            )
            .to_pandas()
            .pipe(clean_school_holidays)
            .pipe(process_holidays, customers_df)
            .pipe(groupby_location)
        )

    elif division == 'dairy_at':
        holidays_df = get_holidays_austria_with_slovenia(
            list(panel_data['nfa_cod'].unique())
        )
        school_holidays_df = (
            get_snowflake_df_spark(
                SNOWFLAKE_DATABASE,
                'VCP_DTM_AFC',
                'DACH_AT_EDP_SCO_HOL_MAP',
                "koalas"
            )
            .to_pandas()
        )
        panel_data = panel_data.pipe(apply_sku_replacements, division)

    else:
        panel_data = panel_data.pipe(apply_sku_replacements, division)
        holidays_df = (
            get_holidays_df_region(get_country_name(division), "dayoff")
            .pipe(memory_reduction)
            .pipe(add_weekday_of_holidays, "date")
            .pipe(add_holidays_count_in_week, "date")
            .rename({"date": "to_dt"}, axis=1)
        )

    promo_df = (
        get_snowflake_df_spark(
            SNOWFLAKE_DATABASE,
            "VCP_DSP_AFC_EDP_DACH",
            "R_BLP_EDP_DAC_PRM_HEA",
            "koalas"
        )
        .to_pandas()
        .drop_duplicates()
        .pipe(set_promo_mechanic, division)
        .pipe(clean_promo, division)
    )

    promo_volume_df = (
        get_snowflake_df_spark(
            SNOWFLAKE_DATABASE,
            "VCP_DSP_AFC_EDP_DACH",
            "R_BLP_EDP_DAC_PRM_TRN",
            "koalas"
        )
        .to_pandas()
        .drop_duplicates()
        .pipe(clean_promo_volumes, division)
    )

    if division == 'dairy_de':
        promo_df = promo_df.apply(shift_location_start_sellin, axis=1)
        promo_df = apply_plc(country, division, promo_df, "snowflake")
        promo_volume_df = apply_plc(country, division, promo_volume_df, "snowflake")
    else:
        promo_df = promo_df.pipe(apply_sku_replacements, division)
        promo_volume_df = promo_volume_df \
            .pipe(apply_sku_replacements, division)

    promo_means = run_promo_means(promo_df, panel_data)
    promo_uplifts = run_promo_dataprep(promo_df, panel_data)
    promo_bp_features = build_promo_bp_features(promo_volume_df, promo_df)
    promo_uplifts = promo_uplifts.reset_index(drop=True)
    promo_means = promo_means.reset_index(drop=True)

    panel_data = aggregate_sku_panel(panel_data, product_column, location_column)
    panel_data = merge_dataframe(
        panel_data,
        promo_uplifts,
        on=["mat_cod", "nfa_cod", "to_dt"],
        how="left",
        validate="one_to_one"
    )
    panel_data = merge_dataframe(
        panel_data,
        promo_means,
        on=["mat_cod", "nfa_cod", "to_dt"],
        how="left",
        validate="one_to_one"
    )

    if division == 'dairy_de':
        panel_data = (
            panel_data
            .pipe(
                merge_dataframe,
                sellout_df,
                on=["mat_cod", "nfa_cod", "to_dt"],
                how="left",
                validate="one_to_one"
            )
            .pipe(
                merge_dataframe,
                holidays_per_region,
                on=["nfa_cod", "to_dt"],
                how="left",
                validate="many_to_one"
            )
            .pipe(
                merge_dataframe,
                school_holidays,
                on=["nfa_cod", "to_dt"],
                how="left",
                validate="many_to_one"
            )
        )

    elif division == 'dairy_at':
        panel_data = (
            panel_data
            .pipe(
                merge_dataframe,
                holidays_df,
                on=["nfa_cod", "to_dt"],
                how="left",
                validate="many_to_one"
            )
            .drop(columns=["is_school_holiday", "days_before_next_school_holiday"])
            .pipe(
                merge_dataframe,
                school_holidays_df,
                on=["to_dt"],
                how="left",
                validate="many_to_one"
            )
        )

    else:
        panel_data = (
            panel_data
            .pipe(
                merge_dataframe,
                holidays_df,
                on="to_dt",
                how="left",
                validate="many_to_one"
            )
        )

    panel_data = (
        panel_data
        .pipe(
            merge_dataframe,
            promo_bp_features,
            on=["mat_cod", "nfa_cod", "to_dt"],
            how="left",
            validate="one_to_one"
        )
    )

    panel_data["to_dt"] = (
        pd
        .to_datetime(panel_data.to_dt)
        .dt
        .tz_localize(None)
    )

    return panel_data


def cast_decimal_to_numeric(df):
    for col in df.columns:
        if isinstance(df[col].values[0], decimal.Decimal):
            df[col] = pd.to_numeric(df[col], errors="coerce")
    return df


def add_nfa(df: pd.DataFrame, nfa: str) -> pd.DataFrame:
    """Adds new column with nfa code based on nfa description
    after loading each nfa file
    Parameters
    ----------
    df: pandas.DataFrame
        input table
    nfa: str
        name of the nfa (file name)

    Returns
    -------
    pandas.DataFrame with new nfa code column
    """
    nfa_dict = {
        "REWE Gebiete": 10005,
        "PENNY": 12529,
        "Edeka_minden": 10211,
        "Edeka_sudwest": 10219
    }
    df["nfa_desc"] = nfa
    df['nfa_cod'] = df['nfa_desc'].map(nfa_dict)
    return df


def build_end_date(future_months_number: int) -> pd.Timestamp:
    """
    Returns date which is 'future_months_number' months after current date
    """
    return pd.Timestamp(datetime.date.today() + relativedelta(months=future_months_number))


def filter_nan(df: pd.DataFrame) -> pd.DataFrame:
    """Removes nan values in sellout file
    """
    mask1 = df["sellout_date"].isnull()
    mask2 = df["sellout_volumes"].isnull()
    mask3 = df["mat_cod"].isnull()
    return df[(~mask1) & (~mask2) & (~mask3)]


def clean_media(df: pd.DataFrame) -> pd.DataFrame:
    return df.query('umbrella_cod not in "<Null>"')


def process_media(df: pd.DataFrame) -> pd.DataFrame:
    """Aggregates media file by date and umbrella code and
    resamples the weekly to daily"""
    filter_col = [col for col in df if
                  col.startswith("a_and_p") or col.startswith("grp")]
    return (
        df
        .groupby(by=["date", "umbrella_cod"])
        .sum()
        .reset_index()
        .pipe(resample_to_daily_media, filter_col, "umbrella_cod", "date")
        .rename({"date": "to_dt"}, axis=1)
    )


def resample_to_daily_media(
        df: pd.DataFrame, columns: list, location: str,
        target_date_column: str
) -> pd.DataFrame:
    """Resamples weekly to daily; the difference with function resample to
    daily is that it aggregates with a list of columns not just one
    Parameters
    ----------
    df: pandas.DataFrame
        input table
    columns: list
        list of columns to aggregate
    location: str
        location column to aggregate with
    target_date_column: str
        date to use for daily resampling
    Returns
    -------
    pandas.DataFrame
        resampled data to daily
    """
    return (
        df
        .set_index(target_date_column)
        .groupby([location])[columns]
        .resample("1D")
        .pad()
        .reset_index()
    )


def merge_media(df: pd.DataFrame, media_df: pd.DataFrame) -> pd.DataFrame:
    """
        Function to merge media data with orders
        Merge is done based on date and umbrella code
        We only take the last three characters of the orders' umbrella code
    """
    df["umbrella_cod"] = df["mat_umbrella_cod"].apply(lambda x: str(x)[-3:])
    return df.merge(
        media_df,
        on=["to_dt", "umbrella_cod"],
        how="left",
        validate="many_to_one"
    )


def update_datetime(df: pd.DataFrame, date: str) -> pd.DataFrame:
    """Puts a given date in datetime format and renames it to to_dt"""
    df["to_dt"] = pd.to_datetime(df[date], format="%Y-%m-%d")
    return df.dropna(subset=["to_dt"])


@timeit
@shapeit
@showdtypes
def merge_dataframe(
        df: pd.DataFrame, second_dataframe: pd.DataFrame, **kwargs
) -> pd.DataFrame:
    """
    Merges two tables
    """
    return df.merge(
        second_dataframe,
        on=kwargs["on"],
        how=kwargs["how"],
        validate=kwargs["validate"]
    )


def filter_orders_data(df: pd.DataFrame, division: str) -> pd.DataFrame:
    """
        Filters orders by selecting the sales organization code and by
        removing rejected orders, returns
        and filters on distribution channels 0 and 1
    """
    return (
        df
        .pipe(format_to_4_digits, "sales_organization_cod")
        .pipe(filter_organization_cod, division)
        .pipe(remove_rejected_orders)
        .pipe(remove_returns)
        .pipe(filter_on_distribution_channel, division)
        .pipe(filter_nachkomission, division)
    )


def remove_rejected_orders(df: pd.DataFrame) -> pd.DataFrame:
    return df.query('rejection_cod == "#"')


def remove_returns(df: pd.DataFrame) -> pd.DataFrame:
    return df.query('~sal_doc_typ_cod.str.startswith("ZR")', engine="python")


def filter_on_distribution_channel(df: pd.DataFrame, division: str) -> pd.DataFrame:
    mapping_distrib_channel = {
        'dairy_de': '[0,1]',
        'dairy_ch': '[0,1]',
        'dairy_at': '[0,1,3]'
    }
    df["distribution_channel_cod"] = df["distribution_channel_cod"].astype('int64')
    return df.query(f"distribution_channel_cod in {mapping_distrib_channel[division]}")


def filter_nachkomission(df: pd.DataFrame, division: str) -> pd.DataFrame:
    if division == 'dairy_at':
        return (
            df
            .query('order_reason_cod != "A"', engine="python")
            .pipe(remove_nk_history)
        )
    else:
        return df


def remove_nk_history(orders_df: pd.DataFrame) -> pd.DataFrame:
    # Method 1 NK in description :
    orders_df = orders_df[~orders_df['sal_cus_pur_doc_num'].str.endswith('NK')]
    # Method 2 NK duplicate command with 0 delivered qty:
    nk_keys = [
        'cus_cod', 'mat_cod', 'distribution_channel_cod',
        'plant_cod', 'order_creation_date', 'req_delivery_date', 'ordered_quantity'
    ]
    duplicates_to_clean = orders_df[orders_df.duplicated(nk_keys, keep=False)]
    index_of_nks_to_drop = duplicates_to_clean[
        duplicates_to_clean['delivered_quantity'] == 0].index
    orders_df = orders_df[~orders_df.index.isin(index_of_nks_to_drop)]
    return orders_df


def process_orders_data(df: pd.DataFrame, division: str) -> pd.DataFrame:
    """Processes orders' data and adds weekly future orders until monday
    and daily future orders then drops unused columns"""
    if division == 'dairy_at':
        ref_date = 'req_delivery_date'
    else:
        ref_date = 'to_dt'

    df = (
        df
        .pipe(build_target_dates_and_quantities, "to_dt", "ordered_units")
        .pipe(update_creation_date)
        .reset_index(drop=True)
        .pipe(
            add_future_orders_until_weekday_of_n_previous_weeks,
            ref_date, "order_creation_date", "ordered_units",
            interval=range(0, 7), until_weekday_included=0
        )
        .pipe(add_service_level)
        .pipe(
            add_future_orders_of_n_previous_days,
            "to_dt", "order_creation_date", "ordered_units",
            interval=range(0, 21)
        )
    )
    df['to_dt'] = df['to_dt'].map(lambda x: pd.Timestamp(x))
    return df.drop(
        [
            "order_creation_date",
            "material_availability_date",
            "rejection_cod",
            "sal_cus_pur_doc_num",
            "sal_doc_num",
            "sal_doc_typ_cod",
            "order_reason_cod"
        ],
        axis=1
    )


def remove_old_products(df, product_column):
    today = datetime.date.today()
    start_date = (today + relativedelta(months=-12)).strftime("%Y-%m-%d")
    mask = ((df['to_dt'] >= start_date) & (df['ordered_units'] > 0))
    active_prod = list(set(df.loc[mask][product_column]))
    return df.query(f'{product_column}.isin({active_prod})', engine="python")


def format_to_4_digits(df: pd.DataFrame, col_name: str) -> pd.DataFrame:
    df[col_name] = df[col_name].apply(lambda x: str(x).zfill(4))
    return df


def build_target_dates_and_quantities(
        df: pd.DataFrame, new_date_col: str, new_qty_col: str
) -> pd.DataFrame:
    df.loc[:, new_qty_col] = df.loc[:, 'ordered_quantity']
    df.loc[:, new_date_col] = df.loc[:, "material_availability_date"]
    df = df.dropna(subset=[new_date_col, new_qty_col], how="any")
    return df


def update_creation_date(df: pd.DataFrame) -> pd.DataFrame:
    """When order creation date is ulterior to material availability
    date it's replaced by material availability date"""
    mask = df["order_creation_date"] > df["to_dt"]
    df.loc[mask, "order_creation_date"] = df.loc[mask, "to_dt"]
    return df


def add_service_level(df: pd.DataFrame) -> pd.DataFrame:
    """
    Adds two new columns with the difference between
    delivered volumes and ordered ones as well the ratio
    """
    df["service_level"] = df["delivered_quantity"] / df["ordered_quantity"]
    return df


def aggregate_orders(
        df: pd.DataFrame, product_column: str, location_column: str
) -> pd.DataFrame:
    """Aggregates orders' data by country, product, location, customer
    and date by summing orders' volumes and taking first values for
    media indexes"""
    to_agg = {"service_level": "mean",
              **{col: "sum" for col in df.columns if "ordered_units" in col}}
    to_agg.update({f: "first" for f in df.columns if "a_and_p" in f or "grp" in f})
    return (
        df
        .groupby(
            by=[
                "sales_organization_cod",
                product_column,
                location_column,
                "to_dt"
            ]
        )
        .agg(to_agg)
        .reset_index()
    )


def process_products(
        df: pd.DataFrame, division: str
) -> pd.DataFrame:
    """Processes products' table by filtering on the right country,
    formatting products and aggregating them"""
    return (
        df
        .pipe(filter_organization_cod, division)
        .pipe(format_products)
        .pipe(filter_product_features)
        .pipe(aggregate_products)
    )


def format_products(df: pd.DataFrame) -> pd.DataFrame:
    df = df.replace(to_replace=["#", "(null)"], value=np.NaN)
    df = format_to_4_digits(df, "sales_organization_cod")
    df.loc[:, "distribution_channel_cod"] = \
        df["distribution_channel_cod"].astype(str).apply(
            lambda x: x[-1:]).astype('int64')
    return df


def filter_product_features(df: pd.DataFrame) -> pd.DataFrame:
    """Remove useless product columns
    """
    cols_to_drop = [col for col in df.columns if any(substring in col for substring in [
        'weight', 'length', 'flavour', 'dimension', 'width'])]
    return df.drop(cols_to_drop, axis=1)


def aggregate_products(df: pd.DataFrame) -> pd.DataFrame:
    """Aggregates products' table by country and product to keep
    one value
    """
    df = df.drop(["mat_ean_cod"], axis=1)
    to_agg: Dict[str, Union[str, Callable]] = {
        k: most_frequent_value for k in df.columns if k not in [
            "sales_organization_cod",
            "mat_cod",
            "mat_desc",
            "mat_creation_dat"
        ]
    }
    to_agg.update(
        {
            "mat_desc": "first",
            "mat_creation_dat": "min"
        }
    )

    df = (
        df
        .groupby(by=["sales_organization_cod", "mat_cod"])
        .agg(to_agg)
        .reset_index()
    )
    df["mat_cod"] = df["mat_cod"].astype('int64')
    return df


def format_customers(df: pd.DataFrame, division: str) -> pd.DataFrame:
    """
        Processes customers' table by filtering on country,
        dropping duplicates, cleaning the table, formatting sales organization
        and changing format of distribution channel and nfa code to integer
    """
    df = (
        df
        .pipe(filter_organization_cod, division)
        .drop_duplicates()
        .pipe(clean_customer_data)
        .pipe(format_to_4_digits, "sales_organization_cod")
    )
    df["distribution_channel_cod"] = df["distribution_channel_cod"].astype('int64')
    df['nfa_cod'] = df['nfa_cod'].astype('int64')
    return df.drop(columns=["distribution_channel_desc", "cus_name"], axis=1)


def filter_customers_features(df: pd.DataFrame) -> pd.DataFrame:
    """
        Remove useless customer columns
    """
    cols_to_drop = [col for col in df.columns if 'level' in col]
    cols_to_drop += ['rfa_cod', 'rfa_desc']
    cols_to_drop.remove('level6_cus_cod')
    return df.drop(cols_to_drop, axis=1)


def clean_customer_data(df: pd.DataFrame) -> pd.DataFrame:
    """Cleans customers' table by removing special characters and nans"""
    df = df.replace(to_replace=["#", "(null)", "nan"], value=np.NaN)
    df = df.dropna(subset=['nfa_cod'])
    return df


def filter_organization_cod(df: pd.DataFrame, division: str) -> pd.DataFrame:
    """Keeps only the country code corresponding to the given division"""
    return df[df["sales_organization_cod"] == get_sales_organization_cod(division)]


@timeit
@shapeit
def merge_products(
        sku_panel_df: pd.DataFrame,
        products_df: pd.DataFrame,
        product_column: str
) -> pd.DataFrame:
    """Merges panel data with products on country and product if available
    otherwise on country, brand and customer level three"""
    if product_column == "mat_cod":
        return sku_panel_df.merge(
            products_df,
            on=["sales_organization_cod", "mat_cod"],
            how="left")
    else:
        return sku_panel_df.merge(
            products_df[
                ["sales_organization_cod", "mat_brand_cod", "CUS_LV3_DSC"]
            ].drop_duplicates(),
            on=["sales_organization_cod", "mat_brand_cod"],
            how="left"
        )


def aggregate_sku_panel(
        sku_panel_df: pd.DataFrame, product_column: str, location_column: str
) -> pd.DataFrame:
    """Aggregates sku panel by product, location and date
    It sums ordered volumes and takes the first value for the rest"""
    sku_panel_df = sku_panel_df.drop(["sales_organization_cod"], axis=1)

    agg = {
        k: "first" for k in sku_panel_df.columns
        if k not in [product_column, location_column, "to_dt"]
    }
    agg.update(
        {
            k: "sum" for k in sku_panel_df.columns if "ordered_units" in k
        }
    )

    return (
        sku_panel_df
        .groupby([product_column, location_column, "to_dt"])
        .agg(agg)
        .reset_index()
    )


# TODO: Investigate bug and remove this method
def convert_final_columns(division: str, df: pd.DataFrame) -> pd.DataFrame:
    df.to_dt = df["to_dt"].dt.to_pydatetime()
    df.mat_creation_dat = pd.to_datetime(
        df["mat_creation_dat"], errors="coerce"
    ).astype("datetime64").dt.to_pydatetime()
    df.mat_start_validation_dat = pd.to_datetime(
        df["mat_start_validation_dat"],
        errors="coerce").astype("datetime64").dt.to_pydatetime()
    df.mat_end_validation_dat = pd.to_datetime(
        df["mat_end_validation_dat"],
        errors="coerce").astype("datetime64").dt.to_pydatetime()
    df['nfa_cod'] = df['nfa_cod'].astype('int')
    df[list(set(df.columns) - set(df.select_dtypes("object").columns))] = df[
        list(set(df.columns) - set(df.select_dtypes("object").columns))
    ].fillna(0)
    df = df.astype({'mat_cod': int, 'is_holiday': int})
    if division != "dairy_ch":
        df["is_school_holiday"] = df["is_school_holiday"].fillna(0)
        df = df.astype({'is_school_holiday': int})
    if division == "dairy_ch":
        df = df.astype({'holidays_count_in_week': int})
    if division == "dairy_de":
        df[[
            "a_and_p_conventional", "a_and_p_unconventional",
            "grp_conventional", "grp_unconventional"
        ]] = df[[
            "a_and_p_conventional", "a_and_p_unconventional",
            "grp_conventional", "grp_unconventional"
        ]].fillna(0)
        df = df.astype({
            'sellout_volumes_of_week': float,
            "a_and_p_conventional": int, "a_and_p_unconventional": int,
            "grp_conventional": int, "grp_unconventional": int
        })
    if division == "dairy_at":
        df = df.drop(columns=["holiday_name"])
        df["days_before_next_school_holiday"] = df["days_before_next_school_holiday"].fillna(0)
        df = df.astype({"days_before_next_school_holiday": int})
    df = df.astype({
        'uplift_prev_week': float,
        'downlift_prev_post_promo_week': float, 'downlift_cannib_week': float
    })
    return df


def set_promo_mechanic(promo_headers: pd.DataFrame, division: str) -> pd.DataFrame:
    if division in ('dairy_de', 'dairy_at'):
        promo_headers['promotion_mechanics_cod'] = promo_headers['promotion_mechanics_cod_de_at']
    elif division == 'dairy_ch':
        promo_headers['promotion_mechanics_cod'] = promo_headers['promotion_mechanics_cod_ch']
    promo_headers = promo_headers.drop(
        [
            'promotion_mechanics_cod_de_at',
            'promotion_mechanics_cod_ch'
        ], axis=1
    )
    return promo_headers


def clean_promo(promo_df: pd.DataFrame, division: str) -> pd.DataFrame:
    dedup_keys = [
        'nfa_cod',
        'mat_cod',
        'promotion_start_date_store',
        'promotion_end_date_store',
        'status_code'
    ]

    promo_df = (
        promo_df
        .pipe(filter_promo, division)
        .drop_duplicates(dedup_keys)
    )

    if division == 'dairy_at':
        promo_df['promotion_mechanics_cod'] = promo_df['promotion_mechanics_cod'].fillna(0)

    promo_df = promo_df.dropna(subset=['promotion_mechanics_cod'])
    promo_df['promotion_mechanics_cod'] = promo_df['promotion_mechanics_cod'].astype(int)
    promo_df['promotion_mechanics_cod'] = promo_df[
        'promotion_mechanics_cod'].apply(lambda x: x + 1)

    promo_df = promo_df.drop_duplicates(
        ["mat_cod", "nfa_cod", "promotion_mechanics_cod",
         "promotion_start_date_sellin", "promotion_end_date_sellin"],
        keep="first"
    )
    return promo_df


def clean_promo_volumes(promo_volumes_df: pd.DataFrame, division: str) -> pd.DataFrame:
    promo_volumes_df = promo_volumes_df.drop_duplicates(['mat_cod', 'promotion_cod', 'monday_date'])
    promo_volumes_df = promo_volumes_df.loc[
        promo_volumes_df['territory'] == get_country_name_promo(division)]
    promo_volumes_df['monday_date'] = promo_volumes_df['monday_date']\
        .apply(lambda x: x - datetime.timedelta(days=x.weekday()))
    promo_volumes_df = promo_volumes_df.groupby(['mat_cod', 'monday_date', 'promotion_cod']).agg(
        {'uplift_prev_week': 'max',
         'downlift_prev_post_promo_week': 'min',
         'downlift_cannib_week': 'min'}).reset_index()
    promo_volumes_df['to_dt'] = promo_volumes_df['monday_date'].apply(get_week_days)
    promo_volumes_df = promo_volumes_df.explode('to_dt') \
        .drop('monday_date', axis=1)
    return promo_volumes_df


def build_promo_bp_features(
        promo_df_volume: pd.DataFrame, promo_df: pd.DataFrame
) -> pd.DataFrame:
    mapping_promo_nfa_cod = promo_df[['promotion_cod', 'nfa_cod']].drop_duplicates()
    mapping_promo_nfa_cod = mapping_promo_nfa_cod.drop_duplicates('promotion_cod')
    promo_bp_features = (
        promo_df_volume
        .merge(
            mapping_promo_nfa_cod,
            on=['promotion_cod'],
            how='inner',
            validate='many_to_one'
        )
        .groupby(['mat_cod', 'nfa_cod', 'to_dt'])
        .agg(
            {
                'uplift_prev_week': 'sum',
                'downlift_prev_post_promo_week': 'min',
                'downlift_cannib_week': 'min'
            }
        )
        .reset_index()
    )
    return promo_bp_features


def get_week_days(monday_date):
    return [monday_date + datetime.timedelta(days=i) for i in range(7)]


def shift_location_start_sellin(row: pd.Series) -> pd.Series:
    """Shifts promotion sellin start and end dates based on a shift mapping that depends on
    customers' behaviours.
    This methods applies to a promotions dataframe's row.
    """
    shift_mapping = get_shift_mapping()
    if shift_mapping:
        if row["nfa_cod"] in shift_mapping.keys():
            nfa = row["nfa_cod"]
            row["promotion_start_date_sellin"] = row[
                "promotion_start_date_sellin"] + datetime.timedelta(
                    days=shift_mapping[nfa][0])
            row["promotion_end_date_sellin"] = row[
                "promotion_end_date_sellin"] + datetime.timedelta(
                days=shift_mapping[nfa][1])
    return row


def filter_promo(promo_df: pd.DataFrame, division: str) -> pd.DataFrame:
    """ Clean promotion dataframe \
        - filter selected country \
        - remove canceled promotions \
        - promotions on non-valid sku
    """
    promo_df['status_code'] = promo_df['status_code'].astype(int)
    promo_df = promo_df.loc[(promo_df['status_code'] < 1500) &
                            (promo_df['territory'] == get_country_name_promo(division)) &
                            (promo_df['mat_cod'].astype(str).str.isdigit())]
    promo_df = promo_df.dropna(subset=['mat_cod', 'nfa_cod'], axis=0)
    promo_df['nfa_cod'] = promo_df['nfa_cod'].astype(float).astype(int)
    promo_df['mat_cod'] = promo_df['mat_cod'].astype('int64')
    return promo_df


@timeit
def process_sellout(sellout_df: pd.DataFrame) -> pd.DataFrame:
    """Processes sellout data by aggregating on date, nfa code and product
    then resampling weekly data to daily"""
    return (
        sellout_df
        .groupby(["to_dt", "nfa_cod", "mat_cod"])["sellout_volumes"]
        .sum()
        .reset_index()
        .pipe(resample_to_daily, "sellout_volumes", "mat_cod", "nfa_cod", "to_dt")
        .astype({"nfa_cod": int, "mat_cod": int})
    )


@timeit
def groupby_sellout(
        sellout_df: pd.DataFrame) -> pd.DataFrame:
    """Processes sellout data by aggregating on date, nfa code and product"""
    return (
        sellout_df
        .groupby(["to_dt", "nfa_cod", "mat_cod"])["sellout_volumes_of_week"]
        .sum()
        .reset_index()
    )


def resample_to_daily(
        df: pd.DataFrame, data_type: str, product_column: str, location: str,
        target_date_column: str
) -> pd.DataFrame:
    """Resamples weekly data of a column to daily"""
    return (
        df
        .set_index(target_date_column)
        .groupby([product_column, location])[data_type]
        .resample("1D")
        .pad()
        .reset_index()
        .rename({data_type: data_type + "_of_week"}, axis=1)
    )


# TODO: replace loffset cause will be deprecated soon
def resample_to_weekly(
        df: pd.DataFrame
) -> pd.DataFrame:
    log.info(df.info(verbose=True))
    return (
        df
        .set_index("sellout_date")
        .groupby(["mat_cod", "nfa_cod"])["sellout_volumes"]
        .resample("W", label='left', loffset=pd.DateOffset(days=1))
        .mean()
        .reset_index()
    )


def add_non_delivered_sku_nfa(orders_df: pd.DataFrame) -> pd.DataFrame:
    grouped_orders = (
        orders_df
        .groupby(
            ['distribution_channel_cod', 'nfa_cod', 'mat_cod', 'to_dt']
        )
        .agg({'ordered_quantity': 'sum', 'delivered_quantity': 'sum'})
        .reset_index()
        .sort_values('to_dt')
    )
    grouped_orders['non_delivered_ordered_units'] = (
        grouped_orders['ordered_quantity'] - grouped_orders['delivered_quantity']
    )
    grouped_orders = grouped_orders[
        [
            'distribution_channel_cod',
            'nfa_cod',
            'mat_cod',
            'to_dt',
            'non_delivered_ordered_units'
        ]
    ]

    orders_df = orders_df.merge(
        grouped_orders,
        on=['distribution_channel_cod', 'nfa_cod',
            'mat_cod', 'to_dt'],
        how='left',
        validate='many_to_one'
    )
    return orders_df


def add_non_delivered_nfa(orders_df: pd.DataFrame) -> pd.DataFrame:
    grouped_orders = (
        orders_df
        .groupby(
            ['distribution_channel_cod', 'nfa_cod', 'to_dt']
        )
        .agg({'ordered_quantity': 'sum', 'delivered_quantity': 'sum'})
        .reset_index()
        .sort_values('to_dt')
    )
    grouped_orders['non_delivered_nfa_ordered_units'] = (
        grouped_orders['ordered_quantity'] - grouped_orders['delivered_quantity']
    )

    grouped_orders = grouped_orders[
        [
            'distribution_channel_cod',
            'nfa_cod',
            'to_dt',
            'non_delivered_nfa_ordered_units'
        ]
    ]

    orders_df = orders_df.merge(
        grouped_orders,
        on=['distribution_channel_cod', 'nfa_cod', 'to_dt'],
        how='left',
        validate='many_to_one'
    )
    return orders_df
