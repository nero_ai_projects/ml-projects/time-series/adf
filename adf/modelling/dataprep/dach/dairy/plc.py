import pandas as pd  # type: ignore
from adf.modelling.tools import get_snowflake_df_spark
import logging
from adf.config import SNOWFLAKE_DATABASE


log = logging.getLogger("adf")

KEYWORD_MAPPING = {
    "dairy_at": "AT",
    "dairy_de": "DE",
    "dairy_ch": "CH"
}


def apply_sku_replacements(df: pd.DataFrame, division: str) -> pd.DataFrame:
    log.info("Applying sku replacement...")
    sku_replacement_mapping = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DTM_AFC",
        f"DACH_{KEYWORD_MAPPING[division]}_EDP_SKU_REP_MAP", "koalas"
    ).to_pandas()
    df = replace_skus(df, sku_replacement_mapping)
    return df


def replace_skus(df: pd.DataFrame, mapping: pd.DataFrame) -> pd.DataFrame:
    """ Sku replacement from mapping
    """
    sku_replace_dict = dict(zip(mapping['sku_code_origin'],
                                mapping['sku_code_dest']))
    df["mat_cod"] = df["mat_cod"].astype('int64')
    df["mat_cod"] = df["mat_cod"].replace(sku_replace_dict)
    return df.astype({"mat_cod": 'int64'})
