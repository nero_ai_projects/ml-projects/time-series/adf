import logging
import pandas as pd  # type: ignore
import numpy as np  # type: ignore
from functools import partial  # type: ignore
import databricks.koalas as ks  # type: ignore
from adf.errors import DataprepError
from adf.utils.provider import Provider
from adf.modelling.dataprep.dach.dairy.building_blocks import build_bb
from adf.modelling.dataprep.utils.promos import expand_promos_to_daily
from adf.utils.dataframe import memory_reduction
from adf.utils.decorators import timeit
import traceback

log = logging.getLogger("adf")
Provide = partial(Provider, mlrun=False, session=True, cluster=True)


def run(promo_df: pd.DataFrame,
        orders_df: pd.DataFrame):
    """ Build panel data for Dach Dairy_De
    """
    try:
        log.info("Building promo panel")
        panel_kdf, panel_only_promo_kdf = build_promo_data(
            promo_df,
            orders_df
        )
        log.info("Create building block table")
        bb_table = build_bb(panel_kdf)
        return build_uplifts_promo(panel_only_promo_kdf, bb_table)

    except Exception as err:
        traceback.print_exc()
        message = str(err.__class__.__name__) + ": " + str(err)
        raise DataprepError(message)


@timeit
def build_promo_data(promo_df: pd.DataFrame,
                     orders_df: pd.DataFrame):

    orders_df = orders_df \
        .groupby(['mat_cod', 'nfa_cod', 'to_dt'])['ordered_units'] \
        .sum() \
        .reset_index()

    log.info("Reducing memory of orders...")
    orders_df = memory_reduction(orders_df)
    log.info("Reducing memory of promos...")
    promo_df = memory_reduction(promo_df)

    log.info("Processing promos per mat_cod...")
    promo_no_promo_df = multiprocess_product_orders_with_promos(promo_df, orders_df)
    only_promo_df = promo_no_promo_df.loc[promo_no_promo_df['promotion_mechanics_cod'] != 0]
    return promo_no_promo_df, only_promo_df


def multiprocess_product_orders_with_promos(
        promo_df: pd.DataFrame,
        orders_df: pd.DataFrame):
    promo_df = ks.from_pandas(promo_df)
    start_date = promo_df['promotion_start_date_sellin']
    end_date = promo_df['promotion_end_date_sellin']
    mask = (start_date > end_date)
    promo_df = promo_df.loc[~mask]
    return promo_df.groupby("mat_cod").apply(process_product_promo(orders_df))


def process_product_promo(orders_df):
    def process_product_promo_on_group(
            product_promo_df
    ):
        mat_cod = list(product_promo_df.mat_cod.unique())[0]
        product_orders_df = orders_df.loc[orders_df['mat_cod'] == mat_cod]
        product_promo_df = product_promo_df.pipe(
            clean_promo
        ).pipe(
            expand_promos_to_daily, "promotion_start_date_sellin",
            "promotion_end_date_sellin", "to_dt"
        ).pipe(
            remove_sundays_promo
        ).pipe(
            deduplicates_several_meca_codes
        )
        product_orders_df = product_orders_df. \
            groupby(['mat_cod', 'nfa_cod', 'to_dt']). \
            sum(). \
            reset_index()

        product_promo_enriched_df = product_orders_df.merge(
            product_promo_df,
            on=['to_dt', 'nfa_cod', 'mat_cod'],
            how='right'
        )
        product_promo_enriched_df = product_promo_enriched_df.fillna(value=0)

        product_orders_enriched_df = product_orders_df.merge(
            product_promo_df,
            on=['to_dt', 'nfa_cod', 'mat_cod'],
            how='left'
        )
        product_orders_no_promo = product_orders_enriched_df[
            product_orders_enriched_df['promotion_mechanics_cod'].isnull()
        ]
        product_orders_no_promo.loc[:, "promotion_mechanics_cod"] = 0
        result_df = pd.concat(
            [product_orders_no_promo, product_promo_enriched_df],
            axis=0
        )
        result_df = result_df[[
            "mat_cod",
            "to_dt",
            "nfa_cod",
            "ordered_units",
            "promotion_mechanics_cod",
            "elapsed_time"
        ]]
        return result_df
    return process_product_promo_on_group


def clean_promo(promo_df: pd.DataFrame) -> pd.DataFrame:
    return promo_df[["mat_cod", "nfa_cod", "status_code", "promotion_mechanics_cod",
                     "promotion_start_date_sellin", "promotion_end_date_sellin",
                     "promotion_start_date_store", "promotion_end_date_store"]]


def deduplicates_several_meca_codes(df: pd.DataFrame) -> pd.DataFrame:
    """
    Remove multiple meca codes for a single product x location x date
    """
    df = df.sort_values('promotion_mechanics_cod', ascending=False) \
           .drop_duplicates(['mat_cod', 'to_dt', 'nfa_cod'], keep='first')
    return df


def remove_sundays_promo(promo_df: pd.DataFrame) -> pd.DataFrame:
    """
    Removes Sunday rows
    """
    promo_df["weekday"] = promo_df["to_dt"].dt.weekday
    promo_df = promo_df.loc[promo_df["weekday"] < 6]
    promo_df = promo_df.drop(columns=["weekday"], axis=1)
    return promo_df


def build_uplifts_promo(promo_dataprep: pd.DataFrame,
                        bb_table: pd.DataFrame) -> pd.DataFrame:
    """Merge promo dataframe with building block table containing uplifts on
    'mat_cod', 'plant_cod' and then sum all the uplifts for each dates.
    Parameters
    ----------
    promo_dataprep : pd.DataFrame
        promotion dataframe
    bb_table : pd.DataFrame
        building block uplift dataframe
    Returns
    -------
    pd.DataFrame
        promotion dataframe with total_uplifts column
    """
    promo_df = promo_dataprep.reset_index(drop=True).to_pandas()
    uplifts_df = promo_df.merge(
        bb_table,
        on=["mat_cod", "nfa_cod"],
        how="left"
    )
    uplifts_df["total_uplifts"] = uplifts_df.filter(regex="uplift").sum(axis=1)
    uplifts_df.loc[uplifts_df.to_dt.isnull(), 'total_uplifts'] = np.NaN
    uplifts_df = uplifts_df.groupby(
        ["mat_cod", "to_dt", "nfa_cod"]
    ).agg({"total_uplifts": "sum"}).reset_index()
    uplifts_df = uplifts_df.rename(columns={"to_dt": "to_dt"})
    return uplifts_df
