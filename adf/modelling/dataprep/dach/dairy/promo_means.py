import logging
import math
from datetime import timedelta
import pandas as pd  # type: ignore
import numpy as np  # type: ignore
from functools import partial  # type: ignore
import databricks.koalas as ks  # type: ignore
from adf.errors import DataprepError
from adf.utils.provider import Provider
from adf.utils.dataframe import memory_reduction
from adf.utils.decorators import timeit
from adf.modelling.dataprep.utils.promos import expand_promos_to_daily
import traceback


log = logging.getLogger("adf")
Provide = partial(Provider, mlrun=False, session=True, cluster=True)

HORIZONS = [0, 7, 14, 21, 28, 35, 42, 49, 56]

AGG_LEVELS = {
    "1": ["mat_cod", "nfa_cod", "promotion_mechanics_cod", "time_category", "promo_quantile"],
    "2": ["mat_cod", "nfa_cod", "promotion_mechanics_cod", "time_category"],
    "3": ["mat_cod", "nfa_cod", "time_category"],
    "4": ["mat_cod", "nfa_cod", "promotion_mechanics_cod"],
    "5": ["mat_cod", "promotion_mechanics_cod", "time_category", "promo_quantile"],
    "6": ["mat_cod", "promotion_mechanics_cod", "time_category"],
    "7": ["mat_cod", "time_category"],
    "8": ["mat_cod", "promotion_mechanics_cod"],
    "9": ["mat_cod"]
}


def run(promo_df: pd.DataFrame,
        orders_df: pd.DataFrame):
    """ Build panel data for DACH Dairy
    """
    try:
        return build_promo_data(
            promo_df,
            orders_df
        )
    except Exception as err:
        traceback.print_exc()
        message = str(err.__class__.__name__) + ": " + str(err)
        raise DataprepError(message)


@timeit
def build_promo_data(
    promo_df: pd.DataFrame,
    orders_df: pd.DataFrame
):

    orders_df = orders_df \
        .groupby(['mat_cod', 'nfa_cod', 'to_dt'])['ordered_units'] \
        .sum() \
        .reset_index()

    log.info("Reducing memory of orders...")
    orders_df = memory_reduction(orders_df)
    log.info("Reducing memory of promos...")
    promo_df = memory_reduction(promo_df)

    promo_df = multiprocess_product_orders_with_promos(promo_df, orders_df)
    promo_df = promo_df.rename(columns={"to_dt": "to_dt"})
    return promo_df.to_pandas()


def multiprocess_product_orders_with_promos(
        promo_df: pd.DataFrame,
        orders_df: pd.DataFrame):
    promo_df = ks.from_pandas(promo_df)
    start_date = promo_df.promotion_start_date_sellin
    end_date = promo_df.promotion_end_date_sellin
    mask = start_date > end_date
    promo_df = promo_df.loc[~mask]
    return promo_df.groupby("mat_cod").apply(process_product_promo(orders_df))


def process_product_promo(orders_df):
    def process_product_promo_on_group(
            product_promo_df
    ):
        mat_cod = list(product_promo_df.mat_cod.unique())[0]
        product_orders_df = orders_df.loc[orders_df.mat_cod == mat_cod]
        product_promo_df = product_promo_df.pipe(
            clean_promo
        ).apply(
            shift_franchises_start_sellin, axis=1
        ).pipe(
            expand_promos_to_daily, "promotion_start_date_sellin",
            "promotion_end_date_sellin", "to_dt"
        ).pipe(
            remove_sundays_promo
        ).pipe(
            deduplicates_several_meca_codes
        )
        product_orders_df = product_orders_df. \
            groupby(['mat_cod', 'nfa_cod', 'to_dt']). \
            sum(). \
            reset_index()

        product_promo_enriched_df = product_orders_df.merge(
            product_promo_df,
            on=['to_dt', 'nfa_cod', 'mat_cod'],
            how='right'
        )
        product_promo_enriched_df = product_promo_enriched_df.fillna(value=0)

        product_promo_enriched_df = add_promo_columns(product_promo_enriched_df)

        product_promo_enriched_df = product_promo_enriched_df[[
            "promotion_cod",
            "promotion_mechanics_cod",
            "mat_cod",
            "to_dt",
            "nfa_cod",
            "ordered_units",
            "time_category",
            "promo_quantile"
        ]]
        product_promo_enriched_df = compute_all_promo_means(product_promo_enriched_df)

        product_promo_enriched_df = product_promo_enriched_df[[
            "mat_cod",
            "to_dt",
            "nfa_cod"] + [f"promo_mean_horizon_{i}" for i in HORIZONS]]

        product_promo_enriched_df = product_promo_enriched_df.groupby([
            "mat_cod",
            "to_dt",
            "nfa_cod"
        ])[[f"promo_mean_horizon_{i}" for i in HORIZONS]].max().reset_index()
        log.info(f"Done processing {mat_cod}. "
                 f"""Still missing {
                     product_promo_enriched_df.promo_mean_horizon_0.isna().sum()} promo_means...""")
        return product_promo_enriched_df
    return process_product_promo_on_group


def shift_franchises_start_sellin(row: pd.Series, shift_mapping: dict = None) -> pd.Series:
    """ Shift promo start date sellin date according to different customers
    behaviours
    """
    if shift_mapping:
        if row["nfa_cod"] in shift_mapping.keys():
            nfa = row["nfa_cod"]
            row["promotion_start_date_sellin"] = \
                row["promotion_start_date_store"] + timedelta(days=shift_mapping[nfa][0])
            if len(shift_mapping[nfa]) > 1:
                row["promotion_end_date_sellin"] = \
                    row["promotion_start_date_store"] + timedelta(days=shift_mapping[nfa][1])
            else:
                row["promotion_end_date_sellin"] = row["promotion_end_date_store"]
    return row


def clean_promo(promo_df: pd.DataFrame):
    """Drop useless columns"""
    return promo_df[["mat_cod", "nfa_cod", "status_code",
                     "promotion_cod", "promotion_mechanics_cod",
                     "promotion_start_date_sellin", "promotion_end_date_sellin",
                     "promotion_start_date_store", "promotion_end_date_store"]]


def remove_sundays_promo(promo_df: pd.DataFrame) -> pd.DataFrame:
    """
    Removes Sunday rows
    """
    promo_df["weekday"] = promo_df["to_dt"].dt.weekday
    promo_df = promo_df.loc[promo_df["weekday"] < 6]
    promo_df = promo_df.drop(columns=["weekday"], axis=1)
    return promo_df


def deduplicates_several_meca_codes(df: pd.DataFrame):
    """
    Remove multiple meca codes for a single product x location x date
    """
    df = df.sort_values('promotion_mechanics_cod', ascending=False) \
           .drop_duplicates(['mat_cod', 'to_dt', 'nfa_cod'], keep='first')
    return df


def add_promo_columns(df: pd.DataFrame) -> pd.DataFrame:
    """Add utils promotion columns to be used for promo means calculation :
    quantile_days, time_category, promo_quantile
    """
    quantile_days = df.groupby(
        ["mat_cod", "nfa_cod", "promotion_cod"]
    ).elapsed_time.max().reset_index()
    quantile_days["quantile_days"] = quantile_days.elapsed_time.apply(
        lambda x: math.ceil(max(x, 1) / max(get_quantile(x), 1))
    )
    quantile_days["time_category"] = quantile_days.elapsed_time.apply(get_time_category)
    quantile_days = quantile_days.drop(columns=["elapsed_time"])

    df = df.merge(
        quantile_days, on=['nfa_cod', 'mat_cod', 'promotion_cod']
    )
    df["promo_quantile"] = (
        df.elapsed_time % df.quantile_days
    )
    return df


def get_time_category(x: int) -> int:
    """Bin the length of the promotion
    """
    if x < 7:
        return 0
    elif x < 14:
        return 1
    elif x < 21:
        return 2
    elif x < 42:
        return 3
    elif x < 84:
        return 4
    else:
        return 5


def get_quantile(x: int) -> int:
    """Calculate number of quantiles based on the length of the promotion
    """
    if x < 7:
        return 7
    elif x < 14:
        return 7
    elif x < 21:
        return 3
    elif x < 42:
        return 6
    elif x < 84:
        return 4
    else:
        return 4


def compute_all_promo_means(df: pd.DataFrame) -> pd.DataFrame:
    """Add promo_mean for each horizon
    """
    for horizon in HORIZONS:
        df = compute_promo_means(df, horizon)
    return df


def compute_promo_means(df: pd.DataFrame, horizon) -> pd.DataFrame:
    columns_to_keep = list(df.columns)
    df.loc[:, f"promo_mean_horizon_{horizon}"] = np.nan
    df = df.merge(
        df.groupby(
            ["nfa_cod", "mat_cod", "promotion_cod"]
        ).to_dt.min().reset_index().rename(
            columns={"to_dt": "min_promo_date"}
        ),
        on=["nfa_cod", "mat_cod", "promotion_cod"],
        how="left"
    )
    df["min_promo_date"] = df.min_promo_date - timedelta(days=horizon)
    df = df.sort_values("min_promo_date")

    for agg_level_number, agg_level_columns in AGG_LEVELS.items():
        if df[f"promo_mean_horizon_{horizon}"].isna().sum() == 0:
            break
        agg_level_df = df.groupby(["promotion_cod"] + agg_level_columns).agg(
            {"ordered_units": "mean", "to_dt": "min"}
        ).reset_index().rename(
            columns={"to_dt": "min_promo_date"}
        ).dropna(subset=["ordered_units"]).sort_values("min_promo_date")
        agg_level_df["ordered_units"] = agg_level_df.groupby(
            agg_level_columns
        )["ordered_units"].transform(
            lambda x: x.rolling(5, 1).mean()
        )
        df = pd.merge_asof(
            df,
            agg_level_df,
            by=agg_level_columns,
            on="min_promo_date",
            direction="backward",
            suffixes=(None, f"_{agg_level_number}"),
            allow_exact_matches=False
        )
        df[f"promo_mean_horizon_{horizon}"] = df[f"promo_mean_horizon_{horizon}"].fillna(
            df[f"ordered_units_{agg_level_number}"]
        )
    return df[columns_to_keep + [f"promo_mean_horizon_{horizon}"]]
