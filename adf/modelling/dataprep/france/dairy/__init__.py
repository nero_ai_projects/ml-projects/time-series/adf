"""

-----------
**Context**
-----------

The goal here is to deliver:

* short-term and mid-term weekly forecasts on a 14 weeks horizon

Results are sent to the IBP environment to be shared to France dairy Demand Planners.

NB : forecast is made on a weekly and daily basis, but must be disaggregated
to the daily level before being sent to IBP (98 days total).

Forecast granularities:

* predict volumes per ``SKU / RFA / Week``


Customer granularity specificities:

We can gather the RFAs in 3 groups of different importance:

* Group 1:

It is composed of the most important customers. For these customers we need to have a forecast at
the customers level. Sometimes a RFA can be associated to many warehouses.
There should be only 1 couple RFA/Warehouse
but sometimes if the usual warehouse is not available, the RFA's orders can be processed
by another warehouse.

*Group 2:

For this group of RFA we could function with a model at the warehouse level and split it at
the customer level.
RFA (warehouse 0102 formerly 0086 and warehouse 0514). In the first version, we will try to
forecast it at the customer level.

*Group 3:

Forecasts are not needed for this group of RFAs. To exclude after warehouse replacements
in history in case they replace another unavailable one provisioning some RFAs.

----------------
**Data Sources**
----------------

* Products Hierarchy
* Customers Hierarchy
* Holidays (API)
* Orders
* Promotions:
  * Promotions headers
  * Promotions transactions
  * Promotions history: Cas and EzyPromo
* IBP Master Data

NB: There are 2 sales organization in France Dairy: 0056 (vegetal dairy) and 0024 (regular dairy).
Although we generate
2 forecasts reports (run from 2 pipelines), the data sources are common. We will specify it when
the differentiating is needed.

------------------
** Mapping Files**
------------------

*customers_mapping : relation customer ibp code and cus_cod
*fictive_codes_mapping : list of SKUs to exclude
*ibp_cannibalization_mapping : list of SKUs that cannibalized each other.
*sku_replacement_mapping : SKUs that are being replaced by other (new and old codes)
*tiers_codes_mapping : list of SKUs to exclude
*mechanics_mapping : relation between promotion mechanics for CAS and Ezypromo (promotion tools)
*warehouse_rfa_replacement_mapping : relation between warehouses and RFAs and their corresponding
  prioritization


NB: Works for both sales organization code (0056 and 0024).
NB2: All these mapping files have a very low update frequency and some of
them should be replaced by PLC data. Contact the France EDP demand planners if updates are needed.

--------------------------------------
**Data Preparation Pipelines**
--------------------------------------

1. Products:
------------

Products hierarchy processing consist of the following steps:
- cleaning of the table by replacing null to np.NaN and formatting of the sales organization
code column into a 4 digits string.
- remove duplicates

2. Customers:
-------------

Customers hierarchy processing consist of the following steps:
- dropping null entries for either cus_cod or rfa_cod
- cast the cus_cod, rfa_cod and the distribution channel code into integers
- filter the sales organization code
- exclude specific customers for whom we do not need to forecast
- drop duplicated rows

3. Holidays:
------------

We use the calendar API to collect holidays and days off in France.
Holidays data is processed :
- prepare holidays to fit national granularity
- datetime updates
- add weekday column
- add the counts of holidays per week


4. Orders:
----------

Orders data are the most important data and mapping files are also needed to do the preparation.
The preparation performed are listed here:
- filter sales organization code after correctly formatting the field to a 4 digits string
- filter orders data by removing rejected orders, exclude customers, remove a few warehouses,
  remove unused codes with fictive codes and tiers code mapping files and exclude customers as
   it was done in customers table preparation
- A few processing steps:
  build the reference date column and ordered_units using the columns material availability
  and ordered quantity respectively.
  remove inconsistent orders (when order creation date is after material availability date).
  Do SKU replacement using a mapping file.
  Filter out SKUs in pause, innovations (less than 3 months of history) and long inactive ones.
  Add columns for futures orders.
  Remove unused columns.
- Merge the orders table with the customers table using the cus_cod
- update warehouses' code in orders table and filter the target group of RFAs to use
- aggregate the orders table at the location and product granularity
- update zdpl orders


5. SKU Panel construction:
--------------------------

A panel per product is built in parallelized manner merging products hierarchy table and
orders tables.
The latter was previously merged with customers hierarchy with mapping files.

The panel contains combination of sales org x location x product and dates till
the specified end date selected.

After merging products, orders and customers, we add the holidays data by merging on time column.

The last step of preparation consist of merging promotion data
(on customer x location x product x country and division) which have been prepared separately.
The promotion preparation pipeline is detailed in the section below.

-----------------------------------
**Promotion Preparation Pipelines**
-----------------------------------

Data preparation for promotion is run separately from the main dataprep as it is time consuming
and we only need to run it once a week after weekly promotions' source files refresh
every saturday.


5. Promotion history
--------------------

Promotion history is composed of the data from ancient tool CAS (1 file) and the history of
Ezypromo (1 file) before we start receiving live files.

These 2 files are loaded (promotions_cas and promotions respectively) and are unified using
the customer mapping file after being processed.

*CAS:
  * select only rows where status is in Accepté & Proposé
  * correct a few dates inconsistencies
  * filter out rows where date end store is before date start store
  * update mechanics to fit newer promotions files (Ezy)
  * drop duplicates

*Ezypromo history:
  * update price degradation column (to the right unit)
  * updates mechanics (fill empty to 0)

After processing the 2 files, they are merged (outer) and processed following the rules below:
* add promotion type id
* add location id
* correct product types
* aggregate promotion files by keeping those with the highest forecast addi and gross volume
* cleaning and creating mechanics


6. Promotion headers & transactions
-----------------------------------

Promotions' live files are composed of set of headers and transactions.
These 2 files are combined and are received every week.


*Promotion Preparation Pipeline*
================================

At the end of the promotion pipeline run, 3 reports should be produced:
* promotions means
* promotions uplifts
* promotions downlifts

But before these 3 reports can be produced, some preparation is needed using promotions
source files and orders data.

1. Initial preparation
---------------------

Build promotion panel data by merging promotion with orders data after performing
many processing step:
* merge with customer table
* expand promotion data to daily : between promotion start date sellin to end date sellin
* update mechanics
* ...

At the end of these processing steps, 2 tables are produced: ``orders_df`` and ``promo_df``

Promotion uplifts panel preparation:
------------------------------------

This step uses the ``promo_df`` and ``orders_df`` tables and is performed
in two steps:

* Aggregate the orders' volumes per product, customer (RFA) and date
* Process promotion data at product x RFA x date

    - Clean table
    - Expand promotions to a daily view
    - Remove promotion mechanic code duplicates (keep the first one).
    - Sum KPIs in the orders data per product, customer (RFA) and date.

* Perform a left merge between the product orders data and
  the cleaned promotions' table.
* All lines in the merged table not containing a promotion will have a
  mechanic code of 0.

2 reports are saved for the calculation of uplifts:

* promo_orders: panel with orders that have occurred within promotion period and without
* promo: panel with orders that have occurred within promotion period only

2. Promotion means
------------------

This step is performed using the ``promo_df`` and ``orders_df`` table.
The objective is that for a given day, taking into account the customers
(RFAs) in promotion and the type of promotion, to retroactively calculate
the rolling average of the orders carried out at different horizons
(last 7 occurrences, last 14, etc.).

Before computing the promotion means, many steps are performed:

Product processing for each product in the orders data set:
    * Clean promotions
    * Shift promotion dates
    * Expand promos to daily (ex: if the promotion lasts 4 days,
      we will have 4 lines)
    * For each product, date (requested delivery date) and RFA:

        - Remove promotion mechanic code duplicates (keep the first one).
        - Sum KPIs in the orders data
        - Add these KPIs to the product promotion table (merge promotions
          with the orders aggregated in the step before)
        - Compute quantiles and horizons: horizons represent the last 7,
          14, 21, etc. occurrences of the promotion
        - Compute the promotion rolling means per horizon
        - Select the maximum rolling mean for each product, date and RFA

3. Promotion uplifts
--------------------

This step uses the 2 reports saved earlier in the initial preparation step.

Creating the building blocks

To create the building blocks, we use the panel generated in the step before and compute an uplift
or a downlift given ``cannib``
is set to false or true respectively.

* We take a period of 2 years from the dataset, starting from the current date,
  backward
* For each product and each location, we compute the volumes associated with
  each promotion type (mechanic code) then we aggregate these volumes across all
  promotions (sum per product, RFA and date).
* We regularize volumes if there are sales for non-promo and promo at the
  same time.
* We create new columns with the mean of volume quantities sold for each
  mechanic code including O which is the no promotion.
  This mean will the same for each product and RFA for any date as it is
  computed over the two year period.
* We compute the uplifts/or downlift for each promotion type (excluding 0):

  * uplift calculation:
    ``uplift = (promo_period_mean / no_promo_period_mean) - 1``

  * downlift calculation:
    ``downlift = 1- (promo_period_mean / no_promo_period_mean) ``

  the period here is two years. Uplift/Downlift created here is for one product and one
  RFA.

Then, we have an uplift or downlift on base 100, and which is clipped to 0 if
the value is below 0.

4. Features from promotion transactions
---------------------------------------

In this last step, we will create some features using the promotion transactions'
table from BluePlanner.

* First we use the ``promo_df`` table to get the RFAs and promotion codes
* We then merge this table with the promotion transactions in order to add
  the RFA information
* We aggregate the predicted uplift and downlift for cannibalization and promotion
  dip per product, RFA and date. We use the sum for the uplift and the minimum for
  the downlift.

-----------------
**Preprocessing**
-----------------

Config files for dairy reg and dairy veg are in the config/flows/france/dairy folder.

-------------------------
**Training and Forecast**
-------------------------

Config files for dairy reg and dairy veg are in the config/flows/france/dairy folder.

For dairy reg Training is divided using filters on products and a specific training files for
one of these filters.
* AFH product (has it's own training configuration files -- ``train_afh.json``)
* High rotation products
* Low rotation products
* Lot products

NB: no filters are used for dairy veg

---------------------------------
**IBP export and Business rules**
---------------------------------

We generate 2 IBP reports for France-EDP customer, 1 for each sales organization code.

Business rules are applied for France EDP at the end of the process, before sending
forecasts to IBP platform.

In this part of the code, we use the uplift computed during data preparation to fill the
contribution of future promo on forecasts.

The building blocks promotion is the first thing computed in this part. Then we apply a few
rules to adjust forecast so that it suits best the Demand Planners' team.
Find below the rules for France EDP :
* All the forecasts for Lot products are in Promotion. We force baseline to 0 because these
  products are only sold during promotion
* When there are no promotions active, Promotion contribution are set to 0 for all products
* Forecasts are forced to 0 on sundays and during holidays
* Negative prediction are forced to 0

"""
