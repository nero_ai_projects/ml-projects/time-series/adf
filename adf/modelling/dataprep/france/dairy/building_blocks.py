import traceback
import logging
import numpy as np  # type: ignore
from adf.errors import BuildingBlocksError
from adf.utils.decorators import timeit
from datetime import date
from dateutil.relativedelta import relativedelta
from adf.modelling.dataprep.utils.promos import add_period_means

log = logging.getLogger("adf")


PROMOS_TYPE_COLS = list(np.arange(6))
PROMOS_UPLIFT_COLS = [f"{promo_cod}_uplift" for promo_cod in PROMOS_TYPE_COLS[1:]]


@timeit
def build_bb(promo_df, uplift: bool):
    """
    Builds promotion building block based on input promotion dataframe.
    It calculates an increment or loss of volumes due to promotion effect on the
    selected product for uplift, or the promotion effect on the cannibalizing product
    for downlift

    Parameters
    ----------
    promo_df : pd.DataFrame
        promotion dataframe
    uplift : bool
         whether to perform an uplift or a downlift calculation

    Returns
    -------
    pd.DataFrame
        table containing promotion effect per mat_cod x rfa_cod

    Raises
    ------
    BuildingBlocksError
    """
    try:
        log.info(f"PP type: {type(promo_df)}")
        promo_df = promo_df.pipe(scope_promo)
        promo_df = cast_and_sort_promo(promo_df)
        uplift_df = promo_df \
            .groupby(["mat_cod", "rfa_cod"], as_index=False) \
            .apply(add_uplift_per_location, uplift)
        uplift_df = uplift_df.dropna(subset=PROMOS_UPLIFT_COLS, how='all')
        float_cols = list(uplift_df.select_dtypes(include=['float64', 'float32']).columns)
        int_cols = list(uplift_df.select_dtypes(include=['int']).columns)
        uplift_df[float_cols] = uplift_df[float_cols].fillna(0.0)
        uplift_df[int_cols] = uplift_df[int_cols].fillna(0)
        uplift_df = aggregate_uplifts(uplift_df)

        return uplift_df

    except Exception as err:
        traceback.print_exc()
        message = str(err.__class__.__name__) + ": " + str(err)
        raise BuildingBlocksError(message)


@timeit
def scope_promo(promo_df):
    """Filter Promo Dataset on observation dataframe

    Parameters
    ----------
    promo_df : pd.DataFrame
        input promo dataframe at a daily level

    Returns
    -------
    pd.DataFrame
        scoped promo
    """
    today = date.today()
    start_date = (today + relativedelta(months=-48)).strftime("%Y-%m-%d")
    end_date = today.strftime("%Y-%m-%d")
    promo_df = promo_df.loc[promo_df.to_dt.between(start_date, end_date)]
    return promo_df


@timeit
def cast_and_sort_promo(promo_df):
    promo_df = promo_df[~promo_df['rfa_cod'].isnull()]
    promo_df = promo_df.astype(
        {
            "mat_cod": "int32",
            "rfa_cod": "int32",
            "ordered_units": "int32"
        })
    return promo_df.sort_values(by=["to_dt", "mat_cod", "rfa_cod"])


@timeit
def add_uplift_per_location(
        promo_df, uplift: bool
):
    """Compute uplift/ downlift calculation
        - build columns containing volumes associated to each promo types
        - regularizes volumes if there are sales for non-promo and promo the same time
        - calculate orders' means generated for each promo type on the period
        - calculate uplift or downlift for each promo type compared to a baseline

    Parameters
    ----------
    promo_df : pd.DataFrame
        promotion dataframe
    uplift : bool
        whether to perform an uplift or a downlift calculation

    Returns
    -------
    pd.DataFrame
        computed uplifts or downlifts
    """
    return promo_df \
        .pipe(create_uplift_table) \
        .pipe(regularize_volumes) \
        .pipe(add_period_means, 'mat_cod', 'rfa_cod', PROMOS_TYPE_COLS) \
        .pipe(compute_uplifts, uplift)


@timeit
def create_uplift_table(df_location):
    """Build columns containing volumes associated to each promo types

    Parameters
    ----------
    df_location : pd.DataFrame
        promo dataframe for one location

    Returns
    -------
    pd.DataFrame
        uplift dataframe containing volumes associated to each promo types
    """
    uplift_df = df_location.pivot_table(
        values="ordered_units",
        index=["rfa_cod", "to_dt", "mat_cod"],
        columns=["promotion_mechanics_cod"],
        aggfunc=np.sum
    ).reset_index()
    uplift_df = uplift_df.fillna(0)
    for col in PROMOS_UPLIFT_COLS:
        if col not in uplift_df.columns:
            uplift_df[col] = 0

    reindex_cols = ["rfa_cod", "to_dt", "mat_cod"] + PROMOS_TYPE_COLS
    uplift_df = uplift_df.reindex(columns=reindex_cols)

    uplift_df = uplift_df.groupby(
        ["rfa_cod", "to_dt", "mat_cod"]
    ).agg({el: "sum" for el in PROMOS_TYPE_COLS}).reset_index()
    return uplift_df


@timeit
def regularize_volumes(uplift_df):
    """
    Regularizes volumes if there are sales for non-promo and promo the same
    time
    """
    reg_cols = PROMOS_TYPE_COLS.copy()
    reg_cols.remove(0)
    for col in PROMOS_TYPE_COLS:
        uplift_df.loc[(uplift_df[col] > uplift_df[0]), col] = \
            uplift_df[0] + uplift_df[col]
        uplift_df.loc[(uplift_df[col] > uplift_df[0]), 0] = 0

        uplift_df.loc[(uplift_df[0] > uplift_df[col]), 0] = \
            uplift_df[0] + uplift_df[col]
        uplift_df.loc[(uplift_df[0] > uplift_df[col]), col] = 0

    return uplift_df


@timeit
def compute_uplifts(uplift_df, uplift: bool):
    """calculate uplift or downlift for each promo type compared to a baseline
    - if uplift == True, calculate the uplift of a promotion
    - if uplift == False, calculate the downlift generated by a cannibalizing product in promo

    Parameters
    ----------
    uplift_df : pd.DataFrame
        promotion dataframe with enriched columns
    uplift : bool
        whether to perform an uplift calculation or a downlift

    Returns
    -------
    pd.DataFrame
        promotion dataframe containing uplifts or downlifts cols
    """
    for col in PROMOS_UPLIFT_COLS:
        period_col = col.split("_", 1)[0] + "_period_mean"
        uplift_df[col] = uplift_df[period_col] / uplift_df["0_period_mean"]

    uplift_df = uplift_df.sort_values(by=["to_dt", "mat_cod"])

    if uplift:
        uplift_df[PROMOS_UPLIFT_COLS] = uplift_df[PROMOS_UPLIFT_COLS] - 1
    else:
        uplift_df[PROMOS_UPLIFT_COLS] = 1 - uplift_df[PROMOS_UPLIFT_COLS]

    uplift_df[PROMOS_UPLIFT_COLS] = uplift_df[PROMOS_UPLIFT_COLS].clip(0)
    return uplift_df


@timeit
def aggregate_uplifts(uplift_df):
    """ Aggregate uplift per 'mat_cod', 'rfa_cod', calculating the mean of each promo type
    """
    agg = {col: "mean" for col in PROMOS_UPLIFT_COLS}
    uplift_df = uplift_df.groupby(["mat_cod", "rfa_cod"]).agg(agg).reset_index()
    return uplift_df
