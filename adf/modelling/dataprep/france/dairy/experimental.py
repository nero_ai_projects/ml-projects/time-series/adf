from pyspark.sql import SparkSession
from adf import config
from adf.utils.credentials import get_secret
from pyspark.sql.functions import col
import databricks.koalas as ks  # type: ignore


def get_snowflake_spark_options(
    secret=None, warehouse=config.SNOWFLAKE_WAREHOUSE,
        database=config.SNOWFLAKE_DATABASE, schema=config.SNOWFLAKE_SCHEMA):
    if not secret:
        secret = get_secret(config.SNOWFLAKE_PWD_NAME)

    return {
        "sfUrl": 'danone.west-europe.privatelink.snowflakecomputing.com',
        "sfUser": config.SNOWFLAKE_USER,
        "sfPassword": secret,
        "sfDatabase": database,
        "sfSchema": schema,
        "sfWarehouse": warehouse
    }


def get_snowflake_df_spark(database, schema, source, **kwargs):
    spark = SparkSession.builder.getOrCreate()
    options = get_snowflake_spark_options(database=database, schema=schema, **kwargs)
    query = f'select * from "{database}"."{schema}"."{source}"'
    df = spark.read.format("snowflake").options(**options).option("query", query).load()
    df = df.select([col(column).alias(column.lower()) for column in df.columns])
    return ks.DataFrame(df)
