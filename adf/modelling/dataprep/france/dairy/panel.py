import datetime
import logging
import ast
import numpy as np  # type: ignore
import pandas as pd  # type: ignore
import json
import os
from datetime import date
from dateutil.relativedelta import relativedelta
from adf.errors import DataprepError
from adf.modelling import mllogs
from adf.services.database.query import update_report
from sqlalchemy import desc  # type: ignore
from adf.modelling.dataprep.utils.core import build_sub_panel_indexes
from adf.utils.dataframe import memory_reduction
from adf.modelling.dataprep.utils.holidays import (
    get_holidays_df,
    add_weekday_of_holidays,
    add_holidays_count_in_week
)
from adf.modelling.dataprep.utils.core import build_dataprep_report
from adf.modelling.tools import get_snowflake_df
from adf.modelling.dataprep.france.dairy.experimental import get_snowflake_df_spark
# FIXME: use get_snowflake_df_spark() from ma-spain-dairy branch instead

from adf.modelling.dataprep.utils.orders import add_future_orders_until_weekday_of_n_previous_weeks
from adf.utils.decorators import timeit, showcolumns
from adf.services.calendar.main import prepare_french_holidays_national
from adf.services.database import (
    PromoDataprepReports, BusinessUnits
)
import databricks.koalas as ks  # type: ignore
from adf import config


log = logging.getLogger("adf")


def cast_customer_ids(df):
    df = df.dropna(subset=['customer_sap_cod', 'plant_cod'], how='any')
    df['customer_sap_cod'] = df['customer_sap_cod'].astype('float64')
    df['customer_sap_cod'] = df['customer_sap_cod'].astype('int64')
    df['plant_cod'] = df['plant_cod'].astype('int64')
    return df


def run(
    country: str, division: str, product_level: str,
        location_level: str, **context):
    """ Builds panel data for France Dairy given product_level and location_level.
    :product_level: sku
    :location_level: warehouse, national
    :division: dairy_reg, dairy_veg
    Data is saved as parquet partitioned at location level
    """
    assert product_level == "sku"
    assert location_level in ("warehouse", "national", "departmental")
    assert division in ("dairy_reg", "dairy_veg")

    product_column = {
        "sku": "mat_cod"
    }[product_level]

    location_column = {
        "warehouse": "plant_cod",
        "national": "country_cod",
        "departmental": "rfa_cod"
    }[location_level]

    session = context["session"]
    ks.set_option('compute.default_index_type', 'distributed')

    report = build_dataprep_report(
        session,
        country,
        division,
        product_level,
        location_level,
        scope="panel",
        split_type="",
        split_code=None,
        prep_type="core",
        pipeline_id=context.get("pipeline_id", None)
    )

    mllogs.log_dataprep_report(report)

    try:
        log.info("Building panel")
        panel_df = build_panel_data(session, product_column, location_column, country, division)
        mllogs.log_shape(panel_df)

        log.info("Saving")
        report.upload(panel_df, partition_on=[location_column])
        update_report(session, report, "success")
        mllogs.tag_report_output(report)

    except Exception as err:
        message = str(err.__class__.__name__) + ": " + str(err)
        update_report(session, report, "failed", message)
        raise DataprepError(message)

    finally:
        mllogs.tag_report_output(report)

    return report, None


def build_panel_data(
        session,
        product_column: str, location_column: str,
        country: str, division: str
) -> ks.DataFrame:

    products_df = get_snowflake_df(
        config.SNOWFLAKE_DATABASE, 'VCP_DSP_AFC_EDP_FRC', 'R_SAP_EDP_FRA_PDT_HIE') \
        .pipe(process_products, product_column)
    # 39k rows

    fictive_codes_df = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, "VCP_DTM_AFC", "EDP_FRC_FIC_COD_MAP"
    )
    # 100 rows

    tiers_codes_df = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, 'VCP_DTM_AFC', "EDP_FRC_TRS_COD_MAP"
    )
    # 300 rows

    customers = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, 'VCP_DSP_AFC_EDP_FRC', 'D_SAP_EDP_FRA_DRY_CUS') \
        .pipe(prepare_customers, division)
    # 53k rows

    mapping_rfa_plt = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, 'VCP_DTM_AFC', 'EDP_FRC_WRH_RFA_REP_MAP'
    )
    # 700 rows

    sku_mapping = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, "VCP_DTM_AFC", "EDP_FRC_SKU_REP_MAP"
    )
    # 200 rows

    # (!) 98M rows
    orders_df = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, 'VCP_DSP_AFC_EDP_FRC', 'D_SAP_EDP_FRA_DRY_ORD')

    orders_df = orders_df \
        .dropna(
            subset=['material_availability_date', 'order_creation_date', 'req_delivery_date'],
            how='any') \
        .drop_duplicates() \
        .pipe(filter_sales_org_cod, division) \
        .pipe(filter_orders_data, fictive_codes_df, tiers_codes_df) \
        .pipe(process_orders_data, sku_mapping, product_column) \
        .pipe(merge_with_customers, customers, 'cus_cod') \
        .pipe(update_plant_cod, mapping_rfa_plt, division, None, [1, 2]) \
        .pipe(aggregate_orders_ks, product_column, location_column) \
        .pipe(correct_zdpl_orders, division, product_column, location_column)

    holidays_df = get_holidays_df(country, "holidays&dayoff") \
        .pipe(prepare_french_holidays_national) \
        .pipe(memory_reduction) \
        .pipe(update_holidays_datetime) \
        .pipe(add_weekday_of_holidays, "to_dt") \
        .pipe(add_holidays_count_in_week, "to_dt")

    holidays_df = ks.from_pandas(holidays_df)

    end_date = build_end_date(future_months_number=6, end_test_date=None)

    log.info("Building SKU panels")
    panel_dfs = multiprocess_product_panel(
        orders_df, products_df, product_column, location_column, end_date
    )

    return panel_dfs \
        .merge(ks.broadcast(holidays_df), on="to_dt", how="left") \
        .pipe(merge_promotions, session, country, division, product_column, location_column) \
        .reset_index(drop=True)


@timeit
@showcolumns
def multiprocess_product_panel(
        orders_df, products_df, product_column, location_column, end_date
) -> ks.DataFrame:
    return orders_df \
        .groupby(product_column, as_index=False) \
        .apply(build_product_panels, products_df, product_column, location_column, end_date)


# type: ignore
@timeit
def build_product_panels(
        df: pd.DataFrame, products_df, product_column, location_column, end_date
):
    """ Returns a panel_df for a given product
    """
    indexes = build_sub_panel_indexes(
        df,
        end_date,
        product_column,
        location_column,
        "to_dt",
        sales_organization_column="sales_organization_cod",
        object_type="pandas"
    )

    indexes["to_dt"] = indexes["to_dt"].astype("datetime64[ns]")
    df["to_dt"] = df["to_dt"].astype("datetime64[ns]")

    # (!) df in a pandas dataframe,
    # no external operations are allowed as it will trigger pickle errors
    # GroupBy.apply is considered slow, see
    # https://koalas.readthedocs.io/en/latest/reference/api/databricks.koalas.groupby.GroupBy.apply.html

    return df \
        .merge(indexes, on=list(indexes.columns), how="right") \
        .fillna(value={"ordered_units": 0}) \
        .pipe(merge_products, products_df, product_column) \
        .pipe(aggregate_panel, product_column, location_column)


@timeit
@showcolumns
def filter_sales_org_cod(df, division: str):
    """
    Filter orders data on the given division (dairy_reg or dairy_veg)
    """
    df = format_to_4_digits(df, "sales_organization_cod")
    df = df[df['sales_organization_cod'] == get_sales_organization_cod(division)]
    return df


@timeit
def get_sales_organization_cod(division: str) -> str:
    """
    Get the right sales organization cod according to the given division
    """
    sales_org_mapping = {"dairy_reg": '0024',
                         "dairy_veg": '0056'}
    return sales_org_mapping[division]


@timeit
def prepare_customers(df, division):
    df = df \
        .pipe(filter_customers) \
        .pipe(cast_customers) \
        .pipe(filter_sales_org_cod, division) \
        .pipe(exclude_specific_customer) \
        .drop_duplicates()
    return df


@timeit
def filter_customers(df):
    df['cus_cod'] = df['cus_cod'].apply(lambda x: pd.to_numeric(x, errors="coerce"))
    df['rfa_cod'] = df['rfa_cod'].apply(lambda x: pd.to_numeric(x, errors="coerce"))
    df = df[df['cus_cod'].notna() & df['rfa_cod'].notna()]
    return df


@timeit
def cast_customers(df):
    df['cus_cod'] = df['cus_cod'].astype(int)
    df['rfa_cod'] = df['rfa_cod'].astype(int)
    df['distribution_channel_cod'] = df['distribution_channel_cod'].astype(int)
    return df


@timeit
@showcolumns
def merge_with_customers(df, customers, on=None):
    df = df.dropna(subset=[on])
    customers = customers.dropna(subset=['cus_cod', 'rfa_cod'], how='any')
    df_cus = df.merge(customers[['cus_cod', 'rfa_cod']].drop_duplicates(), how='inner', on=on)
    return df_cus


@timeit
@showcolumns
def update_plant_cod(df, mapping_rfa_plt, division, keep_top=None, prio_is=None):
    if prio_is is None:
        prio_is = [1]

    mapping_rfa_plt = mapping_rfa_plt[mapping_rfa_plt['prio'].isin(prio_is)]
    if keep_top:
        mapping_rfa_plt = mapping_rfa_plt[:keep_top]
    if division == "dairy_reg":
        if "plant_cod" in list(df.columns):
            df = df.drop(['plant_cod'], axis=1)
        df = df.merge(ks.broadcast(mapping_rfa_plt[['plant_cod_target', 'rfa_cod']]),
                      on='rfa_cod', how='inner')
        df = df.rename(columns={'plant_cod_target': 'plant_cod'})
        df["plant_cod"] = df['plant_cod'].apply(lambda x: pd.to_numeric(x, errors="coerce"))
    return df


def map_sap_rfa(df, customers):
    # 30k rows
    ibp_sap = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, 'VCP_DTM_AFC', 'EDP_FRC_CUS_MAP'
    ) \
        .dropna(subset=['customer_ibp_cod', 'customer_sap_cod'], how='any') \
        .pipe(cast_customer_ids)
    ibp_sap = ibp_sap[['customer_ibp_cod', 'customer_sap_cod']].drop_duplicates()
    df_sap = df.merge(ibp_sap, how='inner', on='customer_ibp_cod') \
        .rename(columns={"customer_sap_cod": "cus_cod"})
    customers_red = customers[['cus_cod', 'rfa_cod']].drop_duplicates()
    return df_sap.merge(customers_red, how='inner', on='cus_cod') \
        .drop_duplicates()


@timeit
@showcolumns
def filter_orders_data(df, fictive_codes_df, tiers_codes_df):
    """
    Filter orders data:
    - remove rejected orders
    - filter on the right warehouses
    - remove unused codes
    - remove specific customers
    """

    df = df.pipe(remove_rejected_orders) \
           .pipe(filter_plant) \
           .pipe(remove_returns) \
           .pipe(remove_unused_codes, fictive_codes_df, tiers_codes_df) \
           .pipe(exclude_specific_customer)
    df = df[df['distribution_channel_cod'].isin([0, 1])]
    df = df.dropna(subset=["material_availability_date",
                           "order_creation_date"])
    return df


def remove_rejected_orders(df):
    """
    Keep only valid orders with no rejection code
    """
    return df.query('rejection_cod == "#"')


# Comment out this function at RFA level for prio 2 with AFH code
def filter_plant(df):
    """
    Keep only warehouses for our forecast scope:
    - plant cod from 77 to 86 included
    - plant cod 102 and 514
    """
    df['plant_cod'] = df['plant_cod'].map(lambda x: int(x) if x != '#' else 0)
    df = df[df['plant_cod'].isin(list(range(77, 87)) + [102, 514])]
    return df


def remove_returns(df):
    def get_prefix(x) -> str:
        return str(x)[:2]

    df['sal_doc_typ_cod_short'] = df['sal_doc_typ_cod'].apply(get_prefix)
    return df[~df.sal_doc_typ_cod.str.contains('^ZR')]


def remove_unused_codes(df, fictive_codes_df, tiers_codes_df):
    """
    Remove fictive codes and tiers codes except Alpro ones (cologistic) based
    on tables provided by the business
    """
    codes_to_exclude = list(set(fictive_codes_df['mat_cod'].to_numpy())) + list(
        set(tiers_codes_df['mat_cod'].to_numpy())
    )
    return df[~df.mat_cod.isin(codes_to_exclude)]


def exclude_specific_customer(df):
    """
    Exclude specific customer from dataframe
    """

    cus_to_exclude = [150033622, 150033817, 150051245, 150051476, 150059699]
    return df[~df.cus_cod.isin(cus_to_exclude)]


@timeit
def filter_pause_prod(df, product_column: str):
    """
        Replace the function filter_prod_scope_backtest when in production
        Remove only long pause product from the scope
    """
    today = date.today()
    start_date = (today + relativedelta(months=-12)).strftime("%Y-%m-%d")
    inno_limit = (today + relativedelta(days=-90)).strftime("%Y-%m-%d")

    with ks.option_context("compute.ops_on_diff_frames", True):
        df = df[(df['to_dt'] >= start_date) & (df['ordered_units'] > 0) &
                (df['delivered_quantity'] > 0)]
    df_scope = df[[product_column, 'to_dt']].groupby([product_column]).min()

    inno_prod = list(map(int, df_scope[df_scope['to_dt'] > inno_limit].index.to_numpy()))
    pause_prod = [131243, 131244, 131245, 145168, 146086, 146085]

    return df[(~df[product_column].isin(inno_prod)) & (~df[product_column].isin(pause_prod))]


def filter_prod_scope_backtest(
        df, start_date: str,
        end_date: str, product_column: str
):
    """
    Create the good product scope for the backtest:
    - keep only FU that are active during the backtest period
    - exclude FU considered as inno for the backtest period (less than 3 months
    of history from the start of the backtest period)
    - exclude FU with a long pause in orders (2 years without orders) but
    considered as active in the backtest period
    """
    active_prod = list(set(df.loc[(df['to_dt'] >= start_date) & (
        df['to_dt'] < end_date) & (df['ordered_units'] > 0) & (
            df['delivered_quantity'] > 0)][product_column]))
    backtest_limit = str(pd.Timestamp(start_date) - pd.Timedelta(days=90))[:10]
    df_scope = df.loc[df[product_column].isin(active_prod)]
    df_scope = df_scope.groupby(product_column)['to_dt'] \
                       .agg('min') \
                       .reset_index()
    inno_prod = list(set(
        df_scope[df_scope['to_dt'] > backtest_limit][product_column]))
    pause_prod = [131243, 131244, 131245, 145168, 146086, 146085]
    return df.query(f'({product_column}.isin({active_prod})) & \
                     ~({product_column}.isin({inno_prod})) & \
                     ~({product_column}.isin({pause_prod}))')


@timeit
@showcolumns
def process_orders_data(df, mapping, product_column: str):
    """
    Apply transformations on orders data:
    - add target columns (time and quantity)
    - update creation date
    - apply SKU replacement
    - filter on backtest scope
    - add future orders
    """
    df = build_target_dates_and_quantities(df, "to_dt", "ordered_units")
    df = update_creation_date(df)
    log.info("Apply SKU replacement")
    df = replace_skus(df, mapping)
    log.info("Filter products backtest scope")
    with ks.option_context("compute.ops_on_diff_frames", True):
        df = filter_pause_prod(df, product_column) \
            .pipe(add_future_orders_until_weekday_of_n_previous_weeks,
                  "to_dt", "order_creation_date", "ordered_units",
                  interval=range(0, 8), until_weekday_included=6)

    return df.drop(
        [
            "order_creation_date",
            "material_availability_date",
            "rejection_cod",
            "sal_cus_pur_doc_num",
            "sal_doc_num",
            "sal_doc_typ_cod",
            "order_reason_cod",
            "req_delivery_date",
            "ordered_quantity",
            "delivered_quantity",
            "doc_uom_cod",
            "sku_code_origin",
            "sku_code_dest"
        ],
        axis=1
    )


def format_to_4_digits(df, col_name: str):
    """
    Format numeric column to 4 digits (e.g. "24" -> "0024")
    """
    df[col_name] = df[col_name].apply(lambda x: str(x).zfill(4))
    return df


def build_target_dates_and_quantities(df, new_date_col: str, new_qty_col: str):
    """
    Create target columns for quantity and time from raw columns
    """
    df.loc[:, new_qty_col] = df.loc[:, "ordered_quantity"]
    df.loc[:, new_date_col] = df.loc[:, "material_availability_date"]
    df = df.dropna(subset=[new_date_col, new_qty_col], how="any")
    return df


def update_creation_date(df):
    """
    Update incorrect order creation dates (creation dates > availability date)
    """
    with ks.option_context("compute.ops_on_diff_frames", True):
        mask = df["order_creation_date"] > df["to_dt"]
        df.loc[mask, "order_creation_date"] = df.loc[mask, "to_dt"]
        return df


@timeit
@showcolumns
def aggregate_orders_ks(df, product_column: str, location_column: str):
    """
    Aggregate orders to forecast granularity: sales org, product, warehouse, day
    """
    df['distribution_channel_cod'] = df['distribution_channel_cod'].astype(int)
    col_to_sum = [col for col in df.columns if "ordered_units" in col]
    to_agg = {col: "sum" for col in col_to_sum}
    return df \
        .groupby(["sales_organization_cod", product_column, location_column, "to_dt"]) \
        .agg(to_agg) \
        .reset_index()


@timeit
def process_products(df, product_column: str):
    """
    Process product master data: format data, cast column to int and drop useless columns
    """
    df = format_products(df)
    df[product_column] = df[product_column].astype("int64")
    df = df.drop(['distribution_channel_cod', 'mat_start_validation_dat',
                  'mat_end_validation_dat', 'mat_creation_dat'], axis=1) \
        .drop_duplicates(['sales_organization_cod', product_column, 'mat_desc',
                          'mat_volume_value', 'mat_brand_desc'])
    return df


def format_products(df):
    """
    Format product master data: replace null values, format sales org to 4 digits
    """
    df = df.replace(to_replace=["#", "(null)"], value=np.NaN)
    df = format_to_4_digits(df, "sales_organization_cod")
    return df


def update_holidays_datetime(df):
    """
    Cast date from holidays dataframe to pandas datetime
    """
    df["to_dt"] = pd.to_datetime(df["to_dt"], format="%Y-%m-%d")
    return df.dropna(subset=["to_dt"])


def build_end_date(future_months_number: int,
                   end_test_date: str = None) -> pd.Timestamp:
    """
    Returns date which is 'future_months_number' months after current date
    """
    if end_test_date is not None:
        return pd.Timestamp(end_test_date)
    return pd.Timestamp(datetime.date.today() + relativedelta(
        months=future_months_number))


def merge_products(
        df: pd.DataFrame, products_df: pd.DataFrame, product_column: str
):
    """
    Merge products dataframe to main dataframe with orders
    """
    df[product_column] = df[product_column].astype('int64')
    return df.merge(
        products_df,
        on=["sales_organization_cod", product_column],
        how="left")


def aggregate_panel(panel_df: pd.DataFrame, product_column: str, location_column: str):
    """
    Aggregate main dataframe to forecast granularity: product, location, time
    """
    panel_df = panel_df.drop(["sales_organization_cod"], axis=1)
    agg = {k: "first" for k in panel_df.columns
           if k not in [product_column, location_column, "to_dt"]}
    agg.update({k: "sum" for k in panel_df.columns
                if "ordered_units" in k})

    return panel_df \
        .groupby([product_column, location_column, "to_dt"]) \
        .agg(agg) \
        .reset_index()


@timeit
def merge_promotions(
        df, session, country: str, division: str,
        product_column: str, location_column: str
):
    """
    Merges promo features into orders dataframe based specified arguments
    """
    product_level = {
        "mat_cod": "sku",
    }[product_column]

    location_level = {
        "plant_cod": "warehouse",
        "country_cod": "national",
        "rfa_cod": "departmental"
    }[location_column]

    business_unit = session.query(BusinessUnits)\
        .filter(BusinessUnits.country == country) \
        .filter(BusinessUnits.division == division) \
        .first()

    every_promo_dataprep = session.query(PromoDataprepReports) \
        .filter(PromoDataprepReports.business_unit_id == business_unit.id) \
        .filter(PromoDataprepReports.product_level == product_level) \
        .filter(PromoDataprepReports.location_level == location_level) \
        .filter(PromoDataprepReports.prep_type == "promotion") \
        .filter(PromoDataprepReports.status == "success") \
        .filter(PromoDataprepReports.updated_at.isnot(None))  # type: ignore

    promo_means_dataprep = every_promo_dataprep \
        .filter(PromoDataprepReports.scope == "promo_means") \
        .order_by(desc(PromoDataprepReports.updated_at)) \
        .first()

    promo_uplifts_dataprep = every_promo_dataprep \
        .filter(PromoDataprepReports.scope == "uplifts_promo") \
        .order_by(desc(PromoDataprepReports.updated_at)) \
        .first()

    promo_downlift_dataprep = every_promo_dataprep \
        .filter(PromoDataprepReports.scope == "downlifts_promo_canib") \
        .order_by(desc(PromoDataprepReports.updated_at)) \
        .first()

    try:
        promo_means_df = promo_means_dataprep.download(object_type="koalas")
        promo_means_cannib = build_rolling_means_cannib(promo_means_df, location_column)
    except Exception:
        promo_means_df = None
        promo_means_cannib = None
    # 6M rows

    try:
        promo_uplifts_df = promo_uplifts_dataprep.download(object_type="koalas")
    except Exception:
        promo_uplifts_df = None
    # 6M rows

    try:
        promo_downlift_df = promo_downlift_dataprep.download(object_type="koalas")
    except Exception:
        promo_downlift_df = None
    # 6M rows

    if promo_means_df is not None:
        df = df.merge(
            ks.broadcast(promo_means_df),
            on=[product_column, location_column, "to_dt"],
            how="left")

    if promo_downlift_df is not None:
        df = df.merge(
            ks.broadcast(promo_downlift_df),
            on=[product_column, location_column, "to_dt"],
            how="left")

    if promo_means_cannib is not None:
        df = df.merge(
            ks.broadcast(promo_means_cannib),
            on=[product_column, location_column, "to_dt"],
            how="left")

    if promo_uplifts_df is not None:
        df = df.merge(
            ks.broadcast(promo_uplifts_df),
            on=[product_column, location_column, "to_dt"],
            how="left")

    return df


# ToDo update this function when working at RFA level
@timeit
@showcolumns
def correct_zdpl_orders(df, division: str, product_column: str, location_column: str):
    """
    Correct ZDPL orders (i.e. fictive orders that offset each other over a few weeks but
    which have very high volumes (negative one week then positive a few weeks later)
    - Negative orders are set to zero
    - Positive orders are subtracted from the equivalent negative volume
    """
    if division == 'dairy_reg':
        filepath = os.path.join(os.path.dirname(__file__), 'zdpl_sku.json')
        with open(filepath, 'r') as file:
            zdpl = json.load(file)
        df = df.reset_index(drop=True)
        try:
            for el in zdpl:
                df['to_dt'] = df['to_dt'].mask(
                    (
                        df[product_column] == el['mat_cod']
                    ) & (
                        df['plant_cod'] == el['plant_cod']
                    ) & (
                        df['to_dt'] == el['date_to_zero']
                    ) & (
                        df['ordered_units'] < 0
                    ), df['to_dt'].apply(
                        lambda x: x + pd.Timedelta(
                            days=(
                                datetime.datetime.strptime(
                                    el['date_to_add'], '%Y-%m-%d'
                                ) - datetime.datetime.strptime(
                                    el['date_to_zero'], '%Y-%m-%d'
                                )
                            ).days
                        )
                    )
                )
                df = df.spark.checkpoint()
        except Exception as err:
            log.warning(err)
        col_to_sum = [col for col in df.columns if "ordered_units" in col]
        to_agg = {col: "sum" for col in col_to_sum}
        if "plant_cod" in df.columns:
            columns = [
                "sales_organization_cod", product_column, location_column, "plant_cod", "to_dt"
            ]
        else:
            columns = [
                "sales_organization_cod", product_column, location_column, "to_dt"
            ]
        df = df.groupby(columns).agg(to_agg).reset_index()
    return df


@timeit
def build_rolling_means_cannib(promo_means_df, location_column: str):
    """
    Create rolling means for cannibalization: associate to each cannibalized product
    ("FdR") the value of the rolling mean promo associated to the cannibalizing
    product ("Lot promo")
    """
    mapping_lot_fdr = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, "VCP_DTM_AFC", 'EDP_FRC_IBP_CNB_MAP'
    )
    # 200 rows
    mapping_lot_fdr = mapping_lot_fdr.dropna()
    mapping_lot_fdr = mapping_lot_fdr.rename(
        {'mat_cod_fdr': 'code_fdr', 'mat_cod_lot': 'code_lot'}, axis=1)
    dict_mapping = mapping_lot_fdr.set_index('code_lot').to_dict()['code_fdr']

    promo_means_df['mat_cod_fdr'] = promo_means_df['mat_cod'].map(dict_mapping)
    cols = [el for el in promo_means_df.columns if 'promo_mean_horizon_' in el]
    promo_means_cannib = promo_means_df.loc[~promo_means_df['mat_cod_fdr'].isnull()]
    promo_means_cannib['mat_cod_fdr'] = promo_means_cannib['mat_cod_fdr'].apply(ast.literal_eval)
    promo_means_cannib = promo_means_cannib \
        .explode('mat_cod_fdr') \
        .groupby(['mat_cod_fdr', 'to_dt', location_column]) \
        .mean()[cols] \
        .reset_index()
    cols_lot = [el + '_lot' for el in cols]
    promo_means_cannib.columns = ['mat_cod', 'to_dt', location_column] + cols_lot
    promo_means_cannib[cols_lot] = promo_means_cannib[cols_lot].fillna(0)
    return promo_means_cannib


def replace_skus(df, mapping):
    df = df \
        .merge(ks.broadcast(mapping), left_on='mat_cod', right_on='sku_code_origin', how='left') \
        .astype({'mat_cod': 'int64', 'sku_code_dest': 'int64'})

    with ks.option_context("compute.ops_on_diff_frames", True):
        df["mat_cod"] = df.apply(
            lambda row: row['sku_code_dest'] if row['sku_code_dest'] > 0 else row['mat_cod'], axis=1
        )
    df["mat_cod"] = df["mat_cod"].astype('int64')
    return df
