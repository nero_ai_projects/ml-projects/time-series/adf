import pandas as pd  # type: ignore
from adf.modelling.tools import get_snowflake_df
from adf.config import SNOWFLAKE_DATABASE
import logging


log = logging.getLogger("adf")


def apply_sku_replacements(df):
    sku_replacement_mapping = get_snowflake_df(
        SNOWFLAKE_DATABASE, "VCP_DTM_AFC", "EDP_FRC_SKU_REP_MAP"
    )
    df = replace_skus(df, sku_replacement_mapping)
    return df


def replace_skus(df: pd.DataFrame, mapping: pd.DataFrame) -> pd.DataFrame:
    """ Sku replacement from mapping
    """
    mapping['sku_code_origin'] = mapping['sku_code_origin'].astype('int64')
    mapping['sku_code_dest'] = mapping['sku_code_dest'].astype('int64')
    sku_replace_dict = dict(zip(mapping['sku_code_origin'],
                                mapping['sku_code_dest']))
    skus = set(list(df['mat_cod'].to_numpy()))
    for el in skus:
        if el not in sku_replace_dict.keys():
            sku_replace_dict[el] = el
    df["mat_cod"] = df["mat_cod"].astype("int64")
    df["mat_cod"] = df["mat_cod"].map(sku_replace_dict)
    return df


def get_ibp_master_data() -> pd.DataFrame:
    """
        Load IBP master data and apply transformation
    """
    df = get_snowflake_df(SNOWFLAKE_DATABASE, "VCP_DTM_AFC", "EDP_FRC_IBP_MD")
    mapping = {'024_D': 'code FDR actif',
               '024_E': 'code export',
               '024_F': 'code spé LIDL (ils sont arrêtés)',
               '024_I': 'code inno (FDR)',
               '024_L': 'code Lot',
               '024_N': 'code arrêté',
               '024_R': 'code spécifique AFH',
               '021_N': 'code arrêté'}
    df['forecast_class_desc'] = df['forecast_class']
    df['forecast_class_desc'] = df['forecast_class_desc'].replace(mapping)
    return df
