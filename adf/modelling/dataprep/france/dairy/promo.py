import logging
import datetime
import json
import numpy as np  # type: ignore
from functools import partial  # type: ignore
from typing import Union
from sqlalchemy import desc  # type: ignore
import databricks.koalas as ks  # type: ignore
from pyspark.sql import SparkSession
from adf.services.database.query import update_report
from adf.errors import DataprepError
from adf.modelling import mllogs
from adf.modelling.dataprep.france.dairy.building_blocks import build_bb
from adf.modelling.dataprep.france.dairy.promo_uplifts import (
    build_uplifts_promo, build_promo_data as build_promo_uplifts_data)
from adf.modelling.dataprep.france.dairy.promo_means import (
    build_promo_data as build_promo_means_data)
from adf.modelling.dataprep.france.dairy.panel import (
    process_orders_data, filter_orders_data, aggregate_orders_ks,
    correct_zdpl_orders, filter_sales_org_cod, prepare_customers,
    merge_with_customers, map_sap_rfa, update_plant_cod
)
from adf.modelling.dataprep.france.dairy.experimental import get_snowflake_df_spark
# FIXME: use get_snowflake_df_spark() from ma-spain-dairy branch instead
from adf.modelling.dataprep.france.dairy.plc import apply_sku_replacements
from adf.modelling.dataprep.utils.core import build_promo_dataprep_report
from adf.services.database import get_session, PromoDataprepReports
from adf.utils.provider import Provider
from adf.utils.decorators import timeit
from adf.utils.credentials import get_dbutils
import traceback
from adf import config
from adf.types import DataFrame

log = logging.getLogger("adf")
Provide = partial(Provider, mlrun=False, session=True, cluster=True)

SHIFT_MAPPING = {
    77: [0, -3],
    78: [0, -3],
    79: [0, -4],
    80: [0, -4],
    81: [0, -4],
    82: [-1, -4],
    83: [-1, -3],
    84: [-1, -3],
    85: [-1, -3]
}


def prepare_promo(
        country: str, division: str,
        product_level: str, location_level: str, **context
):
    """
    Builds panel data for France Dairy given product_level and location_level

    :product_level: sku
    :location_level: warehouse, departmental
    """
    spark = SparkSession.builder.getOrCreate()
    checkpoint_dir = (
        f"{config.MOUNT}/{config.FOLDER_CHECKPOINTS}/{country}/{division}/prepare_promo"
    )
    spark.sparkContext.setCheckpointDir(checkpoint_dir)
    orders_report, promo_report = None, None
    with ks.option_context('compute.default_index_type', 'distributed'):
        assert product_level == "sku"
        assert location_level in ("warehouse", "departmental")

        product_column = {
            "sku": "mat_cod",
        }[product_level]

        location_column = {
            "warehouse": "plant_cod",
            "national": "country_cod",
            "departmental": "rfa_cod"
        }[location_level]

        session = context["session"]
        pipeline_id = context.get("pipeline_id", None)
        orders_report = build_promo_dataprep_report(
            session, country, division, product_level, location_level,
            scope="orders_report", split_type="", split_code=None,
            pipeline_id=pipeline_id
        )
        promo_report = build_promo_dataprep_report(
            session, country, division, product_level, location_level,
            scope="promo_report", split_type="", split_code=None, pipeline_id=pipeline_id
        )
        mllogs.log_dataprep_report(promo_report)
        mllogs.log_dataprep_report(orders_report)
        try:
            orders_df, promo_df = build_orders_promo_panel(
                country, division, product_column, location_column
            )
            update_report(session, promo_report, "success")
            mllogs.tag_report_output(promo_report)
            promo_report.upload(promo_df)
            update_report(session, orders_report, "success")
            mllogs.tag_report_output(orders_report)
            orders_report.upload(orders_df)
        except Exception as err:
            traceback.print_exc()
            message = str(err.__class__.__name__) + ": " + str(err)
            update_report(session, orders_report, "failed", message)
            update_report(session, promo_report, "failed", message)
            raise DataprepError(message)
    get_dbutils(spark).fs.rm(checkpoint_dir, recurse=True)
    return orders_report, promo_report


def run(
        country: str, division: str,
        product_level: str, location_level: str,
        promo_report, orders_report,
        **context
):
    """
    Builds panel data for France Dairy given product_level and location_level

    :product_level: sku
    :location_level: warehouse, departmental
    """
    with ks.option_context('compute.default_index_type', 'distributed'):
        assert product_level == "sku"
        assert location_level in ("warehouse", "departmental")

        product_column = {
            "sku": "mat_cod",
        }[product_level]

        location_column = {
            "warehouse": "plant_cod",
            "national": "country_cod",
            "departmental": "rfa_cod"
        }[location_level]

        session = context["session"]
        pipeline_id = context.get("pipeline_id", None)
        report = build_promo_dataprep_report(
            session, country, division, product_level, location_level,
            scope="promo_orders", split_type="", split_code=None,
            pipeline_id=pipeline_id
        )
        only_promo_report = build_promo_dataprep_report(
            session, country, division, product_level, location_level,
            scope="promo", split_type="", split_code=None, pipeline_id=pipeline_id
        )
        mllogs.log_dataprep_report(report)
        mllogs.log_dataprep_report(only_promo_report)

        log.info("Building promo uplifts panel")
        orders_df = orders_report.download(object_type="koalas")
        orders_df["to_dt"] = ks.to_datetime(orders_df["to_dt"])
        promo_df = promo_report.download(object_type="koalas")
        promo_df["promotion_start_date_sellin"] = ks.to_datetime(
            promo_df["promotion_start_date_sellin"]
        )
        promo_df["promotion_end_date_sellin"] = ks.to_datetime(
            promo_df["promotion_end_date_sellin"]
        )

        try:
            panel_df, panel_only_promo_df = build_promo_uplifts_data(
                orders_df, promo_df, product_column, location_column)
            log.info("Saving")
            only_promo_report.upload(panel_only_promo_df.reset_index(drop=True))
            update_report(session, only_promo_report, "success")
            mllogs.tag_report_output(only_promo_report)
            report.upload(panel_df.reset_index(drop=True))
            update_report(session, report, "success")
            mllogs.tag_report_output(report)

        except Exception as err:
            traceback.print_exc()
            message = str(err.__class__.__name__) + ": " + str(err)
            update_report(session, report, "failed", message)
            update_report(session, only_promo_report, "failed", message)
            raise DataprepError(message)

        finally:
            mllogs.tag_report_output(report)
            mllogs.tag_report_output(only_promo_report)

        return only_promo_report, None


def promo_means(
        country: str, division: str, product_level: str,
        location_level: str, pipeline_id, promo_report, orders_report,
        **kwargs
):
    with ks.option_context('compute.default_index_type', 'distributed'):
        assert product_level == "sku"
        assert location_level in ("warehouse", "departmental")

        product_column = {
            "sku": "mat_cod",
        }[product_level]

        location_column = {
            "warehouse": "plant_cod",
            "national": "country_cod",
            "departmental": "rfa_cod"
        }[location_level]

        session = get_session()

        report = build_promo_dataprep_report(
            session,
            country,
            division,
            product_level,
            location_level,
            scope="promo_means",
            split_type="",
            split_code=None,
            pipeline_id=pipeline_id
        )
        mllogs.log_dataprep_report(report)
        log.info("Building promo means panel")

        orders_df = orders_report.download(object_type="koalas")
        orders_df["to_dt"] = ks.to_datetime(orders_df["to_dt"])
        promo_df = promo_report.download(object_type="koalas")
        promo_df["promotion_start_date_sellin"] = ks.to_datetime(
            promo_df["promotion_start_date_sellin"]
        )
        promo_df["promotion_end_date_sellin"] = ks.to_datetime(
            promo_df["promotion_end_date_sellin"]
        )
        try:
            promo_df = build_promo_means_data(
                promo_df, orders_df, product_column, location_column
            )
            log.info("Saving")
            report.upload(promo_df.reset_index(drop=True))
            update_report(session, report, "success")
            mllogs.tag_report_output(report)
        except Exception as err:
            traceback.print_exc()
            message = str(err.__class__.__name__) + ": " + str(err)
            raise DataprepError(message)
        return report


def uplifts_promo(
        country: str, division: str, product_level: str,
        location_level: str, cannib: bool, dataprep_report, pipeline_id, **context
):
    assert product_level == "sku"
    assert location_level in ("warehouse", "departmental")

    product_column = {
        "sku": "mat_cod",
    }[product_level]

    location_column = {
        "warehouse": "plant_cod",
        "national": "country_cod",
        "departmental": "rfa_cod"
    }[location_level]

    session = get_session()

    if cannib:
        scope = "downlifts_promo_canib"
    else:
        scope = "uplifts_promo"

    report = build_promo_dataprep_report(
        session,
        country,
        division,
        product_level,
        location_level,
        scope=scope,
        split_type="",
        split_code=None,
        pipeline_id=pipeline_id
    )
    mllogs.log_dataprep_report(report)

    log.info("Building uplift panel")
    # Specify division and country
    panel_df = load_dataprep_report_data(session, "promo_orders", 'koalas')
    panel_only_promo_df = dataprep_report.download(
        object_type="koalas"
    )
    log.info("Create building block table")
    bb_table = build_bb(panel_df, not cannib)
    df = build_uplifts_promo(panel_only_promo_df,
                             bb_table,
                             product_column,
                             location_column,
                             cannib)
    log.info("Saving")
    report.upload(df)
    update_report(session, report, "success")
    mllogs.tag_report_output(report)

    return report


def load_dataprep_report_data(session, scope: str, object_type='koalas'):
    df_report = session.query(PromoDataprepReports). \
        filter(PromoDataprepReports.status == "success"). \
        filter(PromoDataprepReports.scope == scope). \
        order_by(desc(PromoDataprepReports.updated_at)). \
        first()
    return df_report.download(object_type=object_type)


def build_orders_promo_panel(
        country: str, division: str, product_column: str, location_column: str
):
    """ Build orders and promo panel data for France Dairy for a given
    product_level and location_level in order to compute promo features
    """
    log.info("Loading orders...")
    fictive_codes_df = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, "VCP_DTM_AFC", "EDP_FRC_FIC_COD_MAP"
    )
    tiers_codes_df = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, 'VCP_DTM_AFC', "EDP_FRC_TRS_COD_MAP"
    )

    sku_mapping = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, "VCP_DTM_AFC", "EDP_FRC_SKU_REP_MAP"
    )

    orders_df = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, 'VCP_DSP_AFC_EDP_FRC', 'D_SAP_EDP_FRA_DRY_ORD') \
        .dropna(
            subset=['material_availability_date', 'order_creation_date', 'req_delivery_date'],
            how='any') \
        .drop_duplicates() \
        .pipe(filter_sales_org_cod, division) \
        .pipe(filter_orders_data, fictive_codes_df, tiers_codes_df) \
        .pipe(process_orders_data, sku_mapping, product_column)

    log.info("Loading promos...")
    mapping_cus_df = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, 'VCP_DTM_AFC', 'EDP_FRC_CUS_MAP'
    ) \
        .dropna(subset=['plant_cod']) \
        .pipe(cast_customer_ids)
    mapping_meca_df = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, 'VCP_DTM_AFC', 'EDP_FRC_MEC_MAP'
    )

    promo_cas = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, 'VCP_DSP_AFC_EDP_FRC', 'F_CAS_EDP_FRA_PRM_HIS') \
        .fillna(value={"promotion_mechanics_cod": 0}) \
        .dropna(subset=['plant_cod'])\
        .pipe(process_promo_cas, mapping_meca_df)

    promo_ezy = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, 'VCP_DSP_AFC_EDP_FRC', 'F_EZY_EDP_FRA_PRM_HIS'
    )

    promo_df = unify_promotions(promo_cas, promo_ezy, mapping_cus_df)

    promo_live_main = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, 'VCP_DSP_AFC_EDP_FRC', 'R_EZY_EDP_FRA_PRM_HEA') \
        .pipe(prepare_live_main_promo, mapping_cus_df)

    promo_live_volume = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, 'VCP_DSP_AFC_EDP_FRC', 'F_EZY_EDP_FRA_PRM_TRS'
    )
    promo_live_volume['week'] = ks.to_datetime(promo_live_volume['week'], errors='coerce')

    promo_live = merge_live_promo(promo_live_main, promo_live_volume)

    promo_df = ks.concat([promo_df, promo_live]) \
        .drop_duplicates([
            col for col in promo_df.columns
            if col != ['promotion_cod', 'promotion_mechanics_cod']
        ])

    if product_column == 'mat_cod':
        promo_df = apply_sku_replacements(promo_df)

    promo_df = promo_df.apply(shift_locations_start_sellin, axis=1)
    customers = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, 'VCP_DSP_AFC_EDP_FRC', 'D_SAP_EDP_FRA_DRY_CUS'
    )
    customers = prepare_customers(customers, division)
    orders_df = merge_with_customers(orders_df, customers, 'cus_cod')
    promo_df = map_sap_rfa(promo_df, customers)

    mapping_rfa_plt = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, 'VCP_DTM_AFC', 'EDP_FRC_WRH_RFA_REP_MAP'
    )
    orders_df = orders_df \
        .pipe(aggregate_orders_ks, product_column, location_column) \
        .pipe(update_plant_cod, mapping_rfa_plt, division, None, [1, 2]) \
        .pipe(correct_zdpl_orders, division, product_column, location_column) \
        .pipe(add_mapping_promo_lot_fdr, promo_df)
    orders_df["to_dt"] = ks.to_datetime(orders_df["to_dt"])

    promo_df["promotion_start_date_sellin"] = ks.to_datetime(
        promo_df["promotion_start_date_sellin"]
    )
    promo_df["promotion_end_date_sellin"] = ks.to_datetime(
        promo_df["promotion_end_date_sellin"]
    )
    return orders_df, promo_df


@timeit
def unify_promotions(
        df_cas: DataFrame, df_ezy: DataFrame, df_cus_ibp: DataFrame
) -> DataFrame:
    """
    Merge CAS and Ezy promo data, fill missing values for promotion type ID
    and location and handle duplicated promotions
    """
    df_ezy['price_degradation'] = df_ezy['price_degradation'].map(
        lambda x: str(int(np.sign(x) * x * 100)))
    df_ezy.loc[df_ezy[
        'promotion_mechanics_cod'].isnull(), 'promotion_mechanics_cod'] = 0
    df_promo = ks.concat([df_cas, df_ezy], join='outer')
    df_promo = add_promo_type_id(df_promo)
    df_promo = add_location_id(df_promo, df_cus_ibp)
    df_promo = correct_product_types(df_promo)
    df_promo = df_promo.drop(['std_price', 'week_ibp', 'mat_desc',
                              'customer_ibp_desc'], axis=1)
    df_promo = df_promo.pipe(aggregate_duplicate_promotions) \
        .pipe(clean_promo)

    return df_promo


@timeit
def prepare_live_main_promo(
        df_live: DataFrame, mapping_cus_df: DataFrame
) -> DataFrame:
    """
    Prepares live promo dataframe :
        - filter columns
        - add plant_cod and monday's date of promotion start
        - drop duplicates promos with different mechanics codes

    Parameters
    ----------
    df_live : DataFrame
        promo dataframe from live extracts
    mapping_cus_df : DataFrame
        map customer_ibp_cod and plant_cod

    Returns
    -------
    DataFrame
        prepared promo dataframe
    """
    df_live = df_live.dropna(subset=['promotion_mechanics_cod'])
    df_live = df_live[['promotion_cod', 'mat_cod', 'customer_ibp_cod',
                       'status',
                       'promotion_mechanics_cod',
                       'promotion_mechanics_desc',
                       'promotion_start_date_sellin',
                       'promotion_end_date_sellin',
                       'promotion_start_date_store',
                       'promotion_end_date_store',
                       'product_type',
                       'promotion_type']]
    df_live['monday_promotion_start_date_sellin'] = df_live['promotion_start_date_sellin'].apply(
        lambda x: x - datetime.timedelta(x.weekday())
    )
    df_live = df_live.pipe(add_location_id, mapping_cus_df) \
        .pipe(correct_product_types) \
        .pipe(drop_multiple_mecha_codes_for_same_promo)
    return df_live


@timeit
def drop_multiple_mecha_codes_for_same_promo(
        promo_live_main: DataFrame
) -> DataFrame:
    """
    Drops duplicate promotions with the same ['promotion_cod', 'mat_cod', 'customer_ibp_cod',
    'promotion_start_date_sellin', 'promotion_end_date_sellin'] but with different mechanics

    Parameters
    ----------
    promo_live_main : DataFrame
        live promo dataframe

    Returns
    -------
    DataFrame
        de-duplicated live promo dataframe
    """
    return promo_live_main.drop_duplicates(
        ['promotion_cod', 'mat_cod', 'customer_ibp_cod',
         'promotion_start_date_sellin', 'promotion_end_date_sellin']
    )


@timeit
def merge_live_promo(
        promo_live_main: DataFrame, promo_live_volume: DataFrame
) -> DataFrame:
    """Merge the two promo live files in order to deduplicate promotions.
    The prioritization is here done on the promotion generating the most forecast_addi_volume

    Parameters
    ----------
    promo_live_main : DataFrame
        prepared promo dataframe containing upcoming promotions with dates
    promo_live_volume : DataFrame
        volume promo dataframe containing predicted increment of promotions per week

    Returns
    -------
    DataFrame
        de-duplicated promo dataframe
    """
    promo_live_main['promotion_cod'] = promo_live_main['promotion_cod'].astype('int64')
    promo_live_volume['promotion_cod'] = promo_live_volume['promotion_cod'].astype('int64')
    df_promo = promo_live_main.merge(
        ks.broadcast(promo_live_volume[
            ['promotion_cod', 'mat_cod', 'customer_ibp_cod', 'week', 'forecast_addi_volume']
        ]),
        how='left',
        on=['promotion_cod', 'mat_cod', 'customer_ibp_cod']
    )
    df_promo = df_promo.query(
        'week >= monday_promotion_start_date_sellin and week <= promotion_end_date_sellin')
    df_promo = df_promo.drop('monday_promotion_start_date_sellin', axis=1)
    df_promo = df_promo.groupby(
        [col for col in df_promo
         if col not in ['week', 'forecast_addi_volume']]).forecast_addi_volume.sum() \
        .reset_index()
    df_promo = df_promo.pipe(aggregate_duplicate_promotions, live_data=True) \
        .pipe(clean_promo)
    df_promo = df_promo.replace(['FDR', 'LOT'], ['Fond de rayon', 'Lot Promo'])
    return df_promo


@timeit
def clean_promo(df: DataFrame) -> DataFrame:
    """ Type and filter promo columns
    """
    df['plant_cod'] = df['plant_cod'].astype('int64')
    df["promotion_mechanics_cod"] = df[
        "promotion_mechanics_desc"].apply(merge_promotion_meca_codes)
    df["promotion_mechanics_cod"] = df[
        "promotion_mechanics_cod"].astype('int64')
    return df[["mat_cod", "plant_cod", "customer_ibp_cod",
               "product_type", "promotion_mechanics_cod", "promotion_cod",
               "promotion_start_date_sellin", "promotion_end_date_sellin"]]


@timeit
def process_promo_cas(df: DataFrame,
                      df_mapping_meca: DataFrame) -> DataFrame:
    """
    Process CAS promotion data and harmonize format in order to fit Ezy promo
    promotion data structure
    """
    df = df.loc[df['status'].isin(['Accepté', 'Proposé'])]
    df = resolve_date_inconsistencies(df)
    df = df.loc[df['promotion_end_date_store'] >= df['promotion_start_date_store']]
    dict_map_meca = create_mapping_meca(df_mapping_meca)
    df = map_mechanics(df, dict_map_meca)
    df = map_promo_type(df)
    df["mat_cod"] = df["mat_cod"].mask(df['mat_cod'] == 'DECEMBRE N° 2 - 2018 - Alpro', 132216)
    df = df.drop_duplicates()
    return df


@timeit
def resolve_date_inconsistencies(
        df: DataFrame,
        start_date_col: str = 'promotion_start_date_store',
        end_date_col: str = 'promotion_end_date_store') -> DataFrame:
    df[end_date_col] = df[end_date_col].mask(
        (df[end_date_col] == '1900-01-01') & (df[start_date_col] == '2017-10-11'),
        ks.to_datetime('2017-10-21')
    ).mask(
        (df[end_date_col] == '2017-01-11') & (df[start_date_col] == '2017-01-20'),
        ks.to_datetime('2017-02-11')
    ).mask(
        (df[end_date_col] == '2018-05-15') & (df[start_date_col] == '2018-07-03'),
        ks.to_datetime('2018-07-15')
    ).mask(
        (df[end_date_col] == '2016-01-13'), ks.to_datetime('2019-01-13')
    ).mask(
        (df[end_date_col] == '2018-04-04') & (df[start_date_col] == '2018-06-22'),
        ks.to_datetime('2018-07-04')
    ).mask(
        (df[end_date_col] == '2018-09-14') & (df[start_date_col] == '2018-10-02'),
        ks.to_datetime('2018-10-14')
    ).mask(
        (df[end_date_col] == '2017-12-01') & (df[start_date_col] == '2017-12-06'),
        ks.to_datetime('2017-12-31')
    )
    df[start_date_col] = df[start_date_col].mask(
        (df[end_date_col] == '2018-08-11') & (df[start_date_col] == '2018-08-31'),
        ks.to_datetime('2018-07-31')
    ).mask(
        (df[end_date_col] == '2018-11-10') & (df[start_date_col] == '2018-11-30'),
        ks.to_datetime('2018-10-30')
    ).mask(
        (df[end_date_col] == '2019-05-05') & (df[start_date_col] == '2018-04-24'),
        ks.to_datetime('2019-04-24')
    ).mask(
        (df[end_date_col] == '2019-12-29') & (df[start_date_col] == '2019-09-17'),
        ks.to_datetime('2019-12-17')
    )
    return df


@timeit
def create_mapping_meca(df_map: DataFrame) -> dict:
    """
    Create the mapping dictionary between CAS and Ezy promo for promotion
    mechanics
    """

    def decode_nkfd(x):
        try:
            return x.encode('ascii').decode('utf-8')
        except Exception:
            return x

    df_map['mechanic_cas'] = df_map['mechanic_cas'].str.normalize('NFKD')
    df_map['mechanic_cas'] = df_map['mechanic_cas'].map(lambda x: decode_nkfd(x))
    df_map = df_map.replace({'mechanic_cas': 'RI /unit0,3'}, 'RI €/unit0,3') \
        .replace({'mechanic_cas': 'RI /unit10'}, 'RI €/unit10')
    map_promo_meca = df_map[['mechanic_cas', 'mechanic_ezy']] \
        .set_index('mechanic_cas') \
        .to_dict()['mechanic_ezy']
    map_promo_meca = {
        k: 'LV ' + v if (
            'Lot virtuel' in k
        ) or ('2+2' in k) or (
            'Lot 4 pour 3' in k
        ) or ('6 pour 4' in k) else v for k, v in map_promo_meca.items()
    }
    map_promo_meca['Fidélité28'] = map_promo_meca['Fidélité29']
    map_promo_meca['RI €/unit0'] = map_promo_meca['RI €/unit0,3']
    return map_promo_meca


@timeit
def map_mechanics(df: DataFrame, dict_map_meca: dict) -> DataFrame:
    """
    Map CAS mechanics into Ezy promo format
    """
    meca_to_update = [
        'Fidelite', 'Fidelite %', 'Fidelite % Progressive', 'RI', 'RI %', 'RI €/unit'
    ]
    df['std_meca'] = df['promotion_mechanics_desc']
    df['price_degradation'] = df['price_degradation'].map(
        lambda x: str(int(np.sign(x) * x * 100))
    )
    df["std_meca"] = df["std_meca"].mask(df['promotion_mechanics_desc'].isin(meca_to_update),
                                         df['promotion_mechanics_desc'] + df['price_degradation'])
    df['promotion_mechanics_desc'] = df['std_meca'].map(dict_map_meca)
    df = df.drop(['std_meca'], axis=1)
    return df


@timeit
def map_promo_type(df: DataFrame) -> DataFrame:
    """
    Map CAS promotion type into Ezy promo format
    """
    map_promo_type_dict = {
        'OC standard': 'OC Standard',
        'Magazine/carte': 'OP Magasine/Carte',
        'Tract National': 'Tract National',
        'Tract régional': 'Tract Regional',
        'Tract regional': 'Tract Regional',
        'OC nego': 'OC Nego',
        'OC négo': 'OC Nego'
    }

    df['promotion_type'] = df['promotion_type'].map(map_promo_type_dict)
    return df


@timeit
def add_promo_type_id(df: DataFrame) -> DataFrame:
    """
    Add promotion type ID when it's unknown (mostly Ezy promo data) based on
    history
    """
    map_promo_type_id = df.loc[~(df['promotion_type_id'].isnull())][
        ['promotion_type_id', 'promotion_type']] \
        .drop_duplicates() \
        .set_index('promotion_type') \
        .to_dict()['promotion_type_id']
    map_promo_type_id['OC Standard'] = 'STA'
    map_promo_type_id['Tract Regional'] = 'TRG'
    map_promo_type_id['OP Magasine/Carte'] = 'CAR'
    map_promo_type_id['Autres'] = 'MISC'
    map_promo_type_id['OP Magasin'] = 'MAG'
    df['promotion_type_id'] = df['promotion_type_id'].mask(
        df['promotion_type_id'].isnull(), df['promotion_type']
    ).map(map_promo_type_id)
    return df


@timeit
def add_location_id(df: DataFrame,
                    df_cus_ibp: DataFrame) -> DataFrame:
    """
    Add location ID when it's unknown (mostly Ezy promo data) based on history
    """
    map_cus_loc = df_cus_ibp[['customer_ibp_cod', 'plant_cod']] \
        .drop_duplicates() \
        .set_index('customer_ibp_cod') \
        .to_dict()['plant_cod']
    df['plant_cod'] = df['customer_ibp_cod'].map(map_cus_loc)
    df['mat_cod'] = df['mat_cod'].astype(int)
    return df


@timeit
def aggregate_duplicate_promotions(df: DataFrame, live_data: bool = False) -> DataFrame:
    """
    Handle duplicate promotions for a same product, at a same period, for a
    same customer.
    We keep the one with the highest forecast addi and gross volume
    """
    if live_data:
        prioritisation_key = ['forecast_addi_volume']
    else:
        prioritisation_key = ['forecast_addi_volume', 'forecast_gross_volume']

    indexes = [el for el in df.columns if el not in prioritisation_key]
    df = df.groupby(indexes).sum()[prioritisation_key].reset_index()
    indexes_for_unicity = ['status', 'mat_cod', 'promotion_start_date_sellin',
                           'product_type', 'customer_ibp_cod', 'plant_cod']
    df_unicity = df.sort_values(by=indexes_for_unicity + prioritisation_key,
                                ascending=False)
    df_unicity = df_unicity.drop_duplicates(subset=indexes_for_unicity,
                                            keep="first")
    return df_unicity


@timeit
def merge_promotion_meca_codes(promo_meca: str) -> int:
    """Merge mechanic codes based on their description

    Parameters
    ----------
    promo_meca : str
        description of the mechanic code

    Returns
    -------
    int
    """
    if promo_meca.startswith('LV'):
        return 1
    elif promo_meca.startswith('RI'):
        return 2
    elif promo_meca.startswith('FID'):
        return 3
    elif promo_meca.startswith('PRIX CHOC'):
        return 4
    else:
        return 5


@timeit
def correct_product_types(df: DataFrame) -> DataFrame:
    codes_fdr = [128278, 132234, 132236, 106399, 121453, 133222]
    codes_lot = [138218, 138217, 110790, 121425, 132239, 115026, 121313,
                 121314, 121315, 121428, 121429, 133124, 133126, 133131,
                 133133, 133134, 134909]
    df['product_type'] = df['product_type'].mask(df['mat_cod'].isin(codes_lot), "Lot Promo")
    df['product_type'] = df['product_type'].mask(df['mat_cod'].isin(codes_fdr), "Fond de rayon")
    return df


@timeit
def cast_customer_ids(df: DataFrame) -> DataFrame:
    df = df.dropna(subset=['customer_sap_cod', 'plant_cod'], how='any')
    df['customer_sap_cod'] = df['customer_sap_cod'].astype('float64')
    df['customer_sap_cod'] = df['customer_sap_cod'].astype('int64')
    df['plant_cod'] = df['plant_cod'].astype('int64')
    return df


def shift_locations_start_sellin(row):
    """Shifts promotion sellin start and end dates based on a
       shift mapping that depends on customers' behaviours.
       This methods applies to a promotions dataframe's row.
    """
    if row["plant_cod"] in SHIFT_MAPPING.keys():
        plant_cod = row["plant_cod"]
        row["promotion_start_date_sellin"] = row["promotion_start_date_sellin"].apply(
            lambda x: x + datetime.timedelta(days=SHIFT_MAPPING[plant_cod][0])
        )
        row["promotion_end_date_sellin"] = row["promotion_end_date_sellin"].apply(
            lambda x: x + datetime.timedelta(days=SHIFT_MAPPING[plant_cod][1])
        )
    return row


@timeit
def add_mapping_promo_lot_fdr(orders_df: DataFrame,
                              promo_df: DataFrame) -> Union[DataFrame, None]:
    """Adds a product type field to orders, from the one contained in the promo file

    Parameters
    ----------
    orders_df : DataFrame
        input orders dataframe
    promo_df : DataFrame
        input promo dataframe

    Returns
    -------
    Union[DataFrame, None]
        orders_df with product_type field, or unicity error

    Raises
    ------
    DataprepError
        raise error if a product is both fdr and lot_promo
    """
    promo_df_mapping = promo_df[
        promo_df['mat_cod'].isin(orders_df['mat_cod'].unique().to_numpy())
    ]
    promo_df_clean = promo_df_mapping[['mat_cod', 'product_type']].drop_duplicates()
    # To DO : check why there are int and string in product type
    # promo_df_clean = promo_df_clean.sort_values(by=['product_type']) \
    promo_df_clean = promo_df_clean.drop_duplicates(subset=['mat_cod'], keep='first')
    if promo_df_clean['mat_cod'].nunique() == len(promo_df_clean):
        mapping_lot_fdr = promo_df_clean[[
            'mat_cod', 'product_type']].set_index('mat_cod').to_dict()['product_type']
        orders_df['product_type'] = orders_df['mat_cod'].map(mapping_lot_fdr)
        return orders_df
    else:
        raise DataprepError('Mapping Lot Promo FdR not unique')


@timeit
def convert_to_fdr(
        promo_df: DataFrame
) -> DataFrame:
    """Create promotion dataprep based on cannibalisation mapping. Promotion fields correspond to
    lot_promo products, associated to fdr products through cannibalization mapping

    Parameters
    ----------
    promo_df : DataFrame
        input promo dataframe

    Returns
    -------
    DataFrame
        promotion file for FdR product with promo periods of associated lot promo
    """

    mapping_lot_fdr = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, 'VCP_DTM_AFC', 'EDP_FRC_IBP_CNB_MAP'
    )
    mapping_lot_fdr = mapping_lot_fdr.rename(columns={'mat_cod_fdr': 'code_fdr',
                                                      'mat_cod_lot': 'code_lot'})
    mapping_lot_fdr['code_fdr'] = mapping_lot_fdr['code_fdr'].apply(lambda x: json.loads(x))
    mapping_lot_fdr = mapping_lot_fdr.explode('code_fdr')
    mapping_lot_fdr['code_fdr'] = mapping_lot_fdr['code_fdr'].astype('int64')

    promo_df_code_lot = promo_df[
        promo_df['mat_cod'].isin(mapping_lot_fdr['code_lot'].unique())]
    promo_df_fdr_lot = promo_df_code_lot.rename(columns={'mat_cod': "code_lot"}) \
        .merge(ks.broadcast(mapping_lot_fdr),
               on='code_lot',
               how='inner') \
        .rename(columns={'code_fdr': 'mat_cod'}) \
        .drop_duplicates()
    promo_df_fdr_lot['product_type'] = 'Fond de rayon'

    return promo_df_fdr_lot
