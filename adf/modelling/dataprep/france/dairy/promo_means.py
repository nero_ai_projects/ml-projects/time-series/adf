import logging
import math
import pandas as pd  # type: ignore
import numpy as np  # type: ignore
from functools import partial  # type: ignore
from datetime import timedelta
from adf.utils.provider import Provider
from adf.utils.decorators import timeit
from adf.modelling.preprocessing.tools.aggregator import most_frequent_value
from adf.modelling.dataprep.utils.promos import expand_promos_to_daily
from adf.types import DataFrame


log = logging.getLogger("adf")
Provide = partial(Provider, mlrun=False, session=True, cluster=True)

HORIZONS = [0, 7, 14, 21, 28, 35, 42, 49, 56]


def cast_customer_ids(df: DataFrame) -> DataFrame:
    df = df.dropna(subset=['customer_sap_cod', 'plant_cod'], how='any')
    df['customer_sap_cod'] = df['customer_sap_cod'].astype('float64')
    df['customer_sap_cod'] = df['customer_sap_cod'].astype('int64')
    df['plant_cod'] = df['plant_cod'].astype('int64')
    return df


@timeit
def build_promo_data(
        promo_df: DataFrame, orders_df: DataFrame,
        product_column: str, location_column: str
):
    promo_df = promo_df[['mat_cod',
                         'rfa_cod',
                         'promotion_mechanics_cod',
                         'promotion_start_date_sellin',
                         'promotion_cod',
                         'promotion_end_date_sellin']]

    agg_levels = get_agg_levels(product_column, location_column)

    promo_df = multiprocess_product_orders_with_promos(
        promo_df, orders_df, product_column, location_column, agg_levels
    )

    return promo_df


def get_agg_levels(
        product_column: str, location_column: str
) -> dict:
    agg_levels = {
        "1": [product_column, location_column, "promotion_mechanics_cod",
              "time_category", "promo_quantile"],
        "2": [product_column, location_column, "promotion_mechanics_cod",
              "time_category"],
        "3": [product_column, location_column, "time_category"],
        "4": [product_column, location_column, "promotion_mechanics_cod"],
        "5": [product_column, "promotion_mechanics_cod", "time_category",
              "promo_quantile"],
        "6": [product_column, "promotion_mechanics_cod", "time_category"],
        "7": [product_column, "time_category"],
        "8": [product_column, "promotion_mechanics_cod"],
        "9": [product_column]
    }
    return agg_levels


@timeit
def multiprocess_product_orders_with_promos(
        promo_df: DataFrame, orders_df: DataFrame, product_column: str,
        location_column: str, agg_levels: dict
):
    orders_df[product_column] = orders_df[product_column].astype(int)
    orders_df[location_column] = orders_df[location_column].astype(int)
    promo_df[product_column] = promo_df[product_column].astype(int)
    promo_df[location_column] = promo_df[location_column].astype(int)
    promo_df["promotion_mechanics_cod"] = promo_df["promotion_mechanics_cod"].astype(int)
    promo_df['promotion_cod'] = promo_df['promotion_cod'].astype(str)
    orders_df = orders_df.to_pandas()
    resd = promo_df.groupby(product_column, as_index=False).apply(
        process_product_promo, orders_df, product_column, location_column, agg_levels
    )
    return resd


@timeit
def process_product_promo(
    product_promo_df: DataFrame, orders_df: DataFrame,
    product_column: str, location_column: str, agg_levels: dict
):
    prod_cod = list(product_promo_df.mat_cod.unique())[0]
    start_date = product_promo_df.promotion_start_date_sellin
    end_date = product_promo_df.promotion_end_date_sellin
    mask = (start_date > end_date)
    product_promo_df = product_promo_df.loc[~mask]
    product_orders_df = orders_df.loc[orders_df[product_column] == prod_cod]

    product_promo_df = product_promo_df.pipe(
        clean_promo, product_column, location_column
    ).pipe(
        expand_promos_to_daily, 'promotion_start_date_sellin',
        'promotion_end_date_sellin', 'to_dt'
    ).pipe(
        drop_daily_duplicates_promo, product_column, location_column
    )
    product_orders_df = product_orders_df. \
        groupby([product_column, location_column, 'to_dt']). \
        sum(). \
        reset_index()

    product_promo_enriched_df = product_orders_df.merge(
        product_promo_df,
        on=[product_column, location_column, 'to_dt'],
        how='right'
    )
    product_promo_enriched_df = product_promo_enriched_df.fillna(value=0)

    product_promo_enriched_df = add_promo_columns(
        product_promo_enriched_df, product_column, location_column)

    product_promo_enriched_df = product_promo_enriched_df[[
        "promotion_cod",
        product_column,
        "to_dt",
        location_column,
        "ordered_units",
        "promotion_mechanics_cod",
        "time_category",
        "promo_quantile"
    ]]

    product_promo_enriched_df["promotion_mechanics_cod"] = product_promo_enriched_df[
        "promotion_mechanics_cod"].astype('int64')

    product_promo_enriched_df = compute_all_promo_means(
        product_promo_enriched_df, agg_levels, product_column, location_column, HORIZONS)

    product_promo_enriched_df = product_promo_enriched_df[[
        product_column,
        "to_dt",
        location_column] + [f"promo_mean_horizon_{i}" for i in HORIZONS]]

    product_promo_enriched_df = product_promo_enriched_df.groupby([
        product_column,
        "to_dt",
        location_column
    ])[[f"promo_mean_horizon_{i}" for i in HORIZONS]].max().reset_index()
    log.info(
        f"Done processing {prod_cod}. "
        f"""Still missing {
            product_promo_enriched_df.promo_mean_horizon_0.isna().sum()
        } promo_means...""")
    return product_promo_enriched_df


@timeit
def clean_promo(
        promo_df: DataFrame, product_column: str, location_column: str
):
    promo_df = promo_df.drop_duplicates(
        [
            product_column, location_column, "promotion_mechanics_cod",
            "promotion_start_date_sellin",
            "promotion_end_date_sellin"
        ],
        keep="first"
    )
    promo_df = promo_df.groupby(
        [product_column, location_column,
         'promotion_start_date_sellin', 'promotion_end_date_sellin'],
        as_index=False
    ).apply(
        lambda group: group.loc[
            group.promotion_mechanics_cod == most_frequent_value(
                group.promotion_mechanics_cod)
        ]
    )
    promo_df = promo_df.groupby(
        [product_column, location_column,
         'promotion_mechanics_cod', 'promotion_start_date_sellin'],
        as_index=False
    ).apply(
        lambda group: group.loc[
            group.promotion_end_date_sellin == group.promotion_end_date_sellin.max()
        ]
    )
    promo_df = promo_df.groupby(
        [product_column, location_column,
         'promotion_mechanics_cod', 'promotion_end_date_sellin'],
        as_index=False
    ).apply(
        lambda group: group.loc[
            group.promotion_start_date_sellin == group.promotion_start_date_sellin.min()
        ]
    )
    return promo_df


@timeit
def aggregate_orders_at_rfa_cod(
        orders_df: DataFrame, product_column: str, location_column: str
) -> DataFrame:
    orders_df = orders_df.groupby([product_column,
                                   'to_dt',
                                   location_column])[['ordered_units']].sum() \
                         .reset_index()
    return orders_df


@timeit
def drop_daily_duplicates_promo(
        promo_df: DataFrame, product_column: str, location_column: str
) -> DataFrame:
    """Drop duplicates in daily promo dataframe based on the greater elapsed_time

    Parameters
    ----------
    promo_df : DataFrame
        input promo dataframe at a daily level
    product_column : str
        target product column. value must be "mat_cod"
    location_column: str
        target location column.

    Returns
    -------
    DataFrame
        de-duplicated promo dataframe at a daily level
    """
    promo_df = promo_df.groupby(
        [product_column, location_column, 'to_dt', 'promotion_mechanics_cod'],
        as_index=False
    ).apply(lambda group: group.loc[group.elapsed_time == group.elapsed_time.max()])
    promo_df = promo_df.drop_duplicates([
        product_column, location_column, 'to_dt', 'elapsed_time'])
    return promo_df


@timeit
def add_promo_columns(
        df: DataFrame, product_column: str, location_column: str
):
    quantile_days = df.groupby(
        [product_column, location_column, "promotion_cod"]
    ).elapsed_time.max().reset_index()
    quantile_days["quantile_days"] = quantile_days.elapsed_time.apply(
        lambda x: math.ceil(max(x, 1) / max(get_quantile(x), 1))
    )
    quantile_days["time_category"] = quantile_days.elapsed_time.apply(get_time_category)
    quantile_days = quantile_days.drop(columns=["elapsed_time"])

    df = df.merge(
        quantile_days, on=[location_column, product_column, 'promotion_cod']
    )
    df["promo_quantile"] = (
        df.elapsed_time % df.quantile_days
    )
    return df


def get_time_category(x):
    if x < 7:
        return 0
    elif x < 14:
        return 1
    elif x < 21:
        return 2
    elif x < 42:
        return 3
    elif x < 84:
        return 4
    else:
        return 5


def get_quantile(x):
    if x < 7:
        return 7
    elif x < 14:
        return 7
    elif x < 21:
        return 3
    elif x < 42:
        return 6
    elif x < 84:
        return 4
    else:
        return 4


@timeit
def compute_all_promo_means(
        df: DataFrame, agg_levels: dict, product_column: str,
        location_column: str, horizons
) -> DataFrame:
    for horizon in horizons:
        df = compute_promo_means(df, horizon, agg_levels, product_column, location_column)
    return df


@timeit
def compute_promo_means(
        df: DataFrame, horizon: int, agg_levels: dict,
        product_column: str, location_column: str
) -> DataFrame:
    columns_to_keep = list(df.columns)
    df[f"promo_mean_horizon_{horizon}"] = np.nan
    df = df.merge(
        df.groupby(
            [product_column, location_column, "promotion_cod"]
        ).to_dt.min().reset_index().rename(
            columns={"to_dt": "min_promo_date"}
        ),
        on=[product_column, location_column, "promotion_cod"],
        how="left"
    )
    df["min_promo_date"] = df.min_promo_date - timedelta(days=horizon)
    df = df.sort_values("min_promo_date")

    for agg_level_number, agg_level_columns in agg_levels.items():
        if df[f"promo_mean_horizon_{horizon}"].isna().sum() == 0:
            break
        agg_level_df = df.groupby(["promotion_cod"] + agg_level_columns).agg(
            {"ordered_units": "mean", "to_dt": "min"}
        ).reset_index().rename(
            columns={"to_dt": "min_promo_date"}
        ).dropna(subset=["ordered_units"]).sort_values("min_promo_date")
        agg_level_df["ordered_units"] = agg_level_df.groupby(
            agg_level_columns
        )["ordered_units"].transform(
            lambda x: x.rolling(5, 1).mean()
        )
        agg_level_df[product_column] = agg_level_df[product_column] \
            .astype('int64')
        df[product_column] = df[product_column].astype('int64')
        df[location_column] = df[location_column].astype('int64')
        if location_column in agg_level_df.columns:
            agg_level_df[location_column] = agg_level_df[location_column].astype('int64')
        df = pd.merge_asof(
            df,
            agg_level_df,
            by=agg_level_columns,
            on="min_promo_date",
            direction="backward",
            suffixes=(None, f"_{agg_level_number}"),
            allow_exact_matches=False
        )
        df[f"promo_mean_horizon_{horizon}"] = df[f"promo_mean_horizon_{horizon}"].fillna(
            df[f"ordered_units_{agg_level_number}"]
        )
    return df[columns_to_keep + [f"promo_mean_horizon_{horizon}"]]
