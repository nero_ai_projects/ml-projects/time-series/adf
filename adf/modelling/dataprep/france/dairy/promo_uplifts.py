from typing import Tuple
import logging
import pandas as pd  # type: ignore
from functools import partial  # type: ignore
import databricks.koalas as ks  # type: ignore
from adf.utils.provider import Provider
from adf.utils.decorators import timeit
from adf.types import DataFrame

from adf.modelling.dataprep.utils.promos import expand_promos_to_daily

log = logging.getLogger("adf")
Provide = partial(Provider, mlrun=False, session=True, cluster=True)


@timeit
def build_promo_data(
        orders_df: DataFrame, promo_df: DataFrame,
        product_column: str, location_column: str
) -> Tuple[DataFrame, DataFrame]:

    promo_df = promo_df[['mat_cod',
                         'rfa_cod',
                         'promotion_mechanics_cod',
                         'promotion_start_date_sellin',
                         'promotion_cod',
                         'promotion_end_date_sellin']]

    log.info("Processing promos per product ...")
    promo_no_promo_df = multiprocess_product_orders_with_promos(
        promo_df,
        orders_df,
        product_column,
        location_column
    )
    only_promo_df = promo_no_promo_df.loc[
        promo_no_promo_df["promotion_mechanics_cod"] != 0
    ]
    return promo_no_promo_df, only_promo_df


@timeit
def multiprocess_product_orders_with_promos(
        promo_df: DataFrame, orders_df: DataFrame,
        product_column: str, location_column: str
):
    orders_df[product_column] = orders_df[product_column].astype(int)
    orders_df[location_column] = orders_df[location_column].astype(int)
    promo_df[product_column] = promo_df[product_column].astype(int)
    promo_df[location_column] = promo_df[location_column].astype(int)
    promo_df["promotion_mechanics_cod"] = promo_df["promotion_mechanics_cod"].astype(int)
    promo_df['promotion_cod'] = promo_df['promotion_cod'].astype(str)
    orders_df = orders_df.to_pandas()
    resd = promo_df.groupby(product_column, as_index=False).apply(
        process_product_promo, orders_df, product_column, location_column
    )
    return resd


# TODO: See if there is a need to discriminate promotions
@timeit
def process_product_promo(
    product_promo_df: DataFrame, orders_df: DataFrame, product_column: str, location_column: str
):
    start_date = product_promo_df['promotion_start_date_sellin']
    end_date = product_promo_df['promotion_end_date_sellin']
    mask = (start_date > end_date)
    product_promo_df = product_promo_df.loc[~mask]
    product_orders_df = orders_df.loc[
        orders_df[product_column].isin(product_promo_df.mat_cod.unique())
    ]
    product_promo_df = product_promo_df.pipe(
        clean_promo, product_column, location_column
    )
    product_promo_df = product_promo_df.pipe(
        expand_promos_to_daily, 'promotion_start_date_sellin',
        'promotion_end_date_sellin', 'to_dt'
    ).pipe(remove_sundays_promo)
    product_orders_df = product_orders_df. \
        groupby([product_column, location_column, 'to_dt']). \
        sum(). \
        reset_index()
    product_promo_enriched_df = product_orders_df.merge(
        product_promo_df,
        on=[product_column, location_column, 'to_dt'],
        how='right'
    )
    product_promo_enriched_df = product_promo_enriched_df.fillna(value=0)
    product_orders_enriched_df = product_orders_df.merge(
        product_promo_df,
        on=[product_column, location_column, 'to_dt'],
        how='left'
    )
    product_orders_no_promo = product_orders_enriched_df[
        product_orders_enriched_df.promotion_mechanics_cod.isnull()
    ]
    product_orders_no_promo["promotion_mechanics_cod"] = 0
    result_df = pd.concat(
        [product_orders_no_promo, product_promo_enriched_df],
        axis=0
    )
    result_df = result_df[[
        product_column,
        "to_dt",
        location_column,
        "ordered_units",
        "promotion_mechanics_cod",
        "elapsed_time"
    ]]
    return result_df


@timeit
def clean_promo(
        promo_df: DataFrame, product_column: str, location_column: str
):
    promo_df = promo_df.drop_duplicates(
        [product_column, location_column, "promotion_mechanics_cod",
         "promotion_start_date_sellin", "promotion_end_date_sellin"],
        keep="first"
    )
    return promo_df


@timeit
def remove_sundays_promo(
        promo_df: DataFrame
) -> DataFrame:
    """
    Removes Sunday rows
    """
    promo_df["weekday"] = promo_df["to_dt"].dt.weekday
    promo_df = promo_df.loc[promo_df.weekday < 6]
    promo_df = promo_df.drop(columns=["weekday"], axis=1)
    return promo_df


@timeit
def build_uplifts_promo(
        promo_dataprep: DataFrame, bb_table: DataFrame,
        product_column: str, location_column: str, cannib: bool
) -> DataFrame:
    """Merge promo dataframe with building block table containing uplifts on
    'mat_cod', 'plant_cod' and then sum all the uplifts for each dates.
    Similar computation with cannibalisation building block table

    Parameters
    ----------
    promo_dataprep : DataFrame
        promotion dataframe
    bb_table : DataFrame
        building block uplift or downlift cannibalization dataframe
    product_column : str
        target product column. value must be "mat_cod"
    location_column : str
        target product column. value must be "plant_cod"  or "rfa_cod"
    cannib : bool
        if True, this aggregation phase concerns cannibalisation downlifts

    Returns
    -------
    DataFrame
        promotion dataframe with total_uplifts (total_downlift_cannib) column
    """
    with ks.option_context("compute.ops_on_diff_frames", True):
        promo_df = promo_dataprep.reset_index()
        uplifts_df = promo_df.merge(
            ks.broadcast(bb_table),
            on=[product_column, location_column],
            how="left"
        )
        uplifts_df["total_uplifts"] = uplifts_df.filter(regex="uplift").sum(axis=1)
        uplifts_df = uplifts_df.groupby(
            [product_column, location_column, "to_dt"]
        ).agg({"total_uplifts": "sum"}).reset_index()
        if cannib:
            uplifts_df = uplifts_df.rename(columns={"total_uplifts":
                                                    "total_downlift_cannib"})
        return uplifts_df
