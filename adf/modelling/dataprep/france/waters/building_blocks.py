import traceback
import logging
import pandas as pd  # type: ignore
import numpy as np  # type: ignore
from datetime import date
from typing import Union, List, Dict, Callable
from dateutil.relativedelta import relativedelta
import databricks.koalas as ks  # type: ignore
from adf.errors import BuildingBlocksError
from adf.utils.mappings import get_mappings, save_mappings
from adf.utils.decorators import timeit
from adf.modelling.dataprep.utils.core import build_building_blocks_report
from adf.services.database.query import update_report


log = logging.getLogger("adf")


@timeit
def build_bb(
        promos_kdf: ks.DataFrame, compute_market_shares: bool, **context
) -> pd.DataFrame:
    session = context["session"]
    pipeline_id = context["pipeline_id"]
    dataprep_id = context["dataprep_id"]

    bb_report = build_building_blocks_report(
        session,
        "france",
        "waters",
        scope="promo_building_block",
        dataprep_id=dataprep_id,
        pipeline_id=pipeline_id
    )

    try:
        promos_df = promos_kdf \
            .pipe(scope_promo, "to_dt") \
            .pipe(
                cast_and_sort_promo,
                sorting_indexes=["to_dt", "fu_cod", "rfa_cod"],
                dtypes={
                    "fu_cod": "int32",
                    "rfa_cod": "int32",
                    "ordered_volumes": "float"
                }
            )
        if compute_market_shares:
            ratios = compute_ratios(
                promos_df, franchise_column="rfa_cod", product_column="fu_cod",
                mechanics_column="promotion_mechanics_desc",
                no_promo_mechanic_value="no_promo", target_qty="ordered_volumes") \
                .reset_index(drop=True)
            save_mappings(ratios, "france", "waters", "market_share_ratios")
        else:
            ratios = get_mappings("france", "waters", "market_share_ratios", object_type="koalas")

        promos_type_cols = list(set(promos_df["promotion_mechanics_desc"].to_numpy()))
        promos_uplift_cols = [k + "_uplift" for k in promos_type_cols if "no_promo" not in k]

        uplift_df = promos_df \
            .groupby(["fu_cod", "rfa_cod"], as_index=False)\
            .apply(add_uplift(promos_type_cols, promos_uplift_cols)) \
            .reset_index(drop=True) \
            .pipe(add_market_shares, ratios, promos_uplift_cols) \
            .dropna(subset=promos_uplift_cols, how='all')

        float_cols = list(uplift_df.select_dtypes(include=['float64', 'float32']).columns)
        int_cols = list(uplift_df.select_dtypes(include=['int']).columns)
        uplift_df[float_cols] = uplift_df[float_cols].fillna(0.0)
        uplift_df[int_cols] = uplift_df[int_cols].fillna(0)

        # NB: here we switch back to pandas df to avoid huge latencies due to Spark partitioning
        uplift_df = uplift_df \
            .to_pandas() \
            .pipe(add_total_uplift_increment, promos_uplift_cols) \
            .pipe(normalize_uplifts, promos_uplift_cols) \
            .pipe(aggregate_uplifts, promos_uplift_cols)

        bb_report.upload(uplift_df)
        update_report(session, bb_report, "success")

        return uplift_df

    except Exception as err:
        traceback.print_exc()
        message = str(err.__class__.__name__) + ": " + str(err)
        update_report(session, bb_report, "failed", message)
        raise BuildingBlocksError(message)


def scope_promo(
        df: ks.DataFrame, date_column: str
) -> ks.DataFrame:
    """
    Returns filtered promos dataframe based on temporal scope
    """
    today = date.today()
    start_date = (today + relativedelta(months=-24)).strftime("%Y-%m-%d")
    end_date = today.strftime("%Y-%m-%d")
    return df.loc[df[date_column].between(start_date, end_date)]


def cast_and_sort_promo(
        df: ks.DataFrame, sorting_indexes: List[str], dtypes: Dict[str, str]
) -> ks.DataFrame:
    """
    Cast types of input dataframe based on dtypes input mapping and sort based on sorting
    indexes
    """
    df = df.astype(dtypes)
    return df.sort_values(by=sorting_indexes)


def compute_ratios(
        promos_df: ks.DataFrame, franchise_column: str, product_column: str, mechanics_column: str,
        no_promo_mechanic_value: Union[int, str], target_qty: str
) -> ks.DataFrame:
    """
    For each franchise, computes the volume market shares in "no_promo" period
    """
    total_volume_no_promo = promos_df \
        .loc[promos_df[mechanics_column] == no_promo_mechanic_value] \
        .groupby(product_column, as_index=False)[target_qty] \
        .sum() \
        .rename(columns={target_qty: "total_no_promo"}) \
        .to_pandas()

    return promos_df \
        .groupby([franchise_column, product_column], as_index=False) \
        .apply(compute_ratios_per_franchise(
            total_volume_no_promo, franchise_column, product_column,
            mechanics_column, no_promo_mechanic_value, target_qty))


def compute_ratios_per_franchise(
        total_volume_no_promo: pd.DataFrame,
        franchise_column: str,
        product_column: str,
        mechanics_column: str,
        no_promo_mechanic_value: Union[int, str],
        target_qty: str
) -> Callable:
    def compute_ratios_per_franchise_on_group(
            promos_df: pd.DataFrame
    ):
        promos_df = promos_df.reset_index(drop=True)
        franchise = list(promos_df[franchise_column].unique())[0]
        franchise_no_promo = promos_df \
            .loc[promos_df[mechanics_column] == no_promo_mechanic_value] \
            .groupby(product_column, as_index=False)[target_qty] \
            .sum() \
            .rename(columns={target_qty: "franchise_no_promo"})
        no_promo = franchise_no_promo \
            .merge(total_volume_no_promo, on=product_column, how="left")
        no_promo["ratio"] = no_promo.franchise_no_promo / no_promo.total_no_promo
        no_promo[franchise_column] = franchise
        return no_promo[[franchise_column, product_column, "ratio"]].astype(
            {
                franchise_column: "int32",
                product_column: "int32",
                "ratio": "float"
            }
        )
    return compute_ratios_per_franchise_on_group


def add_uplift(
        promos_type_cols: List[str], promos_uplift_cols: List[str]
) -> Callable:
    def add_uplift_on_group(
            promos_df: pd.DataFrame
    ):
        return promos_df \
            .reset_index(drop=True) \
            .pipe(create_uplift_table, promos_type_cols, promos_uplift_cols) \
            .pipe(regularize_volumes, promos_type_cols) \
            .pipe(add_annual_means, promos_type_cols) \
            .pipe(compute_uplifts, promos_type_cols, promos_uplift_cols)
    return add_uplift_on_group


def create_uplift_table(
        promos_df: pd.DataFrame, promos_type_cols: List[str], promos_uplift_cols: List[str]
) -> pd.DataFrame:
    uplift_df = pd \
        .pivot_table(
            promos_df,
            values="ordered_volumes",
            index=["rfa_cod", "to_dt", "fu_cod"],
            columns=["promotion_mechanics_desc"],
            aggfunc=np.sum) \
        .reset_index()
    uplift_df = uplift_df.fillna(0)
    for col in promos_uplift_cols:
        if col not in uplift_df.columns:
            uplift_df[col] = 0

    reindex_cols = ["rfa_cod", "to_dt", "fu_cod"] + promos_type_cols
    uplift_df = uplift_df.reindex(columns=reindex_cols)
    return uplift_df \
        .groupby(["rfa_cod", "to_dt", "fu_cod"]) \
        .agg({el: "sum" for el in promos_type_cols}) \
        .reset_index()


def regularize_volumes(
        uplift_df: pd.DataFrame, promos_type_cols: List[str]
) -> pd.DataFrame:
    """
    Regularizes volumes if there are sales for non-promo and promo the same time
    """
    reg_cols = promos_type_cols.copy()
    non_promo_value = reg_cols[0]
    reg_cols.remove(non_promo_value)
    for col in reg_cols:
        uplift_df.loc[(uplift_df[col] > uplift_df[non_promo_value]), col] = \
            uplift_df[non_promo_value] + uplift_df[col]
        uplift_df.loc[(uplift_df[col] > uplift_df[non_promo_value]), non_promo_value] = 0

        uplift_df.loc[(uplift_df[non_promo_value] > uplift_df[col]), non_promo_value] = \
            uplift_df[non_promo_value] + uplift_df[col]
        uplift_df.loc[(uplift_df[non_promo_value] > uplift_df[col]), col] = 0
    return uplift_df


def add_annual_means(
        uplift_df: pd.DataFrame, promos_type_cols: List[str]
) -> pd.DataFrame:
    for column in promos_type_cols:
        mean_df = uplift_df.loc[uplift_df[column] > 0]
        mean_df = mean_df \
            .groupby(["fu_cod", "rfa_cod"])[column] \
            .mean() \
            .reset_index()
        mean_df = mean_df.rename(columns=({column: f"{column}_annual_mean"}))
        uplift_df = uplift_df.merge(
            mean_df,
            on=["fu_cod", "rfa_cod"],
            how="left"
        )
    return uplift_df


def compute_uplifts(
        uplift_df: pd.DataFrame, promos_type_cols: List[str], promos_uplift_cols: List[str]
) -> pd.DataFrame:
    non_promo_value = promos_type_cols[0]
    for col in promos_uplift_cols:
        annual_col = "_".join(col.split("_")[:-1]) + "_annual_mean"
        uplift_df[col] = uplift_df[annual_col] / uplift_df[str(non_promo_value) + "_annual_mean"]
    uplift_df = uplift_df.sort_values(by=["to_dt", "fu_cod"])
    uplift_df[promos_uplift_cols] = uplift_df[promos_uplift_cols] - 1
    uplift_df[promos_uplift_cols] = uplift_df[promos_uplift_cols].clip(0)
    return uplift_df


def add_market_shares(
        uplift_df: ks.DataFrame, ratios: pd.DataFrame, promos_uplift_cols: List[str]
) -> ks.DataFrame:
    uplift_df = uplift_df.merge(
        ratios,
        on=["fu_cod", "rfa_cod"],
        how="left"
    )
    for col in promos_uplift_cols:
        uplift_df[col] = uplift_df[col] * uplift_df["ratio"]
    uplift_df = uplift_df.drop(columns=["ratio"])
    return uplift_df


def add_total_uplift_increment(
        uplift_df: pd.DataFrame, promos_uplift_cols: List[str]
) -> pd.DataFrame:
    for col in promos_uplift_cols:
        increment_df = uplift_df \
            .groupby(["to_dt", "fu_cod"], as_index=False)[col] \
            .sum()
        increment_df = increment_df.rename(columns={col: f"{col}_total_increment"})
        uplift_df = uplift_df.merge(
            increment_df,
            on=["to_dt", "fu_cod"],
            how="left"
        )
    return uplift_df


def normalize_uplifts(
        uplift_df: pd.DataFrame, promos_uplift_cols: List[str]
) -> pd.DataFrame:
    normalizer = float(0)
    tot_increments = uplift_df.filter(regex=".total_increment*").columns
    for col in tot_increments:
        normalizer += uplift_df[col]
    normalizer = 1 + (normalizer / len(tot_increments))
    for col in promos_uplift_cols:
        uplift_df[col] = uplift_df[col] / normalizer
    return uplift_df


def aggregate_uplifts(
        uplift_df: pd.DataFrame, promos_uplift_cols: List[str]
) -> pd.DataFrame:
    agg = {col: "mean" for col in promos_uplift_cols}
    return uplift_df \
        .groupby(["fu_cod", "rfa_cod"]) \
        .agg(agg) \
        .reset_index()
