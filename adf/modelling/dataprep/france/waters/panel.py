import logging
from typing import List, Union, Callable, Dict, Tuple
import numpy as np  # type: ignore
import pandas as pd  # type: ignore
from datetime import date, timedelta
from sqlalchemy import desc  # type: ignore
from sqlalchemy.orm import Session  # type: ignore
import databricks.koalas as ks  # type: ignore
from adf import config
from adf.services import get_session
from adf.services.database import (
    BusinessUnits,
    BuildingBlocksReports
)
from adf.services.database.query.reports import update_report
from adf.utils.decorators import timeit
from adf.errors import DataprepError
from adf.modelling import mllogs
from adf.modelling.tools import get_snowflake_df
from adf.modelling.dataprep.france.waters.promo import run as run_promos_dataprep
from adf.modelling.dataprep.france.waters.sellout import run as process_and_export_sellout
from adf.modelling.dataprep.utils.core import (
    build_sub_panel_indexes,
    build_end_date,
    safe_merge,
    build_dataprep_report
)
from adf.utils.dataframe import memory_reduction
from adf.modelling.dataprep.utils.holidays import (
    get_holidays_df,
    update_holidays_datetime,
    add_weekday_of_holidays,
    add_holidays_count_in_week,
    add_holidays_count_in_month
)
from adf.modelling.dataprep.utils.iri import aggregate_iri_data
from adf.modelling.dataprep.utils.orders import (
    add_future_orders_of_n_previous_days,
    add_future_orders_until_weekday_of_n_previous_weeks
)
from adf.modelling.dataprep.utils.weather import (
    process_weather_data,
    aggregate_weather
)
from adf.modelling.dataprep.utils.seasonality import add_days_before_end_of_month
from adf.modelling.preprocessing.tools.aggregator import most_frequent_value
from adf.services.calendar.main import prepare_french_holidays_national

log = logging.getLogger("adf")


def run(
        country: str, division: str, product_level: str, location_level: str, **context
):
    """
    Builds panel/innovation data for France Waters given country, division, product_level and
    location_level.

    NB: Here country must be "france" and division must be "waters".

    Outputs will be uploaded onto dedicated S3 folder and all metadata will be stored in Postgre
    database using custom reports.

    Parameters
    ----------
    country: str
        "france"
    division: str
        "waters"
    product_level: str
        "fu"
    location_level: str
        "national" or "regional"

    Returns
    -------
    Tuple[adf.services.database.DataprepReports, None]
        Tuple of panel adf.services.database.DataprepReports and None (meaning no innovation report)
    """

    assert product_level == "fu"
    assert location_level in ("national", "regional")

    session = context["session"]
    pipeline_id = context.get("pipeline_id", None)

    report = build_dataprep_report(
        session,
        country,
        division,
        product_level,
        location_level,
        scope="panel",
        split_type="",
        split_code=None,
        prep_type="core",
        pipeline_id=pipeline_id
    )

    mllogs.log_dataprep_report(report)

    try:
        log.info("Building panel")
        panel_df = build_panel_data(
            session,
            country,
            division,
            product_level,
            location_level,
            pipeline_id=pipeline_id
        )
        mllogs.log_shape(panel_df)
        log.info("Saving")
        report.upload(panel_df)
        update_report(session, report, "success")
        mllogs.tag_report_output(report)

    except Exception as err:
        message = str(err.__class__.__name__) + ": " + str(err)
        update_report(session, report, "failed", message)
        raise DataprepError(message)

    finally:
        mllogs.tag_report_output(report)

    return report, None


def build_panel_data(
        session: Session, country: str, division: str,
        product_level: str, location_level: str, **kwargs
) -> pd.DataFrame:
    """
    Builds France Waters panel data for a specified product level and location level.

    It loads all the relevant data sources, cleaning and processing them, and generating specific
    features.

    The output of this method is a dataframe where each row corresponds to a
    product x location x target date level with associated target quantity and features of various
    nature (known orders, promotions attributes and uplifts, weather inputs, products and customers
    characteristics, holidays data etc.).

    Parameters
    ----------
    session: sqlalchemy.orm.Session
    country: str
        "france"
    division: str
        "waters"
    product_level: str
        "fu_cod"
    location_level: str
        "national_cod"
    kwargs

    Returns
    -------
    pandas.DataFrame
        panel dataframe where each row corresponds to a product x location x target date level \
        with associated target quantity and features
    """

    product_column = {
        "fu": "fu_cod",
    }[product_level]

    location_column = {
        "national": "country_cod",
        "regional": "rfa_cod",
    }[location_level]

    # LOAD AND PROCESS ALL DATA SOURCES
    log.info("loading and processing all data sources")

    uom_df = get_snowflake_df(
        config.SNOWFLAKE_DATABASE, "VCP_DSP_AFC_WTR_FRC", "R_SAP_WTR_FRA_MAT_UOM") \
        .drop_duplicates()

    sku_ean_df = build_sku_ean_mapping(uom_df)

    active_forecast_units = get_snowflake_df(
        config.SNOWFLAKE_DATABASE, "VCP_DSP_AFC_WTR_FRC", "R_MAN_WTR_FRA_FTR_FCU") \
        .pipe(process_filters_fu, product_column)

    active_products_df = get_snowflake_df(
        config.SNOWFLAKE_DATABASE, "VCP_DSP_AFC_WTR_FRC", "R_SAP_WTR_FRA_PDT_HIE") \
        .pipe(process_products, active_forecast_units)

    active_forecast_units_df = active_products_df[["mat_cod", "fu_cod"]] \
        .drop_duplicates()

    active_products_df = aggregate_products(active_products_df)

    rfa_df = get_snowflake_df(
        config.SNOWFLAKE_DATABASE, "VCP_DSP_AFC_WTR_FRC", "R_SAP_WTR_FRA_CUS_HIE_NFA_RFA") \
        .pipe(process_rfa)

    orders_df = get_snowflake_df(
        config.SNOWFLAKE_DATABASE, "VCP_DSP_AFC_WTR_FRC", "D_SAP_WTR_FRA_DRY_ORD") \
        .pipe(filter_orders_data) \
        .pipe(process_orders_data, active_forecast_units_df, rfa_df, uom_df) \
        .pipe(aggregate_panel, [product_column, "rfa_cod", "to_dt"])

    panel_promos_no_promos_shipment_df, \
        panel_promos_only_shipment_df, \
        panel_promos_no_promos_stores_df, \
        promos_means_df, \
        promos_volumes_df = run_promos_dataprep(
            country,
            division,
            product_level,
            "regional",
            orders_df,
            active_forecast_units_df,
            building_block=True,
            compute_market_shares=True
        )
    promos_uplifts_coefficients_df = get_promo_uplifts(
        "france", "waters", panel_promos_only_shipment_df
    )

    weather_df = get_snowflake_df(
        config.SNOWFLAKE_DATABASE, "VCP_DTM_AFC", "WTR_FRC_WEA_HIS") \
        .pipe(process_weather_data, "to_dt")[
            ["to_dt", "precipintensity", "apparenttemperaturemax"]]\
        .pipe(aggregate_weather, "country_cod", "to_dt") \
        .drop("country_cod", axis=1)

    holidays_df = get_holidays_df(country, "holidays&dayoff") \
        .pipe(process_holidays)

    # BUILD FINAL PANEL_DF
    log.info(f"building {product_column} panel dfs")
    end_date = build_end_date(lag_type="weeks", lag_value=15)
    panel_df = multiprocess_product_panel(
        orders_df, product_column, "rfa_cod", end_date) \
        .to_pandas() \
        .reset_index(drop=True)

    # Build Sellout IBP Export
    sellout_df = process_and_export_sellout(
        session,
        country,
        division,
        product_column,
        panel_df.drop([k for k in panel_df.columns if "future" in k], axis=1),
        panel_promos_no_promos_stores_df.drop("ordered_volumes", axis=1),
        active_forecast_units_df,
        sku_ean_df,
        pipeline_id=kwargs.get("pipeline_id", None)
    )

    # AGGREGATE DATA
    if location_column == "country_cod":
        panel_df.loc[:, location_column] = 0
        panel_df = aggregate_panel(panel_df, [product_column, location_column, "to_dt"])
        promos_uplifts_coefficients_df = aggregate_promos(
            promos_uplifts_coefficients_df, product_column, location_column, "sum"
        )
        promos_means_df = aggregate_promos(promos_means_df, product_column, location_column, "sum")
        promos_volumes_df = aggregate_promos(
            promos_volumes_df, product_column, location_column, "sum"
        )
        sellout_df = aggregate_iri_data(
            sellout_df, "sellout", by=[product_column, location_column, "to_dt"]
        )

    return panel_df \
        .pipe(safe_merge, "panel", active_products_df, "products",
              warning_type="warning", on="fu_cod",
              how="left", validate="many_to_one") \
        .pipe(safe_merge, "panel", promos_uplifts_coefficients_df, "promos_uplifts_coefficients_df",
              warning_type="warning", on=[product_column, location_column, "to_dt"],
              how="left", validate="one_to_one") \
        .pipe(safe_merge, "panel", promos_means_df, "promos_means",
              warning_type="warning", on=[product_column, location_column, "to_dt"],
              how="left", validate="one_to_one") \
        .pipe(safe_merge, "panel", promos_volumes_df, "promos_volumes",
              warning_type="warning", on=[product_column, location_column, "to_dt"],
              how="left", validate="one_to_one") \
        .pipe(safe_merge, "panel", sellout_df, "sellout",
              warning_type="warning", on=[product_column, location_column, "to_dt"],
              how="left", validate="many_to_one") \
        .pipe(safe_merge, "panel", holidays_df, "holidays",
              warning_type="warning", on="to_dt",
              how="left", validate="many_to_one") \
        .pipe(safe_merge, "panel", weather_df, "weather",
              warning_type="warning", on="to_dt",
              how="left", validate="many_to_one") \
        .pipe(add_days_before_end_of_month, "to_dt")


def multiprocess_product_panel(
        orders_df: pd.DataFrame, product_column: str,
        location_column: str, end_date: pd.Timestamp
) -> ks.DataFrame:
    return ks \
        .from_pandas(orders_df) \
        .groupby(product_column, as_index=False) \
        .apply(build_product_panels(product_column, location_column, end_date))


def build_product_panels(
        product_column: str, location_column: str, end_date: pd.Timestamp
) -> Callable:

    """ Returns a delayed panel_df for a given product
    """
    def build_product_panels_on_group(
            sub_orders_df: pd.DataFrame
    ):
        indexes = build_sub_panel_indexes(
            sub_orders_df,
            end_date,
            product_column,
            location_column,
            "to_dt"
        )
        sub_panel_df = indexes \
            .merge(
                sub_orders_df,
                on=list(indexes.columns),
                how="left",
                validate="one_to_one") \
            .fillna(
                value={col: 0 for col in sub_orders_df.columns
                       if ("ordered_volumes" in col) and ("future" not in col)})
        return sub_panel_df

    return build_product_panels_on_group


@timeit
def filter_orders_data(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Filters initial orders dataframe to remove non-relevant orders registered in data.

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe

    Returns
    -------
    pandas.DataFrame
        Filtered orders dataframe from which the following orders are removed: \
        - orders with a "rejected" tag \
        - virtual orders used to model stocks movements etc. \
        - orders from specific out-of-scope ship-to customers \
        - returned orders (negative values) \
    """
    return df \
        .pipe(remove_rejected_orders) \
        .pipe(remove_virtual_transfers) \
        .pipe(exclude_specific_ship_cus, [650027692]) \
        .pipe(remove_returns)


def remove_rejected_orders(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Removes orders with a "rejected" tag from input orders dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe

    Returns
    -------
    pandas.DataFrame
        filtered orders dataframe
    """
    return df \
        .query('rejection_cod == "#"') \
        .drop("rejection_cod", axis=1)


def remove_virtual_transfers(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Removes orders considered as virtual transfers from input orders dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe

    Returns
    -------
    pandas.DataFrame
        filtered orders dataframe
    """
    return df \
        .query('~sal_cus_pur_doc_num.str.contains("TRANSFERT", case=False)', engine="python") \
        .drop("sal_cus_pur_doc_num", axis=1)


def exclude_specific_ship_cus(
        df: pd.DataFrame, codes: List[int], from_date: str = "2020-01-01"
) -> pd.DataFrame:
    """
    Removes orders linked to specified out-of-scope ship-to customers (in codes parameter) from a
    specified date (in from_date parameter) from input orders dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe
    codes: List[int]
        list of codes to remove from df
    from_date: str (format %Y-%m-%d)
        dates from which orders linked to codes must be removed

    Returns
    -------
    pandas.DataFrame
        filtered orders dataframe
    """
    return df \
        .query(f'~((shp_cus_cod.isin({codes})) & (order_creation_date >= "{from_date}"))',
               engine="python")


def remove_returns(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Removes orders linked to specified out-of-scope ship-to customers (in codes parameter) from a
    specified date (in from_date parameter) from input orders dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe

    Returns
    -------
    pandas.DataFrame
        filtered orders dataframe
    """
    return df.query('~sal_doc_typ_cod.str.startswith("ZR")', engine="python")


@timeit
def process_orders_data(
        df: pd.DataFrame, fu_mapping: pd.DataFrame,
        rfa_mapping: pd.DataFrame, uom_df: pd.DataFrame,
) -> pd.DataFrame:
    """
    Processes orders input dataframe to: \
    - format data \
    - update order creation date \
    - build final target date and quantity \
    - convert target quantity to volume \
    - add forecast units level \
    - add RFA level
    - add weekly and monthly future orders features \
    - drop unused columns

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe at sku level
    fu_mapping: pandas.DataFrame
        input sku/fu mapping dataframe
    rfa_mapping: pandas.DataFrame
        input SAP/RFA customers mapping dataframe
    uom_df: pandas.DataFrame
        input unit-of-measures tables (~conversions table) at sku level

    Returns
    -------
    pandas.DataFrame
        processed orders dataframe with relevant targets, features and forecast units information
    """
    return df \
        .pipe(build_target_dates_and_quantities, "to_dt", "ordered_units") \
        .pipe(update_creation_date) \
        .pipe(convert_to_volumes, uom_df, "ordered_volumes") \
        .pipe(add_orders_flags_features, "orders_type_flags", "ordered_volumes") \
        .pipe(add_fu_to_orders, fu_mapping) \
        .pipe(add_rfa_to_orders, rfa_mapping) \
        .pipe(add_future_orders_of_n_previous_days,
              "to_dt", "global_order_creation_date", "ordered_volumes",
              interval=range(0, 21)) \
        .pipe(add_future_orders_until_weekday_of_n_previous_weeks,
              "to_dt", "global_order_creation_date", "ordered_volumes",
              interval=range(0, 15),
              until_weekday_included=(date.today() - timedelta(days=1)).weekday(),
              add_weekday_in_column_name=False) \
        .drop(["order_creation_date", "global_order_creation_date"], axis=1)


def build_target_dates_and_quantities(
        df: pd.DataFrame, new_date_col: str, new_qty_col: str
) -> pd.DataFrame:
    """
    Builds final target date and target quantity columns and adds them to initial order dataframe.
    Creation of target fields is based on masks and business conditions.

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe
    new_date_col: str
        name of target date column to be created
    new_qty_col: str
        name of target quantity column to be created

    Returns
    -------
    pandas.DataFrame
        updated dataframe containing target field that were created (non-null data in them)
    """
    mask1 = df["delivery_doc_type_cod"].isin(["ZLF", "ZLFO", "ZLFV", "ZLR"])
    mask2 = df["delivery_doc_type_cod"].isnull()
    mask3 = df["actual_mvt_date"].isnull()

    df.loc[mask1 & ~mask3, new_date_col] = df.loc[mask1 & ~mask3, "actual_mvt_date"]
    df.loc[mask1 & mask3, new_date_col] = df.loc[mask1 & mask3, "planned_mvt_date"]
    df.loc[mask1, new_qty_col] = df.loc[mask1, "delivered_sku_quantity"]

    df.loc[mask2, new_date_col] = df.loc[mask2, "material_availability_date"]
    df.loc[mask2, new_qty_col] = df.loc[mask2, "ordered_sku_quantity"]

    return df \
        .drop(
            [
                "delivery_doc_type_cod",
                "actual_mvt_date",
                "planned_mvt_date",
                "material_availability_date",
                "delivered_sku_quantity",
                "ordered_sku_quantity"
            ], axis=1
        ) \
        .dropna(subset=[new_date_col, new_qty_col], how="any")


def update_creation_date(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Updates global_order_creation_date/order_creation_date field inside input orders dataframe
    replacing by actual_movement_date/to_dt (~delivery date) value if
    global_order_creation_date/order_creation_date is set after delivery date.

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe

    Returns
    -------
    pandas.DataFrame
        updated dataframe containing updated global_order_creation_date/order_creation_date field
    """
    mask1 = df["order_creation_date"] > df["to_dt"]
    df.loc[mask1, "order_creation_date"] = df.loc[mask1, "to_dt"]
    mask2 = df["global_order_creation_date"] > df["to_dt"]
    df.loc[mask2, "global_order_creation_date"] = df.loc[mask2, "to_dt"]
    return df


def convert_to_volumes(
        df: pd.DataFrame, uom_df: pd.DataFrame, new_col: str
) -> pd.DataFrame:
    """
    Adds new columns to initial orders dataframe containing conversion of target quantity into
    target volume (expressed in kilo liters)

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe
    uom_df: pandas.DataFrame
        input unit-of-measures tables (~conversions table) at sku level
    new_col: str
        new column name for target volumes

    Returns
    -------
    pandas.DataFrame
        updated dataframe containing additional target volumes
    """
    volumes_conversions_df = build_conversions_to_liters(
        uom_df, df["sku_uom_cod"].unique(), "L"
    )
    df = safe_merge(
        df,
        "orders",
        volumes_conversions_df,
        "volumes_conversion",
        warning_type="error",
        on=["mat_cod", "sku_uom_cod"],
        how="inner",
        validate="many_to_one"
    )
    df[new_col] = (df["ordered_units"] * df.value) / 1000  # L to KL
    return df.drop(["sku_uom_cod", "value", "ordered_units"], axis=1)


def build_conversions_to_liters(
        uom_df: pd.DataFrame, units_to_convert: List[str], target_unit: str
) -> pd.DataFrame:
    """
    Builds a conversion dataframe from initial unit-of-measures dataframe, based on units to convert
    and target unit to achieve.

    Parameters
    ----------
    uom_df: pandas.DataFrame
        input unit-of-measures tables (~conversions table) at sku level
    units_to_convert: List[str]
        list of units to be converted
    target_unit: str
        target unit, here it is "L" for liters

    Returns
    -------
    pandas.DataFrame
        conversion dataframe with columns mat_cod / sku_uom_cod (initial unit) / value (conversion
        value from sku_uom_cod unit to target unit)
    """
    uom_df = uom_df.loc[uom_df.uom_cod.isin(np.append(units_to_convert, target_unit))]
    conversions = uom_df.pivot(index="mat_cod", columns="uom_cod")["conversion_ratio"]
    conversions.columns.name = None
    col = conversions.columns.drop(target_unit)
    conversions[col] = 1 / conversions[col]
    conversions = conversions[col] \
        .mul(conversions[target_unit], axis=0) \
        .reset_index()
    return conversions \
        .melt(id_vars="mat_cod", value_vars=list(conversions.columns.drop("mat_cod")),
              var_name="uom_cod") \
        .rename({"uom_cod": "sku_uom_cod"}, axis=1)


def add_orders_flags_features(
    df: pd.DataFrame, flag_col_name: str, target_qty_col_name: str
) -> pd.DataFrame:
    """
    Adds columns to input orders dataframe for each flag type with corresponding volumes inside

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe
    flag_col_name: str
        name of column to create to store orders type flags information
    target_qty_col_name: str
        name of column containing quantity target to forecast

    Returns
    -------
    pandas.DataFrame
        enriched orders dataframe
    """
    temp = df \
        .pipe(add_orders_flags, flag_col_name)[[flag_col_name, target_qty_col_name]] \
        .pivot(columns=flag_col_name, values=target_qty_col_name)
    temp.columns.name = None
    temp = temp.rename({k: target_qty_col_name + "_" + k for k in temp.columns}, axis=1)
    return pd \
        .concat([df, temp], axis=1) \
        .drop(
            ["sal_doc_typ_cod", "order_reason_cod", "order_change_reason_cod", flag_col_name],
            axis=1)


def add_orders_flags(
        df: pd.DataFrame, flag_col_name: str
) -> pd.DataFrame:
    """
    Adds new columns containing "tags" (PROMO, SA, SS or NaN) for each order on orders input
    dataframe based on business rules applied with masks.

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe
    flag_col_name: str
        name of column to create to store orders type flags information

    Returns
    -------
    pandas.DataFrame
        updated orders dataframe containing orders tags (PROMO, SA, SS or NaN)
    """
    mask = (df["sal_doc_typ_cod"] == "ZKB") & \
           ((df["order_reason_cod"] == "46S") | (df["order_change_reason_cod"] == "T04"))
    df.loc[mask, flag_col_name] = "sa"

    mask = (df["sal_doc_typ_cod"] == "ZSO") & \
           ((df["order_reason_cod"] == "46S") | (df["order_change_reason_cod"] == "T04"))
    df.loc[mask, flag_col_name] = "ss"

    mask = (df["sal_doc_typ_cod"] == "ZSO") & (
        df["order_change_reason_cod"].isin(["D02", "LDM", "TRA", "NC6"])
    )
    df.loc[mask, flag_col_name] = "promo"

    mask = (df["sal_doc_typ_cod"] == "ZSO") & (df["order_change_reason_cod"] == "RH1")
    df.loc[mask, flag_col_name] = "push"

    df[flag_col_name] = df[flag_col_name].fillna("baseline")
    return df


def add_fu_to_orders(
        df: pd.DataFrame, sku_fu_df: pd.DataFrame
) -> pd.DataFrame:
    """
    Adds forecast units codes and description to input orders dataframe.

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe
    sku_fu_df: pandas.DataFrame
        sku/fu mapping dataframe

    Returns
    -------
    pandas.DataFrame
        updated orders dataframe containing forecast units information
    """
    return df \
        .pipe(
            safe_merge,
            "orders",
            sku_fu_df,
            "sku_fu",
            warning_type="warning",
            on="mat_cod",
            how="inner",
            validate="many_to_one") \
        .drop(["mat_cod"], axis=1)


def add_rfa_to_orders(
        df: pd.DataFrame, rfa_mapping: pd.DataFrame,
) -> pd.DataFrame:
    """
    Adds rfa level information to input orders dataframe, using a mapping sap_cus_cod/rfa_cod

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe
    rfa_mapping: pandas.DataFrame
        input sap_cus_cod/rfa_cod mapping dataframe

    Returns
    -------
    pandas.DataFrame
        enriched orders dataframe containing rfa information. Sales organization, distribution \
        channel and cus_cod granularities are also removed.
    """
    return df \
        .pipe(
            safe_merge,
            "orders",
            rfa_mapping.rename({"cus_cod": "shp_cus_cod"}, axis=1),
            "rfa_mapping",
            warning_type="warning",
            on=["sales_organization_cod", "distribution_channel_cod", "shp_cus_cod"],
            how="inner",
            validate="many_to_one") \
        .drop(
            ["sales_organization_cod",
             "distribution_channel_cod",
             "shp_cus_cod"], axis=1
        )


@timeit
def aggregate_panel(
        df: pd.DataFrame, groupby_cols: List[str]
) -> pd.DataFrame:
    """
    Aggregates input orders dataframe to the groupby_cols indexes level summing all volumes

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe
    groupby_cols: List[str]
        list of columns on which to groupby before aggregating

    Returns
    -------
    pandas.DataFrame
        aggregated orders dataframe
    """
    to_agg = {col: "sum" for col in df.columns if "ordered_volumes" in col}
    return df \
        .groupby(by=groupby_cols) \
        .agg(to_agg) \
        .reset_index()


@timeit
def process_products(
        df: pd.DataFrame, active_forecast_units: list
) -> pd.DataFrame:
    """
    Processed input products hierarchy dataframe using relevant sku/fu mapping and target
    product column: \

    - Formatting data \

    - Harmonizing units of weights, volumes, and dimensions \

    - Adding FU level information

    Parameters
    ----------
    df: pandas.DataFrame
        input products hierarchy dataframe
    active_forecast_units: list
        list of active fu_cod on which to forecast

    Returns
    -------
    pandas.DataFrame
        processed products hierarchy dataframe
    """
    return df \
        .rename({"frc_unt_cod": "fu_cod", "frc_unt_dsc": "fu_desc"}, axis=1) \
        .replace(to_replace=["#", "(null)"], value=np.NaN) \
        .query('sales_organization_cod.isin(["0046"])', engine="python") \
        .drop(["sales_organization_cod"], axis=1) \
        .dropna(how="any", subset=["mat_cod", "fu_cod"]) \
        .pipe(cast_to_int, "distribution_channel_cod", "int8") \
        .pipe(cast_to_int, "fu_cod", "int32") \
        .pipe(update_products_units) \
        .pipe(filter_forecast_units, active_forecast_units)


def cast_to_int(
        df: pd.DataFrame, col: str, int_type: str
) -> pd.DataFrame:
    """
    Casts input col from input dataframe into int_type, dropping generated new values.
    Returns updated dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        input dataframe
    col: str
        column to cast
    int_type: type of int. Can be int8, int32 etc.

    Returns
    -------
    pandas.DataFrame
        updated dataframe
    """
    df[col] = pd.to_numeric(df[col], downcast="integer", errors="coerce")
    return df \
        .dropna(subset=[col], how="any", axis=0) \
        .astype({col: int_type})


def update_products_units(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Filters input products hierarchy dataframe to remove non-harmonized rows not expressed in
    target units for both volumes and weights.

    Parameters
    ----------
    df: pandas.DataFrame
        input products hierarchy dataframe

    Returns
    -------
    pandas.DataFrame
        filtered products hierarchy dataframe, with renamed volume and weight attributes columns
    """
    return df \
        .query('(mat_weight_unit == "KG") and (mat_volume_unit == "PL2")') \
        .drop(["mat_weight_unit", "mat_volume_unit", "mat_dimension_unit"], axis=1) \
        .rename(
            {
                "mat_net_weight_value": "mat_net_weight_value_kg",
                "mat_volume_value": "mat_volume_value_pl2",
                "mat_length_value": "mat_length_value_m",
                "mat_width_value": "mat_width_value_m",
                "mat_height_value": "mat_height_value_m"
            },
            axis=1
        )


def aggregate_products(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Aggregates input products hierarchy dataframe to forecast units level

    Parameters
    ----------
    df: pandas.DataFrame
        input products hierarchy dataframe

    Returns
    -------
    pandas.DataFrame
        aggregated products hierarchy dataframe at forecast unit level
    """
    to_agg: Dict[str, Union[str, Callable]] = {
        k: most_frequent_value for k in df.columns if k not in [
            "fu_cod", "mat_cod", "mat_desc", "ean_cod"
        ]
    }
    return df \
        .groupby(by=["fu_cod"]) \
        .agg(to_agg) \
        .reset_index()


@timeit
def process_rfa(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Processes input SAP/APO customers mapping replacing wrong data and formatting them, and
    returning SAP customers codes and RFA codes information

    Parameters
    ----------
    df: pandas.DataFrame
        input SAP/APO customers mapping

    Returns
    -------
    pandas.DataFrame
        formatted SAP/APO mapping
    """
    return df[[
        "sales_organization_cod",
        "cus_distribution_channel_cod",
        "cus_cod",
        "rfa_cod"]] \
        .query('sales_organization_cod.isin(["0046"])', engine="python") \
        .rename(columns={"cus_distribution_channel_cod": "distribution_channel_cod"}) \
        .pipe(cast_to_int, "distribution_channel_cod", "int8") \
        .replace(to_replace="#", value=np.NaN) \
        .drop_duplicates()


@timeit
def process_filters_fu(
        df: pd.DataFrame, product_column: str
) -> list:
    """
    Processes input dataframe to remove duplicates, bad data and cast types.

    Parameters
    ----------
    df: pandas.DataFrame
        input dataframe
    product_column: str
        target product column

    Returns
    -------
    list
        list of products obtained ofter processing
    """
    fu_list = df \
        .query(f'status == "" and ~{product_column}.isna()', engine="python")[product_column] \
        .drop_duplicates() \
        .astype("int") \
        .tolist()
    log.info(f"active FUs in filter: {len(fu_list)}")
    return fu_list


@timeit
def filter_forecast_units(
        df: pd.DataFrame, active_forecast_units: list
) -> pd.DataFrame:
    """
    Filters input sku/fu mapping dataframe keeping only active forecast units contained in
    active_forecast_units parameter

    Parameters
    ----------
        df: pandas.DataFrame
            input sku/fu mapping dataframe
        active_forecast_units: list
            list of active forecast units

    Returns
    -------
    pandas.DataFrame
        filtered sku/fu mapping dataframe
    """
    df = df.query(f'fu_cod.isin({active_forecast_units})', engine="python")
    log.info(f"active FU for forecast: {len(df.fu_cod.unique())}")
    return df


@timeit
def aggregate_promos(
        df: pd.DataFrame, product_column: str, location_column: str, aggregation_method
) -> pd.DataFrame:
    """
    Aggregates input promotions dataframe on a specified set of product x location x date
    columns summing volumes columns

    Parameters
    ----------
    df: pandas.DataFrame
        input promotions dataframe
    product_column: str
        target product column. Value is "fu_cod"
    location_column: str
        target location column. Value can be "rfa_cod" or "country_cod"
    aggregation_method: callable, string, dictionary, or list of string/callables
        function to use for aggregating the data

    Returns
    -------
    pandas.DataFrame
        aggregated promotions dataframe with a one-level indexed columns
    """
    if location_column == "country_cod":
        df = df.drop("rfa_cod", axis=1)
        if "country_cod" not in df.columns:
            df[location_column] = 0
    return df \
        .groupby([product_column, location_column, "to_dt"]) \
        .agg(aggregation_method) \
        .reset_index()


@timeit
def build_sku_ean_mapping(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Builds a SKU/EAN mapping dataframe from an input unit_of_measure dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        input unit_of_measure dataframe

    Returns
    -------
    pandas.DataFrame
        SKU/EAN mapping dataframe
    """
    df = df.drop_duplicates(subset=["ean_cod", "mat_cod"])
    df = df[df["ean_cod"] != "#"]
    return df[["ean_cod", "mat_cod"]]


@timeit
def process_holidays(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Processes holidays input dataframe updating types and adding features such as weekday of
    holidays, holidays count in week and month

    Parameters
    ----------
    df: pandas.DataFrame
        input holidays dataframe

    Returns
    -------
    pandas.DataFrame
        processed holidays dataframe
    """
    return df \
        .pipe(prepare_french_holidays_national) \
        .drop_duplicates(subset="to_dt") \
        .pipe(memory_reduction) \
        .pipe(update_holidays_datetime, "to_dt") \
        .pipe(add_weekday_of_holidays, "to_dt") \
        .pipe(add_holidays_count_in_week, "to_dt") \
        .pipe(add_holidays_count_in_month, "to_dt")


@timeit
def get_promo_uplifts(
        country: str, division: str, panel_promos_df: pd.DataFrame
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Gets and builds promotions uplift coefficients and rolling means dataframes from promotions
    dataprep reports and building blocks reports

    Parameters
    ----------
    country: str
        "france"
    division: str
        "waters"
    panel_promos_df: pandas.DataFrame
        input panel promos dataframe

    Returns
    -------
    Tuple[pandas.DataFrame]
        promotions uplifts and rolling means dataframes
    """
    session = get_session()

    business_unit = session.query(BusinessUnits) \
        .filter(BusinessUnits.country == country) \
        .filter(BusinessUnits.division == division) \
        .first()

    bb_table = session \
        .query(BuildingBlocksReports) \
        .filter(BuildingBlocksReports.status == "success") \
        .filter(BuildingBlocksReports.business_unit_id == business_unit.id) \
        .filter(BuildingBlocksReports.scope == "promo_building_block") \
        .order_by(desc(BuildingBlocksReports.updated_at)) \
        .first() \
        .download(object_type="pandas")

    for col in bb_table.filter(regex="uplift").columns:
        bb_table.loc[(bb_table[col] > 1), col] = 1

    bb_df = panel_promos_df.merge(
        bb_table,
        on=["fu_cod", "rfa_cod"],
        how="left",
        validate="many_to_one"
    )

    # For each row, select the uplift coefficient linked to promotion mechanics
    bb_df["promo_uplift_coefficient"] = [
        row[row["promotion_mechanics_desc"] + "_uplift"]
        if row["promotion_mechanics_desc"] + "_uplift" in bb_df.columns else np.NaN
        for _, row in bb_df.iterrows()
    ]

    uplifts_coefficients_df = bb_df \
        .groupby(["fu_cod", "rfa_cod", "to_dt"]) \
        .promo_uplift_coefficient \
        .sum() \
        .reset_index()

    return uplifts_coefficients_df
