import logging
import traceback
import math
import pandas as pd  # type: ignore
import numpy as np  # type: ignore
from functools import partial  # type: ignore
import databricks.koalas as ks  # type: ignore
from typing import Tuple, Callable, Dict, List
from datetime import timedelta
from adf import config
from adf.services.database import get_session
from adf.errors import DataprepError
from adf.modelling.tools import get_snowflake_df
from adf.utils.provider import Provider
from adf.utils.decorators import timeit
from adf.modelling.dataprep.utils.core import build_promo_dataprep_report
from adf.services.database.query import update_report
from adf.modelling.dataprep.france.waters.building_blocks import build_bb
from adf.modelling.dataprep.utils.promos import expand_promos_to_daily
from adf.modelling.dataprep.utils.core import safe_merge

log = logging.getLogger("adf")
Provide = partial(Provider, mlrun=False, session=True, cluster=True)

AGG_LEVELS = {
    "1": ["fu_cod", "rfa_cod", "promotion_mechanics_desc", "time_category", "promo_quantile"],
    "2": ["fu_cod", "rfa_cod", "promotion_mechanics_desc", "time_category"],
    "3": ["fu_cod", "rfa_cod", "time_category"],
    "4": ["fu_cod", "rfa_cod", "promotion_mechanics_desc"],
    "5": ["fu_cod", "promotion_mechanics_desc", "time_category", "promo_quantile"],
    "6": ["fu_cod", "promotion_mechanics_desc", "time_category"],
    "7": ["fu_cod", "time_category"],
    "8": ["fu_cod", "promotion_mechanics_desc"],
    "9": ["fu_cod"]
}

horizons = [7 * t for t in range(0, 15)]


def run(
        country: str, division: str, product_level, location_level,
        orders_df: pd.DataFrame, products_df: pd.DataFrame, **context
):
    """
    Builds promos data for France Waters
    """
    assert product_level == "fu"
    assert location_level == "regional"

    session = get_session()

    pipeline_id = context.get("pipeline_id", None)
    building_block = context.get("building_block", False)
    compute_market_shares = context.get("compute_market_shares", False)

    panel_promos_report = build_promo_dataprep_report(
        session,
        country,
        division,
        product_level,
        location_level,
        scope="promo_panel",
        split_type="",
        split_code=None,
        pipeline_id=pipeline_id
    )

    try:
        log.info("Creating promotions panel and rolling means dataframes")
        panel_promos_no_promos_shipment_kdf, \
            panel_only_promos_shipment_kdf, \
            panel_promos_no_promos_stores_kdf, \
            promos_means_kdf, \
            promos_volumes_df = build_promos_data(orders_df, products_df)
        log.info("Saving")
        panel_promos_report.upload(
            panel_only_promos_shipment_kdf.reset_index(drop=True),
            engine="pyarrow"
        )

        if building_block:
            log.info("Creating promos building blocks dataframe")
            build_bb(
                panel_promos_no_promos_shipment_kdf,
                compute_market_shares,
                session=session,
                dataprep_id=str(panel_promos_report.id),
                pipeline_id=pipeline_id
            )

        update_report(session, panel_promos_report, "success")
        return panel_promos_no_promos_shipment_kdf.to_pandas().reset_index(drop=True), \
            panel_only_promos_shipment_kdf.to_pandas().reset_index(drop=True), \
            panel_promos_no_promos_stores_kdf.to_pandas().reset_index(drop=True), \
            promos_means_kdf.to_pandas().reset_index(drop=True), \
            promos_volumes_df

    except Exception as err:
        traceback.print_exc()
        message = str(err.__class__.__name__) + ": " + str(err)
        update_report(session, panel_promos_report, "failed", message)
        raise DataprepError(message)


@timeit
def build_promos_data(
        orders_df: pd.DataFrame, products_df: pd.DataFrame
) -> Tuple[ks.DataFrame, ks.DataFrame, ks.DataFrame, ks.DataFrame, pd.DataFrame]:

    shift_mapping = get_snowflake_df(
        config.SNOWFLAKE_DATABASE, "VCP_DSP_AFC_WTR_FRC", "R_MAN_WTR_FRA_PMN_PHS") \
        .pipe(process_promos_phasing)

    promos_df = get_snowflake_df(
        config.SNOWFLAKE_DATABASE, "VCP_DSP_AFC_WTR_FRC", "F_BLP_WTR_FRA_IBP_PMN") \
        .pipe(filter_status) \
        .pipe(drop_duplicated_promos) \
        .pipe(process_string_mechanics) \
        .pipe(add_fu_to_promos, products_df) \
        .drop_duplicates(
            subset=[
                "fu_cod",
                "rfa_cod",
                "promotion_mechanics_desc",
                "promotion_start_date_store"],
            keep="first") \
        .pipe(correct_promos_dates_in_stores_at_rfa_level,
              "promotion_start_date_store", "promotion_end_date_store")

    promos_volumes_df = promos_df \
        .apply(partial(shift_rfa_start_sellin, shift_mapping), axis=1) \
        .pipe(remove_wrong_promos) \
        .pipe(expand_promos_to_daily, "promotion_start_date_expedition",
              "promotion_end_date_expedition", "to_dt") \
        .groupby(["fu_cod", "rfa_cod", "to_dt"])["forecasted_volumes"] \
        .sum() \
        .reset_index()

    log.info(f"processing promos per fu_cod: {process_product_panel_promos.__name__} "
             f"at shipment level")
    promo_no_promo_shipment_kdf = multiprocess_product_orders_with_promos(
        "shipment",
        promos_df,
        orders_df,
        shift_mapping,
        process_product_panel_promos
    )
    only_promo_shipment_kdf = promo_no_promo_shipment_kdf.loc[
        promo_no_promo_shipment_kdf["promotion_mechanics_desc"] != "no_promo"
    ]

    log.info(f"processing promos per fu_cod: {process_product_panel_promos.__name__} "
             f"at store level")
    promo_no_promo_stores_kdf = multiprocess_product_orders_with_promos(
        "store",
        promos_df,
        orders_df,
        shift_mapping,
        process_product_panel_promos
    )

    log.info(f"processing promos per fu_cod: {process_product_promos_means.__name__}")
    promos_rolling_means_kdf = multiprocess_product_orders_with_promos(
        "shipment",
        promos_df,
        orders_df,
        shift_mapping,
        process_product_promos_means
    )

    return (
        promo_no_promo_shipment_kdf,
        only_promo_shipment_kdf,
        promo_no_promo_stores_kdf,
        promos_rolling_means_kdf,
        promos_volumes_df
    )


@timeit
def process_promos_phasing(
        df: pd.DataFrame
) -> Dict[int, List[int]]:
    """
    Processes input promos phasing table in order to return a dictionary with shifting intervals
    (in days) for each RFA code.

    Parameters
    ----------
    df: pandas.DataFrame
        input promotions phasing dataframe

    Returns
    -------
    Dict[int: List[int]
        dictionary with rfa_cod as keys and shifting intervals in days as values
    """
    df = df.set_index("rfa_cod")
    df.columns = [float(col[1:]) * 7 for col in df.columns]
    result = {}
    for row in df.iterrows():
        shift = row[1].sort_index()
        shift = shift[shift != 0]
        value = [shift.index[0], shift.index[-1] + 7]
        result[row[0]] = value
    return result


@timeit
def filter_status(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Filters input promotions dataframe keeping only CONFIRMED and INVOICED promotions

    Parameters
    ----------
    df: pandas.DataFrame
        input promotions dataframe

    Returns
    -------
    pandas.DataFrame
        filtered promotions dataframe
    """
    df = df[df["status_cod"].isin(["1300", "1500"])]
    return df.drop("status_cod", axis=1)


@timeit
def drop_duplicated_promos(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Drops duplicated rows corresponding to similar promotions keeping the one with the maximum
    forecasted uplift volumes

    Parameters
    ----------
    df: pandas.DataFrame
        input promotions dataframe

    Returns
    -------
    pandas.DataFrame
        cleaned promotions dataframe
    """
    keys = list(df.columns)
    keys.remove("forecasted_volumes")

    return df \
        .groupby(keys)["forecasted_volumes"] \
        .max() \
        .reset_index()


@timeit
def process_string_mechanics(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Processes promotion_mechanics_desc data : \
    - Replacing spaces by underscores \
    - Replacing "="  with "equal" \
    - Replacing "%" with "percent" \
    - Replacing "+" with "plus" \
    - Replacing "-" with "moins" \
    - Removing all accents \
    - Removing all the following characters: (){}@,;
    - Replacing multiple underscores by one
    - Removing underscores at the end of strings

    Parameters
    ----------
    df: pandas.DataFrame
        input promotions dataframe

    Returns
    -------
    pandas.DataFrame
        processed promotions dataframe with formatted promotion_mechanics_desc
    """
    df["promotion_mechanics_desc"] = df["promotion_mechanics_desc"] \
        .pipe(replace_special_characters) \
        .pipe(remove_accents) \
        .pipe(remove_special_characters) \
        .pipe(remove_multiple_and_end_of_string_underscores) \
        .str \
        .lower()
    return df


def replace_special_characters(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Processes input dataframe : \
    - Replacing spaces by underscores \
    - Replacing "="  with "equal" \
    - Replacing "%" with "percent" \
    - Replacing "+" with "plus" \
    - Replacing "-" with "moins"

    Parameters
    ----------
    df: pandas.DataFrame
        input promotions dataframe

    Returns
    -------
    pandas.DataFrame
        processed promotions dataframe with formatted fields
    """
    return df \
        .replace(r"\s+", "_", regex=True) \
        .replace(r"\=", "_equal_", regex=True) \
        .replace(r"%", "_percent_", regex=True) \
        .replace(r"\+", "_plus_", regex=True) \
        .replace(r"\-", "_moins_", regex=True)


def remove_accents(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Removes all accents from dataframe values

    Parameters
    ----------
    df: pandas.DataFrame
        input promotions dataframe

    Returns
    -------
    pandas.DataFrame
        processed promotions dataframe with formatted fields
    """
    return df \
        .str \
        .normalize("NFKD") \
        .str \
        .encode("ascii", errors="ignore") \
        .str \
        .decode("utf-8")


def remove_special_characters(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Removes all the following characters from input dataframe: (){}@,;

    Parameters
    ----------
    df: pandas.DataFrame
        input promotions dataframe

    Returns
    -------
    pandas.DataFrame
        processed promotions dataframe with formatted fields
    """
    return df\
        .replace("[(){}@,;]", "", regex=True) \
        .replace("\n\t", " ", regex=True)


def remove_multiple_and_end_of_string_underscores(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Replaces multiple underscores by one, and removes underscores at the end of strings

    Parameters
    ----------
    df: pandas.DataFrame
        input promotions dataframe

    Returns
    -------
    pandas.DataFrame
        processed promotions dataframe with formatted fields
    """
    return df \
        .replace("_+", "_", regex=True) \
        .str \
        .rstrip("_")


@timeit
def add_fu_to_promos(
        df: pd.DataFrame, products_df: pd.DataFrame
) -> pd.DataFrame:
    """
    Adds forecast unit information to input promotions dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        input promotions dataframe
    products_df: pandas.DataFrame
        input products dataframe containing forecast units information

    Returns
    -------
    pandas.DataFrame
        enriched promotions dataframe with forecast units information
    """
    return df \
        .pipe(
            safe_merge,
            "promos",
            products_df,
            "sku_fu_mapping",
            warning_type="warning",
            on="mat_cod",
            how="inner",
            validate="many_to_one") \
        .drop(["mat_cod"], axis=1) \
        .drop_duplicates()


@timeit
def correct_promos_dates_in_stores_at_rfa_level(
        df: pd.DataFrame, start_date_col: str, end_date_col: str
) -> pd.DataFrame:
    """
    Cleans promotions data truncating the end of a promo in case it ends after the beginning of
    another one on the same temporal slot (end of promo is then put one day before the beginning of
    the other one). New end date is replaced in the end_date_col in input promotions dataframe.

    Parameters
    ----------
    df: pandas.DataFrame
        input promotions dataframe
    start_date_col: str
        name of target beginning promotions date column
    end_date_col: str
        name of target end promotions date column

    Returns
    -------
    pandas.DataFrame
        updated promotions dataframe with correct end promotions dates avoiding overlaps
    """
    df = df.sort_values(by=[start_date_col, "promotion_mechanics_desc"])
    df["end"] = df.groupby(["fu_cod", "rfa_cod"])[start_date_col].shift(-1) - pd.Timedelta(days=1)
    df[end_date_col] = df[[end_date_col, "end"]].min(axis=1)
    return df.drop("end", axis=1)


@timeit
def multiprocess_product_orders_with_promos(
        level: str,
        promos_df: pd.DataFrame,
        orders_df: pd.DataFrame,
        shift_mapping: Dict[int, List[int]],
        function: Callable
):
    return ks \
        .from_pandas(promos_df) \
        .groupby("fu_cod") \
        .apply(process_product_promos(level, orders_df, shift_mapping, function))


def process_product_promos(
        level: str,
        orders_df: pd.DataFrame,
        shift_mapping: Dict[int, List[int]],
        function: Callable
) -> Callable:
    def process_product_promos_on_group(
            product_promos_df: pd.DataFrame
    ):
        fu_cod = list(product_promos_df["fu_cod"].unique())[0]
        start_date = product_promos_df["promotion_start_date_store"]
        end_date = product_promos_df["promotion_end_date_store"]
        mask = (start_date > end_date)
        product_promos_df = product_promos_df.loc[~mask]
        product_orders_df = orders_df.loc[orders_df["fu_cod"] == fu_cod]
        return function(
            level,
            product_promos_df,
            product_orders_df,
            shift_mapping
        )

    return process_product_promos_on_group


def process_product_panel_promos(
        level: str,
        product_promos_df: pd.DataFrame,
        product_orders_df: pd.DataFrame,
        shift_mapping: Dict[int, List[int]]
):
    assert level in ("shipment", "store")

    if level == "shipment":
        product_promos_df = product_promos_df \
            .apply(partial(shift_rfa_start_sellin, shift_mapping), axis=1)

    product_promos_df = product_promos_df \
        .pipe(remove_wrong_promos) \
        .pipe(
            expand_promos_to_daily,
            "promotion_start_date_expedition" if level == "shipment"
            else "promotion_start_date_store",
            "promotion_end_date_expedition" if level == "shipment"
            else "promotion_end_date_store",
            "to_dt") \
        .pipe(clean_promo)

    product_orders_df = product_orders_df \
        .groupby(["fu_cod", "rfa_cod", "to_dt"])["ordered_volumes"] \
        .sum() \
        .reset_index()

    product_promo_enriched_df = product_orders_df.merge(
        product_promos_df,
        on=["to_dt", "rfa_cod", "fu_cod"],
        how="right"
    )
    product_promo_enriched_df = product_promo_enriched_df.fillna(value=0)

    product_orders_enriched_df = product_orders_df.merge(
        product_promos_df,
        on=["to_dt", "rfa_cod", "fu_cod"],
        how="left"
    )

    product_orders_enriched_df.loc[
        product_orders_enriched_df["promotion_mechanics_desc"].isnull(),
        "promotion_mechanics_desc"
    ] = "no_promo"
    product_orders_no_promo = product_orders_enriched_df.loc[
        product_orders_enriched_df["promotion_mechanics_desc"] == "no_promo"
    ]

    final_product_promo_df = pd.concat(
        [product_orders_no_promo, product_promo_enriched_df],
        axis=0
    )
    final_product_promo_df = final_product_promo_df[[
        "fu_cod",
        "rfa_cod",
        "to_dt",
        "ordered_volumes",
        "promotion_mechanics_desc"
    ]]
    return final_product_promo_df


def shift_rfa_start_sellin(shift_mapping: Dict[int, List[int]], row):
    """
    Shifts promotion sellin start and end dates based on a shift mapping that depends on customers'
    behaviours.
    This methods applies to a promotions dataframe's row.
    """
    if row["rfa_cod"] in shift_mapping.keys():
        rfa_cod = row["rfa_cod"]
        row["promotion_start_date_expedition"] = \
            row["promotion_start_date_store"] + timedelta(days=shift_mapping[rfa_cod][0])
        if len(shift_mapping[rfa_cod]) > 1:
            row["promotion_end_date_expedition"] = \
                row["promotion_start_date_store"] + timedelta(days=shift_mapping[rfa_cod][1])
        else:
            row["promotion_end_date_expedition"] = row["promotion_end_date_store"]
    return row


def remove_wrong_promos(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Removes row from input promotions dataframe for which start expedition date is after end
    expedition date

    Parameters
    ----------
    df: pandas.DataFrame
        input promotions dataframe

    Returns
    -------
    pandas.DataFrame
        cleaned promotions dataframe
    """
    mask = (df["promotion_start_date_expedition"] > df["promotion_end_date_expedition"])
    return df.loc[~mask]


def clean_promo(
        promos_df: pd.DataFrame
) -> pd.DataFrame:
    """
    Cleans input promotions dataframe to remove duplicated and overlapping promotions

    For a given promotions (product x customers x promotion mechanics), if slots are overlapping,
    we keep the widest slot.
    For a given slot (product x customers x date), if we have 2 promotions mechanics at the same
    time, we keep the first one.

    Parameters
    ----------
    promos_df: pandas.DataFrame
        input promotions dataframe

    Returns
    -------
    pandas.DataFrame
        updated promotions dataframe
    """
    return promos_df \
        .groupby(
            ["fu_cod", "rfa_cod", "to_dt", "promotion_mechanics_desc"]
        ) \
        .agg({"elapsed_time": "max"}) \
        .reset_index() \
        .groupby(["fu_cod", "rfa_cod", "to_dt", "elapsed_time"]) \
        .agg({"promotion_mechanics_desc": "first"}) \
        .reset_index()


def process_product_promos_means(
        level: str,
        product_promos_df: pd.DataFrame,
        product_orders_df: pd.DataFrame,
        shift_mapping: Dict[int, List[int]]
):
    assert level in ("shipment", "store")

    if level == "shipment":
        product_promos_df = product_promos_df \
            .apply(partial(shift_rfa_start_sellin, shift_mapping), axis=1)

    product_promos_df = product_promos_df \
        .pipe(remove_wrong_promos) \
        .pipe(
            expand_promos_to_daily,
            "promotion_start_date_expedition" if level == "shipment"
            else "promotion_start_date_store",
            "promotion_end_date_expedition" if level == "shipment"
            else "promotion_end_date_store",
            "to_dt") \
        .pipe(clean_promo)

    product_orders_df = product_orders_df \
        .groupby(["fu_cod", "rfa_cod", "to_dt"])["ordered_volumes"] \
        .sum() \
        .reset_index()

    product_promo_enriched_df = product_orders_df \
        .merge(
            product_promos_df,
            on=["to_dt", "rfa_cod", "fu_cod"],
            how="right") \
        .fillna(value=0) \
        .pipe(add_promo_columns)[[
            "fu_cod",
            "rfa_cod",
            "to_dt",
            "ordered_volumes",
            "promotion_mechanics_desc",
            "time_category",
            "promo_quantile"]] \
        .pipe(compute_all_promo_means)[[
            "fu_cod",
            "rfa_cod",
            "to_dt"] + [f"promo_mean_horizon_{i}" for i in horizons]] \
        .groupby(["fu_cod", "rfa_cod", "to_dt"])[[
            f"promo_mean_horizon_{i}" for i in horizons]] \
        .max() \
        .reset_index()

    return product_promo_enriched_df


def add_promo_columns(
        df: pd.DataFrame
) -> pd.DataFrame:
    quantile_days = df \
        .groupby(["fu_cod", "rfa_cod", "promotion_mechanics_desc"]) \
        .elapsed_time \
        .max() \
        .reset_index()
    quantile_days["quantile_days"] = quantile_days \
        .elapsed_time \
        .apply(lambda x: math.ceil(max(x, 1) / max(get_quantile(x), 1)))
    quantile_days["time_category"] = quantile_days \
        .elapsed_time \
        .apply(get_time_category)
    quantile_days = quantile_days.drop(columns=["elapsed_time"])

    df = df.merge(
        quantile_days, on=["rfa_cod", "fu_cod", "promotion_mechanics_desc"]
    )
    df["promo_quantile"] = (
        df.elapsed_time % df.quantile_days
    )
    return df


def get_time_category(x: int) -> int:
    if x < 14:
        return 0
    elif x < 28:
        return 1
    elif x < 42:
        return 2
    else:
        # For promotions that lasts more than 5 weeks
        return 3


def get_quantile(x: int) -> int:
    if x < 14:
        return 7
    elif x < 28:
        return 14
    elif x < 42:
        return 6
    else:
        return 4


def compute_all_promo_means(
        df: pd.DataFrame
) -> pd.DataFrame:
    for horizon in horizons:
        df = compute_promo_means(df, horizon)
    return df


def compute_promo_means(
        df: pd.DataFrame, horizon: int
) -> pd.DataFrame:
    columns_to_keep = list(df.columns)
    df[f"promo_mean_horizon_{horizon}"] = np.nan
    df = df.merge(
        df
        .groupby(["fu_cod", "rfa_cod", "promotion_mechanics_desc"])
        .to_dt
        .min()
        .reset_index()
        .rename(columns={"to_dt": "min_promo_date"}),
        on=["fu_cod", "rfa_cod", "promotion_mechanics_desc"],
        how="left"
    )
    df["min_promo_date"] = df.min_promo_date - timedelta(days=horizon)
    df = df.sort_values("min_promo_date")

    for agg_level_number, agg_level_columns in AGG_LEVELS.items():
        if df[f"promo_mean_horizon_{horizon}"].isna().sum() == 0:
            break
        agg_level_df = df \
            .groupby(agg_level_columns) \
            .agg({"ordered_volumes": "mean", "to_dt": "min"}) \
            .reset_index() \
            .rename(columns={"to_dt": "min_promo_date"}) \
            .dropna(subset=["ordered_volumes"]) \
            .sort_values("min_promo_date")
        agg_level_df["ordered_volumes"] = agg_level_df \
            .groupby(agg_level_columns)["ordered_volumes"] \
            .transform(lambda x: x.rolling(5, 1).mean())
        df = pd.merge_asof(
            df,
            agg_level_df,
            by=agg_level_columns,
            on="min_promo_date",
            direction="backward",
            suffixes=(None, f"_{agg_level_number}"),
            allow_exact_matches=False
        )
        df[f"promo_mean_horizon_{horizon}"] = df[f"promo_mean_horizon_{horizon}"].fillna(
            df[f"ordered_volumes_{agg_level_number}"]
        )
    return df[columns_to_keep + [f"promo_mean_horizon_{horizon}"]]
