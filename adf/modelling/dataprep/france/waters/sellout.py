import logging
from typing import Callable, Dict, List, Union
import numpy as np  # type: ignore
import pandas as pd  # type: ignore
from sqlalchemy.orm import Session  # type: ignore
from adf import config
from adf.services.database import (
    BusinessUnits,
    SelloutIBPReports
)
from adf.services.database.query.reports import (
    build_report,
    update_report
)
from adf.modelling.tools import get_snowflake_df
from adf.modelling.dataprep.utils.core import safe_merge
from adf.modelling.dataprep.utils.iri import (
    extract_ean_from_iri,
    extract_rfa_from_iri,
    resample_to_weekly,
    resample_sellout_to_daily,
    aggregate_iri_data
)
from adf.modelling.preprocessing.tools.aggregator import most_frequent_value

log = logging.getLogger("adf")


def run(
        session: Session, country: str, division: str, product_column: str,
        orders_df: pd.DataFrame, panel_promos_no_promos: pd.DataFrame,
        sku_fu_mapping: pd.DataFrame, sku_ean_df: pd.DataFrame, **kwargs
) -> pd.DataFrame:
    """
    Extracts and processes sellout data. Along the pipeline, exports data into dedicated
    storage in order to make it available for IBP requests

    Parameters
    ----------
    session: session: sqlalchemy.orm.Session
    country: str
        "france"
    division: str
        "waters"
    product_column: str
        target product column
    orders_df: pandas.DataFrame
        input orders dataframe containing baseline and promos orders
    panel_promos_no_promos: pd.DataFrame
        dataframe containing daily promos information at store level (obtained from promos
        dataprep)
    sku_fu_mapping: pandas.DataFrame
        input sku/fu mapping dataframe
    sku_ean_df: pandas.DataFrame
        input sku/ean mapping dataframe
    kwargs

    Returns
    -------
    pandas.DataFrame
        processed sellout dataframe at fu x rfa x weekly level
    """

    # Load Business Unit
    business_unit = session.query(BusinessUnits) \
        .filter(BusinessUnits.country == country) \
        .filter(BusinessUnits.division == division) \
        .one()

    # Build dedicated report
    report = build_report(
        session,
        SelloutIBPReports,
        business_unit=business_unit,
        description="Processing sellout data",
        status="processing",
        reason="pending_build",
        pipeline_id=kwargs.get("pipeline_id", None)
    )

    sellout_df = get_and_process_sellout(sku_ean_df, sku_fu_mapping) \
        .pipe(split_ean_sellout_volumes_per_fu, orders_df, panel_promos_no_promos) \
        .groupby(["fu_cod", "rfa_cod", "to_dt"]) \
        .sellout \
        .sum() \
        .reset_index()

    # Export data
    export_df = sellout_df \
        .copy() \
        .pipe(format_sellout_export)
    update_report(session, report, "processing", "pending_export")
    report.upload(export_df, sep=';')
    update_report(session, report, "success", None)

    # Return resampled daily sellout for usage in dataprep
    return sellout_df \
        .pipe(resample_sellout_to_daily, "sellout", "fu_cod", "rfa_cod", "to_dt") \
        .pipe(aggregate_iri_data, "sellout", by=[product_column, "rfa_cod", "to_dt"])


def get_and_process_sellout(
        sku_ean_df: pd.DataFrame, sku_fu_mapping: pd.DataFrame
):
    """
    Extracts and processes sellout data into a weekly dataframe

    Parameters
    ----------
    sku_ean_df: pd.DataFrame
        input sku/ean mapping
    sku_fu_mapping: pd.DataFrame
        input sku/fu mapping

    Returns
    -------
    pd.DataFrame
        enriched weekly sellout dataframe
    """
    return get_snowflake_df(
        config.SNOWFLAKE_DATABASE, "VCP_DSP_AFC_WTR_FRC", "F_MAN_WTR_FRA_SEL_OUT_RFA") \
        .drop_duplicates() \
        .pipe(extract_ean_from_iri, "product", "ean_cod") \
        .pipe(extract_rfa_from_iri, "rfa", "rfa_cod") \
        .groupby(["ean_cod", "rfa_cod", "to_dt"])["sellout_ean"] \
        .sum() \
        .reset_index() \
        .pipe(resample_to_weekly) \
        .pipe(safe_merge, "iri", sku_ean_df, "sku_ean_mapping",
              warning_type="warning", on="ean_cod", how="inner",
              validate="many_to_many") \
        .pipe(safe_merge, "iri", sku_fu_mapping, "sku_fu",
              warning_type="warning", on="mat_cod", how="inner",
              validate="many_to_one") \
        .drop(["mat_cod"], axis=1) \
        .drop_duplicates()


def split_ean_sellout_volumes_per_fu(
        sellout_df: pd.DataFrame, orders_df: pd.DataFrame, panel_promos_no_promos: pd.DataFrame
) -> pd.DataFrame:
    """
    Adds a new "sellout" column corresponding to FU sellout volumes. This new volumes is a split
    of EAN sellout volumes between the several FU associated to each EAN.

    Parameters
    ----------
    sellout_df: pd.DataFrame
        input weekly sellout dataframe
    orders_df: pd.DataFrame
        input daily baseline and promos orders dataframe
    panel_promos_no_promos: pd.DataFrame
        input dataframe containing promotions information at daily and store level

    Returns
    -------
    pd.DataFrame
        weekly sellout dataframe with sellout volumes per FU
    """
    orders_weekly_df = aggregate_to_monday_starting_weeks(
        orders_df,
        aggregation={
            k: "sum"
            for k in ("ordered_volumes", "ordered_volumes_baseline", "ordered_volumes_promo")
        }
    )
    orders_promos_no_promos_weekly_df = aggregate_to_monday_starting_weeks(
        panel_promos_no_promos,
        aggregation={"promotion_mechanics_desc": most_frequent_value}
    )

    sellout_df = sellout_df \
        .merge(
            orders_weekly_df.merge(
                orders_promos_no_promos_weekly_df,
                on=["fu_cod", "rfa_cod", "to_dt"],
                how="left",
                validate="one_to_one"
            ),
            how="left",
            on=["fu_cod", "rfa_cod", "to_dt"],
            validate="many_to_one") \
        .fillna(
            {
                "ordered_volumes_baseline": 0,
                "ordered_volumes_promo": 0,
                "promotion_mechanics_desc": "no_promo"
            })

    # Add count of associated FU for each EAN
    sellout_df["count_fu_cod"] = sellout_df \
        .groupby("ean_cod")["fu_cod"] \
        .transform("nunique")

    # Calculate sellout volumes per FU, splitting EAN sellout volumes between associated FUs
    # This will depend on the number of associated FU per EAN
    mask_fu_count = (sellout_df.count_fu_cod > 1)  # multi-associations
    # Work on multi-associations
    ean_fu_volumes = create_ordered_volumes_custom_rolling_sums(
        sellout_df,
        mask_fu_count,
        group_level=["ean_cod", "fu_cod", "rfa_cod", "to_dt"],
        level_suffix="FU_EAN"
    )
    ean_volumes = create_ordered_volumes_custom_rolling_sums(
        sellout_df,
        mask_fu_count,
        group_level=["ean_cod", "rfa_cod", "to_dt"],
        level_suffix="EAN"
    )

    weights = sellout_df[mask_fu_count].merge(
        ean_fu_volumes.merge(
            ean_volumes,
            on=["ean_cod", "rfa_cod", "to_dt"],
            how="inner",
            validate="many_to_one"
        ),
        how="left",
        on=["ean_cod", "fu_cod", "rfa_cod", "to_dt"],
        validate="one_to_one"
    )

    for suffix in ("no_promos", "promos_D02", "promos_no_D02"):
        weights["weight_" + suffix] = \
            weights["rolling_sum_ordered_volumes_" + suffix + "_FU_EAN"] / weights[
                "rolling_sum_ordered_volumes_" + suffix + "_EAN"]

    # Correct wrong weights
    for suffix in ("no_promos", "promos_D02", "promos_no_D02"):
        col = "weight_" + suffix
        weights.loc[(weights[col] < 0) | (weights[col] > 1), col] = np.NaN

    mask_no_promo = (weights.promotion_mechanics_desc == "no_promo")
    weights.loc[mask_no_promo, "sellout"] = \
        weights.loc[mask_no_promo, "weight_no_promos"] * weights.loc[mask_no_promo, "sellout_ean"]

    mask_promos_d02 = (
        (weights.promotion_mechanics_desc != "no_promo") & (
            ~(weights.weight_promos_D02.isna()) | (weights.weight_promos_D02 > 0))
    )
    weights.loc[mask_promos_d02, "sellout"] = \
        weights.loc[mask_promos_d02, "weight_promos_D02"] * \
        weights.loc[mask_promos_d02, "sellout_ean"]

    mask_promos_no_d02 = (
        (weights.promotion_mechanics_desc != "no_promo") & (
            (weights.weight_promos_D02.isna()) | (weights.weight_promos_D02 == 0))
    )
    weights.loc[mask_promos_no_d02, "sellout"] = \
        weights.loc[mask_promos_no_d02, "weight_promos_no_D02"] * \
        weights.loc[mask_promos_no_d02, "sellout_ean"]

    sellout_df = sellout_df \
        .drop(
            ["ordered_volumes_baseline", "ordered_volumes_promo", "promotion_mechanics_desc"],
            axis=1)\
        .merge(
            weights[["ean_cod", "fu_cod", "rfa_cod", "to_dt", "sellout"]],
            on=["ean_cod", "fu_cod", "rfa_cod", "to_dt"],
            how="left",
            validate="many_to_one"
        )

    # Work on single-associations
    # If only one FU is linked to one EAN, then sellout_ean = sellout_fu
    sellout_df.loc[~mask_fu_count, "sellout"] = sellout_df.loc[~mask_fu_count, "sellout_ean"]

    # Use uniform splits between FU from EAN volumes in case of NaN in resulting sellout volume
    mask = (sellout_df.sellout.isna())
    sellout_df.loc[mask, "sellout"] = \
        sellout_df.loc[mask, "sellout_ean"] / sellout_df.loc[mask, "count_fu_cod"]

    return sellout_df


def aggregate_to_monday_starting_weeks(
        df: pd.DataFrame, aggregation: Dict[str, Union[str, Callable]]
) -> pd.DataFrame:
    """
    Return aggregated dataframe with dates being mondays of each week.
    Aggregation is made using the aggregation dictionary provided as a parameter.

    Parameters
    ----------
    df: pandas.DataFrame
        input dataframe to aggregate
    aggregation: Dict[str: Union[str, Callable]]
        description of aggregation methods for each column to aggregate

    Returns
    -------
    pandas.DataFrame
        aggregated dataframe at fu x rfa x week level
    """
    return df \
        .groupby([
            "fu_cod",
            "rfa_cod",
            pd.Grouper(key="to_dt", freq="W-MON", closed="left", label="left")]) \
        .agg(aggregation) \
        .reset_index()


def create_ordered_volumes_custom_rolling_sums(
        df: pd.DataFrame, mask: pd.Series, group_level: Union[str, List[str]],
        level_suffix: str
) -> pd.DataFrame:
    """
    Returns dataframe containing rolling sums on ordered promos or baseline volumes for "no_promo",
    "promos with D02 tag" and "promos without D02 tag" at group_level level.
    Input dataframe is filtered using a mask.

    Business rules are as follows, for each group_level index: \
    - "No promo": rolling sum is made on baseline volumes of 3 previous weeks  \
    (excluding current week) \
    - "Promo with D02": rolling sum is made on promos volumes of 6 previous weeks  \
    (excluding current week) \
    - "Promo without D02": rolling sum is made on baseline volumes of 6 previous weeks  \
    (excluding current week) \

    Parameters
    ----------
    df: pandas.DataFrame
        input sellout dataframe at weekly level
    mask: pandas.Series
        mask to apply to input sellout dataframe to work on
    group_level: str, List[str]
        columns on which to group
    level_suffix: str
        suffix name to use to name generated column. It should give an indication on groupby level

    Returns
    -------
    pandas.DataFrame
        dataframe at group_level with generated rolling sums columns
    """
    no_promos = df[mask] \
        .groupby(group_level) \
        .ordered_volumes_baseline \
        .sum() \
        .shift() \
        .rolling(3, min_periods=1) \
        .sum() \
        .rename("rolling_sum_ordered_volumes_no_promos_" + level_suffix)
    promos_d02 = df[mask] \
        .groupby(group_level) \
        .ordered_volumes_promo \
        .sum() \
        .shift() \
        .rolling(6, min_periods=1) \
        .sum() \
        .rename("rolling_sum_ordered_volumes_promos_D02_" + level_suffix)
    promos_no_d02 = df[mask] \
        .groupby(group_level) \
        .ordered_volumes_baseline \
        .sum() \
        .shift() \
        .rolling(6, min_periods=1) \
        .sum() \
        .rename("rolling_sum_ordered_volumes_promos_no_D02_" + level_suffix)
    return pd \
        .concat(
            [no_promos, promos_d02, promos_no_d02],
            ignore_index=False,
            axis=1) \
        .reset_index()


def format_sellout_export(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Formats sellout input dataframe to meet IBP exports requirements

    Parameters
    ----------
    df: pd.DataFrame
        input sellout dataframe

    Returns
    -------
    pd.DataFrame
        formatted dataframe
    """
    df["sales_organization_cod"] = "0046"
    df["fu_cod"] = df["fu_cod"].apply(lambda x: "0" + str(x) if len(str(x)) < 6 else x)
    df["rfa_cod"] = df["rfa_cod"].apply(lambda x: "0" + str(x) if len(str(x)) < 6 else x)
    df["sellout"] = round(df["sellout"], 6)
    return df[[
        "sales_organization_cod",
        "fu_cod",
        "rfa_cod",
        "to_dt",
        "sellout"
    ]]
