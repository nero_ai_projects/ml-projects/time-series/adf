import traceback
import logging
from datetime import date
from dateutil.relativedelta import relativedelta
import pandas as pd  # type: ignore
import numpy as np  # type: ignore
from adf.errors import BuildingBlocksError
from adf.utils.decorators import timeit


log = logging.getLogger("adf")

uplift_cols = [
    "1_uplift",
    "6_uplift",
    "7_uplift",
    "8_uplift"
]

type_cols = [
    0,
    1,
    6,
    7,
    8
]


@timeit
def build_bb(promo_df):
    try:
        promo_df = promo_df.pipe(scope_promo, "req_delivery_date")
        promo_df = cast_and_sort_promo(promo_df)
        uplift_df = promo_df.groupby(
            ["mat_cod", "rfa_cod"], as_index=False
        ).apply(add_uplift_per_franchise)

        uplift_df = uplift_df.dropna(subset=uplift_cols, how='all')
        float_cols = list(uplift_df.select_dtypes(include=['float64', 'float32']).columns)
        int_cols = list(uplift_df.select_dtypes(include=['int']).columns)
        uplift_df[float_cols] = uplift_df[float_cols].fillna(0.0)
        uplift_df[int_cols] = uplift_df[int_cols].fillna(0)
        uplift_df = aggregate_uplifts(uplift_df)

        return uplift_df.to_pandas()

    except Exception as err:
        traceback.print_exc()
        message = str(err.__class__.__name__) + ": " + str(err)
        raise BuildingBlocksError(message) from err


def scope_promo(df, date_column):
    today = date.today()
    start_date = (today + relativedelta(months=-24)).strftime("%Y-%m-%d")
    end_date = today.strftime("%Y-%m-%d")

    return df.loc[df[date_column].between(start_date, end_date)]


def cast_and_sort_promo(promo_df):
    promo_df = promo_df.astype(
        {
            "mat_cod": "int32",
            "rfa_cod": "int32",
            "ordered_volumes": "int32"
        })
    return promo_df.sort_values(by=["req_delivery_date", "mat_cod", "rfa_cod"])


def add_uplift_per_franchise(promo_df):
    return promo_df. \
        pipe(create_uplift_table). \
        pipe(regularize_volumes). \
        pipe(add_annual_means). \
        pipe(compute_uplifts)


def create_uplift_table(df_franchise):
    df_franchise = df_franchise.reset_index(drop=True)
    uplift_df = pd.pivot_table(
        df_franchise,
        values="ordered_volumes",
        index=["rfa_cod", "req_delivery_date", "mat_cod"],
        columns=["promotion_mechanics_cod"],
        aggfunc=np.sum
    ).reset_index()
    uplift_df = uplift_df.fillna(0)
    for col in uplift_cols:
        if col not in uplift_df.columns:
            uplift_df[col] = 0

    reindex_cols = ["rfa_cod", "req_delivery_date", "mat_cod"] + type_cols
    uplift_df = uplift_df.reindex(columns=reindex_cols)

    uplift_df = uplift_df.groupby(
        ["rfa_cod", "req_delivery_date", "mat_cod"]
    ).agg({el: "sum" for el in type_cols}).reset_index()
    return uplift_df


def regularize_volumes(uplift_df):
    """
    Regularizes volumes if there are sales for non-promo and promo the same time
    """
    reg_cols = type_cols.copy()
    reg_cols.remove(0)
    for col in reg_cols:
        uplift_df.loc[(uplift_df[col] > uplift_df[0]), col] = \
            uplift_df[0] + uplift_df[col]
        uplift_df.loc[(uplift_df[col] > uplift_df[0]), 0] = 0

        uplift_df.loc[(uplift_df[0] > uplift_df[col]), 0] = \
            uplift_df[0] + uplift_df[col]
        uplift_df.loc[(uplift_df[0] > uplift_df[col]), col] = 0

    return uplift_df


def add_annual_means(uplift_df):
    for column in type_cols:
        mean_df = uplift_df.loc[uplift_df[column] > 0]
        mean_df = mean_df.groupby(["mat_cod", "rfa_cod"])[column].\
            mean().reset_index()
        mean_df = mean_df.rename(columns=({column: f"{column}_annual_mean"}))
        uplift_df = uplift_df.merge(
            mean_df,
            on=["mat_cod", "rfa_cod"],
            how="left"
        )

    return uplift_df


def compute_uplifts(uplift_df):
    for col in uplift_cols:
        annual_col = col.split("_", 1)[0] + "_annual_mean"
        uplift_df[col] = uplift_df[annual_col] / uplift_df["0_annual_mean"]

    uplift_df = uplift_df.sort_values(by=["req_delivery_date", "mat_cod"])
    uplift_df[uplift_cols] = uplift_df[uplift_cols] - 1
    uplift_df[uplift_cols] = uplift_df[uplift_cols].clip(0)
    return uplift_df


def aggregate_uplifts(uplift_df):
    agg = {col: "mean" for col in uplift_cols}
    uplift_df = uplift_df.groupby(
        ["mat_cod", "rfa_cod"]
    ).agg(agg).reset_index()
    return uplift_df
