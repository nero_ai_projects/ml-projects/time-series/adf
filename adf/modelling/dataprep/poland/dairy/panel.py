import logging
from datetime import date, timedelta
import pandas as pd  # type: ignore
from adf.errors import DataprepError
from adf.modelling import mllogs
from adf.utils.dataframe import memory_reduction
from adf.services.database.query.reports import update_report
from adf.modelling.dataprep.poland.dairy.promo import run as run_promo_dataprep
from adf.modelling.dataprep.poland.dairy.promo_means import run as run_promo_means
from adf.modelling.dataprep.utils.core import (
    build_panel_indexes,
    build_end_date,
    build_dataprep_report
)
from adf.modelling.dataprep.utils.holidays import (
    add_holidays_count_in_week,
    add_weekday_of_holidays,
    get_holidays_df
)
from adf.modelling.dataprep.utils.orders import (
    add_future_orders_of_n_previous_days,
    remove_innovations,
    add_future_orders_until_weekday_of_n_previous_weeks
)
from adf.modelling.preprocessing.tools.aggregator import most_frequent_value
from adf.modelling.tools import get_snowflake_df
from adf.utils.decorators import timeit
from adf.modelling.dataprep.utils.plc import apply_plc

log = logging.getLogger("adf")

CONVERSION_COLS = {
    "ordered_quantity": "ordered_volumes",
    "service_level_diff": "service_level_difference",
}


def run(country: str, division: str, product_level: str, location_level: str, **context):
    """Builds panel data for Poland Dairy given product_level and location_level.
    :product_level: sku
    :location_level: regional (rfa code), national (country code)
    """
    assert product_level == "sku"
    assert location_level in ("regional", "national")

    product_column = {
        "sku": "mat_cod"
    }[product_level]

    location_column = {
        "regional": "rfa_cod",
        "national": "country_cod",
    }[location_level]

    session = context["session"]

    report = build_dataprep_report(
        session,
        country,
        division,
        product_level,
        location_level,
        scope="panel",
        split_type="",
        split_code=None,
        prep_type="core",
        pipeline_id=context.get("pipeline_id", None),
    )

    mllogs.log_dataprep_report(report)

    try:
        log.info("Building panel")
        panel_df = build_panel_data(product_column, location_column)
        mllogs.log_shape(panel_df)

        log.info("Saving")
        report.upload(panel_df, partition_on=["rfa_cod"])
        update_report(session, report, "success")
        mllogs.tag_report_output(report)

    except Exception as err:
        message = str(err.__class__.__name__) + ": " + str(err)
        update_report(session, report, "failed", message)
        raise DataprepError(message)

    finally:
        mllogs.tag_report_output(report)

    return report, None


def build_panel_data(product_column: str, location_column: str) -> pd.DataFrame:

    # LOAD AND PROCESS DATA SOURCES
    products_df = get_snowflake_df(
        "DEV_VCP", "VCP_DSP_AFC_EDP_PLD", "R_SAP_EDP_POL_PDT_HIE"
    ).pipe(process_products)

    customers_df = get_snowflake_df(
        "DEV_VCP", "VCP_DSP_AFC_EDP_PLD", "R_SAP_EDP_POL_CUS_HIE_NFA_RFA"
    ).pipe(process_customers)

    sku_replacements_df = get_snowflake_df(
        "DEV_VCP", "VCP_DTM_AFC", "EDP_PLD_SKU_REP_MAP"
    )
    in_outs_sku_list = get_snowflake_df(
        "DEV_VCP", "VCP_DTM_AFC", "EDP_PLD_IN_OUT_PDT"
    )[product_column].unique().tolist()

    holidays_df = (
        get_holidays_df("poland", "dayoff")
        .pipe(memory_reduction)
        .pipe(add_weekday_of_holidays, "date")
        .pipe(add_holidays_count_in_week, "date")
        .rename({"date": "req_delivery_date"}, axis=1)
    )

    orders_df = (
        get_snowflake_df(
            "DEV_VCP", "VCP_DSP_AFC_EDP_PLD", "D_SAP_EDP_POL_DRY_ORD")
        .pipe(filter_orders_data)
        .merge(products_df[["mat_cod", "mat_weight_unit", "mat_weight_value"]], on="mat_cod",
               how="inner", validate="many_to_one")
        .pipe(process_orders, sku_replacements_df)
        .merge(customers_df[["cus_cod", "rfa_cod", "level6_cus_cod"]], on="cus_cod",
               how="inner", validate="many_to_one")
        .pipe(
            remove_innovations,
            product_column,
            "req_delivery_date",
            min_depth_history=3,
            depth_unit="M",
            view_from_date=date.today().strftime("%Y-%m-%d")
        )
        .pipe(remove_in_outs, in_outs_sku_list)
        .pipe(replace_rfa_cods)
        .pipe(aggregate_panel, product_column, "rfa_cod")
    )
    orders_df["warehouse_cod"] = "0694"

    # Build panel dataframe
    end_date = build_end_date(lag_type="weeks", lag_value=15)
    panel_indexes = build_panel_indexes(
        orders_df, end_date, product_column, "rfa_cod", "req_delivery_date"
    )
    panel_df = (
        panel_indexes
        .merge(orders_df, on=list(panel_indexes.columns), how="left", validate="one_to_one")
        .fillna(
            value={
                col: 0 for col in orders_df.columns
                if ("ordered_volumes" in col) and ("future" not in col)
            }
        )
        .pipe(add_country_cod)
    )

    # Build promos dataframes
    reduced_panel_df = panel_df[
        [
            "mat_cod",
            "rfa_cod",
            "req_delivery_date",
            "ordered_volumes"
        ]
    ]
    promo_means = run_promo_means(
        reduced_panel_df, customers_df
    )
    promo_uplifts = run_promo_dataprep(
        reduced_panel_df, customers_df
    )

    # Aggregate panel dataframe
    if location_column == "rfa_cod":
        panel_df = (
            panel_df
            .merge(aggregate_customers(customers_df, location_column),
                   on="rfa_cod", how="left", validate="many_to_one")
            .merge(promo_uplifts, on=["mat_cod", "req_delivery_date", "rfa_cod"],
                   how="left", validate="one_to_one")
            .merge(promo_means, on=["mat_cod", "rfa_cod", "req_delivery_date"],
                   how="left", validate="one_to_one")
        )
    else:
        promo_cols = [x for x in promo_means.columns if "promo_mean_horizon" in x]
        promo_means = (
            promo_means
            .groupby(["mat_cod", "req_delivery_date"])[promo_cols]
            .sum()
            .reset_index()
        )
        promo_uplifts = (
            promo_uplifts
            .groupby(["mat_cod", "req_delivery_date"])
            .total_uplifts.sum()
            .reset_index()
        )
        panel_df = (
            panel_df
            .pipe(aggregate_panel, product_column, location_column)
            .merge(promo_uplifts, on=["mat_cod", "req_delivery_date"],
                   how="left", validate="one_to_one")
            .merge(promo_means, on=["mat_cod", "req_delivery_date"],
                   how="left", validate="one_to_one")
        )

    panel_df = (
        panel_df
        .merge(aggregate_products(products_df, product_column),
               on="mat_cod", how="left", validate="many_to_one")
        .merge(holidays_df, on="req_delivery_date", how="left", validate="many_to_one")
        .pipe(remove_inactive_skus)
    )
    panel_df = apply_plc("poland", "dairy", panel_df, "snowflake")
    panel_df = remove_inactive_rfas(panel_df)

    return panel_df


@timeit
def process_products(products_df: pd.DataFrame) -> pd.DataFrame:
    return (
        products_df
        .pipe(filter_on_distribution_channel)
        .drop_duplicates()[
            [
                "mat_umbrella_cod",
                "mat_umbrella_desc",
                "mat_brand_cod",
                "mat_family_cod",
                "flavour_cod",
                "flavour_desc",
                "mat_cod",
                "mat_weight_unit",
                "mat_weight_value",
                "mat_units_number_per_sku",
            ]
        ]
    )


@timeit
def process_customers(customers_df: pd.DataFrame) -> pd.DataFrame:
    customers_df = (
        customers_df
        .drop_duplicates()
        .pipe(filter_on_distribution_channel)
        .query('level4_cus_cod!="#"')
    )

    customers_df.loc[customers_df["level6_cus_cod"] == 250017316, "rfa_cod"] = 20331
    customers_df.loc[customers_df["level6_cus_cod"] == 250173470, "rfa_cod"] = 20319
    customers_df.loc[customers_df["level6_cus_cod"] == 250075351, "rfa_cod"] = 20628
    customers_df.loc[customers_df["level6_cus_cod"] == 250109873, "rfa_cod"] = 20568
    return customers_df[
        [
            "cus_cod",
            "nfa_cod",
            "rfa_cod",
            "level6_cus_cod"
        ]
    ]


@timeit
def process_sellout(sellout_df: pd.DataFrame) -> pd.DataFrame:
    return (
        sellout_df.groupby(["sellout_offtake", "mat_cod", "cus_cod", "sellout_date"])[
            "sellout_vol_pc"
        ]
        .sum()
        .compute()
        .reset_index()
        .pipe(filter_out_sellout)
        .rename({"sellout_date": "req_delivery_date"}, axis=1)
    )


@timeit
def filter_orders_data(df: pd.DataFrame) -> pd.DataFrame:
    return (
        df
        .pipe(remove_rejected_orders)
        .pipe(filter_on_sales_type_cod)
        .pipe(filter_on_distribution_channel)
    )


@timeit
def process_orders(df: pd.DataFrame, sku_replacements_df: pd.DataFrame) -> pd.DataFrame:
    return (
        df
        .pipe(do_sku_replacements, sku_replacements_df)
        .pipe(add_service_level)
        .pipe(convert_to_tons, CONVERSION_COLS)
        .pipe(
            add_future_orders_of_n_previous_days,
            "req_delivery_date",
            "order_creation_date",
            "ordered_volumes",
            interval=range(0, 21))
        .pipe(
            add_future_orders_until_weekday_of_n_previous_weeks,
            "req_delivery_date",
            "order_creation_date",
            "ordered_volumes",
            interval=range(0, 5),
            until_weekday_included=(date.today() - timedelta(days=1)).weekday(),
            add_weekday_in_column_name=False)
        .drop(
            [
                "order_creation_date",
                "ordered_quantity",
                "delivered_quantity",
                "service_level_diff",
                "mat_weight_unit",
                "mat_weight_value",
            ],
            axis=1
        )
    )


def do_sku_replacements(
        orders_df: pd.DataFrame, sku_replacements_df: pd.DataFrame
) -> pd.DataFrame:
    sku_replacements_dict = {
        sku_replacements_df.loc[i, "old_mat_cod"]: sku_replacements_df.loc[i, "new_mat_cod"]
        for i, _ in sku_replacements_df.iterrows()
    }
    for product in orders_df["mat_cod"].unique():
        if product in sku_replacements_dict:
            orders_df.loc[
                orders_df["mat_cod"] == product, "mat_cod"
            ] = sku_replacements_dict[product]
    return orders_df


def convert_to_tons(df: pd.DataFrame, col_dict: dict) -> pd.DataFrame:
    mask_kg = df["mat_weight_unit"] == "KG"
    mask_g = df["mat_weight_unit"] == "G"
    mask_lb = df["mat_weight_unit"] == "LB"
    for old_col, new_col in col_dict.items():
        df.loc[mask_kg, new_col] = (
            df.loc[mask_kg, old_col] * df.loc[mask_kg, "mat_weight_value"] / 1000
        )
        df.loc[mask_g, new_col] = (
            df.loc[mask_g, old_col] * df.loc[mask_g, "mat_weight_value"] / 1000000
        )
        df.loc[mask_lb, new_col] = (
            (df.loc[mask_lb, old_col] * df.loc[mask_lb, "mat_weight_value"] * 0.453) / 1000
        )
    return df


def add_service_level(df: pd.DataFrame) -> pd.DataFrame:
    """
    Adds two new columns with the difference between
    delivered volumes and ordered ones as well the ratio
    """
    df["service_level_diff"] = df["ordered_quantity"] - df["delivered_quantity"]
    df["service_level"] = df["delivered_quantity"] / df["ordered_quantity"]
    return df


@timeit
def aggregate_panel(df: pd.DataFrame, product_column: str, location_column: str) -> pd.DataFrame:
    """
    Aggregates orders/panel dataframe to required granularity:
    product_column x location_column x target date
    """
    agg = {
        "service_level": "mean",
        "service_level_difference": "sum",
        **{k: "sum" for k in df.columns if "ordered_volumes" in k},
    }
    return (
        df
        .groupby(by=[product_column, location_column, "req_delivery_date"])
        .agg(agg)
        .reset_index()
    )


def add_country_cod(df: pd.DataFrame) -> pd.DataFrame:
    df.loc[:, "country_cod"] = 0
    return df


def aggregate_products(df: pd.DataFrame, product_column: str) -> pd.DataFrame:
    df = df.drop(["mat_weight_value", "mat_weight_unit"], axis=1)
    agg = {k: most_frequent_value for k in df.columns if k != product_column}
    return (
        df
        .groupby(by=[product_column])
        .agg(agg)
        .reset_index()
    )


def aggregate_customers(df: pd.DataFrame, location_column: str) -> pd.DataFrame:
    df = df.drop(["cus_cod"], axis=1)
    agg = {k: most_frequent_value for k in df.columns if k != location_column}
    return (
        df
        .groupby(by=[location_column])
        .agg(agg)
        .reset_index()
    )


def remove_in_outs(df: pd.DataFrame, in_outs_sku_list: list) -> pd.DataFrame:
    return df.query(f'mat_cod not in {in_outs_sku_list}')


def remove_rejected_orders(df: pd.DataFrame) -> pd.DataFrame:
    return df.query("rejection_cod in ['#', 'Z3','Z5','Z8','ZE','ZF','ZK']")


def filter_on_sales_type_cod(df: pd.DataFrame) -> pd.DataFrame:
    types_to_keep = [
        "ZCL1",
        "ZINT",
        "ZKB",
        "ZOR",
        "ZPF",
        "ZRE2",
        "ZRE3",
        "ZREV",
        "ZSO",
        "ZSO3",
        "ZSOV",
    ]
    return df.query(f"sal_doc_typ_cod.isin({types_to_keep})", engine="python")


def filter_on_distribution_channel(df: pd.DataFrame) -> pd.DataFrame:
    return df.query("distribution_channel_cod == 0 or distribution_channel_cod == '00'")


def filter_out_sellout(df: pd.DataFrame) -> pd.DataFrame:
    return df.query('sellout_offtake == "Offtake"')


def remove_inactive_skus(df: pd.DataFrame) -> pd.DataFrame:
    inactive_skus = [
        100473, 102662, 102664, 105408, 105691, 105995, 105996,
        106000, 106004, 106005, 106011, 106012, 107204, 108517,
        108532, 109167, 109670, 109728, 109729, 109731, 110043,
        111538, 111731, 112070, 112433, 112794, 112819, 112828,
        118445, 118448, 119392, 119954, 120073, 121592, 121671,
        124553, 124806, 124996, 127000, 127046, 127642, 128312,
        128320, 130679, 130682, 131463, 131464, 131858, 132424,
        134157, 134402, 135262, 135263, 135264, 135393, 135510,
        135594, 135596, 135597, 135598, 135948, 135950, 135959,
        136002, 136263, 136295, 138299, 138300, 138301, 138325,
        139085, 139678, 139798, 140481, 140482, 140791, 140882,
        140902, 140938, 141107, 141220, 143236, 144338, 144425,
        144431, 144432, 144434, 144435, 144481, 144485, 144486,
        144489, 144490, 144491, 144492, 144494, 144622, 144623,
        144634, 144649, 145300, 146371, 146436, 146588, 146589,
        146642, 146737, 147827, 147848, 147861, 148382, 149234,
        149857, 149980, 149992, 149993, 151399, 151403, 151421,
        151743, 151748, 151749, 151879, 152007, 153244, 153247,
        155016, 155017, 155769, 155867, 155869, 155870, 156228,
        156283, 156285, 156692, 157147, 157149, 158052, 158319,
        159213, 159397, 159403, 159404, 159927, 161507, 161508,
        161609, 162069, 162467, 162542, 162830, 163176, 163235,
        163412, 163505, 163537, 163877, 163878, 164104, 164766,
        164767, 166122, 167127, 167488, 168261, 169074, 169238,
        169239, 169592, 170300, 170321, 170430, 170907, 171049,
        171197, 172939, 173657, 174446, 34758, 60013, 60014,
        60015, 60725, 70799, 70802, 73769, 75248, 86361,
        86524, 90171, 90604, 90791, 91664, 94942, 98024,
        98699, 98700, 98879, 99344, 147396
    ]
    return df.loc[~df.mat_cod.isin(inactive_skus)]


def remove_inactive_rfas(df: pd.DataFrame) -> pd.DataFrame:
    inactive_rfas = [20415, 20419]
    return df.loc[~df.rfa_cod.isin(inactive_rfas)]


def replace_rfa_cods(df: pd.DataFrame) -> pd.DataFrame:
    df.loc[df.rfa_cod == 20594, "rfa_cod"] = 20319
    return df
