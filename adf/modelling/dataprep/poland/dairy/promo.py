import logging
import traceback
from datetime import timedelta
from functools import partial  # type: ignore

import pandas as pd  # type: ignore
import databricks.koalas as ks  # type: ignore
import numpy as np  # type: ignore
from adf.errors import DataprepError
from adf.modelling.dataprep.poland.dairy.building_blocks import build_bb
from adf.modelling.dataprep.utils.promos import expand_promos_to_daily
from adf.modelling.preprocessing.tools.aggregator import most_frequent_value
from adf.modelling.tools import get_snowflake_df
from adf.utils.dataframe import memory_reduction
from adf.utils.decorators import timeit
from adf.utils.provider import Provider


log = logging.getLogger("adf")
Provide = partial(Provider, mlrun=False, session=True, cluster=True)

SHIFT_MAPPING = {
    20323: [-3, 6],
    20324: [-4, -2],
    20325: [-4, 0],
    20331: [-4, 3],
    20568: [-5, 2],
    20330: [-5, -2],
    20561: [-5, -1],
    20334: [-4, 1],
    20564: [-5],
    20562: [-3, 6],
    20563: [-6, 1],
    20418: [-8, -1],
    20575: [0, 5],
    20417: [0, 6],
    20338: [-4, 6],
    20573: [0, 11]
}


def run(orders_df, customers_df):
    """ Build panel data for Poland Dairy
    """
    try:
        panel_ddf, panel_only_promo_ddf = build_promo_data(
            orders_df, customers_df
        )
        bb_table = build_bb(
            panel_ddf
        )
        return build_uplifts_promo(panel_only_promo_ddf, bb_table)

    except Exception as err:
        traceback.print_exc()
        message = str(err.__class__.__name__) + ": " + str(err)
        raise DataprepError(message)


@timeit
def build_promo_data(orders_df, customers_df):
    sku_replacements_df = get_snowflake_df(
        "DEV_VCP", "VCP_DTM_AFC", "EDP_PLD_SKU_REP_MAP"
    )

    customers_df = customers_df[['rfa_cod', 'level6_cus_cod']].drop_duplicates()

    log.info("Loading promos...")
    promo_df = (
        get_snowflake_df("DEV_VCP", "VCP_DSP_AFC_EDP_PLD", "R_BLP_EDP_POL_PRM_HEA")
        .pipe(preprocess_promo, sku_replacements_df)
        .merge(customers_df, on="rfa_cod", how="inner", validate="many_to_many")
    )

    log.info("Reducing memory of orders...")
    orders_df = memory_reduction(orders_df)
    log.info("Reducing memory of promos...")
    promo_df = memory_reduction(promo_df)

    promo_no_promo_df = multiprocess_product_orders_with_promos(promo_df, orders_df)
    only_promo_df = promo_no_promo_df.loc[promo_no_promo_df.promotion_mechanics_cod != 0]
    return promo_no_promo_df, only_promo_df


# TODO: What do we do with one-day promos ?
def multiprocess_product_orders_with_promos(
        promo_df: pd.DataFrame,
        orders_df: pd.DataFrame
):
    return ks.from_pandas(promo_df).groupby("mat_cod").apply(
        process_product_promo(orders_df)
    )


def process_product_promo(orders_df: pd.DataFrame):
    def process_product_promo_on_group(
        product_promo_df: pd.DataFrame
    ):
        mat_cod = list(product_promo_df.mat_cod.unique())[0]
        start_date = product_promo_df.promotion_start_date_sellin
        end_date = product_promo_df.promotion_end_date_sellin
        mask = start_date > end_date
        product_promo_df = product_promo_df.loc[~mask]
        product_orders_df = orders_df.loc[orders_df.mat_cod == mat_cod]

        product_promo_df = product_promo_df.apply(
            shift_franchises_start_sellin, axis=1
        ).pipe(
            expand_promos_to_daily, "promotion_start_date_sellin",
            "promotion_end_date_sellin", "req_delivery_date"
        ).pipe(
            discriminate_promo_codes
        ).pipe(
            clean_promo_new
        ).pipe(
            remove_sundays_promo
        )
        product_orders_df = product_orders_df. \
            groupby(['mat_cod', 'rfa_cod', 'req_delivery_date']). \
            sum(). \
            reset_index()

        product_promo_enriched_df = product_orders_df.merge(
            product_promo_df,
            on=['req_delivery_date', 'rfa_cod', 'mat_cod'],
            how='right'
        )
        product_promo_enriched_df = product_promo_enriched_df.fillna(value=0)

        product_orders_enriched_df = product_orders_df.merge(
            product_promo_df,
            on=['req_delivery_date', 'rfa_cod', 'mat_cod'],
            how='left'
        )
        product_orders_no_promo = product_orders_enriched_df[
            product_orders_enriched_df.promotion_mechanics_cod.isnull()
        ]
        product_orders_no_promo.loc[:, "promotion_mechanics_cod"] = 0
        result_df = pd.concat(
            [product_orders_no_promo, product_promo_enriched_df],
            axis=0
        )
        result_df = result_df[[
            "mat_cod",
            "req_delivery_date",
            "rfa_cod",
            "ordered_volumes",
            "promotion_mechanics_cod",
            "elapsed_time"
        ]]
        return result_df
    return process_product_promo_on_group


def preprocess_promo(promo_df: pd.DataFrame, sku_replacements_df: pd.DataFrame):
    promo_df.loc[promo_df["rfa_cod"] == 900000001, "rfa_cod"] = 250201997
    return do_sku_replacements(promo_df, sku_replacements_df)


def shift_franchises_start_sellin(row: pd.Series) -> pd.Series:
    """ Shift promo start date sellin date according to different customers
    behaviours
    """
    if row["rfa_cod"] in SHIFT_MAPPING.keys():
        rfa = row["rfa_cod"]
        row["promotion_start_date_sellin"] = \
            row["promotion_start_date_store"] + timedelta(days=SHIFT_MAPPING[rfa][0])
        if len(SHIFT_MAPPING[rfa]) > 1:
            row["promotion_end_date_sellin"] = \
                row["promotion_start_date_store"] + timedelta(days=SHIFT_MAPPING[rfa][1])
        else:
            row["promotion_end_date_sellin"] = row["promotion_end_date_store"]
    elif row["rfa_cod"] == 20319:
        if row["promotion_mechanics_cod"] == 8:
            row["promotion_start_date_sellin"] = row["promotion_start_date_store"]
            row["promotion_end_date_sellin"] = row["promotion_start_date_store"] + timedelta(days=5)
        elif row["promotion_mechanics_cod"] == 6:
            row["promotion_start_date_sellin"] \
                = row["promotion_start_date_store"] + timedelta(days=-7)
            row["promotion_end_date_sellin"] \
                = row["promotion_start_date_store"] + timedelta(days=-5)
    return row


def clean_promo(promo_df: pd.DataFrame):
    promo_df = promo_df.drop_duplicates(
        ["mat_cod", "rfa_cod", "promotion_mechanics_cod",
         "promotion_start_date_sellin", "promotion_end_date_sellin"],
        keep="first"
    )
    promo_df = promo_df.groupby(
        ['mat_cod', 'rfa_cod', 'promotion_start_date_sellin', 'promotion_end_date_sellin']
    ).agg({"promotion_mechanics_cod": most_frequent_value}).reset_index()

    promo_df = promo_df.groupby(
        ['mat_cod', 'rfa_cod', 'promotion_mechanics_cod', 'promotion_start_date_sellin']
    ).agg({"promotion_end_date_sellin": "max"}).reset_index()
    return promo_df


def clean_promo_new(promo_df: pd.DataFrame):
    promo_df = promo_df.groupby(
        ['mat_cod', 'rfa_cod', 'req_delivery_date', 'promotion_mechanics_cod']
    ).elapsed_time.max().reset_index()

    promo_df = promo_df.groupby(
        ['mat_cod', 'rfa_cod', 'req_delivery_date', 'elapsed_time']
    ).promotion_mechanics_cod.apply(
        lambda x: 6 if 6 in list(x) else 8 if 8 in list(x) else max(list(x))
    ).reset_index()
    return promo_df


def discriminate_promo_codes(promo_df: pd.DataFrame) -> pd.DataFrame:
    """
    Groups and separates promotion_mechanics_cods based on the type and moment of the promotion
    """
    mask = ~promo_df["promotion_mechanics_cod"].isin([6, 7, 8])
    promo_df.loc[mask, "promotion_mechanics_cod"] = 1
    return promo_df


def remove_sundays_promo(promo_df: pd.DataFrame) -> pd.DataFrame:
    """
    Removes Sunday rows
    """
    promo_df["weekday"] = promo_df["req_delivery_date"].dt.weekday
    promo_df = promo_df.loc[promo_df.weekday < 6]
    promo_df = promo_df.drop(columns=["weekday"], axis=1)
    return promo_df


def build_uplifts_promo(promo_dataprep, bb_table):
    promo_df = promo_dataprep.reset_index(drop=True).to_pandas()

    uplifts_df = promo_df.merge(
        bb_table,
        on=["mat_cod", "rfa_cod"],
        how="left"
    )
    uplifts_df["total_uplifts"] = uplifts_df.filter(regex="uplift").sum(axis=1)
    uplifts_df.loc[uplifts_df.req_delivery_date.isnull(), 'total_uplifts'] = np.NaN
    uplifts_df = uplifts_df.groupby(
        ["mat_cod", "req_delivery_date", "rfa_cod"]
    ).agg({"total_uplifts": "sum"}).reset_index()
    return uplifts_df


def do_sku_replacements(
        orders_df: pd.DataFrame, sku_replacements_df: pd.DataFrame
) -> pd.DataFrame:
    sku_replacements_dict = {
        sku_replacements_df.loc[i, "old_mat_cod"]: sku_replacements_df.loc[i, "new_mat_cod"]
        for i, _ in sku_replacements_df.iterrows()
    }
    for product in orders_df["mat_cod"].unique():
        if product in sku_replacements_dict:
            orders_df.loc[
                orders_df["mat_cod"] == product, "mat_cod"
            ] = sku_replacements_dict[product]
    return orders_df
