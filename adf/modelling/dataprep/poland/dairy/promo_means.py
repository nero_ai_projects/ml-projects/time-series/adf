import logging
import math
import traceback
from datetime import timedelta
from functools import partial  # type: ignore
import databricks.koalas as ks  # type: ignore
import numpy as np  # type: ignore
import pandas as pd  # type: ignore
from adf.errors import DataprepError
from adf.modelling.preprocessing.tools.aggregator import most_frequent_value
from adf.modelling.tools import get_snowflake_df
from adf.utils.dataframe import memory_reduction
from adf.utils.decorators import timeit
from adf.utils.provider import Provider

log = logging.getLogger("adf")
Provide = partial(Provider, mlrun=False, session=True, cluster=True)

SHIFT_MAPPING = {
    20323: [-3, 6],
    20324: [-4, -2],
    20325: [-4, 0],
    20331: [-4, 3],
    20568: [-5, 2],
    20330: [-5, -2],
    20561: [-5, -1],
    20334: [-4, 1],
    20564: [-5],
    20562: [-3, 6],
    20563: [-6, 1],
    20418: [-8, -1],
    20575: [0, 5],
    20417: [0, 6],
    20338: [-4, 6],
    20573: [0, 11],
}

AGG_LEVELS = {
    "1": ["mat_cod", "rfa_cod", "promotion_mechanics_cod", "time_category", "promo_quantile"],
    "2": ["mat_cod", "rfa_cod", "promotion_mechanics_cod", "time_category"],
    "3": ["mat_cod", "rfa_cod", "time_category"],
    "4": ["mat_cod", "rfa_cod", "promotion_mechanics_cod"],
    "5": ["mat_cod", "promotion_mechanics_cod", "time_category", "promo_quantile"],
    "6": ["mat_cod", "promotion_mechanics_cod", "time_category"],
    "7": ["mat_cod", "time_category"],
    "8": ["mat_cod", "promotion_mechanics_cod"],
    "9": ["mat_cod"],
}

HORIZONS = [0, 7, 14, 21, 28, 35, 42, 49, 56, 63, 70, 77, 84, 91, 98]


def run(orders_df, customers_df):
    """Build panel data for Poland Dairy"""
    try:
        promo_df = build_promo_data(orders_df, customers_df)
        return promo_df.to_pandas()
    except Exception as err:
        traceback.print_exc()
        message = str(err.__class__.__name__) + ": " + str(err)
        raise DataprepError(message) from err


@timeit
def build_promo_data(orders_df, customers_df):
    sku_replacements_df = get_snowflake_df(
        "DEV_VCP", "VCP_DTM_AFC", "EDP_PLD_SKU_REP_MAP"
    )

    customers_df = customers_df[["rfa_cod", "level6_cus_cod"]].drop_duplicates()

    log.info("Loading promos...")
    promo_df = (
        get_snowflake_df(
            "DEV_VCP", "VCP_DSP_AFC_EDP_PLD", "R_BLP_EDP_POL_PRM_HEA"
        )
        .pipe(preprocess_promo, sku_replacements_df)
        .merge(customers_df, on="rfa_cod", how="inner", validate="many_to_many")
    )

    log.info("Reducing memory of orders...")
    orders_df = memory_reduction(orders_df)
    log.info("Reducing memory of promos...")
    promo_df = memory_reduction(promo_df)

    promo_df = multiprocess_product_orders_with_promos(promo_df, orders_df)
    return compute_cannibalization(promo_df)


def preprocess_promo(promo_df: pd.DataFrame, sku_replacements_df: pd.DataFrame):
    promo_df.loc[promo_df["rfa_cod"] == 900000001, "rfa_cod"] = 250201997
    return do_sku_replacements(promo_df, sku_replacements_df)


def multiprocess_product_orders_with_promos(promo_df: pd.DataFrame, orders_df: pd.DataFrame):
    promo_df = ks.from_pandas(promo_df)
    return promo_df.groupby("mat_cod").apply(process_product_promo(orders_df))


def process_product_promo(orders_df):
    def process_product_promo_on_group(
        product_promo_df: pd.DataFrame
    ):
        mat_cod = list(product_promo_df.mat_cod.unique())[0]
        start_date = product_promo_df.promotion_start_date_sellin
        end_date = product_promo_df.promotion_end_date_sellin
        mask = start_date > end_date
        product_promo_df = product_promo_df.loc[~mask]
        product_orders_df = orders_df.loc[orders_df.mat_cod == mat_cod]
        product_promo_df = (
            product_promo_df
            .apply(shift_franchises_start_sellin, axis=1)
            .pipe(clean_promo)
            .pipe(expand_to_daily_promo)
            # .pipe(discriminate_promo_codes)
        )
        product_orders_df = (
            product_orders_df
            .groupby(["mat_cod", "rfa_cod", "req_delivery_date"])
            .sum()
            .reset_index()
        )

        product_promo_enriched_df = product_orders_df.merge(
            product_promo_df, on=["req_delivery_date", "rfa_cod", "mat_cod"], how="right"
        )
        product_promo_enriched_df = product_promo_enriched_df.fillna(value=0)

        product_promo_enriched_df = add_promo_columns(product_promo_enriched_df)

        product_promo_enriched_df = product_promo_enriched_df[
            [
                "promotion_cod",
                "mat_cod",
                "req_delivery_date",
                "rfa_cod",
                "ordered_volumes",
                "promotion_mechanics_cod",
                "time_category",
                "promo_quantile",
            ]
        ]
        product_promo_enriched_df = compute_all_promo_means(product_promo_enriched_df)

        product_promo_enriched_df = product_promo_enriched_df[
            [
                "mat_cod", "req_delivery_date", "rfa_cod"
            ] + [
                f"promo_mean_horizon_{i}" for i in HORIZONS
            ]
        ]

        product_promo_enriched_df = (
            product_promo_enriched_df.groupby(["mat_cod", "req_delivery_date", "rfa_cod"])[
                [f"promo_mean_horizon_{i}" for i in HORIZONS]
            ]
            .max()
            .reset_index()
        )
        log.info(
            f"Done processing {mat_cod}. "
            f"Still missing {product_promo_enriched_df.promo_mean_horizon_0.isna().sum()} "
            "promo_means..."
        )
        return product_promo_enriched_df
    return process_product_promo_on_group


def shift_franchises_start_sellin(row: pd.Series) -> pd.Series:
    """Shift promo start date sellin date according to different customers
    behaviours
    """
    if row["rfa_cod"] in SHIFT_MAPPING.keys():
        rfa = row["rfa_cod"]
        row["promotion_start_date_sellin"] = row["promotion_start_date_store"] + timedelta(
            days=SHIFT_MAPPING[rfa][0]
        )
        if len(SHIFT_MAPPING[rfa]) > 1:
            row["promotion_end_date_sellin"] = row["promotion_start_date_store"] + timedelta(
                days=SHIFT_MAPPING[rfa][1]
            )
        else:
            row["promotion_end_date_sellin"] = row["promotion_end_date_store"]
    elif row["rfa_cod"] == 20319:
        if row["promotion_mechanics_cod"] == 8:
            row["promotion_start_date_sellin"] = row["promotion_start_date_store"]
            row["promotion_end_date_sellin"] = row["promotion_start_date_store"] + timedelta(days=5)
        elif row["promotion_mechanics_cod"] == 6:
            row["promotion_start_date_sellin"] = row["promotion_start_date_store"] + timedelta(
                days=-7
            )
            row["promotion_end_date_sellin"] = row["promotion_start_date_store"] + timedelta(
                days=-5
            )
    return row


def clean_promo(promo_df: pd.DataFrame):
    promo_df = promo_df.drop_duplicates(
        [
            "mat_cod",
            "rfa_cod",
            "promotion_mechanics_cod",
            "promotion_start_date_sellin",
            "promotion_end_date_sellin",
        ],
        keep="first",
    )
    promo_df = promo_df.groupby(
        ["mat_cod", "rfa_cod", "promotion_start_date_sellin", "promotion_end_date_sellin"],
        as_index=False,
    ).apply(
        lambda group: group.loc[
            group.promotion_mechanics_cod == most_frequent_value(group.promotion_mechanics_cod)
        ]
    )
    promo_df = promo_df.groupby(
        ["mat_cod", "rfa_cod", "promotion_mechanics_cod", "promotion_start_date_sellin"],
        as_index=False,
    ).apply(
        lambda group: group.loc[
            group.promotion_end_date_sellin == group.promotion_end_date_sellin.max()
        ]
    )
    promo_df = promo_df.groupby(
        ["mat_cod", "rfa_cod", "promotion_mechanics_cod", "promotion_end_date_sellin"],
        as_index=False,
    ).apply(
        lambda group: group.loc[
            group.promotion_start_date_sellin == group.promotion_start_date_sellin.min()
        ]
    )
    return promo_df


def expand_to_daily_promo(promo_df: pd.DataFrame) -> pd.DataFrame:
    """
    Creates a date column and adds a row for each dates between start date and end date
    """
    promo_df["time_delta"] = (
        promo_df.promotion_end_date_sellin - promo_df.promotion_start_date_sellin
    )
    max_delta = max(promo_df.time_delta.dt.days)
    elapsed_times = pd.concat(
        [
            pd.DataFrame({"elapsed_time": np.arange(0, delta + 1)}).assign(
                time_delta=pd.to_timedelta(delta, unit="d")
            )
            for delta in range(max_delta + 1)
        ]
    )
    promo_df = pd.merge(promo_df, elapsed_times, on="time_delta")
    promo_df["req_delivery_date"] = promo_df.promotion_start_date_sellin + pd.to_timedelta(
        promo_df.elapsed_time, unit="d"
    )
    promo_df = promo_df.loc[promo_df.req_delivery_date <= promo_df.promotion_end_date_sellin]
    promo_df = promo_df.drop(
        columns=["time_delta", "promotion_start_date_sellin", "promotion_end_date_sellin"]
    )
    promo_df = promo_df.groupby(
        ["mat_cod", "rfa_cod", "req_delivery_date", "promotion_mechanics_cod"], as_index=False
    ).apply(lambda group: group.loc[group.elapsed_time == group.elapsed_time.max()])
    promo_df = promo_df.groupby(
        ["mat_cod", "rfa_cod", "req_delivery_date", "elapsed_time"], as_index=False
    ).apply(prioritize_duplicates)
    return promo_df


def discriminate_promo_codes(promo_df: pd.DataFrame) -> pd.DataFrame:
    """
    Groups and separates promotion_mechanics_cods based on the type and moment of the promotion
    """
    mask = ~promo_df["promotion_mechanics_cod"].isin([6, 7, 8])
    promo_df.loc[mask, "promotion_mechanics_cod"] = 1
    return promo_df


def prioritize_duplicates(group):
    first_mechanic = group.loc[group.promotion_mechanics_cod == 6]
    second_mechanic = group.loc[group.promotion_mechanics_cod == 8]
    if len(first_mechanic):
        return first_mechanic
    elif len(second_mechanic):
        return second_mechanic
    else:
        return group.loc[group.promotion_mechanics_cod == group.promotion_mechanics_cod.max()]


def add_promo_columns(df):
    quantile_days = (
        df.groupby(["mat_cod", "rfa_cod", "promotion_cod"]).elapsed_time.max().reset_index()
    )
    quantile_days["quantile_days"] = quantile_days.elapsed_time.apply(
        lambda x: math.ceil(max(x, 1) / max(get_quantile(x), 1))
    )
    quantile_days["time_category"] = quantile_days.elapsed_time.apply(get_time_category)
    quantile_days = quantile_days.drop(columns=["elapsed_time"])

    df = df.merge(quantile_days, on=["rfa_cod", "mat_cod", "promotion_cod"])
    df["promo_quantile"] = df.elapsed_time % df.quantile_days
    return df


def get_time_category(x):
    if x < 7:
        return 0
    elif x < 14:
        return 1
    elif x < 21:
        return 2
    elif x < 42:
        return 3
    elif x < 84:
        return 4
    else:
        return 5


def get_quantile(x):
    if x < 7:
        return 7
    elif x < 14:
        return 7
    elif x < 21:
        return 3
    elif x < 42:
        return 6
    elif x < 84:
        return 4
    else:
        return 4


def compute_all_promo_means(df):
    for horizon in HORIZONS:
        df = compute_promo_means(df, horizon)
    return df


def compute_promo_means(df, horizon):
    columns_to_keep = list(df.columns)
    df[f"promo_mean_horizon_{horizon}"] = np.nan
    df = df.merge(
        df.groupby(["mat_cod", "rfa_cod", "promotion_cod"])
        .req_delivery_date.min()
        .reset_index()
        .rename(columns={"req_delivery_date": "min_promo_date"}),
        on=["mat_cod", "rfa_cod", "promotion_cod"],
        how="left",
    )
    df["min_promo_date"] = df.min_promo_date - timedelta(days=horizon)
    df = df.sort_values("min_promo_date")
    for agg_level_number, agg_level_columns in AGG_LEVELS.items():
        if df[f"promo_mean_horizon_{horizon}"].isna().sum() == 0:
            break
        agg_level_df = (
            df.groupby(["promotion_cod"] + agg_level_columns)
            .agg({"ordered_volumes": "mean", "req_delivery_date": "min"})
            .reset_index()
            .rename(columns={"req_delivery_date": "min_promo_date"})
            .dropna(subset=["ordered_volumes"])
            .sort_values("min_promo_date")
        )
        agg_level_df["ordered_volumes"] = agg_level_df.groupby(agg_level_columns)[
            "ordered_volumes"
        ].transform(lambda x: x.rolling(5, 1).mean())
        df = pd.merge_asof(
            df,
            agg_level_df,
            by=agg_level_columns,
            on="min_promo_date",
            direction="backward",
            suffixes=(None, f"_{agg_level_number}"),
            allow_exact_matches=False,
        )
        df[f"promo_mean_horizon_{horizon}"] = df[f"promo_mean_horizon_{horizon}"].fillna(
            df[f"ordered_volumes_{agg_level_number}"]
        )
    return df[columns_to_keep + [f"promo_mean_horizon_{horizon}"]]


def compute_cannibalization(df):
    products = get_snowflake_df(
        "DEV_VCP", "VCP_DSP_AFC_EDP_PLD", "R_SAP_EDP_POL_PDT_HIE"
    )[["mat_cod", "mat_brand_cod"]].drop_duplicates()
    products = ks.from_pandas(products)
    df = df.merge(products, on="mat_cod")
    brand_means = df \
        .groupby(["mat_brand_cod", "req_delivery_date", "rfa_cod"])["promo_mean_horizon_7"] \
        .sum() \
        .reset_index() \
        .rename(columns={
            "promo_mean_horizon_7": "brand_products_promo_mean_horizon_7"}
        )
    return df \
        .merge(brand_means, on=['mat_brand_cod', 'req_delivery_date', 'rfa_cod']) \
        .drop('mat_brand_cod', axis=1)


def do_sku_replacements(
        orders_df: pd.DataFrame, sku_replacements_df: pd.DataFrame
) -> pd.DataFrame:
    sku_replacements_dict = {
        sku_replacements_df.loc[i, "old_mat_cod"]: sku_replacements_df.loc[i, "new_mat_cod"]
        for i, _ in sku_replacements_df.iterrows()
    }
    for product in orders_df["mat_cod"].unique():
        if product in sku_replacements_dict:
            orders_df.loc[
                orders_df["mat_cod"] == product, "mat_cod"
            ] = sku_replacements_dict[product]
    return orders_df
