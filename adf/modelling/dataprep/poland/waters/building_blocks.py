import traceback
import logging
from datetime import date
from dateutil.relativedelta import relativedelta
import pandas as pd  # type: ignore
import numpy as np  # type: ignore
from typing import Union, List, Dict
from adf.errors import BuildingBlocksError
import databricks.koalas as ks  # type: ignore
from adf.utils.mappings import get_mappings, save_mappings
from adf.utils.decorators import timeit


log = logging.getLogger("adf")

PROMOS_TYPE_COLS = [
    "no_promo",
    "other",
    "multibuy_2@",
    "price_down_bottle",
    "price_down_pack",
    "multibuy_3@",
    "multibuy_other"
]
PROMOS_UPLIFT_COLS = [k + "_uplift" for k in PROMOS_TYPE_COLS[1:]]


@timeit
def build_bb(promo_df, compute_market_shares: bool):
    try:
        promo_df = promo_df.pipe(scope_promo, "req_delivery_date")
        promo_df = cast_and_sort_promo(
            promo_df,
            sorting_indexes=["req_delivery_date", "fu_cod", "rfa_cod"],
            dtypes={
                "fu_cod": "int32",
                "rfa_cod": "int32",
                "delivered_volumes": "float"
            }
        )
        if compute_market_shares:
            ratios = compute_ratios(
                promo_df, franchise_column="rfa_cod", product_column="fu_cod",
                mechanics_column="promotion_mechanics_desc",
                no_promo_mechanic_value="no_promo", target_qty="delivered_volumes"
            ).reset_index(drop=True)
            save_mappings(ratios, "poland", "waters", "market_share_ratios")
        else:
            ratios = get_mappings("poland", "waters", "market_share_ratios", object_type="koalas")

        uplift_df = promo_df.groupby(
            ["fu_cod", "rfa_cod"], as_index=False
        ).apply(add_uplift_per_franchise).reset_index(drop=True)

        uplift_df = add_market_shares(uplift_df, ratios)

        uplift_df = uplift_df.dropna(subset=PROMOS_UPLIFT_COLS, how='all')
        float_cols = list(uplift_df.select_dtypes(include=['float64', 'float32']).columns)
        int_cols = list(uplift_df.select_dtypes(include=['int']).columns)
        uplift_df[float_cols] = uplift_df[float_cols].fillna(0.0)
        uplift_df[int_cols] = uplift_df[int_cols].fillna(0)
        return uplift_df \
            .pipe(add_total_uplift_increment) \
            .pipe(normalize_uplifts) \
            .pipe(aggregate_uplifts)

    except Exception as err:
        traceback.print_exc()
        message = str(err.__class__.__name__) + ": " + str(err)
        raise BuildingBlocksError(message) from err


def scope_promo(df, date_column):
    """
    Returns filtered promos dataframe based on temporal scope
    """
    today = date.today()
    start_date = (today + relativedelta(months=-24)).strftime("%Y-%m-%d")
    end_date = today.strftime("%Y-%m-%d")

    return df.loc[df[date_column].between(start_date, end_date)]


def cast_and_sort_promo(
        df: pd.DataFrame, sorting_indexes: List[str], dtypes: Dict[str, str]
) -> pd.DataFrame:
    """
    Cast types of input dataframe based on dtypes input mapping and sort based on sorting
    indexes
    """
    df = df.astype(dtypes)
    return df.sort_values(by=sorting_indexes)


@timeit
def compute_ratios(
        promo_df: ks.DataFrame, franchise_column: str, product_column: str, mechanics_column: str,
        no_promo_mechanic_value: Union[int, str], target_qty: str
) -> ks.DataFrame:
    """
    For each franchise, computes the volume market shares in "no_promo" period
    """
    total_no_promo = promo_df \
        .loc[promo_df[mechanics_column] == no_promo_mechanic_value] \
        .groupby(product_column, as_index=False)[target_qty].sum().rename(
            columns={target_qty: "total_no_promo"}
        ).to_pandas()
    ratios = promo_df.groupby([franchise_column, product_column], as_index=False).apply(
        compute_ratios_per_franchise(
            total_no_promo, franchise_column, product_column,
            mechanics_column, no_promo_mechanic_value, target_qty
        )
    )
    return ratios


def compute_ratios_per_franchise(
    total_no_promo: pd.DataFrame,
    franchise_column: str,
    product_column: str,
    mechanics_column: str,
    no_promo_mechanic_value: Union[int, str],
    target_qty: str
):
    def compute_ratios_per_franchise_method(promo_df: pd.DataFrame):
        promo_df = promo_df.reset_index(drop=True)
        franchise = list(promo_df[franchise_column].unique())[0]
        franchise_no_promo = promo_df.loc[
            (
                promo_df[mechanics_column] == no_promo_mechanic_value
            )
        ].groupby(product_column, as_index=False)[target_qty].sum().rename(
            columns={target_qty: "franchise_no_promo"}
        )
        no_promo = franchise_no_promo.merge(total_no_promo, on=product_column, how="left")
        no_promo["ratio"] = no_promo.franchise_no_promo / no_promo.total_no_promo
        no_promo[franchise_column] = franchise
        return no_promo[[franchise_column, product_column, "ratio"]].astype(
            {
                franchise_column: "int32",
                product_column: "int32",
                "ratio": "float"
            }
        )
    return compute_ratios_per_franchise_method


def add_uplift_per_franchise(promo_df):
    promo_df = promo_df.reset_index(drop=True)
    return promo_df. \
        pipe(create_uplift_table). \
        pipe(regularize_volumes). \
        pipe(add_annual_means). \
        pipe(compute_uplifts)


def create_uplift_table(
    df_franchise: pd.DataFrame
) -> pd.DataFrame:
    uplift_df = pd \
        .pivot_table(
            df_franchise,
            values="delivered_volumes",
            index=["rfa_cod", "req_delivery_date", "fu_cod"],
            columns=["promotion_mechanics_desc"],
            aggfunc=np.sum) \
        .reset_index()
    uplift_df = uplift_df.fillna(0)
    for col in PROMOS_UPLIFT_COLS:
        if col not in uplift_df.columns:
            uplift_df[col] = 0

    reindex_cols = ["rfa_cod", "req_delivery_date", "fu_cod"] + PROMOS_TYPE_COLS
    uplift_df = uplift_df.reindex(columns=reindex_cols)

    return uplift_df \
        .groupby(["rfa_cod", "req_delivery_date", "fu_cod"]) \
        .agg({el: "sum" for el in PROMOS_TYPE_COLS}) \
        .reset_index()


def regularize_volumes(
    uplift_df: pd.DataFrame
) -> pd.DataFrame:
    """
    Regularizes volumes if there are sales for non-promo and promo the same time
    """
    reg_cols = PROMOS_TYPE_COLS.copy()
    non_promo_value = reg_cols[0]
    reg_cols.remove(non_promo_value)
    for col in reg_cols:
        uplift_df.loc[(uplift_df[col] > uplift_df[non_promo_value]), col] = \
            uplift_df[non_promo_value] + uplift_df[col]
        uplift_df.loc[(uplift_df[col] > uplift_df[non_promo_value]), non_promo_value] = 0

        uplift_df.loc[(uplift_df[non_promo_value] > uplift_df[col]), non_promo_value] = \
            uplift_df[non_promo_value] + uplift_df[col]
        uplift_df.loc[(uplift_df[non_promo_value] > uplift_df[col]), col] = 0
    return uplift_df


def add_annual_means(
    uplift_df
) -> pd.DataFrame:
    for column in PROMOS_TYPE_COLS:
        mean_df = uplift_df.loc[uplift_df[column] > 0]
        mean_df = mean_df \
            .groupby(["fu_cod", "rfa_cod"])[column] \
            .mean() \
            .reset_index()
        mean_df = mean_df.rename(columns=({column: f"{column}_annual_mean"}))
        uplift_df = uplift_df.merge(
            mean_df,
            on=["fu_cod", "rfa_cod"],
            how="left"
        )
    return uplift_df


def compute_uplifts(
        uplift_df: pd.DataFrame
) -> pd.DataFrame:
    non_promo_value = PROMOS_TYPE_COLS[0]
    for col in PROMOS_UPLIFT_COLS:
        annual_col = "_".join(col.split("_")[:-1]) + "_annual_mean"
        uplift_df[col] = uplift_df[annual_col] / uplift_df[str(non_promo_value) + "_annual_mean"]
    uplift_df = uplift_df.sort_values(by=["req_delivery_date", "fu_cod"])
    uplift_df[PROMOS_UPLIFT_COLS] = uplift_df[PROMOS_UPLIFT_COLS] - 1
    uplift_df[PROMOS_UPLIFT_COLS] = uplift_df[PROMOS_UPLIFT_COLS].clip(0)
    return uplift_df


def add_market_shares(
    uplift_df: ks.DataFrame, ratios: pd.DataFrame
) -> pd.DataFrame:
    uplift_df = uplift_df.merge(
        ratios,
        on=["rfa_cod", "fu_cod"],
        how="left"
    )
    for col in PROMOS_UPLIFT_COLS:
        uplift_df[col] = uplift_df[col] * uplift_df["ratio"]
    uplift_df = uplift_df.drop(columns=["ratio"])
    return uplift_df


def add_total_uplift_increment(
    uplift_df
) -> pd.DataFrame:
    for col in PROMOS_UPLIFT_COLS:
        increment_df = uplift_df \
            .groupby(["req_delivery_date", "fu_cod"], as_index=False)[col] \
            .sum()
        increment_df = increment_df.rename(columns={col: f"{col}_total_increment"})
        uplift_df = uplift_df.merge(
            increment_df,
            on=["req_delivery_date", "fu_cod"],
            how="left"
        )
    return uplift_df


def normalize_uplifts(
    uplift_df: pd.DataFrame
) -> pd.DataFrame:
    normalizer = float(0)
    tot_increments = uplift_df.filter(regex=".total_increment*").columns
    for col in tot_increments:
        normalizer += uplift_df[col]
    normalizer = 1 + (normalizer / len(tot_increments))
    for col in PROMOS_UPLIFT_COLS:
        uplift_df[col] = uplift_df[col] / normalizer
    return uplift_df


def aggregate_uplifts(
    uplift_df: pd.DataFrame
) -> pd.DataFrame:
    agg = {col: "mean" for col in PROMOS_UPLIFT_COLS}
    return uplift_df \
        .groupby(["fu_cod", "rfa_cod"]) \
        .agg(agg) \
        .reset_index()
