import logging
from typing import Dict, Union, Callable, List
from datetime import date, timedelta
import pandas as pd  # type: ignore
from adf.errors import DataprepError
from adf.modelling import mllogs
from adf.services.database.query.reports import update_report
from adf.modelling.dataprep.utils.core import (
    build_panel_indexes,
    build_end_date,
    build_dataprep_report
)
from adf.modelling.dataprep.utils.holidays import (
    get_holidays_df,
    add_weekday_of_holidays,
    add_holidays_count_in_week
)
from adf.modelling.dataprep.utils.orders import (
    add_future_orders_of_n_previous_days,
    add_future_orders_until_weekday_of_n_previous_weeks,
    remove_innovations
)
from adf.modelling.dataprep.utils.weather import (
    process_weather_data,
    aggregate_weather
)
from adf.modelling.dataprep.poland.waters.promo import run as run_promos_dataprep
from adf.modelling.preprocessing.tools.aggregator import most_frequent_value
from adf.utils.dataframe import memory_reduction
from adf.modelling.tools import get_snowflake_df
from adf.utils.decorators import timeit
from adf.config import SNOWFLAKE_DATABASE


log = logging.getLogger("adf")


def run(
        country: str, division: str, product_level: str, location_level: str, **context
):
    """
    Builds panel data for Poland Waters for a given product_level and location_level

    Parameters
    ----------
    country: str
        value must be "poland"
    division: str
        value must be "waters"
    product_level: str
        value must be "fu"
    location_level: str
        value can be "regional" or "national"

    Returns
    -------
    Tuple[adf.services.database.DataprepReports]
        tuple of panel and innovation adf.services.database.DataprepReports (innovation report is \
        None here).
    """

    assert product_level == "fu"
    assert location_level in ("regional", "national")

    product_column = {
        "fu": "fu_cod"
    }[product_level]

    location_column = {
        "regional": "rfa_cod",
        "national": "country_cod",
    }[location_level]

    session = context["session"]

    report = build_dataprep_report(
        session,
        country,
        division,
        product_level,
        location_level,
        scope="panel",
        split_type="",
        split_code=None,
        prep_type="core",
        pipeline_id=context.get("pipeline_id", None)
    )

    mllogs.log_dataprep_report(report)

    try:
        log.info("Building panel")
        panel_df = build_panel_data(product_column, location_column)
        mllogs.log_shape(panel_df)

        log.info("Saving")
        report.upload(panel_df)
        update_report(session, report, "success")
        mllogs.tag_report_output(report)

    except Exception as err:
        message = str(err.__class__.__name__) + ": " + str(err)
        update_report(session, report, "failed", message)
        raise DataprepError(message)

    finally:
        mllogs.tag_report_output(report)

    return report, None


def build_panel_data(
        product_column: str, location_column: str
) -> pd.DataFrame:
    """
    Builds Poland Waters panel data for a specified product level and location level.

    It loads all the relevant data sources, cleaning and processing them, and generating specific
    features.

    The output of this method is a dataframe where each row corresponds to a
    product x location x target date level with associated target quantity and features of
    various nature (known orders, promotions attributes and uplifts, weather inputs, products and
    customers characteristics, holidays data etc.).

    Parameters
    ----------
    product_column: str
        value must be "fu_cod"
    location_column: str
        value can be "rfa_cod" or "country_cod"

    Returns
    -------
    pandas.DataFrame
        panel dataframe where each row corresponds to a product x location x target date level \
        with associated target quantity and features
    """

    # LOAD AND PROCESS DATA SOURCES
    products_df = get_snowflake_df(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_WTR_PLD", "R_SAP_WTR_POL_PDT_HIE"
    ) \
        .pipe(process_products)

    customers_df = get_snowflake_df(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_WTR_PLD", "R_SAP_WTR_POL_CUS_HIE_NFA_RFA"
    ) \
        .pipe(process_customers)

    jmp_in_outs_df = get_snowflake_df(
        SNOWFLAKE_DATABASE, "VCP_DTM_AFC", "WTR_PLD_IN_OUT_JMP_PDT"
    ) \
        .groupby([product_column, "req_delivery_date"]) \
        .sum() \
        .reset_index()

    in_outs_fu_list = get_snowflake_df(
        SNOWFLAKE_DATABASE, 'VCP_DTM_AFC', 'WTR_PLD_IN_OUT_PDT'
    ) \
        .pipe(extract_in_outs)

    holidays_df = get_holidays_df("poland", "dayoff") \
        .pipe(memory_reduction) \
        .pipe(add_weekday_of_holidays, "date") \
        .pipe(add_holidays_count_in_week, "date") \
        .rename({"date": "req_delivery_date"}, axis=1)

    panel_df = get_snowflake_df(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_WTR_PLD", "D_SAP_WTR_POL_DRY_ORD"
    ) \
        .pipe(filter_orders_data) \
        .merge(products_df[["mat_cod", "fu_cod", "bottle_format"]],
               on="mat_cod", how="inner", validate="many_to_one") \
        .pipe(process_orders) \
        .merge(customers_df[["cus_cod", "rfa_cod"]],
               on="cus_cod", how="inner", validate="many_to_one") \
        .pipe(aggregate_panel, product_column, "rfa_cod") \
        .pipe(subtract_jmp_in_outs, jmp_in_outs_df) \
        .pipe(remove_innovations, product_column, "req_delivery_date",
              min_depth_history=3, depth_unit="M",
              view_from_date=date.today().strftime("%Y-%m-%d")) \
        .pipe(remove_in_outs, in_outs_fu_list)

    end_date = build_end_date(lag_type="weeks", lag_value=14)

    panel_indexes = build_panel_indexes(
        panel_df, end_date, product_column, "rfa_cod", "req_delivery_date")

    panel_df = panel_indexes \
        .merge(
            panel_df,
            on=list(panel_indexes.columns),
            how="left",
            validate="one_to_one") \
        .fillna(value={"delivered_volumes": 0}) \
        .pipe(add_country_cod)

    panel_df = replace_specific_fu_codes(panel_df)

    promos_uplifts_df, promos_means_df = run_promos_dataprep(
        panel_df, products_df, compute_market_shares=True
    )

    weather_df = get_snowflake_df(
        SNOWFLAKE_DATABASE, "VCP_DTM_AFC", "WTR_PLD_WEA_HIS"
    ) \
        .pipe(process_weather_data, "req_delivery_date")

    # AGGREGATE DATA
    if location_column == "rfa_cod":
        customers_df = aggregate_customers(customers_df, location_column)
        panel_df = panel_df.merge(customers_df, on="rfa_cod", how="left", validate="many_to_one")
    else:
        panel_df = aggregate_panel(panel_df, product_column, location_column)
        promos_uplifts_df = aggregate_promos(promos_uplifts_df, product_column, location_column)
        promos_means_df = aggregate_promos(promos_means_df, product_column, location_column)
    products_df = aggregate_products(products_df, product_column)
    weather_df = aggregate_weather(weather_df, "country_cod", "req_delivery_date") \
        .drop("country_cod", axis=1)

    # MERGE DATA SOURCES TOGETHER
    return panel_df \
        .merge(products_df, on="fu_cod", how="left", validate="many_to_one") \
        .merge(promos_uplifts_df, on=[product_column, location_column, "req_delivery_date"],
               how="left", validate="one_to_one") \
        .merge(promos_means_df, on=[product_column, location_column, "req_delivery_date"],
               how="left", validate="one_to_one") \
        .merge(holidays_df, on=["req_delivery_date"], how="left", validate="many_to_one") \
        .merge(weather_df, on=["req_delivery_date"], how="left", validate="many_to_one")


@timeit
def process_products(
        products_df: pd.DataFrame
) -> pd.DataFrame:
    """
    Processes products hierarchy dataframe performing cleaning and filtering based on business rules
    Parameters
    ----------
    products_df: pandas.DataFrame
        input products hierarchy dataframe
    Returns
    -------
    pandas.DataFrame
        filtered products hierarchy dataframe
    """
    return products_df \
        .pipe(filter_product_scope)[[
            "mat_cod",
            "fu_cod",
            "bottle_format",
            "mat_brand_desc",
            "mat_family_desc",
            "mat_subfamily_desc",
            "pv_cod",
            "pv_desc",
            "fu_desc"
        ]] \
        .drop_duplicates()


@timeit
def process_customers(
        customers_df: pd.DataFrame
) -> pd.DataFrame:
    """
    Processes customers hierarchy dataframe dropping duplicates and unused columns

    Parameters
    ----------
    customers_df: pandas.DataFrame
        input customers hierarchy dataframe

    Returns
    -------
    pandas.DataFrame
        filtered customers hierarchy dataframe
    """
    customers_df = customers_df.drop_duplicates()
    customers_df = customers_df.query('dis_chl_cod == "00"').query('sal_org_cod == "0042"')
    return customers_df[[
        "cus_cod",
        "nfa_cod",
        "nfa_desc",
        "rfa_cod",
        "rfa_desc"
    ]]


@timeit
def extract_in_outs(
        df: pd.DataFrame
) -> List[int]:
    """
    Extracts In&Outs FU codes from input dataframe, returning results as a list of unique codes

    Parameters
    ----------
    df: pandas.DataFrame
        input In&Outs dataframe

    Returns
    -------
    List[int]
        list of unique FU In&Outs codes
    """
    return list(
        df.query("status=='INOUT/ PROMO'")["fu_cod"].unique()
    )


@timeit
def filter_orders_data(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Filters initial orders dataframe cleaning and removing out_of_scope orders based on business
    rules:

    - Removes orders with a "rejected" tag

    - Remove returned orders (negative values)

    - Keeps orders associated to specific sales document type codes

    - Keeps orders associated to distribution channel 0 (stores)

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe

    Returns
    -------
    pandas.Dataframe
        filtered orders dataframe
    """
    return df \
        .pipe(remove_rejected_orders) \
        .pipe(remove_returns) \
        .pipe(filter_on_sales_type_cod) \
        .pipe(filter_on_distribution_channel)


def filter_product_scope(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Filters products hierarchy dataframe only keeping the main waters brand in Poland
    (scope of the model) which is ŻYWIEC ZDRÓJ
    Parameters
    ----------
    df: pandas.DataFrame
        input products hierarchy dataframe
    Returns
    -------
    pandas.Dataframe
        filtered products hierarchy dataframe
    """
    return df.query(
        'mat_brand_desc in '
        '["ZYWIEC ZDROJ FLAVOURED", "ZYWIEC ZDROJ JUICED", '
        '"ZYWIEC ZDROJ PLAIN", "ZYWIEC ZDROJ TEA", "DOBROWIANKA"]'
    ).query("distribution_channel_cod == '00'")


@timeit
def process_orders(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Processes initial orders dataframe:

    - Replacing wrong fu codes

    - Creating final target date and target quantity (for forecasting) columns for both delivered \
      and undelivered orders

    - Converting target quantity to volume (kilo liters)

    - Adding daily and weekly future orders features

    - Dropping unused columns

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe

    Returns
    -------
    pandas.Dataframe
        processed orders dataframe with relevant features
    """
    return df \
        .pipe(replace_fu) \
        .pipe(update_target_quantity) \
        .pipe(convert_to_volumes, 'delivered_volumes') \
        .pipe(add_future_orders_of_n_previous_days,
              "req_delivery_date", "order_creation_date", "delivered_volumes",
              interval=range(0, 21)) \
        .pipe(add_future_orders_until_weekday_of_n_previous_weeks,
              "req_delivery_date", "order_creation_date", "delivered_volumes",
              interval=range(0, 5),
              until_weekday_included=(date.today() - timedelta(days=1)).weekday(),
              add_weekday_in_column_name=False) \
        .drop(["order_creation_date",
               "ordered_quantity",
               "delivered_quantity",
               "bottle_format"],
              axis=1)


@timeit
def convert_to_volumes(
        df: pd.DataFrame, new_col: str
) -> pd.DataFrame:
    """
    Adds a new column to initial orders dataframe containing the conversion of target quantity into
    volume (in kilo liters)

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe
    new_col: str
        name of new column containing the conversion of target quantity into volume

    Returns
    -------
    pandas.Dataframe
        enriched dataframe containing conversion to volume
    """
    df[new_col] = df["delivered_quantity"] * df["bottle_format"] / 1000
    return df


def replace_fu(
    df: pd.DataFrame
) -> pd.DataFrame:
    """
    Replaces wrong fu codes (in fu_cod column) in initial dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        input dataframe

    Returns
    -------
    pandas.Dataframe
        updated dataframe with corrected fu_cod columns
    """
    df.loc[df['fu_cod'] == 26747, 'fu_cod'] = 26748
    return df


def update_target_quantity(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Replaces delivered_quantity by ordered_quantity value when delivered_quantity is null (not
    created for open orders) into input orders dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe

    Returns
    -------
    pandas.DataFrame
        updated orders dataframe with final target delivered quantities
    """
    mask = (df["delivered_quantity"].isnull()) | (df["delivered_quantity"] == 0)
    df.loc[mask, "delivered_quantity"] = df.loc[mask, "ordered_quantity"]
    return df


@timeit
def aggregate_panel(
        df: pd.DataFrame, product_column: str, location_column: str
) -> pd.DataFrame:
    """
    Aggregates initial orders/panel dataframe to the required granularity which corresponds to
    product_column x location_column x target date at a daily level.
    Aggregation is made summing the volume feature to the required level.

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe
    product_column: str
        target product column. Value is "fu_cod"
    location_column: str
        target location column. Value can be "rfa_cod" or "country_cod"

    Returns
    -------
    pandas.Dataframe
        aggregated dataframe
    """
    agg = {k: (lambda x: x.sum(min_count=1)) for k in df.columns if "delivered_volumes" in k}
    return df.groupby(by=[product_column, location_column, "req_delivery_date"], as_index=False) \
        .agg(agg)


@timeit
def subtract_jmp_in_outs(
        orders_df: pd.DataFrame, jmp_in_outs_df: pd.DataFrame
) -> pd.DataFrame:
    """
    Updates initial orders dataframe removing In&Outs volumes (obtained from jmp_in_outs_df)
    from target volumes column, only for customers with RFA code 20440

    Parameters
    ----------
    orders_df: pandas.DataFrame
        input orders dataframe
    jmp_in_outs_df: pandas.DataFrame
        input 'In&Outs volumes for JMP franchise' dataframe

    Returns
    -------
    pandas.Dataframe
        updated orders dataframe
    """
    # Adding JMP"s RFA code as artificial field
    jmp_in_outs_df["rfa_cod"] = 20440
    orders_df = orders_df.merge(
        jmp_in_outs_df,
        on=["fu_cod", "rfa_cod", "req_delivery_date"],
        how="left",
        validate="one_to_one"
    )
    orders_df["delivered_volumes"] -= orders_df["volume"].fillna(0)
    orders_df.loc[orders_df["delivered_volumes"] < 0, "delivered_volumes"] = 0
    return orders_df.drop("volume", axis=1)


def add_country_cod(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Adds a country_cod column to initial dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        input dataframe

    Returns
    -------
    pandas.DataFrame
        updated dataframe with additional country_cod column
    """
    df.loc[:, "country_cod"] = 0
    return df


@timeit
def aggregate_products(
        df: pd.DataFrame, product_column: str
) -> pd.DataFrame:
    """
    Aggregates products hierarchy dataframe to required product level (which is FU)

    Parameters
    ----------
    df: pandas.DataFrame
        input products hierarchy dataframe
    product_column: str
        reference product column for aggregation. Value is "fu_cod" for FU level

    Returns
    -------
    pandas.DataFrame
        aggregated products hierarchy dataframe
    """
    df = df.drop(["mat_cod"], axis=1)
    agg: Dict[str, Union[str, Callable]] = {
        k: "first" for k in df.columns if k not in (product_column, "bottle_format")
    }
    agg.update({"bottle_format": most_frequent_value})

    df = df \
        .groupby(by=[product_column]) \
        .agg(agg) \
        .reset_index()
    return df


@timeit
def aggregate_customers(
        df: pd.DataFrame, location_column: str
) -> pd.DataFrame:
    """
    Aggregates customers hierarchy dataframe to required location level (which is RFA or national)

    Parameters
    ----------
    df: pandas.DataFrame
        input products hierarchy dataframe
    location_column: str
        reference location column for aggregation. Value can be "rfa_cod" or "country_cod"

    Returns
    -------
    pandas.DataFrame
        aggregated customers hierarchy dataframe
        """
    df = df.drop(["cus_cod"], axis=1)
    agg = {k: most_frequent_value for k in df.columns if k != location_column}

    df = df \
        .groupby(by=[location_column]) \
        .agg(agg) \
        .reset_index()
    return df


def remove_returns(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Removes returned orders (negative quantities) from input orders dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe

    Returns
    -------
    pandas.DataFrame
        filtered orders dataframe
    """
    return df.query(
        '(delivered_quantity >= 0) | (delivered_quantity.isnull() & ordered_quantity >= 0)',
        engine="python"
    )


def remove_in_outs(
        df: pd.DataFrame, in_outs_fus_list: list
) -> pd.DataFrame:
    """
    Removes National In&Out products from input orders dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe
    in_outs_fus_list: list
        list of In&Outs FU codes to remove from input orders dataframe

    Returns
    -------
    pandas.DataFrame
        filtered orders dataframe
    """
    return df.query(f'fu_cod not in {in_outs_fus_list}')


def remove_rejected_orders(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Filters orders with a "rejected" tag from input orders dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe

    Returns
    -------
    pandas.DataFrame
        filtered orders dataframe
    """
    return df \
        .query('rejection_cod == "#"') \
        .drop("rejection_cod", axis=1)


def filter_on_sales_type_cod(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Filters orders from input orders dataframe keeping only orders associated with the following
    sales document type codes: "ZSO", "ZKTS", "ZKR", "ZSIT", "ZSOV", "ZREV", "ZRE2", "ZRK", "ZCR",
    "ZDR", "ZCR1" or "ZDR1"

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe

    Returns
    -------
    pandas.DataFrame
        filtered orders dataframe
    """
    types_to_keep = [
        "ZSO", "ZKTS", "ZKR", "ZSIT", "ZSOV", "ZREV",
        "ZRE2", "ZRK", "ZCR", "ZDR", "ZCR1", "ZDR1"
    ]
    return df.query(f'sal_doc_typ_cod.isin({types_to_keep})', engine="python")


def filter_on_distribution_channel(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Filters orders from input orders dataframe keeping only the ones associated to distribution
    channel 0 (~ stores and supermarkets)

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe

    Returns
    -------
    pandas.DataFrame
        filtered orders dataframe
    """
    return df \
        .query('distribution_channel_cod == 0', engine="python") \
        .drop("distribution_channel_cod", axis=1)


def aggregate_promos(
        df: pd.DataFrame, product_column: str, location_column: str
) -> pd.DataFrame:
    """
    Aggregates input promotions dataframe on a specified set of product x location x date
    columns summing volumes columns

    Parameters
    ----------
    df: pandas.DataFrame
        input promotions dataframe
    product_column: str
        target product column. Value is "fu_cod"
    location_column: str
        target location column. Value can be "rfa_cod" or "country_cod"

    Returns
    -------
    pandas.Dataframe
        aggregated promotions dataframe with a one-level indexed columns
    """
    if (location_column == "country_cod") and ("country_cod" not in df.columns):
        df[location_column] = 0
    return df \
        .groupby([product_column, location_column, "req_delivery_date"]) \
        .sum(min_count=1) \
        .reset_index()


def replace_specific_fu_codes(panel_df):
    affected_rfa_cods = [20614, 20428, 20429, 20430, 20431, 20435]
    fu_replacements = {
        26792: 201237,
        26778: 201236,
        26782: 201235,
        26803: 201234,
        26786: 201233,
        26800: 201232,
        200813: 201231,
        200814: 201230,
        200815: 201246
    }
    for from_fu, to_fu in fu_replacements.items():
        panel_df.loc[
            (panel_df.fu_cod == from_fu) & (panel_df.rfa_cod.isin(affected_rfa_cods)),
            "delivered_volumes"
        ] = panel_df.loc[
            (panel_df.fu_cod == from_fu) & (panel_df.rfa_cod.isin(affected_rfa_cods)),
            "delivered_volumes"
        ] * 1.25
        panel_df.loc[
            (panel_df.fu_cod == from_fu) & (panel_df.rfa_cod.isin(affected_rfa_cods)),
            "fu_cod"
        ] = to_fu
    return panel_df
