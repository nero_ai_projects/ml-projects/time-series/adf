import logging
import traceback
import math
import pandas as pd  # type: ignore
import numpy as np  # type: ignore
from functools import partial  # type: ignore
import databricks.koalas as ks  # type: ignore
from typing import Tuple, Callable
from datetime import timedelta
from adf.errors import DataprepError
from adf.modelling.tools import get_snowflake_df
from adf.utils.provider import Provider
from adf.modelling.dataprep.poland.waters.building_blocks import build_bb
from adf.modelling.dataprep.utils.promos import expand_promos_to_daily
from adf.utils.decorators import timeit
from adf.config import SNOWFLAKE_DATABASE


log = logging.getLogger("adf")
Provide = partial(Provider, mlrun=False, session=True, cluster=True)

SHIFT_MAPPING = {
    20461: [-14, 0],
    20442: [-7, 7],
    20443: [-7, 7],
    20434: [-7, 7],
    20428: [-7, 7],
    20450: [-14, 0],
    20446: [-14, 0],
    20460: [-7, 0],
    20429: [-7, 7],
    20447: [-7, 7],
    20430: [-7, 7],
    20451: [0, 7],
    20452: [-14, 0],
    20459: [0, 7],
    20454: [0, 7]
}

AGG_LEVELS = {
    "1": ["fu_cod", "rfa_cod", "promotion_mechanics_desc", "time_category", "promo_quantile"],
    "2": ["fu_cod", "rfa_cod", "promotion_mechanics_desc", "time_category"],
    "3": ["fu_cod", "rfa_cod", "time_category"],
    "4": ["fu_cod", "rfa_cod", "promotion_mechanics_desc"],
    "5": ["fu_cod", "promotion_mechanics_desc", "time_category", "promo_quantile"],
    "6": ["fu_cod", "promotion_mechanics_desc", "time_category"],
    "7": ["fu_cod", "time_category"],
    "8": ["fu_cod", "promotion_mechanics_desc"],
    "9": ["fu_cod"]
}

horizons = [7 * t for t in range(0, 15)]


def run(
        orders_df: pd.DataFrame, products_df: pd.DataFrame, compute_market_shares: bool
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Builds promos data for Poland Waters
    """
    try:
        log.info("Creating promotions panel and rolling means dataframes")
        panel_promos_no_promos_kdf, panel_only_promos_kdf, promos_means_kdf = \
            build_promos_data(orders_df, products_df)
        log.info("Creating promotions uplifts dataframe")
        bb_table = build_bb(
            panel_promos_no_promos_kdf,
            compute_market_shares=compute_market_shares
        )
        promos_total_uplifts = build_uplifts_promo(panel_only_promos_kdf, bb_table)
        return (
            promos_total_uplifts.to_pandas().reset_index(drop=True),
            promos_means_kdf.to_pandas().reset_index(drop=True)
        )

    except Exception as err:
        traceback.print_exc()
        message = str(err.__class__.__name__) + ": " + str(err)
        raise DataprepError(message)


@timeit
def build_promos_data(
    orders_df: pd.DataFrame, products_df: pd.DataFrame
) -> Tuple[ks.DataFrame, ks.DataFrame, ks.DataFrame]:

    promo_mapping_df = get_snowflake_df(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_WTR_PLD", "F_IBP_WTR_POL_PMN_HDR"
    )

    promos_df = get_snowflake_df(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_WTR_PLD", "F_IBP_WTR_POL_PMN_TRS"
    ).pipe(add_promo_mapping_to_promos, promo_mapping_df) \
        .pipe(process_promos) \
        .pipe(add_products_to_promos, products_df[["fu_cod", "pv_cod"]].drop_duplicates())

    log.info(f"Processing promos per fu_cod: {process_product_panel_promo.__name__}")
    promo_no_promo_kdf = multiprocess_product_orders_with_promos(
        promos_df,
        orders_df,
        process_product_panel_promo
    )
    only_promo_kdf = promo_no_promo_kdf.loc[
        promo_no_promo_kdf["promotion_mechanics_desc"] != "no_promo"
    ]
    log.info(f"Processing promos per fu_cod: {process_product_promo_means.__name__}")
    promos_rolling_means = multiprocess_product_orders_with_promos(
        promos_df,
        orders_df,
        process_product_promo_means
    )

    return promo_no_promo_kdf, only_promo_kdf, promos_rolling_means


@timeit
def add_promo_mapping_to_promos(
        df: pd.DataFrame, promo_mapping_df: pd.DataFrame
) -> pd.DataFrame:
    """
    Adds promotion transactional info to input promotions dataframe
    (such as which rfa and which product variant are concerned for each promotion ID)

    Parameters
    ----------
    df: pandas.DataFrame
        input promotions dataframe
    promo_mapping_df: pandas.DataFrame
        promotion transactional info dataframe

    Returns
    -------
    pandas.Dataframe
        enriched promotions dataframe containing promotion transactional info.
    """
    return df.merge(promo_mapping_df, on="promotion_cod", how="inner", validate="many_to_one")


@timeit
def process_promos(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Cleans promotions dataframe: \
    - replacing wrong promotion mechanics \
    - dropping duplicates on subsets

    Parameters
    ----------
    df: pandas.DataFrame
        input promotions dataframe

    Returns
    -------
    pandas.DataFrame
        updated promotions dataframe
    """
    df.loc[df["promotion_mechanics_desc"] == "No info", "promotion_mechanics_desc"] = \
        "price down BOTTLE"
    df["promotion_mechanics_desc"] = \
        df["promotion_mechanics_desc"].apply(lambda x: "_".join(x.lower().split(" ")))
    return df \
        .drop_duplicates(
            subset=[
                "pv_cod",
                "rfa_cod",
                "promotion_mechanics_desc",
                "promotion_start_date_sellin",
                "promotion_end_date_sellin"
            ]
        )


@timeit
def add_products_to_promos(
        df: pd.DataFrame, products_df: pd.DataFrame
) -> pd.DataFrame:
    """
    Adds products attributes to input promotions dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        input promotions dataframe
    products_df: pandas.DataFrame
        products hierarchy dataframe

    Returns
    -------
    pandas.Dataframe
        enriched promotions dataframe containing products attributes at the lowest granularity \
        (FU)
    """
    return df \
        .merge(products_df, on="pv_cod", how="inner", validate="many_to_many") \
        .drop(["pv_cod"], axis=1) \
        .drop_duplicates()


@timeit
def multiprocess_product_orders_with_promos(
    promo_df: pd.DataFrame,
    orders_df: pd.DataFrame,
    function: Callable
):
    return ks.from_pandas(promo_df).groupby("fu_cod").apply(
        process_product_promo(orders_df, function)
    )


def process_product_promo(orders_df: pd.DataFrame, function: Callable):
    def process_product_promo_on_group(
        product_promo_df: pd.DataFrame
    ):
        product_cod = list(product_promo_df.fu_cod.unique())[0]
        start_date = product_promo_df["promotion_start_date_sellin"]
        end_date = product_promo_df["promotion_end_date_sellin"]
        mask = (start_date > end_date)
        product_promo_df = product_promo_df.loc[~mask]
        product_order_df = orders_df.loc[orders_df["fu_cod"] == product_cod]
        return function(
            product_promo_df,
            product_order_df
        )
    return process_product_promo_on_group


def process_product_panel_promo(
        product_promos_df: pd.DataFrame, product_orders_df: pd.DataFrame
):
    product_promos_df = product_promos_df \
        .apply(shift_franchises_start_sellin, axis=1) \
        .pipe(expand_promos_to_daily, "promotion_start_date_sellin",
              "promotion_end_date_sellin", "req_delivery_date") \
        .pipe(clean_promo) \
        .pipe(remove_sundays_promo)

    product_orders_df = product_orders_df \
        .groupby(["fu_cod", "rfa_cod", "req_delivery_date"])["delivered_volumes"] \
        .sum() \
        .reset_index()

    product_promo_enriched_df = product_orders_df.merge(
        product_promos_df,
        on=["req_delivery_date", "rfa_cod", "fu_cod"],
        how="right"
    )
    product_promo_enriched_df = product_promo_enriched_df.fillna(value=0)

    product_orders_enriched_df = product_orders_df.merge(
        product_promos_df,
        on=["req_delivery_date", "rfa_cod", "fu_cod"],
        how="left"
    )
    product_orders_no_promo = product_orders_enriched_df[
        product_orders_enriched_df["promotion_mechanics_desc"].isnull()
    ]
    product_orders_no_promo.loc[:, "promotion_mechanics_desc"] = "no_promo"
    final_product_promo_df = pd.concat(
        [product_orders_no_promo, product_promo_enriched_df],
        axis=0
    )
    final_product_promo_df = final_product_promo_df[[
        "fu_cod",
        "rfa_cod",
        "req_delivery_date",
        "delivered_volumes",
        "promotion_mechanics_desc"
    ]]
    return final_product_promo_df


def shift_franchises_start_sellin(row):
    """
    Shifts promotion sellin start and end dates based on a shift mapping that depends on customers'
    behaviours.
    This methods applies to a promotions dataframe's row.
    """
    if row["rfa_cod"] in SHIFT_MAPPING.keys():
        rfa = row["rfa_cod"]
        row["promotion_start_date_sellin"] = \
            row["promotion_start_date_store"] + timedelta(days=SHIFT_MAPPING[rfa][0])
        if len(SHIFT_MAPPING[rfa]) > 1:
            row["promotion_end_date_sellin"] = \
                row["promotion_start_date_store"] + timedelta(days=SHIFT_MAPPING[rfa][1])
        else:
            row["promotion_end_date_sellin"] = row["promotion_end_date_store"]
    return row


def clean_promo(
        promos_df: pd.DataFrame
) -> pd.DataFrame:
    """
    Cleans input promotions dataframe to remove duplicated and overlapping promotions

    For a given promotions (product x customers x promotion mechanics), if slots are overlapping,
    we keep the widest slot.
    For a given slot (product x customers x date), if we have 2 promotions mechanics at the same
    time, we keep the first one.

    Parameters
    ----------
    promos_df: pandas.DataFrame
        input promotions dataframe

    Returns
    -------
    pandas.DataFrame
        updated promotions dataframe
    """
    return promos_df \
        .groupby(
            ["fu_cod", "rfa_cod", "req_delivery_date", "promotion_mechanics_desc", "promotion_cod"]
        ) \
        .agg({"elapsed_time": "max"}) \
        .reset_index() \
        .groupby(["fu_cod", "rfa_cod", "req_delivery_date", "elapsed_time"]) \
        .agg({"promotion_mechanics_desc": "first", "promotion_cod": "first"}) \
        .reset_index()


def remove_sundays_promo(
        promo_df: pd.DataFrame
) -> pd.DataFrame:
    """
    Removes Sunday rows
    """
    promo_df["weekday"] = promo_df["req_delivery_date"].dt.weekday
    promo_df = promo_df.loc[promo_df.weekday < 6]
    promo_df = promo_df.drop(columns=["weekday"], axis=1)
    return promo_df


def build_uplifts_promo(
    promo_dataprep: ks.DataFrame, bb_table: ks.DataFrame
) -> pd.DataFrame:
    promo_df = promo_dataprep.reset_index(drop=True)
    ks.set_option('compute.ops_on_diff_frames', True)
    bb_table["total_uplifts"] = bb_table.filter(regex="uplift").sum(axis=1)
    uplifts_df = promo_df.merge(
        bb_table[["fu_cod", "rfa_cod", "total_uplifts"]],
        on=["fu_cod", "rfa_cod"],
        how="left"
    )
    uplifts_df = uplifts_df \
        .groupby(["fu_cod", "rfa_cod", "req_delivery_date"]) \
        .agg({"total_uplifts": "sum"}) \
        .reset_index()
    ks.reset_option('compute.ops_on_diff_frames')
    return uplifts_df


def process_product_promo_means(
    product_promos_df: pd.DataFrame, product_orders_df: pd.DataFrame
) -> pd.DataFrame:

    product_promos_df = product_promos_df \
        .apply(shift_franchises_start_sellin, axis=1) \
        .pipe(expand_promos_to_daily, "promotion_start_date_sellin",
              "promotion_end_date_sellin", "req_delivery_date") \
        .pipe(clean_promo) \
        .pipe(remove_sundays_promo)

    product_orders_df = product_orders_df \
        .groupby(["fu_cod", "rfa_cod", "req_delivery_date"])["delivered_volumes"] \
        .sum() \
        .reset_index()

    product_promo_enriched_df = product_orders_df \
        .merge(
            product_promos_df,
            on=["req_delivery_date", "rfa_cod", "fu_cod"],
            how="right") \
        .fillna(value=0) \
        .pipe(add_promo_columns)[[
            "fu_cod",
            "rfa_cod",
            "req_delivery_date",
            "delivered_volumes",
            "promotion_mechanics_desc",
            "promotion_cod",
            "time_category",
            "promo_quantile"]] \
        .pipe(compute_all_promo_means)[[
            "fu_cod",
            "rfa_cod",
            "req_delivery_date"] + [f'promo_mean_horizon_{i}' for i in horizons]] \
        .groupby(["fu_cod", "rfa_cod", "req_delivery_date"])[[
            f'promo_mean_horizon_{i}' for i in horizons]] \
        .max() \
        .reset_index()

    return product_promo_enriched_df


def add_promo_columns(
        df: pd.DataFrame
) -> pd.DataFrame:

    quantile_days = df \
        .groupby(["fu_cod", "rfa_cod", "promotion_cod"]) \
        .elapsed_time \
        .max() \
        .reset_index()
    quantile_days["quantile_days"] = quantile_days \
        .elapsed_time \
        .apply(lambda x: math.ceil(max(x, 1) / max(get_quantile(x), 1)))
    quantile_days["time_category"] = quantile_days \
        .elapsed_time \
        .apply(get_time_category)
    quantile_days = quantile_days.drop(columns=["elapsed_time"])

    df = df.merge(
        quantile_days, on=["rfa_cod", "fu_cod", "promotion_cod"]
    )
    df["promo_quantile"] = (
        df.elapsed_time % df.quantile_days
    )
    return df


def get_time_category(x: int) -> int:
    if x < 7:
        return 0
    elif x < 14:
        return 1
    elif x < 21:
        return 2
    elif x < 42:
        return 3
    elif x < 84:
        return 4
    else:
        return 5


def get_quantile(x: int) -> int:
    if x < 7:
        return 7
    elif x < 14:
        return 7
    elif x < 21:
        return 3
    elif x < 42:
        return 6
    elif x < 84:
        return 4
    else:
        return 4


def compute_all_promo_means(
        df: pd.DataFrame
) -> pd.DataFrame:
    for horizon in horizons:
        df = compute_promo_means(df, horizon)
    return df


def compute_promo_means(
        df: pd.DataFrame, horizon: int
) -> pd.DataFrame:
    columns_to_keep = list(df.columns)
    df[f"promo_mean_horizon_{horizon}"] = np.nan
    df = df.merge(
        df
        .groupby(["fu_cod", "rfa_cod", "promotion_cod"])
        .req_delivery_date
        .min()
        .reset_index()
        .rename(columns={"req_delivery_date": "min_promo_date"}),
        on=["fu_cod", "rfa_cod", "promotion_cod"],
        how="left"
    )
    df["min_promo_date"] = df.min_promo_date - timedelta(days=horizon)
    df = df.sort_values("min_promo_date")

    for agg_level_number, agg_level_columns in AGG_LEVELS.items():
        if df[f"promo_mean_horizon_{horizon}"].isna().sum() == 0:
            break
        agg_level_df = df \
            .groupby(["promotion_cod"] + agg_level_columns) \
            .agg({"delivered_volumes": "mean", "req_delivery_date": "min"}) \
            .reset_index() \
            .rename(columns={"req_delivery_date": "min_promo_date"}) \
            .dropna(subset=["delivered_volumes"]) \
            .sort_values("min_promo_date")
        agg_level_df["delivered_volumes"] = agg_level_df \
            .groupby(agg_level_columns)["delivered_volumes"] \
            .transform(lambda x: x.rolling(5, 1).mean())
        df = pd.merge_asof(
            df,
            agg_level_df,
            by=agg_level_columns,
            on="min_promo_date",
            direction="backward",
            suffixes=(None, f"_{agg_level_number}"),
            allow_exact_matches=False
        )
        df[f"promo_mean_horizon_{horizon}"] = df[f"promo_mean_horizon_{horizon}"].fillna(
            df[f"delivered_volumes_{agg_level_number}"]
        )
    return df[columns_to_keep + [f"promo_mean_horizon_{horizon}"]]
