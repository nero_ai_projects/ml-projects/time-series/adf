import glob

import pandas as pd

from adf.modelling.dataprep.rus.config import SI_KEY_COLUMNS


def build_intermediate_caf_file(files_mask: str) -> str:
    """

    Parameters
    ----------
    files_mask: mask that will be used to read csv files and combine them together.
        Mask must contain pgi or mad substring.
        Examples:  'data/raw/caf/caf_pgi*csv', 'data/raw/caf/caf_mad*csv'

    Returns
    -------
        path to created file
    """
    if 'pgi' in files_mask:
        date_col = 'pgi_date'
    elif 'mad' in files_mask:
        date_col = 'mad_date'
    else:
        msg = 'files_mask doesnt contain mag or pgi: cant define type of date column'
        raise ValueError(msg)

    caps_date_col = date_col.upper()
    use_cols = ['SKU', 'TBT', 'BU', 'WH', caps_date_col, 'CHAIN', 'SALES_ORDERED_RUB']
    caf = pd.concat([pd.read_csv(f, usecols=use_cols) for f in glob.glob(files_mask)])

    chain_count = caf.CHAIN.nunique()
    if chain_count > 1:
        msg = f'caf data source doesnt support a few customers data. Customers count: {chain_count}'
        raise NotImplementedError(msg)
    del caf['CHAIN']

    col_mapping = {
        caps_date_col: date_col,
        'SKU': 'sku',
        'TBT': 'techbillto',
        'BU': 'bu',
        'WH': 'dc',
        'SALES_ORDERED_RUB': 'caf',
    }
    caf.rename(columns=col_mapping, inplace=True)

    caf[date_col] = pd.to_datetime(caf[date_col])

    caf.drop_duplicates(subset=['sku', 'techbillto', 'bu', 'dc', date_col], inplace=True)

    mad_or_pgi = 'pgi' if 'pgi' in files_mask else 'mad'
    output_file = f'data/intermediate/caf_{mad_or_pgi}.parquet'
    caf.to_parquet(output_file)
    return output_file


def add_caf(sellin: pd.DataFrame, caf: pd.DataFrame, date_col: str = 'mad_date') -> pd.DataFrame:
    """

    Parameters
    ----------
    sellin: pd.Dataframe with si data. Must contain partition.
    caf: pd.Dataframe that contain caf
    date_col: name of data column

    Returns
    -------
    dataframe with caf data
    """

    last_train_date = sellin[sellin.partition == 'train'][date_col].max()

    train_caf = caf[caf[date_col] <= last_train_date]
    train_caf = train_caf[['sku', 'techbillto', 'dc', 'bu', date_col, 'caf']]

    sellin = sellin.merge(train_caf, how='left')
    sellin['caf'] = sellin.groupby(SI_KEY_COLUMNS).caf\
        .fillna(method='ffill') \
        .fillna(method='backfill')
    return sellin
