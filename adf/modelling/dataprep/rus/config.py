import pandas as pd
import os
from datetime import datetime
from adf.ru import modelling_cli

path = modelling_cli.main_path
if not os.path.exists(path):
    os.makedirs(path)

suffix = modelling_cli.suffix
# TODO: code review: put all constants into separate objects
# Main path
INTERMEDIATE_SELL_IN_PATH = f'{path}/data/intermediate/prep_first_part_{suffix}.parquet'

PREPROCESSED_PATH = f"{path}/data/processed/preprocessing_{suffix}.parquet"
TOTAL_FC_PATH = f"{path}/data/results/outputs/total_{suffix}.parquet"
POSTPROCESSING_PATH = f"{path}/data/results/outputs/fc_postprocessed_{suffix}.parquet"
POSTPROCESSING_UPLIFTLEVEL_PATH = f"{path}/data/results/outputs/fc_postprocessed_uplift_level_{suffix}.parquet"

# Secondary path

DATE_COLUMN_MAPPING = {'st': 'mad_date',
                       'mt': 'pgi_date',
                       'lt': 'pgi_date'}

# TODO: code review: rename key columns (techbillto -> tbt, dc -> wh)
SI_MAD_COLUMNS_RENAME_MAPPING = {'mad_date': 'mad_date',
                                 'wh': 'dc',
                                 'chain': 'client',
                                 'bu': 'bu',
                                 'tbt': 'techbillto',
                                 'ordered': 'ordered',
                                 'invoiced': 'invoiced',
                                 'sku': 'sku',
                                 'mt_discount': 'discount',
                                 'sales_invoiced_rub': 'sales_invoiced_rub'}
SI_PGI_COLUMNS_RENAME_MAPPING = {'pgi_date': 'pgi_date',
                                 'wh': 'dc',
                                 'chain': 'client',
                                 'bu': 'bu',
                                 'tbt': 'techbillto',
                                 'ordered': 'ordered',
                                 'invoiced': 'invoiced',
                                 'sku': 'sku',
                                 'mt_discount': 'discount',
                                 'sales_invoiced_rub': 'sales_invoiced_rub'}

SI_MAD_DTYPE = {'mad_date': 'datetime64[ns]',
                'dc': 'category',
                'client': 'category',
                'bu': 'category',
                'techbillto': 'category',
                'ordered': 'float64',
                'invoiced': 'float64',
                'sku': 'category',
                'discount': 'uint8',
                'sales_invoiced_rub': 'float64'}
SI_PGI_DTYPE = {'pgi_date': 'datetime64[ns]',
                'dc': 'category',
                'client': 'category',
                'bu': 'category',
                'techbillto': 'category',
                'ordered': 'float64',
                'invoiced': 'float64',
                'sku': 'category',
                'discount': 'uint8',
                'sales_invoiced_rub': 'float64'}

SI_KEY_COLUMNS = ['techbillto', 'client', 'bu', 'dc', 'sku']
SI_CATEGORICAL_COLUMNS = SI_KEY_COLUMNS
SI_AGG_RULES = {'ordered': 'sum',
                'invoiced': 'sum',
                'discount': 'max',
                'sales_invoiced_rub': 'sum',
                }

SI_PREP_AGG_RULES = {'ordered': 'max',
                     'discount': 'max',
                     'price': 'max',
                     'partition': 'first',
                     }

MD_KEY_CUST_COLUMNS_RENAME = {'chain': 'client',
                              'tbt': 'techbillto',
                              'bu': 'bu'}
MD_KEY_CUST_COLUMNS = list(MD_KEY_CUST_COLUMNS_RENAME.values())

MD_KEY_PROD_COLUMNS_RENAME = {'mat_cod': 'sku'}
MD_KEY_PROD_COLUMNS = list(MD_KEY_PROD_COLUMNS_RENAME.values())

# File paths

FT_OUTPUT_FILE_PATH = f'{path}/data/intermediate/Fintool_promo_{suffix}.parquet'
EXTENDED_PROMO_FILE_PATH = f'{path}/data/intermediate/Extended_promo_{suffix}.parquet'

INTERMEDIATE_BP_MAD_PROMO_FILE_PATH = f'{path}/data/intermediate/promo_bp_mad.parquet'
INTERMEDIATE_BP_PGI_PROMO_FILE_PATH = f'{path}/data/intermediate/promo_bp_pgi.parquet'

KEYS_LISTS_FOLDER_PATH = f'{path}/data/tmp/check_loss'
CHECK_LOSS_FILE_PATH = f'{path}/data/results/reports/check_loss/lost_keys.parquet'

FC_OUT_PATH = f'{path}/data/results/outputs/forecast_{suffix}.parquet'
BL_OUT_PATH = f'{path}/data/results/outputs/baseline_{suffix}.parquet'

SI_PREPROCESSING_TMP_FOLDER_PATH = f'{path}/data/tmp/si_recovered_{suffix}'
LOG_FOLDER = f'{path}/logs'

MODEL_PARAMS_FOLDER_PATH = f'{path}/settings'

CODE_DC_MAGNIT = 'R0V087'

# Dates
FIRST_SELL_IN_DATE = pd.to_datetime('2019-01-01', format='%Y-%m-%d')
# TODO: code review: set first_sell_in_date current date minus 3 years
today = datetime.now().today()
LAST_SELL_IN_DATE = pd.to_datetime(today, format='%Y-%m-%d') - pd.Timedelta(days=1)

FIRST_FORECAST_DATE = modelling_cli.first_fc_date

# Snowflake
SNOWFLAKE_DATABASE = 'DEV_CIS'
BP_COLUMNS_RENAME = {'promo_id': 'promo_id',
                     'tbt': 'techbillto',
                     'sku': 'sku',
                     'ship_start': 'ship_start',
                     'ship_end': 'ship_end'}
BP_COLUMNS = list(BP_COLUMNS_RENAME.keys())

ROT_COLUMNS_RENAME = {'old_sku': 'old_sku',
                      'new_sku': 'new_sku',
                      'date': 'date'}
ROT_COLUMNS = list(ROT_COLUMNS_RENAME.keys())
FT_COLUMNS_RENAME = {'wh': 'dc',
                     'chain': 'client',
                     'bu': 'bu',
                     'tbt': 'techbillto',
                     'ft_sku': 'ft_sku',
                     'ship_start': 'ship_start',
                     'ship_end': 'ship_end',
                     'discount': 'discount'}
FT_DTYPE = {'dc': 'category',
            'client': 'category',
            'bu': 'category',
            'techbillto': 'category',
            'ft_sku': 'category',
            'ship_start': 'datetime64[ns]',
            'ship_end': 'datetime64[ns]',
            'discount': 'float64'}

ADF_EXPORT_ST_MAGNIT = ['mad_date', 'tbt', 'chain', 'bu', 'sku', 'so', 'wh', 'bl',
                        'uplift', 'version']

SPLIT_EXPORT_ST = ['mad_date', 'tbt', 'chain', 'bu', 'sku', 'so', 'wh', 'bl',
                 'uplift', 'version'] 

ADF_EXPORT_ST = ['mad_date', 'tbt', 'chain', 'bu', 'sku', 'so', 'wh', 'bl',
                 'uplift', 'csl', 'adf_bl', 'adf_uplift', 'cust_bl', 'cust_uplift',
                 'data_type', 'version']
ADF_EXPORT_MT = ['pgi_date', 'tbt', 'chain', 'bu', 'sku', 'so', 'wh', 'bl',
                 'uplift', 'version']
ADF_EXPORT_LT = ['pgi_date', 'tbt', 'chain', 'bu', 'sku', 'so', 'wh', 'bl', 'version']

EXPORT_KEY_COLUMNS_RENAME = {'techbillto': 'tbt',
                             'client': 'chain',
                             'bu': 'bu',
                             'sku': 'sku',
                             'dc': 'wh'}

PRIOR_COLUMNS_WEIGHT = {'chain_desc': 1,
                        'techbillto': 2,
                        'umb_brd_desc': 4,
                        'brand_desc': 8,
                        'family_desc': 16,
                        'sku': 32}
PRIOR_COLUMNS_RENAME = {'mat_cod': 'sku',
                        'wh': 'dc',
                        'chain': 'client',
                        'tbt': 'techbillto',
                        'date': 'mad_date',
                        'volume_kg': 'cust_fc'}

SF_TYPE = {'mat_cod': 'int64',
           'sku': 'int64',
           'product_nat_desc': 'object',
           'subfamily': 'object',
           'tbt': 'int64',
           'techbillto': 'int64',
           'client': 'object',
           'chain': 'object',
           'bu': 'object',
           'dc': 'int64',
           'wh': 'int64',
           'chain_desc': 'object'}
