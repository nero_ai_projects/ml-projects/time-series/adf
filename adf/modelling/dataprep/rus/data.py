from adf.modelling.dataprep.rus.tools import get_snowflake_df
import pandas as pd
from . import config


def get_shelf_price() -> pd.DataFrame:
    return get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_EXT_SHF_PRC')


def get_sell_out() -> pd.DataFrame:
    return get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DWH', 'F_EXT_IBP_MAG_SO')


def get_stock() -> pd.DataFrame:
    return get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DWH', 'F_EXT_IBP_MAG_STOCK')