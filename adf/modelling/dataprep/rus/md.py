import pandas as pd
import numpy as np
import logging

import adf.modelling.dataprep.rus.config as config
from adf.modelling.dataprep.rus.tools import get_snowflake_df


def add_cust_hier(df: pd.DataFrame,
                  key_cols: list = None,
                  use_cols: list = None):
    '''
    Adds customer hierarchy based on key columns

    :param df: pandas dataframe
    :param key_cols: columns containing key by customer hierarchy
    :param use_cols: columns with different levels of the customer hierarchy
    :return: input dataframe with additional columns
    '''
    key_cols = config.MD_KEY_CUST_COLUMNS if key_cols is None else key_cols
    md_cust = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_EXT_CUS_MD')
    md_cust = md_cust.rename(columns=config.MD_KEY_CUST_COLUMNS_RENAME) \
        .drop(columns=['price_group'])\
        .drop_duplicates()  # tmp drop 'price group' reason: duplicated data

    if len(df) == 0:
        logging.info('Empty DataFrame')
    else:
        use_cols = list(set(df.columns) | set(md_cust.columns)
                        ) if use_cols is None else use_cols
        df = pd.merge(df, md_cust, how='left', on=key_cols)
        df = df[use_cols]
    return df


def add_prod_hier(df: pd.DataFrame,
                  key_cols: list = None,
                  use_cols: list = None):
    '''
    Adds product  hierarchy based on key column

    :param df: pandas dataframe
    :param key_cols: columns containing key by product hierarchy
    :param use_cols: columns with different levels of the product hierarchy
    :return: input dataframe with additional columns
    '''
    key_cols = config.MD_KEY_PROD_COLUMNS if key_cols is None else key_cols
    md_prod = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_EXT_PDT_MD')
    md_prod = md_prod.rename(columns=config.MD_KEY_PROD_COLUMNS_RENAME)\
        .drop_duplicates()

    if len(df) == 0:
        logging.info('Empty DataFrame')
    else:
        use_cols = list(set(df.columns) | set(md_prod.columns)
                        ) if use_cols is None else use_cols
        df = pd.merge(df, md_prod, how='left', on=key_cols)
        df = df[use_cols]
    return df


def get_ultrafresh_sku() -> pd.DataFrame:
    '''
    Returns a dataframe with ultrafresh SKUs
    '''
    df = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_EXT_PDT_MD',
                          selection="MAT_COD, SHELF_LIFE")
    df.dropna(inplace=True)
    df.columns = ['sku', 'shelf_life']
    df = df.astype(int)
    df['ultrafresh'] = np.where(df.shelf_life < 20, 1, 0)
    df.drop(columns=['shelf_life'], inplace=True)
    return df
