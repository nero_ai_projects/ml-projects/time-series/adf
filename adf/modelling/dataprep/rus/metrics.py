import numpy as np
import pandas as pd
import logging

from . import config, utils
from adf.modelling.dataprep.rus.tools import get_snowflake_df
from adf.modelling.dataprep.rus.tools import get_last_export_date

logger = logging.getLogger(__name__)


def fa(df: pd.DataFrame,
       act_col: str = 'ordered',
       fc_col: str = 'fc',
       ae_gr_cols: list = None,
       out_gr_cols: list = None,
       date_column: str = 'pgi_date') -> pd.DataFrame:
    '''
    Calculates forecast accuracy

    :param df: df for calculate accuracy
    :param act_col: name of the fact column
    :param fc_col: name of the forecast column
    :param ae_gr_cols: the level of detail at which metrics are calculated
    :param out_gr_cols: the levels of detail to which metrics are aggregated
     after calculation for output
    :return: pandas df with act, fc, acc, ae at requested level of details
    '''
    if out_gr_cols is None:
        out_gr_cols = ['partition']
    if ae_gr_cols is None:
        ae_gr_cols = [date_column] + config.SI_KEY_COLUMNS
    df[[act_col, fc_col]] = df[[act_col, fc_col]].round(2)
    used_cols = list(
        set(ae_gr_cols.copy() + out_gr_cols.copy()))
    used_cols.append(act_col)
    used_cols.append(fc_col)

    gr_df = df[used_cols].groupby(list(set(ae_gr_cols + out_gr_cols)),
                                  as_index=False, observed=True).sum()
    del df
    gr_df['AE'] = abs(gr_df[act_col] - gr_df[fc_col])

    out_df = gr_df.groupby(out_gr_cols, as_index=False, observed=True).agg(
        {act_col: 'sum',
         fc_col: 'sum',
         'AE': 'sum'})
    out_df['Acc'] = np.where(out_df[act_col] == 0, 0,
                             1 - out_df['AE'] / out_df[act_col])

    return out_df


def fa_w(
        df: pd.DataFrame,
        act_col: str = 'ordered',
        fc_col: str = 'fc',
        ae_gr_cols: list = None,
        out_gr_cols: list = None,
        date_column: str = 'pgi_date',
) -> pd.DataFrame:
    '''
    Calculates forecast accuracy based on the sku coefficient

    :param df: df for calculate accuracy
    :param act_col: name of the fact column
    :param fc_col: name of the forecast column
    :param ae_gr_cols: the level of detail at which metrics are calculated
    :param out_gr_cols: the levels of detail to which metrics are aggregated
    after calculation for output
    :return: pandas df with act, fc, acc, ae at requested level of details
    '''
    if out_gr_cols is None:
        out_gr_cols = ['partition']
    if ae_gr_cols is None:
        ae_gr_cols = [date_column] + config.SI_KEY_COLUMNS
    prod_md = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_EXT_PDT_MD',
                               selection="distinct MAT_COD, PRODUCT_NAT_DESC")
    kpi_coef = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DWH', 'F_EXT_ABC_KPI_DMD') \
        .astype({'coefficient': 'float64'})
    kpi_coef = kpi_coef.merge(prod_md, left_on='subfamily',
                              right_on='product_nat_desc', how='right') \
        .rename(columns={'mat_cod': 'sku'}) \
        .drop(columns=['subfamily', 'product_nat_desc'])
    utils.astype_cols(kpi_coef)
    kpi_coef['coefficient'] = kpi_coef['coefficient'].fillna(1)

    if 'sku' not in ae_gr_cols:
        out_df = fa(df,
                    act_col,
                    fc_col,
                    ae_gr_cols,
                    out_gr_cols)
    else:
        df[[act_col, fc_col]] = df[[
            act_col, fc_col]].round(2)
        used_cols = list(
            set(ae_gr_cols.copy() + out_gr_cols.copy()))
        used_cols.append(act_col)
        used_cols.append(fc_col)

        gr_df = df[used_cols].groupby(list(set(ae_gr_cols + out_gr_cols)),
                                      as_index=False,
                                      observed=True).sum()
        del df
        gr_df = gr_df.merge(kpi_coef, how='left', on='sku')
        ae_w = 'AE_w'
        gr_df[ae_w] = abs(
            gr_df[act_col] - gr_df[fc_col]) * gr_df['coefficient']

        out_df = gr_df.groupby(out_gr_cols, as_index=False, observed=True).agg(
            {act_col: 'sum',
             fc_col: 'sum',
             ae_w: 'sum'})
        out_df['Acc'] = np.where(out_df[act_col] == 0, 0,
                                 1 - out_df[ae_w] / out_df[act_col])

    return out_df


def fa_period(
        df: pd.DataFrame,
        period: str = 'st',
        act_col: str = 'ordered',
        fc_col: str = 'fc',
        date_column: str = 'pgi_date',
        out_gr_cols: list = None,
        snap_danone: bool = False
) -> pd.DataFrame:
    '''
    Calculates forecast accuracy for different periods (st/mt/lt)

    :param df: df for calculate accuracy
    :param period: period to which metrics are calculated : st/mt/lt
    :param act_col: name of the fact column
    :param fc_col: name of the forecast column
    :param date_column: name of the date column
    :param out_gr_cols: the levels of detail to which metrics are aggregated
    after calculation for output
    :param snap_danone: flags for danone's snapshots
    :return: pandas df with act, fc, acc, ae at requested level of details
            (for st : act_w, ae_w including margin weight)
    '''
    utils.astype_cols(df)
    if out_gr_cols is None:
        out_gr_cols = ['partition']
    if not snap_danone:
        md_channel = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_EXT_CUS_MD',
                                      selection="distinct CHAIN, CHAIN_DESC, CHANNEL, TBT, BU") \
            .rename(columns={'tbt': 'techbillto',
                             'chain': 'client'})
        utils.astype_cols(md_channel)
        df = df.merge(md_channel,
                      how='left',
                      on=['techbillto', 'client', 'bu'])
        cust_kpi = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DWH', 'F_EXT_CUS_KPI_SFA_DMD') \
            .rename(columns={'chain': 'customer'})
        utils.astype_cols(cust_kpi)
        df = df.merge(cust_kpi, how='left', left_on=['chain_desc'],
                      right_on=['customer'])

        df['customer'] = np.where(df['customer'].isna(), df['channel'],
                                  df['customer'])
    df = df[df[date_column] <= get_last_export_date(period)]
    if period == 'st':
        df['week_date'] = df[date_column].dt.isocalendar().week

        md_prod_uf = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_EXT_PDT_MD',
                                      selection="distinct MAT_COD, SHELF_LIFE") \
            .rename(columns={'mat_cod': 'sku'})
        utils.astype_cols(md_prod_uf)
        df = df.merge(md_prod_uf, how='left', on=['sku'])
        df_uf = fa_w(df=df[df['shelf_life'] < 20],
                     act_col=act_col,
                     fc_col=fc_col,
                     ae_gr_cols=[date_column,
                                 'customer', 'dc', 'sku'],
                     out_gr_cols=list(set(
                         [date_column, 'week_date', 'customer', 'dc', 'sku'] + out_gr_cols)))
        del df_uf['Acc']
        df_uf = df_uf.groupby(
            list(set(['week_date', 'customer', 'dc', 'sku'] + out_gr_cols)),
            as_index=False,
            observed=True).sum()
        df_fa = fa_w(df=df[df['shelf_life'] >= 20],
                     act_col=act_col,
                     fc_col=fc_col,
                     ae_gr_cols=['week_date',
                                 'customer', 'dc', 'sku'],
                     out_gr_cols=list(set(['week_date', 'customer', 'dc', 'sku'] + out_gr_cols)))
        del df_fa['Acc']
        df_fa = pd.concat([df_uf, df_fa])
        ae_w = 'AE_w'
        df_fa = df_fa.groupby(out_gr_cols, as_index=False, observed=True) \
            .agg({act_col: 'sum',
                  fc_col: 'sum',
                  ae_w: 'sum'})
        df_fa['Acc'] = np.where(df_fa[act_col] == 0, 0,
                                1 - df_fa[ae_w] / df_fa[act_col])

    if period == 'mt':
        df['week_date'] = df[date_column].dt.isocalendar().week
        df_fa = fa(df=df,
                   act_col=act_col,
                   fc_col=fc_col,
                   ae_gr_cols=['week_date',
                               'customer', 'dc', 'sku'],
                   out_gr_cols=out_gr_cols)

    if period == 'lt':
        df['month_date'] = df[date_column].dt.strftime('%Y%m')
        df_fa = fa(df=df,
                   act_col=act_col,
                   fc_col=fc_col,
                   ae_gr_cols=['month_date',
                               'customer', 'sku'],
                   out_gr_cols=out_gr_cols)

    return df_fa


def metrics_promo_for_adf(df,
                          category=False,
                          product=False,
                          customer=False,
                          lvl='client',
                          forecast='fc'):
    """
    Calculates forecast accuracy, forecast bias and absolute error by category, by customer or
    in total either on chain or on techbillto level.
    :param df: pandas dataframe with actuals and forecast, should contain:
        ['Partition', 'SHIP_START', 'FT_SHIP_START', 'Date', 'Chain', 'Techbillto',
        'PRODUCT_CODE', 'PRD_ProdType', 'Ordered', 'Forecast']
    :param category: boolean, True if category detail is requested
    :param customer: boolean, True if customer detail is requested
    :param lvl: ['chain', 'techbillto'] - level of metrics calculation
    :return: pandas dataframe, contains actuals, forecast, AE, FA, BIAS in the specified detail
    """
    df = df.astype({'techbillto': 'object',
                    'client': 'object',
                    'bu': 'object',
                    'dc': 'int8',
                    'sku': 'int64'})
    df = df[(df['partition'] == 'fc') & (
            (~df['start_date_bp'].isna()) | (~df['ft_start_date'].isna()))].copy()
    df.rename(columns={'start_date_bp': 'start_date',
                       'pgi_date': 'Date'}, inplace=True)

    # переход на линейки
    prod_md = pd.read_csv('data/masterdata/Product_MD.csv', sep=';',
                          usecols=['SKU', 'Sku/линейка']).drop_duplicates()
    df = df.merge(prod_md, how='left', left_on='sku', right_on='SKU')
    df.drop(columns=['sku', 'SKU'], inplace=True)
    df = df.groupby(['Date', 'techbillto', 'client', 'Sku/линейка']).agg(
        {'start_date': 'min',
         'ft_start_date': 'min',
         'ordered': 'sum',
         forecast: 'sum'}).reset_index()

    fc_period_start = min(df['Date'])

    gr_list = ['Sku/линейка']  # ['PRODUCT_CODE', 'PRD_ProdType']
    if lvl == 'client':
        gr_list.append('client')
    else:
        gr_list.extend(['techbillto', 'client'])
    act_gr_list = gr_list + ['start_date']
    fc_gr_list = gr_list + ['ft_start_date']

    df_act = df[~df['start_date'].isna()].groupby(act_gr_list, as_index=False).agg(
        {'ordered': 'sum'})
    df_fc = df[~df['ft_start_date'].isna()].groupby(fc_gr_list, as_index=False).agg(
        {forecast: 'sum'})
    df = pd.merge(df_fc, df_act, how='left', left_on=fc_gr_list, right_on=act_gr_list)
    df = df.dropna()

    df['AE'] = abs(df['ordered'] - df[forecast])
    df = df[df['ft_start_date'] >= fc_period_start]
    df['gr'] = 'gr'

    gr_list = ['gr']
    if product:
        gr_list.append('PRODUCT_CODE')
    if category:
        gr_list.append('PRD_ProdType')
    if customer:
        gr_list.append('Chain')

    df = df.groupby(gr_list, as_index=False).sum()
    df = df.drop(columns=['gr'])

    df['Acc'] = np.where((df['ordered'] != 0), (1 - df['AE'] / df['ordered']), 0)
    df['BIAS'] = (df['ordered'] - df[forecast]) / df[forecast]
    return df
