import logging  # type: ignore
from joblib.parallel import Parallel, delayed  # type: ignore
from glob import glob  # type: ignore
from tqdm import tqdm  # type: ignore
import numpy as np  # type: ignore
import pandas as pd  # type: ignore
import shutil  # type: ignore
import os  # type: ignore
from joblibspark import register_spark


from adf.modelling.dataprep.rus import config, utils, si, promo, price

logger = logging.getLogger("adf")


def run(country: str,
        **context):
    """ Builds panel data for Russia.
    :product_level: sku
    :location_level: warehouse, national
    :division: dairy_reg, dairy_veg
    Data is saved as dask dataframe partitioned at location level
    """
    raise NotImplementedError('function in adf.modelling.dataprep.rus.panel not implemented')


def build_panel_data(
        horizon: str,
        filtered: str
):
    """
    Gets a date (PGI or MAD) and a path to
    the json configuration file with parameters

    Steps:
        - Assembles several sell-in files into one
        - Removing all rows earlier than 2019 year
        - Filtering one date (choosing sell-in on a given date)
        - Applying SKU rotations
        - Removing delisted keys
        - Dividing into PGI and MAD sell-in
        - Aggregating
        - Saves PGI and MAD sell-in into a parquet file
        - Adds BP promo days
        - Adds missed dates
        - Adds future horizon
        - Adds FT promo
        - Adds partitions
    """
    utils.create_directory_tree()
    date_column = config.DATE_COLUMN_MAPPING[horizon]
    current_time = pd.to_datetime('today').strftime('%Y-%m-%d_%H:%M')
    logging.basicConfig(filename=config.LOG_FOLDER + f'/{current_time}.log',
                        format='%(asctime)s - %(message)s',
                        level=logging.INFO,
                        datefmt='%Y-%m-%d %H:%M:%S')
    logging.info('Run preprocessing with default parameters')
    register_spark()
    utils.add_folder(config.KEYS_LISTS_FOLDER_PATH)
    last_date = utils.get_last_fc_date(horizon)
    data = si.get_sell_in(date_column, filtered, last_date)
    data = si.rotate_sku(data)
    data = si.delete_delisted_keys(data, date_column)
    data = si.delete_delisted_sku_within_warehouse(data)
    data = si.aggregate_sell_in(data, date_column)
    si.save_sell_in(data, date_column,
                    output_path=config.INTERMEDIATE_SELL_IN_PATH)
    data = si.cut_bp_promo(data, date_column)
    promo.get_fintool_promo(date_column, filtered)
    fc_start = config.FIRST_FORECAST_DATE
    price_changes = price.get_price_changes()
    utils.add_folder(config.SI_PREPROCESSING_TMP_FOLDER_PATH)
    second_preprocessing_part(data, price_changes, date_column, last_date,
                              fc_start, config.SI_PREPROCESSING_TMP_FOLDER_PATH,
                              config.PREPROCESSED_PATH)
    # utils.save_lost_keys() # TODO: check, fix or del


@utils.gc_collect
def second_preprocessing_part(data: pd.DataFrame, price_changes: pd.DataFrame,
                              date_column: str, last_date: pd.Timestamp,
                              fc_start: pd.Timestamp, tmp_folder: str, output_path: str) -> None:
    shutil.rmtree(tmp_folder)
    os.mkdir(tmp_folder)
    path_ft = config.FT_OUTPUT_FILE_PATH
    Parallel(n_jobs=-1, backend='spark')(delayed(preprocessing_one_sku)(
        sample, price_changes, sku, date_column, last_date, fc_start, tmp_folder, path_ft)
                        for sku, sample in tqdm(data.groupby('sku'), total=data.sku.nunique()))
    # TODO: code review: set observed=True, sort=False

    data = Parallel(n_jobs=-1, backend='spark')(
        delayed(pd.read_parquet)(path)
        for path in glob(f'{tmp_folder}/*.parquet'))
    data = utils.concat_categorical(data, ignore_index=True)

    data[config.SI_KEY_COLUMNS + ['partition']] = \
        data[config.SI_KEY_COLUMNS + ['partition']] \
        .astype('category')
    data[date_column] = data[date_column].astype('datetime64[D]')
    data['ordered'] = np.where(data['ordered'] < 0, 0, data['ordered'])
    data.to_parquet(output_path, index=False)
    return data


@utils.gc_collect
def preprocessing_one_sku(data: pd.DataFrame, price_changes: pd.DataFrame,
                          sku: str, date_column: str, last_date: pd.Timestamp,
                          fc_start: pd.Timestamp, output_folder: str, path_ft: str) -> None:
    data = si.add_missed_dates(data, date_column, last_date)
    data = si.add_ft_promo(data, date_column, path_ft, fc_start)
    data = si.process_prices(data, date_column, fc_start)
    data = price.calc_future_prices(data, price_changes, date_column, fc_start)
    if len(data) > 0:
        data = set_partitions(data, date_column, fc_start)
        data = data[data[date_column] <= last_date]
        data = si.aggregate_prep_sell_in(data, date_column)
        data[config.SI_KEY_COLUMNS + ['partition']] = \
            data[config.SI_KEY_COLUMNS + ['partition']].astype('category')
        data.to_parquet(f'{output_folder}/{sku}.parquet', index=False)


def set_partitions(data: pd.DataFrame, date_column: str, fc_start: pd.Timestamp,) -> pd.DataFrame:
    data['partition'] = np.where(data[date_column] <
                                 fc_start, 'train', 'fc')
    data['partition'] = data['partition'].astype('category')
    return data
