import numpy as np
import pandas as pd
from adf.modelling.dataprep.rus.tools import get_snowflake_df
from adf.modelling.dataprep.rus import config, utils


def get_price_changes() -> pd.DataFrame:
    """
    Gets and processes the price changes dataset
    """
    pc = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_EXT_SND_PRC_INC',
                          selection="*",
                          filter_query="WHERE PRICE_CHANGE != 0")
    pc['price_change'] = pc['price_change'].astype(float)
    pc['date'] = pc['month'].apply(lambda x: x + '-01')
    pc['date'] = pd.to_datetime(pc['date'])
    pc.drop(columns=['month'], inplace=True)
    pc.sort_values('version', inplace=True)
    pc.drop_duplicates(['date', 'sku'], keep='last', inplace=True)
    pc.sort_values('date', inplace=True)
    return pc


def calc_future_prices_one_key(sample: pd.DataFrame, price_changes: pd.DataFrame,
                               date_column: str, fc_start_date: pd.Timestamp) -> pd.DataFrame:
    """
    Calculates future prices for a given sample (one key) and replaces (or adds)
    them from the given date till the end of the sample
    """
    sample.sort_values(date_column, inplace=True)
    fc_end_date = sample[date_column].max()
    future_price = pd.DataFrame({date_column: sample[sample['discount'] == 0][date_column]})
    past_regular = sample[(sample[date_column] < fc_start_date) &
                          (sample['discount'] == 0) &
                          (sample['price'].notna())].sort_values(date_column)
    if len(past_regular):
        start_price_point = past_regular.iloc[-1]
    elif len(sample[(sample[date_column] >= fc_start_date) &
                    (sample['discount'] == 0) &
                    (sample['price'].notna())]):
        start_price_point = sample[(sample[date_column] >= fc_start_date) &
                                   (sample['discount'] == 0) &
                                   (sample['price'].notna())].sort_values(date_column).iloc[0]
    else:
        sample['price'] = None
        return sample
    future_price['future_price'] = start_price_point['price']
    start_price_point_date = start_price_point[date_column]
    for row in price_changes[(price_changes.sku == sample.sku.unique()[0]) &
                             (price_changes.date >= start_price_point_date) &
                             (price_changes.date < fc_end_date)].sort_values('date').itertuples():
        future_price['future_price'] = np.where(future_price[date_column] > row.date,
                                                future_price['future_price'] *
                                                (1 + row.price_change),
                                                future_price['future_price'])

    future_price = future_price.merge(pd.DataFrame(
        {date_column: pd.date_range(fc_start_date, fc_end_date, normalize=True)}),
        # normalize can be deleted when fc_start_date have date only without time
        on=date_column, how='right')
    future_price['future_price'] = future_price['future_price'].ffill()
    if future_price['future_price'].isna().sum():
        future_price['future_price'].iloc[0] = start_price_point['price']
        future_price['future_price'] = future_price['future_price'].ffill()
    future_price = future_price.merge(sample[[date_column, 'discount']],
                                      on=date_column, how='left')
    future_price['discount'].fillna(0, inplace=True)
    future_price['future_price'] = future_price['future_price'] * \
        (1 - future_price['discount'] / 100)

    sample = sample.merge(future_price[[date_column, 'future_price']],
                          on=date_column, how='left')
    sample['price'] = np.where(sample[date_column] >= fc_start_date,
                               sample['future_price'],
                               sample['price'])
    sample.drop(columns=['future_price'], inplace=True)
    return sample


def calc_future_prices(sell_in: pd.DataFrame, price_changes: pd.DataFrame,
                       date_column: str, fc_start_date: pd.Timestamp) -> pd.DataFrame:
    """
    Calculates future prices for the sell in dataset and replaces (or adds)
    them from the given date till the end of the sample
    """
    sell_in = [calc_future_prices_one_key(sample, price_changes, date_column, fc_start_date)
               for _, sample in sell_in.groupby(config.SI_KEY_COLUMNS, observed=True)]
    sell_in = utils.concat_categorical(sell_in, ignore_index=True)
    return sell_in
