import pandas as pd
import numpy as np
import logging

from adf.modelling.dataprep.rus.tools import get_snowflake_df
import adf.modelling.dataprep.rus.utils as utils
import adf.modelling.dataprep.rus.config as config


def calc_promo_day(discount: list) -> list:
    """
    Numerates promo days of the promo slots for a given discount column
    may be used with a 'transform' method of groupby
    """
    promo_day_feature = list()
    promo_day = 0
    prev_discount = None
    for element in list(discount):
        if element == 0:
            promo_day_feature.append(0)
            promo_day = 0
        else:
            if prev_discount != element:
                promo_day = 0
            promo_day += 1
            promo_day_feature.append(promo_day)
        prev_discount = element
    return promo_day_feature


def calc_promo_length(discount: list) -> list:
    """
    Calculates promo length of the promo slots from a given discount column
    may be used with a 'transform' method of groupby
    """
    promo_length_feature = list()
    promo_length = 0
    prev_discount = None
    for element in list(discount):
        if element == 0:
            if promo_length > 0:
                promo_length_feature += [promo_length] * promo_length
            promo_length = 0
            promo_length_feature.append(promo_length)
        else:
            if prev_discount != element:
                if promo_length > 0:
                    promo_length_feature += [promo_length] * promo_length
                    promo_length = 1
                else:
                    promo_length += 1
            else:
                promo_length += 1
        prev_discount = element
    if promo_length > 0:
        promo_length_feature += [promo_length] * promo_length
    return promo_length_feature


def add_promo_day(data: pd.DataFrame, date_column: str) -> pd.DataFrame:
    """
    Adds a promo day feature for a given whole sell-in dataset
    """
    data.sort_values(date_column, inplace=True)
    data['promo_day'] = data.groupby(config.SI_KEY_COLUMNS)['discount'].transform(calc_promo_day)
    return data


def add_promo_length(data: pd.DataFrame, date_column: str) -> pd.DataFrame:
    """
    Adds a promo length feature for a given whole sell-in dataset
    """
    data.sort_values(date_column, inplace=True)
    data['promo_length'] = data.groupby(config.SI_KEY_COLUMNS)['discount'] \
        .transform(calc_promo_length)
    return data


def calc_promo_id(discount: list) -> list:
    """
    Numerates promo slots according to a given discount column (sets promo id)
    may be used with a 'transform' method of groupby
    """
    promo_id_list = list()
    promo_id = 0
    prev_discount = None
    for element in list(discount):
        if element == 0:
            promo_id_list.append(0)
        else:
            if prev_discount != element:
                promo_id += 1
            promo_id_list.append(promo_id)
        prev_discount = element
    return promo_id_list


def add_promo_id(data: pd.DataFrame, date_column: str) -> pd.DataFrame:
    """
    Adds a column with generated promo ids for a given whole sell-in dataset
    """
    data.sort_values(date_column, inplace=True)
    data['promo_id'] = data.groupby(config.SI_KEY_COLUMNS)['discount'].transform(calc_promo_id)
    return data


def get_r1() -> pd.DataFrame:
    r1 = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_EXT_CUS_MD',
                          selection="distinct TBT, PRICE_GROUP")
    r1.rename(columns={'tbt': 'techbillto'}, inplace=True)
    r1.sort_values(by='price_group', inplace=True)
    r1 = r1.groupby('techbillto').first().reset_index()
    r1['price_group'] = np.where(r1['price_group'] == 'R1', 1, 0)
    return r1


def add_r1(promo_df: pd.DataFrame) -> pd.DataFrame:
    r1 = get_r1()
    promo_df = promo_df.merge(r1, on='techbillto', how='left')
    promo_df['price_group'].fillna(0, inplace=True)
    promo_df = promo_df.astype({'price_group': 'int'})
    return promo_df


def adjust_dates_r1(promo_df: pd.DataFrame, date_column: str) -> pd.DataFrame:
    if date_column == 'mad_date':
        promo_df['start_date'] = np.where(promo_df['price_group'] == 1,
                                          promo_df['ship_start'],
                                          promo_df['ship_start'] -
                                          pd.Timedelta(days=1))
        promo_df['end_date'] = np.where(promo_df['price_group'] == 1,
                                        promo_df['ship_end'],
                                        promo_df['ship_end'] -
                                        pd.Timedelta(days=1))

    if date_column == 'pgi_date':
        promo_df['start_date'] = np.where(promo_df['price_group'] == 1,
                                          promo_df['ship_start'] +
                                          pd.Timedelta(days=1),
                                          promo_df['ship_start'])
        promo_df['end_date'] = np.where(promo_df['price_group'] == 1,
                                        promo_df['ship_end'] +
                                        pd.Timedelta(days=1),
                                        promo_df['ship_end'])
    promo_df.drop(columns=['price_group', 'ship_start', 'ship_end'], inplace=True)
    return promo_df


def add_ft_promo_to_df(data: pd.DataFrame,
                       fc_start: pd.Timestamp,
                       date_column: str = None,
                       path_ft: str = None) -> pd.DataFrame:
    """
    Merging fintool promo to sell-in with filled future dates starting with
    the given date (ft_start, ft_end)

    :param fc_start: forecast start date
    :param date_column: date column ('mad_date' or 'pgi_date')
    :param data: sellin (whole or future period)
    :param path_ft: path to fintool promo
    :return: incoming df with info about NKA promo on future
    """
    promo = pd.read_parquet(path_ft)
    promo.rename(columns={'promo_date': date_column}, inplace=True)
    promo.dropna(inplace=True)
    promo['discount_ft'] = promo['discount_ft'] * 100
    promo[date_column] = pd.to_datetime(promo[date_column], format='%Y-%m-%d')
    promo[['sku', 'techbillto']] = promo[['sku', 'techbillto']].astype(int)
    promo = promo[promo[date_column] >= (fc_start + pd.Timedelta(weeks=4))]
    cols_on = [date_column] + config.SI_KEY_COLUMNS
    data = data.merge(promo, on=cols_on, how='left')
    return data


def map_sku_sap_ft(ft_df: pd.DataFrame) -> pd.DataFrame:
    """
    Maps the sku from Fintool to the sku from SAP

    """
    map_sku = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_EXT_PDT_MD',
                               selection="distinct MAT_COD, FT_SKU",
                               filter_query="where ACTIVE = TRUE")
    map_sku.rename(columns={'mat_cod': 'sku'}, inplace=True)
    map_sku = map_sku[~map_sku['ft_sku'].isna()]
    ft_df = ft_df.merge(map_sku, how='left', on='ft_sku')
    ft_df.drop(columns=['ft_sku'], inplace=True)
    ft_df = ft_df[~ft_df['sku'].isna()]
    return ft_df


@utils.gc_collect
def get_fintool_promo(date_column: str,
                      filtered: str) -> pd.DataFrame:
    """
    Prepares files from Fintool for all channels

    """
    promo_q = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_SND_FINTOOL_IBP',
                               selection="distinct QUARTER")
    start_ft = config.FIRST_FORECAST_DATE + pd.Timedelta(weeks=4)
    start_ft_q, start_ft_y = start_ft.quarter, start_ft.year
    promo_q['q'], promo_q['y'] = \
        promo_q['quarter'].str[1:2].astype(int), promo_q['quarter'].str[3:7].astype(int)
    promo_q = promo_q.sort_values(by='quarter', ascending=False)
    promo_ft_quarter = promo_q[(promo_q['y'] == start_ft_y) & (promo_q['q'] == start_ft_q)
                               ].quarter.values[0]
    cols = ', '.join([col.upper() for col in config.FT_COLUMNS_RENAME.keys()])
    if len(filtered):
        filtered = " AND " + filtered
    ft_q_curr = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_SND_FINTOOL_IBP',
                                 selection=f"{cols}",
                                 filter_query=f"where QUARTER = '{promo_ft_quarter}'{filtered}")
    start_ft_q_next = start_ft_q + 1 if start_ft_q < 4 else 1
    start_ft_y_next = start_ft_y if start_ft_q < 4 else start_ft_y + 1
    promo_ft_quarter_next = promo_q[(promo_q['y'] == start_ft_y_next) &
                                    (promo_q['q'] == start_ft_q_next)].quarter.values[0]
    ft_q_next = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_SND_FINTOOL_IBP',
                                 selection=f"{cols}",
                                 filter_query=f"where QUARTER "
                                              f"= '{promo_ft_quarter_next}'{filtered}")
    ft = pd.concat([ft_q_curr, ft_q_next])
    del ft_q_curr, ft_q_next
    ft.rename(columns=config.FT_COLUMNS_RENAME, inplace=True)
    ft = ft[list(config.FT_DTYPE.keys())].drop_duplicates()
    ft = ft.astype(config.FT_DTYPE)
    ft = ft[~ft.ft_sku.isna()]
    ft = map_sku_sap_ft(ft)
    ft = add_r1(ft)
    ft = adjust_dates_r1(ft, date_column)
    ft_day = utils.expand_df_period(ft,
                                    start='start_date',
                                    end='end_date')
    ft_day.rename(columns={'discount': 'discount_ft',
                           'date': 'promo_date'}, inplace=True)
    ft_day.to_parquet(config.FT_OUTPUT_FILE_PATH, index=False)
    logging.info('Promo Fintool done')
    return ft_day
