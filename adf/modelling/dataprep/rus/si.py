from collections import defaultdict

from adf.modelling.dataprep.rus.tools import get_snowflake_df

import numpy as np

import pandas as pd
from joblib import Parallel, delayed

from tqdm.notebook import tqdm

from . import config
from . import promo
from . import utils


def filter_keys(data: pd.DataFrame, filtered: dict):
    """
    Generates a request for a filter

    :param data: sellin dataframe
    :param filtered: request for a filter
    :return: query
    """
    query = ' & '.join([f'({col} in {filter})'
                        for col, filter in filtered.items()])
    return data.query(query)


def get_actual_skus(date_column: str) -> list:
    """
    Gets list of actuals sku

    :param date_column: name of date column
    :return: list of sku
    """
    dataset_name = 'V_CDS_F_SELLIN_MAD' if date_column == 'mad_date' else 'V_CDS_F_SELLIN_PGI'
    sku_list = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP',
                                dataset_name, selection="DISTINCT SKU")
    sku_list = sku_list[sku_list['sku'] != '#']
    sku_list = set(sku_list['sku'])
    last_si_date = get_snowflake_df(config.SNOWFLAKE_DATABASE,
                                    'CIS_DSP_IBP',
                                    dataset_name,
                                    selection=f"MAX({date_column.upper()})").iloc[0, 0]
    delisting = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_EXT_SND_DEL',
                                 selection="SKU, DATE_OUT", filter_query="WHERE WH is Null")
    delisting = delisting[delisting['date_out'] < last_si_date]
    # TODO: change threshold date into relevant
    delisting = set(delisting['sku'])
    sku_list = list(sku_list - delisting)
    return sku_list


@utils.gc_collect
@utils.check_loss
def get_sell_in(date_column: str, filter_query: str,
                last_date: pd.Timestamp = None) -> pd.DataFrame:
    """
    Obtains sell in from Snowflake, transforms data types and
    groups by the granularity

    :param date_column: name of date column
    :param filter_query: query of filter
    :param last_date: last date of forecast
    :return: dataframe
    """
    if date_column == 'mad_date':
        rename_mapping = config.SI_MAD_COLUMNS_RENAME_MAPPING
        dtype = config.SI_MAD_DTYPE
        dataset_name = 'V_CDS_F_SELLIN_MAD'
    else:
        rename_mapping = config.SI_PGI_COLUMNS_RENAME_MAPPING
        dtype = config.SI_PGI_DTYPE
        dataset_name = 'V_CDS_F_SELLIN_PGI'
    cols = ', '.join([col.upper() for col in rename_mapping.keys()])
    if len(filter_query):
        filter_query = " AND " + filter_query

    def read_one_sku_from_sf(sku):
        sample = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', dataset_name,
                                  selection=f"{cols}",
                                  filter_query=f"WHERE SKU = '{sku}'{filter_query}")
        sample.rename(columns=rename_mapping, inplace=True)
        sample = sample.astype(dtype)
        return sample

    sku_list = get_actual_skus(date_column=date_column)
    data = Parallel(n_jobs=-1, backend='threading')(
        delayed(read_one_sku_from_sf)(sku) for sku in tqdm(sku_list))
    data = utils.concat_categorical(data)
    if last_date:
        data = data[data[date_column] <= last_date]
    return data


@utils.gc_collect
@utils.check_loss
def rotate_sku(data: pd.DataFrame) -> pd.DataFrame:
    """
    Applies rotations to SKUs in sell-in according to
    the file with rotations
    """
    rotation = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_EXT_LNG_PLN')
    rotation = rotation[config.ROT_COLUMNS].rename(columns=config.ROT_COLUMNS_RENAME)
    rotation = rotation[~rotation['old_sku'].isna()]
    rotation['old_sku'] = rotation['old_sku'].astype(int)
    rotation = rotation.groupby(['new_sku', 'old_sku'])['date'].min().reset_index()
    rotation.sort_values('date', ascending=True, inplace=True)

    for sku in list(set(rotation.old_sku) & set(rotation.new_sku)):
        rotation['new_sku'][(rotation.old_sku == sku) |
                            (rotation.new_sku == sku)] = \
            rotation['new_sku'][(rotation.old_sku == sku) |
                                (rotation.new_sku == sku)].iloc[-1]

    rotation = rotation[rotation.new_sku != rotation.old_sku]
    rotation = dict(zip(rotation.old_sku, rotation.new_sku))
    data['sku'].replace(rotation, inplace=True)
    data['sku'] = data['sku'].astype('category')
    return data


@utils.check_loss
def delete_delisted_keys(data: pd.DataFrame,
                         date_column: str) -> pd.DataFrame:
    """
    Removes keys from the dataframe that
    do not have positive volumes within the last 3 months
    """
    target_keys = data[data['ordered'] > 0] \
        .groupby(config.SI_KEY_COLUMNS,
                 as_index=False,
                 observed=True)[date_column].max() \
        .rename(columns={date_column: 'max_date'})
    first_fc_date = pd.to_datetime(config.FIRST_FORECAST_DATE, format='%Y-%m-%d')
    period_start_date = first_fc_date - pd.Timedelta(days=90)

    target_keys = target_keys[target_keys.max_date > period_start_date][
        config.SI_KEY_COLUMNS]
    data = data.merge(target_keys, how='right', on=config.SI_KEY_COLUMNS)
    return data


@utils.gc_collect
@utils.check_loss
def delete_delisted_sku_within_warehouse(data: pd.DataFrame) -> pd.DataFrame:
    """
    Removes delisted SKUs according to the file
    with delisted SKUs within warehouses
    """
    delisted = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_EXT_SND_DEL')
    delisted = delisted[delisted.wh.notna()].rename(columns={'wh': 'dc'})
    delisted['date_out'] = pd.to_datetime(delisted['date_out'])
    first_fc_date = pd.to_datetime(config.FIRST_FORECAST_DATE, format='%Y-%m-%d')
    period_start_date = first_fc_date - pd.Timedelta(days=90)
    delisted = delisted[(delisted.date_out > period_start_date) &
                        (delisted.date_out < config.FIRST_FORECAST_DATE)]
    delisted['flag'] = 0
    delisted.drop(columns=['date_out'], inplace=True)
    data = data.merge(delisted, on=['sku', 'dc'], how='left')
    data = data[data['flag'].isna()]
    data.drop(columns=['flag'], inplace=True)
    data[['sku', 'dc']] = data[['sku', 'dc']].astype('category')
    return data


def mark_innovations(data: pd.DataFrame,
                     date_column: str,
                     train_period_end: str,
                     remove: bool = False) -> pd.DataFrame:
    """
    Marks keys that's history length is less than 4 weeks
    For removing innovations, set the 'remove' argument True
    """
    inno = data.query('ordered > 0') \
        .groupby(config.SI_KEY_COLUMNS, as_index=False, observed=True) \
        .agg({date_column: 'first'})
    inno['innovation'] = np.where(inno[date_column] >
                                  pd.to_datetime(train_period_end) -
                                  pd.Timedelta(days=28),
                                  True, False)
    inno = inno.drop(columns=[date_column])
    data = data.merge(inno, on=config.SI_KEY_COLUMNS, how='left')
    data['innovation'].fillna(False, inplace=True)
    if remove:
        data = data[data['innovation'] == 0]
        data.drop(columns=['innovation'], inplace=True)
    return data


@utils.check_loss
def aggregate_sell_in(data: pd.DataFrame,
                      date_column: str) -> pd.DataFrame:
    """
    Aggregates data (for drop duplicates)

    :param data: dataframe
    :param date_column: name of date column
    :return: dataframe w/o duplicates
    """
    data = data.groupby([date_column] + config.SI_KEY_COLUMNS,
                        as_index=False,
                        observed=True).agg(config.SI_AGG_RULES)
    return data


@utils.check_loss
def save_sell_in(data: pd.DataFrame,
                 date_column: str,
                 output_path: str) -> pd.DataFrame:
    """
    Transforms columns into more efficient type and saves sell-in with
    a given date
    """
    data[config.SI_CATEGORICAL_COLUMNS] = \
        data[config.SI_CATEGORICAL_COLUMNS].astype('category')
    data.to_parquet(output_path, index=False)
    return data


@utils.gc_collect
def add_missed_dates(data: pd.DataFrame,
                     date_column: str,
                     last_date: pd.Timestamp) -> pd.DataFrame:
    """
    Adds missed dates in sell-in
    """
    # categories = utils.get_categories(data, config.SI_KEY_COLUMNS)
    result = defaultdict(list)
    for key_values, sample in data.groupby(config.SI_KEY_COLUMNS):
        key_start_date = max(sample[date_column].min(),  # can be ordered = 0 in the beginning
                             config.FIRST_SELL_IN_DATE)
        dates = pd.date_range(start=key_start_date,
                              end=last_date)
        result[date_column].extend(dates)
        length = len(dates)

        for col, val in zip(config.SI_KEY_COLUMNS, key_values):
            result[col].extend([val] * length)
    result = pd.DataFrame.from_dict(result)
    if len(result) > 0:
        data = result.merge(data, on=[date_column] + config.SI_KEY_COLUMNS,
                            how='left')
        data.fillna({'ordered': 0, 'discount': 0}, inplace=True)
        data = data.astype({'ordered': 'float32',
                            'discount': 'uint8'})
    return data


@utils.gc_collect
def add_ft_promo(data: pd.DataFrame,
                 date_column: str,
                 path_ft: str,
                 fc_start: pd.Timestamp) -> pd.DataFrame:
    """
    Adds fintool promo

    :param data: dataframe
    :param date_column: name of date column
    :param path_ft: path to fintool promo
    :param fc_start: forecast start date
    :return: dataframe with Fintool promo
    """
    data[['sku', 'techbillto']] = data[['sku', 'techbillto']].astype(int)
    data = promo.add_ft_promo_to_df(data=data,
                                    fc_start=fc_start,
                                    date_column=date_column,
                                    path_ft=path_ft)
    data['discount'] += data['discount_ft'].fillna(0)
    data.drop(columns=['discount_ft'], inplace=True)
    data.fillna({'ordered': 0,
                 'discount': 0,
                 }, inplace=True)
    return data


def cut_bp_promo(sell_in: pd.DataFrame, date_column: str) -> pd.DataFrame:
    """
    Removes promo slots from the 4th week of the forecast period
    """
    last_bp_day = config.FIRST_FORECAST_DATE + pd.Timedelta(weeks=4)
    sell_in['discount'] = np.where(sell_in[date_column] < last_bp_day, sell_in['discount'], 0)
    return sell_in


def delete_delisted_keys_for_fc(data):
    """
    Drop keys using file, which was generated from preprocessing for mvp
    (delisted in Q2 and Q3 2021)
    """
    delisted_keys_q2q3 = pd.read_csv(f'{config.path}/data/masterdata/del_q2_q3.csv')
    data = data.merge(delisted_keys_q2q3, on=['sku', 'techbillto'], how='left')
    data = data[data['type'].isna()]
    data.drop(columns='type', inplace=True)
    return data


def process_prices(sell_in: pd.DataFrame, date_column: str,
                   fc_start_date=None) -> pd.DataFrame:
    """
    Calculates price from cost and
    fills missing price values according to the nearest last or next value
    """
    sell_in['price'] = sell_in['sales_invoiced_rub'] / sell_in['invoiced']
    sell_in.drop(columns=['sales_invoiced_rub', 'invoiced'], inplace=True)
    sell_in = promo.add_promo_id(sell_in, date_column)
    sell_in.sort_values(config.SI_KEY_COLUMNS + [date_column], inplace=True)
    sell_in['price'] = sell_in.groupby(config.SI_KEY_COLUMNS + ['promo_id'])['price'] \
        .ffill().bfill()
    sell_in.drop(columns=['promo_id'], inplace=True)
    if fc_start_date:
        sell_in['price'] = np.where(sell_in[date_column] > fc_start_date,
                                    None, sell_in['price'])
    return sell_in


def aggregate_prep_sell_in(data: pd.DataFrame,
                           date_column: str) -> pd.DataFrame:
    """
    Aggregates data after preprocessing (for drop duplicates)

    :param data: dataframe
    :param date_column: name of date column
    :return: dataframe w/o duplicates
    """
    data.sort_values(config.SI_KEY_COLUMNS + [date_column], inplace=True)
    data = data.groupby([date_column] + config.SI_KEY_COLUMNS,
                        as_index=False,
                        observed=True).agg(config.SI_PREP_AGG_RULES)
    return data
