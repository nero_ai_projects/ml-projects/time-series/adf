import logging
from datetime import date
from typing import Optional, Union

from adf import utils
from adf.utils.dataframe import create_table, get_col_types
from adf.modelling.dataprep.rus import config


import pandas as pd  # type: ignore

log = logging.getLogger("adf")


def get_snowflake_df(
        database: Union[str, Optional[str]], schema: str, source: str,
        selection: str = "*",
        filter_query: str = "", **kwargs
) -> pd.DataFrame:
    """
    Loads pandas dataframe given schema x database x view/table

    Parameters
    ----------
    schema: str
        schema of the view or table
    database: str
        database in which the view or table is stored
    source: str
        table or view name
    selection: str, default is ""
        additional condition
    filter_query: str, default is ""
        additional filter in query that should be added to default query

    Returns
    -------
    pandas.DataFrame
    """
    ctx = utils.get_snowflake_connector(database=database, schema=schema, **kwargs)
    query = f'select {selection} from "{database}"."{schema}"."{source}"'
    if filter_query != "":
        query += f' {filter_query}'
    data = pd.read_sql(query, ctx)
    data = data.rename(columns={k: k.lower() for k in data.columns})
    # Convert time columns into pandas datetime
    for column in [col for col in data.columns
                   if len(data[col].dropna()) > 0 and isinstance(data[col].dropna().iloc[0], date)]:
        data[column] = pd.to_datetime(data[column], errors="coerce")
    return data


def export_fc_to_snowflake(
        df: pd.DataFrame,
        horizon: str,
        date_column: str,
        export_table: str = 'test',
        magnit: bool = False
):
    """
        Upload pandas dataframe to Snowflake database.schema.view/table

    :param df: export dataframe
    :param horizon: type of horizon
    :param date_column: name of date column
    :param export_table: type of table for export to Snowflake:
        production or test version ('prod' or 'test')
    """
    num_cols = ['bl', 'uplift', 'csl']
    gr_cols = config.SI_KEY_COLUMNS + [date_column]
    df = df[df[date_column] >= config.FIRST_FORECAST_DATE]
    last_fc_date = get_last_export_date(horizon)
    df = df[df[date_column] <= last_fc_date]
    df[list(set(num_cols).difference(df.columns))] = 0
    df[num_cols] = df[num_cols].round(3)
    if horizon == 'st':
        if magnit:
            cols = config.ADF_EXPORT_ST_MAGNIT
        else:
            cols = config.ADF_EXPORT_ST
    elif horizon == 'mt':
        cols = config.ADF_EXPORT_MT
        df[date_column] = df[date_column] - pd.TimedeltaIndex(df[date_column].dt.dayofweek,
                                                              unit='d')
        df = df.groupby(gr_cols,
                        as_index=False,
                        observed=True)[num_cols].sum().reset_index()
    else:
        cols = config.ADF_EXPORT_LT
        df[date_column] = df[date_column] - pd.TimedeltaIndex(df[date_column].dt.day - 1,
                                                              unit='d')
        df = df.groupby(gr_cols,
                        as_index=False,
                        observed=True)[num_cols].sum().reset_index()
    df[list(df.select_dtypes("category").columns)] = df[
        list(df.select_dtypes("category").columns)].astype(str)
    df['so'] = 5000
    df['version'] = pd.to_datetime("today").strftime("%Y-%m-%d %H:%m")
    df.rename(columns=config.EXPORT_KEY_COLUMNS_RENAME, inplace=True)
    df = df[cols]

    df[date_column] = df[date_column].dt.strftime("%Y-%m-%d")
    col_type = get_col_types(df)
    test_table = '' if export_table == 'prod' else '_TST'
    magnit_table = '' if not magnit else '_MAGNIT'
    if export_table == 'prod' and not magnit:
        create_table(f"ADF_OUTPUT_{horizon.upper()}",
                     'create_or_append', col_type, df, schema="CIS_DWH")
    elif export_table == 'test' or magnit:
        create_table(f"ADF_OUTPUT_{horizon.upper()}{magnit_table}{test_table}",
                     'create_or_append', col_type, df, schema="CIS_DMT_IBP")


def get_last_export_date(horizon: str) -> pd.Timestamp:
    """
    Determines the latest forecast date for exporting to snowflake by horizon type

    :param horizon: type of horizon
    :return: last forecast date for exporting
    """
    last_date = config.LAST_SELL_IN_DATE
    if horizon == 'st':
        last_date = config.FIRST_FORECAST_DATE + pd.Timedelta(weeks=6)
    elif horizon == 'mt':
        last_date_tech = config.FIRST_FORECAST_DATE + pd.Timedelta(days=3 * 31)
        last_date = last_date_tech + pd.Timedelta(days=6 - last_date_tech.weekday())
    elif horizon == 'lt':
        last_date_tech = config.FIRST_FORECAST_DATE + pd.Timedelta(days=18 * 31)
        last_date = last_date_tech + pd.Timedelta(
            days=last_date_tech.days_in_month - last_date_tech.day)
    return last_date


def export_fc_to_snowflake_d0(
        df: pd.DataFrame,
        export_table: str = 'prod'
):
    df['so'] = 5000
    df['version'] = pd.to_datetime("today").strftime("%Y-%m-%d %H:%m")
    df['mad_date'] = df['mad_date'].dt.strftime("%Y-%m-%d")
    df[list(df.select_dtypes("category").columns)] = df[
        list(df.select_dtypes("category").columns)].astype(str)

    df.rename(columns=config.EXPORT_KEY_COLUMNS_RENAME, inplace=True)
    df = df[config.SPLIT_EXPORT_ST]

    col_type = get_col_types(df)

    if export_table == 'prod':
        create_table("ADF_OUTPUT_ST_D0",
                     'create_or_append', col_type, df, schema="CIS_DWH")
    elif export_table == 'test':
        create_table("ADF_OUTPUT_ST_D0_TST",
                     'create_or_append', col_type, df, schema="CIS_DWH")
