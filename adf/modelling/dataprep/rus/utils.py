import numpy as np
import pandas as pd
import pyarrow as pa
import os
import logging
import pyarrow.parquet as pq
from collections import defaultdict
from glob import glob
import shutil
from pandas.api.types import union_categoricals

from . import config
from typing import Union

logger = logging.getLogger(__name__)


def gc_collect(func):
    '''
    Wrapper that call gc.collect after function calls
    '''

    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)

        import gc
        _ = gc.collect()

        return result

    return wrapper


def log(func):
    def wrapper(*args, **kwargs):
        report_func_name = f'{func.__name__}'
        logger.info(f'{report_func_name}')
        data = func(*args, **kwargs)
        return data

    return wrapper


def check_loss(func):
    '''
    Decorator for saving list of keys with additional data such as
    ordered volumes after a step of preprocessing
    '''

    def wrapper(*args, **kwargs):
        report_func_name = f'{func.__name__}'
        logger.info(f'{report_func_name}')
        data = func(*args, **kwargs)
        report = data[config.SI_KEY_COLUMNS].drop_duplicates()

        # report files numeration
        report_paths = sorted(glob(f'{config.KEYS_LISTS_FOLDER_PATH}/*'))
        if len(report_paths) == 0:
            last_number = '01'
        else:
            last_number = int(report_paths[-1].split('/')[-1].split('_')[0])
            last_number = str(last_number + 1).zfill(2)

        report.to_parquet(f'{config.KEYS_LISTS_FOLDER_PATH}/'
                          f'{last_number}_{report_func_name}.parquet',
                          index=False)
        size = round(sum(data.memory_usage(deep=True)) / 2 ** 30, 2)
        logger.info(f'si_shape={data.shape} '
                    f'keys={report.shape[0]} '
                    f'ordered={data.ordered.sum()} '
                    f'size={size} GB '
                    f'{data.dtypes}')
        return data

    return wrapper


@log
def save_lost_keys() -> None:
    '''
    Assembles datasets with keys have been saved after each preprocessing step
    and obtains list of lost keys with identification where they have been lost
    '''
    report_file_paths = sorted(glob(f'{config.KEYS_LISTS_FOLDER_PATH}/*'))
    steps = ['_'.join(path.split('/')[-1].split('.')[0].split('_')[1:])
             for path in report_file_paths]
    reports_list = list()
    for step, path_prev, path_next in zip(steps[1:],
                                          report_file_paths[:-1],
                                          report_file_paths[1:]):
        report_prev = pd.read_parquet(path_prev)
        report_next = pd.read_parquet(path_next)
        report = report_prev.merge(report_next,
                                   on=config.SI_KEY_COLUMNS,
                                   how='outer',
                                   indicator=True)
        report = report[report._merge == 'left_only'][config.SI_KEY_COLUMNS]
        report['step'] = step
        reports_list.append(report)
    report = pd.concat(reports_list, ignore_index=True)
    report['dc'] = report['dc'].astype(int)
    # TODO: should be found, why it crashes with an error if not to make 'dc' column a int type
    report.to_parquet(config.CHECK_LOSS_FILE_PATH, index=False)
    shutil.rmtree(config.KEYS_LISTS_FOLDER_PATH)
    os.mkdir(config.KEYS_LISTS_FOLDER_PATH)


def convert_types(
        data: pd.DataFrame, float_columns: list,
        int_columns: list
) -> pd.DataFrame:
    '''
    Converts into more memory efficient datatypes

    Works only with float type yet (float64 -> float32), but
    if we use other int columns, we should add processing these ones
    '''
    data[float_columns] = data[float_columns].apply(pd.to_numeric,
                                                    downcast='float')
    data[int_columns] = data[int_columns].apply(pd.to_numeric,
                                                downcast='integer')
    return data


def append_to_parquet_table(dataframe, filepath, writer=None):
    '''Method writes/append dataframes in parquet format.
    link # https://stackoverflow.com/questions/47113813/
    using-pyarrow-how-do-you-append-to-parquet-file

    This method is used to write pandas DataFrame as pyarrow Table in
    parquet format. If the methods is invoked with writer,
    it appends dataframe to the already written pyarrow table.

    :param dataframe: pd.DataFrame to be written in parquet format.
    :param filepath: target file location for parquet file.
    :param writer: ParquetWriter object to write pyarrow tables
    in parquet format.
    :return: ParquetWriter object. This can be passed in the subsequenct
    method calls to append DataFrame in the pyarrow Table
    '''
    table = pa.Table.from_pandas(dataframe)
    if writer is None:
        writer = pq.ParquetWriter(filepath, table.schema)
    writer.write_table(table=table)
    return writer


def add_folder(path: str) -> None:
    '''
    Creates folder
    '''
    if not os.path.exists(path):
        os.mkdir(path)


def create_directory_tree() -> None:
    """
    Creates the necessary folders
    """
    paths = [
        'data/masterdata',
        'data/processed',
        'data/intermediate',
        'data/raw/price_increase',
        'data/raw/promo_bp',
        'data/raw/promo_ft',
        'data/raw/sell_in',
        'data/results/outputs',
        'data/results/reports/check_loss',
        'data/tmp/check_loss',
        'data/tmp/si_mad_recovered',
        'logs',
        f'data/tmp/si_recovered_{config.suffix}'
    ]
    for path in paths:
        path = f'{config.path}/{path}'
        if not os.path.exists(path):
            os.makedirs(path)


def expand_df_period(df: pd.DataFrame,
                     start: str = 'start',
                     end: str = 'end',
                     added_features: bool = False,
                     save_start_end_col: bool = False) -> pd.DataFrame:
    '''
    The function creates a flat dataframe from the passed dataframe.
    Each row in the dataframe will be expanded to the range of dates(by day).
    The output dataframe doesn't contain columns that passed
    in start and end arguments.

    Limitation: Space in the column name does not allow. ValueError will be
    raised in this case.

    :param added_features: added promo_day and promo_length
    :param save_start_end_col: keep start, end columns in df
    :param df: pd.DataFrame
    :param start: column that contains start of the period
    :param end:  column that contains end of the period
    :return: pd.DataFrame
    '''
    # TODO: add option change name for col 'date'

    tmp = defaultdict(list)
    if save_start_end_col:
        columns = [col for col in df.columns]
    else:
        columns = [
            col for col in df.columns if col not in [start, end]]

    cols_with_space = [col for col in columns if ' ' in col]
    if cols_with_space:
        logging.info(
            'Promo FT | '
            'Using columns that contains space in the name impossible.')
        raise ValueError(
            f'Using columns that contains space in the name impossible.'
            f' Columns to rename: {cols_with_space}')

    for row in df.itertuples(index=False):
        dates = pd.date_range(start=getattr(row, start),
                              end=getattr(row, end))
        tmp['date'].extend(dates)
        length = len(dates)
        for col in columns:
            tmp[col].extend([getattr(row, col)] * length)
        if added_features:
            tmp['promo_length'].extend([length] * length)
            tmp['promo_day'].extend(np.arange(1, length + 1))

    return pd.DataFrame.from_dict(tmp)


def open_rfa_mapping(rfa_mapping_path: str =
                     'data/masterdata/Customer KPI SFA Demand.xlsx'
                     ) -> pd.DataFrame:
    rfa_mapping = pd.read_excel(rfa_mapping_path)[
        ['Segmentation', 'RFA']]
    rfa_mapping.rename(
        columns={'Segmentation': 'client', 'RFA': 'rfa'}, inplace=True)
    rfa_mapping['rfa'] = [x[-1]
                          for x in rfa_mapping['rfa'].str.split(' ').tolist()]
    rfa_mapping['rfa'] = rfa_mapping['rfa'].astype(int)
    return rfa_mapping


def get_categories(data: pd.DataFrame,
                   categorical_columns: list) -> dict:
    categories = dict()
    for col in categorical_columns:
        categories[col] = pd.api.types. \
            CategoricalDtype(categories=set(data[col]))
    return categories


def concat_categorical(dfs, *args, **kwargs) -> pd.DataFrame:
    """
    Concatenate while preserving categorical columns.
    """
    # Iterate on categorical columns common to all dfs
    for col in set.intersection(
            *[
                set(df.select_dtypes(include='category').columns)
                for df in dfs
            ]
    ):
        # Generate the union category across dfs for this column
        uc = union_categoricals([df[col] for df in dfs])
        # Change to union category for all dataframes
        for df in dfs:
            df[col] = pd.Categorical(df[col].values, categories=uc.categories)
    return pd.concat(dfs, *args, **kwargs)


def mark_cut_history(data: pd.DataFrame,
                     date_column: str,
                     start_fc_date: Union[pd.Timestamp, str],
                     cut_history_months: int) -> pd.DataFrame:
    """
    Leaves N months of history in the dataset
    """
    data['unused_history'] = \
        np.where(data[date_column] >=
                 pd.to_datetime(start_fc_date) -
                 pd.Timedelta(days=30 * cut_history_months),
                 False, True)
    return data


def astype_cols(df):
    for col in df.columns.intersection(config.SF_TYPE.keys()):
        df[col] = df[col].astype(config.SF_TYPE[col]).astype('category')


def get_last_fc_date(horizon: str) -> pd.Timestamp:
    """
    Determines the latest forecast date by horizon type

    :param horizon: type of horizon
    :return: last forecast date
    """
    last_date = config.LAST_SELL_IN_DATE
    if horizon == 'st':
        last_date = config.FIRST_FORECAST_DATE + pd.Timedelta(weeks=6)
    elif horizon == 'mt':
        last_date = config.FIRST_FORECAST_DATE + pd.Timedelta(days=3.5 * 31)
    elif horizon == 'lt':
        last_date = config.FIRST_FORECAST_DATE + pd.Timedelta(days=19 * 31)
    return last_date
