import traceback
import logging
import pyspark.sql.functions as sf  # type: ignore
from pyspark.sql import SparkSession  # type: ignore
from adf.errors import BuildingBlocksError
from adf.modelling.tools import get_snowflake_df_spark  # type: ignore
from adf.utils.credentials import get_dbutils
from adf.services.database.query import update_report
from adf.modelling.dataprep.utils.core import build_building_blocks_report
from datetime import date
from dateutil.relativedelta import relativedelta  # type: ignore
from adf.modelling.dataprep.spain.dairy.plc import get_core_plc
from adf.config import SNOWFLAKE_DATABASE, SPAIN_PRIMARY_WAREHOUSES, MOUNT, FOLDER_CHECKPOINTS
from adf.utils.mappings import get_mappings, save_mappings
from adf.modelling.dataprep.spain.dairy.update import null_to_zero

uplift_cols = ["crm_uplift", "mc_uplift", "tpr_uplift"]


log = logging.getLogger("adf")
spark = SparkSession.builder.getOrCreate()


def build_bb(promo_df, compute_market_shares, **context):
    session = context["session"]
    pipeline_id = context["pipeline_id"]
    dataprep_id = context["dataprep_id"]

    checkpoint_dir = f"{MOUNT}/{FOLDER_CHECKPOINTS}/spain/dairy/building_blocks"
    spark.sparkContext.setCheckpointDir(checkpoint_dir)

    bb_report = build_building_blocks_report(
        session,
        "spain",
        "dairy",
        scope="promo_building_block",
        dataprep_id=dataprep_id,
        pipeline_id=pipeline_id,
    )

    try:
        promo_df = promo_df.repartition(1000, ["mat_cod", "cus_cod"])

        promo_df = scope_promo(promo_df)

        promo_df = merge_franchises(promo_df)

        promo_df = add_promo_descriptions(promo_df)

        promo_df = cast_and_sort_promo(promo_df)

        if compute_market_shares:
            ratios = compute_ratios(promo_df).persist()
            save_mappings(ratios, "spain", "dairy", "market_share_ratios")
        else:
            ratios = get_mappings("spain", "dairy", "market_share_ratios", "spark").persist()

        promo_df = promo_df.checkpoint()

        uplift_df = add_uplift_per_franchise(promo_df)

        uplift_df = add_market_shares(uplift_df, ratios)

        uplift_df = add_total_uplift_increment(uplift_df)

        uplift_df = normalize_uplifts(uplift_df)

        uplift_df = aggregate_uplifts(uplift_df)

        uplift_df = add_reno_uplifts(uplift_df)

        bb_report.upload(uplift_df, partition_on="mat_cod")
        update_report(session, bb_report, "success")

        get_dbutils(spark).fs.rm(checkpoint_dir, recurse=True)

    except Exception as err:
        traceback.print_exc()
        message = str(err.__class__.__name__) + ": " + str(err)
        update_report(session, bb_report, "failed", message)
        raise BuildingBlocksError(message)


def cast_and_sort_promo(promo_df):
    cols_to_cast = [
        "mat_cod",
        "cus_cod",
        "warehouse_cod",
        "promotion_mechanics_cod",
        "adjusted_units",
        "franchise_cod",
    ]
    for column in cols_to_cast:
        promo_df = promo_df.withColumn(column, sf.col(column).cast("int"))
    return promo_df.orderBy(["to_dt", "mat_cod", "warehouse_cod"], ascending=[1, 1, 1])


def scope_promo(promo_df):
    today = date.today()
    start_date = (today + relativedelta(months=-12)).strftime("%Y-%m-%d")
    end_date = today.strftime("%Y-%m-%d")
    return promo_df.where(sf.col("to_dt").between(start_date, end_date))
    # return promo_df.where(sf.col("to_dt") <= end_date)


def merge_franchises(promo_df):
    customer_df = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_SPN", "R_MAN_EDP_SPN_CUS_SAL", "spark"
    )

    customer_df = customer_df.select("cus_cod", "franchise_cod").drop_duplicates()

    promo_df = promo_df.join(customer_df, on=["cus_cod"], how="left")

    promo_df = promo_df.where(
        (sf.col("franchise_cod") != "####") & (sf.col("franchise_cod").isNotNull())
    )
    return promo_df


def add_promo_descriptions(promo_df):
    promo_mapping = get_mappings("spain", "dairy", "promo_types", object_type="spark").persist()
    promo_df = promo_df.join(promo_mapping, on="promotion_mechanics_cod", how="left")
    return promo_df


def compute_ratios(promo_df):
    ratios = promo_df.where(
        (sf.col("promotion_mechanics_cod") == 0) &
        (sf.col("warehouse_cod").isin(SPAIN_PRIMARY_WAREHOUSES))
    )

    ratios = ratios.groupBy("warehouse_cod", "franchise_cod").agg(
        sf.sum("adjusted_units").alias("volume_franchise_primary")
    )

    volume_primaries = promo_df.groupBy("warehouse_cod").agg(
        sf.sum("adjusted_units").alias("volume_primary")
    )

    ratios = (
        ratios.join(volume_primaries, on="warehouse_cod", how="left")
        .withColumn("ratio", sf.col("volume_franchise_primary") / sf.col("volume_primary"))
        .select("franchise_cod", "warehouse_cod", "ratio")
        .drop_duplicates()
    )

    return ratios


def add_uplift_per_franchise(promo_df):
    log.info("Creating uplifts...")
    promo_df = create_uplift_table(promo_df)
    log.info("Regularize volumes...")
    promo_df = regularize_volumes(promo_df)
    log.info("Add annual means...")
    promo_df = add_annual_means(promo_df)
    log.info("Compute uplifts...")
    promo_df = compute_uplifts(promo_df)
    return promo_df


def create_uplift_table(df_franchise):
    uplift_df = (
        df_franchise.groupBy("franchise_cod", "to_dt", "warehouse_cod", "mat_cod")
        .pivot("promotion_type_desc")
        .agg(sf.sum("adjusted_units").alias("adjusted_units"))
    )

    cols_to_transform = {"0": "no_promo", "Multicompra": "mc", "CRM": "crm", "TPR": "tpr"}

    for old_name, new_name in cols_to_transform.items():
        if old_name in uplift_df.columns:
            uplift_df = uplift_df.withColumnRenamed(old_name, new_name)
            uplift_df = null_to_zero(uplift_df, new_name)
        else:
            uplift_df = uplift_df.withColumn(new_name, sf.lit(0))

    metrics_to_compute = [sf.sum(column).alias(column) for column in cols_to_transform.values()]

    uplift_df = uplift_df.groupBy("franchise_cod", "warehouse_cod", "to_dt", "mat_cod").agg(
        *metrics_to_compute
    )

    return uplift_df


def regularize_volumes(uplift_df):
    for column in ("mc", "crm", "tpr"):
        uplift_df = uplift_df.withColumn(
            column,
            sf.when(
                sf.col(column) > sf.col("no_promo"), sf.col(column) + sf.col("no_promo")
            ).otherwise(sf.col(column)),
        )

        uplift_df = uplift_df.withColumn(
            "no_promo",
            sf.when(sf.col(column) > sf.col("no_promo"), 0)
            .when(sf.col(column) < sf.col("no_promo"), sf.col(column) + sf.col("no_promo"))
            .otherwise(sf.col("no_promo")),
        )

        uplift_df = uplift_df.withColumn(
            column, sf.when(sf.col(column) < sf.col("no_promo"), 0).otherwise(sf.col(column))
        )

    return uplift_df


def add_annual_means(uplift_df):
    group_list = ["mat_cod", "warehouse_cod", "franchise_cod"]
    for column in ("crm", "mc", "tpr", "no_promo"):
        mean_df = uplift_df.where(sf.col(column) > 0)
        mean_df = mean_df.groupBy(group_list).agg(sf.mean(column).alias(f"{column}_annual_mean"))
        uplift_df = uplift_df.join(mean_df, on=group_list, how="left")
        uplift_df = null_to_zero(uplift_df, f"{column}_annual_mean")

    return uplift_df


def compute_uplifts(uplift_df):
    uplift_df = (
        uplift_df.withColumn(
            "crm_uplift", sf.col("crm_annual_mean") / sf.col("no_promo_annual_mean")
        )
        .withColumn("mc_uplift", sf.col("mc_annual_mean") / sf.col("no_promo_annual_mean"))
        .withColumn("tpr_uplift", sf.col("tpr_annual_mean") / sf.col("no_promo_annual_mean"))
    )

    for column in uplift_cols:
        uplift_df = uplift_df.withColumn(column, sf.col(column) - 1)

    uplift_df = (
        uplift_df.withColumn(
            "crm_uplift", sf.when(sf.col("crm_uplift") < 0, 0).otherwise(sf.col("crm_uplift"))
        )
        .withColumn("mc_uplift", sf.when(sf.col("mc_uplift") < 0, 0).otherwise(sf.col("mc_uplift")))
        .withColumn(
            "tpr_uplift", sf.when(sf.col("tpr_uplift") < 0, 0).otherwise(sf.col("tpr_uplift"))
        )
    )

    uplift_df = uplift_df.orderBy(["warehouse_cod", "to_dt", "mat_cod"])
    return uplift_df


def add_market_shares(uplift_df, ratios):
    log.info("Add market shares...")
    uplift_df = uplift_df.join(ratios, on=["franchise_cod", "warehouse_cod"], how="left")
    for column in uplift_cols:
        uplift_df = uplift_df.withColumn(column, sf.col(column) * sf.col("ratio"))

    return uplift_df.drop("ratio")


def add_total_uplift_increment(uplift_df):
    group_list = ["warehouse_cod", "to_dt", "mat_cod"]
    for column in uplift_cols:
        increment_df = uplift_df.groupBy(group_list).agg(
            sf.sum(column).alias(f"{column}_total_increment")
        )

        uplift_df = uplift_df.join(increment_df, on=group_list, how="left")
        uplift_df = null_to_zero(uplift_df, f"{column}_total_increment")
    return uplift_df


def normalize_uplifts(uplift_df):
    for column in uplift_cols:
        uplift_df = uplift_df.withColumn(
            column,
            sf.col(column) / (
                1 + (
                    (
                        sf.col("crm_uplift_total_increment") +
                        sf.col("mc_uplift_total_increment") +
                        sf.col("tpr_uplift_total_increment")
                    ) / 3
                )
            )
        )
    return uplift_df


def aggregate_uplifts(uplift_df):
    metrics = [sf.mean(col).alias(col) for col in uplift_cols]

    uplift_df = uplift_df.groupBy("warehouse_cod", "mat_cod", "franchise_cod").agg(*metrics)
    return uplift_df


def add_reno_uplifts(uplift_df):
    scope = get_core_plc()
    scope_reno = (
        scope.where(sf.col("phasein_start_date").isNotNull())
        .select(sf.col("mat_cod").alias("product"), sf.col("mat_cod_reference").alias("mat_cod"))
        .drop_duplicates()
        .where(sf.col("product") != sf.col("mat_cod"))
    )
    reno_uplift = (
        uplift_df.join(scope_reno, on="mat_cod", how="inner")
        .withColumn("mat_cod", sf.col("product"))
        .drop("product")
    )
    uplift_df = uplift_df.unionByName(reno_uplift).drop_duplicates()

    return uplift_df
