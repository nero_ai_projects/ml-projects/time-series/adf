import pyspark.sql.functions as sf  # type: ignore
import pyspark.sql.types as st  # type: ignore
from pyspark.sql.window import Window  # type: ignore
import logging

log = logging.getLogger("adf")

TARGET = "adjusted_units"
RAMPUP_DAYS = 150
LAUNCH_WINDOWS = {
    1: "V1",
    2: "V1",
    3: "V1",
    4: "V2",
    5: "V2",
    6: "V2",
    7: "V2",
    8: "V3",
    9: "V3",
    10: "V3",
    11: "V3",
    12: "V1",
}
FEATURES_FOR_ATTRIBUTES_DATA = [
    "mat_cod",
    "warehouse_cod",
    "to_dt",
    "mat_umbrella_cod",
    "mat_prodbrand_cod",
    "mat_subfamily_cod",
    "mat_length_value",
    "mat_width_value",
    "mat_height_value",
    "total_shelf_life",
    "mat_weight",
    "mat_units",
    "product_type_cod",
    "mat_volume",
    "service_level",
    "region",
    "is_holiday",
    "warehouse_opening",
    "calculated_launch_window",
]

# TODO P2 : read core dataprep reports instead of files on S3
# TODO P2 : replace the hardcoded product_level and location_level
#            with the ones from the dataprep reports


def build_panel_data(df):
    log.info("Building inno panel data for unique products...")
    panel_data = filter_product_with_no_orders(df)

    panel_data = parse_types_features(panel_data)

    panel_data = get_launch_dates_and_launch_windows(panel_data)

    panel_data = get_cum_sum_orders_and_keep_attributes(panel_data)

    return panel_data


def filter_product_with_no_orders(panel_data):
    mat_cods_to_keep = (
        panel_data.groupBy("mat_cod")
        .agg(sf.sum(TARGET).alias(TARGET))
        .where(sf.col(TARGET) != 0)
        .select("mat_cod")
        .distinct()
    )
    panel_data = panel_data.join(mat_cods_to_keep, on="mat_cod", how="inner")
    return panel_data


def parse_types_features(panel_data):
    panel_data = panel_data.withColumn("to_dt", sf.to_date("to_dt"))
    l_int_features = [
        "mat_cod",
        "warehouse_cod",
        "mat_length_value",
        "mat_width_value",
        "mat_height_value",
        "total_shelf_life",
        "mat_umbrella_cod",
        "mat_prodbrand_cod",
        "is_holiday",
        "warehouse_opening",
    ]
    for column in l_int_features:
        panel_data = panel_data.withColumn(column, sf.col(column).cast(st.IntegerType()))

    return panel_data


@sf.udf(st.StringType())
def get_launch_window(x):
    return LAUNCH_WINDOWS[x]


def get_launch_dates_and_launch_windows(panel_data):
    df_calculated_launch_date = (
        panel_data.where(sf.col(TARGET) != 0)
        .select("mat_cod", "to_dt")
        .groupBy("mat_cod")
        .agg(sf.min("to_dt").alias("calculated_launch_date"))
    )

    panel_data = (
        panel_data.join(df_calculated_launch_date, on="mat_cod", how="left")
        .where(sf.col("to_dt") > sf.col("calculated_launch_date"))
        .withColumn("end_rampup_date", sf.date_add(sf.col("calculated_launch_date"), RAMPUP_DAYS))
        .withColumn(
            "days_since_launch", sf.datediff(sf.col("to_dt"), sf.col("calculated_launch_date"))
        )
        .withColumn(
            "calculated_launch_window", get_launch_window(sf.month("calculated_launch_date"))
        )
    )

    return panel_data


def calculating_cum_sum_orders(df_input):
    l_columns = ["mat_cod", "warehouse_cod", "to_dt", "days_since_launch"]
    df_input_cumsum = (
        df_input.select(l_columns + [TARGET]).groupBy(l_columns).agg(sf.sum(TARGET).alias(TARGET))
    )

    df_right = df_input.select(
        "mat_cod", "calculated_launch_date", "end_rampup_date"
    ).drop_duplicates()

    df_input_cumsum = df_input_cumsum.join(df_right, on="mat_cod", how="left")

    window = (
        Window.partitionBy("mat_cod", "warehouse_cod")
        .orderBy("to_dt")
        .rangeBetween(Window.unboundedPreceding, 0)
    )

    df_input_cumsum = df_input_cumsum.withColumn("cum_sum_" + TARGET, sf.sum(TARGET).over(window))

    l_columns = [
        "mat_cod",
        "warehouse_cod",
        "calculated_launch_date",
        "end_rampup_date",
        "to_dt",
        "days_since_launch",
        TARGET,
        "cum_sum_" + TARGET,
    ]
    df_input_cumsum = df_input_cumsum[l_columns]
    return df_input_cumsum


def get_cum_sum_orders_and_keep_attributes(panel_data):
    panel_data_rampup_phase_ts = calculating_cum_sum_orders(panel_data)
    panel_data_rampup_phase_attributes = panel_data.select(
        FEATURES_FOR_ATTRIBUTES_DATA
    ).drop_duplicates()

    result = panel_data_rampup_phase_ts.join(
        panel_data_rampup_phase_attributes, on=["mat_cod", "warehouse_cod", "to_dt"], how="left"
    )
    return result
