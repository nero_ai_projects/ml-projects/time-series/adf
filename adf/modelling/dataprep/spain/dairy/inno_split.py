import pyspark.sql.functions as sf  # type: ignore
import pyspark.sql.types as st  # type: ignore
from adf.utils.mappings import get_mappings, save_mappings
from adf.modelling.tools import get_snowflake_df_spark
from adf.config import SNOWFLAKE_DATABASE
import logging

log = logging.getLogger("adf")


def apply_innovation_split(df):
    log.info("Applying IBP Master Data rules")
    core_scope, inno_scope, ibp_mat_cods = implement_ibp_master_data_rules()
    log.info("Update innovation tables")
    update_innovation_tables(inno_scope)
    log.info("Filter table in two: core and innovation")
    core_df, inno_df = filter_scopes(df, core_scope, inno_scope)
    return core_df, inno_df


def implement_ibp_master_data_rules():
    master_ibp = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_SPN", "R_IBP_EDP_SPN_MAT_STS", "spark"
    )

    ibp_mat_cods = master_ibp.select("mat_cod").distinct().collect()
    ibp_mat_cods = [e["mat_cod"] for e in ibp_mat_cods]

    master_ibp = master_ibp.where(~sf.col("mat_cod").contains("[A-Za-z]")).withColumn(
        "mat_cod", sf.col("mat_cod").cast(st.IntegerType())
    )

    plc = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_SPN", "R_IBP_EDP_SPN_PRO_LFC_REF_PRO", "spark"
    )

    master_ibp = master_ibp.join(plc, on="mat_cod", how="left")

    scope = master_ibp.where(
        (
            sf.col("product_activation_cod") != "Z3"
        ) & (
            sf.col("product_aggregation") != "STOP ALGORITHM"
        )
    )
    inno_scope = scope.where(
        (sf.col("innovation_status") == "GPS") & (sf.col("is_active") == "true")
    )
    core_scope = scope.where(sf.col("innovation_status").isin(["CORE_LT", "CORE_ST"]))

    return core_scope, inno_scope, ibp_mat_cods


def filter_scopes(df, core_scope, inno_scope):
    mat_cod_scope = core_scope.select("mat_cod").distinct()

    core_df = df.join(mat_cod_scope, on="mat_cod", how="inner")

    old_innovations = get_mappings("spain", "dairy", "old_innovations", object_type="spark")

    inno_skus = old_innovations.select(sf.col("PRODUCT").alias("mat_cod")).distinct()

    inno_scope_skus = inno_scope.select("mat_cod").distinct()

    inno_skus = inno_skus.unionByName(inno_scope_skus).drop_duplicates()

    inno_df = df.join(inno_skus, on="mat_cod", how="inner")
    return core_df, inno_df


def update_innovation_tables(inno_scope):
    old_innovations = get_mappings("spain", "dairy", "old_innovations", object_type="spark")
    current_innovations = get_mappings("spain", "dairy", "current_innovations", object_type="spark")

    real_current_inno_skus = inno_scope.select("mat_cod").distinct()

    old_inno_newbies = (
        current_innovations.select(sf.col("PRODUCT").alias("mat_cod"))
        .join(real_current_inno_skus, on="mat_cod", how="left_anti")
        .withColumnRenamed("mat_cod", "PRODUCT")
    )

    old_innovations = old_innovations.unionByName(old_inno_newbies).drop_duplicates()

    current_innovations = real_current_inno_skus.withColumnRenamed("mat_cod", "PRODUCT")

    save_mappings(old_innovations, "spain", "dairy", "old_innovations")
    save_mappings(current_innovations, "spain", "dairy", "current_innovations")


# def merge_sellout(inno_df):
#     sellout_df = get_dask_df("spain", "dairy", "sellout_hist")
#     cus_prod_loc = get_mappings("spain", "dairy", "customer_product_location_mapping")

#     sellout_df = sellout_df.merge(cus_prod_loc, how="left", on=["cus_cod", "mat_cod"])
#     sellout_df = sellout_df.groupby(
#         ["mat_cod", "to_dt", "warehouse_cod"]).agg({"sellout": "sum"}).reset_index().compute()
#     inno_df = inno_df.merge(sellout_df, how="left", on=["mat_cod", "to_dt", "warehouse_cod"])
#     inno_df = inno_df.astype({"mat_cod": "int32", "warehouse_cod": "int32", "cus_cod": "int32"})
#     return inno_df
