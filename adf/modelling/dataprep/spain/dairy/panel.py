import logging
from datetime import timedelta
from functools import partial
import pandas as pd  # type: ignore
from pyspark.sql import SparkSession  # type: ignore
import pyspark.sql.functions as sf  # type: ignore
import pyspark.sql.types as st  # type: ignore
from pyspark.sql.window import Window  # type: ignore
from adf.errors import DataprepError
from sqlalchemy import desc  # type: ignore
from adf.modelling import mllogs
from adf.modelling.dataprep.utils.core import (
    build_dataprep_report,
    build_innovation_dataprep_report,
)
from adf.modelling.tools import get_snowflake_df_spark
from adf.utils.credentials import get_dbutils
from adf.config import SNOWFLAKE_DATABASE, MOUNT, FOLDER_CHECKPOINTS
from adf.modelling.dataprep.spain.dairy.update import update_orders_primaries
from adf.modelling.dataprep.spain.dairy.plc import apply_plc_rules
from adf.modelling.dataprep.spain.dairy.inno_split import apply_innovation_split
from adf.modelling.dataprep.spain.dairy.update import null_to_zero, keep_last
import adf.modelling.dataprep.spain.dairy.inno_panel as inno_prep
from adf.services.database.query.reports import update_report
from adf.utils.provider import Provider
from adf.services.database import get_session, PromoDataprepReports, BusinessUnits


log = logging.getLogger("adf")
Provide = partial(Provider, mlrun=False, session=True, cluster=True)

qty_col_list = ["adjusted_units", "assigned_units"]


def run(country: str, division: str, product_level: str, location_level: str, **context):
    """Build panel data for Spain dairy given product_level and location_level
    Done for core and inno model both

    :product_level: sku
    :location_level: warehouse
    """
    assert product_level in ("sku",)
    assert location_level in ("warehouse",)
    spark = SparkSession.builder.getOrCreate()
    checkpoint_dir = f"{MOUNT}/{FOLDER_CHECKPOINTS}/{country}/{division}/build_panel"
    spark.sparkContext.setCheckpointDir(checkpoint_dir)

    session = context["session"]
    pipeline_id = context.get("pipeline_id", None)

    report = build_dataprep_report(
        session,
        country,
        division,
        product_level,
        location_level,
        scope="panel",
        split_type="",
        split_code=None,
        prep_type="core",
        pipeline_id=pipeline_id,
    )
    inno_report = build_innovation_dataprep_report(
        session,
        country,
        division,
        product_level,
        location_level,
        scope="inno",
        split_type="",
        split_code=None,
        pipeline_id=pipeline_id,
    )
    mllogs.log_dataprep_report(report)
    mllogs.log_dataprep_report(inno_report)

    try:
        log.info("Building panel")
        core_panel_df, inno_panel_df = build_panel_data()
        core_panel_df, inno_panel_df = apply_covid_rules(core_panel_df, inno_panel_df)
        mllogs.log_shape(core_panel_df)
        mllogs.log_shape(inno_panel_df)

        log.info("Saving")
        report.upload(core_panel_df)
        inno_report.upload(inno_panel_df)
        update_report(session, report, "success")
        update_report(session, inno_report, "success")
        mllogs.tag_report_output(report)
        mllogs.tag_report_output(inno_report)

        get_dbutils(spark).fs.rm(checkpoint_dir, recurse=True)

        return report, inno_report

    except Exception as err:
        message = str(err.__class__.__name__) + ": " + str(err)
        update_report(session, report, "failed", message)
        update_report(session, inno_report, "failed", message)
        raise DataprepError(message)

    finally:
        mllogs.tag_report_output(report)
        mllogs.tag_report_output(inno_report)


def apply_covid_rules(core_panel_df, inno_panel_df):
    start_date = "2020-03-09"
    end_date = "2020-04-13"

    core_panel_df = core_panel_df.withColumn(
        "is_covid",
        sf.when((sf.col("to_dt") >= start_date) & (sf.col("to_dt") < end_date), 1).otherwise(0),
    )

    inno_panel_df = inno_prep.build_panel_data(inno_panel_df)

    inno_panel_df = inno_panel_df.withColumn(
        "is_covid",
        sf.when((sf.col("to_dt") >= start_date) & (sf.col("to_dt") < end_date), 1).otherwise(0),
    )
    return core_panel_df, inno_panel_df


def build_panel_data():
    core_data = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_SPN", "F_TWI_EDP_SPN_SAL_DOC", "spark"
    )

    log.info("Drop null orders...")
    core_data = drop_null_orders(core_data)
    # core_data = drop_primary_whs_as_customer(core_data)
    core_data = apply_plc_rules(core_data)
    core_data = update_orders_primaries(core_data)

    core_data = clean_orders(core_data)
    core_data = core_data.checkpoint()
    core_data = finalize_cleaning(core_data)
    core_data = core_data.checkpoint()
    core_data = merge_products(core_data)
    core_data = add_service_level(core_data)
    core_data = add_holidays(core_data)
    core_data = add_warehouse_calendar(core_data)
    core_data = core_data.checkpoint()
    core_data = add_cannibalization_features(core_data)
    core_data = merge_promotions(core_data)
    core_data = finalize_dataprep(core_data)
    core_data, inno_data = apply_innovation_split(core_data)

    return core_data, inno_data


def drop_null_orders(orders_df):
    orders_df = orders_df.where(
        ~((sf.col("adjusted_units") == 0) & (sf.col("assigned_units") == 0))
    )
    orders_df = orders_df.drop("delivered_units")
    orders_df = orders_df.where(
        sf.col("adjusted_units").isNotNull() | sf.col("assigned_units").isNotNull()
    )
    return orders_df


def merge_promotions(orders_df):
    session = get_session()
    business_unit = (
        session.query(BusinessUnits)
        .filter(BusinessUnits.country == "spain")
        .filter(BusinessUnits.division == "dairy")
        .first()
    )
    promo_dataprep = (
        session.query(PromoDataprepReports)
        .filter(PromoDataprepReports.business_unit_id == business_unit.id)
        .filter(PromoDataprepReports.product_level == "sku")
        .filter(PromoDataprepReports.location_level == "warehouse")
        .filter(PromoDataprepReports.prep_type == "promotion")
        .filter(PromoDataprepReports.scope == "finalized_promo")
        .filter(PromoDataprepReports.status == "success")
        .filter(PromoDataprepReports.updated_at.isnot(None))
        .order_by(desc(PromoDataprepReports.updated_at))
        .first()
    )
    retailers_dataprep = (
        session.query(PromoDataprepReports)
        .filter(PromoDataprepReports.business_unit_id == business_unit.id)
        .filter(PromoDataprepReports.product_level == "sku")
        .filter(PromoDataprepReports.location_level == "warehouse")
        .filter(PromoDataprepReports.prep_type == "promotion")
        .filter(PromoDataprepReports.scope == "retailers_promo")
        .filter(PromoDataprepReports.status == "success")
        .filter(PromoDataprepReports.updated_at.isnot(None))
        .order_by(desc(PromoDataprepReports.updated_at))
        .first()
    )
    uplifts_dataprep = (
        session.query(PromoDataprepReports)
        .filter(PromoDataprepReports.business_unit_id == business_unit.id)
        .filter(PromoDataprepReports.product_level == "sku")
        .filter(PromoDataprepReports.location_level == "warehouse")
        .filter(PromoDataprepReports.prep_type == "promotion")
        .filter(PromoDataprepReports.scope == "uplifts_promo")
        .filter(PromoDataprepReports.status == "success")
        .filter(PromoDataprepReports.updated_at.isnot(None))
        .order_by(desc(PromoDataprepReports.updated_at))
        .first()
    )

    promo_df = promo_dataprep.download(object_type="spark")
    retailers_df = retailers_dataprep.download(object_type="spark")
    uplifts_df = uplifts_dataprep.download(object_type="spark")

    join_keys = ["mat_cod", "warehouse_cod", "to_dt"]
    how = "left"

    orders_df = orders_df.join(retailers_df, on=join_keys, how=how)

    orders_df = orders_df.join(uplifts_df, on=join_keys, how=how)

    result_df = orders_df.join(
        promo_df,
        (
            orders_df.mat_cod == promo_df.product_index
        ) & (
            orders_df.warehouse_cod == promo_df.location_index
        ) & (
            orders_df.to_dt == promo_df.time_index
        ),
        how=how,
    )

    to_drop_cols = ["product_index", "location_index", "time_index"]

    result_df = result_df.drop(*to_drop_cols)

    null_cols = [col for col in promo_df.columns if col not in to_drop_cols]

    for column in null_cols:
        result_df = null_to_zero(result_df, column)
    return result_df


def clean_orders(orders_df):
    log.info("* Cleaning orders")
    aggregation = [sf.sum(column).alias(column) for column in qty_col_list]
    orders_df = orders_df.groupBy("mat_cod", "warehouse_cod", "to_dt").agg(*aggregation)
    log.info("* Done cleaning orders")
    return orders_df


# def merge_sellout(orders_df):
#     #TODO WHEN SELLOUT IS READY
#     log.info("* Merging with sellout")
#     sellout_df = get_dask_df("spain", "dairy", "sellout_hist")\
#         .rename(columns={"generation_date": "to_dt", "cos_cus": "cus_cod"})
#     mapping_df = get_mappings("spain", "dairy", "?")  # TODO : find which mapping to use

#     sellout_df = sellout_df.merge(mapping_df, how="left", on=["cus_cod", "mat_cod"])
#     sellout_df = sellout_df.groupby(
#         ['mat_cod', 'to_dt', 'warehouse_cod'])['sellout'].sum().reset_index()
#     orders_df = dd.from_pandas(orders_df, npartitions=1)
#     orders_df = orders_df.merge(sellout_df, how="left", on=["mat_cod", "to_dt", "warehouse_cod"])
#     return orders_df


def finalize_cleaning(orders_df):
    log.info("* Cleaning orders")
    orders_df = expand_grid(orders_df)

    orders_df = resample_grid(orders_df)

    return orders_df


def merge_products(orders_df):
    log.info("* Merging with product hierarchy")

    products_df = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_SPN", "R_MAN_EDP_SPN_MAT_SAL", "spark"
    )
    cols_to_drop = ("flavour_cod", "mat_active_flag", "active")

    products_df = products_df.drop(*cols_to_drop)

    products_df = products_df.drop_duplicates()

    products_df = products_df.withColumn(
        "mat_volume",
        (
            sf.col("mat_length_value") * sf.col("mat_width_value") * sf.col("mat_height_value")
        ) / 1000,
    )

    products_df = products_df.withColumn("mat_weight", sf.col("mat_weight") * 1000)

    orders_df = orders_df.join(products_df, how="left", on="mat_cod")
    return orders_df


def add_service_level(orders_df):
    log.info("* Adding service level")
    orders_df = orders_df.withColumn(
        "service_level", sf.col("adjusted_units") - sf.col("assigned_units")
    )
    return orders_df


def resample_grid(orders_df):
    """
    Resamples data to create rows for missing dates
    """
    log.info("Resample grid...")

    groupby_columns = list(set(orders_df.columns) - set(qty_col_list))

    today = orders_df.agg(sf.max("to_dt").alias("today")).collect()[0]["today"]

    first_day = orders_df.agg(sf.min("to_dt").alias("first_day")).collect()[0]["first_day"]

    prior_date_range = list(pd.date_range(first_day, today))

    resample_group_list = ["mat_cod", "warehouse_cod"]

    prior_date_range_df = (
        orders_df.select(resample_group_list)
        .drop_duplicates()
        .withColumn("prior_date_range", sf.array([sf.lit(date) for date in prior_date_range]))
    )

    dates_available = orders_df.groupBy(resample_group_list).agg(
        sf.collect_set("to_dt").alias("available_dates")
    )

    log.info("Create df to append...")

    list_to_drop = ["prior_date_range", "available_dates", "to_dt"]

    df_to_append = (
        orders_df.join(dates_available, on=resample_group_list, how="left")
        .join(prior_date_range_df, on=resample_group_list, how="left")
        .withColumn("dates_to_add", sf.array_except("prior_date_range", "available_dates"))
        .drop(*list_to_drop)
    )

    for column in df_to_append.columns:
        if column not in ["dates_to_add", "mat_cod", "warehouse_cod"]:
            df_to_append = df_to_append.withColumn(column, sf.lit(0))

    df_to_append = df_to_append.select("*", sf.explode_outer("dates_to_add").alias("to_dt")).drop(
        "dates_to_add"
    )

    log.info("Set other columns to zero...")
    for column in df_to_append.columns:
        if column not in ["to_dt", "mat_cod", "warehouse_cod"]:
            df_to_append = df_to_append.withColumn(column, sf.lit(0))

    log.info("Enrich with missing dates...")
    orders_df = orders_df.unionByName(df_to_append)

    log.info("Resample...")

    metrics = [sf.sum(column).alias(column) for column in qty_col_list]

    orders_df = orders_df.groupBy(groupby_columns).agg(*metrics)

    log.info("Finalized cleaning...")
    return orders_df


def expand_grid(orders_df):
    """
    Creates horizon rows for coming forecasts
    """
    log.info("Expand grid...")
    today = orders_df.agg(sf.max("to_dt").alias("today")).collect()[0]["today"]

    future = add_future_weeks(orders_df, today)

    future = future.where(sf.col("to_dt") > today)

    orders_df = orders_df.unionByName(future)

    return orders_df


def add_future_weeks(df, today):
    log.info("Add future weeks...")
    date_range = list(pd.date_range(today, today + timedelta(days=7 * 15)))
    next_weeks = df.withColumn("to_dt_list", sf.array([sf.lit(date) for date in date_range])).drop(
        "to_dt"
    )
    next_weeks = next_weeks.select("*", sf.explode_outer("to_dt_list").alias("to_dt")).drop(
        "to_dt_list"
    )

    for column in next_weeks.columns:
        if column not in ["to_dt", "mat_cod", "warehouse_cod"]:
            next_weeks = next_weeks.withColumn(column, sf.lit(0))

    df = df.unionByName(next_weeks)

    return df


def prep_holidays(df):
    df = df.withColumn("date", sf.col("date").cast(st.DateType()))

    window = Window.partitionBy("region").orderBy("date")

    df = df.withColumn("eve", sf.lag("is_holiday").over(window))
    df = df.withColumn(
        "is_holiday",
        sf.when((sf.dayofweek("date") == 0) & (sf.col("eve") == 1), 1)
        .when((sf.dayofweek("date") == 6), 0)
        .otherwise(sf.col("is_holiday")),
    )

    df = df.select(sf.col("date").alias("to_dt"), "is_holiday", "region").drop_duplicates()

    return df


def add_holidays(df):
    df = df.withColumn("region", get_warehouse_region("warehouse_cod"))

    calendar_df = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_EXP_AFC", "CALENDAR_DATA", "spark"
    )

    calendar_df = calendar_df.where(sf.col("location").contains("spain"))
    calendar_df = calendar_df.withColumn("region", sf.trim(sf.split("location", "_").getItem(1)))

    window = Window.partitionBy("region", "date").orderBy(sf.col("date").desc())
    calendar_df = keep_last(calendar_df, window)
    calendar_df = prep_holidays(calendar_df)
    calendar_df = calendar_df.drop("index")

    df = df.join(calendar_df, how="left", on=["to_dt", "region"])

    df = df.withColumn("holiday_day_of_week", sf.dayofweek("to_dt")).withColumn(
        "holiday_day_of_week",
        sf.when(sf.col("is_holiday") == 0, -1).otherwise(sf.col("holiday_day_of_week")),
    )

    holidays_region_count = calendar_df.groupBy("to_dt").agg(
        sf.sum("is_holiday").alias("holidays_region_count")
    )

    max_regions_for_holiday = holidays_region_count.agg(
        sf.max("holidays_region_count").alias("holidays_region_count")
    ).collect()[0]["holidays_region_count"]

    df = df.join(holidays_region_count, on=["to_dt"], how="left")

    df = null_to_zero(df, "holidays_region_count")

    df = df.withColumn(
        "closed_holidays",
        sf.when(sf.col("holidays_region_count") == max_regions_for_holiday, 1).otherwise(0),
    )

    df = df.withColumn(
        "partly_closed_holidays",
        sf.when(
            (
                (
                    sf.col("holidays_region_count") < max_regions_for_holiday
                ) & (sf.col("holidays_region_count") > (max_regions_for_holiday // 4))
            ) | (
                (
                    sf.col("holidays_region_count") > 0
                ) & (
                    sf.col("holidays_region_count") <= (max_regions_for_holiday // 4)
                ) & (sf.col("is_holiday") == 1)
            ),
            1,
        ).otherwise(0),
    )

    return df


@sf.udf(st.StringType())
def get_warehouse_region(warehouse_cod):
    warehouse_locations = {170: "CT", 218: "MD", 171: "MD", 174: "VC", 175: "NC", 176: "AN"}
    return warehouse_locations.get(warehouse_cod)


def add_warehouse_calendar(df):
    warehouse_calendar_df = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_SPN", "R_MAN_EDP_SPN_CAL_WRH", "spark"
    )
    warehouse_calendar_df = (
        warehouse_calendar_df.where(sf.col("sales_organization_cod") == 36)
        .drop("sales_organization_cod")
        .withColumn("to_dt", sf.col("to_dt").cast(st.TimestampType()))
    )

    keys = ["to_dt", "warehouse_cod"]
    window = Window.partitionBy(keys).orderBy(sf.col("to_dt").desc())

    warehouse_calendar_df = keep_last(warehouse_calendar_df, window)

    df = df.join(warehouse_calendar_df, how="left", on=keys)
    return df


def remove_orders_on_warehouse_closure(df):
    for col in qty_col_list:
        df = df.withColumn(col, sf.when(sf.col("warehouse_opening") == 0, 0).otherwise(sf.col(col)))
    return df


def add_cannibalization_features(orders_df):
    log.info("Add cannibalization features...")
    gby_list = ["product_type_cod", "mat_subfamily_cod", "mat_prodbrand_cod", "mat_umbrella_cod"]
    for gby in gby_list:
        log.info("Add cannibalization feature for {}...".format(gby))
        for col in qty_col_list:
            orders_df = calculate_cannibalization(orders_df, gby, col, diff=True)

    for column in gby_list:
        orders_df = orders_df.withColumn(column, sf.col(column).cast(st.StringType()))
    return orders_df


def calculate_cannibalization(df, gby, col, diff=False):
    group_list = ["to_dt", "warehouse_cod", gby]
    name = f"{gby}_{col}_sum_{'with_diff' if diff else 'without_diff'}"

    gb = df.groupBy(group_list).agg(sf.sum(col).alias(name))

    df = df.join(gb, how="left", on=group_list)

    if diff:
        df = df.withColumn(name, sf.col(name) - sf.col(col))
    return df


def finalize_dataprep(df):
    log.info("Finalize Dataprep...")
    df = df.withColumn("adjusted_units_mean", sf.col("adjusted_units"))
    numeric_cols = [
        (f.name, f.dataType)
        for f in df.schema.fields
        if isinstance(
            f.dataType, st.IntegerType
        ) or isinstance(
            f.dataType, st.DoubleType
        ) or isinstance(
            f.dataType, st.FloatType
        )
    ]

    for column_name, column_type in numeric_cols:
        df = null_to_zero(df, column_name)
        df = df.withColumn(column_name, sf.col(column_name).cast(column_type))

    target_str_cols = ["product_desc", "probrand_desc", "family_desc"]

    for column in target_str_cols:
        df = df.withColumn(column, sf.col(column).cast(st.StringType()))

    decimal_cols = [f.name for f in df.schema.fields if isinstance(f.dataType, st.DecimalType)]

    for column in decimal_cols:
        df = null_to_zero(df, column)
        df = df.withColumn(column, sf.col(column).cast(st.FloatType()))

    return df
