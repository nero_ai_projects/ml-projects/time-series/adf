from datetime import datetime
import pyspark.sql.functions as sf  # type: ignore
import pyspark.sql.types as st  # type: ignore
from adf.utils.mappings import get_mappings, save_mappings
from adf.modelling.tools import get_snowflake_df_spark
from adf.config import SNOWFLAKE_DATABASE
import logging


log = logging.getLogger("adf")


def apply_plc_rules(df):
    log.info("Applying delisting and renovation from PLC")
    sku_replacement_mapping = get_mappings(
        "spain", "dairy", "sku_replacement_mapping", object_type="spark"
    ).persist()
    delistings_hist = get_mappings("spain", "dairy", "delistings", object_type="spark").persist()
    df, sku_replacement_mapping = generate_delistings_renovations(
        df, sku_replacement_mapping, delistings_hist
    )
    df = update_sku_replacements(df, sku_replacement_mapping)
    return df


def update_sku_replacements(core_df, sku_replacements):
    log.info("Update SKU Replacements...")
    sku_replacements = sku_replacements.drop_duplicates()
    mask = sf.col("sku_code_origin") != sf.col("sku_code_dest")
    sku_replacements = sku_replacements.where(mask)
    core_df = replace_skus(core_df, sku_replacements)
    save_mappings(sku_replacements, "spain", "dairy", "sku_replacement_mapping")
    return core_df


def replace_skus(df, mapping):
    """
    Sku replacement from mapping
    """
    sku_replaces = mapping.select(
        sf.col("sku_code_origin").alias("mat_cod"), sf.col("sku_code_dest").alias("new_mat_cod")
    )
    df = df.join(sku_replaces, on="mat_cod", how="left")
    df = (
        df.withColumn(
            "mat_cod",
            sf.when(sf.col("new_mat_cod").isNotNull(), sf.col("new_mat_cod")).otherwise(
                sf.col("mat_cod")
            ),
        )
        .withColumn("mat_cod", sf.col("mat_cod").cast(st.IntegerType()))
        .drop("new_mat_cod")
        .drop_duplicates()
    )

    return df


def generate_delistings_renovations(df, sku_replacement_mapping, delistings_hist):
    scope = get_core_plc()

    scope_reno = scope.where(sf.col("phasein_start_date").isNotNull())

    delistings_hist = delistings_hist.join(
        scope_reno.select("mat_cod").drop_duplicates(), on="mat_cod", how="left_anti"
    )

    sku_replacement_mapping = sku_replacement_mapping.join(
        scope_reno.select(
            sf.col("mat_cod").alias("sku_code_origin"),
            sf.col("mat_cod_reference").alias("sku_code_dest"),
        ).drop_duplicates(),
        on=["sku_code_origin", "sku_code_dest"],
        how="left_anti",
    )

    today = datetime.now()
    today = today.strftime("%Y-%m-%d")

    sku_replacement_mapping_to_append = (
        scope_reno.where(sf.col("phasein_end_date") < today)
        .select(
            sf.col("mat_cod_reference").alias("sku_code_origin"),
            sf.col("mat_cod").alias("sku_code_dest"),
        )
        .drop_duplicates()
    )

    sku_replacement_mapping = sku_replacement_mapping.unionByName(
        sku_replacement_mapping_to_append
    ).drop_duplicates()

    reference_products = (
        scope_reno.where(sf.col("phasein_end_date") > today)
        .select(sf.col("mat_cod_reference").alias("mat_cod"))
        .withColumn("is_reference", sf.lit(True))
        .drop_duplicates()
    )

    reno = (
        df.join(
            scope_reno.where(sf.col("phasein_end_date") > today)
            .select(
                sf.col("mat_cod").alias("new_mat_cod"), sf.col("mat_cod_reference").alias("mat_cod")
            )
            .drop_duplicates(),
            on=["mat_cod"],
            how="inner",
        )
        .withColumn(
            "mat_cod",
            sf.when(sf.col("new_mat_cod").isNotNull(), sf.col("new_mat_cod")).otherwise(
                sf.col("mat_cod")
            ),
        )
        .drop("new_mat_cod")
    )

    df = df.unionByName(reno).drop_duplicates()

    scope_delisting = scope.where(sf.col("phaseout_end_date").isNotNull())

    delistings_hist_to_append = (
        scope_delisting.join(reference_products, on="mat_cod", how="left")
        .join(
            sku_replacement_mapping.select(sf.col("sku_code_origin").alias("mat_cod"))
            .withColumn("is_sku_replacement", sf.lit(True))
            .drop_duplicates(),
            on="mat_cod",
            how="left",
        )
        .where(
            (
                sf.col("phaseout_end_date") < today
            ) & (
                ~sf.col("is_reference")
            ) & (
                ~sf.col("is_sku_replacement")
            )
        )
        .select("mat_cod")
        .distinct()
    )

    delistings_hist = delistings_hist.unionByName(delistings_hist_to_append).drop_duplicates()

    df = df.join(delistings_hist, on="mat_cod", how="left_anti")

    save_mappings(delistings_hist, "spain", "dairy", "delistings")

    return df, sku_replacement_mapping


def get_core_plc():
    product_assignment = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_SPN", "R_IBP_EDP_SPN_PRO_LFC_REF_PRO", "spark"
    )

    product_assignment = product_assignment.where(sf.col("is_active") == "true")

    ibp_master_data = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_SPN", "R_IBP_EDP_SPN_MAT_STS", "spark"
    )

    ibp_master_data = ibp_master_data.where(~sf.col("mat_cod").contains("[A-Za-z]")).withColumn(
        "mat_cod", sf.col("mat_cod").cast(st.IntegerType())
    )

    ibp_master_data = ibp_master_data.where(
        (
            sf.col("product_activation_cod") != "Z3"
        ) & (
            sf.col("product_aggregation") != "STOP ALGORITHM"
        )
    )

    scope = product_assignment.join(
        ibp_master_data, on=["mat_cod", "sales_organization_cod"], how="left"
    )
    scope = scope.where(sf.col("innovation_status") != "GPS")

    forecast_dates = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_SPN", "R_IBP_EDP_SPN_PRO_LFC_FOR_DAT", "spark"
    )

    forecast_dates = forecast_dates.where(sf.col("launch_dimension") == "DNSOID")

    today = datetime.now()
    today = today.strftime("%Y-%m-%d")

    forecast_dates = forecast_dates.withColumn("mat_cod", sf.col("mat_cod").cast(st.IntegerType()))
    forecast_dates = forecast_dates.where(sf.col("forecast_start_date") <= today)

    scope = scope.join(forecast_dates, on="mat_cod", how="left")

    return scope
