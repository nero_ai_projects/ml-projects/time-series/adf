import logging
import pandas as pd  # type: ignore
import numpy as np  # type: ignore
from datetime import timedelta
from functools import partial  # type: ignore
from pyspark.sql import SparkSession  # type: ignore
import pyspark.sql.functions as sf  # type: ignore
import pyspark.sql.types as st  # type: ignore
from pyspark.sql.window import Window  # type: ignore
from sqlalchemy import desc  # type: ignore
from adf.errors import DataprepError
from adf.modelling import mllogs
from adf.modelling.tools import get_snowflake_df_spark
from adf.utils.credentials import get_dbutils
from adf.utils.provider import Provider
from adf.modelling.dataprep.utils.core import build_promo_dataprep_report
from adf.services.database.query.reports import update_report
from adf.modelling.dataprep.spain.dairy.building_blocks import build_bb
from adf.services.database import get_session, BusinessUnits
from adf.modelling.dataprep.spain.dairy.inno_split import (
    implement_ibp_master_data_rules,
    filter_scopes,
)
from adf.services.database.building_blocks_reports import BuildingBlocksReports
from adf.modelling.dataprep.spain.dairy.plc import apply_plc_rules
from adf.modelling.dataprep.spain.dairy.update import (
    replace_non_primary_warehouse,
    update_orders_primaries,
    null_to_zero,
    keep_last,
)
from adf.config import SNOWFLAKE_DATABASE, SPAIN_PRIMARY_WAREHOUSES, MOUNT, FOLDER_CHECKPOINTS
from adf.enums import Indexes
import traceback


log = logging.getLogger("adf")
Provide = partial(Provider, mlrun=False, session=True, cluster=True)
spark = SparkSession.builder.getOrCreate()


def run(country: str, division: str, product_level, location_level, **context):
    """Build promos data for Spain Dairy given product_level and location_level
    :product_level: sku
    :location_level: warehouse
    """
    assert product_level in ("sku",)
    assert location_level in ("warehouse",)

    checkpoint_dir = f"{MOUNT}/{FOLDER_CHECKPOINTS}/{country}/{division}/promo_dataprep"
    spark.sparkContext.setCheckpointDir(checkpoint_dir)

    session = context["session"]
    pipeline_id = context.get("pipeline_id", None)
    building_block = context.get("building_block", False)
    compute_market_shares = context.get("compute_market_shares", False)
    report = build_promo_dataprep_report(
        session,
        country,
        division,
        product_level,
        location_level,
        scope="promo",
        split_type="",
        split_code=None,
        pipeline_id=pipeline_id,
    )
    mllogs.log_dataprep_report(report)

    try:
        log.info("Building panel")
        panel_kdf, panel_only_promo_kdf = build_promo_data()
        log.info("Saving")
        report.upload(panel_only_promo_kdf, partition_on="mat_cod")
        panel_kdf = panel_kdf.checkpoint()
        if building_block:
            log.info("Create building block table")
            build_bb(
                panel_kdf,
                compute_market_shares=compute_market_shares,
                session=session,
                dataprep_id=str(report.id),
                pipeline_id=pipeline_id,
            )
        update_report(session, report, "success")
        mllogs.tag_report_output(report)
        get_dbutils(spark).fs.rm(checkpoint_dir, recurse=True)

    except Exception as err:
        traceback.print_exc()
        message = str(err.__class__.__name__) + ": " + str(err)
        update_report(session, report, "failed", message)
        raise DataprepError(message)

    finally:
        mllogs.tag_report_output(report)

    return report, None


def build_promo_data():
    log.info("Loading orders...")
    orders_df = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_SPN", "F_TWI_EDP_SPN_SAL_DOC", "spark"
    )

    log.info("Apply PLC Rules...")
    orders_df = apply_plc_rules(orders_df)

    log.info("Order primaries...")
    orders_not_primaries = orders_df.where(~sf.col("warehouse_cod").isin(SPAIN_PRIMARY_WAREHOUSES))
    orders_primaries_updated = update_orders_primaries(orders_df)
    orders_df = orders_not_primaries.unionByName(orders_primaries_updated).drop_duplicates()
    orders_df = replace_non_primary_warehouse(orders_df)

    orders_df = orders_df.checkpoint()

    log.info("Implement IBP master data rules...")
    core_scope, inno_scope, ibp_mat_cods = implement_ibp_master_data_rules()
    orders_df, _ = filter_scopes(orders_df, core_scope, inno_scope)

    # log.info("Warehouses from orders...")
    # orders_df = drop_primary_whs_as_customer(orders_df)

    metrics_to_compute = [
        sf.sum(metric).alias(metric)
        for metric in ["adjusted_units", "assigned_units", "delivered_units"]
    ]

    orders_df = orders_df.groupBy("mat_cod", "cus_cod", "warehouse_cod", "to_dt").agg(
        *metrics_to_compute
    )

    log.info("Loading promos...")
    promo_df = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_SPN", "F_MAN_EDP_SPN_PMN_PLN", "spark"
    )

    log.info("Apply PLC Rules...")
    promo_df = apply_plc_rules(promo_df)

    window = Window.partitionBy("cus_cod", "mat_cod").orderBy(sf.col("to_dt").desc())

    mapping = keep_last(orders_df, window)

    mapping = mapping.select("cus_cod", "mat_cod", "warehouse_cod").drop_duplicates()

    promo_df = promo_df.join(mapping, on=["cus_cod", "mat_cod"], how="left")

    promo_df = promo_df.where(sf.col("warehouse_cod").isin(SPAIN_PRIMARY_WAREHOUSES))

    log.info("Merge promo with customers")
    customer_df = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_SPN", "R_MAN_EDP_SPN_CUS_SAL", "spark"
    )

    customer_df = customer_df.select("cus_cod", "franchise_cod").drop_duplicates()

    promo_df = promo_df.join(customer_df, on=["cus_cod"], how="left")

    log.info("Processing promos...")
    promo_no_promo_df = process_product_orders_with_promos(promo_df, orders_df)

    only_promo_df = promo_no_promo_df.where(sf.col("promotion_mechanics_cod") != 0)
    return promo_no_promo_df, only_promo_df


def process_product_orders_with_promos(promo_df, orders_df):

    promo_df = promo_df.where(
        sf.col("promotion_start_date_sellin") < sf.col("promotion_end_date_sellin")
    )

    promo_df = shift_franchises_start_sellin(promo_df)

    promo_df = clean_promo(promo_df)

    promo_df = expand_promos_to_daily(
        promo_df, "promotion_start_date_sellin", "promotion_end_date_sellin", "to_dt"
    )

    promo_df = discriminate_codes_promo(promo_df)

    promo_df = remove_sundays_promo(promo_df)

    join_keys = ["to_dt", "cus_cod", "mat_cod", "warehouse_cod"]

    promo_enriched_df = orders_df.join(promo_df, on=join_keys, how="right")

    promo_enriched_df = promo_enriched_df.withColumn(
        "adjusted_units",
        sf.when(sf.col("adjusted_units").isNull(), 0).otherwise(sf.col("adjusted_units")),
    )

    orders_enriched_df = orders_df.join(promo_df, on=join_keys, how="left")

    product_orders_no_promo = orders_enriched_df.where(sf.col("promotion_mechanics_cod").isNull())

    product_orders_no_promo = product_orders_no_promo.withColumn(
        "promotion_mechanics_cod", sf.lit(0)
    ).withColumn("promotion_type_desc", sf.lit("no_promo"))

    selection_list = [
        sf.col("mat_cod").cast(st.IntegerType()),
        sf.col("cus_cod").cast(st.IntegerType()),
        sf.col("warehouse_cod").cast(st.FloatType()),
        sf.col("to_dt").cast(st.TimestampType()),
        sf.col("adjusted_units").cast(st.FloatType()),
        sf.col("promotion_mechanics_cod").cast(st.FloatType()),
    ]

    return (
        promo_enriched_df.select(selection_list)
        .unionByName(product_orders_no_promo.select(selection_list))
        .drop_duplicates()
    )


def shift_franchises_start_sellin(promo_df):
    """Shift promo start date sellin date of 3 opened days
    for specific retailers
    """
    mon_tue_wed = sf.dayofweek("promotion_start_date_sellin").isin([0, 1, 2])

    promo_df = promo_df.withColumn(
        "promotion_start_date_sellin",
        sf.when(mon_tue_wed, sf.date_add("promotion_start_date_sellin", -4)).otherwise(
            sf.date_add("promotion_start_date_sellin", -3)
        ),
    )

    return promo_df


def clean_promo(promo_df):
    """
    Drops duplicates by prioritizing lower promotion_type_cod and then lower promotion_mechanics_cod
    Then if there are still duplicates, truncates the end date of older promotions
    """
    dates_window = Window.partitionBy(
        "mat_cod", "cus_cod"
    ).orderBy("promotion_start_date_sellin")

    type_cod_window = Window.partitionBy(
        "mat_cod", "cus_cod", "promotion_start_date_sellin"
    ).orderBy("promotion_start_date_sellin")

    mechanics_cod_window = Window.partitionBy(
        "mat_cod", "cus_cod", "promotion_start_date_sellin", "promotion_type_cod"
    ).orderBy("promotion_start_date_sellin")

    promo_df = promo_df.withColumn(
        "promotion_type_cod",
        sf.min("promotion_type_cod").over(type_cod_window)
    ).withColumn(
        "promotion_mechanics_cod",
        sf.min("promotion_mechanics_cod").over(mechanics_cod_window)
    )

    promo_df = promo_df.drop_duplicates(
        subset=['mat_cod', 'cus_cod', 'promotion_start_date_sellin']
    )

    promo_df = promo_df.withColumn(
        "tmp",
        sf.lag("promotion_start_date_sellin").over(dates_window)
    )

    promo_df = promo_df.withColumn("tmp", sf.date_add("tmp", -1))

    promo_df = promo_df.withColumn(
        "promotion_end_date_sellin",
        sf.when(
            sf.least(
                "promotion_end_date_sellin",
                "tmp"
            ) >= sf.col("promotion_start_date_sellin"),
            sf.least("promotion_end_date_sellin", "tmp")
        ).otherwise(sf.col("promotion_end_date_sellin"))
    ).drop("tmp")

    return promo_df


def discriminate_codes_promo(promo_df):
    """
    Separates promotion_mechanics_cod for promotions that lasted longer than 7 days
    """
    col_promotion_mechanics_cod = "promotion_mechanics_cod"
    col_elapsed_time = "elapsed_time"
    promo_df = promo_df.withColumn(
        col_promotion_mechanics_cod,
        sf.when(sf.col(col_elapsed_time) > 7, -sf.col(col_promotion_mechanics_cod)).otherwise(
            sf.col(col_promotion_mechanics_cod)
        ),
    ).drop(col_elapsed_time)
    return promo_df


def remove_sundays_promo(promo_df):
    """
    Removes Sunday rows
    """
    return promo_df.where(sf.dayofweek("to_dt") < 6)


@sf.udf(st.DateType())
def add_days(date_col, days_col):
    return date_col + timedelta(days=days_col)


def expand_promos_to_daily(df, promo_start_date_column, promo_end_date_column, target_date_col):
    """
    Creates a target_date_col column and adds a row for each dates between promo_start_date_column
    and promo_end_date_column into input promotions df

    Parameters
    ----------
    df: spark.DataFrame
        input promotions dataframe
    promo_start_date_column: str
        name of column containing the start date information for each promotion
    promo_end_date_column: str
        name of column containing the end date information for each promotion
    target_date_col: str
        name of target date column to create in input dataframe

    Returns
    -------
    spark.DataFrame
        updated dataframe
    """
    df = df.withColumn(
        "time_delta_days",
        sf.datediff(sf.col(promo_end_date_column), sf.col(promo_start_date_column)),
    ).withColumn(
        "time_delta",
        sf.unix_timestamp(
            sf.col(promo_end_date_column).cast(st.TimestampType())
        ) -
        sf.unix_timestamp(
            sf.col(promo_start_date_column).cast(st.TimestampType())
        ),
    )

    max_delta = df.agg(sf.max("time_delta_days").alias("max_time_delta")).collect()[0][
        "max_time_delta"
    ]

    elapsed_times = pd.concat(
        [
            pd.DataFrame({"elapsed_time": np.arange(0, delta + 1)}).assign(
                time_delta=pd.to_timedelta(delta, unit="d")
            )
            for delta in range(max_delta + 1)
        ]
    )

    elapsed_times = spark.createDataFrame(elapsed_times)

    elapsed_times = elapsed_times.withColumn("time_delta", sf.col("time_delta") / 1000000000)

    df = df.join(elapsed_times, on="time_delta", how="left")

    df = df.withColumn(
        target_date_col,
        add_days(sf.col(promo_start_date_column).cast(st.DateType()), sf.col("elapsed_time")),
    )

    df = df.where(sf.col(target_date_col) <= sf.col(promo_end_date_column).cast(st.DateType()))

    to_drop = ("time_delta", "time_delta_days", promo_start_date_column, promo_end_date_column)

    df = df.drop(*to_drop)

    return df.drop_duplicates()


def finalize_promo(product_level, location_level, preprocessing_report, pipeline_id):
    session = get_session()
    report = build_promo_dataprep_report(
        session,
        "spain",
        "dairy",
        product_level,
        location_level,
        scope="finalized_promo",
        split_type="",
        split_code=None,
        pipeline_id=pipeline_id,
    )

    mllogs.log_dataprep_report(report)

    log.info("Building panel")
    df = preprocessing_report.download(object_type="spark")
    df = pivot_by_promo_type_and_fill_na(df)

    log.info("Saving")
    report.upload(df)
    update_report(session, report, "success")
    mllogs.tag_report_output(report)
    return report


def pivot_by_promo_type_and_fill_na(df):
    agg_cols = [col for col in df.columns if find_prefix(col)]
    new_cols = Indexes.copy()
    for col in agg_cols:
        new_cols.extend(
            [
                f"{col}_new_promo_high",
                f"{col}_old_promo_high",
                f"{col}_new_promo_low",
                f"{col}_old_promo_low",
            ]
        )

    metrics = [sf.sum(column).alias(column) for column in agg_cols]
    df = df.withColumn("promo_type", get_promo_type("promotion_mechanics_cod"))

    df = df.groupBy(Indexes + ["promo_type"]).agg(*metrics)

    df = df.groupBy(Indexes).pivot("promo_type").agg(*metrics)

    for col in new_cols:
        if col not in df.columns:
            df = df.withColumn(col, sf.lit(None).cast(st.IntegerType()))

    return df


@sf.udf(st.StringType())
def get_promo_type(mechanics):
    new_promo_high = [7, 10, 11, 15, 19]
    old_promo_high = list(np.array(new_promo_high) * -1)
    if mechanics == 0:
        return "non_promo"
    elif mechanics in new_promo_high:
        return "new_promo_high"
    elif mechanics in old_promo_high:
        return "old_promo_high"
    elif mechanics > 0:
        return "new_promo_low"
    elif mechanics < 0:
        return "old_promo_low"


def find_prefix(x):
    return any([keyword in x for keyword in ["rolling_mean", "cum_mean"]])


def retailers_promo(product_level, location_level, dataprep_report, pipeline_id, **kwargs):
    session = get_session()
    report = build_promo_dataprep_report(
        session,
        "spain",
        "dairy",
        product_level,
        location_level,
        scope="retailers_promo",
        split_type="",
        split_code=None,
        pipeline_id=pipeline_id,
    )

    mllogs.log_dataprep_report(report)

    log.info("Building panel")
    promo_dataprep = dataprep_report.download(object_type="spark")
    df = build_retailers_promo(promo_dataprep)

    log.info("Saving")
    report.upload(df)
    update_report(session, report, "success")
    mllogs.tag_report_output(report)
    return report


def build_retailers_promo(dataprep):
    log.info("Loading orders...")
    orders_df = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_SPN", "F_TWI_EDP_SPN_SAL_DOC", "spark"
    )

    log.info("Apply PLC Rules...")
    orders_df = apply_plc_rules(orders_df)

    log.info("Implement IBP master data rules...")
    core_scope, inno_scope, _ = implement_ibp_master_data_rules()
    orders_df, _ = filter_scopes(orders_df, core_scope, inno_scope)

    log.info("Order primaries...")
    orders_df = replace_non_primary_warehouse(orders_df)
    # orders_df = drop_primary_whs_as_customer(orders_df)

    log.info("Loading promos...")
    dataprep = dataprep.repartition(1000, ["mat_cod", "cus_cod"])
    promo_df = dataprep.select(
        "promotion_mechanics_cod", "cus_cod", "mat_cod", "to_dt", "warehouse_cod"
    ).drop_duplicates()

    log.info("Expand dates quarterly...")
    orders_quarter_df = expand_dates_quarter(orders_df)

    log.info("Get top retailers quarterly...")
    orders_quarter_top_retailers_df = get_top_retailers_quarterly(orders_quarter_df, 20)

    orders_quarter_top_retailers_df = orders_quarter_top_retailers_df.withColumn(
        "top_retailer", sf.lit(1)
    ).withColumnRenamed("adjusted_units", "adjusted_units_top")

    log.info("Merge orders with top retails orders...")
    orders_quarter_df = orders_quarter_df.join(
        orders_quarter_top_retailers_df,
        on=["cus_cod", "mat_cod", "warehouse_cod", "to_dt"],
        how="left",
    )

    null_cols = ["adjusted_units", "adjusted_units_top", "top_retailer"]

    for col_name in null_cols:
        orders_quarter_df = null_to_zero(orders_quarter_df, col_name)

    orders_quarter_df = (
        orders_quarter_df.withColumn("shifted_date", sf.add_months("to_dt", -3))
        .withColumn(
            "quarter_dt",
            sf.concat_ws(
                "-", sf.year("shifted_date"), (sf.quarter("shifted_date") - 1) * 3 + 1, sf.lit(1)
            ).cast(st.TimestampType()),
        )
        .select("quarter_dt", "mat_cod", "warehouse_cod", "cus_cod", "top_retailer")
        .withColumn("mat_cod", sf.col("mat_cod").cast("int"))
        .withColumn("cus_cod", sf.col("cus_cod").cast("int"))
        .withColumn("warehouse_cod", sf.col("warehouse_cod").cast("int"))
    )

    promo_df = (
        promo_df.withColumn(
            "quarter_dt",
            sf.concat_ws("-", sf.year("to_dt"), (sf.quarter("to_dt") - 1) * 3 + 1, sf.lit(1)).cast(
                st.TimestampType()
            ),
        )
        .withColumn("mat_cod", sf.col("mat_cod").cast("int"))
        .withColumn("cus_cod", sf.col("cus_cod").cast("int"))
        .withColumn("warehouse_cod", sf.col("warehouse_cod").cast("int"))
        .withColumn("promotion_mechanics_cod", sf.col("promotion_mechanics_cod").cast("int"))
        .drop("adjusted_units")
    )

    by_list = ["cus_cod", "mat_cod", "warehouse_cod"]
    on = "quarter_dt"

    schema = "cus_cod int, mat_cod int, warehouse_cod int, to_dt date, quarter_dt timestamp, \
        promotion_mechanics_cod int, top_retailer int"

    def asof_join(left, right):
        left = left.sort_values("to_dt")
        right = right.sort_values("quarter_dt")
        return pd.merge_asof(left, right, on=on, by=by_list)

    log.info("Merge promos with top retailer data...")
    retailers_promos_df = (
        promo_df.groupby(by_list)
        .cogroup(orders_quarter_df.groupby(by_list))
        .applyInPandas(asof_join, schema=schema)
        .select(
            "to_dt",
            "mat_cod",
            "warehouse_cod",
            "cus_cod",
            "promotion_mechanics_cod",
            "top_retailer",
        )
    )

    retailers_promos_df = retailers_promos_df.withColumn(
        "source",
        sf.when(sf.col("promotion_mechanics_cod") < 0, "old_promo")
        .when(sf.col("promotion_mechanics_cod") > 0, "new_promo")
        .otherwise("unknown"),
    ).where(sf.col("source") != "unknown")

    log.info("Get top retailer counts...")
    promos_orders_grouped_df = retailers_promos_df.groupBy(
        "to_dt", "mat_cod", "warehouse_cod", "source"
    ).agg(
        sf.sum("top_retailer").alias("top_retailer_sum"),
        sf.count("top_retailer").alias("top_retailer_count"),
    )

    log.info("Pivot promos with retailer info...")
    promos_orders_grouped_pivot_df = (
        promos_orders_grouped_df.groupBy("to_dt", "warehouse_cod", "mat_cod")
        .pivot("source")
        .agg(
            sf.sum("top_retailer_sum").alias("top_retailer_sum"),
            sf.sum("top_retailer_count").alias("top_retailer_count"),
        )
    )

    null_cols = [
        ("new_promo_top_retailer_sum", "top_retailer_sum_new_promo"),
        ("new_promo_top_retailer_count", "top_retailer_count_new_promo"),
        ("old_promo_top_retailer_sum", "top_retailer_sum_old_promo"),
        ("old_promo_top_retailer_count", "top_retailer_count_old_promo"),
    ]

    for col_name, new_col_name in null_cols:
        promos_orders_grouped_pivot_df = null_to_zero(promos_orders_grouped_pivot_df, col_name)
        promos_orders_grouped_pivot_df = promos_orders_grouped_pivot_df.withColumnRenamed(
            col_name, new_col_name
        )

    return promos_orders_grouped_pivot_df


def expand_dates_quarter(df):
    """
    Resampling to quarter granularity
    """
    df = (
        df.withColumn(
            "to_dt",
            sf.date_add(
                sf.concat_ws(
                    "-",
                    sf.year("to_dt") + (3 * sf.quarter("to_dt") / 12).cast("int"),
                    3 * sf.quarter("to_dt") % 12 + 1,
                    sf.lit(1),
                ).cast(st.DateType()),
                -1,
            ).cast(st.TimestampType()),
        )
        .groupBy("cus_cod", "mat_cod", "warehouse_cod", "to_dt")
        .agg(sf.sum("adjusted_units").alias("adjusted_units"))
    )
    return df


def get_top_retailers_quarterly(df, top):
    window = Window.partitionBy("mat_cod", "warehouse_cod", "to_dt").orderBy(
        sf.col("adjusted_units").desc()
    )

    df = df.withColumn("rank", sf.rank().over(window)).where(sf.col("rank") <= top).drop("rank")
    return df


def uplifts_promo(product_level, location_level, dataprep_report, pipeline_id, **kwargs):
    session = get_session()
    report = build_promo_dataprep_report(
        session,
        "spain",
        "dairy",
        product_level,
        location_level,
        scope="uplifts_promo",
        split_type="",
        split_code=None,
        pipeline_id=pipeline_id,
    )

    mllogs.log_dataprep_report(report)

    log.info("Building panel")
    promo_dataprep = dataprep_report.download(object_type="spark")
    df = build_uplifts_promo(session, promo_dataprep)

    log.info("Saving")
    report.upload(df)
    update_report(session, report, "success")
    mllogs.tag_report_output(report)
    return report


def build_uplifts_promo(session, promo_dataprep):
    business_unit = session.query(BusinessUnits). \
        filter(BusinessUnits.country == "spain"). \
        filter(BusinessUnits.division == "dairy"). \
        first()

    bb_report = session.query(BuildingBlocksReports). \
        filter(BuildingBlocksReports.status == "success"). \
        filter(BuildingBlocksReports.business_unit_id == business_unit.id). \
        filter(BuildingBlocksReports.scope == "promo_building_block"). \
        order_by(desc(BuildingBlocksReports.updated_at)). \
        first()

    bb_table = bb_report.download(object_type="spark")

    customer_df = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_SPN", "R_MAN_EDP_SPN_CUS_SAL", "spark"
    )

    customer_df = (
        customer_df.select("cus_cod", "franchise_cod")
        .drop_duplicates()
        .where(~sf.col("franchise_cod").contains("[#]"))
        .withColumn("franchise_cod", sf.col("franchise_cod").cast(st.IntegerType()))
    )

    promo_df = promo_dataprep.join(customer_df, on=["cus_cod"], how="left")

    uplifts_df = promo_df.join(
        bb_table, on=["mat_cod", "warehouse_cod", "franchise_cod"], how="left"
    )

    uplifts_df = uplifts_df.withColumn(
        "total_uplift", sf.col("mc_uplift") + sf.col("tpr_uplift") + sf.col("crm_uplift")
    )

    uplifts_df = uplifts_df.groupBy("mat_cod", "to_dt", "warehouse_cod").agg(
        sf.sum("total_uplift").alias("total_uplift")
    )
    return uplifts_df
