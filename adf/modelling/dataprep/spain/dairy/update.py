import logging
import pyspark.sql.functions as sf  # type: ignore
import pyspark.sql.types as st  # type: ignore
from pyspark.sql.window import Window  # type: ignore
from adf.config import SPAIN_PRIMARY_WAREHOUSES
from adf.utils.mappings import save_mappings

log = logging.getLogger("adf")


def update_orders_primaries(orders_df):
    """From mapping tables, update SKU codes, last primary warehouse locations
    for each Product/Customer couple
    """
    log.info("Map last location for each sku/customer couple")
    orders_df = orders_df.where(sf.col("warehouse_cod").isin(SPAIN_PRIMARY_WAREHOUSES))

    group_list = ["cus_cod", "mat_cod"]

    window = Window.partitionBy(group_list).orderBy(sf.col("to_dt").desc())

    mapping = keep_last(orders_df, window)

    mapping = mapping.select(group_list).drop_duplicates()

    orders_df = orders_df.join(mapping, on=group_list, how="left")

    orders_df = orders_df.orderBy("to_dt")
    return orders_df


def update_primaries_mappings(orders_df, location_mappings):
    """Update orders_df and mappings with latest updated warehouse codes"""
    orders_df = update_primaries(orders_df, location_mappings)
    orders_df = update_mappings(orders_df)
    return orders_df


def update_primaries(orders_df, location_mappings):
    """Update orders_df and mappings with latest updated primary warehouse codes"""
    if location_mappings.head(1).isEmpty:
        return orders_df

    location_mappings = location_mappings.select(
        "cus_cod", "mat_cod", sf.col("warehouse_cod").alias("warehouse_cod_mapping")
    )
    location_mappings = location_mappings.where(
        sf.col("warehouse_cod").isin(SPAIN_PRIMARY_WAREHOUSES)
    )

    log.info(f"Merging location_changes with orders...")

    orders_df = orders_df.join(location_mappings, on=["cus_cod", "mat_cod"], how="left")

    orders_df = orders_df.withColumn(
        "warehouse_cod",
        sf.when(
            sf.col("warehouse_cod_mapping").isNotNull(), sf.col("warehouse_cod_mapping")
        ).otherwise(sf.col("warehouse_cod")),
    ).withColumn("warehouse_cod", sf.col("warehouse_cod").cast(st.IntegerType()))

    log.info("Done merging location_changes with orders !")

    return orders_df


def update_mappings(orders_df):
    """Update mapping table with updated data scope"""
    log.info("Updating location mappings")

    window = Window.partitionBy("mat_cod", "cus_cod").orderBy("to_dt")

    location_mappings = keep_last(orders_df, window)

    location_mappings = location_mappings.select(
        "cus_cod", "mat_cod", "warehouse_cod"
    ).drop_duplicates()
    log.info("Done updating location mappings")
    save_mappings(location_mappings, "spain", "dairy", "customer_product_location_mapping")
    return orders_df


def replace_non_primary_warehouse(df):
    df = df.where(sf.col("warehouse_cod") != sf.col("cus_cod"))
    dl_crp_codes = (
        df.select("warehouse_cod")
        .where(~sf.col("warehouse_cod").isin(SPAIN_PRIMARY_WAREHOUSES))
        .distinct()
        .collect()
    )
    dl_crp_codes = [e["warehouse_cod"] for e in dl_crp_codes]

    while True:
        log.info("Replacing non primary warehouse")
        df = replace_dl_crps(df, dl_crp_codes)
        new_warehouse_cods = df.select("warehouse_cod").distinct().collect()
        new_warehouse_cods = [e["warehouse_cod"] for e in new_warehouse_cods]
        if set(new_warehouse_cods) == set(SPAIN_PRIMARY_WAREHOUSES):
            break
    df = df.where(~sf.col("cus_cod").isin(dl_crp_codes))
    return df


def replace_dl_crps(df, dl_crp_codes):
    """Replace DL/CRP codes as Locations by the last Primary to send orders to
    those as customers
    """
    location_mappings = df.where(sf.col("cus_cod").isin(dl_crp_codes))

    window = Window.partitionBy("mat_cod", "cus_cod").orderBy("to_dt")

    location_mappings = keep_last(location_mappings, window)

    location_mappings = location_mappings.select("mat_cod", "warehouse_cod", "cus_cod")

    df = df.withColumn(
        "to_dt",
        sf.when(
            sf.col("warehouse_cod").isin(dl_crp_codes) & (sf.dayofweek("to_dt") != 2),
            sf.date_add("to_dt", -3),
        )
        .when(
            sf.col("warehouse_cod").isin(dl_crp_codes) & (sf.dayofweek("to_dt") == 2),
            sf.date_add("to_dt", -2),
        )
        .otherwise(sf.col("to_dt")),
    )

    df_dl_crp = df.where(sf.col("warehouse_cod").isin(dl_crp_codes))
    df_wh = df.where(~sf.col("warehouse_cod").isin(dl_crp_codes))
    locations_mappings = location_mappings.withColumnRenamed(
        "warehouse_cod", "up_to_date_wh"
    ).withColumnRenamed("cus_cod", "warehouse_cod")

    df_dl_crp = df_dl_crp.join(locations_mappings, how="left", on=["mat_cod", "warehouse_cod"])
    df_dl_crp = df_dl_crp.drop("warehouse_cod")
    df_dl_crp = df_dl_crp.withColumnRenamed("up_to_date_wh", "warehouse_cod")
    df_dl_crp = df_dl_crp.where(sf.col("warehouse_cod").isNotNull()).withColumn(
        "warehouse_cod", sf.col("warehouse_cod").cast(st.IntegerType())
    )
    df = df_dl_crp.unionByName(df_wh)
    return df


def drop_primary_whs_as_customer(df):
    """
    Returns orders without rows where cus_cod is in a newly added primary warehouses 175/176.

    Parameters
    ----------
    df: input orders dataframe

    Returns
    -------
    spark.DataFrame
        dataframe without orders with cus_cod in newly added primary warehouses 175/176
    """
    df = df.where(~((sf.col("cus_cod") == 175) | (sf.col("cus_cod") == 176)))
    return df


def null_to_zero(df, col_name):
    return df.withColumn(
        col_name, sf.when(sf.col(col_name).isNull(), 0).otherwise(sf.col(col_name))
    )


def keep_last(df, window):

    df = df.withColumn("rank", sf.rank().over(window)).where(sf.col("rank") == 1).drop("rank")
    return df
