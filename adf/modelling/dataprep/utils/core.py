import logging
import datetime
from itertools import product
from typing import Optional, List, Union
import databricks.koalas as ks  # type: ignore
import pandas as pd  # type: ignore
from dateutil.relativedelta import relativedelta
from adf.utils.decorators import timeit
from adf.errors import DataprepError
from adf.services.database.dataprep_reports import (
    DataprepReports,
    PromoDataprepReports,
    InnoDataprepReports
)
from adf.services.database.building_blocks_reports import BuildingBlocksReports
from adf.services.database.query import (
    build_report,
    get_business_unit
)
from adf.types import DataFrame

log = logging.getLogger("adf")


def from_date_to_datetime(
        df: pd.DataFrame, columns: Union[str, List[str]], **kwargs
) -> pd.DataFrame:
    """
    Converts a column or list of columns into datetime64[ns] columns and returns updated dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        input dataframe
    columns: str, List[str]
        columns to be typed as datetime64[ns]

    Returns
    -------
    pandas.DataFrame
        updated dataframe
    """
    df[columns] = df[columns].apply(lambda x: pd.to_datetime(x, **kwargs))
    return df


@timeit
def build_panel_indexes(
        orders_df: pd.DataFrame, end_date: pd.Timestamp, product_column: str,
        location_column: str, date_column: str,
        customer_column: Optional[str] = None,
        sales_organization_column: Optional[str] = None
) -> pd.DataFrame:
    """
    Returns a dataframe containing all combinations of
    sales_organization (if existing) x products x locations x dates built from all sales
    organizations (if existing), products and locations contained in input orders dataframe, and
    from minimal local product dates to a specified end date

    Parameters
    ----------
    orders_df: pandas.DataFrame
        input orders dataframe from which to build indexes
    end_date: pandas.Timestamp
        maximal date to build date indexes for each product x location combination
    product_column: str
        column name of product field in orders dataframe for which to generate indexes
    location_column: str
        column name of location field in orders dataframe for which to generate indexes
    date_column: str
        column name of target date field in orders dataframe for which to generate indexes
    customer_column: str, optional
        column name of customer field in orders dataframe for which to generate indexes
    sales_organization_column: str, optional

    Returns
    -------
    pandas.DataFrame
        dataframe with columns sales_organization (if existing) x products x locations x dates \
        containing all combinations
    """
    panel_indexes = pd.concat(
        [
            build_sub_panel_indexes(
                orders_df.query(f'{product_column} == {one_product}'),
                end_date,
                product_column,
                location_column,
                date_column,
                customer_column,
                sales_organization_column
            )
            for one_product in orders_df[product_column].unique()
        ],
        axis=0
    )
    return panel_indexes


def build_sub_panel_indexes(
        sub_orders_df: DataFrame, end_date: pd.Timestamp, product_column: str,
        location_column: str, date_column: str,
        customer_column: Optional[str] = None,
        sales_organization_column: Optional[str] = None,
        object_type: str = "pandas"
) -> DataFrame:
    """
    Returns a dataframe containing all combinations of
    sales_organization (if existing) x product x locations x dates built from a sale
    organization (if existing), a product and locations contained in input orders sample dataframe
    (only one unique product value in this sample), and from minimal local product dates to a
    specified end date

    Parameters
    ----------
    sub_orders_df: pandas.DataFrame
        input orders sample dataframe from which to build indexes, containing only one unique \
        product value
    end_date: pandas.Timestamp
        maximal date to build date indexes for each product x location combination
    product_column: str
        column name of product field in orders dataframe for which to generate indexes
    location_column: str
        column name of location field in orders dataframe for which to generate indexes
    date_column: str
        column name of target date field in orders dataframe for which to generate indexes
    customer_column: str, optional
        column name of customer field in orders dataframe for which to generate indexes
    sales_organization_column: str, optional

    Returns
    -------
    pandas.DataFrame
        dataframe with columns sales_organization (if existing) x product x locations x dates \
        containing all combinations
    """
    index = {}
    if sales_organization_column:
        index.update({sales_organization_column: sub_orders_df[sales_organization_column].unique()})
    if customer_column:
        index.update({customer_column: sub_orders_df[customer_column].unique()})
    index.update({
        product_column: sub_orders_df[product_column].unique(),
        location_column: sub_orders_df[location_column].unique(),
        date_column: pd.date_range(sub_orders_df[date_column].min(), end_date)
    })
    if object_type == "pandas":
        return pd.DataFrame(list(product(*index.values())), columns=list(index.keys()))
    elif object_type == "koalas":
        return ks.DataFrame(list(product(*index.values())), columns=list(index.keys()))
    else:
        raise Exception(f"Could not download dataframe from mount with type {object_type}")


def flatten_columns(
        two_levels_columns: Union[pd.Index, List]
) -> List:
    """
    Creates a list of one-level columns names from a two-level pandas columns indexes

    Example: transforms pandas.Index((col1, op1), (col2, op2)) to [col1_op1, col2_op2]

    Parameters
    ----------
    two_levels_columns: two-level pandas.Index
        two-level pandas columns indexes

    Returns
    -------
    List
        list of one-level columns names
    """
    one_level_columns = [col + "_" + op for col, op in two_levels_columns]
    one_level_columns = [col[:-1] if col[-1] == "_" else col for col in one_level_columns]
    return one_level_columns


def build_end_date(
        lag_type: str, lag_value: int
) -> pd.Timestamp:
    """
    Returns a date which is a future lag of current date based on input number of days, weeks or
    months

    Parameters
    ----------
    lag_type: str, {"days", "weeks", "month"}
        lag type to apply on current date
    lag_value: int
        value of lag to apply on current date

    Returns
    -------
    pandas.Timestamp
        date which is a future lag of current date
    """
    assert lag_type in ("days", "weeks", "months")
    if lag_type == "days":
        return pd.Timestamp(datetime.date.today() + relativedelta(days=lag_value))
    elif lag_type == "weeks":
        return pd.Timestamp(datetime.date.today() + relativedelta(weeks=lag_value))
    else:
        return pd.Timestamp(datetime.date.today() + relativedelta(months=lag_value))


def format_to_4_digits(
        df: pd.DataFrame, col_name: str
) -> pd.DataFrame:
    """
    Formats a given field from input dataframe onto 4 characters

    Parameters
    ----------
    df: pandas.DataFrame
        input dataframe
    col_name: str
        field/column from dataframe to format

    Returns
    -------
    pandas.DataFrame
        updated dataframe containing formatted field that was replaced
    """
    df[col_name] = df[col_name].apply(lambda x: str(x).zfill(4))
    return df


def safe_merge(
        left_df: pd.DataFrame, left_source_name: str, right_df: pd.DataFrame,
        right_source_name: str, warning_type: str = "warning", **kwargs
) -> pd.DataFrame:
    """
    Performs a classical left or inner pandas.DataFrame.merge between left_df and right_df raising a
    warning/error based on warning_type parameter in case some elements from left_df do not find a
    match in right_df

    Parameters
    ----------
    left_df: pandas.DataFrame
        left dataframe
    left_source_name: str
        name of left dataframe source
    right_df: pandas.Dataframe
        right_dataframe
    right_source_name: str
        name of right dataframe source
    warning_type: DataprepError or DataprepWarning
        type of warning/error to raise

    Returns
    -------
    pandas.DataFrame
        merged dataframes output
    """
    assert warning_type in ("error", "warning")
    assert kwargs.get("how", None) in ("inner", "left")
    assert kwargs.get("on", None) is not None

    how = kwargs["how"]
    kwargs.pop("how")
    keys = kwargs["on"]

    left_merge = left_df.merge(right_df, how="left", indicator=True, **kwargs)
    missing_rows = left_merge["_merge"].value_counts()["left_only"]
    missing_elements = len(left_merge.query('_merge == "left_only"')[keys].drop_duplicates())

    message = f"{how} merging {left_source_name}/{right_source_name}. "
    if missing_rows > 0:
        message += f"missing: {missing_rows} rows ~ {missing_elements} unique {keys}"
        if warning_type == "error":
            raise DataprepError(message)
        else:
            log.warning(message)
    else:
        log.info(message)

    return left_merge.drop("_merge", axis=1) if how == "left" \
        else left_df.merge(right_df, how=how, **kwargs)


def build_dataprep_report(
        session,
        country,
        division,
        product_level,
        location_level,
        scope,
        split_type,
        split_code,
        prep_type,
        pipeline_id
) -> DataprepReports:
    bu_id = get_business_unit(session, country, division).id
    fields = {
        "business_unit_id": bu_id,
        "product_level": product_level,
        "location_level": location_level,
        "scope": scope,
        "split_type": split_type,
        "split_code": split_code,
        "prep_type": prep_type,
        "pipeline_id": pipeline_id
    }
    report = build_report(session, DataprepReports, **fields)
    return report


def build_promo_dataprep_report(
        session,
        country,
        division,
        product_level,
        location_level,
        scope,
        split_type,
        split_code,
        pipeline_id
) -> PromoDataprepReports:
    bu_id = get_business_unit(session, country, division).id
    fields = {
        "business_unit_id": bu_id,
        "product_level": product_level,
        "location_level": location_level,
        "scope": scope,
        "split_type": split_type,
        "split_code": split_code,
        "prep_type": "promotion",
        "pipeline_id": pipeline_id
    }
    report = build_report(session, PromoDataprepReports, **fields)
    return report


def build_innovation_dataprep_report(
        session,
        country,
        division,
        product_level,
        location_level,
        scope,
        split_type,
        split_code,
        pipeline_id
) -> InnoDataprepReports:
    bu_id = get_business_unit(session, country, division).id
    fields = {
        "business_unit_id": bu_id,
        "product_level": product_level,
        "location_level": location_level,
        "scope": scope,
        "split_type": split_type,
        "split_code": split_code,
        "prep_type": "innovation",
        "pipeline_id": pipeline_id
    }
    report = build_report(session, InnoDataprepReports, **fields)
    return report


def build_building_blocks_report(
        session,
        country,
        division,
        scope,
        dataprep_id,
        pipeline_id
) -> BuildingBlocksReports:
    bu_id = get_business_unit(session, country, division).id
    fields = {
        "business_unit_id": bu_id,
        "scope": scope,
        "dataprep_report_id": dataprep_id,
        "pipeline_id": pipeline_id
    }
    report = build_report(session, BuildingBlocksReports, **fields)
    return report
