import pandas as pd  # type: ignore
from adf import config
from adf.modelling.tools import get_snowflake_df
from adf.utils.decorators import timeit


@timeit
def get_holidays_df(
        location: str, holiday_type
) -> pd.DataFrame:
    """
    Loads and concatenates all holidays S3 flat files into a single dataframe, for a given country
    and holiday type. This method also formats the date column to %Y-%m-%d.

    Parameters
    ----------
    location: str
        country for which to extract holidays data
    holiday_type: str
        value can be "dayoff" or "holidays&dayoff"

    Returns
    -------
    pandas.DataFrame
        holidays dataframe
    """
    holidays_df = get_snowflake_df(
        config.SNOWFLAKE_DATABASE, config.SNOWFLAKE_SCHEMA, "CALENDAR_DATA"
    )
    holidays_df = holidays_df.loc[
        (holidays_df.location == location) & (holidays_df.holiday_type == holiday_type)
    ]
    holidays_df["date"] = pd.to_datetime(holidays_df["date"], format="%Y-%m-%d")
    return holidays_df


def update_holidays_datetime(
        df: pd.DataFrame, col_name: str
) -> pd.DataFrame:
    """
    Transforms date column from input holidays dataframe into pandas.Timestamps

    Parameters
    ----------
    df: pandas.DataFrame
        input holidays dataframe
    col_name: str
        name of date column from input dataframe

    Returns
    -------
    pandas.DataFrame
        updated holidays dataframe with pandas.Timestamps dtype for date column
    """
    df[col_name] = pd.to_datetime(df[col_name], format="%Y-%m-%d")
    return df.dropna(subset=[col_name])


def add_weekday_of_holidays(
        df: pd.DataFrame, date_column: str
) -> pd.DataFrame:
    """
    Adds holiday's weekday as a new column into input holidays dataframe.
    For non-holiday days, value of this new column is set to -1.

    Parameters
    ----------
    df: pandas.DataFrame
        input holidays dataframe
    date_column: str
        date column in input dataframe

    Returns
    -------
    pandas.DataFrame
        enriched holidays dataframe
    """

    mask = df["is_holiday"] == 1
    df.loc[mask, "holiday_day_of_week"] = df.loc[mask, date_column].dt.weekday
    df["holiday_day_of_week"] = df["holiday_day_of_week"].fillna(-1)
    return df


def add_holidays_count_in_week(
        df: pd.DataFrame, date_column: str, location_column: str = None
) -> pd.DataFrame:
    """
    Adds count of holidays per current week as a new column into input holidays dataframe.
    If location column is provided, will compute the holiday count per location.
    NB: value of this added field will be the same for all daily rows of a similar week

    Parameters
    ----------
    df: pandas.DataFrame
        input holidays dataframe
    date_column: str
        date column in input dataframe
    location_column: str
        if location column is given, groupby includes it as well
    Returns
    -------
    pandas.DataFrame
        enriched holidays dataframe
    """
    if location_column is None:
        holidays_count = df \
            .groupby(pd.Grouper(
                key=date_column, freq="W-MON", closed="left", label="left")
            )["is_holiday"] \
            .sum() \
            .reset_index() \
            .rename({"is_holiday": "holidays_count_in_week"}, axis=1)

        df = df.merge(
            holidays_count,
            on=date_column,
            how="left",
            validate="one_to_one"
        )
    else:
        holidays_count = df \
            .groupby([location_column, pd.Grouper(
                key=date_column, freq="W-MON", closed="left", label="left")
            ])["is_holiday"] \
            .sum() \
            .reset_index() \
            .rename({"is_holiday": "holidays_count_in_week"}, axis=1)

        df = df.merge(
            holidays_count,
            on=[date_column, location_column],
            how="left",
            validate="one_to_one"
        )
    df["holidays_count_in_week"] = df["holidays_count_in_week"].ffill()
    return df


def add_holidays_count_in_month(
        df: pd.DataFrame, date_column: str
) -> pd.DataFrame:
    """
    Adds count of holidays per current month as a new column into input holidays dataframe.
    NB: value of this added field will be the same for all daily rows of a similar month

    Parameters
    ----------
    df: pandas.DataFrame
        input holidays dataframe
    date_column: str
        date column in input dataframe

    Returns
    -------
    pandas.DataFrame
        enriched holidays dataframe
    """

    holidays_count = df \
        .groupby(pd.Grouper(
            key=date_column, freq="MS", closed="left", label="left")
        )["is_holiday"] \
        .sum() \
        .reset_index() \
        .rename({"is_holiday": "holidays_count_in_month"}, axis=1)

    df = df.merge(
        holidays_count,
        on=date_column,
        how="left",
        validate="one_to_one"
    )
    df["holidays_count_in_month"] = df["holidays_count_in_month"].ffill()
    return df
