import pandas as pd  # type: ignore
from typing import List
import databricks.koalas as ks  # type: ignore
from adf.utils.decorators import timeit
from adf.modelling.dataprep.utils.core import safe_merge


def extract_ean_from_iri(
        df: pd.DataFrame, product_column: str, new_ean_column: str
) -> pd.DataFrame:
    """
    Adds EAN column to input iri dataframe and removes initial product column

    Parameters
    ----------
    df: pandas.DataFrame
        input iri dataframe
    product_column: str
        initial product column of input iri dataframe
    new_ean_column: str
        name of new ean column to create

    Returns
    -------
    pandas.DataFrame
        updated iri dataframe
    """
    df[new_ean_column] = df[product_column].str.strip().str[-13:]
    return df.drop(product_column, axis=1)


def extract_rfa_from_iri(
    df: pd.DataFrame, rfa_column: str, new_rfa_column: str
) -> pd.DataFrame:
    """
    Adds rfa_cod column to input iri dataframe and removes initial rfa column

    Parameters
    ----------
    df: pandas.DataFrame
        input iri dataframe
    rfa_column: str
        initial rfa column of input iri dataframe
    new_rfa_column: str
        name of new ean rfa column to create

    Returns
    -------
    pandas.DataFrame
        updated iri dataframe
    """
    df[new_rfa_column] = df[rfa_column] \
        .str \
        .strip() \
        .str[:7]
    df[new_rfa_column] = pd.to_numeric(df[new_rfa_column], errors="coerce", downcast="integer")

    return df \
        .drop(rfa_column, axis=1) \
        .dropna(subset=[new_rfa_column], axis=0) \
        .astype({new_rfa_column: "int32"})


def extract_date_from_iri(
        df: pd.DataFrame, date_column: str
) -> pd.DataFrame:
    """
    Updates date column into %Y-%m-%d format of input iri dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        input iri dataframe
    date_column: str
        name of date column to update

    Returns
    -------
    pandas.DataFrame
        updated iri dataframe
    """

    df[date_column] = df[date_column].str.strip().str[7:17]
    df[date_column] = pd.to_datetime(df[date_column], format="%d-%m-%Y")
    return df


def resample_to_weekly(
        sellout_df: pd.DataFrame
) -> pd.DataFrame:
    """
    Resamples weekly input dataframe to add missing weeks, front-filling EAN and RFA column,
    leaving sellout volumes to NaN for added missing weeks.

    Parameters
    ----------
    sellout_df: pandas.DataFrame
        input iri dataframe at weekly level

    Returns
    -------
    pd.DataFrame
        enriched dataframe at weekly level
    """
    return ks \
        .from_pandas(sellout_df) \
        .groupby(["ean_cod", "rfa_cod"]) \
        .apply(resample_to_weekly_on_group) \
        .to_pandas() \
        .reset_index(drop=True)


def resample_to_weekly_on_group(
        product_sellout_df: pd.DataFrame
):
    """
    Resamples weekly sub-dataframe containing one unique EAN code and one unique RFA code,
    It adds missing weeks, front-filling EAN and RFA column, leaving sellout volumes to NaN
    for added missing weeks.

    Parameters
    ----------
    product_sellout_df: pandas.DataFrame
        input iri sub-dataframe at weekly level, containing one unique EAN code and one unique
        RFA code

    Returns
    -------
    pd.DataFrame
        enriched sub-dataframe at weekly level
    """
    product_sellout_df = product_sellout_df \
        .reset_index(drop=True) \
        .set_index("to_dt") \
        .resample("W-MON", label="left", closed="left") \
        .asfreq() \
        .reset_index()
    product_sellout_df.loc[:, ["ean_cod", "rfa_cod"]] = \
        product_sellout_df.loc[:, ["ean_cod", "rfa_cod"]].ffill()
    return product_sellout_df.astype({"rfa_cod": "int32"})


def aggregate_to_sku(
        df: pd.DataFrame, data_type: str, sku_column: str, ean_column: str
) -> pd.DataFrame:
    """
    Aggregates input iri dataframe from EAN to SKU level

    Parameters
    ----------
    df: pandas.DataFrame
        input iri dataframe
    data_type: str, {"sellout", "wd"}
        iri data source of input iri dataframe
    sku_column: str
        name of column containing SKU information in iri dataframe
    ean_column: str
        name of column containing EAN information in iri dataframe

    Returns
    -------
    pandas.DataFrame
        aggregated iri dataframe
    """

    sku_per_ean_count = df \
        .groupby(ean_column)[sku_column] \
        .nunique() \
        .reset_index() \
        .rename(columns={sku_column: "count_" + sku_column})
    df = df.merge(
        sku_per_ean_count,
        on=[ean_column],
        how="left",
        validate="many_to_one"
    )
    # Calculate final sellout/wd per SKU
    df[data_type] = df[data_type + "_ean"] / df["count_" + sku_column]
    df = df.drop("count_" + sku_column, axis=1)
    return df


def aggregate_iri_data_to_fu(
        df: pd.DataFrame, sku_fu_mapping: pd.DataFrame, data_type: str, sku_column: str,
        by: List[str]
) -> pd.DataFrame:
    """
    Adds FU information to input iri dataframe and then aggregates it from SKU to FU level

    Parameters
    ----------
    df: pandas.DataFrame
        input iri dataframe
    sku_fu_mapping: pandas.DataFrame
        input SKU/FU mapping dataframe
    data_type: str, {"sellout", "wd"}
        iri data source of input iri dataframe
    sku_column: str
        name of column containing SKU information in iri dataframe
    by: List[str]
        list of columns names to aggregate on
    Returns
    -------
    pandas.DataFrame
        aggregated iri dataframe
    """
    return df \
        .pipe(
            safe_merge,
            "iri",
            sku_fu_mapping,
            "sku_fu",
            warning_type="warning",
            on=sku_column,
            how="inner",
            validate="many_to_one") \
        .groupby(by) \
        .agg({data_type: "sum"}) \
        .reset_index()


def resample_sellout_to_daily(
        df: pd.DataFrame, data_type: str, fu_column: str,
        location_column: str, target_date_column: str
) -> pd.DataFrame:
    """
    Creates weekly and monthly iri values from iri input dataframe and resamples them to daily
    granularity

    Parameters
    ----------
    df: pandas.DataFrame
        input iri dataframe
    data_type: str, {"sellout", "wd"}
        iri data source of input iri dataframe
    fu_column: str
        name of FU column in iri dataframe
    location_column: str
        name of location column in iri dataframe
    target_date_column: str
        target date column in iri dataframe

    Returns
    -------
    pandas.DataFrame
        update iri dataframe at daily granularity with weekly and monthly values/features
    """

    df_daily = df \
        .set_index(target_date_column) \
        .groupby([fu_column, location_column])[data_type] \
        .resample("1D") \
        .pad() \
        .reset_index() \
        .rename({data_type: data_type + "_of_week"}, axis=1)

    df_monthly = df \
        .groupby(
            [
                fu_column,
                location_column,
                pd.Grouper(key=target_date_column, freq="MS", closed="left", label="left")
            ]
        )[data_type] \
        .sum() \
        .reset_index() \
        .rename({data_type: data_type + "_of_month"}, axis=1)

    result = df_daily.merge(
        df_monthly,
        on=[fu_column, location_column, target_date_column],
        how="left",
        validate="one_to_one"
    )
    result[data_type + "_of_month"] = result[data_type + "_of_month"].ffill()

    return result


@timeit
def aggregate_iri_data(
        df: pd.DataFrame, data_type: str, by: List[str]
) -> pd.DataFrame:
    """
    Aggregates input iri dataframe to required group_by level

    Parameters
    ----------
    df: pandas.DataFrame
        input iri dataframe
    data_type: str, {"sellout", "wd"}
        iri data source of input iri dataframe
    by: List[str]
        columns on which to group by before aggregating

    Returns
    -------
    pandas.DataFrame
        aggregated iri dataframe
    """
    if ("country_cod" in by) and ("country_cod" not in df.columns):
        df["country_cod"] = 0
    return df \
        .groupby(by) \
        .agg({data_type + "_of_week": "sum", data_type + "_of_month": "sum"}) \
        .reset_index()
