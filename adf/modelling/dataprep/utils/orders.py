import calendar
import logging
from datetime import datetime, timedelta, date
from typing import Union, List

import databricks.koalas as ks  # type: ignore
import numpy as np  # type: ignore
import pandas as pd  # type: ignore
from operator import attrgetter
from adf.errors import DataprepError
from adf.utils.decorators import timeit
from adf.types import DataFrame

log = logging.getLogger("adf")


def remove_innovations(
        df: pd.DataFrame, product_column: str, date_column: str,
        min_depth_history: int = 3, depth_unit: str = "M",
        view_from_date: Union[datetime, str] = datetime.today()
) -> pd.DataFrame:
    """
    Returns input dataframe removing products with less than a given history depth (innovation).
    In run mode, view_from will be set to execution date.

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe
    product_column: str
        target product column name for later forecasts to be made
    date_column: str
        target date column for later forecasts to be made
    min_depth_history: int, default is 3
        minimal depth history value not to be considered as innovation and removed
    depth_unit: str.
        temporal unit in which min_depth_history value is expressed. \
        Can either be: \
         - "D" for "days" \
         - "W" for "weeks" \
         - "M" for "months. \
         Default is "M".
    view_from_date: datetime or str of format %Y-%m-%d
        reference date from which to compare history depth

    Returns
    -------
    pandas.Dataframe
        updated cleaned orders dataframe after innovation have been removed from it.
    """

    assert depth_unit in ("D", "W", "M")
    if isinstance(view_from_date, str):
        view_from_date = datetime.strptime(view_from_date, "%Y-%m-%d")

    filter_df = df \
        .groupby(product_column)[date_column] \
        .min() \
        .reset_index() \
        .drop_duplicates()

    mask = (view_from_date - filter_df[date_column]) >= np.timedelta64(
        min_depth_history, depth_unit)
    products_to_keep = filter_df.loc[mask, product_column].unique()

    initial_products = len(df[product_column].unique())
    df = df.query(
        f'{product_column}.isin({list(products_to_keep)})', engine="python"
    )
    final_products = len(df[product_column].unique())
    log.info(f"Removed innovations: {initial_products - final_products}")
    return df


def add_future_orders_of_n_previous_days(
        df: pd.DataFrame,
        target_date: str,
        order_creation_date: str,
        target_quantity: str,
        interval: Union[int, List[int], range]
) -> pd.DataFrame:
    """
    For each time horizon element n in 'interval' parameter, this method creates a column filled
    with 'target quantity' value if an order (~ row of input dataframe) was created and known
    n days before specified 'target date'.

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe
    target_date: str
        name of "target date" column in df
    order_creation_date: str
        name of "order creation date" column in df
    target_quantity: str
        name of "target quantity" column in df
    interval: int, or List[int], or range
        time horizon(s) n on which to generate the feature (future orders registered
        n days before target date) for.

    Returns
    -------
    pandas.DataFrame
        enriched dataframe with future orders features
    """

    if isinstance(interval, int):
        interval = [interval]

    for n in interval:
        if n < 0:
            raise DataprepError(
                "Future orders creation: number of previous days must be positive"
            )

        before_n_days = (df[target_date] - df[order_creation_date]) > (timedelta(days=n))

        if n == 0:
            col_name = "future_" + target_quantity + "_until_current_day"
        elif n == 1:
            col_name = "future_" + target_quantity + "_until_previous_day"
        else:
            col_name = "future_" + target_quantity + "_until_previous_" + str(n) + "_days"

        df[col_name] = df \
            .loc[before_n_days, target_quantity] \
            .fillna(0)

    return df


@timeit
def add_future_orders_until_weekday_of_n_previous_weeks(
        df: DataFrame,
        target_date: str,
        order_creation_date: str,
        target_quantity: str,
        interval: Union[int, List[int], range],
        until_weekday_included: int,
        add_weekday_in_column_name: bool = True
) -> DataFrame:
    """
    For each time horizon element n in 'interval' parameter, this method creates a column filled
    with 'target quantity' value if an order (~ row of input dataframe) was created and known
    n weeks before specified target date's week or during target date's week before a specified
    weekday (included).

    Parameters
    ----------
    df: DataFrame
        input orders dataframe
    target_date: str
        name of "target date" column in df
    order_creation_date: str
        name of "order creation date" column in df
    target_quantity: str
        name of "target quantity" column in df
    interval: int, or List[int], or range
        time horizon(s) n on which to generate the feature for
    until_weekday_included: str
        weekday index until which future orders are considered in target date's week (included). \
        (index 0 is for Monday, 6 is for Sunday).
        NB: given this, you should pay attention not to use future orders features in case your
        pipeline is launched on a Monday.
        # TODO: Refactoring to make it possible
    add_weekday_in_column_name: bool. Default is True
        indicates whether to add weekday in generated future orders columns' names or not.

    Returns
    -------
    DataFrame
        enriched dataframe with future orders features
    """

    if isinstance(interval, int):
        interval = [interval]

    for col in [target_date, order_creation_date]:
        df[col + "_mondays"] = df.apply(
            lambda x: x[col] - timedelta(days=x[col].weekday()), axis=1
        )

    for n in interval:
        df = add_future_orders_until_weekday_of_n_previous_weeks_for_interval(
            df,
            n,
            target_date,
            order_creation_date,
            target_quantity,
            until_weekday_included,
            add_weekday_in_column_name
        )

    df = df.drop([target_date + "_mondays", order_creation_date + "_mondays"], axis=1)

    return df


@timeit
def add_future_orders_until_weekday_of_n_previous_weeks_for_interval(
        df: DataFrame,
        n: int,
        target_date: str,
        order_creation_date: str,
        target_quantity: str,
        until_weekday_included: int,
        add_weekday_in_column_name: bool = True
):
    if n < 0:
        raise DataprepError(
            "Future orders creation: number of previous weeks must be positive"
        )

    diff = df[target_date + "_mondays"] - df[order_creation_date + "_mondays"]
    if isinstance(df, ks.DataFrame):
        before_n_week = (diff > timedelta(days=7 * n).total_seconds())
        n_week = (diff == timedelta(days=7 * n).total_seconds())
    else:
        before_n_week = (diff > timedelta(days=7 * n))
        n_week = (diff == timedelta(days=7 * n))
    until_weekday = (
        df[order_creation_date].astype('datetime64[ns]').dt.weekday <= until_weekday_included
    )

    mask = before_n_week | (n_week & until_weekday)

    if add_weekday_in_column_name:
        weekday_name = get_weekday_name(until_weekday_included)
    else:
        weekday_name = "previous_day"

    if n == 0:
        col_name = "future_" + target_quantity + "_until_" + weekday_name + "_of_current_week"
    elif n == 1:
        col_name = "future_" + target_quantity + "_until_" + weekday_name + "_of_previous_week"
    else:
        col_name = "future_" + target_quantity + "_until_" + weekday_name + "_of_previous_" \
                   + str(n) + "_weeks"

    df[col_name] = 0
    df[col_name] = df[col_name].mask(mask, df[target_quantity]).fillna(0)
    return df


def get_weekday_name(
        weekday: int
) -> str:
    """
    Returns weekday name for a given weekday index.
    For weekday = 0, this methods will return "monday". For weekday = 6, it will return "sunday"

    Parameters
    ----------
    weekday: int
        index to transform into weekday name

    Returns
    -------
    str
        weekday name in lower cases
    """
    weekday_name = calendar.day_name[weekday].lower()
    return weekday_name


def add_future_orders_until_day_of_month_of_n_previous_months(
        df: pd.DataFrame,
        target_date: str,
        order_creation_date: str,
        target_quantity: str,
        interval: Union[int, List[int], range],
        until_day_of_month_included: int
) -> pd.DataFrame:
    """
    For each time horizon element n in 'interval' parameter, this method creates a column filled
    with 'target quantity' value if an order (~ row of input dataframe) was created and known
    n months before specified target date's month or during target date's month before a
    specified month day (included).

    NB: For backtesting, until_month_day_included must be fixed. For live, \
    until_month_day_included can be month_day of execution date.

    Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe
    target_date: str
        name of "target date" column in df
    order_creation_date: str
        name of "order creation date" column in df
    target_quantity: str
        name of "target quantity" column in df
    interval: int, or List[int], or range
        time horizon(s) n on which to generate the feature for
    until_day_of_month_included: int
        reference date (~ view from) until which future orders are considered in target date's \
        month (excluded)

    Returns
    -------
    pandas.DataFrame
        enriched dataframe with future orders features
    """

    if isinstance(interval, int):
        interval = [interval]

    for col in [target_date, order_creation_date]:
        df[col + "_first_day_of_month"] = df[col] - (df[col].dt.day - 1).astype('timedelta64[D]')

    for n in interval:

        if n < 0:
            raise DataprepError(
                "Future orders creation: number of previous months must be positive"
            )

        diff = (
            df[target_date + "_first_day_of_month"].dt.to_period("M") -
            df[order_creation_date + "_first_day_of_month"].dt.to_period("M")
        ).apply(attrgetter("n"))

        before_n_month = (diff > n)
        n_month = (diff == n)
        until_day_of_month = df[order_creation_date].dt.day <= until_day_of_month_included

        mask = before_n_month | (n_month & until_day_of_month)

        if n == 0:
            col_name = "future_" + target_quantity + "_until_day_" + \
                       str(until_day_of_month_included) + "_of_current_month"
        elif n == 1:
            col_name = "future_" + target_quantity + "_until_day_" + \
                       str(until_day_of_month_included) + "_of_previous_month"
        else:
            col_name = "future_" + target_quantity + "_until_day_" + \
                       str(until_day_of_month_included) + "_of_previous_" + str(n) + "_months"

        df[col_name] = df \
            .loc[mask, target_quantity] \
            .fillna(0)

    df = df.drop(
        [
            target_date + "_first_day_of_month",
            order_creation_date + "_first_day_of_month"
        ],
        axis=1
    )

    return df


def remove_products_not_in_last_min_depth_history(
        df: pd.DataFrame, product_column: str, date_column: str,
        min_depth_history: int = 3
) -> pd.DataFrame:
    """
    Removes products that are not listed in the last min_depth_history weeks
    starting from today

        Parameters
    ----------
    df: pandas.DataFrame
        input orders dataframe
    product_column: str
        target product column name for later forecasts to be made
    date_column: str
        target date column for later forecasts to be made
    min_depth_history: int, default is 3 weeks
        minimal depth history value to consider to remove the product

    """

    mask = ((df[date_column] >= pd.to_datetime(date.today() - timedelta(min_depth_history * 7))) &
            (df[date_column] <= date.today().strftime("%Y-%m-%d")))

    products_in_depth_history = df.loc[mask, product_column].unique()
    initial_products = df[product_column].unique()

    products_to_remove = set(initial_products) - set(products_in_depth_history)

    df = df.query(
        f'{product_column} not in ({list(products_to_remove)})', engine="python"
    )
    log.info(f"Removed skus: {len(products_to_remove)}")
    return df
