import pandas as pd  # type: ignore
from adf.modelling.tools import get_dask_df, get_snowflake_df
from adf.services.database import get_session, BusinessUnits
from adf.config import (
    SNOWFLAKE_MAPPING_COUNTRIES,
    SNOWFLAKE_MAPPING_DIVISIONS,
    SNOWFLAKE_MAPPING_COUNTRIES_TABLE
)


def apply_plc(
    country: str, division: str,
    df: pd.DataFrame, source: str = "dbfs"
) -> pd.DataFrame:
    scope = get_core_plc(country, division, source)
    renovation_products = []
    delisted_products = []
    scope_reno = scope.copy().dropna(subset=["phasein_start_date"])
    scope_reno = scope_reno.loc[scope_reno.mat_cod != scope_reno.mat_cod_reference]

    for product in scope_reno['mat_cod'].unique():
        reference_product = scope_reno.loc[
            (scope_reno['mat_cod'] == product), 'mat_cod_reference'
        ].values[0]

        phasein_end_date = scope_reno.loc[
            scope_reno['mat_cod'] == product, 'phasein_end_date'
        ]

        if ((phasein_end_date - pd.to_datetime('today')) < pd.Timedelta(0)).values[0]:
            df.loc[df.mat_cod == reference_product, "mat_cod"] = product
        else:
            renovation_products.append(reference_product)
            reno = df.loc[df.mat_cod == reference_product].copy()
            reno.mat_cod = product
            df = df.append(reno, ignore_index=True)

    scope_delisting = scope.copy().dropna(subset=["phaseout_end_date"])
    scope_delisting = scope_delisting.loc[
        scope_delisting.mat_cod == scope_delisting.mat_cod_reference
    ]

    for product in scope_delisting['mat_cod'].unique():
        delisting_end = scope.loc[
            scope['mat_cod'] == product, 'phaseout_end_date'
        ]
        if (
            ((delisting_end - pd.to_datetime('today')) < pd.Timedelta(0)).values[0]
        ) and (
            product not in renovation_products
        ):
            delisted_products.append(product)

    ibp_delisted_products = get_ibp_delisted_products(country, division, source)
    df = df[(~df.mat_cod.isin(delisted_products)) | (~df.mat_cod.isin(ibp_delisted_products))]

    return df


def get_core_plc(country: str, division: str, source: str = "dbfs") -> pd.DataFrame:
    if source == "dbfs":
        forecast_dates = get_dask_df(country, division, "plc_forecast_dates").compute()
        reference_products = get_dask_df(country, division, "plc_reference_products").compute()
    else:
        forecast_dates = get_snowflake_df(
            "DEV_VCP",
            f"VCP_DSP_AFC_{SNOWFLAKE_MAPPING_DIVISIONS[division]}_"
            f"{SNOWFLAKE_MAPPING_COUNTRIES[country]}",
            f"R_IBP_{SNOWFLAKE_MAPPING_DIVISIONS[division]}_"
            f"{SNOWFLAKE_MAPPING_COUNTRIES_TABLE[country]}_PRO_LFC_FOR_DAT"
        )
        reference_products = get_snowflake_df(
            "DEV_VCP",
            f"VCP_DSP_AFC_{SNOWFLAKE_MAPPING_DIVISIONS[division]}_"
            f"{SNOWFLAKE_MAPPING_COUNTRIES[country]}",
            f"R_IBP_{SNOWFLAKE_MAPPING_DIVISIONS[division]}_"
            f"{SNOWFLAKE_MAPPING_COUNTRIES_TABLE[country]}_PRO_LFC_REF_PRO"
        )
    reference_products = reference_products.query('is_active == "true"')
    forecast_dates = forecast_dates.loc[
        forecast_dates["launch_dimension"] == get_sales_organization_cod(
            country, division
        ).rjust(4, '0')
    ]
    scope = reference_products.merge(forecast_dates, on=["mat_cod"], how="left")
    return scope


def get_ibp_delisted_products(country: str, division: str, source: str = "dbfs") -> list:
    if source == "dbfs":
        ibp_master_data = get_dask_df(country, division, "ibp_master_data").compute()
    else:
        ibp_master_data = get_snowflake_df(
            "DEV_VCP",
            f"VCP_DSP_AFC_{SNOWFLAKE_MAPPING_DIVISIONS[division]}_"
            f"{SNOWFLAKE_MAPPING_COUNTRIES[country]}",
            f"R_IBP_{SNOWFLAKE_MAPPING_DIVISIONS[division]}_"
            f"{SNOWFLAKE_MAPPING_COUNTRIES_TABLE[country]}_MAT_STS"
        )
    ibp_delisted_products = list(ibp_master_data.query(
        "product_activation_cod == 'Z3' and product_aggregation == 'STOP ALGORITHM' "
        "and innovation_status != 'GPS'"
    ).mat_cod.unique())
    return ibp_delisted_products


def get_sales_organization_cod(country: str, division: str) -> str:
    return get_session().query(BusinessUnits).filter(
        BusinessUnits.country == country
    ).filter(
        BusinessUnits.division == division
    ).first().sales_org_cod
