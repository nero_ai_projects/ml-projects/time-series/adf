from typing import List
import numpy as np  # type: ignore
import pandas as pd  # type: ignore


def add_customers_count_to_promos(
        df: pd.DataFrame, groupby_cols: List[str], cus_cod_col: str, drop: bool = True
) -> pd.DataFrame:
    """
    Adds a column containing the number of unique cus_cod after grouping by input dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        input promotions dataframe
    groupby_cols: List[str]
        columns on which to aggregate to count cus_cod unique occurrences
    cus_cod_col: str
        name of column containing cus_cods
    drop: bool, default is True
        whether or not to drop cus_cod_col from input dataframe (removing duplicates)

    Returns
    -------
    pandas.DataFrame
        enriched promotions dataframe
    """
    df["cus_count"] = df \
        .groupby(groupby_cols)[cus_cod_col] \
        .transform("nunique")

    if drop:
        return df \
            .drop(cus_cod_col, axis=1) \
            .drop_duplicates()
    else:
        return df


def expand_promos_to_daily(
        df: pd.DataFrame, promo_start_date_column: str,
        promo_end_date_column: str, target_date_col: str
) -> pd.DataFrame:
    """
    Creates a target_date_col column and adds a row for each dates between promo_start_date_column
    and promo_end_date_column into input promotions df

    Parameters
    ----------
    df: pandas.DataFrame
        input promotions dataframe
    promo_start_date_column: str
        name of column containing the start date information for each promotion
    promo_end_date_column: str
        name of column containing the end date information for each promotion
    target_date_col: str
        name of target date column to create in input dataframe

    Returns
    -------
    pandas.DataFrame
        updated dataframe
    """
    df["time_delta"] = df[promo_end_date_column] - df[promo_start_date_column]
    max_delta = max(df.time_delta.dt.days)
    elapsed_times = pd.concat([
        pd.DataFrame(
            {
                "elapsed_time": np.arange(0, delta + 1)
            }
        ).assign(
            time_delta=pd.to_timedelta(delta, unit="d")
        )
        for delta in range(max_delta + 1)
    ])
    df = pd.merge(df, elapsed_times, on="time_delta")
    df[target_date_col] = df[promo_start_date_column] + pd.to_timedelta(df.elapsed_time, unit='d')
    df = df.loc[df[target_date_col] <= df[promo_end_date_column]]
    df = df.drop(
        columns=["time_delta", promo_start_date_column, promo_end_date_column]
    )
    return df.drop_duplicates()


def add_period_means(uplift_df: pd.DataFrame, product_column: str,
                     location_column: str, promo_types_cols: List[str]) -> pd.DataFrame:
    """Add additional columns containing orders' means generated for
    each promo type on the period
    Parameters
    ----------
    uplift_df : pd.DataFrame
        input uplift dataframe,
        must contain promo_types_cols columns, representing volumes associated to
        each type of promotions
    product_column : str
        target product column, depending on the business unit
    location_column : str
        target location column, depending on the business unit
    promo_types_cols : List[str]
        columns containing volumes associated to each type of promotions
    Returns
    -------
    pd.DataFrame
        enriched uplift dataframe
    """
    for column in promo_types_cols:
        mean_df = uplift_df.loc[uplift_df[column] > 0]
        mean_df = mean_df.groupby([product_column, location_column])[column] \
            .mean().reset_index()
        mean_df = mean_df.rename(columns=({column: f"{column}_period_mean"}))
        uplift_df = uplift_df.merge(
            mean_df,
            on=[product_column, location_column],
            how="left"
        )
    return uplift_df
