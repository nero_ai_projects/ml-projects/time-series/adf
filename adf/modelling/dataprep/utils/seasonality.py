import pandas as pd  # type: ignore


def add_days_before_end_of_month(
        df: pd.DataFrame, date_column: str
) -> pd.DataFrame:
    """
    Adds a column to input dataframe containing the number of days before the end of month for dates
    elements contained in date_column of input dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        input dataframe
    date_column: str
        dates column of input dataframe for which to compute the number of days before end of month

    Returns
    -------
    pandas.DataFrame
        enriched dataframe
    """
    df["days_before_end_of_month"] = df[date_column].dt.daysinmonth - df[date_column].dt.day
    return df
