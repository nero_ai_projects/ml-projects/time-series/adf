import numpy as np  # type: ignore
import pandas as pd  # type: ignore
from adf.utils.decorators import timeit


@timeit
def process_weather_data(
        df: pd.DataFrame, target_date_column: str
) -> pd.DataFrame:
    """
    Creates additional day length, and temperature spread column into input weather dataframe,
    drops unused columns and renames date column with required date_column name

    Parameters
    ----------
    df: pandas.DataFrame
        input weather dataframe
    target_date_column: str
        new name to give to date column in input weather dataframe

    Returns
    -------
    pandas.DataFrame
        updated weather dataframe
    """
    df.columns = df.columns.str.lower()
    df["daylength"] = df["sunsettime"] - df["sunrisetime"]
    df = df.drop(["sunsettime", "sunrisetime"], axis=1)
    df["temperaturespread"] = (df["apparenttemperaturemax"] - df["apparenttemperaturemin"])
    return df\
        .rename({"date": target_date_column}, axis=1)\
        .drop("time", axis=1)


@timeit
def aggregate_weather(
        df: pd.DataFrame, location_column: str, date_column: str
) -> pd.DataFrame:
    """
    Aggregates numerical weather features, taking their means, to the required location x date
    level

    Parameters
    ----------
    df: pandas.DataFrame
        input weather dataframe
    location_column: str
        name of target location column
    date_column: str
        name of target date column

    Returns
    -------
    pandas.DataFrame
        aggregated weather dataframe
    """

    agg = {
        k: "mean" for k in df.columns
        if k not in ("department_cod", "region_cod", location_column, date_column) and
        df[k].dtype in (np.float64, np.int64)
    }

    if (location_column == "country_cod") and (location_column not in df.columns):
        df.insert(1, location_column, 0)

    return df \
        .groupby([location_column, date_column]) \
        .agg(agg) \
        .reset_index()
