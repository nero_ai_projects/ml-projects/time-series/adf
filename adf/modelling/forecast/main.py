import logging
import traceback
from uuid import UUID
from typing import Union, Dict, Any, List, Optional
from sqlalchemy.orm import Session  # type: ignore
import databricks.koalas as ks  # type: ignore
from tabulate import tabulate
from adf import utils
from adf.enums import Indexes, Index
from adf.services import storage
from adf.errors import ForecastError
from adf.modelling import mllogs
from adf.services.database.query.reports import get_report, build_report, update_report
from adf.modelling.forecast.tools.accuracy import calculate_global_accuracy
from adf.modelling.forecast.tools.forecaster import Forecaster
from adf.modelling.forecast.tools.formatter import format_output
from adf.modelling.tools import split
from adf.services.database import ForecastReports
from adf.services.database import TrainingReports


log = logging.getLogger("adf")


def run(
    config: Dict,
    training_id: Union[str, UUID],
    input_mount: str,
    input_blob: str,
    filters_config: list,
    kpis: bool,
    splits_config: Optional[List[Dict]] = None,
    **context,
) -> ForecastReports:
    """Program that takes as input a configuration file describing the
    model to load and the data to make predictions on, and output
    forecasts on a path
    """
    session = context["session"]
    algorithm_type = context.get("algorithm_type", None)

    log.info("Building Forecaster")
    forecaster = Forecaster(config)

    log.info("Loading training reports and models")

    base_report = get_report(session, training_id, TrainingReports)
    mllogs.tag_report(base_report)

    forecaster.merge_config(base_report.description)
    mllogs.log_json_artifact(config, "config.json")

    if base_report.status == "processing":
        model_list = forecaster.models._config
    else:
        model_list = [
            setup_prediction_material(session, model) for model in forecaster.models._config
        ]

    log.info("Building report")
    report = build_report(
        session,
        ForecastReports,
        description=forecaster._config,
        training_report=base_report,
        input_mount=input_mount,
        input_blob=input_blob,
        pipeline_id=context.get("pipeline_id", None),
    )

    log.info("Loading data")
    df = storage.read_parquet(
        input_mount,
        input_blob,
        object_type=forecaster.get("object_type", "pandas"),
        engine=forecaster.get("engine_type", "auto"),
    )
    if not set(Indexes).issubset(df.columns):
        raise ValueError("Input DataFrame doesn't have expected Indexes")

    if isinstance(df, ks.DataFrame):
        df[Index.time.value] = ks.to_datetime(df[Index.time.value])
        if config.get("partitioned_on", None):
            df[config.get("partitioned_on", None)] = df[config.get("partitioned_on", None)].astype(
                int
            )
    if filters_config is None:
        filters_config = []
    if "forecast_product_filters" in config:
        log.info("Applying filters")
        filters_config.extend(config["forecast_product_filters"])

    filters = utils.Filters(filters_config)
    df = filters.filter(df)
    mllogs.log_shape(df)
    mllogs.log_json_artifact(filters_config, "filters.json")

    df = df.set_index(Indexes)
    if splits_config:
        log.info(f"Forecasting on splits : {splits_config}")
        df = split(df, forecaster.time.level, splits_config)
        if not len(df):
            return report

    mllogs.log_shape(df)
    forecaster.validate_forecast_dates(base_report.description["train_dates"])

    df[list(df.select_dtypes("category").columns)] = df[
        list(df.select_dtypes("category").columns)
    ].astype(str)
    mllogs.tag_report(report)

    try:
        log.info("Forecasting")
        forecast_df = forecaster.forecast(
            str(report.id), model_list, df, filters_config, algorithm_type
        )

        mllogs.log_shape(forecast_df)
        log.info("Formatting output")
        forecast_df = format_output(forecast_df, report, forecaster, kpis)
        mllogs.log_shape(forecast_df)

        log.info("Saving data")
        report.upload(forecast_df)
        update_report(session, report, "success")
        mllogs.tag_report_output(report)

        global_output = calculate_global_accuracy(
            forecast_df, forecaster.get("time")["level"], kpis
        )
        log.info(
            "\n" + tabulate(global_output, headers="keys", tablefmt="fancy_grid", showindex=False)
        )
        mllogs.log_accuracy(global_output)

        return report

    except Exception:
        message = traceback.format_exc()
        update_report(session, report, "failed", message)
        raise ForecastError(message)

    finally:
        mllogs.tag_report_status(report)


def setup_prediction_material(session: Session, model_config: Dict) -> Dict:
    """Get a model and its config from a report, return those in a dict"""
    log.info("Loading additional training report %s, config and model", model_config["id"])
    _, predictor = get_training_artifacts(session, model_config["id"], model_config["type"])
    upper_quantile_model, lower_quantile_model = None, None
    if model_config.get("uncertainty_upper_id") and model_config.get("uncertainty_lower_id"):
        _, upper_quantile_model = get_training_artifacts(
            session, model_config["uncertainty_upper_id"], model_config["type"]
        )
        _, lower_quantile_model = get_training_artifacts(
            session, model_config["uncertainty_lower_id"], model_config["type"]
        )
    prediction_dict = create_prediction_dict(
        predictor, upper_quantile_model, lower_quantile_model, model_config
    )
    return prediction_dict


def get_training_artifacts(session: Session, training_id: Union[str, UUID], model_type: str) -> Any:
    report = get_report(session, training_id, TrainingReports)
    predictor = report.download(model_type=model_type)
    return report, predictor


def create_prediction_dict(
    model: Any, upper_quantile_model: Any, lower_quantile_model: Any, config: Dict
) -> Dict[str, Any]:
    return {
        "model": model,
        "upper_quantile_model": upper_quantile_model,
        "lower_quantile_model": lower_quantile_model,
        "parameters": config,
    }


def is_inno_config(forecaster: Forecaster) -> bool:
    if "predict_time_after_launch" in forecaster._config:
        if len(forecaster.predict_from_dates) == 0:
            return True
        else:
            raise ValueError(
                "Predict_time_after_launch was given in the config,"
                " predict_from_dates must be empty"
            )
    return False
