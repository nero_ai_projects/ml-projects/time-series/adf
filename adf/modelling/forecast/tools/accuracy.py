from typing import Union
import pandas as pd  # type: ignore


def calculate_accuracy(forecast_df: pd.DataFrame) -> pd.DataFrame:
    """
    Adds errors, forecast_accuracy and bias columns
    """
    return forecast_df \
        .pipe(prepare_forecast_df, quartiles=False) \
        .pipe(calculate_error) \
        .pipe(add_forecast_accuracy) \
        .pipe(add_forecast_bias) \
        .pipe(add_uncertainty)


def calculate_global_accuracy(
        forecast_df: pd.DataFrame, time_level: str, kpis: bool
) -> pd.DataFrame:
    """
    Creates a new accuracy df aggregated on day, week or month gap depending on time base,
    including performance KPIs or not
    """
    if kpis:
        return forecast_df\
            .copy() \
            .pipe(prepare_forecast_df, quartiles=False)\
            .pipe(calculate_error)\
            .pipe(add_interval_coverage) \
            .pipe(aggregate_data, time_level, kpis)\
            .pipe(add_forecast_accuracy)\
            .pipe(add_forecast_bias)\
            .pipe(pd.DataFrame.reset_index)
    else:
        return forecast_df\
            .copy() \
            .pipe(prepare_forecast_df, quartiles=True) \
            .pipe(aggregate_data, time_level, kpis) \
            .pipe(pd.DataFrame.reset_index)


def calculate_global_accuracy_pred_uncertainty(
        forecast_df: pd.DataFrame, time_base_column="week_gap"
) -> pd.DataFrame:
    """
    Creates a new accuracy df aggregated on day_gap
    """
    return forecast_df \
        .copy() \
        .pipe(aggregate_data_prediction_date_uncertainty, time_base_column=time_base_column) \
        .pipe(add_forecast_accuracy) \
        .pipe(add_forecast_bias) \
        .pipe(pd.DataFrame.reset_index)


def calculate_global_scorecard_quartiles(
        forecast_df: pd.DataFrame, time_base_column="week_gap"
) -> pd.DataFrame:
    """
    Creates a new accuracy df aggregated on day_gap
    """
    return forecast_df \
        .copy() \
        .pipe(aggregate_data_prediction_date_quartiles, time_base_column=time_base_column) \
        .pipe(add_forecast_accuracy) \
        .pipe(add_forecast_bias) \
        .pipe(pd.DataFrame.reset_index)


def prepare_forecast_df(
        df: pd.DataFrame, suffix: str = "", quartiles: bool = False
) -> pd.DataFrame:
    """
    Rounds predictions values and deals with negative numbers
    Creates quartiles based on the ground truth
    """
    df["prediction" + suffix] = df["prediction" + suffix].clip(0)
    df["prediction" + suffix] = df["prediction" + suffix].round(4)
    if quartiles:
        try:
            df["quartiles"] = pd.qcut(
                df.ground_truth, q=4,
                labels=["quartile_1", "quartile_2", "quartile_3", "quartile_4"],
                duplicates='drop'
            )
        except ValueError:
            df["quartiles"] = pd.qcut(
                df.ground_truth, q=4,
                labels=["quartile_1", "quartile_2", "quartile_3"],
                duplicates='drop'
            )
        df['quartiles'] = df['quartiles'].astype(str)
    return df


def calculate_error(
        df: pd.DataFrame, suffix: str = ""
) -> pd.DataFrame:
    """
    Calculates columns error and error_abs
    """
    df["error" + suffix] = df["prediction" + suffix] - df["ground_truth"]
    df["error_abs" + suffix] = df["error" + suffix].abs()
    return df


def calculate_range(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Computes range
    """
    if {"min_prediction", "max_prediction"}.issubset(df.columns):
        df["range"] = ((df["max_prediction"] - df["min_prediction"]).round(2)).abs()
    else:
        raise ValueError("Missing columns min and max predictions")
    return df


def add_normalized_range(df):
    """
    Add normalized range column
    """
    df["norm_range"] = (df.apply(normalize_range, axis=1)).round(2)
    return df


def normalize_range(
        row: pd.Series
) -> int:
    """
    Normalizes range with ground truth
    """
    if row["ground_truth"] != 0:
        return abs(row["range"] / row["ground_truth"])
    return 1


def add_mean_interval_score(
        df: pd.DataFrame, alpha: float
) -> pd.DataFrame:
    df["mean_interval_score"] = df \
        .apply(calculate_mean_interval_score, alpha=alpha, axis=1) \
        .round(2)
    return df


def calculate_mean_interval_score(
        row: pd.Series, alpha: float
) -> int:
    if row["ground_truth"] != 0:
        if row["ground_truth"] < row["min_prediction"]:
            return row["norm_range"] + abs(
                2 / alpha * (
                    row['min_prediction'] - row["ground_truth"]
                ) / row["ground_truth"])
        elif row["ground_truth"] > row["max_prediction"]:
            return row["norm_range"] + abs(
                2 / alpha * (
                    row['ground_truth'] - row["max_prediction"]
                ) / row["ground_truth"])
    else:
        if row["ground_truth"] < row["min_prediction"]:
            return row["norm_range"] + abs(
                2 / alpha * (row['min_prediction'] - row["ground_truth"]))
        elif row["ground_truth"] > row["max_prediction"]:
            return row["norm_range"] + abs(
                2 / alpha * (row['ground_truth'] - row["max_prediction"]))
    return 1


def aggregate_data(
        df: pd.DataFrame, time_level: str, kpis: bool
) -> pd.DataFrame:
    """
    Aggregate data on day_gap
    """
    columns = ["prediction", "max_prediction", "min_prediction"]
    time_column = {
        "day": "day_gap",
        "week": "week_gap",
        "month": "month_gap"
    }[time_level]

    if kpis:
        columns = ["ground_truth"] + columns + ["error", "error_abs"]
        coverage = df[[time_column] + ["interval_coverage"]].groupby(time_column).first()
        return df[[time_column] + columns]\
            .groupby(time_column)\
            .sum()\
            .merge(coverage, on=time_column)
    else:
        return df[[time_column] + columns]\
            .groupby(time_column)\
            .sum()


def aggregate_data_prediction_date_uncertainty(
        df: pd.DataFrame, time_base_column="week_gap"
) -> pd.DataFrame:
    """
    Aggregates data on prediction date
    """
    return df \
        .groupby(time_base_column) \
        .agg({"ground_truth": "sum",
              "prediction": "sum",
              "min_prediction": "sum",
              "max_prediction": "sum",
              "error": "sum",
              "error_abs": "sum",
              "normalized_range": "mean",
              "coverage": "mean",
              "mean_interval_score": "mean"})


def aggregate_data_prediction_date_quartiles(
        df: pd.DataFrame, time_base_column="week_gap"
) -> pd.DataFrame:
    """
    Aggregates data on prediction date and quartiles
    """
    try:
        df["quartiles"] = pd.qcut(
            df.ground_truth, q=4,
            labels=["quartile_1", "quartile_2", "quartile_3", "quartile_4"],
            duplicates='drop'
        )
    except ValueError:
        df["quartiles"] = pd.qcut(
            df.ground_truth, q=4,
            labels=["quartile_1", "quartile_2", "quartile_3"],
            duplicates='drop'
        )
    return df \
        .groupby([time_base_column, "quartiles"]) \
        .agg({"ground_truth": "sum",
              "prediction": "sum",
              "min_prediction": "sum",
              "max_prediction": "sum",
              "error": "sum",
              "error_abs": "sum",
              "normalized_range": "mean",
              "coverage": "mean",
              "mean_interval_score": "mean"})


def add_forecast_accuracy(
        df: pd.DataFrame, suffix: str = ""
) -> pd.DataFrame:
    """
    Adds forecast accuracy column
    """
    df["forecast_accuracy" + suffix] = df \
        .apply(calculate_forecast_accuracy, suffix=suffix, axis=1) \
        .round(4)
    return df


def calculate_forecast_accuracy(
        row: pd.Series, suffix: str = ""
) -> Union[int, float]:
    """
    Calculates forecast accuracy considering edge cases
    """
    if row["ground_truth"] > 0:
        return max(0, 1 - (row["error_abs" + suffix] / row["ground_truth"]))
    if row["prediction" + suffix] > 0:
        return 0
    return 1


def add_forecast_bias(
        df: pd.DataFrame, suffix: str = ""
) -> pd.DataFrame:
    """
    Adds bias column
    """
    df["bias" + suffix] = df.apply(calculate_forecast_bias, suffix=suffix, axis=1).round(4)
    return df


def calculate_forecast_bias(
        row: pd.Series, suffix: str = ""
) -> Union[int, float]:
    """
    Calculates bias considering edge cases
    """
    if row["ground_truth"] > 0:
        return row["error" + suffix] / row["ground_truth"]
    if row["prediction" + suffix] > 0:
        return row["prediction" + suffix]
    return 0


def rename_uncertainty_metric(df: pd.DataFrame) -> pd.DataFrame:
    df.rename(columns={"norm_range": "normalized_range",
                       "in_interval": "coverage"}, inplace=True)
    return df


def add_uncertainty(df: pd.DataFrame) -> pd.DataFrame:
    df["in_interval"] = df.apply(in_interval, axis=1)
    return df


def in_interval(row: pd.Series):
    return (row["ground_truth"] > row["min_prediction"]) & \
        (row["ground_truth"] < row["max_prediction"])


def add_interval_coverage(df: pd.DataFrame) -> pd.DataFrame:
    day_gaps_coverage = df.groupby("day_gap").\
        agg({"in_interval": "size", "day_gap": "count"})
    df["interval_coverage"] = df.apply(calculate_coverage, args=(day_gaps_coverage,), axis=1)
    return df


def calculate_coverage(row: pd.Series, day_gaps_coverage: pd.DataFrame) -> pd.DataFrame:
    coverage = day_gaps_coverage.loc[row["day_gap"]]
    return 100 / coverage["day_gap"] * coverage["in_interval"]
