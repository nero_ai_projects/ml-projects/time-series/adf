from typing import List, Dict, Iterator
import logging
from datetime import datetime, date
import pandas as pd  # type: ignore
import dask.dataframe as dd  # type: ignore
import databricks.koalas as ks  # type: ignore
from pyspark.sql.functions import lit, explode, array  # type: ignore
from pyspark.sql import types  # type: ignore
import pytz
from jsonschema import validate  # type: ignore
from dask import compute  # type: ignore
from dask import delayed  # type: ignore
from adf.config import load_config
from adf import utils
from adf.modelling.forecast.tools.prediction import get_horizons, predict_step
from adf.modelling.tools import get_target_date
from adf.enums import Index
from adf.types import DataFrame


log = logging.getLogger("adf")


class Forecaster(utils.Schema):

    SCHEMA = load_config("modelling/forecasting.json")
    GENERATOR_SCHEMA = load_config("modelling/generator.json")
    FORECAST_SCHEMA = load_config("forecast/dtypes.json")
    algorithm_type = None

    def __init__(self, config: Dict):
        super().__init__(config)
        validate(config.get("updating_functions"), self.GENERATOR_SCHEMA)

    def forecast(
        self, report_id: str, models: List[Dict],
        panel_df: DataFrame, filters_config: List, algorithm_type: str
    ) -> pd.DataFrame:
        """ Run predictions for all prediction_dates and horizons

        If the panel data is a dask dataframe, the predictions are optimized
        by splitting the prediction data into smaller dataframes based on the partitions

        Runs all partitions and horizons in parallel
        """
        min_date = min(self.prediction_dates).strftime("%Y-%m-%d")
        panel_df = panel_df.query(f'{Index.time.value} >= "{min_date}"')
        self.algorithm_type = algorithm_type
        futures = []
        horizons = get_horizons(models)
        if isinstance(panel_df, pd.DataFrame):
            for horizon in horizons:
                futures.append(delayed(predict_step)(
                    model_list=models, panel_df=panel_df,
                    conf=self._config, target_column=self.target,
                    filters_config=filters_config, prediction_dates=self.prediction_dates,
                    algorithm_type=algorithm_type, horizon=horizon, partition_number=0
                ))
            return pd.concat(compute(*futures))
        elif isinstance(panel_df, dd.DataFrame):
            for i, partition in enumerate(panel_df.partitions):
                log.info(f"Setting up parallelization for partition {i}")
                if len(partition):
                    for horizon in horizons:
                        futures.append(delayed(predict_step)(
                            model_list=models, panel_df=partition,
                            conf=self._config, target_column=self.target,
                            filters_config=filters_config, prediction_dates=self.prediction_dates,
                            algorithm_type=algorithm_type, horizon=horizon, partition_number=i
                        ))
            return pd.concat(compute(*futures))
        elif isinstance(panel_df, ks.DataFrame):
            panel_df = panel_df.reset_index().to_spark()
            panel_df = panel_df.withColumn(
                "horizon",
                array([lit(i) for i in range(len(models))])  # type: ignore
            )
            panel_df = panel_df.withColumn('horizon', explode(panel_df.horizon))
            schema = get_forecast_schema()
            partitioned_on = self.get("partitioned_on", None)
            method = self.forecast_for_partition(models, filters_config, algorithm_type)
            result = panel_df \
                .groupby(*(partitioned_on + ["horizon"]) if partitioned_on else "horizon") \
                .applyInPandas(method, schema=schema) \
                .toPandas()
            result["time_index"] = pd.to_datetime(result["time_index"])
            result["prediction_date"] = pd.to_datetime(result["prediction_date"])
            return result

    @property
    def prediction_dates(self) -> Iterator[datetime]:
        if len(self.predict_from_dates) == 0:
            yield pytz.utc.localize(get_target_date(
                datetime.combine(date.today(), datetime.min.time()), 0, self, self.algorithm_type
            ))
        else:
            for predict_date in self.predict_from_dates:
                yield pytz.utc.localize(datetime.strptime(predict_date, "%Y-%m-%d"))

    def validate_forecast_dates(self, train_dates: List[str]) -> None:
        validation = False
        if len(train_dates) > 1:
            validation = (min(self.prediction_dates) <= pytz.utc.localize(datetime.strptime(
                train_dates[1],
                "%Y-%m-%d"
            )))
        if validation:
            raise ValueError("Prediction date can't be inferior to the training end date")

    def forecast_for_partition(self, models, filters_config, algorithm_type):
        def pandas_udf_for_partition(df):
            horizon = list(df.horizon.unique())[0]
            partitioned_on = self.get("partitioned_on", None)
            partition_number = 0
            if partitioned_on:
                partition_number = [
                    list(value) for value in list(df[partitioned_on].drop_duplicates().values)
                ][0]
            df = df.drop(columns=["horizon"])
            return predict_step(
                model_list=models, panel_df=df,
                conf=self._config, target_column=self.target,
                filters_config=filters_config, prediction_dates=list(self.prediction_dates),
                algorithm_type=algorithm_type, horizon=horizon,
                partition_number=partition_number
            )
        return pandas_udf_for_partition


def get_forecast_schema():
    """ Returns the Spark Schema of the output of the forecast.
    """
    column_types = [
        types.StructField("product_index", types.IntegerType()),
        types.StructField("location_index", types.IntegerType()),
        types.StructField("customer_index", types.IntegerType()),
        types.StructField("time_index", types.DateType()),
        types.StructField("prediction_date", types.DateType()),
        types.StructField("prediction", types.DoubleType()),
        types.StructField("max_prediction", types.DoubleType()),
        types.StructField("min_prediction", types.DoubleType()),
        types.StructField("time_step", types.IntegerType()),
        types.StructField("ground_truth", types.DoubleType()),
        types.StructField("error", types.DoubleType()),
        types.StructField("error_abs", types.DoubleType()),
        types.StructField("forecast_accuracy", types.DoubleType()),
        types.StructField("bias", types.DoubleType()),
        types.StructField("in_interval", types.BooleanType())
    ]
    schema = types.StructType(column_types)
    return schema
