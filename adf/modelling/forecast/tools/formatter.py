import pandas as pd  # type: ignore
import numpy as np  # type: ignore
from adf import config
from adf.enums import Index
from adf.services.database.base import Base
import adf.modelling.forecast.main as forecast
from adf.modelling.forecast.tools.forecaster import Forecaster


def format_output(forecast_df: pd.DataFrame, report: Base, forecaster: Forecaster, kpis: bool) \
        -> pd.DataFrame:
    return forecast_df\
        .pipe(reset_timezone)\
        .pipe(rename_columns)\
        .pipe(set_utc)\
        .pipe(add_day_gap)\
        .pipe(add_week_gap, forecaster)\
        .pipe(add_month_gap, forecaster)\
        .pipe(add_metadata, forecaster, report)\
        .pipe(normalize, forecaster, kpis)


def reset_timezone(forecast_df) -> pd.DataFrame:
    for col in forecast_df.select_dtypes(include="datetimetz").columns:
        forecast_df[col] = forecast_df[col].dt.tz_localize(None)
    return forecast_df


def rename_columns(forecast_df) -> pd.DataFrame:
    return forecast_df.rename(columns={
        Index.product.value: "product_code",
        Index.location.value: "location_code",
        Index.customer.value: "customer_code",
        Index.time.value: "target_date",
    })


def set_utc(df: pd.DataFrame) -> pd.DataFrame:
    df["target_date"] = pd.to_datetime(df["target_date"], utc=True)
    df["prediction_date"] = pd.to_datetime(df["prediction_date"], utc=True)
    return df


def add_day_gap(df: pd.DataFrame) -> pd.DataFrame:
    """ add column describing the gap in days between the base
    predicting date and the target date
    """
    df["day_gap"] = (df["target_date"] - df["prediction_date"]).dt.days
    return df


def add_week_gap(df: pd.DataFrame, forecaster: Forecaster) -> pd.DataFrame:
    """ add column describing the gap in weeks between the base
    predicting date and the target date
    """
    if forecaster.time.level in ["week", "day"]:
        df["week_gap"] = np.floor(
            (
                df["target_date"] - (
                    df["prediction_date"] - pd.to_timedelta(
                        df["prediction_date"].dt.weekday, unit='D'
                    )
                )
            ) / np.timedelta64(1, 'W')
        )
    elif forecaster.time.level == "month":
        df["week_gap"] = np.floor(
            (
                df["target_date"] - (
                    df["prediction_date"] - pd.to_timedelta(
                        df["prediction_date"].dt.day, unit='D'
                    )
                )
            ) / np.timedelta64(1, 'W')
        )
    return df


def add_month_gap(df: pd.DataFrame, forecaster: Forecaster) -> pd.DataFrame:
    """ add column describing the gap in months between the base
    predicting date and the target date
    """
    if forecaster.time.level == "week":
        df["month_gap"] = np.floor(
            (
                df["target_date"] - (
                    df["prediction_date"] - pd.to_timedelta(
                        df["prediction_date"].dt.weekday, unit='D'
                    )
                )
            ) / np.timedelta64(1, 'M')
        )
    elif forecaster.time.level == "month":
        df["month_gap"] = np.floor(
            (
                df["target_date"] - (
                    df["prediction_date"] - pd.to_timedelta(
                        df["prediction_date"].dt.day, unit='D'
                    )
                )
            ) / np.timedelta64(1, 'M')
        )
    else:
        df["month_gap"] = np.floor(
            (df["target_date"] - df["prediction_date"]) / np.timedelta64(1, 'M')
        )

    return df


def add_metadata(forecast_df: pd.DataFrame, forecaster: Forecaster, report: Base) -> pd.DataFrame:
    """ Add metadata that will be used for the ibp export
    """
    division = f'{forecaster.tags.division}_inno' if forecast.is_inno_config(forecaster) \
        else forecaster.tags.division
    return forecast_df.assign(
        id=str(report.id),
        execution_date=report.created_at.isoformat(),
        product_level=forecaster.product.level,
        location_level=forecaster.location.level,
        customer_level=forecaster.customer.level,
        time_base=forecaster.time.level,
        country=forecaster.tags.country,
        division=division,
    )


def normalize(forecast_df: pd.DataFrame, forecaster: Forecaster, kpis: bool) -> pd.DataFrame:
    """ Ensure columns conform to standard
    """
    if forecast.is_inno_config(forecaster):
        dtypes = config.load_config("forecast/dtypes_inno.json")
    else:
        dtypes = config.load_config("forecast/dtypes.json")
    forecast_df = forecast_df[list(dtypes.keys())]

    if not kpis:
        for col in ["ground_truth", "in_interval", "forecast_accuracy", "bias"]:
            if col in forecast_df.columns:
                forecast_df[col] = np.nan

    for col, dtype in dtypes.items():
        if col in ("execution_date", "prediction_date", "target_date"):
            forecast_df[col] = forecast_df[col].values.astype(dtype)
        else:
            forecast_df[col] = forecast_df[col].astype(dtype)
    return forecast_df
