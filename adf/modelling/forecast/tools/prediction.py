import logging
from typing import List, Iterable, Dict, Tuple, Optional
from datetime import datetime
import pandas as pd  # type: ignore
import numpy as np  # type: ignore
import dask.dataframe as dd  # type: ignore
from sklearn.ensemble import RandomForestRegressor  # type: ignore
from adf import utils
from adf.enums import Index, Indexes
from adf.modelling.tools import get_target_date, find_all_features
from adf.modelling.forecast.tools.accuracy import calculate_accuracy
from adf.types import Predictions, Model


log = logging.getLogger("adf")


def predict_step(
    model_list: List[Dict], panel_df: pd.DataFrame, conf: Dict,
    target_column: str, filters_config: Dict, prediction_dates: List[datetime],
    algorithm_type: str, horizon: int, partition_number
) -> pd.DataFrame:
    """ This method:
    - Selects the model and features for the current step
    - Forecasts based on the data for the current step
    """
    if isinstance(panel_df, dd.DataFrame):
        panel_df = panel_df.compute()
    if panel_df.index.names != Indexes:
        panel_df[Index.time.value] = pd.to_datetime(panel_df[Index.time.value])
        panel_df = panel_df.set_index(Indexes)
    log.info(
        f"Starting prediction for partition {partition_number} and horizon {horizon}!"
    )
    conf_schema = utils.BaseSchema(conf)

    model, step_features, prediction_dict = init_predict(
        model_list, panel_df, conf_schema, horizon
    )
    scoped_panel_df = scope_panel_df(
        panel_df, horizon, conf_schema, algorithm_type, prediction_dates
    )
    if not len(scoped_panel_df):
        return get_empty_dataframe()
    prediction_df = get_prediction_data(
        scoped_panel_df, model, step_features, conf_schema, prediction_dict, horizon
    )
    if len(prediction_df):
        prediction_df = prediction_df.reset_index(level="prediction_date")
        prediction_df = add_ground_truth(prediction_df, panel_df, target_column)
        filters = utils.Filters(filters_config)
        prediction_df = filters.replace(prediction_df)
        prediction_df = calculate_accuracy(prediction_df)
        prediction_df = prediction_df.reset_index()
        return prediction_df
    else:
        return get_empty_dataframe()


def get_empty_dataframe():
    dtypes = np.dtype([
        ("product_index", int),
        ("location_index", int),
        ("customer_index", int),
        ("time_index", np.datetime64),
        ("prediction_date", np.datetime64),
        ("prediction", float),
        ("max_prediction", float),
        ("min_prediction", float),
        ("time_step", int),
        ("ground_truth", float),
        ("error", float),
        ("error_abs", float),
        ("forecast_accuracy", float),
        ("bias", float),
        ("in_interval", bool)
    ])
    data = np.empty(0, dtype=dtypes)
    return pd.DataFrame(data)


def init_predict(
    model_list: List[Dict], panel_df: pd.DataFrame, conf: utils.BaseSchema, horizon: int
) -> Tuple[Model, List[str], Dict]:
    """ Retrieves all relevant informations from the database data to forecast the current step
    """
    prediction_dict = pick_step_prediction_dict(model_list, horizon)
    model = prediction_dict["model"]
    model_features = find_all_features(
        panel_df, prediction_dict["parameters"]["specific_features"]
    )
    step_features = find_all_features(
        panel_df, conf._config["common_features"]
    ) + model_features
    return model, step_features, prediction_dict


def scope_panel_df(
    panel_df: pd.DataFrame, horizon: int, conf_schema: utils.BaseSchema,
    algorithm_type: str, prediction_dates: List[datetime]
) -> pd.DataFrame:
    """ Returns a subset of the panel data where only the data of the current step is selected
    """
    scoped_panel_dfs = []
    for prediction_date in prediction_dates:
        target_date = get_target_date(prediction_date, horizon, conf_schema, algorithm_type)
        scoped_panel_df = panel_df.reset_index()
        scoped_panel_df = scoped_panel_df.loc[
            scoped_panel_df[Index.time.value] == target_date.strftime("%Y-%m-%d")
        ].set_index(Indexes)
        scoped_panel_df = scoped_panel_df.assign(prediction_date=prediction_date)
        scoped_panel_df["prediction_date"] = pd.to_datetime(
            scoped_panel_df["prediction_date"], utc=True
        )
        scoped_panel_df = scoped_panel_df.set_index(["prediction_date"], append=True)
        scoped_panel_dfs.append(scoped_panel_df)
    return pd.concat(scoped_panel_dfs)


def get_prediction_data(
    scoped_panel_df: pd.DataFrame, model: Model,
    step_features: List[str], conf: utils.BaseSchema, prediction_dict: Dict, horizon: int
) -> pd.DataFrame:
    """ Selects the data for the current step, validates it, runs the forecast
    and the uncertainty if needed and formats the output.
    """
    df_to_predict = build_df_to_predict(
        scoped_panel_df, step_features, conf.target
    )
    validate_df_to_predict(df_to_predict, model)
    prediction_df = pd.DataFrame()
    if len(df_to_predict):
        predictions = model.predict(df_to_predict)
        upper_predictions, lower_predictions = predict_uncertainty(
            prediction_dict, df_to_predict
        )
        prediction_df = build_prediction_df(
            df_to_predict, predictions, upper_predictions, lower_predictions, horizon
        )
    return prediction_df


def add_ground_truth(forecast_df: pd.DataFrame, panel_df: pd.DataFrame, target: str) \
        -> pd.DataFrame:
    """ Merges forecast with original panel to retrieve ground truth
    """
    ground_truth = pd.DataFrame({"ground_truth": panel_df[target]})
    forecast_df = forecast_df.merge(
        ground_truth, left_index=True, right_index=True
    )
    return forecast_df


def build_df_to_predict(
    scoped_panel_df: pd.DataFrame, features: List[str], target: str
) -> pd.DataFrame:
    """ Returns df_to_predict keeping features specified in the config
    """
    cols_df_to_predict = features.copy()
    cols_df_to_predict.remove(target)
    df_to_predict = scoped_panel_df[cols_df_to_predict].copy()
    return df_to_predict


def build_prediction_df(
    reference: pd.DataFrame,
    predictions: Predictions,
    upper_predictions: Optional[Predictions],
    lower_predictions: Optional[Predictions],
    step: int
) -> pd.DataFrame:
    """ Build the prediction dataframe keeping index of reference
    """
    return pd.DataFrame(
        {
            "prediction": predictions,
            "max_prediction": upper_predictions if upper_predictions is not None else np.nan,
            "min_prediction": lower_predictions if lower_predictions is not None else np.nan,
            "time_step": step
        },
        index=reference.index
    )


def pick_step_prediction_dict(model_list: List[Dict], step: int) \
        -> Dict:
    """ Picks model and features to predict depending on the forecast step
    """
    for prediction_dict in model_list:
        if step in prediction_dict["parameters"]["prediction_steps"]:
            return prediction_dict
    raise Exception(f"Missing prediction_step for step {step}")


def validate_df_to_predict(df_to_predict: pd.DataFrame, model: Model) -> None:
    """ Validate prediction data based on model
    """
    if isinstance(model, RandomForestRegressor):
        if df_to_predict.isna().values.any():
            raise ValueError(
                "df_to_predict contains NA values which "
                "are not accepted by RandomForestRegressor"
            )


def get_horizons(models: List) -> Iterable[int]:
    """ Get horizon to predict on from models config
    """
    return [model["parameters"]["prediction_steps"][0] for model in models]


def predict_uncertainty(
    prediction_dict: Dict, df: pd.DataFrame
) -> Tuple[Optional[Predictions], Optional[Predictions]]:
    """ Upper and lower quantile prediction for creating an
    uncertainty interval
    """
    upper_quantile_model = prediction_dict["upper_quantile_model"]
    lower_quantile_model = prediction_dict["lower_quantile_model"]
    upper_predictions, lower_predictions = None, None
    if upper_quantile_model and lower_quantile_model:
        upper_predictions = upper_quantile_model.predict(df)
        lower_predictions = lower_quantile_model.predict(df)
    return upper_predictions, lower_predictions
