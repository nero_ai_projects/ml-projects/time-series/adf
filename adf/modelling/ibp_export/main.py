import uuid
import logging
import pandas as pd  # type: ignore
from sqlalchemy.orm import Session  # type: ignore
import adf.services.database as db
from adf.services.database.query.reports import (
    build_report, get_report, update_report
)
from adf.modelling.ibp_export import tools
from adf.errors import IBPExportError
from adf.modelling import mllogs
from adf.services.database import (
    PipelineReports, DataprepReports
)

log = logging.getLogger("adf")


def run(forecast_ids, dataprep_id=None, **context):
    """ Export forecast data to IBP standard
    """
    session = context["session"]

    pipeline_id = context.get("pipeline_id", None)
    pipeline_run_type = context.get("pipeline_run_type", None)
    prediction_date = context.get("prediction_date", None)
    prerequired_forecast_ids = context.get("prerequired_forecast_ids", None)
    backtest = context.get("backtest", None)

    if not forecast_ids:
        log.info("Could not find forecasts for this IBP export")
        return build_report(
            session,
            db.IBPReports,
            description="No Forecast ID was given to the IBP export",
            status="skipped",
            reason="no_forecast_ids",
            pipeline_id=pipeline_id,
            pipeline_run_type=pipeline_run_type
        )

    log.info(f"Loading ForecastReports : {forecast_ids}")
    forecast_reports, description = tools.get_iso_reports(
        session, forecast_ids, db.ForecastReports
    )

    log.info(f"Forecast reports : {forecast_reports}")

    log.info("Loading Business Unit")
    business_unit = session.query(db.BusinessUnits) \
        .filter(db.BusinessUnits.country == description["country"]) \
        .filter(db.BusinessUnits.division == description["division"]) \
        .one()

    log.info("Loading data")
    forecast_df = pd.concat(
        [report.download(object_type="pandas") for report in forecast_reports]
    )

    if prediction_date:
        log.info(f"Filtering on prediction_date {prediction_date}...")
        forecast_df = forecast_df.loc[forecast_df.prediction_date == prediction_date]
    mllogs.log_shape(forecast_df)

    log.info("Building report")
    conf = forecast_reports[0].description
    report = build_report(
        session, db.IBPReports, description={**conf, **description},
        business_unit=business_unit, reason="pending_build",
        pipeline_id=pipeline_id, pipeline_run_type=pipeline_run_type
    )
    mllogs.tag_report(report)

    if (pipeline_id is not None or dataprep_id is not None) and conf["time"]["level"] == "week":

        log.info("Disaggregate to daily")

        if dataprep_id:
            dataprep_df = get_report(
                session, dataprep_id, DataprepReports
            ).download(object_type="koalas")
        else:
            dataprep_df = get_dataprep_df(pipeline_id, session)

        dataprep_df[list(dataprep_df.select_dtypes("category").columns)] = dataprep_df[
            list(dataprep_df.select_dtypes("category").columns)
        ].astype(str)

        prerequired_forecasts = None

        if prerequired_forecast_ids:
            prerequired_forecast_reports, description = \
                tools.get_iso_reports(session, prerequired_forecast_ids, db.ForecastReports)

            prerequired_forecasts = pd.concat([
                report.download(object_type="pandas") for report in prerequired_forecast_reports
            ])

        forecast_df = tools.disaggregate_forecasts(
            forecast_df,
            dataprep_df,
            conf,
            prerequired_forecasts,
            backtest
        )

    forecast_df = forecast_df.sort_values(
        by=["product_code", "location_code", "target_date"]
    )

    try:
        log.info("Building export data")
        export_df = tools.build_export(report, business_unit, forecast_df)
        mllogs.log_shape(export_df)
        update_report(session, report, "processing", "pending_export")

        log.info("Exporting")
        if backtest:
            log.info("Exporting for benchmark")
            report.upload_benchmark(forecast_df)

        report.upload(export_df, sep=';')
        mllogs.tag_report_output(report)
        mllogs.tag_export(report.mount, report.blob)
        update_report(session, report, "success", None)

        return report

    except Exception as err:
        message = str(err.__class__.__name__) + ": " + str(err)
        update_report(session, report, "failed", message)
        raise IBPExportError(message)

    finally:
        mllogs.tag_report_status(report)


def get_dataprep_df(pipeline_id: uuid.UUID, session: Session) -> pd.DataFrame:
    pipeline_report = get_report(
        session,
        pipeline_id,
        PipelineReports
    )
    dataprep_reports = pipeline_report.dataprep_report
    core_dataprep_reports = [
        dataprep_report
        for dataprep_report in dataprep_reports
        if dataprep_report.prep_type == "core"
    ]
    if core_dataprep_reports:
        return dataprep_reports[0].download(object_type="koalas")
    else:
        raise Exception(f"Could not find core dataprep report for pipeline_id : {pipeline_id}")
