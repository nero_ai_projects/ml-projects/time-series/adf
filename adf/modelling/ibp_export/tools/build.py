import logging
import numpy as np  # type: ignore
import pandas as pd  # type: ignore
from adf import config
from adf import utils
from adf.services.database import (
    IBPReports,
    BusinessUnits
)
from adf.modelling.ibp_export.tools.spain.business_rules import (
    apply_spain_dairy_rules
)
from adf.modelling.ibp_export.tools.dach.business_rules import (
    apply_dach_dairy_de_rules
)
from adf.modelling.ibp_export.tools.poland.business_rules import (
    apply_poland_dairy_rules,
    apply_poland_waters_rules
)
from adf.modelling.ibp_export.tools.france.business_rules import (
    apply_france_dairy_rules,
    apply_france_waters_rules
)
from adf.modelling.ibp_export.tools.canary.business_rules import (
    apply_canary_dairy_rules
)

log = logging.getLogger("adf")


def build_export(
        ibp_report: IBPReports,
        business_unit: BusinessUnits,
        forecast_df: pd.DataFrame
) -> pd.DataFrame:
    """ Construct export based on config, business unit and forecast information
    """
    conf = utils.BaseSchema(ibp_report.description)
    output = config.load_config("ibp_export/output.json")

    columns = [c["name"] for c in output]
    dtypes = {c["rename"]: c["dtype"] for c in output}
    rename = {c["name"]: c["rename"] for c in output}

    export_df = pd.DataFrame(columns=columns) \
        .pipe(assign_location, forecast_df, conf) \
        .pipe(assign_product, forecast_df, conf) \
        .pipe(assign_customer, forecast_df, conf) \
        .pipe(assign_uom, business_unit) \
        .pipe(assign_sales_org, business_unit) \
        .pipe(assign_date, forecast_df) \
        .pipe(assign_prediction, forecast_df) \
        .pipe(pd.DataFrame.rename, columns=rename) \
        .pipe(apply_business_rules, ibp_report) \
        .pipe(round_numerics) \
        .pipe(convert_types, dtypes) \
        .pipe(remove_unused_columns, columns=rename)

    return export_df


def assign_location(export_df, forecast_df, conf) -> pd.DataFrame:
    """ Fills column with location related code
    """
    if conf.location.level == "national":
        return export_df

    target = {
        "departmental": "department",
        "warehouse": "warehouse"
    }[conf.location.level]

    return export_df.assign(**{target: forecast_df.location_code})


def assign_product(export_df, forecast_df, conf) -> pd.DataFrame:
    """ Fills column with product related code
    """
    target = {
        "sku": "sku",
        "fu": "forecast_unit",
        "pv": "product_variant",
        "subfamily": "subfamily",
        "brand": "brand",
        "umbrella": "umbrella"
    }[conf.product.level]

    return export_df.assign(**{target: forecast_df.product_code})


def assign_customer(export_df, forecast_df, conf) -> pd.DataFrame:
    """ Fills column with product related code
    """
    if conf.customer.level == "all":
        return export_df

    target = {
        "cus_level_1": "customer_level_1",
        "cus_level_2": "customer_level_2",
        "cus_level_3": "customer_level_3",
        "cus_level_4": "customer_level_4",
        "shipto": "shipto"
    }[conf.customer.level]

    target_ibp = "customer_code"
    return export_df.assign(**{target: forecast_df.customer_code,
                               target_ibp: forecast_df.customer_code})


def assign_uom(export_df, business_unit) -> pd.DataFrame:
    """ Fill column with uom related code
    """
    return export_df.assign(**{"uom": business_unit.uom})


def assign_sales_org(export_df, business_unit) -> pd.DataFrame:
    """ Fill column with sales organization code
    """
    return export_df.assign(**{"sales_organization": business_unit.sales_org_cod})


def assign_date(export_df, forecast_df) -> pd.DataFrame:
    """ Fill date column
    """
    return export_df.assign(
        **{"date": forecast_df.target_date, "prediction_date": forecast_df.prediction_date}
    )


def assign_prediction(export_df, forecast_df) -> pd.DataFrame:
    """ Fill forecast column
    """
    export_df = export_df.assign(**{"total": forecast_df.prediction})
    export_df = export_df.assign(**{"baseline": forecast_df.prediction})
    export_df = export_df.assign(**{"min_prediction": forecast_df.min_prediction})
    export_df = export_df.assign(**{"max_prediction": forecast_df.max_prediction})
    return export_df


def round_numerics(export_df) -> pd.DataFrame:
    """ Round all numeric values into integers
    """
    for col in export_df.select_dtypes(include=np.number).columns.tolist():
        export_df[col] = export_df[col].round()
    return export_df


def convert_types(export_df, dtypes) -> pd.DataFrame:
    """ Convert export column types
    """
    for column, dtype in dtypes.items():

        if column not in export_df:
            continue

        if dtype in ("int", "float"):
            export_df[column] = pd.to_numeric(export_df[column], errors='coerce')
        elif "datetime" in dtype:
            export_df[column] = pd.to_datetime(
                export_df[column], errors='coerce'
            )

        if export_df[column].notnull().values.any():
            if "datetime" in dtype:
                export_df[column] = export_df[column].values.astype(dtype)
            else:
                export_df[column] = export_df[column].astype(dtype)

    export_df = export_df.where(export_df.notnull(), "")
    return export_df


def apply_business_rules(export_df, report) -> pd.DataFrame:
    """ Apply specific business units rules to the ibp export
    """
    if config.ENV != "test":
        country = report.description["country"]
        division = report.description["division"]

        if country == "spain" and division == "dairy":
            export_df = apply_spain_dairy_rules(export_df, report)
        elif country == "poland" and division == "waters":
            export_df = apply_poland_waters_rules(export_df, report)
        elif country == "poland" and division == "dairy":
            export_df = apply_poland_dairy_rules(export_df, report)
        elif country == "france" and division == "waters":
            export_df = apply_france_waters_rules(export_df, report)
        elif country == "dach":
            export_df = apply_dach_dairy_de_rules(export_df, report)
        elif country == "france" and division in ("dairy_reg", "dairy_veg"):
            export_df = apply_france_dairy_rules(export_df, report)
        elif country == "canary":
            export_df = apply_canary_dairy_rules(export_df, report)

    return export_df


def remove_unused_columns(export_df, columns):
    return export_df[list([col for col in columns.values() if col in export_df])]
