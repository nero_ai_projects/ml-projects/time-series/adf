import logging
import numpy as np  # type: ignore
import pandas as pd  # type: ignore
from sqlalchemy import desc  # type: ignore

import adf.modelling.dataprep.canary.dairy.data_sources as data_sources  # type: ignore
from adf.services.database import get_session, DataprepReports

log = logging.getLogger("adf")


def apply_canary_dairy_rules(export_df, report):
    export_df = export_df[~export_df.Baseline.isna()]

    export_df = add_dataprep(export_df, report)
    export_df = apply_plc(export_df)
    export_df = apply_warehouse_calendar(export_df)
    export_df = apply_building_blocks(export_df)
    export_df = apply_warehouse_aggregation(export_df)

    export_df["Sales Org"] = export_df["Sales Org"].apply("{:0>4}".format)
    export_df["WAREHOUSE"] = export_df["WAREHOUSE"].apply("{:0>4}".format)

    return export_df


def add_dataprep(export_df, report):
    session = get_session()
    dataprep_report = (
        session.query(DataprepReports)
        .filter(DataprepReports.pipeline_id == report.pipeline_id)
        .filter(DataprepReports.status == "success")
        .filter(DataprepReports.scope == "panel")
        .order_by(desc(DataprepReports.updated_at))
        .first()
    )

    dataprep = dataprep_report.download(object_type="pandas")

    dataprep = dataprep.replace(np.nan, 0)
    mapping = {
        "plt_cod": ("WAREHOUSE", "int64"),
        "sales_org_cod": ("Sales Org", "int64"),
        "mat_ava_dat": ("TIME", "datetime64"),
        "cus_superchain_cod": ("Customer level 2", "int64"),
        "mat_cod": ("SKU", "int64"),
    }
    dataprep = dataprep.rename(
        columns={col_before: col_after for col_before, (col_after, _) in mapping.items()}
    )
    dataprep = dataprep.astype({col_after: dtype for _, (col_after, dtype) in mapping.items()})
    export_df = export_df.astype({col_after: dtype for _, (col_after, dtype) in mapping.items()})
    export_df = export_df.merge(
        dataprep, how="left", on=[col_after for _, (col_after, _) in mapping.items()]
    )

    return export_df


def apply_building_blocks(export_df: pd.DataFrame) -> pd.DataFrame:
    export_df["mean_uplift"] = export_df.Baseline * export_df.total_uplifts
    sum_uplifts = export_df.groupby(["Customer level 2", "SKU", "FORECASTED_FROM", "TIME"])[
        "mean_uplift"
    ].sum()
    sum_uplifts = sum_uplifts.reset_index().rename(columns={"mean_uplift": "total_uplift"})
    export_df = export_df.merge(
        sum_uplifts, on=["Customer level 2", "SKU", "FORECASTED_FROM", "TIME"], how="left"
    )
    export_df.Promotion = export_df.total_uplift
    export_df.loc[(export_df.Promotion > export_df.Total), "Promotion"] = export_df.Total
    export_df.Baseline = export_df.Total - export_df.Promotion

    to_round = ["Total", "Baseline", "Promotion"]
    export_df[to_round] = export_df[to_round].round(0)
    export_df = export_df.replace(np.nan, 0)
    export_df = export_df.astype({col: "int32" for col in to_round})

    return export_df


def apply_warehouse_calendar(export_df):
    export_df["Total"] = export_df["Total"] * export_df.warehouse_opening
    export_df["Baseline"] = export_df["Baseline"] * export_df.warehouse_opening
    export_df["Promotion"] = export_df["Promotion"] * export_df.warehouse_opening

    return export_df


def apply_warehouse_aggregation(export_df):
    columns = export_df.columns
    export_df = export_df.groupby(["Sales Org", "WAREHOUSE", "SKU", "FORECASTED_FROM", "TIME"]).agg(
        Total=("Total", "sum"),
        Baseline=("Baseline", "sum"),
        Promotion=("Promotion", "sum"),
        **{
            col: (col, "first")
            for col in columns
            if col
            not in [
                "Sales Org",
                "WAREHOUSE",
                "SKU",
                "FORECASTED_FROM",
                "TIME",
                "Total",
                "Baseline",
                "Promotion",
            ]
        },
    )
    export_df = export_df.reset_index()
    export_df["Customer"] = export_df["Customer level 2"] = export_df["Customer level 1"]
    return export_df


def apply_plc(export_df):
    plc = data_sources.plc.fetch()

    plc = plc.sort_values(by="date").reset_index(drop=True)

    for _, row in plc.loc[plc.before != plc.after].iterrows():
        export_df.loc[
            (export_df.SKU == row.before) & (export_df.TIME >= row.date), "SKU"
        ] = row.after

    for _, row in plc.loc[plc.before == plc.after].iterrows():
        export_df.loc[(export_df.SKU == row.before) & (export_df.TIME >= row.date), "Total"] = 0

    return export_df
