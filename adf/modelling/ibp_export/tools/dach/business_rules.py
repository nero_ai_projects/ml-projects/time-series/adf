from typing import List, Union

import datetime
import logging

import numpy as np  # type: ignore
import pandas as pd  # type: ignore
from adf import config
from adf.modelling.dataprep.dach import get_country_name
from adf.modelling.dataprep.dach.dairy.panel import (
    format_customers, filter_orders_data, filter_customers_features, cast_decimal_to_numeric,
    process_orders_data, merge_dataframe
)
from adf.modelling.dataprep.dach import get_country_name_promo
from adf.modelling.dataprep.dach.dairy.plc import apply_sku_replacements
from adf.modelling.dataprep.utils.holidays import get_holidays_df
from adf.modelling.dataprep.utils.plc import apply_plc
from adf.modelling.dataprep.utils.promos import expand_promos_to_daily
from adf.modelling.ibp_export.tools.utils import merge_daily_with_weekly
from adf.modelling.tools import get_snowflake_df_spark
from adf.services.database import BusinessUnits, DataprepReports, get_session
from dateutil.relativedelta import relativedelta
from sqlalchemy import desc  # type: ignore
import databricks.koalas as ks  # type: ignore

log = logging.getLogger("adf")


def apply_dach_dairy_de_rules(export_df: pd.DataFrame, report) -> pd.DataFrame:
    """Set of dach dairy business rules to apply for generating
    the ibp export
    """
    export_df = ks.from_pandas(export_df)
    division = report.description["division"]
    if report.description["time"]["level"] == "day":
        export_df = merge_daily_with_weekly(export_df, "dach", division)
    export_df = export_df.to_pandas()
    export_df = (
        export_df.pipe(filter_out_stop_algo, division)
        .pipe(apply_building_blocks, "dach", division)
        .pipe(remove_old_products, "dach", division)
        .pipe(apply_business_rules, division)
        .pipe(update_warehouse, division)
    )
    export_df["Sales Org"] = export_df["Sales Org"].apply(lambda x: str(x).zfill(4))
    export_df["WAREHOUSE"] = export_df["WAREHOUSE"].apply(lambda x: str(x).zfill(4))
    export_df["Customer"] = export_df["Customer"].apply(lambda x: str(x).zfill(6))
    export_df["Customer level 1"] = export_df["Customer level 1"].apply(lambda x: str(x).zfill(6))
    return export_df


def update_warehouse(export_df: pd.DataFrame, division: str):
    customers_df = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, 'VCP_DSP_AFC_EDP_DACH', "R_SAP_EDP_DAC_CUS_HIE_NFA_RFA", "koalas"
    ).to_pandas().pipe(
        cast_decimal_to_numeric
    ).pipe(format_customers, division).pipe(filter_customers_features)

    orders_df = (
        get_snowflake_df_spark(
            config.SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_DACH", "D_SAP_EDP_DAC_DRY_ORD", "koalas"
        )
        .to_pandas()
        .pipe(filter_orders_data, division)
        .drop_duplicates()
        .pipe(process_orders_data, division)
    )

    orders_df.cus_cod = orders_df.cus_cod.astype(int)

    orders_df = orders_df.pipe(
        merge_dataframe,
        customers_df[["cus_cod", "distribution_channel_cod", "nfa_cod"]],
        on=["cus_cod", "distribution_channel_cod"],
        how="left",
        validate="many_to_one",
    )

    if division == "dairy_de":
        orders_df = apply_plc("dach", division, orders_df, "snowflake")
    else:
        orders_df = orders_df.pipe(apply_sku_replacements, division)
    orders_df["plant_cod"] = orders_df["plant_cod"].apply(lambda x: str(x).zfill(4))

    mat_cod_wh = orders_df[["plant_cod", "mat_cod"]].drop_duplicates(subset=["mat_cod"])

    export_df = export_df.drop("WAREHOUSE", axis=1)
    mat_cod_wh = mat_cod_wh.rename(columns={"plant_cod": "WAREHOUSE", "mat_cod": "SKU"})
    export_df = merge_dataframe(
        export_df, mat_cod_wh, on=["SKU"], how="left", validate="many_to_one"
    )
    return export_df


def filter_out_stop_algo(export_df: pd.DataFrame, division: str):
    master_data = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_DACH", "R_IBP_EDP_DAC_MAT_STS", df_type="koalas"
    )
    master_data = master_data[master_data["product_aggregation"] == "STOP ALGORITHM"]
    export_df = export_df[~export_df["SKU"].isin(master_data["mat_cod"].to_numpy())]
    return export_df


def remove_old_products(df: pd.DataFrame, country: str, division: str) -> pd.DataFrame:
    today = datetime.date.today()
    start_date = (today + relativedelta(months=-12)).strftime("%Y-%m-%d")
    session = get_session()
    business_unit = (
        session.query(BusinessUnits)
        .filter(BusinessUnits.country == country)
        .filter(BusinessUnits.division == division)
        .first()
    )
    dataprep_report = (
        session.query(DataprepReports)
        .filter(DataprepReports.status == "success")
        .filter(DataprepReports.prep_type == "core")
        .filter(DataprepReports.business_unit_id == business_unit.id)
        .order_by(desc(DataprepReports.updated_at))
        .first()
    )
    dataprep = dataprep_report.download()
    mask = (dataprep["to_dt"] >= start_date) & (dataprep["ordered_units"] > 0)
    active_prod = np.unique(dataprep.loc[mask, "mat_cod"].to_numpy())
    return df[df.SKU.isin(active_prod)]


def apply_building_blocks(export_df: pd.DataFrame, country: str, division: str) -> pd.DataFrame:
    """Merge promo building block table with ibp export"""
    session = get_session()
    business_unit = (
        session.query(BusinessUnits)
        .filter(BusinessUnits.country == country)
        .filter(BusinessUnits.division == division)
        .first()
    )

    dataprep_report = (
        session.query(DataprepReports)
        .filter(DataprepReports.status == "success")
        .filter(DataprepReports.prep_type == "core")
        .filter(DataprepReports.business_unit_id == business_unit.id)
        .order_by(desc(DataprepReports.updated_at))
        .first()
    )

    dataprep = dataprep_report.download(object_type="koalas").to_pandas()
    promo_uplift = dataprep[["mat_cod", "nfa_cod", "to_dt", "total_uplifts"]].pipe(
        cast_promo_rename
    )
    promo_uplift["total_uplifts"] = promo_uplift["total_uplifts"] / (
        1 + promo_uplift["total_uplifts"]
    )
    return export_df.pipe(merge_bb_with_export, promo_uplift).pipe(clean_export_from_bb)


def get_in_out_promo_mappings_snowflake(division: str) -> pd.DataFrame:
    if division == "dairy_at":
        return get_snowflake_df_spark(
            config.SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_DACH",
            "R_MAN_EDP_DAC_AUS_INP_OUT_PRM_MAP", "koalas"
        ).to_pandas()
    if division == "dairy_ch":
        return get_snowflake_df_spark(
            config.SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_DACH",
            "R_MAN_EDP_DAC_SWZ_INP_OUT_PRM_MAP", "koalas"
        ).to_pandas()


def get_in_out_promo_nfa_mappings_snowflake() -> pd.DataFrame:
    return get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_DACH",
        "R_MAN_EDP_DAC_GER_INP_OUT_NFA_PRM_MAP", "koalas"
    ).to_pandas()


def apply_business_rules(df: pd.DataFrame, division: str) -> pd.DataFrame:
    if division == "dairy_de":
        io_products_df = get_in_out_promo_nfa_mappings_snowflake().pipe(prepare_io_product_files)
        io_products_df = apply_plc("dach", division, io_products_df, "snowflake")
    else:
        io_products_df = get_in_out_promo_mappings_snowflake(division).pipe(
            apply_sku_replacements, division
        )

    # io_products_df = ks.from_pandas(io_products_df)

    io_products_df = (
        io_products_df.drop_duplicates()
        .assign(is_IO_on_promo=1)
        .rename(columns={"mat_cod": "SKU", "nfa_cod": "Customer level 1"})
    )

    promo_df = (
        get_snowflake_df_spark(
            config.SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_DACH",
            "R_BLP_EDP_DAC_PRM_HEA", "koalas"
        )
        .to_pandas()
        .drop_duplicates()
    )
    if division in ("dairy_de", "dairy_at"):
        promo_df["promotion_mechanics_cod"] = promo_df["promotion_mechanics_cod_de_at"]
    elif division == "dairy_ch":
        promo_df["promotion_mechanics_cod"] = promo_df["promotion_mechanics_cod_ch"]
    promo_df = promo_df.drop(
        ["promotion_mechanics_cod_de_at", "promotion_mechanics_cod_ch"], axis=1
    )
    dedup_keys = [
        "nfa_cod",
        "mat_cod",
        "promotion_start_date_store",
        "promotion_end_date_store",
        "status_code",
    ]

    promo_df["status_code"] = promo_df["status_code"].fillna(0)
    promo_df["status_code"] = promo_df["status_code"].astype(int)
    promo_df = promo_df.loc[
        (
            promo_df["status_code"] < 1500
        ) & (
            promo_df["territory"] == get_country_name_promo(division)
        ) & (
            promo_df["mat_cod"].astype(str).str.isdigit()
        )
    ]
    promo_df = promo_df.dropna(subset=["mat_cod", "nfa_cod"], axis=0)
    promo_df["nfa_cod"] = promo_df["nfa_cod"].astype(float).astype(int)
    promo_df["mat_cod"] = promo_df["mat_cod"].astype("int64")
    promo_df = promo_df.drop_duplicates(dedup_keys)

    if division == "dairy_at":
        promo_df["promotion_mechanics_cod"] = promo_df["promotion_mechanics_cod"].fillna(0)

    promo_df = promo_df.dropna(subset=["promotion_mechanics_cod"])
    promo_df["promotion_mechanics_cod"] = promo_df["promotion_mechanics_cod"].astype(int)
    promo_df["promotion_mechanics_cod"] = promo_df["promotion_mechanics_cod"].apply(lambda x: x + 1)

    promo_df = promo_df.drop_duplicates(
        [
            "mat_cod",
            "nfa_cod",
            "promotion_mechanics_cod",
            "promotion_start_date_sellin",
            "promotion_end_date_sellin",
        ],
        keep="first",
    )

    if division == "dairy_de":
        promo_df = apply_plc("dach", division, promo_df, "snowflake")
    else:
        promo_df = promo_df.pipe(apply_sku_replacements, division)

    df = (
        df.pipe(remove_dead_nfa)
        .pipe(prev_to_zero_on_weekends)
        .pipe(prev_to_zero_nat_bank_holidays, division)
        .pipe(prev_to_zero_outside_promo_io_products, division, promo_df, io_products_df)
        .pipe(cap_negative_pred_to_zero)
    )

    if division == "dairy_de":
        df = df.pipe(prev_to_zero_bavaria_bank_holidays)

    elif division == "dairy_ch":
        df = df.pipe(prev_to_zero_on_special_weekday_customer)

    return df


def normalize_uplift(
        promo_uplift: pd.DataFrame, col_to_normalize: Union[str, List[str]] = "total_uplifts"
) -> pd.DataFrame:
    agg_col = ["SKU", "Customer level 1"]
    promo_uplift[col_to_normalize] = promo_uplift.groupby(by=agg_col)[col_to_normalize].transform(
        lambda x: (x - x.min()) / (x.max() - x.min())
    )
    return promo_uplift


def cast_promo_rename(promo_df: pd.DataFrame) -> pd.DataFrame:
    promo_df["total_uplifts"] = promo_df["total_uplifts"].fillna(0)
    promo_df = promo_df.astype({"nfa_cod": "int", "mat_cod": "int"})
    promo_df = promo_df.rename(
        columns={"nfa_cod": "Customer level 1", "mat_cod": "SKU", "to_dt": "TIME"}
    )
    return promo_df


def merge_bb_with_export(export_df: pd.DataFrame, promo_uplift: pd.DataFrame) -> pd.DataFrame:
    export_df = export_df.merge(promo_uplift, on=["Customer level 1", "SKU", "TIME"], how="left")
    export_df["mean_uplift"] = export_df.Baseline * export_df.total_uplifts
    sum_uplifts = export_df.groupby(["Customer level 1", "SKU", "FORECASTED_FROM", "TIME"])[
        "mean_uplift"
    ].sum()
    sum_uplifts = sum_uplifts.reset_index().rename(columns={"mean_uplift": "total_uplift"})
    export_df = export_df.merge(
        sum_uplifts, on=["Customer level 1", "SKU", "FORECASTED_FROM", "TIME"], how="left"
    )
    export_df.Promotion = export_df.total_uplift
    export_df.loc[(export_df.Promotion > export_df.Total), "Promotion"] = export_df.Total
    export_df.Baseline = export_df.Total - export_df.Promotion

    return export_df


def clean_export_from_bb(export_df: pd.DataFrame) -> pd.DataFrame:
    """Clean export dataframe from merged building blocks columns"""
    export_df = export_df.drop("total_uplifts", axis=1)
    agg_dict = {
        "Total": "sum",
        "Baseline": "sum",
        "Promotion": "sum",
        **{
            el: "first"
            for el in list(export_df.columns)
            if el not in (
                "Customer level 1",
                "SKU",
                "FORECASTED_FROM",
                "TIME",
                "Total",
                "Baseline",
                "Promotion",
            )
        },
    }
    export_df = (
        export_df.groupby(["Customer level 1", "SKU", "FORECASTED_FROM", "TIME"])
        .agg(agg_dict)
        .reset_index()
    )

    to_round = ["Total", "Baseline", "Promotion"]
    export_df[to_round] = export_df[to_round].round(0)
    export_df = export_df.astype({col: "int32" for col in to_round})
    return export_df


def cap_negative_pred_to_zero(df: pd.DataFrame) -> pd.DataFrame:
    if len(df.loc[(df["Promotion"] < 0) | (df["Baseline"] < 0) | (df["Total"] < 0)]) > 0:
        if len(df.loc[(df["Promotion"] < 0) & (df["Baseline"] > 0) & (df["Total"] > 0)]) > 0:
            df.loc[(df["Promotion"] < 0) & (df["Baseline"] > 0), "Baseline"] = df.loc[
                (df["Promotion"] < 0) & (df["Baseline"] > 0), "Total"
            ]
            df.loc[(df["Promotion"] < 0) & (df["Baseline"] > 0), "Promotion"] = 0
        if len(df.loc[(df["Promotion"] > 0) & (df["Baseline"] < 0) & (df["Total"] > 0)]) > 0:
            df.loc[(df["Promotion"] < 0) & (df["Baseline"] > 0), "Baseline"] = df.loc[
                (df["Promotion"] < 0) & (df["Baseline"] > 0), "Total"
            ]
            df.loc[(df["Promotion"] < 0) & (df["Baseline"] > 0), "Promotion"] = 0
        if len(df.loc[df["Total"] < 0]) > 0:
            df.loc[(df["Total"] < 0), ["Total", "Baseline", "Promotion"]] = 0
    return df


def remove_dead_nfa(df: pd.DataFrame) -> pd.DataFrame:
    dead_nfas = [10002, 10222, 10013, 10242]
    df = df.loc[~df["Customer level 1"].isin(dead_nfas)]
    return df


def prev_to_zero_on_weekends(df: pd.DataFrame) -> pd.DataFrame:
    df["weekday"] = df["TIME"].map(lambda x: pd.Timestamp(x).weekday())
    df.loc[df["weekday"].isin([5, 6]), ["Total", "Baseline", "Promotion"]] = 0
    df = df.drop("weekday", axis=1)
    return df


def prev_to_zero_nat_bank_holidays(
        df: pd.DataFrame,
        division: str,
        time_col="TIME",
        col_to_zeros: Union[str, List[str], None] = None,
) -> pd.DataFrame:
    if col_to_zeros is None:
        col_to_zeros = ["Total", "Baseline", "Promotion"]
    country_name = get_country_name(division)
    holidays_df = get_holidays_df(country_name, "dayoff")
    time_arr = df[time_col].to_numpy()
    holidays_df = holidays_df.loc[
        (holidays_df["date"] >= min(time_arr)) & (holidays_df["date"] <= max(time_arr))
    ]
    holidays_dates = set(holidays_df.loc[holidays_df["is_holiday"] == 1]["date"])
    df.loc[df[time_col].isin(holidays_dates), col_to_zeros] = 0
    return df


def prev_to_zero_bavaria_bank_holidays(
        df: pd.DataFrame, time_col="TIME", col_to_zeros: Union[str, List[str], None] = None
) -> pd.DataFrame:
    if col_to_zeros is None:
        col_to_zeros = ["Total", "Baseline", "Promotion"]
    bavaria_nfa_cods = set(get_bavaria_nfa_cod())
    bavaria_holidays_df = get_holidays_df("germany_BY", "dayoff").drop(
        columns=["holiday_name", "days_before_next_holiday"]
    )
    bavaria_holidays_df = bavaria_holidays_df.loc[
        (
            bavaria_holidays_df["date"] >= min(df[time_col])
        ) & (
            bavaria_holidays_df["date"] <= max(df[time_col])
        )
    ]
    bavaria_holidays_dates = bavaria_holidays_df.loc[bavaria_holidays_df["is_holiday"] == 1]["date"]
    df.loc[
        (
            df[time_col].isin(bavaria_holidays_dates) & (
                df["Customer level 1"].isin(bavaria_nfa_cods)
            )
        ),
        col_to_zeros,
    ] = 0
    return df


def get_bavaria_nfa_cod() -> pd.Series:
    customers_df = (
        get_snowflake_df_spark(
            config.SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_DACH",
            "R_SAP_EDP_DAC_CUS_HIE_NFA_RFA", "koalas"
        ).to_pandas()
        .pipe(format_customers, "dairy_de")
        .query('country_code == "DE"')
        .filter(items=["nfa_cod", "region_code"])
        .drop_duplicates()
        .dropna()
        .astype({"region_code": "int64"})
    )

    mapping_region_cod = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_DACH",
        "R_MAN_EDP_DAC_GER_RGN_COD_MAP", "koalas"
    ).to_pandas().query('country_code == "DE" & region_code_str == "BY"') \
        .astype({"region_code": "int64"})

    bavaria_customers_df = customers_df.merge(
        mapping_region_cod, on="region_code", validate="many_to_one", how="inner"
    )
    return bavaria_customers_df["nfa_cod"]


def prev_to_zero_outside_promo_io_products(
        df: pd.DataFrame, division: str, promo_df: pd.DataFrame, io_products_df: pd.DataFrame
) -> pd.DataFrame:
    """Put prediction to zero when IO product is not in promotion
    - per customer for DE
    - at a national level for AT and CH
    """
    if division == "dairy_de":
        df = prev_to_zero_outside_promo_io_products_per_client(df, io_products_df)
    else:
        df = prev_to_zero_outside_promo_io_products_national(df, promo_df, io_products_df)
    return df


def prev_to_zero_outside_promo_io_products_per_client(
        df: pd.DataFrame, io_products_df: pd.DataFrame
) -> pd.DataFrame:
    """Put prediction to zero when IO product is not in promotion per NFA"""
    df = df.merge(
        io_products_df, on=["SKU", "Customer level 1"], how="left"
    )
    df["Promotion"] = np.where(df["is_IO_on_promo"] == 1, df["Total"], df["Promotion"])
    df["Baseline"] = np.where(df["is_IO_on_promo"] == 1, 0, df["Baseline"])
    df = df.drop("is_IO_on_promo", axis=1)
    return df


def prev_to_zero_outside_promo_io_products_national(
        df: pd.DataFrame, promo_df: pd.DataFrame, io_products_df: pd.DataFrame
) -> pd.DataFrame:
    """Put prediction to zero when IO product is not in promotion at a national level"""

    skus = set(df["SKU"])
    promo_df = promo_df.loc[
        (
            promo_df["mat_cod"].isin(skus)
        ) & (
            promo_df["promotion_end_date_sellin"] >= min(df["TIME"])
        ) & (
            promo_df["promotion_start_date_sellin"] <= max(df["TIME"])
        )
    ][["mat_cod", "promotion_start_date_sellin", "promotion_end_date_sellin"]].drop_duplicates()

    promo_df_expand = (
        promo_df.pipe(
            expand_promos_to_daily,
            "promotion_start_date_sellin",
            "promotion_end_date_sellin",
            "to_dt",
        )
        .drop("elapsed_time", axis=1)
        .drop_duplicates()
        .assign(is_IO_on_promo=1)
        .rename(columns={"mat_cod": "SKU", "to_dt": "TIME"})
    )

    df = df.merge(promo_df_expand, on=["SKU", "TIME"], how="left", validate="many_to_one")
    df.loc[
        (df["is_IO_on_promo"].isnull()) & (df["SKU"].isin(io_products_df["SKU"])),
        ["Total", "Baseline", "Promotion"]
    ] = 0
    df = df.drop("is_IO_on_promo", axis=1)
    return df


def prev_to_zero_on_special_weekday_customer(df: pd.DataFrame) -> pd.DataFrame:
    df["weekday"] = df["TIME"].map(lambda x: pd.Timestamp(x).weekday())
    df.loc[
        (df["weekday"] == 4) & (
            df["Customer level 1"].isin(
                [12482, 12483, 12484, 12489, 12499, 12505, 12536, 12547, 12548, 12553, 12612]
            )
        ),
        ["Total", "Baseline", "Promotion"],
    ] = 0
    df.loc[
        (df["weekday"] == 0) & (df["Customer level 1"].isin([12489, 12505, 12499])),
        ["Total", "Baseline", "Promotion"],
    ] = 0
    df = df.drop("weekday", axis=1)
    return df


def prepare_io_product_files(df: pd.DataFrame) -> pd.DataFrame:
    """Prepare raw file containing IO products per Customer"""
    df["product"] = df["product"].str.split(" / ")
    df["mat_cod"] = df["product"].apply(lambda x: int(x[0]))
    df["sku_desc"] = df["product"].apply(lambda x: x[1])
    df = df.drop(columns=["product"]).rename(columns={"customer": "nfa_cod"}).drop_duplicates()
    return df[["mat_cod", "nfa_cod"]]
