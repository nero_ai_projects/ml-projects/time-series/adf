import logging
import numpy as np  # type: ignore
import pandas as pd  # type: ignore
import databricks.koalas as ks  # type: ignore
from adf.modelling.ibp_export.tools import utils
from adf.modelling.ibp_export.tools.dach.business_rules import prev_to_zero_nat_bank_holidays
from adf.types import DataFrame


log = logging.getLogger("adf")


def disaggregate_forecasts(
    forecasts: DataFrame,
    dataprep_df: DataFrame,
    config: dict,
    prerequired_forecasts: DataFrame,
    backtest: bool,
):
    with ks.option_context(
        "compute.ops_on_diff_frames", True, "compute.default_index_type", "distributed"
    ):
        weighted_table = None
        prod_col, loc_col, cus_col, time_col = utils.get_dataprep_columns(config)
        prod_col, loc_col, cus_col, dataprep_df = utils.fill_missing_columns(
            prod_col, loc_col, cus_col, dataprep_df
        )
        params = None
        if config["tags"]["country"] == "spain" and config["tags"]["division"] == "dairy":
            params = utils.get_params(
                prod_col=prod_col,
                loc_col=loc_col,
                cus_col=(cus_col, "ignore"),
                time_col=time_col,
                target_col=config["target"],
            )
            params.update({"horizon": 14})
            weighted_table = create_weighted_table_spain_dairy(
                dataprep_df, forecasts, prerequired_forecasts, params
            )
        elif config["tags"]["country"] == "poland" and config["tags"]["division"] == "waters":
            params = utils.get_params(
                prod_col=prod_col,
                loc_col=loc_col,
                cus_col=(cus_col, "ignore"),
                time_col=time_col,
                target_col=config["target"],
            )
            params.update({"horizon": 14})
            weighted_table = create_weighted_table_poland_waters(
                dataprep_df, forecasts, prerequired_forecasts, params
            )
        elif config["tags"]["country"] == "dach":
            params = utils.get_params(
                prod_col=prod_col,
                loc_col=loc_col,
                cus_col=(cus_col, "ignore"),
                time_col=time_col,
                target_col=config["target"],
            )
            params.update({"horizon": 14})
            weighted_table = create_weighted_table_dach_dairy_de(
                dataprep_df, config, forecasts, prerequired_forecasts, params
            )
        elif config["tags"]["country"] == "poland" and config["tags"]["division"] == "dairy":
            params = utils.get_params(
                prod_col=prod_col,
                loc_col=loc_col,
                cus_col=cus_col,
                time_col=time_col,
                target_col=config["target"],
            )
            params.update({"horizon": 14})
            weighted_table = create_weighted_table_poland_dairy(
                dataprep_df, forecasts, prerequired_forecasts, params
            )
        elif config["tags"]["country"] == "france" and config["tags"]["division"] in (
            "dairy_reg",
            "dairy_veg",
        ):
            params = utils.get_params(
                prod_col=prod_col,
                loc_col=loc_col,
                cus_col=(cus_col, "ignore"),
                time_col=time_col,
                target_col=config["target"],
            )
            params.update({"horizon": 14})
            weighted_table = create_weighted_table_france_dairy(
                dataprep_df, forecasts, prerequired_forecasts, params
            )
        elif config["tags"]["country"] == "france" and config["tags"]["division"] == "waters":
            params = utils.get_params(
                prod_col=prod_col,
                loc_col=loc_col,
                cus_col=(cus_col, "ignore"),
                time_col=time_col,
                target_col=config["target"],
            )
            params.update({"horizon": 14})
            weighted_table = create_weighted_table_france_waters(
                dataprep_df, forecasts, prerequired_forecasts, params
            )
        elif config["tags"]["country"] == "canary" and config["tags"]["division"] == "dairy":
            params = utils.get_params(
                prod_col=prod_col,
                loc_col=loc_col,
                cus_col=cus_col,
                time_col=time_col,
                target_col=config["target"],
            )
            params.update({"horizon": 14})
            weighted_table = create_weighted_table_canary_dairy(
                dataprep_df, forecasts, prerequired_forecasts, params
            )

        disaggregated_forecasts = utils.disaggregate_to_daily(
            weighted_table, forecasts, dataprep_df, config, backtest, params
        )
        return disaggregated_forecasts


def create_weighted_table_spain_dairy(
    dataprep_df: DataFrame, forecasts: DataFrame, prerequired_forecasts: DataFrame, params: dict
):
    dataprep_df = dataprep_df[
        [
            params["prod_col"],
            params["loc_col"],
            params["cus_col"],
            params["time_col"],
            params["target"],
            "warehouse_opening",
            "is_holiday",
        ]
    ]
    warehouse_openings = (
        dataprep_df.groupby([params["loc_col"], params["time_col"]])
        .warehouse_opening.first()
        .reset_index()
    )
    dataprep_df = utils.set_date_columns(dataprep_df, params)

    wh_closure_weeks = (
        dataprep_df.loc[
            ((dataprep_df.warehouse_opening == 0) | (dataprep_df.is_holiday == 1)) &
            (dataprep_df[params["time_col"]].dt.weekday != 6) &
            (dataprep_df[params["time_col"]] < forecasts.prediction_date.min())
        ]
        .week_start.unique()
        .tolist()
    )

    almudena_holiday_week = (
        dataprep_df.loc[
            (dataprep_df[params["time_col"]].dt.day == 9) &
            (dataprep_df[params["time_col"]].dt.month == 11) &
            (dataprep_df[params["time_col"]] < forecasts.prediction_date.min())
        ]
        .week_start.unique()
        .tolist()
    )

    snow_storm_21_week = (
        dataprep_df.loc[
            (dataprep_df.to_dt.dt.weekofyear == 1) & (dataprep_df.to_dt.dt.year == 2021)
        ]
        .week_start.unique()
        .tolist()
    )

    weeks_to_remove = (wh_closure_weeks, almudena_holiday_week, snow_storm_21_week)
    weeks_to_remove = np.concatenate(weeks_to_remove, axis=None)

    dataprep_df = (
        dataprep_df.loc[
            ~dataprep_df.week_start.isin([pd.to_datetime(val) for val in weeks_to_remove])
        ]
        .pipe(utils.calculate_weekly_quantities, params)
        .pipe(utils.calculate_rolling_mean_weights, params)
        .pipe(utils.reduce_dataprep, forecasts.target_date.min(), params)
    )
    dataprep_df = (
        dataprep_df.to_pandas().pipe(utils.lag_weights, params).pipe(utils.pivot_dataset, params)
    )

    if isinstance(dataprep_df, ks.DataFrame):
        dataprep_df = dataprep_df.to_pandas()

    if isinstance(warehouse_openings, ks.DataFrame):
        warehouse_openings = warehouse_openings.to_pandas()

    if prerequired_forecasts is not None:
        prerequired_forecasts = prerequired_forecasts.pipe(
            utils.calculate_forecast_weights, params
        ).pipe(utils.format_forecasts, params)

        prerequired_forecasts[params["time_col"]] = pd.DatetimeIndex(
            prerequired_forecasts[params["time_col"]]
        ).tz_localize(None)

        prerequired_forecasts = prerequired_forecasts.merge(
            warehouse_openings,
            left_on=[params["time_col"], "location_code"],
            right_on=[params["time_col"], params["loc_col"]],
        )
        prerequired_forecasts.loc[prerequired_forecasts.warehouse_opening == 0, "weight"] = 0

        dataprep_df = utils.update_weights(dataprep_df, prerequired_forecasts, params)

    dataprep_df[params["time_col"]] = pd.DatetimeIndex(dataprep_df[params["time_col"]]).tz_localize(
        None
    )
    dataprep_df = utils.apply_spain_weighted_distrib(dataprep_df)

    dataprep_df = dataprep_df.merge(
        warehouse_openings,
        left_on=[params["time_col"], "location_code"],
        right_on=[params["time_col"], params["loc_col"]],
    )
    dataprep_df.loc[dataprep_df.warehouse_opening == 0, "weight"] = 0
    dataprep_df = dataprep_df.drop(columns=["warehouse_opening"])
    return utils.normalize_weights(dataprep_df, params)


def create_weighted_table_poland_waters(
    dataprep_df: DataFrame, forecasts: DataFrame, prerequired_forecasts: DataFrame, params: dict
):
    dataprep_df = dataprep_df[
        [
            params["prod_col"],
            params["loc_col"],
            params["cus_col"],
            params["time_col"],
            params["target"],
            "is_holiday",
        ]
    ]
    dataprep_df = utils.set_date_columns(dataprep_df, params)

    to_remove = (
        dataprep_df.loc[
            (
                dataprep_df.is_holiday == 1
            ) & (
                dataprep_df[params["time_col"]] < forecasts.prediction_date.min()
            )
        ]
        .week_start.unique()
        .to_numpy()
    )

    to_crop = dataprep_df.loc[(dataprep_df.is_holiday == 1)][params["time_col"]].unique().to_numpy()

    weights = (
        dataprep_df.loc[~dataprep_df.week_start.isin([pd.to_datetime(val) for val in to_remove])]
        .pipe(utils.calculate_weekly_quantities, params)
        .pipe(utils.calculate_rolling_mean_weights, params)
        .pipe(utils.reduce_dataprep, forecasts.target_date.min(), params)
    )
    weights = weights.to_pandas().pipe(utils.lag_weights, params).pipe(utils.pivot_dataset, params)

    if prerequired_forecasts is not None:
        prerequired_forecasts = prerequired_forecasts.pipe(
            utils.calculate_forecast_weights, params
        ).pipe(utils.format_forecasts, params)

        weights = utils.update_weights(weights, prerequired_forecasts, params)

    weights.loc[weights[params["time_col"]].isin(to_crop), "weight"] = 0

    return utils.normalize_weights(weights, params)


def create_weighted_table_poland_dairy(
    dataprep_df: DataFrame, forecasts: DataFrame, prerequired_forecasts: DataFrame, params: dict
):
    dataprep_df = dataprep_df[
        [
            params["prod_col"],
            params["loc_col"],
            params["cus_col"],
            params["time_col"],
            params["target"],
            "is_holiday",
        ]
    ]
    dataprep_df = utils.set_date_columns(dataprep_df, params)

    to_remove = (
        dataprep_df.loc[
            (
                dataprep_df.is_holiday == 1
            ) & (
                dataprep_df[params["time_col"]] < forecasts.prediction_date.min()
            )
        ]
        .week_start.unique()
        .to_numpy()
    )

    to_crop = dataprep_df.loc[(dataprep_df.is_holiday == 1)][params["time_col"]].unique().to_numpy()

    dataprep_df = (
        dataprep_df.loc[~dataprep_df.week_start.isin([pd.to_datetime(val) for val in to_remove])]
        .pipe(utils.calculate_weekly_quantities, params)
        .pipe(utils.calculate_rolling_mean_weights, params)
        .pipe(utils.reduce_dataprep, forecasts.target_date.min(), params)
    )
    dataprep_df = (
        dataprep_df.to_pandas().pipe(utils.lag_weights, params).pipe(utils.pivot_dataset, params)
    )

    if prerequired_forecasts is not None:
        prerequired_forecasts = prerequired_forecasts.pipe(
            utils.calculate_forecast_weights, params
        ).pipe(utils.format_forecasts, params)

        dataprep_df = utils.update_weights(dataprep_df, prerequired_forecasts, params)
    dataprep_df.loc[dataprep_df[params["time_col"]].isin(to_crop), "weight"] = 0
    dataprep_df = utils.normalize_weights(
        dataprep_df, params, ignore_weekdays=[5, 6], ignore_dates=to_crop
    )
    dataprep_df["customer_code"] = dataprep_df["customer_code"].astype(int)
    return dataprep_df


def create_weighted_table_dach_dairy_de(
    dataprep_df: DataFrame,
    config: dict,
    forecasts: DataFrame,
    prerequired_forecasts: DataFrame,
    params: dict,
):
    dataprep_df = dataprep_df[
        [
            params["prod_col"],
            params["loc_col"],
            params["cus_col"],
            params["time_col"],
            params["target"],
            "is_holiday",
        ]
    ]
    dataprep_df = utils.set_date_columns(dataprep_df, params)

    to_remove = (
        dataprep_df.loc[
            (
                dataprep_df.is_holiday == 1
            ) & (
                dataprep_df[params["time_col"]] < forecasts.prediction_date.min()
            )
        ]
        .week_start.unique()
        .to_numpy()
    )

    dataprep_df = (
        dataprep_df.loc[~dataprep_df.week_start.isin([pd.to_datetime(val) for val in to_remove])]
        .pipe(utils.calculate_weekly_quantities, params)
        .pipe(utils.calculate_rolling_mean_weights, params)
        .pipe(utils.reduce_dataprep, forecasts.target_date.min(), params)
    )
    dataprep_df = (
        dataprep_df.to_pandas().pipe(utils.lag_weights, params).pipe(utils.pivot_dataset, params)
    )

    if prerequired_forecasts is not None:
        prerequired_forecasts = prerequired_forecasts.pipe(
            utils.calculate_forecast_weights, params
        ).pipe(utils.format_forecasts, params)

        dataprep_df = utils.update_weights(dataprep_df, prerequired_forecasts, params)

    dataprep_df = process_dataprep_rules_dach(dataprep_df, config, params)

    return utils.normalize_weights(dataprep_df, params)


def process_dataprep_rules_dach(dataprep_df, config, params):
    division = config["tags"]["division"]
    time_col = params["time_col"]
    col_to_zeros = ["weight"]
    dataprep_df.loc[dataprep_df[time_col].dt.weekday.isin([5, 6]), "weight"] = 0.0
    dataprep_df = dataprep_df.pipe(prev_to_zero_nat_bank_holidays, division, time_col, col_to_zeros)

    return dataprep_df


def create_weighted_table_france_dairy(
    dataprep_df: DataFrame, forecasts: DataFrame, prerequired_forecasts: DataFrame, params: dict
):
    dataprep_df = dataprep_df[
        [
            params["prod_col"],
            params["loc_col"],
            params["cus_col"],
            params["time_col"],
            params["target"],
            "is_holiday",
        ]
    ]
    dataprep_df = utils.set_date_columns(dataprep_df, params)

    to_remove = (
        dataprep_df.loc[
            (
                dataprep_df.is_holiday == 1
            ) & (
                dataprep_df[params["time_col"]] < forecasts.prediction_date.min()
            )
        ]
        .week_start.unique()
        .to_numpy()
    )

    dataprep_df = (
        dataprep_df.loc[~dataprep_df.week_start.isin([pd.to_datetime(val) for val in to_remove])]
        .pipe(utils.calculate_weekly_quantities, params)
        .pipe(utils.calculate_rolling_mean_weights, params)
        .pipe(utils.reduce_dataprep, forecasts.target_date.min(), params)
    )
    dataprep_df = (
        dataprep_df.to_pandas().pipe(utils.lag_weights, params).pipe(utils.pivot_dataset, params)
    )

    if prerequired_forecasts is not None:
        prerequired_forecasts = prerequired_forecasts.pipe(
            utils.calculate_forecast_weights, params
        ).pipe(utils.format_forecasts, params)

        dataprep_df = dataprep_df.pipe(utils.update_weights, prerequired_forecasts, params).pipe(
            utils.normalize_weights, params
        )

        dataprep_df = utils.update_weights(dataprep_df, prerequired_forecasts, params)

    return utils.normalize_weights(dataprep_df, params)


def create_weighted_table_france_waters(
    dataprep_df: DataFrame, forecasts: DataFrame, prerequired_forecasts: DataFrame, params: dict
):
    dataprep_df = dataprep_df[
        [
            params["prod_col"],
            params["loc_col"],
            params["cus_col"],
            params["time_col"],
            params["target"],
            "is_holiday",
        ]
    ]
    dataprep_df = utils.set_date_columns(dataprep_df, params)

    to_remove = (
        dataprep_df.loc[
            (
                dataprep_df.is_holiday == 1
            ) & (
                dataprep_df[params["time_col"]] < forecasts.prediction_date.min()
            )
        ]
        .week_start.unique()
        .to_numpy()
    )

    to_crop = dataprep_df.loc[(dataprep_df.is_holiday == 1)][params["time_col"]].unique().to_numpy()

    weights = (
        dataprep_df.loc[~dataprep_df.week_start.isin([pd.to_datetime(val) for val in to_remove])]
        .pipe(utils.calculate_weekly_quantities, params)
        .pipe(utils.calculate_rolling_mean_weights, params)
        .pipe(utils.reduce_dataprep, forecasts.target_date.min(), params)
    )
    weights = weights.to_pandas().pipe(utils.lag_weights, params).pipe(utils.pivot_dataset, params)

    if prerequired_forecasts is not None:
        prerequired_forecasts = prerequired_forecasts.pipe(
            utils.calculate_forecast_weights, params
        ).pipe(utils.format_forecasts, params)

        weights = utils.update_weights(weights, prerequired_forecasts, params)

    weights.loc[weights[params["time_col"]].isin(to_crop), "weight"] = 0
    return utils.normalize_weights(weights, params)


def create_weighted_table_canary_dairy(
    dataprep_df: DataFrame, forecasts: DataFrame, prerequired_forecasts: DataFrame, params: dict
):
    if isinstance(dataprep_df, ks.DataFrame):
        dataprep_df = dataprep_df.to_pandas()

    dataprep_df = dataprep_df[
        [
            params["prod_col"],
            params["loc_col"],
            params["cus_col"],
            params["time_col"],
            params["target"],
            "warehouse_opening",
            "is_holiday",
        ]
    ]
    dataprep_df = utils.set_date_columns(dataprep_df, params)
    warehouse_openings = (
        dataprep_df.groupby([params["loc_col"], params["time_col"]])
        .warehouse_opening.first()
        .reset_index()
    )

    to_remove = (
        dataprep_df.loc[
            (
                dataprep_df.is_holiday == 1
            ) & (
                dataprep_df[params["time_col"]] < forecasts.prediction_date.min()
            )
        ]
        .week_start.unique()
        .tolist()
    )

    dataprep_df = (
        dataprep_df.loc[~dataprep_df.week_start.isin([pd.to_datetime(val) for val in to_remove])]
        .pipe(utils.calculate_weekly_quantities, params)
        .pipe(utils.calculate_rolling_mean_weights, params)
        .pipe(utils.reduce_dataprep, forecasts.target_date.min(), params)
    )
    dataprep_df = dataprep_df.pipe(utils.lag_weights, params).pipe(utils.pivot_dataset, params)

    if prerequired_forecasts is not None:
        prerequired_forecasts = prerequired_forecasts.pipe(
            utils.calculate_forecast_weights, params
        ).pipe(utils.format_forecasts, params)

        dataprep_df = utils.update_weights(dataprep_df, prerequired_forecasts, params)

    dataprep_df = dataprep_df.merge(
        warehouse_openings,
        left_on=[params["time_col"], "location_code"],
        right_on=[params["time_col"], params["loc_col"]],
    )
    dataprep_df.loc[dataprep_df.warehouse_opening == 0, "weight"] = 0
    dataprep_df = dataprep_df.drop(columns=["warehouse_opening"])
    return utils.normalize_weights(dataprep_df, params)
