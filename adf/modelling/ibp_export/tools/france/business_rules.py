import pandas as pd  # type: ignore
import os
from sqlalchemy import desc  # type: ignore
import databricks.koalas as ks  # type: ignore
from adf.modelling.ibp_export.tools.utils import merge_daily_with_weekly
from adf.services.database import (
    get_session,
    BusinessUnits,
    DataprepReports,
    BuildingBlocksReports,
    PromoDataprepReports
)
from adf.modelling.tools import get_snowflake_df
from adf.modelling.dataprep.france.dairy.experimental import get_snowflake_df_spark
from adf.modelling.dataprep.france.dairy.promo import (
    process_promo_cas, unify_promotions, cast_customer_ids
)
from adf.modelling.dataprep.utils.holidays import get_holidays_df
from adf.modelling.dataprep.france.dairy.panel import (
    prepare_customers as prepare_france_dairy_customers,
    merge_with_customers as merge_with_france_dairy_customers,
    update_plant_cod as update_france_dairy_plant_cod
)
from adf import config


def apply_france_waters_rules(export_df, report):
    """
    Sets up France Waters business rules to apply for generating the IBP export
    """
    country = report.description["country"]
    division = report.description["division"]
    export_df = export_df.astype({"FU": "int32"})

    # Convert KL to L
    for col in ["Total", "Baseline", "Promotion", "Min", "Max"]:
        export_df[col] = export_df[col] * 1000

    # If daily forecasts, merge daily forecasts on short term with weekly disaggregation for other
    # horizons
    if report.description["time"]["level"] == "day":
        export_df = merge_daily_with_weekly(export_df, country, division)

    # Add promotion building block
    export_df = apply_building_blocks(export_df, country, division)
    export_df = finalize_format_france_waters(export_df)

    return export_df


def apply_france_dairy_rules(export_df: pd.DataFrame, report):
    """ Set of france dairy business rules to apply for generating
    the ibp export
    """
    country = report.description["country"]
    division = report.description["division"]
    export_df = apply_building_blocks(export_df, country, division)
    export_df = update_france_dairy_warehouse_cod(export_df, division)
    export_df = apply_france_dairy_business_rules(export_df)
    export_df["Sales Org"] = export_df["Sales Org"].apply(lambda x: str(x).zfill(4))

    export_df["WAREHOUSE"] = export_df["WAREHOUSE"].apply(
        lambda x: str(int(x)).zfill(4) if x != "" else x
    )
    export_df["SKU"] = export_df["SKU"].apply(lambda x: str(x).zfill(6))
    export_df["Customer level 4"] = export_df["Customer level 4"].apply(
        lambda x: str(x).zfill(6) if x != "" else x
    )
    export_df["Customer"] = export_df["Customer"].apply(
        lambda x: str(x).zfill(6) if x != "" else x
    )
    export_df["Dept"] = export_df["Dept"].apply(
        lambda x: str(x).zfill(6) if x in ("None", "") else x
    )
    return export_df


def apply_building_blocks(
        export_df: pd.DataFrame, country: str, division: str
) -> pd.DataFrame:
    """ Merge promos building block table with ibp export
    """
    session = get_session()

    business_unit = session.query(BusinessUnits) \
        .filter(BusinessUnits.country == country) \
        .filter(BusinessUnits.division == division) \
        .first()

    if division == "waters":
        bb_table = session \
            .query(BuildingBlocksReports) \
            .filter(BuildingBlocksReports.status == "success") \
            .filter(BuildingBlocksReports.business_unit_id == business_unit.id) \
            .filter(BuildingBlocksReports.scope == "promo_building_block") \
            .order_by(desc(BuildingBlocksReports.updated_at)) \
            .first() \
            .download(object_type="pandas")

        for col in bb_table.filter(regex="uplift").columns:
            bb_table.loc[(bb_table[col] > 1), col] = 1

        promos_df = session \
            .query(DataprepReports) \
            .filter(DataprepReports.status == "success") \
            .filter(DataprepReports.business_unit_id == business_unit.id) \
            .filter(DataprepReports.scope == "promo_panel") \
            .filter(DataprepReports.prep_type == "promotion") \
            .order_by(desc(DataprepReports.updated_at)) \
            .first() \
            .download(object_type="koalas") \
            .to_pandas()

        bb_df = promos_df \
            .merge(
                bb_table,
                on=["fu_cod", "rfa_cod"],
                how="left") \
            .pipe(format_and_aggregate_bb)

        return export_df. \
            pipe(merge_bb_with_export, bb_df). \
            pipe(clean_export_from_bb)

    elif division in ("dairy_reg", "dairy_veg"):
        promo_uplifts_dataprep = session.query(PromoDataprepReports) \
            .filter(PromoDataprepReports.business_unit_id == business_unit.id) \
            .filter(PromoDataprepReports.product_level == "sku") \
            .filter(PromoDataprepReports.location_level == "departmental") \
            .filter(PromoDataprepReports.scope == "uplifts_promo") \
            .filter(PromoDataprepReports.status == "success") \
            .filter(PromoDataprepReports.updated_at.isnot(None))  # type: ignore
        promo_uplifts_dataprep = promo_uplifts_dataprep \
            .order_by(desc(PromoDataprepReports.updated_at)) \
            .first()

        promo_uplift = promo_uplifts_dataprep.download(object_type="pandas")

        promo_uplift['total_uplifts'] = promo_uplift['total_uplifts'] / (
            1 + promo_uplift['total_uplifts'])

        promo_uplift = promo_uplift[['mat_cod', 'rfa_cod', 'to_dt', 'total_uplifts']] \
            .pipe(cast_and_rename_promos)

        export_df['TIME'] = export_df['TIME'].astype(str)

        export_df['TIME'] = pd.to_datetime(export_df['TIME'], utc=True)
        promo_uplift['TIME'] = pd.to_datetime(promo_uplift['TIME'], utc=True)

        return export_df. \
            pipe(merge_bb_with_export_france_dairy, promo_uplift). \
            pipe(clean_export_from_bb_france_dairy)


def update_france_dairy_warehouse_cod(df, division):
    # orders = get_snowflake_df(
    #   config.SNOWFLAKE_DATABASE, 'VCP_DSP_AFC_EDP_FRC', 'D_SAP_EDP_FRA_DRY_ORD'
    # )
    # orders['material_availability_date'] = pd.to_datetime(
    # orders['material_availability_date'], errors='coerce'
    # )
    # orders['order_creation_date'] = pd.to_datetime(orders['order_creation_date'], errors='coerce')
    # orders['req_delivery_date'] = pd.to_datetime(orders['req_delivery_date'], errors='coerce')
    orders = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, 'VCP_DSP_AFC_EDP_FRC', 'D_SAP_EDP_FRA_DRY_ORD')
    customers = get_snowflake_df(
        config.SNOWFLAKE_DATABASE, 'VCP_DSP_AFC_EDP_FRC', 'D_SAP_EDP_FRA_DRY_CUS'
    )
    customers = ks.from_pandas(prepare_france_dairy_customers(customers, division))
    orders_red = orders[['cus_cod', 'plant_cod']].drop_duplicates()
    orders_red = merge_with_france_dairy_customers(orders_red, customers, 'cus_cod')
    orders_red['plant_cod'] = orders_red['plant_cod'].astype('int64')
    mapping_rfa_plt = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, 'VCP_DTM_AFC', 'EDP_FRC_WRH_RFA_REP_MAP'
    )
    orders_red = update_france_dairy_plant_cod(orders_red, mapping_rfa_plt, division, None, [1, 2])
    orders_red = orders_red[['rfa_cod', 'plant_cod']] \
        .drop_duplicates() \
        .rename(columns={'rfa_cod': 'Customer', 'plant_cod': 'WAREHOUSE'})
    df = df.drop('WAREHOUSE', axis=1)
    df['Customer'] = df['Customer'].astype(int)
    orders_red['Customer'] = orders_red['Customer'].astype(int)
    df = df.merge(orders_red.to_pandas(), on='Customer', how='left')
    return df


def format_and_aggregate_bb(
        df: pd.DataFrame
) -> pd.DataFrame:

    df = df.fillna(0)

    df["total_uplift"] = df \
        .filter(regex="uplift") \
        .sum(axis=1)
    df["to_dt"] = pd.to_datetime(df["to_dt"], utc=True)

    return df \
        .groupby(["fu_cod", "to_dt"]) \
        .total_uplift \
        .mean() \
        .reset_index() \
        .astype({"fu_cod": "int"}) \
        .rename(
            columns={
                "fu_cod": "FU",
                "to_dt": "TIME"
            }
        )


def apply_france_dairy_business_rules(df: pd.DataFrame):
    """ Apply all France business rules
    """
    promo_df = load_france_dairy_reg_promo_df()
    skus_lot = set(
        promo_df.loc[promo_df['product_type'] == 'Lot Promo']['mat_cod'].unique().to_numpy()
    )
    # promo_df_dates = get_promo_df_dates(df, promo_df)
    df.loc[df['SKU'].isin(skus_lot), 'Promotion'] = df.loc[df['SKU'].isin(skus_lot), 'Total']
    df.loc[df['SKU'].isin(skus_lot), 'Baseline'] = 0
    # df = df.apply(lambda x: bb_to_zero_no_promo(x, promo_df_dates, skus_lot), axis=1)
    df['Total'] = df['Baseline'] + df['Promotion']
    return df \
        .pipe(prev_to_zero_for_sunday_french_holidays) \
        .pipe(cap_negative_pred_to_zero)


def bb_to_zero_no_promo(row, promo_df: pd.DataFrame, skus_lot: pd.DataFrame):
    sku = row['SKU']
    rfa = row['Customer level 4']
    date = pd.Timestamp(row['TIME'])
    sub_df = promo_df.loc[(promo_df['mat_cod'] == sku) & (promo_df['rfa_cod'] == rfa)]['to_dt']
    if len(sub_df) == 0:
        if sku not in skus_lot:
            row['Baseline'] = row['Total']
        row['Promotion'] = 0
    else:
        dates = sorted(list(sub_df)[0])
        if date not in dates:
            if sku not in skus_lot:
                row['Baseline'] = row['Total']
            row['Promotion'] = 0
    return row


def cap_negative_pred_to_zero(
        df: pd.DataFrame
) -> pd.DataFrame:
    if len(df.loc[(df['Promotion'] < 0) | (df['Baseline'] < 0) | (df['Total'] < 0)]) > 0:
        if len(df.loc[(df['Promotion'] < 0) & (df['Baseline'] > 0) & (df['Total'] > 0)]) > 0:
            df.loc[(df['Promotion'] < 0) & (df['Baseline'] > 0), 'Baseline'] = df.loc[
                (df['Promotion'] < 0) & (df['Baseline'] > 0), 'Total']
            df.loc[(df['Promotion'] < 0) & (df['Baseline'] > 0), 'Promotion'] = 0
        if len(df.loc[(df['Promotion'] > 0) & (df['Baseline'] < 0) & (df['Total'] > 0)]) > 0:
            df.loc[(df['Promotion'] < 0) & (df['Baseline'] > 0), 'Baseline'] = df.loc[
                (df['Promotion'] < 0) & (df['Baseline'] > 0), 'Total']
            df.loc[(df['Promotion'] < 0) & (df['Baseline'] > 0), 'Promotion'] = 0
        if len(df.loc[df['Total'] < 0]) > 0:
            df.loc[(df['Total'] < 0), ['Total', 'Baseline', 'Promotion']] = 0
    return df


def prev_to_zero_for_sunday_french_holidays(df: pd.DataFrame) -> pd.DataFrame:
    """Put prevision to zero on holidays and bank holidays for France
    """
    holidays_df = get_holidays_df('france', "holidays&dayoff")
    df['TIME'] = pd.to_datetime(df['TIME'], utc=True)
    holidays_df['date'] = pd.to_datetime(holidays_df['date'], utc=True)
    holidays_df = holidays_df.loc[(
        holidays_df['date'] >= min(df['TIME'])) & (
        holidays_df['date'] <= max(df['TIME']))
    ]
    holidays_dates = set(holidays_df.loc[holidays_df['is_holiday'] == 1]['date'])
    df.loc[df['TIME'].isin(holidays_dates), ['Total', 'Baseline', 'Promotion']] = 0
    df['weekday'] = df['TIME'].map(lambda x: pd.Timestamp(x).weekday())
    df.loc[df['weekday'] == 6, ['Total', 'Baseline', 'Promotion']] = 0
    return df.drop('weekday', axis=1)


def load_france_dairy_reg_promo_df() -> pd.DataFrame:
    """Load and prepare promotion file for France dairy regular
    """
    mapping_cus_df = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, "VCP_DTM_AFC", 'EDP_FRC_CUS_MAP'
    ) \
        .pipe(cast_customer_ids)
    mapping_meca_df = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, "VCP_DTM_AFC", 'EDP_FRC_MEC'
    )
    promo_cas = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_FRC", "F_CAS_EDP_FRA_PRM_HIS"
    ) \
        .pipe(process_promo_cas, mapping_meca_df)
    promo_ezy = get_snowflake_df_spark(
        config.SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_FRC", "F_EZY_EDP_FRA_PRM_HIS"
    )
    promo_df = unify_promotions(promo_cas, promo_ezy, mapping_cus_df)
    return promo_df


def cast_and_rename_promos(
        promo_df: pd.DataFrame
) -> pd.DataFrame:
    promo_df = promo_df \
        .fillna({"total_uplifts": 0}) \
        .dropna(subset=["rfa_cod", "mat_cod"], how="any")
    promo_df = promo_df.astype(
        {
            "rfa_cod": "int",
            "mat_cod": "int",
            "to_dt": "str"
        }
    )
    promo_df = promo_df.rename(
        columns={"rfa_cod": "Customer level 4",
                 "mat_cod": "SKU",
                 "to_dt": "TIME"})
    return promo_df


def merge_bb_with_export_france_dairy(export_df, promo_uplift):
    export_df = export_df.merge(promo_uplift, on=["Customer level 4", "SKU", "TIME"], how="left")
    export_df = export_df.fillna({"total_uplifts": 0})
    export_df["total_uplift"] = export_df.Baseline * export_df.total_uplifts
    export_df.Promotion = export_df.total_uplift
    export_df.loc[(export_df.Promotion > export_df.Total), "Promotion"] = export_df.Total
    export_df.Baseline = export_df.Total - export_df.Promotion
    return export_df


def merge_bb_with_export(
        export_df, bb_df
) -> pd.DataFrame:

    if "DATABRICKS_RUNTIME_VERSION" in os.environ:
        for col in bb_df.select_dtypes(include="datetime64[ns, UTC]").columns:
            bb_df[col] = bb_df[col].dt.tz_localize(None)

    export_df = export_df \
        .merge(bb_df, on=["FU", "TIME"], how="left") \
        .fillna({"total_uplift": 0})

    export_df["mean_uplift"] = export_df.Baseline * export_df.total_uplift
    export_df.drop("total_uplift", axis=1, inplace=True)

    sum_uplifts = export_df \
        .groupby(["FU", "FORECASTED_FROM", "TIME"]) \
        .mean_uplift \
        .sum() \
        .reset_index() \
        .rename(columns={"mean_uplift": "total_uplift"})

    export_df.drop("mean_uplift", axis=1, inplace=True)
    export_df = export_df.merge(sum_uplifts, on=["FU", "FORECASTED_FROM", "TIME"], how="left")
    export_df.Promotion = export_df.total_uplift
    export_df.loc[(export_df.Promotion > export_df.Total), "Promotion"] = export_df.Total
    export_df.Baseline = export_df.Total - export_df.Promotion

    return export_df


def clean_export_from_bb(export_df) -> pd.DataFrame:
    """
    Cleans export dataframe from merged building blocks columns
    """
    agg_dict = {
        "Total": "sum",
        "Baseline": "sum",
        "Promotion": "sum",
        **{
            el: "first"
            for el in list(export_df.columns)
            if el not in ("FU", "FORECASTED_FROM", "TIME")
        }
    }
    export_df = export_df \
        .groupby(["FU", "FORECASTED_FROM", "TIME"]) \
        .agg(agg_dict) \
        .reset_index()

    to_round = ["Total", "Baseline", "Promotion"]
    export_df[to_round] = export_df[to_round].round(0)
    export_df = export_df.astype({col: "int32" for col in to_round})
    return export_df


def clean_export_from_bb_france_dairy(export_df) -> pd.DataFrame:
    """ Clean export dataframe from merged building blocks columns
    """
    export_df = export_df.drop(["total_uplifts", "total_uplift"], axis=1)
    agg_dict = {
        "Total": "sum", "Baseline": "sum", "Promotion": "sum",
        **{el: "first" for el in list(export_df.columns) if el
           not in ("Customer level 4", "SKU", "TIME", "Total", "Baseline", "Promotion")}
    }
    export_df = export_df \
        .groupby(["Customer level 4", "SKU", "TIME"]) \
        .agg(agg_dict) \
        .reset_index()

    to_round = ["Total", "Baseline", "Promotion"]
    export_df[to_round] = export_df[to_round].round(0)
    export_df = export_df.astype({col: "int32" for col in to_round})
    return export_df


def finalize_format_france_waters(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Finalize format of IBP export for France Waters
    """
    df["FU"] = df["FU"].astype(str)
    df["FU"] = df["FU"].apply(lambda x: "0" + x if len(x) < 6 else x)
    df["Sales Org"] = "00" + df["Sales Org"].astype(str)

    df.loc[df.Min < 0, "Min"] = 0
    df.loc[df.Max < 0, "Max"] = 0

    return df
