import logging
import numpy as np  # type: ignore
import pandas as pd  # type: ignore
from sqlalchemy import desc  # type: ignore
from adf.services.database import (
    get_session,
    BusinessUnits,
    DataprepReports
)
from adf.modelling.ibp_export.tools.utils import merge_daily_with_weekly, apply_plc
from adf.modelling.tools import get_snowflake_df


log = logging.getLogger("adf")


def apply_poland_dairy_rules(export_df, report):
    """ Set of poland dairy business rules to apply for generating
    the ibp export
    """
    export_df.loc[export_df.TIME.dt.weekday == 6, ("Total", "Baseline")] = 0
    country = report.description["country"]
    division = report.description["division"]
    products = get_snowflake_df("DEV_VCP", "VCP_DSP_AFC_EDP_PLD", "R_SAP_EDP_POL_PDT_HIE")[[
        "mat_cod", "mat_weight_unit", "mat_weight_value"
    ]].drop_duplicates()
    export_df = export_df.merge(
        products, left_on="SKU", right_on="mat_cod", how="left", validate="many_to_one"
    )
    export_df = convert_to_pieces(export_df)
    export_df = export_df.drop(columns=["mat_weight_unit", "mat_weight_value"])
    export_df = apply_plc(export_df, "poland", "dairy", "snowflake")
    export_df = export_df.astype({"Customer level 4": "int32", "Customer": "int32", "SKU": "int32"})
    export_df = apply_building_blocks(export_df, division, country)
    if report.description["time"]["level"] == "day":
        export_df = merge_daily_with_weekly(export_df, "poland", "dairy")
    export_df["Sales Org"] = "00" + export_df["Sales Org"].astype(str)
    export_df["Customer level 4"] = "0" + export_df["Customer level 4"].astype(str)
    export_df["Customer"] = "0" + export_df["Customer"].astype(str)
    export_df["WAREHOUSE"] = "0694"
    return export_df


def convert_to_pieces(df: pd.DataFrame) -> pd.DataFrame:
    mask_kg = df["mat_weight_unit"] == "KG"
    mask_g = df["mat_weight_unit"] == "G"
    mask_lb = df["mat_weight_unit"] == "LB"
    conversion_cols = [
        "Total",
        "Baseline",
        "Min",
        "Max"
    ]
    for col in conversion_cols:
        df.loc[mask_kg, col] = (
            df.loc[mask_kg, col] * 1000 / df.loc[mask_kg, "mat_weight_value"]
        )
        df.loc[mask_g, col] = (
            df.loc[mask_g, col] * 1000000 / df.loc[mask_g, "mat_weight_value"]
        )
        df.loc[mask_lb, col] = (
            df.loc[mask_lb, col] * 1000 / (df.loc[mask_lb, "mat_weight_value"] * 0.453)
        )
    return df


def apply_poland_waters_rules(export_df, report):
    """ Set of poland waters business rules to apply for generating
    the ibp export
    """
    export_df = export_df.astype({"FU": "int32"})
    export_df["Total"] = export_df["Total"] * 1000
    export_df["Baseline"] = export_df["Baseline"] * 1000
    export_df["Min"] = export_df["Min"] * 1000
    export_df["Max"] = export_df["Max"] * 1000
    if report.description["time"]["level"] == "day":
        export_df = merge_daily_with_weekly(export_df, "poland", "waters")
    export_df["FU"] = export_df["FU"].astype(str)
    export_df["FU"] = export_df["FU"].apply(lambda x: "0" + x if len(x) < 6 else x)
    export_df["Sales Org"] = "00" + export_df["Sales Org"].astype(str)
    return export_df


def apply_building_blocks(export_df: pd.DataFrame, division, country) -> pd.DataFrame:
    """ Merge promo building block table with ibp export
    """
    session = get_session()
    business_unit = get_session().query(BusinessUnits) \
        .filter(BusinessUnits.country == country) \
        .filter(BusinessUnits.division == division) \
        .first()

    if business_unit:
        promo_report = session.query(DataprepReports). \
            filter(DataprepReports.status == "success"). \
            filter(DataprepReports.scope == "panel"). \
            filter(DataprepReports.business_unit_id == business_unit.id). \
            order_by(desc(DataprepReports.updated_at)). \
            first()
        promo_df = promo_report.download()
        promo_df = promo_df. \
            pipe(cast_promo). \
            pipe(rename_promo)
        promo_df["TIME"] = pd.to_datetime(promo_df["TIME"])
        return export_df. \
            pipe(merge_promo_with_export, promo_df). \
            pipe(clean_export_from_promo)


def cast_promo(promo_df) -> pd.DataFrame:
    promo_df = promo_df.replace(np.nan, 0)
    promo_df = promo_df.astype(
        {
            "rfa_cod": "int",
            "mat_cod": "int"
        }
    )
    return promo_df


def rename_promo(promo_df) -> pd.DataFrame:
    """ Merge promo table with building blocks table
    """
    promo_df = promo_df.rename(
        columns={
            "rfa_cod": "Customer level 4",
            "mat_cod": "SKU",
            "req_delivery_date": "TIME"
        }
    )
    return promo_df


def merge_promo_with_export(export_df, bb_df):
    export_df = export_df.merge(bb_df, on=["Customer level 4", "SKU", "TIME"], how="left")
    export_df["mean_uplift"] = export_df.Baseline * export_df.total_uplifts
    sum_uplifts = export_df.groupby(
        ["Customer level 4", "SKU", "FORECASTED_FROM", "TIME"]
    )["mean_uplift"].sum()
    sum_uplifts = sum_uplifts.reset_index().rename(columns={"mean_uplift": "total_uplift"})
    export_df = export_df.merge(
        sum_uplifts, on=["Customer level 4", "SKU", "FORECASTED_FROM", "TIME"], how="left"
    )
    export_df.Promotion = export_df.total_uplift
    export_df.loc[(export_df.Promotion > export_df.Total), "Promotion"] = export_df.Total
    export_df.Baseline = export_df.Total - export_df.Promotion

    return export_df


def clean_export_from_promo(export_df) -> pd.DataFrame:
    """ Clean export dataframe from merged building blocks columns
    """
    # uplift_cols = list(export_df.filter(regex=".uplift*").columns)
    # promo_cols = [
    #    "region",
    #    "ordered_volumes"
    # ]
    # export_df = export_df.drop(uplift_cols + promo_cols, axis=1)
    agg_dict = {
        "Total": "sum", "Baseline": "sum", "Promotion": "sum",
        **{
            el: "first"
            for el in list(export_df.columns)
            if el not in (
                "Customer level 4", "SKU", "FORECASTED_FROM", "TIME",
                "Total", "Baseline", "Promotion"
            )
        }
    }
    export_df = export_df.groupby(
        ["Customer level 4", "SKU", "FORECASTED_FROM", "TIME"]
    ).agg(agg_dict).reset_index()

    to_round = ["Total", "Baseline", "Promotion"]
    export_df[to_round] = export_df[to_round].round(0)
    export_df = export_df.astype({col: "int32" for col in to_round})
    return export_df
