import pydash as py_  # type: ignore
from adf.services.database import get_session
from adf.services.database import IBPReports
from adf.services.database.query import (
    build_report, get_business_unit
)


def build_ibp_report(
        country,
        division,
        description,
        status,
        reason,
        pipeline_id,
        pipeline_run_type
):
    session = get_session()
    bu = get_business_unit(session, country, division)
    return build_report(
        session,
        IBPReports,
        business_unit_id=bu.id,
        description=description,
        status=status,
        reason=reason,
        pipeline_id=pipeline_id,
        pipeline_run_type=pipeline_run_type
    )


def get_iso_reports(session, report_ids, report_model):
    """ Fetch reports given report ids. Only full ids are supported

    It is required the following attributes to be ISO:
        - country
        - division
        - product
        - location
        - time
    """
    reports = session.query(report_model) \
        .filter(report_model.id.in_(report_ids)) \
        .filter(report_model.status == "success") \
        .all()

    if len(report_ids) != len(reports):
        raise ValueError(
            "Expected %s reports but found %s" %
            (len(report_ids), len(reports))
        )

    descriptions = [
        {
            "country": report.description["tags"]["country"],
            "division": report.description["tags"]["division"],
            "product": report.description["product"],
            "location": report.description["location"],
            "time": report.description["time"]
        } for report in reports
    ]

    unique_descriptions = py_.uniq(descriptions)

    if len(unique_descriptions) > 1:
        raise ValueError("Base reports are not ISO")

    return reports, unique_descriptions[0]
