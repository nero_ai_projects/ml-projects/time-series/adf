import logging
import numpy as np  # type: ignore
import pandas as pd  # type: ignore
import databricks.koalas as ks  # type: ignore
import pyspark.sql.functions as sf  # type: ignore
import pyspark.sql.types as st  # type: ignore
from sqlalchemy import desc  # type: ignore
from adf.modelling.tools import get_snowflake_df_spark
from adf.utils.mappings import get_mappings
from adf.modelling.dataprep.spain.dairy.plc import get_core_plc
from adf.modelling.ibp_export.tools.utils import merge_daily_with_weekly
from adf.services.database import get_session, BusinessUnits, DataprepReports, BuildingBlocksReports
from adf.config import SPAIN_PRIMARY_WAREHOUSES, SNOWFLAKE_DATABASE

log = logging.getLogger("adf")


def apply_spain_dairy_rules(export_df, report):
    """Set of spain dairy business rules to apply for generating
    the ibp export
    """
    if report.description["time"]["level"] == "day":
        export_df = merge_daily_with_weekly(export_df, "spain", "dairy")

    export_df = ks.DataFrame(export_df)
    export_df = export_df.drop_duplicates()
    log.info("Apply PLC rules")
    export_df = apply_plc(export_df)
    log.info("Remove orders on warehouse closure")
    export_df = remove_orders_on_warehouse_closure(export_df)
    log.info("Apply building blocks")
    export_df = apply_building_blocks(export_df, "spain", "dairy")
    export_df = export_df.astype({"WAREHOUSE": "int32", "SKU": "int32"})
    export_df["WAREHOUSE"] = "0" + export_df["WAREHOUSE"].astype(str)
    export_df["Sales Org"] = "00" + export_df["Sales Org"].astype(str)
    export_df = export_df.drop_duplicates()
    return export_df


def apply_plc(export_df: ks.DataFrame):
    """Considering the Product Lifecycle data, some products are
    renovated and have to be phased-in (e.g start being forecasted) during
    the forecast horizon, and some products are delisted and have to be
    phased-out (e.g stop being forecasted)
    """
    core_plc_scope = get_core_plc()
    export_df.TIME = ks.to_datetime(export_df.TIME, format="%Y-%m-%d", errors="coerce")
    export_df = export_df.to_spark()

    null_cols = [
        f.name
        for f in export_df.schema.fields
        if isinstance(
            f.dataType, st.NullType
        )
    ]
    for col_name in null_cols:
        export_df = export_df.withColumn(col_name, sf.col(col_name).cast(st.StringType()))
    export_df = apply_renovation(export_df, core_plc_scope)
    export_df = apply_delisting(export_df, core_plc_scope)
    export_df = export_df.drop_duplicates()
    return export_df


def remove_orders_on_warehouse_closure(df):
    """Multiply forecast volume by the warehouse opening
    coefficient (1 if open, 0 if closed, ]0;1[ if partially opened.
    """
    warehouse_calendar_df = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_SPN", "R_MAN_EDP_SPN_CAL_WRH", "koalas"
    )

    warehouse_calendar_df = warehouse_calendar_df.drop("sales_organization_cod", axis=1)
    warehouse_calendar_df["to_dt"] = ks.to_datetime(warehouse_calendar_df["to_dt"])
    warehouse_calendar_df = warehouse_calendar_df.drop_duplicates(
        subset=["to_dt", "warehouse_cod"], keep="last"
    )
    df = df.merge(
        warehouse_calendar_df,
        how="left",
        left_on=["TIME", "WAREHOUSE"],
        right_on=["to_dt", "warehouse_cod"],
    )
    df["Total"] = df["Total"] * df.warehouse_opening
    df["Baseline"] = df["Baseline"] * df.warehouse_opening
    return df


def apply_renovation(export_df, core_plc_scope):
    """Take phase-in start dates from the PLC file,
    then make the renovated products forecasts start from those dates
    """
    scope_reno = core_plc_scope.where(sf.col("phasein_start_date").isNotNull())
    scope_reno_products = scope_reno.select(
        sf.col("mat_cod").alias("SKU"),
        sf.col("phasein_start_date").alias("renovation_date"))

    to_drop = export_df.join(
        scope_reno_products,
        on="SKU",
        how="inner"
    ).where(
        sf.col("TIME") < sf.col("renovation_date")
    ).select("SKU").distinct()

    export_df = export_df.join(to_drop, on="SKU", how="left_anti")
    return export_df


def apply_delisting(export_df, core_plc_scope):
    """Take phase-out end dates from the PLC file,
    then make the delisted products forecasts end at those end
    """
    scope_delisting = core_plc_scope.where(
        (
            sf.col("phasein_start_date").isNotNull()
        ) & (
            sf.col("mat_cod") == sf.col("mat_cod_reference")
        )
    )

    scope_delisting_products = scope_delisting.select(
        sf.col("mat_cod_reference").alias("SKU"), "phaseout_start_date", "phaseout_end_date"
    ).drop_duplicates()

    delisting = export_df.join(scope_delisting_products, on="SKU", how="inner")

    delisting = delisting.groupBy("SKU").applyInPandas(apply_linear_phaseout, delisting.schema)

    to_clip = delisting.where(sf.col("TIME") > sf.col("phaseout_end_date"))

    to_clip = to_clip.withColumn(
        "Total",
        sf.lit(0)
    ).withColumn(
        "Baseline",
        sf.lit(0)
    )

    delisting = delisting.join(to_clip.select("SKU").distinct(), on="SKU", how="left_anti")

    delisting = delisting.unionByName(to_clip)

    to_drop_cols = ["phaseout_start_date", "phaseout_end_date"]
    delisting = delisting.drop(*to_drop_cols)

    export_df = export_df.join(delisting.select("SKU").distinct(), on="SKU", how="left_anti")

    export_df = export_df.unionByName(delisting)

    export_df = ks.DataFrame(export_df)

    export_df.TIME = ks.to_datetime(export_df.TIME, format="%Y-%m-%d")

    cols_to_replace = export_df.select_dtypes(exclude="datetime64[ns]").columns
    for col in cols_to_replace:
        export_df[col] = export_df[col].replace("", np.nan)
    export_df = export_df.dropna(subset=["TIME", "Total"])
    return export_df


def apply_linear_phaseout(delisting):
    """Make delisted SKUs forecasts go down to zero, between their two
    phase-out dates, by following a linear curve.
    """
    to_curve = delisting.loc[
        (
            delisting.TIME >= delisting.phaseout_start_date
        ) & (
            delisting.TIME <= delisting.phaseout_end_date
        )
    ]

    if len(to_curve) == 0:
        return delisting

    for primary in SPAIN_PRIMARY_WAREHOUSES:
        to_curve_p = to_curve.loc[to_curve.WAREHOUSE == primary]
        if len(to_curve_p) == 0:
            break
        total = to_curve_p.head(1).Total.values[0]
        counter = 1
        for index_, row_ in to_curve_p.iterrows():

            to_curve_p.loc[index_, "Total"] = total - (counter / len(to_curve_p) * total)

            to_curve_p.loc[index_, "Baseline"] = total - (counter / len(to_curve_p) * total)
            counter += 1

        delisting.update(to_curve_p)

    return delisting


def apply_building_blocks(export_df: ks.DataFrame, country: str, division: str) -> ks.DataFrame:
    """Merge promo building block table with ibp export"""
    session = get_session()
    business_unit = (
        session.query(BusinessUnits)
        .filter(BusinessUnits.country == country)
        .filter(BusinessUnits.division == division)
        .first()
    )

    bb_report = (
        session.query(BuildingBlocksReports)
        .filter(BuildingBlocksReports.status == "success")
        .filter(BuildingBlocksReports.business_unit_id == business_unit.id)
        .filter(BuildingBlocksReports.scope == "promo_building_block")
        .order_by(desc(BuildingBlocksReports.updated_at))
        .first()
    )

    bb_table = bb_report.download(object_type="koalas")

    for col in bb_table.filter(regex=".uplift*").columns:
        bb_table.loc[(bb_table[col] > 1), col] = 1

    promo_report = (
        session.query(DataprepReports)
        .filter(DataprepReports.status == "success")
        .filter(DataprepReports.business_unit_id == business_unit.id)
        .filter(DataprepReports.scope == "promo")
        .filter(DataprepReports.prep_type == "promotion")
        .order_by(desc(DataprepReports.updated_at))
        .first()
    )

    promo_df = promo_report.download(object_type="koalas")

    log.info("Transform promo")
    promo_df = promo_df.pipe(merge_to_customers).pipe(cast_promo)

    log.info("Merge promo with building blocks")
    bb_df = merge_bb_promo(bb_table, promo_df)

    return export_df.pipe(merge_bb_with_export, bb_df).pipe(clean_export_from_bb)


def merge_to_customers(promo_df) -> ks.DataFrame:
    """Merge promo table with customer hierarchy to get
    the franchise codes
    """
    customers = get_snowflake_df_spark(
        SNOWFLAKE_DATABASE, "VCP_DSP_AFC_EDP_SPN", "R_MAN_EDP_SPN_CUS_SAL", "koalas"
    )

    promo_types = get_mappings("spain", "dairy", "promo_types", object_type="koalas")
    customers = customers[["cus_cod", "franchise_cod"]]
    promo_df = promo_df.merge(customers, on=["cus_cod"], how="left")
    promo_df = promo_df.merge(promo_types, on=["promotion_mechanics_cod"], how="left")
    promo_df = promo_df.loc[~promo_df.franchise_cod.isin(["####"])]
    return promo_df


def cast_promo(promo_df) -> ks.DataFrame:
    cols_to_fillna = promo_df.select_dtypes(exclude="datetime64[ns]").columns
    for col in cols_to_fillna:
        promo_df[col] = promo_df[col].fillna(0)
    promo_df = promo_df.astype({"warehouse_cod": "int", "mat_cod": "int", "franchise_cod": "int"})
    return promo_df


def merge_bb_promo(bb_table, promo_df) -> ks.DataFrame:
    """Merge promo table with building blocks table"""
    bb_table = bb_table.astype({"warehouse_cod": "int", "mat_cod": "int", "franchise_cod": "int"})

    bb_df = promo_df.merge(bb_table, on=["warehouse_cod", "mat_cod", "franchise_cod"], how="left")

    cols_to_fillna = bb_df.select_dtypes(exclude="datetime64[ns]").columns
    for col in cols_to_fillna:
        bb_df[col] = bb_df[col].fillna(0)

    grouper = []

    for col in bb_df.columns:
        if col not in ("mat_cod", "warehouse_cod", "cus_cod", "to_dt", "franchise_cod"):
            grouper.append(col)

    bb_df = (
        bb_df.groupby(["mat_cod", "franchise_cod", "warehouse_cod", "to_dt"])
        .agg({col: "first" for col in grouper})
        .reset_index()
    )

    bb_df = bb_df.rename(columns={"warehouse_cod": "WAREHOUSE", "mat_cod": "SKU", "to_dt": "TIME"})

    return bb_df


def merge_bb_with_export(export_df, bb_df):
    log.info("Merge Export with Building blocks")
    export_df = export_df.merge(bb_df, on=["WAREHOUSE", "SKU", "TIME"], how="left")

    cols_to_fillna = export_df.select_dtypes(exclude="datetime64[ns]").columns
    for col in cols_to_fillna:
        export_df[col] = export_df[col].fillna(0)

    export_df["mean_uplift"] = (
        (export_df.Baseline * export_df.crm_uplift) +
        (export_df.Baseline * export_df.mc_uplift) +
        (export_df.Baseline * export_df.tpr_uplift)
    )

    sum_uplifts = export_df.groupby(["WAREHOUSE", "SKU", "FORECASTED_FROM", "TIME"])[
        "mean_uplift"
    ].sum()
    sum_uplifts = sum_uplifts.reset_index().rename(columns={"mean_uplift": "total_uplift"})

    export_df = export_df.merge(
        sum_uplifts, on=["WAREHOUSE", "SKU", "FORECASTED_FROM", "TIME"], how="left"
    )
    export_df.Promotion = export_df.total_uplift
    export_df.loc[(export_df.Promotion > export_df.Total), "Promotion"] = export_df.Total
    export_df.Baseline = export_df.Total - export_df.Promotion

    return export_df


def clean_export_from_bb(export_df) -> pd.DataFrame:
    """Clean export dataframe from merged building blocks columns"""
    log.info("Clean export from building blocks")
    uplift_cols = list(export_df.filter(regex=".uplift*").columns)
    promo_cols = [
        "franchise_cod",
        "promotion_mechanics_cod",
        "adjusted_units",
        "promotion_type_desc",
    ]
    export_df = export_df.drop(uplift_cols + promo_cols, axis=1)
    agg_dict = {
        el: "first"
        for el in list(export_df.columns)
        if el not in ("WAREHOUSE", "SKU", "FORECASTED_FROM", "TIME")
    }
    export_df = (
        export_df.groupby(["WAREHOUSE", "SKU", "FORECASTED_FROM", "TIME"])
        .agg(agg_dict)
        .reset_index()
    )

    to_round = ["Total", "Baseline", "Promotion"]
    export_df[to_round] = export_df[to_round].round(0)
    export_df = export_df.astype({col: "int32" for col in to_round})
    return export_df.to_pandas()
