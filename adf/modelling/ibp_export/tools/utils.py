import logging
import pandas as pd  # type: ignore
from pandas.core.tools.datetimes import to_datetime  # type: ignore
import numpy as np  # type: ignore
import databricks.koalas as ks  # type: ignore
from datetime import datetime, timedelta
from sqlalchemy import desc  # type: ignore
from dateutil.easter import easter  # type: ignore
from pathlib import Path  # type: ignore
from snowflake.connector.pandas_tools import write_pandas  # type: ignore
from adf.utils.fileutils import load_json
from adf.services.database import (
    get_session,
    PipelineReports,
    BusinessUnits,
    IBPReports,
    SelloutIBPReports
)
from adf.types import DataFrame
from adf.config import (
    SNOWFLAKE_DATABASE,
    SNOWFLAKE_WAREHOUSE,
    SNOWFLAKE_SCHEMA
)
from adf.utils import get_snowflake_connector
from adf.utils.decorators import timeit, showcolumns
from adf.modelling.dataprep.utils.plc import get_core_plc


SPAIN_STATIC_WEIGHTED_DISTRIBUTIONS = load_json(
    str(Path(__file__).parent / "./spain/weighted_distributions.json")
)


log = logging.getLogger("adf")


def get_dataprep_columns(config: dict):
    product_column = config["product"]["column"]
    location_column = config["location"]["column"]
    customer_column = config["customer"]["column"]
    date_column = config["time"]["column"]
    return product_column, location_column, customer_column, date_column


@timeit
def fill_missing_columns(prod_col, loc_col, cus_col, dataprep_df):
    if not prod_col or prod_col not in dataprep_df:
        prod_col = "all_products"
        dataprep_df[prod_col] = 0
    if not loc_col or loc_col not in dataprep_df:
        loc_col = "all_locations"
        dataprep_df[loc_col] = 0
    if not cus_col or cus_col not in dataprep_df:
        cus_col = "all_customers"
        dataprep_df[cus_col] = 0
    return prod_col, loc_col, cus_col, dataprep_df


def get_params(prod_col, loc_col, cus_col, time_col, target_col):
    prod_col = prod_col if isinstance(prod_col, tuple) else (prod_col, False)
    loc_col = loc_col if isinstance(loc_col, tuple) else (loc_col, False)
    cus_col = cus_col if isinstance(cus_col, tuple) else (cus_col, False)
    time_col = time_col if isinstance(time_col, tuple) else (time_col, False)
    target_col = target_col if isinstance(target_col, tuple) else (target_col, False)

    group_cols = [col for col, ignore in [prod_col, loc_col, cus_col] if not ignore]
    ignore = {
        col
        for col, ignore in [
            prod_col,
            loc_col,
            cus_col,
            time_col,
            target_col,
        ] if ignore
    }
    granularity_cols = (
        ["product_code"] if prod_col[0] not in ignore else []
    ) + (
        ["location_code"] if loc_col[0] not in ignore else []
    ) + (
        ["customer_code"] if cus_col[0] not in ignore else []
    )
    return {
        "prod_col": prod_col[0],
        "loc_col": loc_col[0],
        "cus_col": cus_col[0],
        "time_col": time_col[0],
        "group_cols_week": group_cols + ["week_start"],
        "group_cols_day": group_cols + ["week_day"],
        "target": target_col[0],
        "ignore": ignore,
        "granularity_cols": granularity_cols,
    }


@timeit
@showcolumns
def set_date_columns(df: DataFrame, params: dict):
    df[params["time_col"]] = ks.to_datetime(df[params["time_col"]])
    df['week_day'] = df[params["time_col"]].dt.dayofweek
    df['week_start'] = df.apply(
        lambda x: x[params["time_col"]] - x.week_day * timedelta(days=1), axis=1
    )
    return df


@timeit
@showcolumns
def calculate_weekly_quantities(dataprep_df: DataFrame, params: dict):
    df = ks.merge(
        dataprep_df.groupby(
            list(set(params["group_cols_day"]) - {"week_day"}) + [params["time_col"]]
        ).agg(
            {"week_day": "first", "week_start": "first", params["target"]: "sum"}
        ).reset_index(),
        dataprep_df.groupby(
            params["group_cols_week"])[params["target"]].sum().reset_index(name="week_qty"),
        how="left",
        on=params["group_cols_week"]
    )
    return df


@timeit
@showcolumns
def calculate_rolling_mean_weights(dataprep_df: DataFrame, params: dict):
    dataprep_df["weight"] = (dataprep_df[params["target"]] / dataprep_df.week_qty).fillna(0)
    dataprep_df = dataprep_df.sort_values(
        list(set(params["group_cols_week"]) - set("week_start")) + [params["time_col"]]
    )
    dataprep_df["rolling_weight"] = dataprep_df \
        .groupby(params["group_cols_day"]) \
        .weight \
        .apply(lambda x: x.rolling(10, 1).mean())
    return dataprep_df


@timeit
@showcolumns
def reduce_dataprep(dataprep_df: DataFrame, min_date, params):
    horizon = params["horizon"]
    return dataprep_df.loc[
        dataprep_df.week_start >= min_date - timedelta(days=horizon * 7 + 1)
    ]


@timeit
@showcolumns
def lag_weights(dataprep_df: DataFrame, params: dict):
    for h in range(params["horizon"] + 1):
        dataprep_df[h] = dataprep_df.groupby(params["group_cols_day"]). \
            rolling_weight.shift(periods=h + 1)
        dataprep_df[h] = dataprep_df[h].mask(dataprep_df[h].isnull(), dataprep_df.weight)
    return dataprep_df


@timeit
@showcolumns
def pivot_dataset(dataprep_df: DataFrame, params: dict):
    dataprep_df = pd.melt(
        dataprep_df,
        id_vars=[params["time_col"]] + params["group_cols_week"],
        value_vars=[h for h in range(params["horizon"] + 1)],
        var_name="week_gap",
        value_name="weight"
    )
    dataprep_df.week_gap = pd.to_numeric(dataprep_df.week_gap)
    column_renaming = {
        params["prod_col"]: "product_code",
        params["loc_col"]: "location_code",
        params["cus_col"]: "customer_code",
        "week_start": "target_date"
    }
    return dataprep_df.rename(columns=column_renaming)


@timeit
@showcolumns
def calculate_forecast_weights(prerequired_forecasts: DataFrame, params: dict):
    daily_prerequired_forecasts = prerequired_forecasts.groupby(
        params["granularity_cols"] + ["prediction_date", "target_date", "week_gap"]
    ).prediction.sum().reset_index()
    weekly_prerequired_forecasts = prerequired_forecasts.groupby(
        params["granularity_cols"] + ["prediction_date", "week_gap"]
    ).prediction.sum().reset_index()
    prerequired_forecasts = daily_prerequired_forecasts.merge(
        weekly_prerequired_forecasts,
        on=params["granularity_cols"] + ["prediction_date", "week_gap"],
        suffixes=("_day", "_week")
    )
    prerequired_forecasts["weight"] = (
        prerequired_forecasts.prediction_day / prerequired_forecasts.prediction_week
    )
    return prerequired_forecasts


@timeit
@showcolumns
def format_forecasts(prerequired_forecasts: DataFrame, params):
    time_col = params["time_col"]
    prerequired_forecasts = prerequired_forecasts.rename(columns={"target_date": time_col})
    prerequired_forecasts["target_date"] = prerequired_forecasts[time_col].apply(
        lambda x: x - pd.to_timedelta(x.weekday(), unit="D")
    )
    return prerequired_forecasts


@timeit
@showcolumns
def update_weights(
    dataprep_df: DataFrame, prerequired_forecasts: pd.DataFrame, params: dict
):
    log.info("Updating daily weights for disaggregation, from prerequired forecasts")
    time_col = params["time_col"]
    prerequired_forecasts = prerequired_forecasts[
        [time_col] + params["granularity_cols"] + ["target_date", "week_gap", "weight"]
    ]
    dataprep_df = dataprep_df.set_index(
        [time_col] + params["granularity_cols"] + ["week_gap"]
    )
    prerequired_forecasts = prerequired_forecasts.set_index(
        [time_col] + params["granularity_cols"] + ["week_gap"]
    )
    dataprep_df.update(
        prerequired_forecasts
    )
    dataprep_df = dataprep_df.append(
        prerequired_forecasts.loc[~prerequired_forecasts.index.isin(dataprep_df.index)]
    )
    dataprep_df = dataprep_df.reset_index()
    return dataprep_df


@timeit
@showcolumns
def normalize_weights(dataprep_df, params, ignore_weekdays=None, ignore_dates=None, step=0):
    if ignore_weekdays is None:
        ignore_weekdays = [5, 6]
    df_list = []
    for h in range(params["horizon"] + 1):
        weight_sum_df = dataprep_df.query(f'week_gap=={h}').groupby(
            params["granularity_cols"] + ["target_date", "week_gap"]
        )['weight'].sum().reset_index(name='weight_sum')
        df_list.append(weight_sum_df)
    dataprep_df = dataprep_df.merge(
        pd.concat(df_list),
        how="left",
        on=params["granularity_cols"] + ["target_date", "week_gap"]
    )
    dataprep_df["weight"] = (dataprep_df["weight"] / dataprep_df["weight_sum"])
    if ignore_dates is not None and len(list(ignore_dates)):
        dataprep_df["weight"] = dataprep_df["weight"].mask(
            (
                dataprep_df[params["time_col"]].isin(ignore_dates)
            ) & (
                dataprep_df.weight.isnull()
            ),
            0
        )
    dataprep_df["weight"] = dataprep_df["weight"].mask(
        (
            ~dataprep_df[params["time_col"]].dt.weekday.isin(ignore_weekdays)
        ) & (
            dataprep_df.weight.isnull()
        ),
        1 / (7 - len(ignore_weekdays))
    )
    dataprep_df["weight"] = dataprep_df["weight"].mask(
        (
            dataprep_df[params["time_col"]].dt.weekday.isin(ignore_weekdays)
        ) & (
            dataprep_df.weight.isnull()
        ),
        0
    )

    dataprep_df = dataprep_df.drop(columns=["weight_sum"])
    dataprep_df["weight"] = dataprep_df["weight"].mask(dataprep_df.weight < 0, 0)
    dataprep_df["weight"] = dataprep_df["weight"].mask(dataprep_df.weight > 1, 1)
    if not step:
        dataprep_df = normalize_weights(
            dataprep_df, params, ignore_weekdays=ignore_weekdays, ignore_dates=ignore_dates, step=1
        )
    return dataprep_df


@timeit
@showcolumns
def disaggregate_to_daily(weighted_table, forecasts, dataprep_df, config, backtest, params):
    forecasts = ks.merge(
        forecasts,
        weighted_table,
        how="left",
        on=params["granularity_cols"] + ["target_date", "week_gap"]
    )
    forecasts.prediction = forecasts.prediction * forecasts.weight
    forecasts.min_prediction = forecasts.min_prediction * forecasts.weight
    forecasts.max_prediction = forecasts.max_prediction * forecasts.weight
    date_column = config["time"]["column"]

    if backtest:
        forecasts = retrieve_daily_ground_truth(forecasts, dataprep_df, params)
    else:
        forecasts = forecasts \
            .drop(["target_date"], axis=1) \
            .rename(columns={date_column: "target_date"})

    forecasts["target_date"] = pd.DatetimeIndex(forecasts["target_date"]).tz_localize(None)
    forecasts["prediction_date"] = pd.DatetimeIndex(forecasts["prediction_date"]).tz_localize(None)

    forecasts["day_gap"] = (forecasts["target_date"] - forecasts["prediction_date"]).dt.days
    forecasts["time_base"] = "day"
    return forecasts


@timeit
@showcolumns
def retrieve_daily_ground_truth(forecasts, dataprep_df, params):
    date_column = params["time_col"]
    product_column = params["prod_col"]
    location_column = params["loc_col"]
    customer_column = params["cus_col"]

    dataprep_df = dataprep_df[
        [product_column, location_column, customer_column, date_column, params["target"]]
    ].rename(
        columns={
            product_column: "product_code",
            location_column: "location_code",
            customer_column: "customer_code"
        }
    )
    if not isinstance(dataprep_df, pd.DataFrame):
        dataprep_df = dataprep_df.to_pandas()

    forecasts[date_column] = pd.DatetimeIndex(forecasts[date_column]).tz_localize(None)
    dataprep_df[date_column] = pd.DatetimeIndex(dataprep_df[date_column]).tz_localize(None)

    return forecasts \
        .merge(dataprep_df, on=["product_code", "location_code", "customer_code", date_column]) \
        .drop(["ground_truth", "target_date"], axis=1) \
        .rename(columns={params["target"]: "ground_truth", date_column: "target_date"}) \
        .assign(time_base='day')


@timeit
@showcolumns
def apply_spain_weighted_distrib(dataprep_df):

    weeks_mapping = {}
    weeks_mapping = load_easter_weeks_mappings(dataprep_df, weeks_mapping)
    weights = pd.json_normalize(SPAIN_STATIC_WEIGHTED_DISTRIBUTIONS)

    for column in weights.columns:
        col = column.split(".")
        week_name, warehouse, day = col[0], int(col[1]), int(col[2])
        week_number = weeks_mapping.get(week_name, None)
        ratio = weights[column][0]
        if "easter" in week_name:
            dataprep_df["weight"] = dataprep_df["weight"].mask(
                (
                    dataprep_df.to_dt.dt.isocalendar().week == week_number
                ) & (
                    dataprep_df.location_code == warehouse
                ) & (
                    dataprep_df.to_dt.dt.dayofweek == day
                ), ratio
            )

        if "1st_may_2021" in week_name:
            dataprep_df["weight"] = dataprep_df["weight"].mask(
                (
                    dataprep_df.to_dt.dt.isocalendar().week == 17
                ) & (
                    dataprep_df.to_dt.dt.year == 2021
                ) & (
                    dataprep_df.location_code == warehouse
                ) & (
                    dataprep_df.to_dt.dt.dayofweek == day
                ), ratio
            )

    return dataprep_df


@timeit
def load_easter_weeks_mappings(dataprep_df, mappings):
    dataprep_df.target_date = to_datetime(dataprep_df.target_date)
    easter_mappings = {}

    dataprep_easter_period = dataprep_df.loc[
        (
            dataprep_df.target_date.dt.month > 2
        ) & (
            dataprep_df.target_date.dt.month < 5
        )
    ]

    dataprep_contains_easter = len(dataprep_easter_period) > 0

    if dataprep_contains_easter:
        year = list(dataprep_easter_period.target_date.dt.year.unique().tolist())[0]
        easter_holy_week = easter(year).isocalendar()[1]
        easter_preload = easter_holy_week - 1
        easter_week = easter_holy_week + 1

        easter_mappings = {
            "easter_preload": easter_preload,
            "easter_holy_week": easter_holy_week,
            "easter": easter_week
        }
    return {**mappings, **easter_mappings}


def merge_daily_with_weekly(
        export_df, country, division
) -> pd.DataFrame:
    """ Merge daily forecasts with the last made weekly forecasts
    as final forecast results must cover an horizon of 14 weeks.
    """
    try:
        session = get_session()
        business_unit = session.query(BusinessUnits) \
            .filter(BusinessUnits.country == country) \
            .filter(BusinessUnits.division == division) \
            .first()
        weekly_pipeline = session.query(PipelineReports). \
            filter(PipelineReports.algorithm_type == "week"). \
            filter(PipelineReports.run_type == "core"). \
            filter(PipelineReports.status == "success"). \
            filter(PipelineReports.ibp_report.any(business_unit_id=business_unit.id)). \
            order_by(desc(PipelineReports.updated_at)). \
            first()
        if weekly_pipeline:
            weekly_forecasts = weekly_pipeline.ibp_report[0].download_latest_processed(sep=";")
            weekly_forecasts["TIME"] = pd.to_datetime(weekly_forecasts["TIME"])
            weekly_forecasts = weekly_forecasts.loc[weekly_forecasts.TIME >= export_df.TIME.min()]
            weekly_forecasts["FORECASTED_FROM"] = export_df["FORECASTED_FROM"].max()
            weekly_forecasts = weekly_forecasts.loc[
                ~weekly_forecasts.TIME.isin(export_df.TIME.unique())
            ]
            export_df = pd.concat([export_df, weekly_forecasts], axis=0)
        return export_df
    except FileNotFoundError:
        log.error('No weekly forecasts found - returning daily forecasts only')
        return export_df


def apply_plc(df: pd.DataFrame, country: str, division: str, source: str = "dbfs") -> pd.DataFrame:
    """ Considering the Product Lifecycle data, some products are
    renovated and have to be phased-in (e.g start being forecasted) during
    the forecast horizon, and some products are delisted and have to be
    phased-out (e.g stop being forecasted)
    """
    core_plc_scope = get_core_plc(country, division, source)
    df = apply_renovation(df, core_plc_scope)
    df = apply_delisting(df, core_plc_scope)
    return df


def apply_renovation(df, plc_scope):
    """ Take phase-in start dates from the PLC file,
    then make the renovated products forecasts start from those dates
    """
    scope_reno = plc_scope.copy().dropna(subset=["phasein_start_date"])
    for index, row in scope_reno.iterrows():
        mat_cod = row["mat_cod"]
        renovation_date = row["phasein_start_date"]
        to_update = df.loc[df.SKU == mat_cod]
        to_drop = to_update.query(f"TIME < '{renovation_date}'").index
        df = df.drop(to_drop)
    return df


def apply_delisting(df, core_plc_scope):
    """ Take phase-out end dates from the PLC file,
    then make the delisted products forecasts end at those end
    """
    scope_delisting = core_plc_scope.copy().dropna(subset=["phaseout_start_date"])
    scope_delisting = scope_delisting.loc[
        scope_delisting.mat_cod == scope_delisting.mat_cod_reference
    ]
    df = df.reset_index(drop=True)

    for index, row in scope_delisting.iterrows():
        mat_cod = row["mat_cod_reference"]
        phaseout_start = row["phaseout_start_date"]
        phaseout_end = row["phaseout_end_date"]
        delisting = df.loc[df.SKU == mat_cod]
        delisting = apply_linear_phaseout(delisting, phaseout_start, phaseout_end)

        to_clip_index = delisting.loc[delisting.TIME > phaseout_end.strftime("%Y-%m-%d")].index
        to_clip = pd.DataFrame({"Total": 0, "Baseline": 0}, index=to_clip_index)
        delisting.update(to_clip)
        df.update(delisting)

    return df


def apply_linear_phaseout(delisting: pd.DataFrame, start, end) -> pd.DataFrame:
    """ Make delisted SKUs forecasts go down to zero, between their two
    phase-out dates, by following a linear curve.
    """
    delisting_subset = delisting.loc[
        (
            delisting.TIME >= start.strftime("%Y-%m-%d")
        ) & (
            delisting.TIME <= end.strftime("%Y-%m-%d")
        )
    ]

    if len(delisting_subset) == 0:
        return delisting

    delisting_subset = apply_linear_phaseout_on_subset(delisting_subset)
    delisting.update(delisting_subset)
    return delisting


def apply_linear_phaseout_on_subset(df: pd.DataFrame) -> pd.DataFrame:
    df = df.sort_values(by=["TIME"])
    rows = len(df)
    duplication = len(df.loc[df.TIME == df.TIME.unique()[0]])
    if rows % duplication:
        raise Exception(
            f"Data is not uniform : {rows} rows are not divisible by {duplication} groups"
        )
    df["weight"] = sorted(
        list(np.linspace(1, 0, rows // duplication)) * duplication,
        reverse=True
    )
    df[["Total", "Baseline"]] = df[["Total", "Baseline"]].multiply(df.weight, axis=0)
    df = df.drop(columns=["weight"])
    return df


def upload_ibp_export_to_snowflake(
        df: pd.DataFrame, report: IBPReports
):

    column_names = {
        "export_to_ibp": "EXPORT_TO_IBP",
        "execution_date": "EXECUTION_DATE",
        "prediction_date": "PREDICTION_DATE",
        "sales_organization": "SALES_ORGANIZATION",
        "target_date": "TARGET_DATE",
        "product_code": "PRODUCT_CODE",
        "product_level": "PRODUCT_LEVEL",
        "location_code": "LOCATION_CODE",
        "location_level": "LOCATION_LEVEL",
        "customer_code": "CUSTOMER_CODE",
        "customer_level": "CUSTOMER_LEVEL",
        "prediction": "PREDICTION",
        "min_prediction": "MIN_PREDICTION",
        "max_prediction": "MAX_PREDICTION",
        "baseline": "BASELINE",
        "building_block_promotion": "BUILDING_BLOCK_PROMOTION",
        "building_block_marketing": "BUILDING_BLOCK_MARKETING",
        "building_block_weather": "BUILDING_BLOCK_WEATHER",
        "building_block_stock": "BUILDING_BLOCK_STOCK",
        "unit_of_measure": "UNIT_OF_MEASURE",
        "ibp_report_id": "IBP_REPORT_ID",
        "pipeline_id": "PIPELINE_ID"
    }
    result = pd.DataFrame(
        index=df.index,
        columns=list(column_names.values())
    )
    result[column_names["export_to_ibp"]] = True
    result[column_names["execution_date"]] = datetime.utcnow()
    result[column_names["execution_date"]] = result[
        column_names["execution_date"]
    ].dt.ceil(freq='ms').dt.tz_localize('UTC')
    result[column_names["prediction_date"]] = pd.to_datetime(
        df["FORECASTED_FROM"], utc=True
    ).dt.ceil(freq='ms')
    result[column_names["sales_organization"]] = df["Sales Org"]
    result[column_names["target_date"]] = pd.to_datetime(
        df["TIME"], utc=True
    ).dt.ceil(freq='ms')
    product_query = df[[
        "SKU", "FU", "Subfamily", "Product Brand", "Umbrella Brand"
    ]].replace("", np.NaN).count()
    product_level = product_query.loc[product_query > 1].index.values
    result[column_names["product_code"]] = df[product_level[0]] if len(product_level) else 0
    result[column_names["product_level"]] = product_level[0] if len(product_level) else "All"
    location_query = df[[
        "WAREHOUSE", "Dept"
    ]].replace("", np.NaN).count()
    location_level = location_query.loc[location_query > 1].index.values
    result[column_names["location_code"]] = df[location_level[0]] if len(location_level) else 0
    result[column_names["location_level"]] = (
        location_level[0] if len(location_level) else "National"
    )
    customer_query = df[[
        "Customer level 1", "Customer level 2", "Customer level 3", "Customer level 4", "Shipto"
    ]].replace("", np.NaN).count()
    customer_level = customer_query.loc[customer_query > 1].index.values
    result[column_names["customer_code"]] = df[customer_level[0]] if len(customer_level) else 0
    result[column_names["customer_level"]] = customer_level[0] if len(customer_level) else "All"
    result[column_names["prediction"]] = df["Total"]
    result[column_names["min_prediction"]] = df["Min"].replace("", np.NaN)
    result[column_names["max_prediction"]] = df["Max"].replace("", np.NaN)
    result[column_names["baseline"]] = df["Baseline"]
    result[column_names["building_block_promotion"]] = df["Promotion"].replace("", np.NaN)
    result[column_names["building_block_marketing"]] = df["Marketing"].replace("", np.NaN)
    result[column_names["building_block_weather"]] = df["Weather"].replace("", np.NaN)
    result[column_names["building_block_stock"]] = df["Stock"].replace("", np.NaN)
    result[column_names["unit_of_measure"]] = df["UOM"]
    result[column_names["ibp_report_id"]] = str(report.id).replace("-", "")
    result[column_names["pipeline_id"]] = (
        str(report.pipeline_id).replace("-", "") if report.pipeline_id else None
    )
    result = result[list(column_names.values())]

    write_pandas(
        conn=get_snowflake_connector(),
        df=result,
        table_name="IBP_EXPORTS",
        database=SNOWFLAKE_DATABASE,
        schema=SNOWFLAKE_SCHEMA,
        chunk_size=100000,
        compression="gzip"
    )


def upload_sellout_export_to_snowflake(
        df: pd.DataFrame, sellout_ibp_report: SelloutIBPReports
):

    results = df.copy()
    results["export_to_ibp"] = True
    results["execution_date"] = datetime.utcnow()
    results["execution_date"] = (
        results["execution_date"]
        .dt
        .ceil(freq='ms')
        .dt
        .tz_localize('UTC')
    )
    results["product_level"] = "FU"
    results["location_code"] = 0
    results["location_level"] = "All"
    results["customer_level"] = "RFA"
    results["to_dt"] = pd.to_datetime(results["to_dt"], utc=True).dt.ceil(freq='ms')
    results["sellout_ibp_report_id"] = str(sellout_ibp_report.id).replace("-", "")
    results["pipeline_id"] = (
        str(sellout_ibp_report.pipeline_id).replace("-", "")
        if sellout_ibp_report.pipeline_id else None
    )
    column_names = {
        "export_to_ibp": "EXPORT_TO_IBP",
        "sales_organization_cod": "SALES_ORGANIZATION",
        "execution_date": "EXECUTION_DATE",
        "fu_cod": "PRODUCT_CODE",
        "product_level": "PRODUCT_LEVEL",
        "location_code": "LOCATION_CODE",
        "location_level": "LOCATION_LEVEL",
        "rfa_cod": "CUSTOMER_CODE",
        "customer_level": "CUSTOMER_LEVEL",
        "to_dt": "DATE",
        "sellout": "SELLOUT_VOLUMES",
        "sellout_ibp_report_id": "SELLOUT_IBP_REPORT_ID",
        "pipeline_id": "PIPELINE_ID"
    }
    results = results[list(column_names.keys())] \
        .rename(column_names, axis=1)

    connection = get_snowflake_connector(
        warehouse=SNOWFLAKE_WAREHOUSE,
        database=SNOWFLAKE_DATABASE,
        schema=SNOWFLAKE_SCHEMA
    )

    try:
        # Remove data that are more than 2 weeks from current date
        mask = (datetime.now() - timedelta(weeks=2)).strftime("%Y-%m-%d")
        connection \
            .cursor() \
            .execute(
                f'DELETE FROM '
                f'"{SNOWFLAKE_DATABASE}"."{SNOWFLAKE_SCHEMA}".'
                f'"SELLOUT_IBP_EXPORTS" '
                f"WHERE EXECUTION_DATE <= '{mask}'"
            )

        # Write new sellout data
        write_pandas(
            conn=connection,
            df=results,
            table_name="SELLOUT_IBP_EXPORTS",
            database=SNOWFLAKE_DATABASE,
            schema=SNOWFLAKE_SCHEMA,
            chunk_size=100000,
            compression="gzip"
        )
    finally:
        connection.close()
