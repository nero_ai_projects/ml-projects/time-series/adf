from typing import List, Union
import pandas as pd  # type: ignore
from adf.modelling.dataprep.utils.holidays import get_holidays_df


def cap_negative_pred_to_zero(df: pd.DataFrame) -> pd.DataFrame:
    """Handle various cases of negative prediction with Promotion field.

    Parameters
    ----------
    df : pd.DataFrame
        ibp_export dataframe

    Returns
    -------
    pd.DataFrame
        ibp_export with correct predictions
    """
    if len(df.loc[(df['Promotion'] < 0) | (df['Baseline'] < 0) | (df['Total'] < 0)]) > 0:
        if len(df.loc[(df['Promotion'] < 0) & (df['Baseline'] > 0) & (df['Total'] > 0)]) > 0:
            df.loc[(df['Promotion'] < 0) & (df['Baseline'] > 0), 'Baseline'] = df.loc[
                (df['Promotion'] < 0) & (df['Baseline'] > 0), 'Total']
            df.loc[(df['Promotion'] < 0) & (df['Baseline'] > 0), 'Promotion'] = 0
        if len(df.loc[(df['Promotion'] > 0) & (df['Baseline'] < 0) & (df['Total'] > 0)]) > 0:
            df.loc[(df['Promotion'] < 0) & (df['Baseline'] > 0), 'Baseline'] = df.loc[
                (df['Promotion'] < 0) & (df['Baseline'] > 0), 'Total']
            df.loc[(df['Promotion'] < 0) & (df['Baseline'] > 0), 'Promotion'] = 0
        if len(df.loc[df['Total'] < 0]) > 0:
            df.loc[(df['Total'] < 0), ['Total', 'Baseline', 'Promotion']] = 0
    return df


def prev_to_zero_weekday(
        df: pd.DataFrame, weekday: int,
        cols_to_zero: Union[List[str], None] = None
) -> pd.DataFrame:
    """Put prediction to 0 on particular weekday

    Parameters
    ----------
    df : pd.DataFrame
        ibp_export dataframe
    weekday : int
        day of week to predict no volumes
        0 is monday, 6 is sunday
    cols_to_zero : List[str] or None, default is None
        fields in the IBP export to put to 0, by default ['Total', 'Baseline', 'Promotion']

    Returns
    -------
    pd.DataFrame
        ibp_export dataframe
    """
    if cols_to_zero is None:
        cols_to_zero = ['Total', 'Baseline', 'Promotion']
    df['weekday'] = df['TIME'].map(lambda x: pd.Timestamp(x).weekday())
    df.loc[df['weekday'] == weekday, cols_to_zero] = 0
    df = df.drop('weekday', axis=1)
    return df


def prev_to_zero_on_holidays(
        df: pd.DataFrame, country: str,
        holiday_type: str = 'dayoff', cols_to_zero: Union[List[str], None] = None
) -> pd.DataFrame:
    """Put prediction to 0 on selected type of holidays

    Parameters
    ----------
    df : pd.DataFrame
        ibp_export dataframe
    country : str
    holiday_type : str, optional
        available type of holidays for selected country, by default 'dayoff'
    cols_to_zero : List[str], optional. Default is None
        fields in the IBP export to put to 0, by default ['Total', 'Baseline', 'Promotion']

    Returns
    -------
    pd.DataFrame
        ibp_export dataframe
    """
    if cols_to_zero is None:
        cols_to_zero = ['Total', 'Baseline', 'Promotion']
    holidays_df = get_holidays_df(country, holiday_type)
    holidays_df = holidays_df.loc[(
        holidays_df['date'] >= min(df['TIME'])) & (
        holidays_df['date'] <= max(df['TIME']))
    ]
    holidays_dates = set(holidays_df.loc[holidays_df['is_holiday'] == 1]['date'])
    df.loc[df['TIME'].isin(holidays_dates), cols_to_zero] = 0
    return df
