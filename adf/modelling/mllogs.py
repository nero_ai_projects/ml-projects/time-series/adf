import logging
import json
import hashlib
import os
import tempfile
import threading
import copy
import mlflow  # type: ignore
import mlflow.sklearn  # type: ignore
import mlflow.xgboost  # type: ignore
import mlflow.catboost  # type: ignore
import mlflow.lightgbm  # type: ignore
import eli5  # type: ignore
import shap  # type: ignore
import pandas as pd  # type: ignore
import dask.dataframe as dd  # type: ignore
import databricks.koalas as ks  # type: ignore
import plotly.graph_objects as go  # type: ignore
import seaborn as sns  # type: ignore
import matplotlib.pyplot as plt  # type: ignore
from typing import List, Dict, Union, Optional
from pathlib import Path

from mlflow.models import infer_signature  # type: ignore
from xgboost import plot_tree as xgb_plot_tree, XGBRegressor  # type: ignore
from xgboost.core import Booster  # type: ignore
from catboost import CatBoostRegressor  # type: ignore
from lightgbm import plot_tree as lgbm_plot_tree, LGBMRegressor  # type: ignore
from sklearn.ensemble import RandomForestRegressor  # type: ignore
from eli5.sklearn import PermutationImportance  # type: ignore
from adf.services.database.base import Base
from adf import utils
from adf.types import DataFrame, Model
from adf import config


log = logging.getLogger("adf")


def log_json_artifact(obj: Optional[Union[List, Dict]], filename: str) -> None:
    if not obj:
        return
    with tempfile.TemporaryDirectory() as tempdir:
        filepath = Path(tempdir, filename)
        with open(filepath, "w") as stream:
            json.dump(obj, stream, indent=4)
        mlflow.log_artifact(str(filepath))


def tag_report(report: Base, report_number: int = None) -> None:
    report_class = report.__class__.__name__
    if report_number is not None:
        report_class = report_class + str(report_number)
    tags = {
        report_class: str(report.id)
    }
    if hasattr(report, "description"):
        tags.update(**report.description.get("tags", {}))
    mlflow.set_tags(tags)


def tag_report_status(report: Base) -> None:
    if report.reason and len(report.reason) < 5000:
        mlflow.set_tags({
            "status": report.status,
            "reason": report.reason,
        })


def tag_report_output(report: Base) -> None:
    mlflow.set_tags({
        "mount": report.mount,
        "blob": report.blob
    })


def log_shape(df) -> None:
    if isinstance(df, pd.DataFrame) or isinstance(df, ks.DataFrame):
        mlflow.log_metrics({
            "rows": df.shape[0],
            "columns": df.shape[1]
        })


def log_levels(config: utils.Schema) -> None:
    mlflow.log_params({
        "product_level": config.product.level,
        "location_level": config.location.level,
        "time_level": config.time.level,
    })


def log_accuracy(df: pd.DataFrame) -> None:
    if "forecast_accuracy" in df.columns:
        for index, row in df.iterrows():
            mlflow.log_metric(
                "forecast_accuracy",
                row["forecast_accuracy"],
                step=index
            )
            mlflow.log_metric(
                f"fa_{index}", row["forecast_accuracy"]
            )


def log_model_metrics(
    model: Model,
    x_test: DataFrame,
    y_test: DataFrame,
    model_columns: List,
    model_config: Dict,
    cv_results: Optional[Dict]
) -> None:
    """"
    Create temp directory, plot regressor features importance, shap features importance and
    eli5 features permutation in it and into mlflow, then bins the temp dir
    """
    with tempfile.TemporaryDirectory() as tempdir:
        model_name = model_config["name"]
        cat_features = model.__dict__\
            .get("_init_params", {})\
            .get("cat_features", None)
        categorical_feature = model.__dict__\
            .get("_other_params", {})\
            .get("categorical_feature", None)
        log_features_importance_training_model(model, model_columns, model_name, tempdir)
        if "LOG_MODEL_TREE" in os.environ:
            log_model_tree(model, model_name, tempdir)
        log_model_cv_results(cv_results, model_name, tempdir)
        if isinstance(x_test, pd.DataFrame) and len(x_test):
            log_features_importance_shap(model, x_test, y_test, model_name, tempdir)
            if not cat_features and not categorical_feature:
                log_features_permutation(model, x_test, y_test, model_name, tempdir)


def log_correlation_matrix(df: pd.DataFrame, model_name: str):
    with tempfile.TemporaryDirectory() as tempdir:
        filepath = str(Path(tempdir, f"correlation_matrix_{model_name}.png"))
        fig, ax = plt.subplots(1, 1)
        sns.heatmap(df.corr(), xticklabels=1, yticklabels=1)
        plt.savefig(filepath, dpi=400, bbox_inches="tight")
        mlflow.log_artifact(filepath)
        plt.close(fig=fig)


def log_features_importance_training_model(
    model: Model,
    model_columns: List,
    model_name: str,
    tempdir: str
) -> None:
    filepath = str(Path(tempdir, f"features_importance_{model_name}.html"))
    features_importance = list(zip(model_columns, model.feature_importances_))
    features_importance = sorted(features_importance, key=lambda x: x[1])[-30:]
    keys = [x[0] for x in features_importance]
    vals = [x[1] for x in features_importance]
    fig = go.Figure(go.Bar(x=vals, y=keys, orientation="h"))
    fig.update_layout(title={"text": "Features Importance", "x": 0.5})
    fig.write_html(filepath)
    mlflow.log_artifact(filepath)
    plt.close()


def log_features_importance_shap(
    model: Model,
    x_test: pd.DataFrame,
    y_test: pd.DataFrame,
    model_name: str,
    tempdir: str
) -> None:
    if threading.current_thread() is threading.main_thread() and int(os.getenv("PLOT_MODEL", 1)):
        filepath = str(Path(tempdir, f"features_importance_shap_{model_name}.png"))
        figure = plt.figure()
        explainer = shap.TreeExplainer(model)
        shap_values = explainer.shap_values(x_test, y=y_test, check_additivity=False)
        shap.summary_plot(shap_values, x_test, show=False)
        figure.savefig(filepath, bbox_inches="tight", dpi=600)
        mlflow.log_artifact(filepath)
        plt.close(fig=figure)
    else:
        log.warning(
            "\"log_features_importance_shap\" cannot be executed on a child thread. \n"
            "To remove parallelization, add \"scheduler\": \"single-threaded\" "
            "into your training config"
        )


def log_force_plot_shap(model: Model, x_test: pd.DataFrame, model_name: str, tempdir: str):
    if threading.current_thread() is threading.main_thread() and int(os.getenv("PLOT_MODEL", 1)):
        figure = plt.figure()
        explainer = shap.TreeExplainer(model)
        shap_values = explainer.shap_values(x_test)
        filepath = str(Path(tempdir, f"force_plot_shap_{model_name}.png"))
        shap.force_plot(explainer.expected_value, shap_values, x_test)
        figure.savefig(filepath, bbox_inches='tight', dpi=600)
        plt.close(fig=figure)
    else:
        log.warning(
            "\"log_force_plot_shap\" cannot be executed on a child thread. \n"
            "To remove parallelization, add \"scheduler\": \"single-threaded\" "
            "into your training config"
        )


def log_collective_force_plot_shap(
    model: Model, x_test: pd.DataFrame, model_name: str, tempdir: str
):
    if threading.current_thread() is threading.main_thread() and int(os.getenv("PLOT_MODEL", 1)):
        figure = plt.figure()
        explainer = shap.TreeExplainer(model)
        shap_values = explainer.shap_values(x_test)
        filepath = str(Path(tempdir, f"collective_force_plot_shap_{model_name}.png"))
        shap.force_plot(explainer.expected_value, shap_values, x_test)
        figure.savefig(filepath, bbox_inches='tight', dpi=600)
        plt.close(fig=figure)
    else:
        log.warning(
            "\"log_collective_force_plot_shap\" cannot be executed on a child thread. \n"
            "To remove parallelization, add \"scheduler\": \"single-threaded\" "
            "into your training config"
        )


def log_features_permutation(
    model: Model,
    x_test: pd.DataFrame,
    y_test: pd.DataFrame,
    model_name: str,
    tempdir: str
) -> None:
    filepath = str(Path(tempdir, f"features_permutation_eli5_{model_name}.html"))
    permuter = PermutationImportance(model, cv='prefit', n_iter=5)
    permuter.fit(x_test.fillna(-100), y_test)
    fig = eli5.explain_weights(permuter, feature_names=x_test.columns.to_list(), top=30)
    html = eli5.formatters.html.format_as_html(fig, highlight_spaces=False)
    with open(filepath, "w") as f:
        f.write(html)
    mlflow.log_artifact(filepath)


def log_model_tree(model: Model, model_name: str, tempdir: str) -> None:
    filepath = str(Path(tempdir, f"model_tree_{model_name}.png"))

    if isinstance(model, (XGBRegressor, Booster)):
        return log_xgboost_model_tree(model, filepath)

    if isinstance(model, CatBoostRegressor):
        return log_catboost_model_tree(model, filepath)

    if isinstance(model, LGBMRegressor):
        return log_lightgbm_model_tree(model, filepath)

    log.info("Tree plot not implemented for %s", model.__name__)


def log_model_cv_results(cv_results: Optional[Dict],
                         model_name: str, tempdir: str) -> None:
    if cv_results is not None:
        filepath = str(Path(tempdir, f"cv_results_{model_name}.html"))
        html = pd.DataFrame(cv_results).to_html()
        with open(filepath, "w") as f:
            f.write(html)
        mlflow.log_artifact(filepath)


def log_xgboost_model_tree(model: Model, filepath: str) -> None:
    if threading.current_thread() is threading.main_thread() and int(os.getenv("PLOT_MODEL", 1)):
        tree = xgb_plot_tree(model)
        figure = tree.figure
        figure.set_size_inches(80, 60)
        figure.savefig(filepath, dpi=200, bbox_inches="tight")
        mlflow.log_artifact(filepath)
        plt.close(fig=figure)


def log_catboost_model_tree(model: Model, filepath: str) -> None:
    if threading.current_thread() is threading.main_thread() and int(os.getenv("PLOT_MODEL", 1)):
        tree = model.plot_tree(0)
        tree.format = "png"
        tree.render(filepath.rstrip(".png"))
        mlflow.log_artifact(filepath)


def log_lightgbm_model_tree(model: Model, filepath: str) -> None:
    if threading.current_thread() is threading.main_thread() and int(os.getenv("PLOT_MODEL", 1)):
        tree = lgbm_plot_tree(model)
        figure = tree.figure
        figure.set_size_inches(80, 60)
        figure.savefig(filepath, dpi=200, bbox_inches="tight")
        mlflow.log_artifact(filepath)
        plt.close(fig=figure)


def log_params(key: str, obj: Optional[Union[List, Dict, int, float, str]]) -> None:
    if isinstance(obj, list):
        for (i, o) in enumerate(obj):
            log_params(f"{key}_{i}_", o)

    if isinstance(obj, dict):
        for key_, val in obj.items():
            log_params(f"{key}_{key_}", val)

    if obj is None or isinstance(obj, (int, float, str)):
        mlflow.log_param(key, obj)


def save_model(
        model: Model, training_id: str, country: str, division: str,
        splits_config: Optional[List[Dict]], filters_config: Optional[List[Dict]],
        algorithm_type: str, horizon: int,
        data: DataFrame, quantile: Optional[str] = None
):
    run_id = mlflow.active_run().info.run_id
    if isinstance(data, dd.DataFrame):
        data = data.compute()
    elif isinstance(data, ks.DataFrame):
        data = data.to_pandas()
    signature = infer_signature(data, model.predict(data))
    quantile = '-' + quantile if quantile else ''
    split_value = ''
    if splits_config:
        split_value = f'-{hashlib.sha1(str(splits_config).encode("utf-8")).hexdigest()}'
    filters_value = ''
    if filters_config:
        filters_value = f'-{hashlib.sha1(str(filters_config).encode("utf-8")).hexdigest()}'

    if isinstance(model, RandomForestRegressor):
        result = mlflow.sklearn.log_model(model, training_id, signature=signature)
        mlflow.register_model(
            f"runs:/{run_id}/{training_id}",
            f"adf-shared-{country}-{division}{split_value}{filters_value}"
            f"-{algorithm_type}-{horizon}{quantile}"
        )
    elif isinstance(model, CatBoostRegressor):
        result = mlflow.catboost.log_model(model, training_id, signature=signature)
        mlflow.register_model(
            f"runs:/{run_id}/{training_id}",
            f"adf-shared-{country}-{division}{split_value}{filters_value}"
            f"-{algorithm_type}-{horizon}{quantile}"
        )
    elif isinstance(model, (XGBRegressor, Booster)):
        result = mlflow.sklearn.log_model(model, training_id, signature=signature)
        mlflow.register_model(
            f"runs:/{run_id}/{training_id}",
            f"adf-shared-{country}-{division}{split_value}{filters_value}"
            f"-{algorithm_type}-{horizon}{quantile}"
        )
    elif isinstance(model, LGBMRegressor):
        result = mlflow.lightgbm.log_model(model, training_id, signature=signature)
        mlflow.register_model(
            f"runs:/{run_id}/{training_id}",
            f"adf-shared-{country}-{division}{split_value}{filters_value}"
            f"-{algorithm_type}-{horizon}{quantile}"
        )
    else:
        raise NotImplementedError(f"No method available to save model of type {type(model)}")
    return result


def load_model(training_id: str, preprocessing_id: str, model_type):
    runs = mlflow.search_runs(
        experiment_ids=[
            mlflow.get_experiment_by_name(
                f"{config.DATABRICKS_MLFLOW_PATH}training"
            ).experiment_id
        ],
        filter_string=f'tags.PreprocessingReports = "{preprocessing_id}"'
    )
    training_run_id = runs.loc[
        (
            runs[[
                col
                for col in runs.columns
                if col.startswith("tags.TrainingReports_week_")
            ]] == training_id
        ).any(axis=1).index
    ].run_id[0]
    if model_type == "RandomForest":
        model = mlflow.sklearn.load_model(f"runs:/{training_run_id}/{training_id}")
    elif model_type == "CatBoost":
        model = mlflow.catboost.load_model(f"runs:/{training_run_id}/{training_id}")
    elif model_type == "XGBoost":
        model = mlflow.sklearn.load_model(f"runs:/{training_run_id}/{training_id}")
    elif model_type == "LGBM":
        model = mlflow.lightgbm.load_model(f"runs:/{training_run_id}/{training_id}")
    else:
        raise NotImplementedError(
            f"No method available to load model of type {model_type}"
        )
    return model


def log_models(config) -> None:
    models_to_log = copy.deepcopy(config._config.get("models"))
    for model in models_to_log:
        del model["specific_features"]
    for i, model in enumerate(models_to_log):
        model_name = model.get("name")
        log_params(f"model_{model_name}", model.get("type"))
        log_params(f"model_{model_name}", model.get("parameters"))
        log_params(f"model_{model_name}_predict_steps", str(model.get("prediction_steps")))
    log_params("learning", config.get("learning"))


def log_dataprep_report(report: Base) -> None:
    report_class = report.__class__.__name__
    mlflow.set_tags({
        report_class: str(report.id),
        "country": report.business_unit.country,
        "division": report.business_unit.division,
    })
    mlflow.log_params({
        "product_level": report.product_level,
        "location_level": report.location_level,
    })


def tag_export(mount, blob):
    mlflow.set_tags({
        "export_mount": mount,
        "export_blob": blob
    })
