from functools import partial
from typing import Optional, Union
from uuid import UUID
from adf.utils.provider import Provider
from adf.modelling.dataprep import get_dataprep

Provide = partial(Provider, mlrun=True, session=True, cluster=True)


def prepare(
    country: str,
    division: str,
    product_level: str,
    location_level: str,
    pipeline_id: Optional[Union[str, UUID]] = None
):
    dataprep = get_dataprep(
        country, f"{division}"
    )

    dataprep_report, inno_dataprep_report = Provide("dataprep")(dataprep.run)(
        country,
        division,
        product_level,
        location_level,
        pipeline_id=pipeline_id
    )
    return dataprep_report, inno_dataprep_report
