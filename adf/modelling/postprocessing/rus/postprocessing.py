import json  # type: ignore
import logging  # type: ignore

import adf.modelling.postprocessing.rus.seasonal as seasonal
import adf.modelling.postprocessing.rus.priority as priority
import adf.modelling.dataprep.rus.tools as tools
import adf.modelling.dataprep.rus.config as config

import pandas as pd  # type: ignore


logger = logging.getLogger(__name__)


def main(horizon: str,
         export_to_sf: str = None,
         magnit: bool = False):

    """
    Run postprocessing for fc.

    :param horizon: time horizon
    :param export_to_sf: type of export to Snowflake ('prod', 'test', None),
    default = None (without export)
    :param magnit: flag on run only for DC MAGNIT
    :return: dataset with corrections
    """
    logging.info('Run postprocessing')
    date_column = config.DATE_COLUMN_MAPPING[horizon]

    total = pd.read_parquet(config.TOTAL_FC_PATH)
    if magnit:
        total = seasonal.seasonal_correction(total,
                                             date_column)
    if not magnit and horizon == 'st':
        total = priority.add_priority_fc(total, date_column, horizon)
    total.to_parquet(config.POSTPROCESSING_PATH, index=False)
    if export_to_sf:
        tools.export_fc_to_snowflake(total, horizon, date_column, export_to_sf, magnit)