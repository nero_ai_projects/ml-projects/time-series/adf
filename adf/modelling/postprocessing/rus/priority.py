import numpy as np
import pandas as pd
import logging  # type: ignore
from collections import defaultdict
from datetime import date

from adf.modelling.dataprep.rus.tools import get_snowflake_df
from adf.modelling.dataprep.rus import config, utils

logger = logging.getLogger("adf")


def dataprep_prior() -> pd.DataFrame:
    prior = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', "F_EXT_TAB_PRIO",
                             selection="CHAIN_DESC, TBT, UMB_BRD_DESC, BRAND_DESC, "
                                       "FAMILY_DESC, SKU, PRIORITY, PERIOD, DATA_TYPE") \
        .rename(columns=config.PRIOR_COLUMNS_RENAME).astype({'techbillto': 'Int64',
                                                             'sku': 'Int64'})
    cols = prior.columns.difference(['priority', 'period', 'data_type'])
    prior['weight'] = 0
    for col in cols:
        prior['weight'] += np.where(~prior[col].isna(), config.PRIOR_COLUMNS_WEIGHT[col], 0)
    prior[['start', 'end']] = prior['period'].str.split("_")[0]
    prior['start'] = config.FIRST_FORECAST_DATE + pd.to_timedelta(
        prior['start'].str[1:].astype(int), unit='D')
    prior['end'] = config.FIRST_FORECAST_DATE + pd.to_timedelta(
        prior['end'].str[1:].astype(int), unit='D')
    del prior['period']
    return prior


def dataprep_cust_source(datasource: str,
                         date_column: str = 'mad_date') -> pd.DataFrame:
    data = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', datasource,
                            selection=f"WH, CHAIN, BU, TBT, SKU, {date_column.upper()},"
                                      f" VOLUME_KG, VERSION") \
        .rename(columns=config.PRIOR_COLUMNS_RENAME) \
        .astype({'techbillto': int,
                 'sku': int,
                 'dc': int}) \
        .sort_values(by='version', ascending=False) \
        .groupby(config.SI_KEY_COLUMNS + [date_column]) \
        .agg({'cust_fc': 'first', 'version': 'first'}) \
        .reset_index()
    return data


def dataprep_magnit_source(datasource: str,
                           date_column: str) -> pd.DataFrame:
    data = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DMT_IBP', datasource,
                            selection=f"WH, CHAIN, BU, TBT, SKU, {date_column.upper()},"
                                      f" BL, UPLIFT, VERSION") \
        .rename(columns=config.PRIOR_COLUMNS_RENAME) \
        .astype({'techbillto': int,
                 'sku': int,
                 'dc': int,
                 'uplift': 'float64',
                 'bl': 'float64'}) \
        .sort_values(by='version', ascending=False) \
        .groupby(config.SI_KEY_COLUMNS + [date_column]) \
        .agg({'bl': 'first', 'uplift': 'first', 'version': 'first'}) \
        .reset_index()

    data[[date_column, 'version']] = data[[date_column, 'version']].apply(pd.to_datetime,
                                                                          format='%Y-%m-%d')
    return data


def map_source_to_prior(date_column: str,
                        prior: pd.DataFrame,
                        md_prod: pd.DataFrame,
                        md_cust: pd.DataFrame,
                        map_source: pd.DataFrame,
                        sources: dict,
                        ) -> pd.DataFrame:
    hier_cols = ['chain_desc', 'techbillto', 'umb_brd_desc', 'brand_desc', 'family_desc', 'sku']
    df_cust = pd.DataFrame()
    for row in prior.itertuples(index=False):
        hier = [col for col in hier_cols if not pd.isna(getattr(row, col))]
        df = pd.DataFrame({date_column: pd.date_range(start=getattr(row, 'start'),
                                                      end=getattr(row, 'end'))})
        df_hier = pd.DataFrame()
        for col in hier:
            df_hier[col] = [getattr(row, col)]
        prod_hier = list(set(hier).difference(['chain_desc', 'techbillto']))
        if 'chain_desc' in hier:
            df_hier = df_hier.merge(md_cust, how='left', on='chain_desc') \
                .drop(columns=['chain_desc'])
        if prod_hier:
            df_hier = df_hier.merge(md_prod[prod_hier + ['sku']],
                                    how='left', on=prod_hier)
        df_hier['m'], df['m'] = 1, 1
        df = df_hier.merge(df, how='inner') \
            .drop(columns=['m'])
        del df_hier
        data_type = getattr(row, 'data_type')
        if data_type in list(map_source.keys()):
            df = df.merge(sources[data_type], how='left')
        else:
            logging.error(f"Error type of data {data_type}")
            continue
        if data_type in ['MAGNIT_CUS_ST', 'MAGNIT_CUS_MT']:
            df = df[~df['bl'].isna()]
        else:
            df = df[~df['cust_fc'].isna()]

        if not df.empty:
            for info_col in ['weight', 'data_type', 'priority']:
                df[info_col] = getattr(row, info_col)
            df_cust = pd.concat([df_cust, df], ignore_index=True)
    for col in ['cust_fc', 'bl', 'uplift']:
        if col not in df_cust.columns:
            df_cust[col] = None
    return df_cust


def get_priority_cust_fc(date_column: str,
                         horizon: str,
                         ) -> pd.DataFrame:
    prior = dataprep_prior()
    if horizon == 'st':
        prior = prior[prior['data_type'] != 'MAGNIT_CUS_MT']

    elif horizon == 'mt':
        prior = prior[prior['data_type'] == 'MAGNIT_CUS_MT']

    md_cust = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_EXT_CUS_MD',
                               selection="distinct CHAIN, CHAIN_DESC") \
        .rename(columns=config.PRIOR_COLUMNS_RENAME)
    md_prod = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_EXT_PDT_MD',
                               selection="distinct UMB_BRD_DESC, BRAND_DESC,"
                                         " FAMILY_DESC, MAT_COD") \
        .rename(columns=config.PRIOR_COLUMNS_RENAME).astype({'sku': int})

    map_source = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', "F_EXT_DT_VWS_MAP")
    sources = defaultdict(list)
    map_source = map_source[['datatype', 'view_name']].set_index('datatype').T.to_dict('records')[0]
    for data_type in prior.data_type.unique():
        datasource = map_source[data_type].split('.')[2]
        if data_type in ['MAGNIT_CUS_ST', 'MAGNIT_CUS_MT']:
            sources[data_type] = dataprep_magnit_source(datasource, date_column)
        else:
            try:
                sources[data_type] = dataprep_cust_source(datasource, date_column)
            except KeyError as err:
                logging.error(f"{err}: unavailable datatype")

    df_cust = map_source_to_prior(date_column, prior, md_prod, md_cust, map_source, sources)

    if not df_cust.empty:
        df_cust['data_type'] = df_cust['data_type'] + "_" + df_cust['version'].dt.date.astype(str)
        df_cust = df_cust.drop(['umb_brd_desc', 'brand_desc', 'family_desc'],
                               axis=1, errors='ignore') \
            .sort_values(['priority', 'weight'], ascending=[True, False]) \
            .groupby(config.SI_KEY_COLUMNS + [date_column]) \
            .agg({'cust_fc': 'first',
                  'data_type': 'first',
                  'bl': 'first',
                  'uplift': 'first'}) \
            .rename(columns={'bl': 'cust_bl',
                             'uplift': 'cust_uplift'}) \
            .reset_index()
    else:
        msg = 'dataset with customer forecast is empty'
        raise ValueError(msg)

    return df_cust


@utils.gc_collect
def add_priority_fc(df: pd.DataFrame,
                    date_column: str,
                    horizon: str) -> pd.DataFrame:
    """
        Replaces the forecast with a custom one according to the priority table

    :param df: fc from ADF
    :param date_column: name of date_column
    :param horizon: type of horizon
    :return: df with combination of forecast
    """
    df_type = {'techbillto': 'int64',
               'client': 'object',
               'bu': 'object',
               'dc': 'int64',
               'sku': 'int64'}  # TODO: fixed to category type
    df_cust = get_priority_cust_fc(date_column, horizon).astype(df_type)
    df = df[df['partition'] == 'fc'].astype(df_type)
    today = date.today().strftime("%Y-%m-%d")

    df_comb = df.merge(df_cust, how='left', on=config.SI_KEY_COLUMNS + [date_column]) \
        .rename(columns={'bl': 'adf_bl',
                         'uplift': 'adf_uplift'}) \
        .fillna({'data_type': 'ADF_' + today})
    del df_cust, df
    df_comb['cust_fc'] = df_comb['cust_fc'].astype(float)
    df_comb['cust_bl'] = np.where(df_comb['cust_bl'].isna(),
                                  np.where((df_comb['adf_bl'] + df_comb['adf_uplift']) > 0,
                                           df_comb['cust_fc'] * df_comb['adf_bl'] / (
                                                       df_comb['adf_bl'] + df_comb['adf_uplift']),
                                           df_comb['cust_fc']),
                                  df_comb['cust_bl'])
    df_comb['cust_uplift'] = np.where(df_comb['cust_uplift'].isna(),
                                      np.where((df_comb['adf_bl'] + df_comb['adf_uplift']) > 0,
                                               df_comb['cust_fc'] * df_comb['adf_uplift'] / (
                                                       df_comb['adf_bl'] + df_comb['adf_uplift']),
                                               0),
                                      df_comb['cust_uplift'])

    df_comb['bl'] = np.where(df_comb['cust_bl'].isna(), df_comb['adf_bl'], df_comb['cust_bl'])
    df_comb['uplift'] = np.where(df_comb['cust_uplift'].isna(), df_comb['adf_uplift'],
                                 df_comb['cust_uplift'])
    df_comb.drop(columns=['cust_fc'], inplace=True)
    return df_comb
