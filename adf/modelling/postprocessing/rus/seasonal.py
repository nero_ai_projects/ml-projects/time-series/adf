from adf.modelling.dataprep.rus import config, promo
from adf.modelling.dataprep.rus.tools import get_snowflake_df
import numpy as np

import pandas as pd


def add_cat_cust(df):
    """
    Add product and customer hierarchy based on key.

    :param df: pandas dataframe
    :return: input dataframe with additional columns: ['PRD_ProdType', 'PRD_UbrellaBrand',
    'PRODUCT_CODE', 'Sku/линейка', 'Techbillto', 'Chain']
    :notes: 1 - uses two masterdata filses. 2 - it is assumed that key has the following structure:
     xxxx_yyyyyyyyy, where xxxx is product code, yyyyyyyyy is techbillto code.
    """
    prod = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_EXT_PDT_MD',
                            selection='distinct FAMILY_CODE, BRAND_CODE, MAT_COD') \
        .rename(columns={'mat_cod': 'sku'}) \
        .astype({'sku': 'int64'})

    df = pd.merge(df, prod, how='left', on='sku')

    return df


def prepare_data_for_seas(data):
    """
    Prepare dataset for seasonal. Remove everything except "DC Magnit".

    :param data: input dataset
    :return: output dataset (only "DC Magnit" code = R0V087)
    """
    data = add_cat_cust(data)
    data = data[data['client'] == config.CODE_DC_MAGNIT]
    return data


def latest_promo(data, key, date_column='pgi_date'):
    """
    Calculate mean promo and baseline for latest promo.

    :param data: input dataset
    :param key: name of key column
    :param date_column: name of date column
    :return: promo_mean_fact, promo_mean_fc, base_mean_fact, base_mean_fc
    """
    train = data[data['partition'] == 'train']
    ship_starts = train.loc[train['seasonal'] == 1, 'start_date'].max()
    promo = train[(train['start_date'] == ship_starts) & (train['seasonal'] == 1)]

    keys_volume = train[train['key'].isin(list(promo['key'].unique()))]
    keys_volume = keys_volume.groupby('key', as_index=False).agg({'ordered': 'sum'})

    ordered_key = train.loc[train['key'] == key, 'ordered'].sum()
    keys_volume['diff'] = abs(keys_volume['ordered'] - ordered_key)
    best_key = keys_volume.sort_values('diff', ascending=True).reset_index(drop=True).loc[0, 'key']

    # защита от нескольких ключей
    train = train[train['key'] == best_key]
    promo = train[train['start_date'] == ship_starts]

    start_seas = promo[date_column].min()
    end_seas = promo[date_column].max()

    baseline = train[(train['start_date'].isna()) &
                     (((train[date_column] >= start_seas - pd.DateOffset(60)) & (
                             train[date_column] < start_seas)) |
                      (((train[date_column] > end_seas) & (
                              train[date_column] <= end_seas + pd.DateOffset(60)))))]

    promo_mean_fact = promo['ordered'].mean()
    promo_mean_fc = promo['fc'].mean()

    base_mean_fact = baseline['ordered'].mean()
    base_mean_fc = baseline['fc'].mean()

    return promo_mean_fact, promo_mean_fc, base_mean_fact, base_mean_fc


def latest_base(data, date_column='pgi_date'):
    """
    Calculate mean baseline for latest 90 days.

    :param data: input dataset
    :param date_column: name of date column
    :return: latest_base_mean_fact, latest_base_mean_fc
    """
    train = data[data['partition'] == 'train']

    latest_baseline = train[(train['start_date'].isna()) &
                            (train[date_column] >= train[date_column].max() - pd.DateOffset(90))]

    latest_base_mean_fact = latest_baseline['ordered'].mean()
    latest_base_mean_fc = latest_baseline['fc'].mean()

    return latest_base_mean_fact, latest_base_mean_fc


def corr_fc(data, latest_base_mean_fact, latest_base_mean_fc, promo_mean_fact,
            base_mean_fact):
    """
    Adjust the forecast for seasonal promo.

    :param data: fc input dataset
    :param latest_base_mean_fact: latest mean baseline fact
    :param latest_base_mean_fc: latest mean baseline fc
    :param promo_mean_fact: latest mean promo fact
    :param base_mean_fact: latest mean baseline fact
    :param date_column: name of date column
    :return: fc output dataset
    """
    fc = data[data['partition'] == 'fc']
    fc_promo_ft_starts = list(fc.loc[fc['seasonal'] == 1, 'start_date'].unique())

    for fc_ft_start in fc_promo_ft_starts:
        fc_promo = fc[fc['start_date'] == fc_ft_start]

        if base_mean_fact < 0.01:
            continue

        fc_base_corr = latest_base_mean_fact
        fc.loc[(fc['seasonal'] == 1) & (fc['start_date'] == fc_ft_start),
               'fc'] = (fc.loc[(fc['seasonal'] == 1) & (fc['start_date'] == fc_ft_start), 'fc'] /
                        fc.loc[(fc['seasonal'] == 1) & (fc['start_date'] == fc_ft_start), 'fc']
                        .sum())

        fc.loc[(fc['seasonal'] == 1) & (fc['start_date'] == fc_ft_start),
               'fc'] = (fc.loc[(fc['seasonal'] == 1) & (fc['start_date'] == fc_ft_start), 'fc'] *
                        fc_base_corr * promo_mean_fact / base_mean_fact * len(fc_promo))
    return fc


def seasonal_correct_magnit(data, date_column='pgi_date'):
    """
    Adjust seasonal promo in "DC Magnit" based on historical promo or on promos in close SKUs.

    :param data: input dataset
    :param date_column: name of date column
    :return: full dataset ( DC Magnit with corrections, other customers w/o corrections)
    """
    data[date_column] = pd.to_datetime(data[date_column], format='%Y-%m-%d')
    data['key'] = (data['client'].astype(str) + '_' + data['techbillto'].astype(str) + '_' +
                   data['sku'].astype(str) + '_' + data['dc'].astype(str) + '_' +
                   data['bu'].astype(str))
    full_data = data.copy()
    data = prepare_data_for_seas(data)

    # проверяем есть ли Магнит
    if data.empty:
        return full_data

    keys_with_seasonal_train = set(
        data.loc[(data['partition'] == 'train') & (data['seasonal'] == 1), 'key'].unique())
    keys_with_seasonal_fc = set(
        data.loc[(data['partition'] == 'fc') & (data['seasonal'] == 1), 'key'].unique())

    # если нет сезонников на будущее
    if len(keys_with_seasonal_fc) == 0:
        return full_data

    list_data_cor = []

    for key, sample in data.groupby('key', as_index=False):
        if key not in keys_with_seasonal_fc:
            list_data_cor.append(sample)

        elif len(sample[sample['partition'] == 'train']) / 2 >= \
                len(sample[(sample['partition'] == 'train') & (
                        sample['ordered'] > 0.1)]):
            list_data_cor.append(sample)

        elif key in keys_with_seasonal_fc and key in keys_with_seasonal_train:
            promo_mean_fact, promo_mean_fc, base_mean_fact, base_mean_fc = \
                latest_promo(sample, key, date_column)
            latest_base_mean_fact, latest_base_mean_fc = latest_base(sample, date_column)

            fc = corr_fc(sample, latest_base_mean_fact, latest_base_mean_fc, promo_mean_fact,
                         base_mean_fact)
            sample = sample[sample['partition'] != 'fc'].append(fc)

            list_data_cor.append(sample)

        elif key in keys_with_seasonal_fc and key not in keys_with_seasonal_train:
            sku = sample['sku'].unique()[0]
            prod_type = sample['family_code'].unique()[0]
            brand = sample['brand_code'].unique()[0]
            sku_keys = set(data.loc[data['sku'] == sku, 'key'].unique())
            prod_brand_type_keys = set(data.loc[(data['family_code'] == prod_type) &
                                                (data[
                                                     'brand_code'] == brand), 'key'].unique())
            prod_keys = set(data.loc[data['family_code'] == prod_type, 'key'].unique())
            if len(sku_keys & keys_with_seasonal_train) > 0:
                promo_mean_fact, promo_mean_fc, base_mean_fact, base_mean_fc = \
                    latest_promo(data[data['sku'] == sku], key, date_column)

            elif len(prod_brand_type_keys & keys_with_seasonal_train) > 0:
                promo_mean_fact, promo_mean_fc, base_mean_fact, base_mean_fc = \
                    latest_promo(data[(data['family_code'] == prod_type) &
                                      (data['brand_code'] == brand)], key, date_column)
            elif len(prod_keys & keys_with_seasonal_train) > 0:
                promo_mean_fact, promo_mean_fc, base_mean_fact, base_mean_fc = \
                    latest_promo(data[data['family_code'] == prod_type], key, date_column)
            else:
                list_data_cor.append(sample)
                continue

            latest_base_mean_fact, latest_base_mean_fc = latest_base(sample, date_column)
            fc = corr_fc(sample, latest_base_mean_fact, latest_base_mean_fc, promo_mean_fact,
                         base_mean_fact)
            sample = sample[sample['partition'] != 'fc'].append(fc)

            list_data_cor.append(sample)

    data = pd.concat(list_data_cor)
    data = data[full_data.columns]
    full_data = full_data[~full_data['key'].isin(list(data['key'].unique()))].append(data)
    full_data.drop(columns='key', inplace=True)
    full_data[date_column] = full_data[date_column].dt.strftime("%Y-%m-%d")

    return full_data


def seasonal_correction(df, date_column):
    """
    Adjust seasonal promo in "DC Magnit".

    :param df: input dataset
    :param date_column: name of date column
    :param input_proc: path to file with processing data
    :return: output dataset with corrections
    """
    cols = df.columns.to_list()
    df = promo.add_promo_day(df, date_column)
    df = promo.add_promo_length(df, date_column)
    df['promo_length'] = df['promo_length'].astype(int)
    df['start_date'] = np.where(df['promo_day'] == 1, df[date_column], pd.NaT)
    df['start_date'] = df['start_date'].ffill()
    df['start_date'] = np.where(df['discount'] == 0, pd.NaT, df[date_column])
    df['start_date'] = pd.to_datetime(df['start_date'], format='%Y-%m-%d')
    df['end_date'] = df['start_date'] + pd.to_timedelta(df['promo_length'] - 1, 'd')
    df['seasonal'] = np.where(df['promo_length'] > 20, 1, 0)
    df = seasonal_correct_magnit(df, date_column)
    df[date_column] = pd.to_datetime(df[date_column], format='%Y-%m-%d')
    df = df[cols + ['seasonal']]
    return df
