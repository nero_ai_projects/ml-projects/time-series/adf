import pandas as pd
import numpy as np

from adf.modelling import tools
import adf.modelling.dataprep.rus.config as config


def calc_promo_id(discount: list) -> list:
    """
    Numerates promo slots according to a given discount column (sets promo id)
    may be used with a 'transform' method of groupby
    """
    promo_id_list = list()
    promo_id = 0
    prev_discount = None
    for element in list(discount):
        if element == 0:
            promo_id_list.append(0)
        else:
            if prev_discount != element:
                promo_id += 1
            promo_id_list.append(promo_id)
        prev_discount = element
    return promo_id_list


def add_promo_id(data: pd.DataFrame, date_column: str) -> pd.DataFrame:
    """
    Adds a column with generated promo ids for a given whole sell-in dataset
    """
    data.sort_values(date_column, inplace=True)
    data['promo_id'] = data.groupby(config.SI_KEY_COLUMNS)['discount'].transform(calc_promo_id)
    return data


def uplift_level_aggregation(data: pd.DataFrame):
    md_prod = tools.get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_EXT_PDT_MD')
    md_prod = md_prod[['mat_cod', 'ft_sku']]

    data['sku'] = data['sku'].astype('str')
    md_prod.rename(columns={"mat_cod": "sku"}, inplace=True)
    data = data.merge(md_prod, on='sku')

    data = add_promo_id(data, 'pgi_date')
    data = data[['client', 'ft_sku', 'sku', 'ordered', 'pgi_date', 'fc', 'bl', 'uplift', 'discount', 'promo_id']]

    data = data.groupby(['client', 'ft_sku', 'promo_id']).agg({
        'pgi_date': ['min', 'max'],
        'fc': 'sum', 
        'bl': 'sum', 
        'uplift':'sum', 
        'ordered': 'sum', 
        'discount': 'sum'}).reset_index()

    data.to_parquet(config.POSTPROCESSING_UPLIFTLEVEL_PATH, index=False)