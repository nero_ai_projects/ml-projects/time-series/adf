from typing import Dict, Optional
import itertools
import functools
from datetime import datetime
from operator import and_
import logging
import databricks.koalas as ks  # type: ignore
import pandas as pd  # type: ignore
import numpy as np  # type: ignore
import dask.dataframe as dd  # type: ignore
import dask  # type: ignore
from snowflake.connector.pandas_tools import write_pandas  # type: ignore
from pyspark.sql.types import (  # type: ignore
    IntegerType,
    StringType,
    DoubleType,
    StructField,
    StructType,
    DateType,
)
from adf.services.database.query.reports import build_report, update_report
from adf import utils
from adf.services.database import PreprocessingReports
from adf.modelling.preprocessing import tools
from adf.errors import PreprocessingError
from adf.modelling import mllogs
from adf.services import storage
from adf.types import DataFrame
from adf.config import ENV, SNOWFLAKE_DATABASE, SNOWFLAKE_SCHEMA


log = logging.getLogger("adf")


def run(config: Dict, input_mount: str, input_blob: str, **context) -> PreprocessingReports:
    """Method that takes as input
    1) a configuration file describing the data and the processes to do on it
    2) an input file path

    It first parses the configuration file to get the parameters needed for the
    preprocessing steps.
    Then it loads the data, do preparation, aggregation, encoding, lagging and
    outputs the prepared data for ml trainings
    """
    session = context["session"]

    log.info("Building Preprocessor")
    preprocessor = tools.Preprocessor(config)
    mllogs.log_json_artifact(config, "config.json")
    mllogs.log_levels(preprocessor)

    log.info("Loading input")
    df = storage.read_parquet(
        input_mount,
        input_blob,
        object_type=preprocessor.get("object_type", "pandas"),
        engine=preprocessor.get("engine_type", "auto"),
    )

    if isinstance(df, ks.DataFrame) and (
        (
            df[preprocessor.time.column].dtype.type == np.int64
        ) | (df[preprocessor.time.column].dtype.type == np.double)
    ):
        df[preprocessor.time.column] = (df[preprocessor.time.column] / 1e9).astype(datetime)

    df[list(df.select_dtypes("category").columns)] = df[
        list(df.select_dtypes("category").columns)
    ].astype(str)

    mllogs.log_shape(df)

    log.info("Building report")
    report = build_report(
        session,
        PreprocessingReports,
        description=config,
        pipeline_id=context.get("pipeline_id", None),
    )
    mllogs.tag_report(report)

    if report.pipeline_id and report.pipeline.run_type == "core":
        upload_forecasts_input(preprocessor, df, report, context.get("pipeline_id", None))

    partition_on = preprocessor.get("partitioned_on", [])

    if isinstance(df, ks.DataFrame):
        df[preprocessor.time.column] = ks.to_datetime(df[preprocessor.time.column])
        if "time_index" in df.columns:
            df["time_index"] = ks.to_datetime(df["time_index"])

    df = preprocessor.initialize_dataframe(df)

    if partition_on and not isinstance(df, ks.DataFrame):
        futures = []
        unique_values = []
        for col in partition_on:
            unique_values.append(df[col].unique().compute())
        for mask_elements in itertools.product(*unique_values):
            masks = []
            for i, mask_element in enumerate(mask_elements):
                masks.append(df[partition_on[i]] == mask_element)
            futures.append(dask.delayed(df.loc[functools.reduce(and_, masks)]))
        df = dd.from_delayed(futures)

    try:
        log.info("Preprocessing data")
        if isinstance(df, pd.DataFrame):
            df = preprocessor.preprocess(df)
            mllogs.log_shape(df)

            log.info("Saving data")
            report.upload(df, index=True)
        elif isinstance(df, dd.DataFrame):
            log.info("Loading data")
            futures = []
            for i, partition in enumerate(df.partitions):
                log.info(f"Setting up parallelization for partition {i}")
                futures.append(dask.delayed(preprocessor.preprocess)(partition, reset_index=True))
            df = dd.from_delayed(futures)
            log.info("Saving data")
            report.upload(df, partition_on=partition_on if partition_on else None)
        elif isinstance(df, ks.DataFrame):
            if df.index.names != [None]:
                spark_df = df.to_spark(df.index.names)
            else:
                spark_df = df.to_spark()

            schema = get_output_schema(preprocessor, spark_df)
            method = preprocessor.preprocess_for_partition(reset_index=True)
            spark_df = spark_df.groupby(partition_on).applyInPandas(method, schema=schema)
            log.info("Saving data")
            report.upload(spark_df, partition_on=partition_on if partition_on else None)
        else:
            raise Exception(f"Could not preprocess {type(df)}")

        update_report(session, report, "success")
        mllogs.tag_report_output(report)

        return report

    except Exception as err:
        message = str(err.__class__.__name__) + ": " + str(err)
        update_report(session, report, "failed", str(message))
        raise PreprocessingError(message) from err

    finally:
        mllogs.tag_report_status(report)


def get_output_schema(preprocessor, df):
    """Returns the Spark Schema of the output of preprocessor.preprocess.

    TODO: Currently the schema is estimated by running `preprocess` on a small
    data sample. This can probably be rewritten to something nicer and more performant.
    """
    dfp = preprocessor.preprocess(df.limit(100).toPandas())
    mapping = {
        "float16": DoubleType,
        "float32": DoubleType,
        "float64": DoubleType,
        "object": StringType,
        "int8": IntegerType,
        "int16": IntegerType,
        "int32": IntegerType,
        "int64": IntegerType,
        "datetime64[ns]": DateType,
    }
    dfp = dfp.reset_index()
    column_types = [StructField(key, mapping[str(dfp.dtypes[key])]()) for key in dfp.columns]
    schema = StructType(column_types)
    return schema


def distribute_dask_partitions_evenly(df, final_partitions_number):
    res = []
    tmp_length = []
    tmp_partitions = []
    partition_mapping = {
        partition: partition_length
        for partition, partition_length in zip(df.partitions, df.map_partitions(len).compute())
    }
    max_partition_length = sum(partition_mapping.values()) / (final_partitions_number - 1)
    if max(partition_mapping.values()) > max_partition_length:
        raise Exception(f"One of partition exceeds {max_partition_length} rows !")
    for partition, partition_length in partition_mapping.items():
        new_partition_length = sum(tmp_length) + partition_length
        if new_partition_length <= max_partition_length:
            tmp_length += [partition_length]
            tmp_partitions += [partition]
        else:
            if not tmp_length:
                tmp_length = [partition_length]
                tmp_partitions = [partition]
                res += [tmp_partitions]
            else:
                res += [tmp_partitions]
                tmp_length = [partition_length]
                tmp_partitions = [partition]
    res += [tmp_partitions]
    return res


def upload_forecasts_input(
    preprocessor: tools.Preprocessor,
    df: DataFrame,
    report: PreprocessingReports,
    pipeline_id: Optional[str] = None,
):
    if ENV in ("local", "test"):
        column_names = {
            "sales_organization": "sales_organization",
            "snapshot_date": "snapshot_date",
            "target_date": "target_date",
            "product_code": "product_code",
            "product_level": "product_level",
            "location_code": "location_code",
            "location_level": "location_level",
            "customer_code": "customer_code",
            "customer_level": "customer_level",
            "ground_truth": "ground_truth",
            "unit_of_measure": "unit_of_measure",
            "preprocessing_report_id": "preprocessing_report_id",
            "pipeline_id": "pipeline_id",
        }
    else:
        column_names = {
            "sales_organization": "SALES_ORGANIZATION",
            "snapshot_date": "SNAPSHOT_DATE",
            "target_date": "TARGET_DATE",
            "product_code": "PRODUCT_CODE",
            "product_level": "PRODUCT_LEVEL",
            "location_code": "LOCATION_CODE",
            "location_level": "LOCATION_LEVEL",
            "customer_code": "CUSTOMER_CODE",
            "customer_level": "CUSTOMER_LEVEL",
            "ground_truth": "GROUND_TRUTH",
            "unit_of_measure": "UNIT_OF_MEASURE",
            "preprocessing_report_id": "PREPROCESSING_REPORT_ID",
            "pipeline_id": "PIPELINE_ID",
        }
    prod_col = preprocessor.product.column if preprocessor.product.column else "all_products"
    loc_col = preprocessor.location.column if preprocessor.location.column else "all_locations"
    cus_col = preprocessor.customer.column if preprocessor.customer.column else "all_customers"
    if prod_col not in df:
        df[prod_col] = 0
    if loc_col not in df:
        df[loc_col] = 0
    if cus_col not in df:
        df[cus_col] = 0
    df = df.rename(
        columns={
            preprocessor.time.column: column_names["target_date"],
            prod_col: column_names["product_code"],
            loc_col: column_names["location_code"],
            cus_col: column_names["customer_code"],
            preprocessor.ground_truth.column: column_names["ground_truth"],
        }
    )
    df = (
        df.groupby(
            [
                column_names["target_date"],
                column_names["product_code"],
                column_names["location_code"],
                column_names["customer_code"],
            ]
        )[column_names["ground_truth"]]
        .sum()
        .reset_index()
    )
    if isinstance(df, ks.DataFrame):
        df = df.to_pandas()
    elif isinstance(df, dd.DataFrame):
        df = df.compute()
    df[column_names["target_date"]] = (
        pd.to_datetime(df[column_names["target_date"]]).dt.ceil(freq="ms").dt.tz_localize("UTC")
    )
    df[column_names["snapshot_date"]] = datetime.utcnow()
    df[column_names["snapshot_date"]] = (
        df[column_names["snapshot_date"]].dt.ceil(freq="ms").dt.tz_localize("UTC")
    )
    if report.pipeline:
        df[column_names["sales_organization"]] = [
            dataprep_report
            for dataprep_report in report.pipeline.dataprep_report
            if dataprep_report.prep_type == "core"
        ][0].business_unit.sales_org_cod
    else:
        df[column_names["sales_organization"]] = "unknown"
    df[column_names["pipeline_id"]] = str(pipeline_id).replace("-", "") if pipeline_id else None
    df[column_names["preprocessing_report_id"]] = str(report.id).replace("-", "")
    df[column_names["product_level"]] = preprocessor.product.level
    df[column_names["location_level"]] = preprocessor.location.level
    df[column_names["customer_level"]] = preprocessor.customer.level
    df[column_names["unit_of_measure"]] = preprocessor.ground_truth.uom
    df = df[
        [
            column_names["sales_organization"],
            column_names["snapshot_date"],
            column_names["target_date"],
            column_names["product_code"],
            column_names["product_level"],
            column_names["location_code"],
            column_names["location_level"],
            column_names["customer_code"],
            column_names["customer_level"],
            column_names["ground_truth"],
            column_names["unit_of_measure"],
            column_names["preprocessing_report_id"],
            column_names["pipeline_id"],
        ]
    ]
    if ENV in ("local", "test"):
        df.to_sql("forecasts_input", utils.get_database_uri(), if_exists="append", index=False)
    else:
        write_pandas(
            conn=utils.get_snowflake_connector(),
            df=df,
            table_name="FORECASTS_INPUT",
            database=SNOWFLAKE_DATABASE,
            schema=SNOWFLAKE_SCHEMA,
            chunk_size=100000,
            compression="gzip",
        )
