import warnings
from typing import Dict, Any, Union, Callable
import pandas as pd  # type: ignore
import dask.dataframe as dd  # type: ignore
from pandas.core.dtypes.common import is_datetime64_dtype  # type: ignore
import numpy as np  # type: ignore
from adf.enums import Index
from adf.enums import Indexes
from adf import utils
from adf.modelling.preprocessing.tools import Preprocessor
from adf.types import DataFrame, Series


warnings.filterwarnings("ignore", category=DeprecationWarning)


def time_base_aggregate(
        df: DataFrame, pp: Preprocessor) -> DataFrame:
    """
    Aggregates the columns aggregate on a time basis
    Weekly aggregates start from Monday
    Monthly aggregates start on the first of each month
    """
    if pp.time.level == "day":
        return df

    if pp.time.level == "week":
        df[Index.time.value] = utils.start_of_week(df[Index.time.value])

    if pp.time.level == "month":
        df[Index.time.value] = utils.start_of_month(df[Index.time.value])

    aggregation = build_aggregation(pp, df)
    return df.groupby(Indexes).agg(aggregation)


def build_aggregation(pp: Preprocessor, df: DataFrame) -> Dict:
    """ Build the aggregation rules based on config
    """
    result: Dict[str, Union[Callable, str]] = {}

    for feature in pp.features:
        if feature in pp.aggregation.sum:
            result[feature] = "sum"
            continue

        if feature in pp.aggregation.mean:
            result[feature] = "mean"
            continue

        if feature in pp.aggregation.min:
            result[feature] = "min"
            continue

        if feature in pp.aggregation.max:
            result[feature] = "max"
            continue

        if feature in pp.aggregation.first:
            result[feature] = "first"
            continue

        # Note: Perhaps add an extra check here whether you want to do most_frequent value.
        # Otherwise it will always do this if your aggregation method
        # doesn't match one of the ones above.
        if isinstance(df, pd.DataFrame):
            result[feature] = most_frequent_value
        elif isinstance(df, dd.DataFrame):
            result[feature] = dd.Aggregation(
                'most_frequent_value',
                most_frequent_value_chunk,
                most_frequent_value_agg,
                most_frequent_value_finalize
            )
        else:
            raise NotImplementedError(f"`most_frequent_value` not implemented yet for {type(df)}.")

    return result


def most_frequent_value(series: Series) -> Any:
    """ Returns most frequent value of a Series
    """
    result = series.mode()
    if len(result) > 0:
        return result[0]
    if is_datetime64_dtype(series):
        return pd.NaT
    return np.NaN


def most_frequent_value_chunk(series: Series):
    # for the comments, assume only a single grouping column, the
    # implementation can handle multiple group columns.
    #
    # s is a grouped series. value_counts creates a multi-series like
    # (group, value): count
    return series.value_counts()


def most_frequent_value_agg(series: Series):
    # s is a grouped multi-index series. In .apply the full sub-df will passed
    # multi-index and all. Group on the value level and sum the counts. The
    # result of the lambda function is a series. Therefore, the result of the
    # apply is a multi-index series like (group, value): count
    return series.apply(lambda sub_series: sub_series.groupby(level=-1).sum())


def most_frequent_value_finalize(series: Series):
    # s is a multi-index series of the form (group, value): count. First
    # manually group on the group part of the index. The lambda will receive a
    # sub-series with multi index. Next, drop the group part from the index.
    # Finally, determine the index with the maximum value, i.e., the mode.
    level = list(range(series.index.nlevels - 1))
    return (
        series.groupby(level=level)
        .apply(lambda s: series.reset_index(level=level, drop=True).idxmax())
    )
