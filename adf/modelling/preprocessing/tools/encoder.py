from typing import List

import databricks.koalas as ks  # type: ignore
import dask.dataframe as dd  # type:ignore
import pandas as pd  # type: ignore
import numpy as np  # type: ignore
from dask_ml.preprocessing import (  # type: ignore
    LabelEncoder as DaskLabelEncoder, OneHotEncoder as DaskOneHotEncoder, Categorizer
)
from sklearn.preprocessing import LabelEncoder, OneHotEncoder  # type: ignore
from category_encoders.target_encoder import TargetEncoder  # type: ignore
from adf.modelling.preprocessing.tools import Preprocessor
from adf.types import DataFrame
from adf.utils.decorators import timeit


def encode(df: DataFrame, pp: Preprocessor) -> DataFrame:
    """
    encode is performed onto categorical columns and specified columns
    in the encoding parameter.
    Based on type attribute, methods will either perform a
    target encoding or a label encoding
    """
    df = apply_target_encoding(df, pp.encoding.get("target"))
    df = apply_label_encoding(df, pp.encoding.get("label"))
    df = apply_one_hot_encoding(df, pp.encoding.get("one_hot"))
    return df


def apply_target_encoding(df: DataFrame, confs: List) -> DataFrame:
    for conf in confs:
        if isinstance(df, pd.DataFrame):
            encoder = TargetEncoder()
            to_encode = df[conf["columns"]].astype(str)
            fit = encoder.fit_transform(to_encode, df[conf["source"]])
            df[fit.columns] = fit
        else:
            raise Exception("Target encoding is not supported for dask dataframes")
    return df


def apply_label_encoding(df: DataFrame, columns: List) -> DataFrame:
    if not columns:
        return df
    if isinstance(df, pd.DataFrame):
        encoder = LabelEncoder()
        to_encode = df[columns].astype(str)
        fit = to_encode.apply(encoder.fit_transform)
        df[fit.columns] = fit
    elif isinstance(df, ks.DataFrame):
        mapping = {}
        for column in columns:
            mapping[column] = get_column_mapping(df, column)
        df = df.replace(mapping).astype({column: "int" for column in mapping.keys()})
    elif isinstance(df, dd.DataFrame):
        categorizer = Categorizer(columns=columns)
        df = categorizer.fit_transform(df)
        encoder = DaskLabelEncoder()
        for col in columns:
            df[col] = encoder.fit_transform(df[col])
    else:
        raise NotImplementedError("`apply_label_encoding` not implemented for object_type.")

    return df


@timeit
def get_column_mapping(df, column):
    categories = sorted(df[column].unique().to_list())
    return dict(zip(categories, range(len(categories))))


def apply_one_hot_encoding(df: DataFrame, columns: List) -> DataFrame:
    if not columns:
        return df
    if isinstance(df, pd.DataFrame):
        to_encode = df[columns].astype(str)
        encoder = OneHotEncoder()
        fit = encoder.fit(to_encode)
        column_names = fit.get_feature_names(columns)
        result = encoder.fit_transform(to_encode).toarray()
        df = df.drop(columns=columns)
        df[column_names] = pd.DataFrame(np.column_stack(list(zip(*result))), columns=column_names)
    elif isinstance(df, dd.DataFrame):
        categorizer = Categorizer(columns=columns)
        df = categorizer.fit_transform(df)
        to_encode = df[columns]
        encoder = DaskOneHotEncoder()
        result = encoder.fit_transform(to_encode)
        df = df.drop(columns=columns)
        df[list(result.columns)] = result
    elif isinstance(df, ks.DataFrame):
        df = ks.get_dummies(df, columns=columns)
    else:
        raise NotImplementedError("`apply_one_hot_encoding` not implemented for object_type.")
    return df
