from datetime import timedelta
from itertools import product
from typing import Any, List, Tuple

import numpy as np  # type: ignore
import pandas as pd  # type: ignore
from adf.enums import Index, Indexes, Quantile
from adf.utils import Schema
from numpy import inf


def generate_features(df: pd.DataFrame, functions: Schema) -> pd.DataFrame:
    """
    function_list : list of dictionaries, representing functions
    and their given parameters.
    generate_features applies all functions specified in the function_list
    to the a data frame in order to generate new features.
    """
    generator = Generator()
    for func in functions:
        df = generator.apply(df, func.function, **func.get("args"))
    return df


class Generator:

    def apply(self, df: pd.DataFrame, function_name: str, **kwargs) -> Any:
        return getattr(self, function_name)(df, **kwargs)

    def apply_evol(self, df: pd.DataFrame,
                   x: str, y: str, col_name: str) -> pd.DataFrame:
        """ -1 or 1 depending on x > y
        """
        df[col_name] = 2 * (df[x] > df[y]).astype(int) - 1
        return df

    def apply_sum(self, df: pd.DataFrame,
                  x: str, y: str, col_name: str) -> pd.DataFrame:
        """ Sums two columns
        """
        df[col_name] = df[x] + df[y]
        return df

    def apply_diff(self, df: pd.DataFrame,
                   x: str, y: str, col_name: str) -> pd.DataFrame:
        """ Subtracts two columns
        """
        df[col_name] = df[x] - df[y]
        return df

    def apply_prod(self, df: pd.DataFrame,
                   x: str, y: str, col_name: str) -> pd.DataFrame:
        """ Multiplies two columns
        """
        df[col_name] = df[x] * df[y]
        return df

    def apply_div(self, df: pd.DataFrame,
                  x: str, y: str, col_name: str) -> pd.DataFrame:
        """ Divides two columns
        """
        df[col_name] = df[x] / df[y]
        return df

    def apply_sin(self, df: pd.DataFrame,
                  x: str, col_name: str, period: int) -> pd.DataFrame:
        """ Sin function on a column for a specified period
        """
        df[col_name] = 2 * np.sin(2 * np.pi * df[x] / period)
        return df

    def apply_cos(self, df: pd.DataFrame,
                  x: str, col_name: str, period: int) -> pd.DataFrame:
        """ Cos function on a column, for a specified period
        """
        df[col_name] = 2 * np.cos(2 * np.pi * df[x] / period)
        return df

    def apply_log10(self, df: pd.DataFrame,
                    x: str, col_name: str) -> pd.DataFrame:
        """ Applies log10 on a column
        """
        df[col_name] = np.log10(df[x] + 1)
        return df

    def apply_clip(self, df: pd.DataFrame,
                   x: str, col_name: str) -> pd.DataFrame:
        """ makes column values lower to 0 become 0
        """
        df[col_name] = df[x].clip(lower=0)
        return df

    def apply_iso(self, df: pd.DataFrame, formats: List[str]) -> pd.DataFrame:
        """
        Add iso calendar information
        """
        date = df.index.get_level_values(Index.time.value)
        for fmt in formats:
            df[fmt] = compute_iso_format(date, fmt).astype("int16")
        return df

    def apply_rolling_mean(
            self, df: pd.DataFrame, column: str, direction: str,
            start: int, end: int, additional_index: List = None) -> pd.DataFrame:
        """ Calculates rolling mean given direction, start and end
        """
        window = 1 + end - start
        sign = "minus" if direction == "backward" else "plus"
        col_name = f"rolling_mean_{column}_{sign}_{start}_to_{end}"
        cols_before_rollings = list(df.columns)
        grouper = [Index.product.value, Index.location.value, Index.customer.value]
        df, grouper = add_index(df, grouper, additional_index)

        df = df.sort_index()

        if direction == "backward":
            df = df.reset_index()
            df[col_name] = df.groupby(grouper)[column].transform(
                lambda x: x.rolling(window, 1).mean().shift(start)
            )
            if additional_index:
                df = df.set_index(Indexes + additional_index)
            else:
                df = df.set_index(Indexes)
        else:
            df[col_name] = df.groupby(level=grouper, as_index=False)[column] \
                .rolling(window).mean().round(5).groupby(level=grouper) \
                .shift(-end).reset_index(level=0, drop=True).to_frame()

        new_rolling_cols = [col for col in df.columns if col.startswith("rolling")]

        if additional_index:
            df = reset_additional_index(
                df,
                list(set(cols_before_rollings).union(set(new_rolling_cols))),
                additional_index
            )

        return df

    def apply_rolling_quantiles(
            self, df: pd.DataFrame, column: str,
            depths: List, quantiles: List) -> pd.DataFrame:
        """ Applies rolling quantiles with dask
        """
        results = [
            compute_rolling_quantile(df, column, depth, quantile)
            for depth, quantile in product(depths, quantiles)
        ]
        for col_name, col_data in results:
            df[col_name] = col_data
        return df

    def apply_columns_shift(
            self, df: pd.DataFrame,
            columns: List[str], direction: str) -> pd.DataFrame:
        """ Applies horizontal shift on selected columns
        """
        periods = -1 if direction == "left" else 1
        df[columns] = df[columns].shift(periods=periods, axis=1)
        return df

    def apply_evolution(
            self, df: pd.DataFrame, step: int,
            x: str, y: str, col_name: str) -> pd.DataFrame:
        """
        Computes the evolution in percentage between two columns

        Parameters
        ----------
        df: dataframe
        step: shifting period
        x: column to be deduced from
        y: column of reference
        col_name: name of the new column

        Returns
        -------
        df with newly created evolution column
        """
        df[col_name] = (df[x] - df[y]) / df[y] * 100
        df[col_name] = df[col_name].shift(periods=step).fillna(value=0)
        df[col_name] = df[col_name].map(lambda z: z if z != inf and z != -inf else 0)
        return df

    def apply_cum_means(
            self,
            df: pd.DataFrame,
            column: str,
            additional_index: List = None
    ) -> pd.DataFrame:
        """
        Computes the cumulative mean based of the
        aggregated days and quantities for a target
        """
        grouper = [Index.product.value, Index.location.value, Index.customer.value]
        df, grouper = add_index(df, grouper, additional_index)

        df = df.sort_index().reset_index()

        merger = df.merge(
            df.groupby(grouper)[Index.time.value].min().reset_index(),
            on=grouper, how="left"
        )
        df["cum_days"] = (merger[f"{Index.time.value}_x"] - merger[f"{Index.time.value}_y"]).dt.days
        df["cum_qty"] = df.groupby(grouper)[column].cumsum()
        df["cum_mean"] = df.cum_qty / df.cum_days

        if additional_index is None:
            additional_index = []
        df = df.set_index(list(set(grouper) - set(additional_index)) + [Index.time.value])

        return df

    def apply_comparison_on_column(
            self,
            df,
            column,
            operator,
            comparison,
            merge_columns,
            match_columns,
            match_with_index
    ):
        """
        Find values of the merge columns for past dates
        matching the condition on the column and the specified match columns
        (eg: past quantities when a feature is equal to zero for each categories)
        Works with time series
        """
        ops = {
            "==": (lambda x, y: x == y),
            "!=": (lambda x, y: x != y),
            ">": (lambda x, y: x > y),
            "<": (lambda x, y: x < y)
        }
        ops_names = {
            "==": "equals",
            "!=": "not_equals",
            ">": "superior_than",
            "<": "inferior_than"
        }
        df = df.reset_index()
        df = df.sort_values(Index.time.value)
        if match_with_index:
            match_columns = match_columns + Indexes
        by_columns = match_columns.copy()
        by_columns.remove(Index.time.value)
        df = pd.merge_asof(
            df,
            df.loc[ops[operator](df[column], comparison)][match_columns + merge_columns],
            by=by_columns,
            left_on=Index.time.value,
            right_on=Index.time.value,
            suffixes=(None, f"_compare_on_{column}_{ops_names[operator]}_{comparison}")
        )
        df = df.set_index(Indexes)
        return df.fillna(0)

    def apply_view_from_lag(
            self,
            df,
            time_base,
            view_from_depths,
            lag_columns,
            match_columns,
            match_with_index
    ):
        """
        Will create lagged columns with merge_asof based on a newly
        created date column (will be set by default on the saturday)
        """
        if match_with_index:
            match_columns = match_columns + Indexes

        for depth in view_from_depths:
            col_name = f"time_{depth}_{time_base}_before"
            df = df.reset_index()
            df = df.sort_values(Index.time.value)

            by_columns = match_columns.copy()
            by_columns.remove(Index.time.value)

            day_depth = depth
            if time_base == "week":
                day_depth = depth * 7

            df[col_name] = df[Index.time.value] - \
                timedelta(days=day_depth + 2) - \
                df[Index.time.value].dt.weekday.map(timedelta)

            df = pd.merge_asof(
                df,
                df[lag_columns + match_columns],
                by=by_columns,
                left_on=col_name,
                right_on=Index.time.value,
                suffixes=(None, f"_{depth}_{time_base}_before")
            )

            df = df.drop([col_name, f"{Index.time.value}_{depth}_{time_base}_before"], axis=1)

            df = df.set_index(Indexes)
        return df

    def apply_custom_string_matching(
        self, df, column, matches, threshold
    ):
        for i, match_group in enumerate(matches):
            if isinstance(match_group, str):
                df[f"{column}_{i}"] = df[column].str.contains(match_group)
            elif isinstance(match_group, list):
                df[f"{column}_{i}"] = False
                for match in match_group:
                    df.loc[
                        ~df[f"{column}_{i}"], f"{column}_{i}"
                    ] = df.loc[
                        ~df[f"{column}_{i}"], column
                    ].str.contains(match)
            else:
                raise Exception(f"'match_group' type {type(match_group)} is not supported.")
        df = df.drop(
            columns=list(((df.filter(regex=f"{column}_[0-9]*").sum() / len(df)) > threshold).index)
        )
        return df


def compute_iso_format(date: pd.DatetimeIndex, fmt: str) -> pd.array:
    """ Return isoformat of date index
    """
    strftimes = {
        "iso_year": "%G",
        "iso_month": "%m",
        "iso_week": "%V",
        "iso_week_day": "%u",
        "iso_day_of_year": "%j",
    }
    if fmt in strftimes:
        return date.strftime(strftimes[fmt]).values

    if fmt == "iso_week_of_month":
        return ((date.day - 1) // 7 + 1).values

    raise ValueError("Unexpected format %s" % fmt)


def compute_rolling_quantile(
        df: pd.DataFrame, column: str,
        depth: int, quantile: str) -> Tuple[str, pd.DataFrame]:
    """ Computes a rolling quantile on a column, based on a rolling step
    and a quantile type
    """
    quant = Quantile[quantile]
    col_name = f"{column}_quantile_{quant.name}_minus_{depth}"
    col_data = df[column].groupby(list(set(Indexes) - set(["to_dt"]))).rolling(depth) \
        .quantile(quant.value).reset_index(drop=True)
    return col_name, col_data


def add_index(
        df: pd.DataFrame,
        grouper: List,
        additional_index: List = None) -> Tuple[pd.DataFrame, List]:
    """
    Returns df with additional index set and the grouper to use
    (product, location and the additional indexes if needed)
    """
    if not additional_index:
        return df, grouper
    df = df.set_index(additional_index, append=True)
    grouper = grouper + additional_index
    return df, grouper


def reset_additional_index(
        df: pd.DataFrame,
        columns: List,
        additional_index: List) -> pd.DataFrame:
    """ Reset and reorganize df after using
    additional indexes
    """
    for i in range(0, len(additional_index)):
        df = df.reset_index(level=4, drop=False)
    return df.reindex(columns=columns)
