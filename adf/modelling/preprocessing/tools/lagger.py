from typing import Union, Dict
from datetime import timedelta
import pandas as pd  # type: ignore
import logging
from adf.modelling.preprocessing.tools import Preprocessor
from adf.enums import Index, Indexes


log = logging.getLogger("adf")


def generate_lags(df: pd.DataFrame, pp: Preprocessor) -> pd.DataFrame:
    """
    generate_lags shifts the selected columns values (lags) on a fixed amount
    of time (the offset, on the time_column) and inserts it in a
    new dedicated column. It creates one new column per backward
    or forward lag operation
    """
    for lag in pp.get("lags"):
        df = process_lag(
            df, lag.get("column"), lag.get("direction"),
            lag.get("count"), lag.get("step"), lag.get("condition")
        )
    return df


def process_lag(
    df: pd.DataFrame, column: str, direction: str, count: int, step: int, condition: Dict
):
    sign = "minus" if direction == "backward" else "plus"
    for step_number in range(1, count + 1):
        shift = step_number * step
        lagged_column = f"{column}_{sign}_{shift}"
        lagged_data = calculate_lag(df, column, shift, direction, condition)
        df[lagged_column] = lagged_data
        if condition:
            df = calculate_jump(df, lagged_column, direction, condition)
    return df


def calculate_lag(
    df: pd.DataFrame, column: str, shift: int, direction: str, condition: Dict
) -> Union[pd.DataFrame, pd.Series]:
    if direction == "forward":
        shift *= -1
    if condition and condition.get("skip_lag_on_not_equals", None):
        df_match = df.loc[
            df[condition["column"]] == condition["not_equals"]
        ].groupby(level=[0, 1, 2])[column].shift(shift)
        result = df.groupby(level=[0, 1, 2])[column].shift(shift)
        result.loc[result.index.isin(df_match.index)] = df_match
    else:
        result = df.groupby(level=[0, 1, 2])[column].shift(shift)
    return result


def calculate_jump(df, column, direction, condition):
    log.info(
        f"Computing lag jump for column {column}"
    )
    data = df.copy()
    iterator = 0
    remaining = 0
    while (
        len(data.loc[data[condition["column"]] != condition["not_equals"]])
    ) and (
        len(data.loc[data[condition["column"]] != condition["not_equals"]]) != remaining
    ):
        remaining = len(data.loc[data[condition["column"]] != condition["not_equals"]])
        log.info(
            f"Lag jump iteration for {column} : {iterator}"
            f" - {remaining} remaining..."
        )
        iterator += 1
        jump = condition["days_jump"] * (-1 if direction == "forward" else 1) * iterator
        shifted_data = data.copy().reset_index()
        shifted_data.loc[
            shifted_data[condition["column"]] != condition["not_equals"],
            f"{Index.time.value}_shifted"
        ] = shifted_data.loc[
            shifted_data[condition["column"]] != condition["not_equals"], Index.time.value
        ] - timedelta(days=jump)
        shifted_data = shifted_data.loc[~shifted_data[f"{Index.time.value}_shifted"].isnull()]
        shifted_data = shifted_data.drop(columns=Index.time.value)\
            .rename(columns={f"{Index.time.value}_shifted": Index.time.value})\
            .set_index(Indexes)
        to_replace = shifted_data.join(
            data,
            on=Indexes,
            how="left",
            lsuffix="_x"
        )[[column, condition["column"], f"{column}_x", f'{condition["column"]}_x']]
        to_replace = to_replace.reset_index()
        to_replace[Index.time.value] = to_replace[Index.time.value] + timedelta(days=jump)
        to_replace = to_replace.set_index(Indexes)
        to_replace = to_replace.loc[~to_replace[column].isnull()]
        data.loc[
            data.index.isin(to_replace.index.values),
            [column, condition["column"]]
        ] = to_replace[[column, condition["column"]]]
    df[column] = data[column]
    return df
