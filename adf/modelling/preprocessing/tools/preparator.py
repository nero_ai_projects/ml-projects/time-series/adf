import numpy as np  # type: ignore
from pandas.api.types import is_datetime64_any_dtype as is_datetime  # type: ignore
from adf.enums import Index
from adf.enums import Indexes
from adf.modelling.preprocessing.tools import Preprocessor
from adf.types import DataFrame, Series


def set_up_dataframe(df: DataFrame, pp: Preprocessor) -> DataFrame:
    """ Performs na values preprocessing, columns type casting and values sorting
    """
    return df.reset_index(drop=True)\
        .pipe(remove_categories)\
        .pipe(process_na, pp)\
        .pipe(cast, pp)\
        .pipe(scope, pp)


def remove_categories(df: DataFrame) -> DataFrame:
    """ Removes categorical columns
    """
    for column in df.columns:
        df[column] = category_to_str(df[column])
    return df


def category_to_str(series: Series):
    if series.dtype.name == 'category':
        return series.astype(str)
    else:
        return series


def process_na(df: DataFrame, pp: Preprocessor) -> DataFrame:
    """ Replace na values
    """
    non_date_columns = [col for col, typ in zip(df.columns, df.dtypes) if not is_datetime(typ)]

    if pp.na_value is None:
        df[non_date_columns] = df[non_date_columns].replace([np.inf, -np.inf], np.nan)
        return df
    df[non_date_columns] = df[non_date_columns] \
        .replace((np.inf, -np.inf), np.nan) \
        .fillna(pp.na_value)
    return df


def cast(df: DataFrame, pp: Preprocessor) -> DataFrame:
    """ Casts columns to be used as index
    """
    df[Index.product.value] = (
        df[pp.product.column].astype(int) if pp.product.column in df.columns else 0
    )
    df[Index.location.value] = (
        df[pp.location.column].astype(int) if pp.location.column in df.columns else 0
    )
    df[Index.customer.value] = (
        df[pp.customer.column].astype(int) if pp.customer.column in df.columns else 0
    )
    df[Index.time.value] = df[pp.time.column].astype('datetime64[ns]')
    df[pp.ground_truth.column] = df[pp.ground_truth.column].astype(float)
    # df[Index.time.value] = df[pp.time.column].astype('M8[us]')
    return df


def scope(df: DataFrame, pp: Preprocessor) -> DataFrame:
    """ Returns DataFrame with required columns
    """
    return df[Indexes + pp.get("features")]
