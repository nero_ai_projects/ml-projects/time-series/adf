import logging
import pandas as pd  # type: ignore
import dask.dataframe as dd  # type: ignore
from typing import List, Dict, Callable
from jsonschema import validate  # type: ignore
from jsonschema.exceptions import ValidationError  # type: ignore
from adf.config import load_config
from adf import utils
from adf.enums import Indexes
from adf.modelling.preprocessing import tools
from adf.utils.dataframe import memory_reduction
from adf.types import DataFrame


log = logging.getLogger("adf")


class Preprocessor(utils.Schema):

    SCHEMA = load_config("modelling/preprocessing.json")
    GENERATOR_SCHEMA = load_config("modelling/generator.json")

    def __init__(self, config: Dict):
        super().__init__(config)
        validate(config.get("generating_functions"), self.GENERATOR_SCHEMA)
        self.validate_tags(config)

    @property
    def read_columns(self) -> List:
        columns = set(self.get("features"))
        other_columns = []
        for column in [self.product.column, self.location.column, self.customer.column]:
            if column:
                other_columns.append(column)
        return list(columns.union([self.time.column, *other_columns]))

    def initialize_dataframe(self, df: DataFrame):
        return df.pipe(self._logdf(tools.set_up_dataframe), self) \
            .pipe(self._logdf(tools.encode), self) \
            .pipe(self._logdf(tools.time_base_aggregate), self)

    def preprocess(
        self,
        df: DataFrame,
        reset_index: bool = False
    ) -> pd.DataFrame:
        if isinstance(df, dd.DataFrame):
            df = df.compute()
        if df.index.names != Indexes:
            df = df.set_index(Indexes)
        df = memory_reduction(df)
        df = df\
            .pipe(self._logdf(tools.generate_lags), self)\
            .pipe(self._logdf(tools.generate_features), self.generating_functions)

        if self.na_value is not None:
            df = df.fillna(self.na_value)
        if reset_index:
            df = df.reset_index()
        df = df[sorted(df.columns)]
        return df

    def preprocess_for_partition(self, reset_index: bool = False):
        def preprocess_pandas_udf(df: DataFrame):
            return self.preprocess(df, reset_index)
        return preprocess_pandas_udf

    def _logdf(self, func: Callable) -> Callable:
        return utils.shapeit(utils.timeit(func))

    @staticmethod
    def validate_tags(conf):
        tags = conf["tags"]

        if tags["country"] == "france" and not tags["division"] in (
                "waters", "dairy_reg", "dairy_veg"
        ):
            raise ValidationError("Config Tags are incorrect")

        if tags["country"] == "spain" and tags["division"] != "dairy":
            raise ValidationError("Config Tags are incorrect")
