import logging
from typing import List, Set, Dict, Optional, Union
import re
from datetime import datetime, date
from dateutil.relativedelta import relativedelta  # type: ignore
from adf.services.storage import read_parquet
from pyspark.sql.functions import col  # type: ignore
import pandas as pd  # type: ignore
import databricks.koalas as ks  # type: ignore
from pyspark.sql import SparkSession  # type: ignore
from pandas.api.types import is_unsigned_integer_dtype  # type: ignore
import numpy as np  # type: ignore
import dask.dataframe as dd  # type: ignore
from adf import config
from adf.enums import Index, Indexes
from adf import utils
from adf.types import DataFrame
from adf.utils.credentials import get_secret

log = logging.getLogger("adf")


def get_snowflake_spark_options(
    secret=None,
    warehouse=config.SNOWFLAKE_WAREHOUSE,
    database=config.SNOWFLAKE_DATABASE,
    schema=config.SNOWFLAKE_SCHEMA,
):
    if not secret:
        secret = get_secret(config.SNOWFLAKE_PWD_NAME)

    return {
        "sfUrl": "danone.west-europe.privatelink.snowflakecomputing.com",
        "sfUser": config.SNOWFLAKE_USER,
        "sfPassword": secret,
        "sfDatabase": database,
        "sfSchema": schema,
        "sfWarehouse": warehouse,
    }


def get_snowflake_df_spark(database, schema, source, df_type, **kwargs):
    spark = SparkSession.builder.getOrCreate()
    options = get_snowflake_spark_options(database=database, schema=schema, **kwargs)
    query = f'select * from "{database}"."{schema}"."{source}"'
    df = spark.read.format("snowflake").options(**options).option("query", query).load()
    df = df.select([col(column).alias(column.lower()) for column in df.columns])
    if ("partition_nb" in kwargs) and ("repartition_keys" in kwargs):
        if isinstance(kwargs["repartition_keys"], str):
            repartition_keys = [kwargs["repartition_keys"]]
        else:
            repartition_keys = kwargs["repartition_keys"]
        df = df.repartition(kwargs["partition_nb"], repartition_keys)
    if df_type == "spark":
        return df
    elif df_type == "koalas":
        df = ks.DataFrame(df)
        # Convert time columns into koalas datetime
        for column in df.columns:
            if ("date" in column or "dt" in column) and df[column].dtype == object:
                column_values = df[column].dropna()
                if len(column_values) > 0 and isinstance(column_values.iloc[0], date):
                    df[column] = ks.to_datetime(df[column], errors="coerce")
        return df
    else:
        raise Exception(f"Could not load dataframe from snowflake with type {df_type}")


def get_snowflake_df(
    database: Union[str, Optional[str]], schema: str, source: str, filter_query: str = "", **kwargs
) -> pd.DataFrame:
    """
    Loads pandas dataframe given schema x database x view/table


    Parameters
    ----------
    database: str
        database in which the view or table is stored
    schema: str
        schema of the view or table
    source: str
        table or view name
    filter_query: str, default is ""
        additional filter in query that should be added to default query

    Returns
    -------
    pandas.DataFrame
    """
    ctx = utils.get_snowflake_connector(database=database, schema=schema, **kwargs)
    query = f'select * from "{database}"."{schema}"."{source}"'
    if filter_query != "":
        query += f" {filter_query}"
    data = pd.read_sql(query, ctx)
    data = data.rename(columns={k: k.lower() for k in data.columns})
    # Convert time columns into pandas datetime
    for column in [
        col
        for col in data.columns
        if len(data[col].dropna()) > 0 and isinstance(data[col].dropna().iloc[0], date)
    ]:
        data[column] = pd.to_datetime(data[column], errors="coerce")
    return data


def get_dask_df(country: str, division: str, source: str) -> dd.DataFrame:
    """
    Loads all S3 parquet flat files for a specified country x division x data source and
    concatenates them into a unified dask.dataframe.

    Parameters
    ----------
    country: str
        country for which to perform the extract for
    division: str
        division for which to perform the extract for
    source: str
        source for which to perform the extract for

    Returns
    -------
    dask.dataframe.DataFrame
        Concatenation of S3 flat parquet files
    """
    try:
        log.info(f"{source}: get data")
        df = read_parquet(
            config.MOUNT,
            f"{config.FOLDER_REPORTS_DATA}/{country}/{division}/{source}/*.parquet.gzip",
            object_type="dask",
        )
        for col_name in df.columns:
            if (
                isinstance(df[col_name].dtype, pd.api.extensions.ExtensionDtype)
            ) and is_unsigned_integer_dtype(df[col_name].dtype):
                df[col_name] = df[col_name].astype(int)
        return df
    except (Exception, ValueError) as err:
        log.info(f"{source}: unavailable data source - {err}")
        return None


def get_koalas_df(country: str, division: str, source: str) -> dd.DataFrame:
    """
    Loads all S3 parquet flat files for a specified country x division x data source and
    concatenates them into a unified koalas.DataFrame.

    Parameters
    ----------
    country: str
        country for which to perform the extract for
    division: str
        division for which to perform the extract for
    source: str
        source for which to perform the extract for

    Returns
    -------
    dask.dataframe.DataFrame
        Concatenation of S3 flat parquet files
    """
    try:
        log.info(f"{source}: get data")
        df = read_parquet(
            config.MOUNT,
            f"{config.FOLDER_REPORTS_DATA}/{country}/{division}/{source}/",
            object_type="koalas",
        )
        for col_name in df.columns:
            if (
                isinstance(df[col_name].dtype, pd.api.extensions.ExtensionDtype)
            ) and is_unsigned_integer_dtype(df[col_name].dtype):
                df[col_name] = df[col_name].astype(int)
        return df
    except (Exception, ValueError) as err:
        log.info(f"{source}: unavailable data source - {err}")
        return None


def prep_holidays(df):
    df["date"] = pd.to_datetime(df["date"], format="%Y-%m-%d")
    df.sort_values("date", inplace=True)
    df["eve"] = df.groupby("region").is_holiday.shift()
    df.loc[(df.date.dt.weekday == 0) & (df.eve == 1), "is_holiday"] = 1
    df.loc[df["date"].dt.weekday == 6, "is_holiday"] = 0
    df.rename(columns={"date": "to_dt"}, inplace=True)
    return df[["to_dt", "is_holiday", "region"]]


def get_target_date(
    prediction_date: datetime, step: int, conf: utils.BaseSchema, algorithm_type: Optional[str]
) -> datetime:
    """Returns the target date based on prediction date and step.
    Week-based forecasts are shifted to Monday.
    Month-based forecasts are shifted to first day of the month.

    Parameters
    ----------
    prediction_date: datetime
        date from which the prediction is launched
    step: int
        target time horizon
    conf: utils.BaseSchema
        configuration used by Forecaster
    algorithm_type: Optional[str]
        time-level of forecasts. Can be None, "day", "week", "month", "daily_disaggregation" or
        "weekly_disaggregation"

    Returns
    -------
    datetime
        target date of forecast associated to prediction date and other parameters
    """
    if conf.time.level == "day" and algorithm_type == "daily_disaggregation":
        return utils.start_of_week(prediction_date) + relativedelta(days=int(step))
    if conf.time.level == "week" and algorithm_type == "weekly_disaggregation":
        return utils.start_of_month(prediction_date) + relativedelta(weeks=int(step))
    if conf.time.level == "day" and algorithm_type in (None, "day"):
        return prediction_date + relativedelta(days=int(step))
    if conf.time.level == "week" and algorithm_type in (None, "week"):
        return utils.start_of_week(prediction_date) + relativedelta(weeks=int(step))
    if conf.time.level == "month" and algorithm_type in (None, "month"):
        return utils.start_of_month(prediction_date) + relativedelta(months=int(step))

    raise ValueError(f"Unexpected time level {conf.time.level} for algorithm_type {algorithm_type}")


def find_all_features(df: DataFrame, features: List[str]) -> List[str]:
    final_features: List[str] = []
    for feature in features:
        match = False
        for column in df.columns:
            if re.match(f"^{feature}$", column):
                final_features.append(column)
                match = True
        if not match:
            raise ValueError(
                f'Specified feature "{feature}" does not exist in preprocessed '
                f"dataset. Please correct configuration file"
            )
    seen: Set[str] = set()
    all_features = []
    for feature in final_features:
        if feature not in seen:
            all_features.append(feature)
        else:
            seen.add(feature)
    return all_features


def split(df: DataFrame, time_base: str, splits: List[Dict]) -> DataFrame:
    """
    Successively applies filters contained in splits parameter onto input dataframe df for a
    specified time base

    Parameters
    ----------
    df: pandas.DataFrame
        input dataframe indexed by product x location x target date
    time_base: str
        "day", "week" or "month"
    splits: List[dict]
        list of dictionaries describing nature of filter/split to apply

    Returns
    -------
    pandas.DataFrame
        Filtered dataframe
    """
    for split in splits:
        df = apply_split(df, time_base, split)
    return df


def apply_split(df, time_base: str, split):
    """
    Applies one specified filter contained in my_split parameter onto input dataframe df for a
    specified time base

    Parameters
    ----------
    df: pandas.DataFrame
        input dataframe indexed by product x location x target date
    time_base: str
        "day", "week" or "month"
    split: dict
        list of dictionaries describing nature of filter/split to apply

    Returns
    -------
    pandas.DataFrame
        Filtered dataframe
    """
    if split["method"] in [
        "min",
        "max",
        "sum",
        "cumsum",
        "mean",
        "median",
        "unique",
        "nunique",
        "count",
    ]:
        name = time_base + "_" + split["method"]
        if isinstance(df, pd.DataFrame):
            comparison_df = (
                df.copy()
                .groupby(level=Index.product.value)
                .agg({split["column"]: split["method"]})
                .rename({split["column"]: name}, axis=1)
            )
            df = df.join(comparison_df, on=Index.product.value, how="inner")
        elif isinstance(df, ks.DataFrame):
            df = df.reset_index()
            comparison_df = (
                df.copy()
                .groupby(Index.product.value)
                .agg({split["column"]: split["method"]})
                .rename({split["column"]: name}, axis=1)
                .reset_index()
            )
            df = df.merge(comparison_df, on=Index.product.value, how="inner")
            df = df.set_index(Indexes)
        df = df.query(build_split_query(df, name, split["operators"], split["comparisons"]))
        df = df.drop(columns={name})
    if split["method"] == "in":
        if split.get("column", None):
            df = df.loc[(df[split["column"]].isin(split["list"]))]
        elif split.get("index", None):
            if isinstance(df, pd.DataFrame):
                df = df.loc[df.index.get_level_values(split["index"]).isin(split["list"])]
            elif isinstance(df, ks.DataFrame):
                df = df.reset_index()
                df = df.loc[df[split["index"]].isin(split["list"])].set_index(Indexes)
        else:
            raise Exception(f"Incompatible arguments for split on {type(df)}")
    if split["method"] == "not_in":
        if split.get("column", None):
            df = df.loc[~(df[split["column"]].isin(split["list"]))]
        elif split.get("index", None):
            if isinstance(df, pd.DataFrame):
                df = df.loc[~(df.index.get_level_values(split["index"]).isin(split["list"]))]
            elif isinstance(df, ks.DataFrame):
                df = df.reset_index()
                df = df.loc[~(df[split["index"]].isin(split["list"]))].set_index(Indexes)
        else:
            raise Exception(f"Incompatible arguments for split on {type(df)}")
    return df


def build_split_query(df: DataFrame, name: str, operators: List[str], comparisons: Dict) -> str:
    """
    Generates query as string based on input parameters

    Parameters
    ----------
    df: pandas.DataFrame
        input dataframe indexed by product x location x target date
    name: str
        column name on which to apply filter
    operators: List[str]
        list of operators used for filtering
    comparisons: dict
        dictionary containing the parameters to generate the reference to compare with (threshold,
        nature of comparison, multiplier, function to apply etc.)

    Returns
    -------
    pandas.DataFrame
        Filtered dataframe
    """
    if comparisons.get("nature") == "fix":
        thresholds = comparisons["thresholds"]
        queries = [
            f"{name} {operator} {threshold}" for operator, threshold in zip(operators, thresholds)
        ]
    else:
        assert comparisons.get("nature") == "dynamic"
        multipliers, functions = comparisons["multipliers"], comparisons["functions"]
        queries = []
        for operator, multiplier, func in zip(operators, multipliers, functions):
            value = getattr(np, func)(
                df[name].to_numpy() if isinstance(df, ks.DataFrame) else df[name]
            )
            queries.append(f"{name} {operator} {multiplier} * {value}")
    if len(queries) > 1:
        queries[1:] = ["and " + query for query in queries[1:]]
    return " ".join(queries)
