from typing import Tuple
import logging

import mlflow  # type: ignore
import pandas as pd  # type: ignore
from dask import compute  # type: ignore
from dask import delayed  # type: ignore
import databricks.koalas as ks  # type: ignore
from pyspark.sql import SparkSession  # type: ignore
from sqlalchemy.orm import Session  # type: ignore
from sqlalchemy.orm.attributes import flag_modified  # type: ignore
from uuid import UUID
from typing import Union, Dict, List, Optional
from adf import utils
from adf.enums import Indexes, Index
from adf.modelling.training import tools
from adf.modelling import mllogs
from adf.modelling.tools import split, find_all_features
from adf.services.database.query.reports import (
    build_report, update_report, get_report
)
from adf.utils.credentials import get_secret
from adf.services.database import get_session
from adf.services.database import PreprocessingReports
from adf.services.database import TrainingReports
from adf.errors import TrainingError
from adf.types import DataFrame, Model
from adf.config import ENV, SNOWFLAKE_PWD_NAME


log = logging.getLogger("adf")


def run(
        config: dict, preprocessing_id: Union[str, UUID],
        filters_config: list, splits_config: Optional[List[Dict]] = None,
        backtest=False, **context
) -> TrainingReports:
    """ Method that takes as input
    1) a configuration file describing the ML model to create
    2) the preprocessing id that contains data for training
    3) a filter config to specify which product to include into/exclude from training
    4) a splits config to only train on a subset of the data

    Returns the TrainingReport
    """
    session = context["session"]
    algorithm_type = context.get("algorithm_type", None)

    log.info("Loading PreprocessingReport")
    preprocessing_report = get_report(
        session, preprocessing_id, PreprocessingReports
    )
    mllogs.tag_report(preprocessing_report)

    log.info("Building Trainer")
    trainer = tools.Trainer(config)
    if isinstance(preprocessing_report.description, dict):
        trainer.merge_config(preprocessing_report.description)
    else:
        raise Exception(
            f"Preprocessing report's description of type : "
            f"{type(preprocessing_report.description)} cannot be merged with training config "
            f"of type : {type(config)} !"
        )
    mllogs.log_json_artifact(config, "config.json")
    mllogs.log_levels(trainer)

    log.info("Building models list")
    models_list = trainer.build_model_list()
    mllogs.log_models(trainer)

    log.info("Building quantiles models")
    upper_quantile_model, lower_quantile_model = None, None
    if trainer.get("uncertainty"):
        upper_quantile_model = trainer.build_quantile_model(
            parameters=trainer.uncertainty.upper_quantile.get("parameters")
        )
        lower_quantile_model = trainer.build_quantile_model(
            parameters=trainer.uncertainty.lower_quantile.get("parameters")
        )

    log.info("Loading data")
    validate_predict_steps(trainer)
    df = preprocessing_report.download(
        object_type=trainer.get("object_type", "pandas"),
        engine=trainer.get("engine_type", "auto")
    )
    if (
        not set(Indexes).issubset(df.columns)
    ):
        raise ValueError("Input DataFrame doesn't have expected Indexes")

    if isinstance(df, ks.DataFrame):
        df[Index.time.value] = ks.to_datetime(df[Index.time.value])
        if config.get("partitioned_on", None):
            df[config.get("partitioned_on", None)] = (
                df[config.get("partitioned_on", None)].astype(int)
            )

    if filters_config is None:
        filters_config = []
    if "train_product_filters" in config:
        log.info("Applying filters")
        filters_config.extend(config['train_product_filters'])

    filters = utils.Filters(filters_config)
    df = filters.filter(df)
    mllogs.log_shape(df)
    mllogs.log_json_artifact(filters_config, "filters.json")
    df = df.set_index(Indexes)

    features = get_all_features(df, trainer)
    df = df[features]
    df[list(df.select_dtypes("category").columns)] = df[
        list(df.select_dtypes("category").columns)
    ].astype(str)
    mllogs.log_shape(df)
    if splits_config:
        log.info(f"Training on splits {splits_config}")
        df = split(
            df,
            trainer.time.level,
            splits_config
        )
        if not len(df):
            models_list = add_reports(
                session, trainer, preprocessing_report, models_list,
                upper_quantile_model=upper_quantile_model,
                lower_quantile_model=lower_quantile_model,
                pipeline_id=context.get("pipeline_id", None)
            )
            return models_list[0]["report"]

    log.info("Check chosen objective method and clip df to zero if necessary")
    df = clip_df_target(df, trainer)

    log.info("Splitting train set")
    train_set = trainer.get_train_set(
        df, config.get("exclude_train_intervals", None), algorithm_type
    )
    train_x, train_y = trainer.split_x_y(train_set)
    mllogs.log_shape(train_set)

    log.info("Validating train set")
    trainer.validate_train_set(train_x)

    log.info("Adding reports")
    models_list = add_reports(
        session, trainer, preprocessing_report, models_list,
        upper_quantile_model=upper_quantile_model,
        lower_quantile_model=lower_quantile_model,
        pipeline_id=context.get("pipeline_id", None)
    )

    test_set = trainer.get_test_set(df, backtest, algorithm_type)

    current_run = mlflow.active_run()
    current_run_id = current_run.info.run_id
    current_experiment_name = mlflow.get_experiment(current_run.info.experiment_id).name

    scheduler = trainer.get("scheduler", None)
    if scheduler == "single-threaded":
        if isinstance(train_x, ks.DataFrame):
            train_x = train_x.to_pandas()
        if isinstance(train_y, ks.DataFrame):
            train_y = train_y.to_pandas()
        if isinstance(test_set, ks.DataFrame):
            test_set = test_set.to_pandas()
        method = train_model_for_horizon(
            config=config,
            test_set=test_set,
            train_x=train_x, train_y=train_y,
            upper_quantile_model=upper_quantile_model,
            lower_quantile_model=lower_quantile_model,
            algorithm_type=algorithm_type,
            splits_config=splits_config,
            filters_config=filters_config,
            current_run_id=current_run_id,
            current_experiment_name=current_experiment_name,
            secret=(
                get_secret(SNOWFLAKE_PWD_NAME) if ENV not in ("local", "test")
                else None
            )
        )
        for i, model in enumerate(models_list):
            method((i, model))
    elif scheduler in ("processes", "threads"):
        if scheduler != "single-threaded":
            log.info("Setting up parallelization for models training")
        futures = []
        for i, model_dict in enumerate(models_list):
            futures.append(delayed(train_model)(
                i=i, model_dict=model_dict, config=config,
                test_set=test_set,
                train_x=train_x, train_y=train_y,
                upper_quantile_model=upper_quantile_model,
                lower_quantile_model=lower_quantile_model,
                algorithm_type=algorithm_type,
                splits_config=splits_config,
                filters_config=filters_config,
                current_run_id=current_run_id,
                current_experiment_name=current_experiment_name
            ))
        compute(
            *futures,
            num_workers=trainer.get("num_workers", None),
            scheduler=scheduler
        )
    else:
        log.info("Setting up parallelization for models training")
        if isinstance(train_x, ks.DataFrame):
            train_x = train_x.to_pandas()
        if isinstance(train_y, ks.DataFrame):
            train_y = train_y.to_pandas()
        if isinstance(test_set, ks.DataFrame):
            test_set = test_set.to_pandas()
        method = train_model_for_horizon(
            config=config,
            test_set=test_set,
            train_x=train_x, train_y=train_y,
            upper_quantile_model=upper_quantile_model,
            lower_quantile_model=lower_quantile_model,
            algorithm_type=algorithm_type,
            splits_config=splits_config,
            filters_config=filters_config,
            current_run_id=current_run_id,
            current_experiment_name=current_experiment_name,
            secret=(
                get_secret(SNOWFLAKE_PWD_NAME) if ENV not in ("local", "test")
                else None
            )
        )
        spark = SparkSession.builder.getOrCreate()
        spark.sparkContext.parallelize(list(enumerate(models_list))).foreach(method)

    models_list = concat_training_ids(models_list)
    flag_modified(models_list[0]["report"], "description")
    update_report(session, models_list[0]["report"], "success")
    return models_list[0]["report"]


def train_model_for_horizon(
    config: Dict,
    test_set: DataFrame, train_x: DataFrame, train_y: DataFrame,
    upper_quantile_model: Model, lower_quantile_model: Model,
    algorithm_type: str, splits_config: Optional[List[Dict]],
    filters_config: Optional[List[Dict]],
    current_run_id: str, current_experiment_name: str,
    secret=None
):
    def train_model_pandas_udf(data: Tuple[int, Dict]):
        i: int = data[0]
        model_dict: Dict = data[1]
        train_model(
            i=i, model_dict=model_dict, config=config,
            test_set=test_set,
            train_x=train_x, train_y=train_y,
            upper_quantile_model=upper_quantile_model,
            lower_quantile_model=lower_quantile_model,
            algorithm_type=algorithm_type,
            splits_config=splits_config,
            filters_config=filters_config,
            current_run_id=current_run_id,
            current_experiment_name=current_experiment_name,
            secret=secret
        )
    return train_model_pandas_udf


def train_model(
    i: int, model_dict: Dict, config: Dict,
    test_set: DataFrame, train_x: DataFrame, train_y: DataFrame,
    upper_quantile_model: Model, lower_quantile_model: Model,
    algorithm_type: str, splits_config: Optional[List[Dict]],
    filters_config: Optional[List[Dict]],
    current_run_id: str, current_experiment_name: str,
    secret=None
):
    """ Trains for one configuration of the models configurations
    """
    try:
        mlflow.end_run()
        mlflow.set_experiment(current_experiment_name)
        mlflow.start_run(run_id=current_run_id)
    except Exception:
        log.warning(f"Could not restart run_id {current_run_id}")
    session = get_session(secret)
    report = get_report(session, model_dict["report"].id, TrainingReports)
    preprocessing_report = get_report(
        session, report.preprocessing_report.id, PreprocessingReports
    )
    country = preprocessing_report.description.get("tags", {}).get("country", "unknown")
    division = preprocessing_report.description.get("tags", {}).get("division", "unknown")
    lower_quantile_report, upper_quantile_report = None, None
    if (
        model_dict.get("lower_quantile_report", None)
    ) and (
        model_dict.get("upper_quantile_report", None)
    ):
        lower_quantile_report = get_report(
            session, model_dict["lower_quantile_report"].id, TrainingReports
        )
        upper_quantile_report = get_report(
            session, model_dict["upper_quantile_report"].id, TrainingReports
        )
    try:
        trainer = tools.Trainer(config)
        mllogs.tag_report(report, "_" + model_dict["name"])
        log.info(f"Training model {i}")
        model_features = scope_model_features(trainer, model_dict)
        model_features = find_all_features(train_x, model_features)
        log.info(f"Starting fit model {i}...")
        trained_model, best_params, cv_results = trainer.fit(
            model_dict["model"], train_x[model_features], train_y
        )

        trained_upper_quantile_model, trained_lower_quantile_model = None, None
        if upper_quantile_model and lower_quantile_model:
            trained_upper_quantile_model, up_best_params, up_cv_results = trainer.fit(
                upper_quantile_model.copy(), train_x[model_features], train_y
            )
            trained_lower_quantile_model, low_best_params, low_cv_results = trainer.fit(
                lower_quantile_model.copy(), train_x[model_features], train_y
            )

        log.info(f"Fit ended successfully for model {i}!")
        if len(test_set):
            run_eval(
                trainer,
                trained_model,
                model_features,
                model_dict,
                test_set,
                cv_results,
                best_params
            )

        log.info(f"Saving model {i}")
        report.upload(
            trained_model,
            country=country,
            division=division,
            splits_config=splits_config,
            filters_config=filters_config,
            algorithm_type=algorithm_type,
            horizon=i,
            data=train_x[model_features].head(5)
        )
        update_report(session, report, "success")
        mllogs.tag_report_output(report)

        if trained_upper_quantile_model and trained_lower_quantile_model:
            log.info(f"Saving quantile models associated to model {i}")
            if upper_quantile_report:
                upper_quantile_report.upload(
                    trained_upper_quantile_model,
                    country=country,
                    division=division,
                    splits_config=splits_config,
                    filters_config=filters_config,
                    algorithm_type=algorithm_type,
                    horizon=i,
                    data=train_x[model_features].head(5),
                    quantile="upper"
                )
                update_report(session, upper_quantile_report, "success")
                mllogs.tag_report_output(upper_quantile_report)

            if lower_quantile_report:
                lower_quantile_report.upload(
                    trained_lower_quantile_model,
                    country=country,
                    division=division,
                    splits_config=splits_config,
                    filters_config=filters_config,
                    algorithm_type=algorithm_type,
                    horizon=i,
                    data=train_x[model_features].head(5),
                    quantile="lower"
                )
                update_report(session, lower_quantile_report, "success")
                mllogs.tag_report_output(lower_quantile_report)

    except Exception as err:
        message = str(err.__class__.__name__) + ": " + str(err)
        update_report(session, report, "failed", message)
        raise TrainingError(message) from err
    finally:
        mllogs.tag_report_status(report)


def run_eval(trainer: tools.Trainer,
             model: Model,
             models_columns: List,
             model_config: Dict,
             test_set: DataFrame,
             cv_results: Optional[Dict],
             best_params: Optional[Dict]
             ):
    """ Run evaluation on the test set and log metrics in mlflow
    """
    x_test, y_test = trainer.split_x_y(test_set)
    x_test = x_test[models_columns]
    mllogs.log_model_metrics(
        model, x_test, y_test, models_columns, model_config, cv_results
    )
    mllogs.log_json_artifact(best_params, f"best_params_{model_config['name']}.json")


def add_reports(session: Session,
                trainer: tools.Trainer,
                pp_report: PreprocessingReports,
                models: List,
                upper_quantile_model: Optional[Model],
                lower_quantile_model: Optional[Model],
                pipeline_id: Optional[str] = None) -> List:
    """ Add a TrainingReport and its id to each model wrapper
    """
    for i, model in enumerate(models):
        report = build_report(
            session, TrainingReports, description=trainer._config,
            preprocessing_report=pp_report, pipeline_id=pipeline_id
        )
        models[i]["id"] = str(report.id)
        models[i]["report"] = report
        if upper_quantile_model and lower_quantile_model:
            upper_quantile_report = build_report(
                session, TrainingReports, description=trainer._config,
                preprocessing_report=pp_report
            )
            mllogs.tag_report(
                upper_quantile_report, "_" + models[i]["name"] + "_upper_quantile"
            )
            models[i]["uncertainty_upper_id"] = str(
                upper_quantile_report.id
            )
            models[i]["upper_quantile_report"] = upper_quantile_report

            lower_quantile_report = build_report(
                session, TrainingReports, description=trainer._config,
                preprocessing_report=pp_report
            )
            mllogs.tag_report(
                lower_quantile_report, "_" + models[i]["name"] + "_lower_quantile"
            )
            models[i]["uncertainty_lower_id"] = str(
                lower_quantile_report.id
            )
            models[i]["lower_quantile_report"] = lower_quantile_report

    return models


def concat_training_ids(models_list: List):
    """ Add all models ids to the first model report
    """
    for i, model_dict in enumerate(models_list):
        models_list[0]["report"].description["models"][i]["id"] = model_dict["id"]
        models_list[0]["report"].description["models"][i]["uncertainty_lower_id"] = (
            model_dict["uncertainty_lower_id"]
        )
        models_list[0]["report"].description["models"][i]["uncertainty_upper_id"] = (
            model_dict["uncertainty_upper_id"]
        )
    return models_list


def clip_df_target(df: DataFrame, trainer: tools.Trainer) -> pd.DataFrame:
    """ Clips target values to zero depending on the algorithm objective method
    """
    objectives = (
        "reg:tweedie",
        "reg:gamma",
        "count:poisson"
    )
    for model in trainer.models._config:
        params = model["parameters"]
        if "objective" in params and params["objective"] in objectives:
            df[trainer.target] = df[trainer.target].clip(0)
    return df


def get_all_features(df: DataFrame, trainer: tools.Trainer) -> List:
    """ Returns common and specific features as a whole
    """
    all_features = trainer.common_features._config.copy()
    common_features = trainer.common_features._config.copy()
    for model in trainer.models._config:
        if len(set(model["specific_features"]).intersection(set(common_features))) > 0:
            raise ValueError("common features and specific features must not have duplicates")
        for feature in model["specific_features"]:
            if feature not in all_features:
                all_features += [feature]
    return find_all_features(df, all_features)


def scope_model_features(trainer: tools.Trainer, model: Dict) -> List:
    """ Returns the common and specific features for a model
    """
    common_features = trainer.common_features._config.copy()
    common_features.remove(trainer.target)
    return common_features + model["specific_features"]


def validate_predict_steps(trainer: tools.Trainer):
    """ Check if models prediction steps does not overlap nor have gaps
    """
    p_steps = []

    for model in trainer.models._config:
        p_steps.append(set(model.get("prediction_steps")))

    if len(p_steps) > 1 and len(set.intersection(*p_steps)) > 0:
        raise ValueError("Prediction steps must not overlap")
