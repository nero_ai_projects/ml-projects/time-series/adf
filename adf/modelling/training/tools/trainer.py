from typing import (
    Dict, Tuple, Union, Optional, List
)
import logging
from datetime import datetime, date, timedelta
import pandas as pd  # type: ignore
import dask.dataframe as dd  # type: ignore
import databricks.koalas as ks  # type: ignore
from jsonschema import validate  # type: ignore
import joblib  # type: ignore
from xgboost import XGBRegressor  # type: ignore
from xgboost.dask import DaskXGBRegressor  # type: ignore
from catboost import CatBoostRegressor  # type: ignore
from pyspark.ml.feature import VectorAssembler  # type: ignore
try:
    from catboost_spark import Pool, CatBoostRegressor as SparkCatBoostRegressor  # type: ignore
except (ImportError, AttributeError, ModuleNotFoundError):
    Pool, SparkCatBoostRegressor = None, None
from lightgbm import LGBMRegressor  # type: ignore
from lightgbm.dask import DaskLGBMRegressor  # type: ignore


from sklearn.ensemble import RandomForestRegressor  # type: ignore
from sklearn.model_selection import TimeSeriesSplit  # type: ignore
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV  # type: ignore

from adf import config
from adf.enums import Index, Indexes
from adf.utils import Schema
from adf.modelling.tools import get_target_date
from adf.types import Model, DataFrame


log = logging.getLogger("adf")


class Trainer(Schema):
    SCHEMA = config.load_config("modelling/training.json")
    GENERATOR_SCHEMA = config.load_config("modelling/generator.json")

    def __init__(self, config: Dict):
        super().__init__(config)
        validate(config.get("updating_functions"), self.GENERATOR_SCHEMA)

    def get_train_set(
            self, df: DataFrame, exclude_train_intervals: Union[List, None], algorithm_type
    ) -> DataFrame:
        train_set = df.reset_index()
        first_train_date = (
            self.train_dates[0] if len(self.train_dates) else (
                datetime.now() - timedelta(days=365 * 3)
            ).strftime("%Y-%m-%d")
        )
        train_set = train_set.loc[
            (
                train_set[Index.time.value] >= first_train_date
            ) & (
                train_set[Index.time.value] <= self.get_last_train_date(algorithm_type)
            )
        ]

        if exclude_train_intervals:
            for exclude_interval in exclude_train_intervals:
                train_set = train_set.loc[
                    (
                        train_set[Index.time.value] < exclude_interval[0]
                    ) | (
                        train_set[Index.time.value] > exclude_interval[1]
                    )
                ]
        train_set = train_set.set_index(Indexes)
        return train_set

    def get_test_set(self, df: DataFrame, backtest, algorithm_type) -> DataFrame:
        test_set = df.reset_index()
        if backtest:
            test_set = test_set.loc[
                test_set[Index.time.value] >= self.get_first_prediction_date(algorithm_type)
            ]
        else:
            test_set = test_set.loc[
                (
                    test_set[Index.time.value] > self.get_last_train_date(algorithm_type)
                ) & (
                    test_set[Index.time.value] <= self.get_first_prediction_date(algorithm_type)
                )
            ]
        test_set = test_set.set_index(Indexes)
        return test_set

    def get_last_train_date(self, algorithm_type):
        return ((datetime.strptime(self.get_first_prediction_date(algorithm_type), "%Y-%m-%d"
                                   ) - timedelta(days=1)).strftime("%Y-%m-%d")
                if len(self.train_dates) <= 1
                else self.train_dates[1]
                )

    def get_first_prediction_date(self, algorithm_type):
        return (
            get_target_date(date.today(), 0, self, algorithm_type).strftime("%Y-%m-%d")
            if len(self.predict_from_dates) == 0
            else min(self.predict_from_dates)
        )

    def split_x_y(self, df: DataFrame) -> Tuple[DataFrame, DataFrame]:
        """ Returns df splitted in a feature and a target part
        """
        return df[list(set(list(df.columns)) - set([self.target]))], df[[self.target]]

    def build_model_list(self) -> List:
        return [
            {
                "name": model.get("name"),
                "model": {
                    "XGBoost": XGBRegressor,
                    "DaskXGBoost": DaskXGBRegressor,
                    "CatBoost": CatBoostRegressor,
                    "SparkCatBoost": SparkCatBoostRegressor,
                    "LGBM": LGBMRegressor,
                    "DaskLGBM": DaskLGBMRegressor,
                    "RandomForest": RandomForestRegressor
                }[model.type](**model.get("parameters")),
                "specific_features": model.get("specific_features"),
                "id": "",
                "uncertainty_upper_id": "",
                "uncertainty_lower_id": ""
            }
            for model in self.models
        ]

    def build_quantile_model(self, parameters=None, model="CatBoost"):
        if parameters is not None:
            if model == "CatBoost":
                return CatBoostRegressor(**parameters)
            elif model == "LGBM":
                return LGBMRegressor(**parameters)
            elif model == "SparkCatBoost":
                return SparkCatBoostRegressor(**parameters)
            elif model == "DaskLGBM":
                return DaskLGBMRegressor(**parameters)
            else:
                raise ValueError(
                    "Please specify the uncertainty model is one of ['CatBoost', 'LGBM'] "
                    "in the training configuration file"
                )
        else:
            raise ValueError(
                "Please specify the uncertainty loss function in the training configuration file"
            )

    def fit(self, model: Model, train_x: DataFrame, train_y: DataFrame) \
            -> Tuple[Model, Optional[Dict], Optional[Dict]]:
        if isinstance(
            model, (XGBRegressor, RandomForestRegressor, CatBoostRegressor, LGBMRegressor)
        ):
            if isinstance(train_x, dd.DataFrame):
                train_x = train_x.compute()
            elif isinstance(train_x, ks.DataFrame):
                train_x = train_x.to_pandas()
            if isinstance(train_y, dd.DataFrame):
                train_y = train_y.compute()
            elif isinstance(train_y, ks.DataFrame):
                train_y = train_y.to_pandas()

        if self.learning.type == "grid_search":
            return self._fit_grid_search(model, train_x, train_y)
        if self.learning.type == "random_search":
            return self._fit_random_search(model, train_x, train_y)

        params = {}
        if isinstance(model, CatBoostRegressor):
            params["logging_level"] = "Silent"
        if SparkCatBoostRegressor and isinstance(model, SparkCatBoostRegressor):
            assembler = VectorAssembler(
                inputCols=list(train_x.columns),
                outputCol="features"
            ).setHandleInvalid("keep")
            train_x["label"] = train_y
            train = assembler.transform(train_x.to_spark())[["features", "label"]]
            train = Pool(train)
            model = model.fit(train)
        else:
            model.fit(train_x, train_y, **params)
        return model, None, None

    def _fit_grid_search(self, model: Model, train_x: pd.DataFrame, train_y: pd.DataFrame) \
            -> Tuple[Model, Dict, Dict]:
        params = self.learning.get("parameters")
        splits = params.pop("splits")
        time_series_split = TimeSeriesSplit(splits)
        grid_search = GridSearchCV(model, cv=time_series_split, **params)
        params["splits"] = splits
        with joblib.parallel_backend(self.learning.get("parallel_backend", "loky")):
            grid_search.fit(train_x, train_y)
        return grid_search.best_estimator_, grid_search.best_params_, grid_search.cv_results_

    def _fit_random_search(self, model: Model, train_x: pd.DataFrame, train_y: pd.DataFrame) \
            -> Tuple[Model, Dict, Dict]:
        params = self.learning.get("parameters")
        splits = params.pop("splits")
        time_series_split = TimeSeriesSplit(splits)
        random_search = RandomizedSearchCV(model, cv=time_series_split, **params)
        params["splits"] = splits
        with joblib.parallel_backend(self.learning.get("parallel_backend", "loky")):
            random_search.fit(train_x, train_y)
        return random_search.best_estimator_, random_search.best_params_, random_search.cv_results_

    def validate_train_set(self, train_x: DataFrame) -> None:
        """ Performs validation based on input and model type

        RandomForest:
            - doesn't accept NA
        """
        for model in self.models:
            if model.type == "RandomForest":
                if (
                    isinstance(train_x, pd.DataFrame) and train_x.isna().values.any()
                ) | (
                    isinstance(train_x, dd.DataFrame) and train_x.isna().compute().values.any()
                ):
                    raise ValueError(
                        "train_x contains NA values which "
                        "are not accepted by RandomForestRegressor"
                    )
