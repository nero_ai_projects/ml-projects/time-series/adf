import click
from adf.ru import modelling_cli
from adf.ru import setup


@click.group()
def cli():
    return


cli.add_command(modelling_cli.commands)

if __name__ == '__main__':
    setup.set_up_logging()
    cli()
