import pandas as pd
import numpy as np
from glob import glob

from adf.modelling.dataprep.rus import utils, config
from adf.ru import fc


def save_cv_dfs(sell_in_path: str = 'data/processed/si_mad_magnit.parquet',
                date_column: str = 'mad_date',
                output_folder: str = 'data/processed/horizons',
                date_ranges: list = [('2020-11-23', '2021-01-04'),
                                     ('2021-01-05', '2021-02-16'),
                                     ('2021-02-17', '2021-03-31'),
                                     ('2021-04-01', '2021-05-13')]) -> None:
    """
    Saves processed sell-in into several files with given forecast period ranges
    (thus the only difference of the datasets is in different forecast periods)
    """
    data = pd.read_parquet(sell_in_path)
    utils.add_folder(output_folder)

    keys = list()
    for start_fc_date, end_fc_date in date_ranges:
        horizon = data.copy()
        horizon = horizon[horizon[date_column] <= end_fc_date]
        horizon['partition'] = np.where(horizon[date_column] < start_fc_date, 'train', 'fc')
        horizon.to_parquet(f'{output_folder}/si_magnit_{start_fc_date}.parquet')

        keys.append(horizon[config.SI_KEY_COLUMNS][(horizon.partition == 'train') &
                                                   (horizon.ordered != 0)].drop_duplicates())
        keys.append(horizon[config.SI_KEY_COLUMNS][(horizon.partition == 'fc') &
                                                   (horizon.ordered != 0)].drop_duplicates())
    result_list_keys = keys[0].copy()
    for sample in keys:
        result_list_keys = result_list_keys.merge(sample)
    for horizon_path in glob(f'{output_folder}/*'):
        horizon = pd.read_parquet(horizon_path)
        horizon = horizon.merge(result_list_keys)
        horizon.to_parquet(horizon_path, index=False)


def one_exp(data: list, date_column: str, horizon: str,
            cut_history_months: int,
            used_columns: dict, model_params: dict) -> list:
    return [fc.predict_all(fold, used_columns,
                           date_column, model_params, horizon,
                           cut_history_months, out_path=False) for fold in data]


def calc_metrics(forecast: list, period: str = 'st', date_column: str = 'mad_date',
                 fc_col: str = 'fc', out_gr_cols: str = ['partition']) -> tuple:
    train_metric = list()
    test_metric = list()
    for sample in forecast:
        if 'innovation' in sample.columns:
            sample = sample[sample['innovation'] == 0]
        metrics = utils.fa_period(sample[sample.unused_history == 0],
                                  period=period, date_column=date_column,
                                  fc_col=fc_col, out_gr_cols=out_gr_cols)
        train_metric.append(round(metrics[metrics.partition == 'train']['Acc'].iloc[0], 5) * 100)
        test_metric.append(round(metrics[metrics.partition == 'fc']['Acc'].iloc[0], 5) * 100)
    return train_metric, test_metric
