import traceback

import pandas as pd
import numpy as np
import json

from tqdm import tqdm
from joblib import Parallel, delayed
import logging
from joblibspark import register_spark

from xgboost import XGBRegressor
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
from sklearn.compose import ColumnTransformer

import adf.modelling.dataprep.rus.config as config
import adf.modelling.dataprep.rus.utils as utils
import adf.modelling.dataprep.rus.si as si

from adf.ru import features
from adf.modelling.dataprep.rus.tools import get_snowflake_df, export_fc_to_snowflake_d0
from adf.ru import modelling_cli

from typing import Union

logging.basicConfig(filename=config.LOG_FOLDER + '/run_by_keys.log',
                    format='%(asctime)s - %(message)s',
                    level=logging.INFO,
                    datefmt='%Y-%m-%d %H:%M:%S')


def read_file(path_file: str) -> pd.DataFrame:
    """
        Reads the file and converts part of the columns to the categorical
    data type

    :param path_file: the file path
    :return: dataset from the file
    """
    data = pd.read_parquet(path_file)
    data[config.SI_KEY_COLUMNS + ['partition']] = \
        data[config.SI_KEY_COLUMNS + ['partition']
             ].astype('category')
    data.reset_index(drop=True, inplace=True)
    data['discount'] = data['discount'].astype('int8')

    return data


def predict_all(data: Union[str, pd.DataFrame],
                used_columns: dict,
                date_column: str,
                model_params: dict,
                horizon: str = 'st',
                cut_history_months: int = None,
                out_path: Union[str, bool] = config.FC_OUT_PATH,
                product_category: list = None,
                product_type: list = None,
                product_brand: list = None,
                client: list = None,
                target: str = 'ordered',
                bl: bool = False,
                d0: bool = False):
    """
        Prepares a dataset (or part of it based on input parameters)
    and starts training the model and creating a total forecast or baseline,
    parallelizing the process by keys

    :param data: the path to the input file of the input dataframe
    :param used_columns: a dict that contains a list of features, features to scale and
     features of one hot encode
    :param date_column: name of the date column
    :param model_params: dict of model parameters
    :param horizon: st, mt or lt horizon
    :param cut_history_months: leaves N months in the dataset
    :param out_path: the path to the output file
    :param product_md_path: the path to the product hierarchy file
    :param product_category: list of target categories, if the forecast is
     calculated only for them
    :param product_type: list of target product types, if the forecast is
     calculated only for them
    :param product_brand: list of target product brands, if the forecast is
     calculated only for them
    :param client: list of target client, if the forecast is calculated
     only for them
    :param target: name of the target column
    :param x_columns: list of features for the model
    :param date_column: name of the date column
    :param bl: if the flag=True - the baseline is calculated,
     by default flag = False - the total forecast is calculated
    :param d0: if the flag=True - run forecast for bl & uplift split
     by default flag = False
    :return: dataset with input information and added columns with features
     and forecast
    """

    logging.info('___________________________')
    logging.info('Info about model:')
    logging.info(f'features = {used_columns["feature_columns"]}')
    logging.info(f'bl = {bl}')
    logging.info(f'model parameters = {model_params}')
    logging.info('Start prepare dataset for forecast')
    register_spark()

    if type(data) == str:
        data = read_file(data)
    
    # For bl & uplift split
    if d0:
        if product_category is not None or product_type is not None or product_brand is not None: #bl or # TODO
            raise ValueError("d0 option can not work with filter options")

        yesterday = (config.FIRST_FORECAST_DATE - pd.Timedelta(days=14)).date() # TODO Temp value of 14 days, after run, set back 1 day
        last_train_date = pd.to_datetime(yesterday, format='%Y-%m-%d')
        data = data[data['mad_date'] < config.FIRST_FORECAST_DATE]

        data['partition'] = np.where(data['mad_date'] < last_train_date, 'train', 'fc')

    start_fc_date = data[data.partition == 'fc'][date_column].min()
    data = si.mark_innovations(data, date_column=date_column,
                               train_period_end=start_fc_date)
    if cut_history_months:
        data = utils.mark_cut_history(data, date_column, start_fc_date, cut_history_months)
    else:
        data['unused_history'] = False
    last_fc_date = utils.get_last_fc_date(horizon)
    data = data[data[date_column] <= last_fc_date]

    if product_category or product_type or product_brand:
        prod_md = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_EXT_PDT_MD')
        prod_md = prod_md[['mat_cod', 'brand_desc']].drop_duplicates() \
            .rename(columns={'mat_cod': 'SKU',
                             'brand_desc': 'BP_Brand'})
        prod_md.SKU = prod_md.SKU.astype(str)
        skus = set()
        if product_category is not None:
            sku_md = set(
                prod_md[prod_md.BP_ProdCategory.isin(product_category)].SKU)
            skus = skus | sku_md
        if product_type is not None:
            sku_md = set(prod_md[prod_md.PRD_ProdType.isin(product_type)].SKU)
            skus = skus | sku_md
        if product_brand is not None:
            sku_md = set(prod_md[prod_md.BP_Brand.isin(product_brand)].SKU)
            skus = skus | sku_md
        if len(skus) > 0:
            data = data[data.isin(skus)]
    if client is not None:
        data = data[data.client.isin(client)]

    if bl:
        data['discount'] = np.where(data['partition'] == 'fc', 0,
                                    data['discount'])
    logging.info('Dataset prepare')
    tasks = ((sample, target, used_columns, model_params, date_column, bl) for _, sample in
             data.groupby('sku', observed=True))

    result = Parallel(n_jobs=-1, backend='spark')(
        delayed(predict_one_sku)(*task) for task in
        tqdm(tasks, total=data.sku.nunique()))
    result = pd.concat(result, ignore_index=True)
    logging.info('Forecast finished')
    if out_path:
        result.to_parquet(out_path, index=False)
    return result


def predict_one_sku(data: pd.DataFrame,
                    target: str,
                    used_columns: dict,
                    model_params: dict,
                    date_column: str,
                    bl: bool = False):
    """
        Сalls a function to calculate the forecast by keys and collects the
    output dataset with the forecast for one sku

    :param data: input dataset for one sku
    :param target: name of the target column
    :param used_columns: a dict that contains a list of features, features to scale and
     features of one hot encode
    :param date_column: name of the date column
    :return: dataset with input information and added columns with features
     and forecast for one sku
    """
    try:
        sku_sample = list()
        for _, sample in data.groupby(config.SI_KEY_COLUMNS, observed=True):
            if len(sample[sample.partition == 'train']) == 0:
                continue
            sample = predict_one_key(sample,
                                     target,
                                     used_columns,
                                     model_params,
                                     date_column,
                                     bl)
            sku_sample.append(sample)
        if len(sku_sample):
            sku_sample = pd.concat(sku_sample, ignore_index=True)
            return sku_sample
        else:
            msg = 'fc.predict_one_sku : Got empty samples'
            logging.error(msg)

    except BaseException:
        logging.fatal(traceback.format_exc())


def predict_one_key(sample: pd.DataFrame,
                    target: str,
                    used_columns: dict,
                    model_params: dict,
                    date_column: str,
                    bl: bool = False):
    """
        Adds features, trains the model and builds a forecast for a one key

    :param sample: input dataset for one key
    :param target: name of the target column
    :param x_columns: list of features for the model
    :param date_column: name of the date column
    :return: dataset with input information and added columns with features
     and forecast for one key
    """
    if bl:
        sample_bl = sample.copy()
        sample_bl['discount'] = 0
        sample_bl = features.add_features(data=sample_bl,
                                          feat_cols=used_columns['feature_columns'],
                                          date_column=date_column)

    sample = features.add_features(data=sample,
                                   feat_cols=used_columns['feature_columns'],
                                   date_column=date_column)
    # TODO: add raise exception if some features not added
    # 'raise' does not work inside 'try'
    model = get_pipeline(used_columns, model_params)
    X_train = sample[(sample['partition'] == 'train') &
                     (sample['unused_history'] == 0)][used_columns['feature_columns']]
    if len(X_train) == 0:
        sample['fc'] = 0
        sample['innovation'] = True
        return sample
    y_train = sample[(sample['partition'] == 'train') &
                     (sample['unused_history'] == 0)][target]
    X = sample[used_columns['feature_columns']] if not bl \
        else sample_bl[used_columns['feature_columns']]
    model.fit(X_train, y_train)
    sample['fc'] = model.predict(X)
    return sample


def get_model_param_from_name(model_name):
    path = f'{config.path}/settings/{model_name}.json'
    with open(path) as fd:
        params = json.load(fd)
    return params


def get_pipeline(used_columns: dict, model_params: dict):
    """
        Collects a pipeline for feature processing and model training

    :param min_max_cols: list of numeric features that need to be scaled
    :param ohe_columns: list of categorical features that need to be one
     hot encoding
    :return: pipeline
    """
    model = XGBRegressor(**model_params)
    if model is None:
        model = XGBRegressor(max_depth=5, nthread=1)
    numeric_transformer = MinMaxScaler(feature_range=(0, 1))
    cat_transformer = OneHotEncoder(handle_unknown='ignore')

    preprocessor = ColumnTransformer(
        sparse_threshold=0,
        transformers=[
            ('num', numeric_transformer, used_columns['min_max_cols']),
            ('cat', cat_transformer, used_columns['ohe_columns']),
        ],
        remainder='passthrough'
    )
    model = Pipeline(steps=[
        ('preprocessor', preprocessor),
        ('model', model),
    ])
    return model


def main(horizon: str,
         magnit: bool = False,
         used_columns: dict = None,
         d0: bool = False, # Arg for bl & uplift split
         ) -> None:    
    # TODO: discuss replacing used_columns into a config file
    # TODO: replace used_columns and model_params from this function
    date_column = config.DATE_COLUMN_MAPPING[horizon]
    input_path = config.PREPROCESSED_PATH
    output_path = config.TOTAL_FC_PATH
    if used_columns is None:
        used_columns = {'feature_columns': ['year', 'month', 'week', 'dom', 'dow',
                                            'discount', 'promo_day', 'promo_length',
                                            'promo_day_relative', 'prepromo_day', 'postpromo_day',
                                            'prepromo_profile', 'max_value_day'],
                        'min_max_cols': [],
                        'ohe_columns': []}

    model_params = get_model_param_from_name('magnit' if magnit else 'default')
    client = None if not magnit else [config.CODE_DC_MAGNIT]
    if horizon != 'lt':
        logging.info('Start predict_all for fc')
        fc = predict_all(data=input_path,
                         used_columns=used_columns,
                         date_column=date_column,
                         model_params=model_params,
                         horizon=horizon,
                         cut_history_months=10,
                         out_path=config.FC_OUT_PATH,
                         target='ordered',
                         bl=False,
                         client=client,
                         d0=d0)
        fc = fc[['techbillto', 'client', 'bu', 'dc', 'sku', 'ordered', 'discount',
                 'partition', date_column, 'fc', 'innovation', 'unused_history']]
        fc = fc.drop_duplicates()  # TODO: check the cause of duplicates
    logging.info('Start predict_all for bl')
    bl = predict_all(data=input_path,
                     used_columns=used_columns,
                     date_column=date_column,
                     model_params=model_params,
                     horizon=horizon,
                     cut_history_months=10,
                     out_path=config.BL_OUT_PATH,
                     target='ordered',
                     bl=True,
                     client=client,
                     d0=d0)
    bl.rename(columns={'fc': 'bl'}, inplace=True)
    bl = bl[['techbillto', 'client', 'bu', 'dc', 'sku', 'ordered', 'discount',
             'partition', date_column, 'bl', 'innovation', 'unused_history']]
    bl = bl.drop_duplicates()  # TODO: check the cause of duplicates

    bl['bl'] = np.where(bl['bl'] < 0, 0, bl['bl'])
    if horizon != 'lt':
        bl.drop(columns=['ordered', 'discount', 'partition'], inplace=True)
        total = fc.merge(bl, how='left',
                         on=[date_column, 'innovation', 'unused_history'] + config.SI_KEY_COLUMNS)
        total['uplift'] = total['fc'] - total['bl']
        total['uplift'] = np.where(total['discount'] > 0, total['uplift'], 0)
        total['uplift'] = np.where(total['uplift'] < 0, 0, total['uplift'])
    else:
        total = bl


    #for bl & uplift split
    if d0:
        total_d0 = total[total['partition'] == 'fc']
        total_d0['fc'] = np.where(total_d0['fc'] < 0, 0, total_d0['fc'])
        total_d0["coef"] = total_d0["bl"] / total_d0["fc"]
        total_d0['coef'] = np.where(total_d0['coef'] > 1, 1, total_d0['coef'])
        total_d0["bl"] = total_d0["ordered"] * total_d0["coef"]
        total_d0.loc[total_d0['discount'] == 0, "bl"] = total_d0.loc[total_d0['discount'] == 0, "ordered"] # TODO
        total_d0['bl'] = np.where(total_d0['bl'] < 0, 0, total_d0['bl']) # TODO
        total_d0["uplift"] = total_d0["ordered"] - total_d0["bl"]
        
        export_fc_to_snowflake_d0(df=total_d0)

    if not d0:
        total.to_parquet(output_path, index=False)
