import math

import pandas as pd
import numpy as np

import adf.modelling.dataprep.rus.config as config
import adf.modelling.dataprep.rus.promo as promo
from adf.modelling.dataprep.rus.tools import get_snowflake_df


def add_features(data: pd.DataFrame = None,
                 feat_cols: list = None,
                 date_column='mad_date'):
    '''
        Aggregates the call of various functions to add features at the key
    level

    :param data: input dataset
    :param feat_cols: list of columns with features
    :param date_column: name of the date column
    :return: output dataset with additional feature columns
    '''

    data = add_calendar(data, feat_cols, date_column)
    if 'spec_days' in feat_cols:
        data = add_special_days(data, date_column)
    if 'promo_day' in feat_cols:
        data = promo.add_promo_day(data, date_column)
    if 'promo_length' in feat_cols:
        data = promo.add_promo_length(data, date_column)
    if any(i in feat_cols for i in ('profile', 'max_value_day')):
        data = add_profile(data)
    if any(i in feat_cols for i in ('shelf_life', 'uf', 'lsl')):
        data = add_uf_lsl(data)
    if 'postpromo_day' in feat_cols:
        data = add_postpromo_day(data, lag=7, date_column=date_column)
    if 'prepromo_day' in feat_cols:
        data = add_prepromo_day(data, lag=3, date_column=date_column)
    if 'postpromo_profile' in feat_cols:
        data = add_postpromo_profile(data,
                                     promo_day_column='postpromo_day',
                                     profile_column='postpromo_profile')
    if 'prepromo_profile' in feat_cols:
        data = add_postpromo_profile(data,
                                     promo_day_column='prepromo_day',
                                     profile_column='prepromo_profile')
    if 'promo_day_relative' in feat_cols:
        data['promo_day_relative'] = \
            np.where(data['promo_length'] > 0,
                     data['promo_day'] / data['promo_length'],
                     0.)
    # const_cols = get_constant_columns(data)
    # data.drop(columns=const_cols, inplace=True)

    return data


def sin_cos_transform(df):
    '''
        Sin-cos transformation for date column

    :param df: input dataset
    :return: output dataset with transform columns
    '''
    max_value = max(df)
    sin_values = np.sin((2 * math.pi * df) / max_value)
    cos_values = np.cos((2 * math.pi * df) / max_value)
    return sin_values, cos_values


def add_calendar(data: pd.DataFrame,
                 feat_cols: list = None,
                 date_column: str = 'mad_date') -> pd.DataFrame:
    '''
        Adds calendar features to the given dataset

    :param data: input dataset
    :param feat_cols: list of columns with features
    :param date_column: name of the date column
    :return: output dataset with additional feature columns
    '''

    if feat_cols is None:
        feat_cols = ['year', 'month', 'week', 'dom', 'dow', 'hy', 'q', 's']

    if any(i in feat_cols for i in ('month_sin', 'month_cos')):
        data['month_sin'], data['month_cos'] = \
            sin_cos_transform(
                data[date_column].dt.month.astype('uint8'))
    if any(i in feat_cols for i in ('week_sin', 'week_cos')):
        data['week_sin'], data['week_cos'] = \
            sin_cos_transform(
                data[date_column].dt.isocalendar().week.astype('uint8'))
    if any(i in feat_cols for i in ('day_of_week_sin', 'day_of_week_cos')):
        data['day_of_week_sin'], data['day_of_week_cos'] = \
            sin_cos_transform(
                data[date_column].dt.dayofweek.astype('uint8'))

    if 'month_1' in feat_cols:
        data = pd.concat([
            data,
            pd.get_dummies(
                data[date_column].dt.month.astype('uint8').astype('category'),
                prefix='month')],
            axis=1)
    if 'week_1' in feat_cols:
        data = pd.concat([
            data,
            pd.get_dummies(
                data[date_column].dt.isocalendar().week.astype('uint8').astype(
                    'category'),
                prefix='week')],
            axis=1)

    if 'dow_1' in feat_cols:
        data = pd.concat([data, pd.get_dummies(
            data[date_column].dt.dayofweek.astype('uint8').astype('category'),
            prefix='dow')], axis=1)
    if 'month' in feat_cols:
        data['month'] = data[date_column].dt.month.astype('uint8')
    if 'week' in feat_cols:
        data['week'] = data[date_column].dt.isocalendar().week.astype('uint8')
    if 'dow' in feat_cols:
        data['dow'] = data[date_column].dt.weekday.astype('uint8')
    if 'year' in feat_cols:
        data['year'] = data[date_column].dt.year.astype('uint16')
    if 'dom' in feat_cols:
        data['dom'] = data[date_column].dt.day.astype('uint8')
    if 'hy' in feat_cols:
        data['hy'] = (data[date_column].dt.month.astype('uint8') < 7).astype('uint8')
    if 'q' in feat_cols:
        data['q'] = ((data[date_column].dt.month.astype('uint8') - 1) // 3)
    if 's' in feat_cols:
        data['s'] = 1 + data[date_column].dt.month.astype('uint8').replace(12, 0) // 3

    return data


def add_special_days(df,
                     date_column: str = 'mad_date'):
    '''
        Adds specific days, holidays
    List : ['12-31']

    :param df: input dataset
    :param date_column: name of the date column
    :return: output dataset with added column 'spec_days'
    '''
    other = ['12-31']
    spec_days = []
    start_date = df[date_column].min().year
    end_date = df[date_column].max().year
    for i in range(start_date, end_date + 1):
        for j in other:
            day = str(i) + '-' + j
            spec_days.append(day)
    df['spec_days'] = np.where(df.mad_date.isin(spec_days), 1, 0)
    return df


def add_profile(ex):
    '''
        The function adds the profile (historical behaviour during promo
    periods) using weighted average (latter promos with greater weights).
    Added columns: 'profile', 'max_value_day'

    :param ex: pd.DataFrame (required columns: key columns, 'ordered',
     'promo_day', 'partition')
    :return: pd.DataFrame with added columns (
    '''
    pp = ex[(ex['promo_day'] != 0) & (ex['partition'] == 'train')]
    if pp.empty:
        ex['profile'] = 0
        ex['max_value_day'] = 0
    else:
        pp = pp[config.SI_KEY_COLUMNS + ['promo_day', 'ordered']] \
            .groupby(config.SI_KEY_COLUMNS + ['promo_day'],
                     as_index=False,
                     observed=True) \
            .agg(lambda x: np.average(x,
                                      weights=[i for i in
                                               np.linspace(1, 10, len(x))]))
        pp.rename(columns={'ordered': 'profile'}, inplace=True)
        ex = pd.merge(ex, pp, how='left',
                      on=config.SI_KEY_COLUMNS + ['promo_day'])
        ex['profile'] = ex['profile'].fillna(0)  # does not work with inplace
        pp['max_value'] = pp.groupby(config.SI_KEY_COLUMNS, observed=True)['profile'] \
            .transform(max)
        pp['max_value_day'] = np.where(pp['profile'] == pp['max_value'], 1, 0)
        pp = pp[pp['max_value_day'] == 1].drop(columns=['profile', 'max_value'])
        ex = ex.merge(pp, on=config.SI_KEY_COLUMNS + ['promo_day'], how='left')
        ex['max_value_day'] = ex['max_value_day'].fillna(0)
    return ex


def add_postpromo_profile(ex, promo_day_column, profile_column):
    '''
        The function adds the profile (historical behaviour during promo
    periods) using weighted average (latter promos with greater weights).
    Added columns: profile_column

    :param ex: pd.DataFrame (required columns: key columns, 'ordered',
     promo_day_column, 'partition')
    :return: pd.DataFrame with added columns (
    '''
    # this function complement the previous one that does almost the same,
    # so they can be united with adding additional parameters
    # if to separate adding max_value_day into another function

    pp = ex[(ex[promo_day_column] != 0) & (ex['partition'] == 'train')]
    if pp.empty:
        ex[profile_column] = 0
    else:
        pp = pp[config.SI_KEY_COLUMNS + [promo_day_column, 'ordered']] \
            .groupby(config.SI_KEY_COLUMNS + [promo_day_column],
                     as_index=False,
                     observed=True) \
            .agg(lambda x: np.average(x,
                                      weights=[i for i in
                                               np.linspace(1, 10, len(x))]))

        pp.rename(columns={'ordered': profile_column}, inplace=True)
        ex = pd.merge(ex, pp, how='left',
                      on=config.SI_KEY_COLUMNS + [promo_day_column])
        ex[profile_column] = ex[profile_column].fillna(0)  # does not work with inplace
    return ex


def add_uf_lsl(df,
               prod_md_path: str = 'data/masterdata/Product_MD.csv'):
    '''
        Adds shelf life and boolean flag for ultra fresh (UF) skus, boolean
    flag for long shelf life (LSL) skus
    Following columns are added in returned DataFrame: ['shelf_life',
    'uf', 'lsl']

    :param df: input pd.DataFrame
    :param prod_md_path: the path to the file with the product hierarchy
    :return: pd.DataFrame with appended columns: ['shelf_life', 'uf', 'lsl']
    '''
    md_prod_uf = get_snowflake_df(config.SNOWFLAKE_DATABASE, 'CIS_DSP_IBP', 'F_EXT_PDT_MD',
                                  selection="MAT_COD, UF_FLAG, LSL_FLAG")

    md_prod_uf.rename(columns={'mat_cod': 'sku',
                               'uf_flag': 'uf',
                               'lsl_flag': 'lsl'}, inplace=True)
    df = df.merge(md_prod_uf, how='left', on=['sku'])
    return df


def add_postpromo_day(data: pd.DataFrame,
                      lag: int,
                      date_column: str) -> pd.DataFrame:
    """
    Adds a column that indicates the number of the day after the promo slot
    Dataset must have a 'promo_day' column
    'lag' parameter defines how far to mark the days
    """
    postpromo_feature = list()
    for _, sample in data.groupby(config.SI_KEY_COLUMNS, observed=True):
        sample.sort_values(date_column, ascending=True, inplace=True)
        # TODO: Try to use .loc[row_indexer,col_indexer] = value instead
        sample['postpromo_day'] = sample['promo_day'].shift(lag)
        sample['promo_length_lagged'] = sample['promo_length'].shift(lag)
        sample['postpromo_day'] = np.where(sample['discount'] == 0,
                                           sample['postpromo_day'] -
                                           sample['promo_length_lagged'] + lag, 0)
        sample['postpromo_day'] = np.where(sample['promo_length_lagged'] == 0,
                                           0, sample['postpromo_day'])
        postpromo_feature.append(sample[[date_column] + config.SI_KEY_COLUMNS + ['postpromo_day']])
    postpromo_feature = pd.concat(postpromo_feature, ignore_index=True)
    data = data.merge(postpromo_feature, on=[date_column] + config.SI_KEY_COLUMNS, how='left')
    data['postpromo_day'].fillna(0, inplace=True)
    data['postpromo_day'] = data['postpromo_day'].astype(int)
    return data


def add_prepromo_day(data: pd.DataFrame,
                     lag: int,
                     date_column: str) -> pd.DataFrame:
    """
    Adds a column that indicates the number of the day after the promo slot
    Dataset must have a 'promo_day' column
    'lag' parameter defines how far to mark the days
    """
    prepromo_feature = list()
    for _, sample in data.groupby(config.SI_KEY_COLUMNS, observed=True):
        sample.sort_values(date_column, ascending=True, inplace=True)
        # TODO: Try to use .loc[row_indexer,col_indexer] = value instead
        sample['prepromo_day'] = sample['promo_day'].shift(-lag)
        sample['prepromo_day'] = np.where(sample['discount'] == 0, sample['prepromo_day'], 0)
        prepromo_feature.append(sample[[date_column] + config.SI_KEY_COLUMNS + ['prepromo_day']])
    prepromo_feature = pd.concat(prepromo_feature, ignore_index=True)
    data = data.merge(prepromo_feature, on=[date_column] + config.SI_KEY_COLUMNS, how='left')
    data['prepromo_day'].fillna(0, inplace=True)
    data['prepromo_day'] = data['prepromo_day'].astype(int)
    return data


def get_constant_columns(df):
    '''
        Gets constant columns

    :param df: input dataset
    :return: list of constant columns
    '''
    constants_columns = []
    for col in df.select_dtypes('number').columns:
        if min(df[col]) == max(df[col]):
            constants_columns.append(col)
    return constants_columns
