import click
import json  # type: ignore
from datetime import datetime
import pandas as pd

from adf.utils.imports import import_method

today = datetime.now().today()
suffix = 'default'
main_path = '/dbfs/tmp/user_advncd'
first_fc_date = pd.to_datetime(today, format='%Y-%m-%d')


@click.group("modelling")
def commands() -> None:
    """ Preprocess and forecast data
    """
    return


@commands.command("preprocessing")
@click.option(
    "--date_column", "-date",
    required=True,
    help="Type of date (MAD or PGI)")
@click.option(
    "--horizon", "-h",
    required=True,
    help="short (st), mid (mt) or long (lt) term horizon")
@click.option(
    "--params_path", "-params",
    required=True,
    help="path to json configuration file")
def preprocessing(
        params_path: str
) -> None:
    """ Get and preprocess data
    """
    print(params_path)

    with open(params_path) as json_file:
        params = json.load(json_file)
    try:
        filtered = params['filter']
    except BaseException:
        filtered = None
    global suffix
    suffix_name = params['suffix']
    horizon = params['horizon']
    # segmentation = ['segmentation']
    # magnit =['magnit']

    dataprep(filtered,
             suffix_name,
             horizon)


@commands.command("forecast")
@click.option(
    "--params_path", "-params",
    required=True,
    help="path to json configuration file")
@click.option(
    '-magnit',
    required=False,
    is_flag=True,
    default=False
)
@click.option(
    '-d0',
    required=False,
    is_flag=True,
    default=False
)
def predict(
        params_path: str,
        magnit: bool = False,
) -> None:
    """ Training and forecasting
    """
    print(params_path)

    with open(params_path) as json_file:
        params = json.load(json_file)
    suffix_name = params['suffix']
    horizon = params['horizon']
    export_to_sf = params['export_to_sf']
    # segmentation = ['segmentation']
    # magnit =['magnit']

    forecast(horizon,
             export_to_sf,
             suffix_name)


def dataprep(horizon,
             filtered,
             main_path_name,
             suffix_name,
             fc_start: str = None):
    """
        Run main function for preprocessing - build_panel_data
    """
    global suffix
    suffix = suffix_name
    global main_path
    main_path = main_path_name
    if fc_start:
        global first_fc_date
        first_fc_date = pd.to_datetime(fc_start, format='%Y-%m-%d')
    import_method("adf.modelling.dataprep.rus.panel", "build_panel_data")(
        horizon,
        filtered
    )


def forecast(horizon,
             main_path_name,
             suffix_name,
             magnit,
             fc_start: str = None,
             d0: bool = False):
    """
        Run forecast main function
    """
    global suffix
    suffix = suffix_name
    global main_path
    main_path = main_path_name
    if fc_start:
        global first_fc_date
        first_fc_date = pd.to_datetime(fc_start, format='%Y-%m-%d')
    import_method("adf.ru.fc", "main")(
        horizon,
        magnit,
        None,
        d0,
    )


def postprocessing(horizon,
                   export_to_sf,
                   main_path_name,
                   suffix_name,
                   magnit):
    """
        Run forecast main function
    """
    global suffix
    suffix = suffix_name
    global main_path
    main_path = main_path_name
    import_method("adf.modelling.postprocessing.rus.postprocessing", "main")(
        horizon,
        export_to_sf,
        magnit,
    )


def full_run_st(filtered,
                main_path_name,
                suffix_name,
                export_to_sf,
                fc_start: str = None):
    dataprep(horizon='st',
             filtered=filtered,
             main_path_name=main_path_name,
             suffix_name=suffix_name,
             fc_start=fc_start)

    # Run prod Magnit for priority table
    forecast(horizon='st',
             main_path_name=main_path_name,
             suffix_name=suffix_name,
             magnit=True,
             fc_start=fc_start)

    postprocessing(horizon='st',
                   export_to_sf='prod',
                   main_path_name=main_path_name,
                   suffix_name=suffix_name,
                   magnit=True)

    # Run full ADF
    forecast(horizon='st',
             main_path_name=main_path_name,
             suffix_name=suffix_name,
             magnit=False,
             fc_start=fc_start)

    postprocessing(horizon='st',
                   export_to_sf=export_to_sf,
                   main_path_name=main_path_name,
                   suffix_name=suffix_name,
                   magnit=False)
