import json
from xgboost import XGBRegressor
from adf.modelling.dataprep.rus import config


def get_model_param_from_name(model_name):
    path = config.MODEL_PARAMS_FOLDER_PATH + f'/{model_name}.json'

    with open(path) as fd:
        params = json.load(fd)

    return params


def list_models():
    '''
        Initiates the model pool.
    :return: list of named tuples: model names and models itself.
    '''
    pool = []

    # total sku v2 - for mvp
    model = XGBRegressor
    params = get_model_param_from_name('xgb_mvp')

    pool.append((model, params))

    return pool


if __name__ == '__main__':
    print(
        get_model_param_from_name('xgb_mvp')
    )
    print(
        list_models()
    )
