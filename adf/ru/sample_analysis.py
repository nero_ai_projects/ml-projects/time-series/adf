"""
    Module contains functions to make sample analysis
"""

import pandas as pd


def get_yoy_regular_growth(sample: pd.DataFrame, min_counts: int = 600):
    """

    Parameters
    ----------
    sample : pandas dataframe with date for one key
    min_counts: history length that needed to calculate growth

    Returns
    -------

    """

    regular_history = sample[(sample.partition == 'train') & (sample.discount == 0)].copy()

    if len(regular_history) < min_counts:
        return 0

    regular_history['chunk_id'] = regular_history.pgi_date.dt.year

    gr = regular_history.groupby('chunk_id').agg({'ordered': 'mean'})
    if gr.ordered.min() == 0:
        return 0
    # TODO: imbalanced counts:
    #  how to manage cases if we are in start of the year, and we have only 1.5 year of history
    gr['py_ordered'] = gr.ordered.shift(1)
    gr.dropna(inplace=True)
    gr['growth'] = (gr.ordered - gr.py_ordered)/gr.py_ordered

    growth = 0 if gr.empty else gr.growth.mean()

    return round(growth, 2)


def is_regular_growing_sample(sample: pd.DataFrame, growth_threshold: float = 0.10):
    """

    Parameters
    ----------
    sample : pandas dataframe with date for one key
    growth_threshold: limit that will be used to evaluate growth of sample

    Returns
    -------

    """

    growth = get_yoy_regular_growth(sample)
    is_growing = abs(growth) >= growth_threshold
    return is_growing
