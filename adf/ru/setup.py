import os
import logging
import datetime as dt
from adf.modelling.dataprep.rus import config


def set_up_logging(name=''):
    '''
    Functions set up logging module:
        - define log file name as log_[current date and time].log
        - set log format
        - add console to log handlers

    :param name: logger name
    :return:
    '''
    if logging.getLogger(name).handlers:
        logging.info('logging setting up was ignored: logger has handlers')

    log_format = '%(asctime)s - %(levelname)s - %(name)s - %(funcName)s - ' \
                 '%(message)s (%(filename)s:%(lineno)s)'
    log_name = f'log_{dt.datetime.today().strftime("%Y-%m-%d_%H_%M_%S")}.log'
    log_name = os.path.join(config.LOG_FOLDER, log_name)
    logging.basicConfig(filename=log_name,
                        level=logging.INFO,
                        format=log_format,
                        datefmt='%Y-%m-%d %H:%M:%S')
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    console.setFormatter(logging.Formatter(log_format))
    logging.getLogger(name).addHandler(console)


if __name__ == '__main__':

    set_up_logging()
