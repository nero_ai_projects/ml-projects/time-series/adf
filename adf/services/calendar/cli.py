import logging
import warnings
import click
from adf.utils.imports import import_method

log = logging.getLogger("adf")
warnings.filterwarnings('ignore')


@click.group("calendar")
def commands():
    """ Fetch calendar data
    """
    return


@commands.command("holidays")
@click.option("--country", "-c", required=True, help='The country for which you '
                                                     'want to get holiday data')
@click.option("--start_date", "-s", required=True, help='The start date of the period')
@click.option("--end_date", "-e", required=True, help='The end date of the period')
@click.option("--province", "-p", help='The province for which you want to get holiday'
                                       ' data, default None', default=None)
@click.option("--state", "-st", help='The state for which you want to get holiday '
                                     'data, default None', default=None)
@click.option("--add_zones", required=False, default=False)
def get_holidays(
    country: str, start_date: str, end_date: str,
    province: str, state: str, add_zones: bool
) -> None:
    """ Fetch calendar data including holidays for a given period
    """
    import_method("adf.services.calendar.main", "get_holidays")(
        country, start_date, end_date, province, state, add_zones
    )
