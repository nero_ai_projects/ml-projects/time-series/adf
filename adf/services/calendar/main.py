import os
from os.path import dirname, abspath
import logging
from typing import Optional, Any
import inspect
import pandas as pd  # type: ignore
import datetime
import json
import holidays as holidays_lib  # type: ignore
from snowflake.connector.errors import ProgrammingError  # type: ignore
import numpy as np  # type: ignore
from adf.services.calendar.school_holiday_countries import COUNTRIES
from vacances_scolaires_france import SchoolHolidayDates  # type: ignore
from adf import config
from adf.utils.credentials import get_snowflake_connector
from adf.utils.dataframe import create_table, get_col_types


log = logging.getLogger("adf")


def get_holidays(
    country: str, start_date: str, end_date: str,
    province: str = None, state: str = None, add_zones: bool = False
) -> None:
    try:
        country_classes = inspect.getmembers(holidays_lib.countries, inspect.isclass)
        country_class = next(obj for name, obj in country_classes if name == country.capitalize())
        country_provinces = country_class.__dict__.get("PROVINCES", [])
        country_states = country_class.__dict__.get("STATES", [])
        if add_zones and (country_provinces or country_states):
            for country_province in country_provinces:
                run_holidays_etl(
                    country, start_date, end_date, country_province, None
                )
            for country_state in country_states:
                run_holidays_etl(
                    country, start_date, end_date, None, country_state
                )
        else:
            run_holidays_etl(
                country, start_date, end_date, province, state
            )
    except StopIteration:
        raise KeyError("Country %s not available" % country)


def run_holidays_etl(
    country: str, start_date: str, end_date: str,
    province: str = None, state: str = None
) -> None:
    prov_str = '' if province is None else f'_{province}'
    state_str = '' if state is None else f'_{state}'
    location_str = f'{country}{state_str}{prov_str}'
    log.info("Fetching holidays for %s between %s and %s", location_str, start_date, end_date)
    try:
        holidays = retrieve_holidays(country, start_date, end_date, province=province, state=state)
        holidays["date"] = pd.to_datetime(holidays["date"], utc=True).dt.ceil(freq='ms')
        holiday_type = "holidays&dayoff" if country.lower() in COUNTRIES.keys() else "dayoff"
        holidays["holiday_type"] = holiday_type
        holidays["location"] = location_str
        holidays["start_date"] = start_date
        holidays["end_date"] = end_date
        holidays["start_date"] = pd.to_datetime(
            holidays["start_date"], utc=True
        ).dt.ceil(freq='ms')
        holidays["end_date"] = pd.to_datetime(
            holidays["end_date"], utc=True
        ).dt.ceil(freq='ms')
        conn = get_snowflake_connector()
        curr = conn.cursor()
        try:
            curr.execute(
                f'DELETE '
                f'FROM "{config.SNOWFLAKE_DATABASE}"."{config.SNOWFLAKE_SCHEMA}"."CALENDAR_DATA" '
                f"WHERE DATE >= '{start_date}' AND DATE <= '{end_date}' "
                f"AND LOCATION = '{location_str}' AND HOLIDAY_TYPE = '{holiday_type}'"
            )
        except ProgrammingError:
            curr.close()

        col_type = get_col_types(holidays)
        create_table('CALENDAR_DATA', 'create_or_append', col_type, holidays)
        log.info("Finished fetching holidays data")
    except Exception as err:
        log.error(err)
        raise err


def retrieve_holidays(country: str, start_date: str, end_date: str, state: str = None,
                      province: str = None) -> pd.DataFrame:
    """ Given a country, a start date and an end date, retrieves holidays (bank and school)
    """
    holidays = retrieve_bank_holidays(
        country, start_date, end_date, state=state, province=province
    )
    area = state if state else province
    if country.lower() in COUNTRIES.keys():
        # In this case we also add school holidays features
        mapping: Any = COUNTRIES["france"]["mapping"]
        if area and area in mapping:
            school_holidays_dfs = []
            for zone in mapping[area]:
                school_holidays_dfs.append(
                    retrieve_school_holidays(country, start_date, end_date, zone)
                )
            school_holidays = pd.concat(school_holidays_dfs)
        else:
            school_holidays = retrieve_school_holidays(country, start_date, end_date)
        results = []
        for zone, group in school_holidays.groupby("zones"):
            group = add_school_holidays_columns(group)
            group = group.merge(holidays, on=["date"], how="left")
            results.append(group)

        holidays = pd.concat(results, axis=0)

        zone_path = str(COUNTRIES[country.lower()]["zones"])
        zones_file_path = os.path.join(
            dirname(dirname(dirname(dirname(abspath(__file__))))), zone_path
        )
        with open(zones_file_path) as json_file:
            holiday_zones = json.load(json_file)
        holidays['departements'] = holidays['zones'].apply(
            lambda x: holiday_zones[x] if x in holiday_zones else np.nan)
    return holidays


def retrieve_bank_holidays(country: str, start_date: str, end_date: str, state: str = None,
                           province: str = None) -> pd.DataFrame:
    log.info('Retrieving %s Bank Holidays between %s and %s', country, start_date, end_date)
    date_range = pd.date_range(start_date, end_date)
    date_start = datetime.datetime.strptime(start_date, '%Y-%m-%d').date()
    date_end = datetime.datetime.strptime(end_date, '%Y-%m-%d').date()
    region_holidays = holidays_lib.CountryHoliday(
        country.capitalize(),
        prov=province, state=state,
        years=range(date_start.year, date_end.year + 1)
    )
    region_holidays = {k: v for k, v in region_holidays.items() if date_start <= k <= date_end}
    holidays_df = pd.DataFrame(index=date_range)
    holidays_df['is_holiday'] = holidays_df.index.map(
        lambda x: 1 if x in list(region_holidays.keys()) else 0
    )
    holidays_df['holiday_name'] = holidays_df.index.map(
        lambda x: region_holidays[x.date()] if x in list(region_holidays.keys()) else None
    )
    holidays_df['days_before_next_holiday'] = holidays_df. \
        apply(lambda x: get_days_before_next_bank_holiday(x, holidays_df),
              axis=1)
    return holidays_df.reset_index().rename(columns={'index': 'date'})


def retrieve_school_holidays(
    country: str, start_date: str, end_date: str, zone: Optional[str] = None
) -> pd.DataFrame:
    log.info('Retrieving %s School Holidays between %s and %s', country, start_date, end_date)
    return retrieve_french_school_holidays(start_date, end_date, zone)


def retrieve_french_school_holidays(
    start_date: str, end_date: str, zone: Optional[str] = None
) -> pd.DataFrame:
    d = SchoolHolidayDates()
    start_year = start_date.split("-")[0]
    end_year = end_date.split("-")[0]

    results_list = []
    for year in range(int(start_year), int(end_year) + 1):

        date = datetime.date(year, 1, 1)
        finish_date = datetime.date(year + 1, 1, 1)
        year_data = (
            d.holidays_for_year_and_zone(year, zone=zone)
            if zone else d.holidays_for_year(year)
        )
        year_results = []

        while date < finish_date:

            if date in year_data.keys():
                ordered_dict = year_data[date]
                for zone in ["A", "B", "C"]:
                    data = [
                        [date, zone, int(ordered_dict[f"vacances_zone_{zone.lower()}"] is True)]
                    ]
                    day_df = pd.DataFrame(data, columns=["date", "zones", "is_school_holiday"])
                    year_results.append(day_df)
            else:
                for zone in ["A", "B", "C"]:
                    data = [[date, zone, 0]]
                    day_df = pd.DataFrame(data, columns=["date", "zones", "is_school_holiday"])
                    year_results.append(day_df)
            date += datetime.timedelta(days=1)

        year_results = pd.concat(year_results, axis=0)
        results_list.append(year_results)

    results = pd.concat(results_list, axis=0)

    results = results.sort_values(by=["date"])
    return results


def add_school_holidays_columns(holiday_df: pd.DataFrame) -> pd.DataFrame:
    results = []
    holiday_df["date"] = pd.to_datetime(holiday_df["date"], format="%Y-%m-%d")

    for _, row in holiday_df.iterrows():
        row = row.to_frame().transpose()

        row_date = row["date"][0].strftime("%Y-%m-%d")

        if row["is_school_holiday"].values == 1:
            row["days_before_next_school_holiday"] = 0
            start_holiday = holiday_df.loc[
                (holiday_df["date"] < row_date) & (holiday_df["is_school_holiday"] == 0.0)
            ].tail(1)

            if len(start_holiday) == 0:
                row["daynum_school_holiday"] = np.nan
            else:
                row["daynum_school_holiday"] = (row["date"] - start_holiday["date"]).dt.days

        else:
            next_school_holidays = holiday_df.loc[
                (holiday_df["date"] > row_date) & (holiday_df["is_school_holiday"] == 1.0)
            ].head(1)

            if len(next_school_holidays) == 0:
                row["days_before_next_school_holiday"] = np.nan
            else:
                row["days_before_next_school_holiday"] = \
                    (next_school_holidays["date"] - row["date"]).dt.days
            row["daynum_school_holiday"] = 0

        results.append(row)
    return pd.concat(results, axis=0)


def get_days_before_next_bank_holiday(row: pd.Series, holidays: pd.DataFrame) -> float:
    """ Given a date, calculates the number of days before next bank holiday
    """
    row_date = row.name
    holidays_filtered = holidays[row_date:].query('is_holiday == 1')
    return (holidays_filtered.iloc[0].name - row_date).days if len(
        holidays_filtered) != 0 else np.nan


def prepare_french_holidays_national(
        df: pd.DataFrame
) -> pd.DataFrame:
    """
    Transforms holidays dataframe features to make them relevant for a national granularity

    Parameters
    ----------
    df: pandas.DataFrame
        input holidays dataframe

    Returns
    -------
    pandas.DataFrame
        updated holidays dataframe
    """

    df = df.drop(["departements"], axis=1)
    results = []

    national_columns = ["date",
                        "is_holiday",
                        "days_before_next_holiday"]
    zone_columns = ["date",
                    "is_school_holiday",
                    "daynum_school_holiday",
                    "days_before_next_school_holiday"]

    df_national = df[national_columns]\
        .copy() \
        .drop_duplicates()

    for zone in ["A", "B", "C"]:
        df_zone = df.loc[df["zones"] == zone].copy()
        df_zone = df_zone[zone_columns]

        for col in zone_columns:
            if col == "date":
                continue
            df_zone.rename(
                columns={
                    col: f"{zone}_{col}"
                },
                inplace=True
            )
        results.append(df_zone)

    for zone in results:
        df_national = df_national.merge(zone, on=["date"], how="left")

    df_national.rename(columns={"date": "to_dt"}, inplace=True)

    return df_national
