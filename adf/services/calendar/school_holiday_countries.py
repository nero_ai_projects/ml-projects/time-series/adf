
COUNTRIES = {
    'france': {
        'zones': 'adf/services/calendar/france/holiday_zones.json',
        'mapping': {
            "Métropole": ["A", "B", "C"],
            "Alsace-Moselle": ["B"]
        }
    }
}
