from typing import Any
from sqlalchemy.ext.declarative import declarative_base  # type: ignore


Base: Any = declarative_base()
