import uuid
from typing import Optional, Any
import pandas as pd  # type: ignore
from sqlalchemy.orm import relationship  # type: ignore
from sqlalchemy.sql import func  # type: ignore
from sqlalchemy.ext.hybrid import hybrid_property  # type: ignore
from sqlalchemy.ext.hybrid import hybrid_method  # type: ignore
from sqlalchemy import (  # type: ignore
    Column,
    DateTime,
    String,
    ForeignKey
)

from adf import config
from adf.services import storage
from adf.services.database.base import Base
from adf.services.database.custom_types import (
    GUID,
)


class BuildingBlocksReports(Base):

    __tablename__ = "building_blocks_reports"

    id: Any = Column(GUID, default=uuid.uuid4, primary_key=True)
    created_at: Any = Column(DateTime(timezone=True), server_default=func.current_timestamp())
    updated_at: Any = Column(DateTime(timezone=True), onupdate=func.current_timestamp())
    business_unit_id: Any = Column(GUID, ForeignKey("business_units.id"))
    dataprep_report_id: Optional[Any] = Column(
        GUID,
        ForeignKey("dataprep_reports.id"),
        nullable=True
    )
    pipeline_id: Optional[Any] = Column(
        GUID,
        ForeignKey("pipeline_reports.id"),
        nullable=True
    )
    status: Any = Column(String, nullable=False)
    reason: Any = Column(String)
    scope: Any = Column(String, nullable=False)

    dataprep_report: Any = relationship(
        "DataprepReports",
        back_populates="building_block_reports"
    )
    pipeline: Any = relationship("PipelineReports", back_populates="building_blocks_report")
    business_unit: Any = relationship("BusinessUnits", back_populates="building_block_reports")

    def __repr__(self) -> str:
        return "BuildingBlocksReports[{}]".format(str(self.id)[:8])

    @property
    def mount(self) -> str:
        return config.MOUNT

    @property
    def folder(self) -> str:
        return "{folder}/{scope}".format(
            folder=config.FOLDER_BUILDING_BLOCKS,
            scope=self.scope
        )

    @hybrid_property
    def blob(self) -> str:
        return "{folder}/{id}.parquet.gzip".format(
            folder=self.folder,
            id=self.id,
        )

    @hybrid_method
    def upload(self, df, **kwargs) -> None:
        return storage.upload_parquet(df, self.mount, self.blob, **kwargs)

    @hybrid_method
    def download(self, **kwargs) -> pd.DataFrame:
        return storage.read_parquet(self.mount, self.blob, **kwargs)
