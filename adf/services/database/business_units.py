import uuid
from typing import Any
from sqlalchemy.sql import func  # type: ignore
from sqlalchemy.orm import relationship  # type: ignore
from sqlalchemy import (  # type: ignore
    Column,
    DateTime,
    String,
    UniqueConstraint
)

from adf.services.database.base import Base
from adf.services.database.custom_types import (
    GUID
)


class BusinessUnits(Base):

    __tablename__ = "business_units"
    __table_args__ = (
        UniqueConstraint("country", "division", "sales_org_cod"),
    )

    id: Any = Column(GUID, default=uuid.uuid4, primary_key=True)
    created_at: Any = Column(DateTime(timezone=True), server_default=func.current_timestamp())
    updated_at: Any = Column(DateTime(timezone=True), onupdate=func.current_timestamp())
    country: Any = Column(String, nullable=False)
    division: Any = Column(String, nullable=False)
    sales_org_cod: Any = Column(String, nullable=False)
    uom: Any = Column(String, nullable=False)

    dataprep_reports: Any = relationship(
        "DataprepReports",
        back_populates="business_unit",
        cascade="all, delete, delete-orphan",
        single_parent=True
    )
    building_block_reports: Any = relationship(
        "BuildingBlocksReports",
        back_populates="business_unit",
        cascade="all, delete, delete-orphan",
        single_parent=True
    )
    ibp_reports: Any = relationship(
        "IBPReports",
        back_populates="business_unit",
        cascade="all, delete, delete-orphan",
        single_parent=True
    )
    sellout_ibp_reports: Any = relationship(
        "SelloutIBPReports",
        back_populates="business_unit",
        cascade="all, delete, delete-orphan",
        single_parent=True
    )

    def __repr__(self) -> str:
        return "BusinessUnits[{}]".format(str(self.id)[:8])
