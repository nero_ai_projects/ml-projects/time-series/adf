from sqlalchemy import (  # type: ignore
    Column,
    DateTime,
    String,
    Float,
    Integer,
    UniqueConstraint
)
from datetime import datetime
from adf.services.database.base import Base


class CalendarData(Base):

    __tablename__ = "calendar_data"
    __table_args__ = (
        UniqueConstraint("date", "location"),
    )

    date: datetime = Column(DateTime(timezone=True), nullable=False, primary_key=True)
    zones: str = Column(String, nullable=True)
    is_school_holiday: str = Column(String, nullable=True)
    days_before_next_school_holiday: float = Column(Float, nullable=True)
    daynum_school_holiday: float = Column(Float, nullable=True)
    is_holiday: int = Column(Integer, nullable=False)
    holiday_name: str = Column(String, nullable=True)
    days_before_next_holiday: float = Column(Float, nullable=True)
    departements: str = Column(String, nullable=True)
    holiday_type: str = Column(String, nullable=False)
    location: str = Column(String, nullable=False, primary_key=True)
    start_date: datetime = Column(DateTime(timezone=True), nullable=False)
    end_date: datetime = Column(DateTime(timezone=True), nullable=False)
