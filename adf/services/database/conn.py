from sqlalchemy import create_engine  # type: ignore
from sqlalchemy.orm import scoped_session, sessionmaker  # type: ignore
from adf import config
from adf import utils


engine = None
session = None


def setup(secret=None) -> None:
    global engine
    global session

    if engine is None:
        engine = create_engine(
            utils.get_database_uri(secret),
            pool_size=config.DB_POOL_SIZE
        )

    if session is None:
        session = scoped_session(sessionmaker(
            bind=engine,
            autoflush=False,
            autocommit=False,
            expire_on_commit=False
        ))


def dispose() -> None:
    global engine
    global session

    if engine:
        engine.dispose()
        engine = None

    if session:
        session.remove()
        session = None


def get_session(secret=None):
    global session
    if session is None:
        setup(secret)
    return session


def get_engine():
    global engine
    if engine is None:
        setup()
    return engine
