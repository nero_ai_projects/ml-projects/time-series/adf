import uuid
from typing import Any, Optional
from sqlalchemy.sql import func  # type: ignore
from sqlalchemy.orm import relationship  # type: ignore
from sqlalchemy.ext.hybrid import hybrid_property  # type: ignore
from sqlalchemy.ext.hybrid import hybrid_method  # type: ignore
from sqlalchemy import (  # type: ignore
    Column,
    DateTime,
    String,
    ForeignKey,
    Integer
)
from adf import config
from adf.services import storage
from adf.services.database.base import Base
from adf.services.database.custom_types import (
    GUID
)
from adf.types import DataFrame


class DataprepReports(Base):

    __tablename__ = "dataprep_reports"

    id: Any = Column(GUID, default=uuid.uuid4, primary_key=True)
    created_at: Any = Column(DateTime(timezone=True), server_default=func.current_timestamp())
    updated_at: Any = Column(DateTime(timezone=True), onupdate=func.current_timestamp())
    business_unit_id: Any = Column(GUID, ForeignKey("business_units.id"))
    pipeline_id: Optional[Any] = Column(
        GUID,
        ForeignKey("pipeline_reports.id"),
        nullable=True
    )
    status: Any = Column(String, nullable=False)
    reason: Any = Column(String)
    product_level: Any = Column(String, nullable=False)
    location_level: Any = Column(String, nullable=False)
    scope: Any = Column(String, nullable=False)
    split_type: Any = Column(String, nullable=True)
    split_code: Any = Column(Integer, nullable=True)
    prep_type = Column(String)
    __mapper_args__ = {
        'polymorphic_on': prep_type,
        'polymorphic_identity': 'core'
    }

    business_unit: Any = relationship(
        "BusinessUnits",
        back_populates="dataprep_reports"
    )
    pipeline: Any = relationship(
        "PipelineReports",
        back_populates="dataprep_report",
        cascade="all, delete, delete-orphan",
        single_parent=True
    )
    building_block_reports: Any = relationship(
        "BuildingBlocksReports",
        back_populates="dataprep_report"
    )

    def __repr__(self) -> str:
        return "DataprepReports[{}]".format(str(self.id)[:8])

    @property
    def mount(self) -> str:
        return config.MOUNT

    @property
    def folder(self) -> str:
        return config.FOLDER_DATAPREP

    @hybrid_property
    def blob(self) -> str:
        return "{folder}/{id}.parquet.gzip".format(
            folder=self.folder,
            id=self.id,
        )

    @hybrid_method
    def upload(self, df, **kwargs) -> None:
        return storage.upload_parquet(df, self.mount, self.blob, **kwargs)

    @hybrid_method
    def download(self, **kwargs) -> DataFrame:
        return storage.read_parquet(self.mount, self.blob, **kwargs)


class InnoDataprepReports(DataprepReports):
    __mapper_args__ = {'polymorphic_identity': 'innovation'}

    @property
    def mount(self) -> str:
        return config.MOUNT

    @property
    def folder(self) -> str:
        return config.FOLDER_DATAPREP_INNO

    @hybrid_property
    def blob(self) -> str:
        folder = self.folder
        return "{folder}/{id}.parquet.gzip".format(
            folder=folder,
            id=self.id
        )

    @hybrid_method
    def upload(self, df, **kwargs) -> None:
        return storage.upload_parquet(df, self.mount, self.blob, **kwargs)

    @hybrid_method
    def download(self, **kwargs) -> DataFrame:
        return storage.read_parquet(self.mount, self.blob, **kwargs)


class PromoDataprepReports(DataprepReports):
    __mapper_args__ = {'polymorphic_identity': 'promotion'}

    @property
    def mount(self) -> str:
        return config.MOUNT

    @property
    def folder(self) -> str:
        return config.FOLDER_DATAPREP_PROMO

    @hybrid_property
    def blob(self) -> str:
        folder = self.folder
        return "{folder}/{id}.parquet.gzip".format(
            folder=folder,
            id=self.id
        )

    @hybrid_method
    def upload(self, df, **kwargs) -> None:
        return storage.upload_parquet(df, self.mount, self.blob, **kwargs)

    @hybrid_method
    def download(self, **kwargs) -> DataFrame:
        return storage.read_parquet(self.mount, self.blob, **kwargs)
