from typing import Any
from sqlalchemy.orm import relationship  # type: ignore
from sqlalchemy import (  # type: ignore
    Column, String, DateTime, ForeignKey,
    Float, UniqueConstraint, PrimaryKeyConstraint
)
from adf.services.database.base import Base
from adf.services.database.custom_types import (
    GUID
)


class ForecastsInput(Base):
    __tablename__ = "forecasts_input"
    __table_args__ = (
        PrimaryKeyConstraint(
            "sales_organization", "preprocessing_report_id", "target_date",
            "product_code", "location_code", "customer_code"
        ),
        UniqueConstraint(
            "sales_organization", "preprocessing_report_id", "target_date",
            "product_code", "location_code", "customer_code"
        ),
    )
    sales_organization: Any = Column(String, nullable=False)
    snapshot_date: Any = Column(DateTime(timezone=True), nullable=False)
    target_date: Any = Column(DateTime(timezone=True), nullable=False)
    product_code: Any = Column(String(), nullable=False)
    product_level: Any = Column(String(), nullable=False)
    location_code: Any = Column(String())
    location_level: Any = Column(String())
    customer_code: Any = Column(String())
    customer_level: Any = Column(String())
    ground_truth: Any = Column(Float(), nullable=False)
    unit_of_measure: Any = Column(String(), nullable=False)
    preprocessing_report_id: Any = Column(
        GUID, ForeignKey("preprocessing_reports.id"), nullable=False
    )
    preprocessing_report: Any = relationship(
        "PreprocessingReports",
        back_populates="forecasts_input",
        cascade="all, delete, delete-orphan",
        single_parent=True
    )
    pipeline_id: Any = Column(
        GUID, ForeignKey("pipeline_reports.id"), nullable=True
    )
    pipeline: Any = relationship(
        "PipelineReports",
        back_populates="forecasts_input",
        cascade="all, delete, delete-orphan",
        single_parent=True
    )

    def __repr__(self):
        return "ForecastsInput[{}]".format(str(self.preprocessing_report_id)[:8])
