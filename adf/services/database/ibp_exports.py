from typing import Any
from sqlalchemy.orm import relationship  # type: ignore
from sqlalchemy import (  # type: ignore
    Column, String, DateTime, ForeignKey,
    Boolean, Float, UniqueConstraint, PrimaryKeyConstraint
)
from adf.services.database.base import Base
from adf.services.database.custom_types import (
    GUID
)


class IBPExports(Base):
    __tablename__ = "ibp_exports"
    __table_args__ = (
        PrimaryKeyConstraint(
            "sales_organization", "ibp_report_id", "prediction_date", "target_date",
            "product_code", "location_code", "customer_code"
        ),
        UniqueConstraint(
            "sales_organization", "ibp_report_id", "prediction_date", "target_date",
            "product_code", "location_code", "customer_code"
        ),
    )
    export_to_ibp: Any = Column(Boolean, default=False)
    sales_organization: Any = Column(String, nullable=False)
    execution_date: Any = Column(DateTime(timezone=True), nullable=False)
    prediction_date: Any = Column(DateTime(timezone=True), nullable=False)
    target_date: Any = Column(DateTime(timezone=True), nullable=False)
    product_code: Any = Column(String(), nullable=False)
    product_level: Any = Column(String(), nullable=False)
    location_code: Any = Column(String())
    location_level: Any = Column(String())
    customer_code: Any = Column(String())
    customer_level: Any = Column(String())
    prediction: Any = Column(Float(), nullable=False)
    min_prediction: Any = Column(Float())
    max_prediction: Any = Column(Float())
    baseline: Any = Column(Float(), nullable=False)
    building_block_promotion: Any = Column(Float())
    building_block_marketing: Any = Column(Float())
    building_block_weather: Any = Column(Float())
    building_block_stock: Any = Column(Float())
    unit_of_measure: Any = Column(String(), nullable=False)

    ibp_report_id: Any = Column(GUID, ForeignKey("ibp_reports.id"), nullable=False)
    ibp_report: Any = relationship(
        "IBPReports",
        back_populates="ibp_export",
        cascade="all, delete, delete-orphan",
        single_parent=True
    )
    pipeline_id: Any = Column(
        GUID, ForeignKey("pipeline_reports.id"), nullable=True
    )
    pipeline: Any = relationship(
        "PipelineReports",
        back_populates="ibp_export",
        cascade="all, delete, delete-orphan",
        single_parent=True
    )

    def __repr__(self):
        return "IBPExports[{}]".format(str(self.ibp_report_id)[:8])
