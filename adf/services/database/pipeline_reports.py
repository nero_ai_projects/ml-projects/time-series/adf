import uuid
from typing import Any
from sqlalchemy.orm import relationship  # type: ignore
from sqlalchemy import Column, String, DateTime  # type: ignore
from sqlalchemy.sql import func  # type: ignore
from adf.services.database.base import Base
from adf.services.database.custom_types import (
    GUID
)


class PipelineReports(Base):

    __tablename__ = "pipeline_reports"
    id: Any = Column(GUID, default=uuid.uuid4, primary_key=True)
    created_at: Any = Column(
        DateTime(timezone=True), server_default=func.current_timestamp()
    )
    updated_at: Any = Column(
        DateTime(timezone=True), onupdate=func.current_timestamp()
    )
    algorithm_type: Any = Column(String, nullable=False)
    run_type: Any = Column(String)
    status: Any = Column(String, nullable=False)
    reason: Any = Column(String)

    dataprep_report: Any = relationship("DataprepReports", back_populates="pipeline")
    preprocessing_report: Any = relationship("PreprocessingReports", back_populates="pipeline")
    forecasts_input: Any = relationship("ForecastsInput", back_populates="pipeline")
    training_report: Any = relationship("TrainingReports", back_populates="pipeline")
    forecast_report: Any = relationship("ForecastReports", back_populates="pipeline")
    ibp_report: Any = relationship("IBPReports", back_populates="pipeline")
    ibp_export: Any = relationship("IBPExports", back_populates="pipeline")
    sellout_ibp_report: Any = relationship("SelloutIBPReports", back_populates="pipeline")
    sellout_ibp_export: Any = relationship("SelloutIBPExports", back_populates="pipeline")
    building_blocks_report: Any = relationship("BuildingBlocksReports", back_populates="pipeline")

    def __repr__(self):
        return "PipelineReports[{}]".format(str(self.id)[:8])
