import uuid
from typing import Any
from sqlalchemy.orm import relationship  # type: ignore
from sqlalchemy import Column, String, DateTime, ForeignKey  # type: ignore
from sqlalchemy.sql import func  # type: ignore
from sqlalchemy.ext.hybrid import hybrid_property  # type: ignore
from sqlalchemy.ext.hybrid import hybrid_method  # type: ignore
import pandas as pd  # type: ignore
from adf import config
from adf.services import storage
from adf.services.database.base import Base
from adf.services.database.custom_types import (
    CustomJSON, GUID
)


class PreprocessingReports(Base):

    __tablename__ = "preprocessing_reports"
    id: Any = Column(GUID, default=uuid.uuid4, primary_key=True)
    created_at: Any = Column(
        DateTime(timezone=True), server_default=func.current_timestamp()
    )
    updated_at: Any = Column(
        DateTime(timezone=True), onupdate=func.current_timestamp()
    )
    pipeline_id: Any = Column(
        GUID, ForeignKey("pipeline_reports.id"), nullable=True
    )
    description: Any = Column(CustomJSON, nullable=False)
    status: Any = Column(String, nullable=False)
    reason: Any = Column(String)

    training_reports: Any = relationship(
        "TrainingReports",
        back_populates="preprocessing_report",
        cascade="all, delete, delete-orphan",
        single_parent=True
    )
    pipeline: Any = relationship(
        "PipelineReports",
        back_populates="preprocessing_report",
        cascade="all, delete, delete-orphan",
        single_parent=True
    )
    forecasts_input: Any = relationship("ForecastsInput", back_populates="preprocessing_report")

    def __repr__(self):
        return "PreprocessingReports[{}]".format(str(self.id)[:8])

    @property
    def mount(self) -> str:
        return config.MOUNT

    @property
    def folder(self) -> str:
        return config.FOLDER_PREPROCESSING

    @hybrid_property
    def blob(self) -> str:
        return "{folder}/{id}.parquet.gzip".format(
            folder=self.folder,
            id=self.id
        )

    @hybrid_method
    def upload(self, df, **kwargs) -> None:
        return storage.upload_parquet(df, self.mount, self.blob, **kwargs)

    @hybrid_method
    def download(self, **kwargs) -> pd.DataFrame:
        return storage.read_parquet(self.mount, self.blob, **kwargs)
