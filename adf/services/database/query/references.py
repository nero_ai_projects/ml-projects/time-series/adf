from sqlalchemy.orm import Session  # type: ignore
from adf.errors import BusinessUnitError
from adf.services.database import BusinessUnits


def get_business_unit(session: Session, country: str, division: str) -> BusinessUnits:
    """ Returns a BusinessUnit object for a country / division
    """
    business_unit = session.query(BusinessUnits) \
        .filter(BusinessUnits.country == country) \
        .filter(BusinessUnits.division == division) \
        .one_or_none()

    if not business_unit:
        raise BusinessUnitError("Unknown business unit: %s %s" % (country, division))

    return business_unit
