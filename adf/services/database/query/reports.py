from typing import Union, List
import logging
from uuid import UUID
from sqlalchemy.orm import Session  # type: ignore
from adf.services.database.base import Base

log = logging.getLogger("adf")


def build_report(session: Session, report_model: Base, status: str = "processing", **kwargs) \
        -> Base:
    """ Create modelling report with status processing
    """
    report = report_model(status=status, **kwargs)
    session.add(report)
    session.commit()
    log.info("%s: %s, %s", report, report.status, report.reason)
    return report


def update_report(session: Session, report: Base, status: str, reason: str = None) -> Base:
    """ Updates the reports status and reason
    """
    report.status = status
    report.reason = reason
    session.commit()
    log.info("%s: %s, %s", report, report.status, report.reason)
    return report


def get_report(
    session: Session, report_id: Union[str, UUID],
    report_model: Base, fail_for_status: List = None
) -> Base:
    """ Fetch modelling report given at least 7 first character of id
    """
    if fail_for_status is None:
        fail_for_status = ["failed", "skipped", "deprecated"]
    if isinstance(report_id, UUID):
        report_id = str(report_id)

    if len(report_id) < 7:
        raise ValueError(f"Invalid id {report_id}")

    report = session.query(report_model) \
        .filter(report_model.id == report_id) \
        .one_or_none()

    if report is None:
        raise ValueError(
            "%s %s not found" % (report_model.__class__.__name__, report_id)
        )

    if report.status in fail_for_status:
        raise ValueError(
            "%s status is %s" % (report, report.status)
        )

    return report
