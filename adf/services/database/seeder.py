import json
import logging
from pathlib import Path

from adf.services.database import get_session
from adf.services.database import BusinessUnits


logging.basicConfig(
    level="INFO",
    format="%(levelname)-5.5s [%(name)s] %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)

log = logging.getLogger("seeder")


DIR = Path(__file__).resolve().parent.parent.parent


def main():
    session = get_session()
    seed_database(session)


def seed_database(session):
    seed_business_units(session)


def seed_business_units(session):
    created, removed, active = 0, 0, 0
    payloads = load_json(
        DIR.joinpath("config/database"),
        "business_units.json"
    )
    business_units = []

    for payload in payloads:
        active += 1
        business_unit = session.query(BusinessUnits)\
            .filter(BusinessUnits.country == payload["country"])\
            .filter(BusinessUnits.division == payload["division"])\
            .one_or_none()
        if not business_unit:
            created += 1
            business_unit = BusinessUnits(**payload)
            session.add(business_unit)

        business_unit.sales_org_cod = payload["sales_org_cod"]
        business_unit.uom = payload["uom"]

        business_units.append(business_unit)

    session.commit()

    to_remove = set(business_units).symmetric_difference(set(session.query(BusinessUnits).all()))
    if len(to_remove) > 0:
        for business_unit in to_remove:
            removed += 1
            session.delete(business_unit)

    session.commit()

    log.info("Created BusinessUnits: %s", created)
    log.info("Removed BusinessUnits: %s", removed)
    log.info("Active BusinessUnits: %s", active)


def load_json(root, filename):
    filepath = Path(root).joinpath(filename)
    with open(filepath) as stream:
        return json.load(stream)


if __name__ == "__main__":
    main()
