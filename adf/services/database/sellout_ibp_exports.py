from typing import Any
from sqlalchemy.orm import relationship  # type: ignore
from sqlalchemy import (  # type: ignore
    Column, String, DateTime, ForeignKey,
    Boolean, Float, UniqueConstraint, PrimaryKeyConstraint
)
from adf.services.database.base import Base
from adf.services.database.custom_types import (
    GUID
)


class SelloutIBPExports(Base):
    __tablename__ = "sellout_ibp_exports"
    __table_args__ = (
        PrimaryKeyConstraint(
            "sales_organization", "sellout_ibp_report_id", "product_code",
            "location_code", "customer_code", "date"
        ),
        UniqueConstraint(
            "sales_organization", "sellout_ibp_report_id", "product_code",
            "location_code", "customer_code", "date"
        ),
    )
    export_to_ibp: Any = Column(Boolean, default=False)
    sales_organization: Any = Column(String, nullable=False)
    execution_date: Any = Column(DateTime(timezone=True), nullable=False)
    product_code: Any = Column(String(), nullable=False)
    product_level: Any = Column(String(), nullable=False)
    location_code: Any = Column(String())
    location_level: Any = Column(String())
    customer_code: Any = Column(String())
    customer_level: Any = Column(String())
    date: Any = Column(DateTime(timezone=True), nullable=False)
    sellout_volumes: Any = Column(Float())

    sellout_ibp_report_id: Any = Column(GUID, ForeignKey("sellout_ibp_reports.id"), nullable=False)
    sellout_ibp_report: Any = relationship(
        "SelloutIBPReports",
        back_populates="sellout_ibp_export",
        cascade="all, delete, delete-orphan",
        single_parent=True
    )
    pipeline_id: Any = Column(
        GUID, ForeignKey("pipeline_reports.id"), nullable=True
    )
    pipeline: Any = relationship(
        "PipelineReports",
        back_populates="sellout_ibp_export",
        cascade="all, delete, delete-orphan",
        single_parent=True
    )

    def __repr__(self):
        return "SelloutIBPExports[{}]".format(str(self.sellout_ibp_report_id)[:8])
