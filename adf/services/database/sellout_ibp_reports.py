import uuid
from typing import Any
from sqlalchemy.orm import relationship  # type: ignore
from sqlalchemy import Column, String, DateTime, ForeignKey  # type: ignore
from sqlalchemy.sql import func  # type: ignore
from sqlalchemy.ext.hybrid import hybrid_property  # type: ignore
from sqlalchemy.ext.hybrid import hybrid_method  # type: ignore
from adf import config
from adf.services import storage
from adf.services.database.base import Base
from adf.services.database.custom_types import (
    CustomJSON, GUID
)


class SelloutIBPReports(Base):
    __tablename__ = "sellout_ibp_reports"
    id: Any = Column(GUID, default=uuid.uuid4, primary_key=True)
    created_at: Any = Column(DateTime(timezone=True), server_default=func.current_timestamp())
    updated_at: Any = Column(DateTime(timezone=True), onupdate=func.current_timestamp())
    business_unit_id: Any = Column(GUID, ForeignKey("business_units.id"))
    pipeline_id: Any = Column(
        GUID, ForeignKey("pipeline_reports.id"), nullable=True
    )
    status: Any = Column(String, nullable=False)
    reason: Any = Column(String)
    description: Any = Column(CustomJSON, nullable=False)

    business_unit: Any = relationship(
        "BusinessUnits",
        back_populates="sellout_ibp_reports",
        cascade="all, delete, delete-orphan",
        single_parent=True
    )
    pipeline: Any = relationship(
        "PipelineReports",
        back_populates="sellout_ibp_report",
        cascade="all, delete, delete-orphan",
        single_parent=True
    )
    sellout_ibp_export: Any = relationship(
        "SelloutIBPExports",
        back_populates="sellout_ibp_report"
    )

    def __repr__(self):
        return "SelloutIBPReports[{}]".format(str(self.id)[:8])

    @property
    def mount(self) -> str:
        return config.MOUNT

    @property
    def folder(self):
        return "{folder}/{country}/{date}".format(
            folder=config.FOLDER_IBP_EXPORT,
            country=self.business_unit.country.capitalize(),
            date=self.created_at.strftime("%Y%m%d")
        )

    @hybrid_property
    def blob(self) -> str:
        return "{folder}/00{sales_org_cod}{suffix}.csv".format(
            folder=self.folder,
            sales_org_cod=self.business_unit.sales_org_cod,
            suffix=config.IBP_EXPORT_SELLOUT_SUFFIX
        )

    @hybrid_method
    def upload(self, df, **kwargs):
        return storage.upload_csv(df, self.mount, self.blob, **kwargs)

    @hybrid_method
    def download(self, **kwargs):
        return storage.read_csv(self.mount, self.blob, **kwargs)

    @hybrid_method
    def download_latest_processed(self, **kwargs):
        return storage.read_csv(
            self.mount,
            sorted([
                file
                for file in storage.list_all_objects(
                    self.mount,
                    f"{config.FOLDER_IBP_EXPORT}/{self.business_unit.country.capitalize()}"
                )
                if f"00{self.business_unit.sales_org_cod}{config.IBP_EXPORT_SELLOUT_SUFFIX}.csv"
                in file
            ])[-1],
            **kwargs
        )
