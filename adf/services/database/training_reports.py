import uuid
from typing import Any

from sqlalchemy.orm import relationship  # type: ignore
from sqlalchemy import Column, String, DateTime, ForeignKey  # type: ignore
from sqlalchemy.sql import func  # type: ignore
from sqlalchemy.ext.hybrid import hybrid_method, hybrid_property  # type: ignore
from adf.services.database.base import Base
from adf.services.database.custom_types import (
    CustomJSON, GUID
)
from adf.modelling.mllogs import save_model, load_model
from adf import config


class TrainingReports(Base):

    __tablename__ = "training_reports"
    id: Any = Column(GUID, default=uuid.uuid4, primary_key=True)
    created_at: Any = Column(
        DateTime(timezone=True), server_default=func.current_timestamp()
    )
    updated_at: Any = Column(
        DateTime(timezone=True), onupdate=func.current_timestamp()
    )
    preprocessing_report_id: Any = Column(
        GUID,
        ForeignKey("preprocessing_reports.id")
    )
    pipeline_id: Any = Column(
        GUID, ForeignKey("pipeline_reports.id"), nullable=True
    )
    description: Any = Column(CustomJSON, nullable=False)
    status: Any = Column(String, nullable=False)
    reason: Any = Column(String)
    training_type: Any = Column(String)
    __mapper_args__ = {
        'polymorphic_on': training_type,
        'polymorphic_identity': 'core'
    }

    preprocessing_report: Any = relationship(
        "PreprocessingReports",
        back_populates="training_reports"
    )
    forecast_reports: Any = relationship(
        "ForecastReports",
        back_populates="training_report",
        cascade="all, delete, delete-orphan",
        single_parent=True
    )
    pipeline: Any = relationship(
        "PipelineReports",
        back_populates="training_report",
        cascade="all, delete, delete-orphan",
        single_parent=True
    )

    def __repr__(self):
        return "TrainingReports[{}]".format(str(self.id)[:8])

    @property
    def mount(self) -> str:
        return config.MOUNT

    @property
    def folder(self) -> str:
        return config.FOLDER_TRAINING

    @hybrid_property
    def blob(self) -> str:
        return "{folder}/{id}.pickle".format(
            folder=self.folder,
            id=self.id
        )

    @hybrid_method
    def upload(self, model, **kwargs) -> None:
        return save_model(model, str(self.id), **kwargs)

    @hybrid_method
    def download(self, model_type: str) -> Any:
        return load_model(str(self.id), str(self.preprocessing_report_id), model_type)
