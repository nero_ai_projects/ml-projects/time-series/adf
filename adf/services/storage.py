import os
import logging
import databricks.koalas as ks  # type: ignore
import pandas as pd  # type: ignore
from adf.types import DataFrame
from adf.utils.credentials import get_dbutils
from pyspark.sql import SparkSession  # type: ignore
import pyspark.sql as ps  # type: ignore

log = logging.getLogger("adf")


def get_paths(mount: str, prefix: str):
    spark = SparkSession.builder.getOrCreate()
    for dir_path in get_dbutils(spark).fs.ls(f"{mount}/{prefix}"):
        if dir_path.isFile():
            yield dir_path.path[len(mount) + 6:]
        elif dir_path.isDir() and f"{mount}/{prefix}" != dir_path.path:
            yield from get_paths(mount, dir_path.path[len(mount) + 6:])


def list_all_objects(mount: str, prefix: str):
    return list(get_paths(mount, prefix))


def upload_csv(df: pd.DataFrame, mount: str, blob: str, mode: str = "overwrite", **kwargs) -> None:
    """Uploads a pd.DataFrame as csv file"""
    spark = SparkSession.builder.getOrCreate()
    get_dbutils(spark).fs.mkdirs(os.path.dirname(f"{mount}/{blob}"))
    if isinstance(df, pd.DataFrame):
        df = ks.from_pandas(df)
    if df.index.names != [None]:
        spark_df = df.to_spark(df.index.names)
    else:
        spark_df = df.to_spark()
    for col_name, col_type in spark_df.dtypes:
        if col_type == "null":
            spark_df = spark_df.withColumn(col_name, spark_df[col_name].cast("string"))
    return spark_df.write.format("csv").save(f"{mount}/{blob}", header=True, mode=mode, **kwargs)


def read_csv(mount: str, blob: str, infer_schema: bool = True, **kwargs) -> pd.DataFrame:
    """Returns pandas DataFrame saved as .csv locally

    :kwargs: keyword arguments passed to pd.read_csv
    """
    return ks.DataFrame.to_koalas(
        SparkSession.builder.getOrCreate().read.csv(
            f"{mount}/{blob}", header=True, inferSchema=infer_schema, **kwargs
        )
    ).to_pandas()


def upload_parquet(
    df, mount: str, blob: str, compression: str = "gzip", mode: str = "overwrite", **kwargs
) -> None:
    """Uploads a pd.DataFrame as parquet file"""
    spark = SparkSession.builder.getOrCreate()
    get_dbutils(spark).fs.mkdirs(os.path.dirname(f"{mount}/{blob}"))
    if isinstance(df, pd.DataFrame):
        df = ks.from_pandas(df)
    if isinstance(df, ks.DataFrame):
        if df.index.names != [None]:
            spark_df = df.to_spark(df.index.names)
        else:
            spark_df = df.to_spark()
    elif isinstance(df, ps.dataframe.DataFrame):
        spark_df = df

    for col_name, col_type in spark_df.dtypes:
        if col_type == "null":
            spark_df = spark_df.withColumn(col_name, spark_df[col_name].cast("string"))
    spark_writer = spark_df.write
    if "partition_on" in kwargs and kwargs["partition_on"]:
        spark_writer = spark_writer.partitionBy(kwargs["partition_on"])
        kwargs.pop("partition_on", None)
    return spark_writer.format("parquet").save(
        f"{mount}/{blob}", compression=compression, mode=mode, **kwargs
    )


def read_parquet(mount: str, blob: str, **kwargs) -> DataFrame:
    """Returns pandas DataFrame saved as .parquet in mount

    :kwargs: keyword arguments passed to pd.read_parquet
    """
    object_type = kwargs.get("object_type", "pandas")
    kwargs.pop("object_type", None)
    df_spark = SparkSession.builder.getOrCreate().read.parquet(f"{mount}/{blob}", **kwargs)
    df = ks.DataFrame.to_koalas(df_spark)
    if object_type == "pandas":
        return df.to_pandas()
    if object_type == "spark":
        return df_spark
    return df
