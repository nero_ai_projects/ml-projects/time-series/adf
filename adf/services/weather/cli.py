from datetime import datetime
import click
from adf.utils.imports import import_method


@click.group("weather")
def commands():
    """ Fetch weather forecast and historical data
    """
    return


@commands.command("forecast")
@click.option(
    "--country", "-c", required=True,
    help="country to be queried")
@click.option(
    "--division", "-d", required=True,
    help="division for which data will be queried and used")
def fetch_forecast(country: str, division: str):
    """ Fetch weather forecast for the next week
    """
    import_method("adf.services.weather.main", "fetch_forecast_for_current_date")(
        country, division
    )


@commands.command("historical")
@click.option(
    "--country", "-c", required=True,
    help="country to be queried")
@click.option(
    "--division", "-d", required=True,
    help="division for which data will be queried and used")
@click.option(
    "--start_date", "-s", required=False,
    type=click.DateTime(formats=["%Y-%m-%d"]),
    help="interval start date")
@click.option(
    "--end_date", "-e", required=False,
    type=click.DateTime(formats=["%Y-%m-%d"]),
    help="interval end date")
def fetch_historical(country: str, division: str, start_date: datetime, end_date: datetime):
    """ Fetch weather historical data
    """
    import_method("adf.services.weather.main", "fetch_historical_for_dates_or_last_day")(
        country,
        division,
        start_date.date() if start_date else None,
        end_date.date() if end_date else None
    )
