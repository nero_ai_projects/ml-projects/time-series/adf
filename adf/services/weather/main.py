import logging
from typing import List, Dict, Iterator, Tuple, Optional, Union
from datetime import date as Date
from datetime import datetime
from datetime import timedelta

import requests
import pandas as pd  # type: ignore
from jsonschema import validate  # type: ignore
from snowflake.connector.errors import ProgrammingError  # type: ignore

from adf import config
from adf import utils
from adf.errors import WeatherError
from adf.errors import NoApiKeysLeftError
from adf.utils.credentials import get_snowflake_connector
from adf.utils.dataframe import create_table, get_col_types


log = logging.getLogger("adf")
DTYPES = config.load_config("weather/dtypes.json")


def fetch_forecast_for_current_date(country: str, division: str) -> None:
    date = datetime.now().date()
    fetch_forecast(country, division, date)


def fetch_historical_for_dates_or_last_day(
    country: str, division: str, start_date: Optional[Date], end_date: Optional[Date]
) -> None:
    if not (start_date and end_date):
        start_date = datetime.now().date() - timedelta(days=1)
        end_date = start_date
    fetch_historical(country, division, start_date, end_date)


class DarkSkyApi:

    def __init__(self) -> None:
        self.keys = utils.load_weather_apikeys()
        if len(self.keys) < 1:
            raise WeatherError("No API key found")
        self._rotations = 0

    @property
    def key(self) -> str:
        return self.keys[0]

    def rotate(self) -> None:
        log.info("Rotating API keys")
        if self._rotations >= len(self.keys) - 1:
            raise NoApiKeysLeftError("No API keys left")
        self._rotations += 1
        self.keys = self.keys[1:] + self.keys[:1]

    def request(self, latitude: float, longitude: float, type_: str, date: Date) -> Dict:
        """ Requests DarkSky api and returns the payload

        Rotates keys if reach daily limit requests
        Check https://darksky.net/dev/docs
        """
        uri = self.build_uri(latitude, longitude, type_, date)
        response = requests.get(
            uri, params={"exclude": "currently,minutely,hourly,alerts,flags"}
        )

        if response.status_code == 403:
            error = response.json()["error"]
            if error == "daily usage limit exceeded":
                self.rotate()
                return self.request(latitude, longitude, type_, date)

        if response.status_code != 200:
            response.raise_for_status()

        return response.json()

    def build_uri(self, latitude: float, longitude: float, type_: str, date: Date) -> str:
        """ Build the URI to be requested
        """
        uri = config.WEATHER_URI.format(
            key=self.key, latitude=latitude, longitude=longitude
        )
        if type_ == "historical":
            dfmt = date.strftime("%Y-%m-%dT12:00:00")
            uri += f",{dfmt}"
        return uri


def fetch_forecast(country: str, division: str, date: Date) -> None:
    """ Fetch forecast weather data for the next week given a country
    """
    log.info("Fetching weather forecast for %s", country)
    client = DarkSkyApi()
    cities = get_cities(country)
    process_date(client, country, division, cities, "forecast", date)


def fetch_historical(
        country: str, division: str, start_date: Date, end_date: Date) -> None:
    """ Fetch historical weather data in the date range
    """
    client = DarkSkyApi()
    cities = get_cities(country)
    for date in date_range(start_date, end_date):
        log.info("Processing date %s", date)
        process_date(client, country, division, cities, "historical", date)


def process_date(
    client: DarkSkyApi, country: str, division: str,
    cities: List[Dict], type_: str, date: Date
) -> None:
    conn = get_snowflake_connector()
    curr = conn.cursor()
    table = build_weather_table_name(country, division, type_)

    # Delete existing weather data from weather view
    try:
        curr.execute(
            f'DELETE '
            f'FROM "{config.SNOWFLAKE_DATABASE}"."VCP_DTM_AFC"."{table}" '
            f"WHERE DATE = '{date.strftime('%Y-%m-%d')}'"
        )
    except ProgrammingError:
        curr.close()

    # Request new weather data for given date
    weather_df, err = process_cities(
        client,
        pd.DataFrame(columns=["city"]),
        cities,
        type_,
        date
    )
    weather_df = weather_df.astype(DTYPES)
    weather_df["date"] = pd.to_datetime(weather_df["date"], utc=True).dt.ceil(freq='ms')
    col_type = get_col_types(weather_df)
    create_table(table, 'create_or_append', col_type, weather_df, schema="VCP_DTM_AFC")
    log.info("Finished fetching weather data for %s - %s - %s", country, division, date)

    if err is not None:
        raise WeatherError(err)


def process_cities(
    client: DarkSkyApi, weather_df: pd.DataFrame, cities: List[Dict], type_: str, date: Date
) -> Tuple[pd.DataFrame, Optional[Exception]]:
    for city in cities:
        if city["city"] in weather_df.city.unique():
            continue
        try:
            city_data = fetch_data(client, city, type_, date)
            weather_df = weather_df.append(city_data, ignore_index=True)
        except NoApiKeysLeftError as err:
            return weather_df, err
    return weather_df, None


def get_cities(country: str) -> List[Dict]:
    """ Load cities configs given a country
    """
    try:
        cities = config.load_config(f"weather/{country}.json")
        schema = config.load_config("weather/schema.json")
        validate(cities, schema)
        return cities
    except FileNotFoundError:
        raise WeatherError("No config found for %s" % country)


def date_range(start_date: Date, end_date: Date)\
        -> Iterator[Date]:
    """ Yields dates between two dates
    """
    if start_date > end_date:
        raise WeatherError("Start date cannot be after end date")
    delta = end_date - start_date
    for i in range(delta.days + 1):
        yield start_date + timedelta(days=i)


def fetch_data(client: DarkSkyApi, city: Dict, type_: str, date: Date)\
        -> pd.DataFrame:
    """ Requests weather data and build dataframe
    """
    data = client.request(city["latitude"], city["longitude"], type_, date)
    daily_data = data.get("daily", {}).get("data", [])
    if not daily_data:
        log.error("Data unavailable for %s %s", city["city"], date)
        return pd.DataFrame()
    return normalize_df(data, daily_data, city, date)


def normalize_df(data: Dict, daily_data: Union[Dict, List[Dict]], city: Dict, date: Date)\
        -> pd.DataFrame:
    """ Normalize DataFrame with expected columns and types
    """
    weather_df = pd.DataFrame(columns=list(DTYPES.keys()))
    weather_df = weather_df.append(pd.json_normalize(daily_data))
    weather_df["city"] = city["city"]
    weather_df["timezone"] = data["timezone"]
    weather_df["latitude"] = data["latitude"]
    weather_df["longitude"] = data["longitude"]
    weather_df["date"] = date
    return weather_df


def build_weather_table_name(
        country: str, division: str, type_: str
) -> str:
    country_name = config.SNOWFLAKE_MAPPING_COUNTRIES[country]
    division_name = config.SNOWFLAKE_MAPPING_DIVISIONS[division]
    type_name = {
        "historical": "HIS",
        "forecast": "FRCT"
    }[type_]
    return f'{division_name}_{country_name}_WEA_{type_name}'
