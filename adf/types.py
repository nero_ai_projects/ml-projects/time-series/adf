from typing import Union
import databricks.koalas as ks  # type: ignore
import pandas as pd  # type: ignore
import numpy as np  # type: ignore
import dask.dataframe as dd  # type: ignore
from xgboost import XGBRegressor  # type: ignore
from xgboost.dask import DaskXGBRegressor  # type: ignore
from catboost import CatBoostRegressor  # type: ignore
from lightgbm import LGBMRegressor  # type: ignore
from lightgbm.dask import DaskLGBMRegressor  # type: ignore
try:
    from catboost_spark import CatBoostRegressor as SparkCatBoostRegressor  # type: ignore
except ImportError:
    SparkCatBoostRegressor = None
from sklearn.ensemble import RandomForestRegressor  # type: ignore


DataFrame = Union[pd.DataFrame, dd.DataFrame, ks.DataFrame]
Series = Union[pd.Series, dd.Series, ks.DataFrame]
Predictions = Union[np.ndarray, float, None]
Model = Union[
    XGBRegressor, DaskXGBRegressor,
    CatBoostRegressor,
    LGBMRegressor, DaskLGBMRegressor,
    SparkCatBoostRegressor,
    RandomForestRegressor
]
