from typing import List, Any
import json
from azure.keyvault.secrets import SecretClient  # type: ignore
from azure.identity import DefaultAzureCredential  # type: ignore
from azure.core.exceptions import ResourceNotFoundError  # type: ignore
from pyspark.sql import SparkSession  # type: ignore
from pyspark.sql.utils import IllegalArgumentException  # type: ignore

from adf import config
from snowflake import connector  # type: ignore
from adf.errors import AzureKeyVaultSecretError


def get_dbutils(spark):
    if spark.conf.get("spark.databricks.service.client.enabled", None) == "true":
        from pyspark.dbutils import DBUtils  # type: ignore

        return DBUtils(spark)
    else:
        import IPython  # type: ignore

        interactive_shell = IPython.get_ipython()
        if interactive_shell:
            return interactive_shell.user_ns["dbutils"]
        else:
            return None


def get_secret(secret_name: str, bypass_local: bool = False) -> Any:
    spark_session = SparkSession.builder.getOrCreate()
    dbutils = get_dbutils(spark_session)
    keyvault_client = get_keyvault_client()
    try:
        if dbutils:
            secret_value = dbutils.secrets.get(scope=config.KEY_VAULT_NAME, key=secret_name)
        else:
            secret_value = keyvault_client.get_secret(secret_name).value
    except (ResourceNotFoundError, IllegalArgumentException) as err:
        try:
            if secret_name == "local-database-postgres" and not bypass_local:
                return config.DEFAULT_CREDENTIALS
        except Exception:
            raise AzureKeyVaultSecretError(err)
    else:
        try:
            return json.loads(secret_value)
        except json.decoder.JSONDecodeError:
            return secret_value


def get_keyvault_client():
    keyvault_name = config.KEY_VAULT_NAME
    keyvault_uri = f"https://{keyvault_name}.vault.azure.net"
    credential = DefaultAzureCredential()
    client = SecretClient(vault_url=keyvault_uri, credential=credential)
    return client


def get_snowflake_connector(
    secret=None,
    warehouse=config.SNOWFLAKE_WAREHOUSE,
    database=config.SNOWFLAKE_DATABASE,
    schema=config.SNOWFLAKE_SCHEMA,
    client_session_keep_alive=True,
):
    if not secret:
        secret = get_secret(config.SNOWFLAKE_PWD_NAME)

    return connector.connect(
        user=config.SNOWFLAKE_USER,
        password=secret,
        account=config.SNOWFLAKE_ACCOUNT,
        warehouse=warehouse,
        database=database,
        schema=schema,
        client_session_keep_alive=client_session_keep_alive,
        role=config.SNOWFLAKE_ROLE
    )


def get_snowflake_spark_options(
    secret=None,
    warehouse=config.SNOWFLAKE_WAREHOUSE,
    database=config.SNOWFLAKE_DATABASE,
    schema=config.SNOWFLAKE_SCHEMA,
):
    if not secret:
        secret = get_secret(config.SNOWFLAKE_PWD_NAME)

    return {
        "sfUrl": config.SNOWFLAKE_URL,
        "sfUser": config.SNOWFLAKE_USER,
        "sfPassword": secret,
        "sfDatabase": database,
        "sfSchema": schema,
        "sfWarehouse": warehouse,
    }


def get_database_uri(secret=None):
    if config.ENV in ("dev", "prod"):
        return get_database_uri_snowflake(secret)
    else:
        return get_database_uri_postgres()


def get_database_uri_snowflake(
    secret=None,
    database=config.SNOWFLAKE_DATABASE,
    schema=config.SNOWFLAKE_SCHEMA,
    warehouse=config.SNOWFLAKE_WAREHOUSE,
):
    if not secret:
        secret = get_secret(config.SNOWFLAKE_PWD_NAME)

    uri = (
        "snowflake://{user}:{password}@{account}/{database}"
        "/{schema}?warehouse={warehouse}&role={role}&client_session_keep_alive=true"
    ).format(
        user=config.SNOWFLAKE_USER,
        password=secret,
        account=config.SNOWFLAKE_ACCOUNT,
        database=database,
        schema=schema,
        warehouse=warehouse,
        role=config.SNOWFLAKE_ROLE,
    )
    return uri


def get_database_uri_postgres() -> str:
    if config.ENV == "test":
        uri = "postgresql+psycopg2://postgres@0.0.0.0:5433"
    else:
        credentials = config.DEFAULT_CREDENTIALS
        uri = ("postgresql+psycopg2://" "{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}").format(
            **credentials
        )
    return uri


def load_weather_apikeys() -> List:
    tokens = get_secret("weather-tokens")
    return tokens


def load_plc_apikeys() -> dict:
    plc_odata_cred = get_secret("odata-plc")
    return plc_odata_cred
