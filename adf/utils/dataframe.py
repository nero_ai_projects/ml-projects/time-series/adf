import pandas as pd  # type: ignore
import numpy as np  # type: ignore
from snowflake.connector.pandas_tools import write_pandas  # type: ignore
from adf import utils, config


def memory_reduction(
        df: pd.DataFrame, errors: str = "raise"
) -> pd.DataFrame:
    """
    Downcasts numeric variables in input dataframe to reduce memory usage

    Parameters
    ----------
    df: pandas.DataFrame
        input dataframe to downcast
    errors: str. {‘ignore’, ‘raise’, ‘coerce’}, default ‘raise’
        type of error management when casting type \
        - If ‘raise’, then invalid parsing will raise an exception. \
        - If ‘coerce’, then invalid parsing will be set as NaN.\
        - If ‘ignore’, then invalid parsing will return the input.

    Returns
    -------
    pandas.DataFrame
        downcasted input dataframe.
    """
    float_cols = list(df.select_dtypes('float').columns)
    int_cols = list(df.select_dtypes('integer').columns)
    float_cols_to_cast_as_int = [col for col in float_cols if (df[col] % 1 == 0).all()]
    if float_cols_to_cast_as_int:
        float_cols = [col for col in float_cols if col not in float_cols_to_cast_as_int]
        int_cols.extend(float_cols_to_cast_as_int)
    for col in float_cols:
        df[col] = pd.to_numeric(df[col].astype(float), downcast='float', errors=errors)
    for col in int_cols:
        df[col] = pd.to_numeric(df[col].astype(int), downcast='integer', errors=errors)
    return df


def get_col_types(df):
    """
    Helper function to create/modify Snowflake tables;
    gets the column and dtype pair for each item in the dataframe

    args:
        df: dataframe to evaluate
    """
    # get dtypes and convert to df
    df_dtypes = df.dtypes.reset_index().rename(columns={0: 'col'})
    # case matching as snowflake needs it in uppers
    df_dtypes = df_dtypes.apply(lambda x: x.astype(str).str.upper())

    # only considers objects at this point
    # only considers objects and ints at this point
    df_dtypes['col'] = np.where(
        df_dtypes['col'] == 'OBJECT', 'VARCHAR', df_dtypes['col']
    )
    df_dtypes['col'] = np.where(
        df_dtypes['col'].str.contains('DATE'), 'DATETIME', df_dtypes['col']
    )
    df_dtypes['col'] = np.where(
        df_dtypes['col'].str.contains('INT'), 'NUMERIC', df_dtypes['col']
    )
    df_dtypes['col'] = np.where(
        df_dtypes['col'].str.contains('FLOAT'), 'FLOAT', df_dtypes['col']
    )

    # get the column dtype pair
    rows = []
    for index, row in df_dtypes.iterrows():
        rows.append(row['index'] + ' ' + row['col'])

    string_value = ', '.join(rows)  # convert from list to a string object

    string_value = string_value.strip()

    return string_value


def create_table(table, action, col_type, df, schema=config.SNOWFLAKE_SCHEMA):
    """
    Function to create/replace and append to tables in Snowflake
    args:
        table: name of the table to create/modify
        action: whether do the initial create/replace or appending;
        col_type: string with column name associated dtype,
            each pair separated by a comma; comes from get_col_types() func
        df: dataframe to load
    dependencies: function get_col_types();
        helper function to get the col and dtypes to create a table
    """
    # set up connection
    conn = utils.get_snowflake_connector(schema=schema)
    # set up cursor
    cur = conn.cursor()
    if action == 'create_or_replace':
        # set up execute
        cur.execute(
            f"CREATE OR REPLACE TABLE {table} ({col_type})"
        )
        # prep to ensure proper case
        df.columns = [col.upper() for col in df.columns]
        # write df to table
        write_pandas(
            conn,
            df,
            table.upper(),
            chunk_size=100000,
            compression="gzip"
        )
    elif action == 'create_or_append':
        # set up execute
        cur.execute(
            f"CREATE TABLE IF NOT EXISTS {table} ({col_type})"
        )
        # prep to ensure proper case
        df.columns = [col.upper() for col in df.columns]
        # write df to table
        write_pandas(
            conn,
            df,
            table.upper(),
            chunk_size=100000,
            compression="gzip"
        )
    elif action == 'append':
        # convert to a string list of tuples
        df = str(list(df.itertuples(index=False, name=None)))
        # get rid of the list elements so it is a string tuple list
        df = df.replace('[', '').replace(']', '')
        cur.execute(
            f"INSERT INTO {table} VALUES {df}"
        )
