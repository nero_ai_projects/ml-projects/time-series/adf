from typing import Union, Any
from datetime import date
from datetime import datetime
from dateutil.relativedelta import relativedelta

import databricks.koalas as ks  # type: ignore
import pandas as pd  # type: ignore
import dask.dataframe as dd  # type: ignore


def start_of_week(arg: Union[date, datetime, pd.Series, dd.Series, ks.Series]) -> Any:
    """ Moves date to the previous monday
    Accepts datetime.date, datetime.datetime or pd.Series instance
    """
    if isinstance(arg, (date, datetime)):
        return arg + relativedelta(days=-arg.weekday())

    if isinstance(arg, (pd.Series, dd.Series)):
        return arg - arg.dt.weekday.astype("timedelta64[D]")

    if isinstance(arg, (ks.Series,)):
        return arg.apply(lambda d: d + relativedelta(days=-d.weekday()))

    raise TypeError("Unexpected type %s" % type(arg))


def start_of_month(arg: Union[date, datetime, pd.Series, dd.Series]) -> Any:
    """ Move date to first day of the month
    Accepts datetime.date, datetime.datetime or pd.Series instance
    """
    if isinstance(arg, (date, datetime)):
        return arg.replace(day=1)

    if isinstance(arg, (pd.Series, dd.Series)):
        return arg.values.astype("datetime64[M]")

    raise TypeError("Unexpected type %s" % type(arg))


def parse_date_columns(df, columns):
    for column in columns:
        if column in df.columns:
            df[column] = pd.to_datetime(df[column])
    return df
