from typing import Callable, Any
import logging
import time
from functools import wraps
import pandas as pd  # type: ignore
import databricks.koalas as ks  # type: ignore

log = logging.getLogger("adf")


def timeit(method: Callable) -> Callable:
    @wraps(method)
    def timed(*args, **kw) -> Any:
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        log.info("%r: time %2.2fs", method.__name__, (te - ts))
        return result
    return timed


def shapeit(method: Callable) -> Callable:
    @wraps(method)
    def wrapped(*args, **kw) -> Any:
        result = method(*args, **kw)
        if isinstance(result, pd.DataFrame) or isinstance(result, ks.DataFrame):
            log.info("%r: shape %s", method.__name__, result.shape)
        return result
    return wrapped


def showcolumns(method: Callable) -> Callable:
    @wraps(method)
    def wrapped(*args, **kw) -> Any:
        result = method(*args, **kw)
        if isinstance(result, pd.DataFrame) or isinstance(result, ks.DataFrame):
            log.info("%r: columns %s", method.__name__, result.columns.values)
        return result
    return wrapped


def showdtypes(method: Callable) -> Callable:
    @wraps(method)
    def wrapped(*args, **kw) -> Any:
        result = method(*args, **kw)
        if isinstance(result, pd.DataFrame) or isinstance(result, ks.DataFrame):
            log.info("%r: columns %s", method.__name__, result.dtypes.to_dict())
        return result
    return wrapped
