from typing import List, Dict, Union, Any
import json
import logging


log = logging.getLogger("adf")


def load_json(filepath: str) -> Any:
    """ Loads json from a file path
    """
    with open(filepath, 'r') as stream:
        return json.load(stream)


def save_json(filepath: str, instance: Union[List, Dict]) -> None:
    """ Saves dict or list as json file
    """
    with open(filepath, "w") as stream:
        json.dump(instance, stream, sort_keys=True, indent=4)


def read_config(config_path: str) -> Dict:
    """ Loads, parses and returns configuration as a dict
    """
    config_dict = load_json(config_path)
    display_config(config_dict)
    return config_dict


def display_config(configuration: Dict) -> None:
    """ Display config dictionary
    """
    log.info("============= ARGUMENTS =============")
    max_length = max([len(key) for key in configuration.keys()])
    for key, value in configuration.items():
        log.info(f"%{max_length}s --> %s", key, value)
    log.info("============= END OF ARGUMENTS =============")
