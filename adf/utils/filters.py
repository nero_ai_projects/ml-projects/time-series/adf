import functools
from operator import and_
import pandas as pd  # type: ignore
from adf import config
from adf.utils import shapeit
from adf.utils import Schema, BaseSchema
from adf.types import DataFrame


class Filters(Schema):

    SCHEMA = config.load_config("modelling/filters.json")

    @shapeit
    def filter(self, df: DataFrame) -> DataFrame:
        for filter_ in iter(self):
            if filter_.type in ["include", "exclude"]:
                df = self._filter_values(df, filter_)
        return df

    @shapeit
    def replace(self, df: DataFrame) -> DataFrame:
        for filter_ in iter(self):
            if filter_.type == "replace":
                df = self._replace_values(df, filter_)
        return df

    @staticmethod
    def _replace_values(df: DataFrame, filter_: BaseSchema) -> DataFrame:
        masks = []
        for scope in filter_.get("scope"):
            if isinstance(df, pd.DataFrame) and (scope.get("column") in df.index.names):
                masks.append(
                    df.index.get_level_values(scope.get("column")).isin(scope.get("match"))
                )
            else:
                masks.append(df[scope.get("column")].isin(scope.get("match")))
        df[filter_.get("column")] = df[filter_.get("column")].mask(
            functools.reduce(and_, masks), filter_.get("values")
        )
        return df

    @staticmethod
    def _filter_values(df: DataFrame, filter_: BaseSchema) -> DataFrame:
        if isinstance(df, pd.DataFrame) and filter_.column in df.index.names:
            mask = df.index.get_level_values(filter_.column) \
                .isin(filter_.get("values"))
        else:
            mask = df[filter_.column].isin(filter_.get("values"))
        return df.loc[mask] if filter_.type == "include" else df.loc[~mask]
