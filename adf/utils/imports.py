from typing import Any


def import_method(module: str, method: str) -> Any:
    return (getattr(
        __import__(module, fromlist=[method]), method
    ))
