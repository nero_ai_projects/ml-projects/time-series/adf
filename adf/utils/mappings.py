import os
import pandas as pd  # type: ignore
from adf import config
from adf.services import storage
from adf.utils.paths import get_project_root
from adf.types import DataFrame


def get_mappings(country: str, division: str, mapping_name: str, object_type: str = "pandas"):
    if config.ENV == "test":
        if "inno" in mapping_name:
            mapping_path = f"../tests/modelling/config/innovation/{mapping_name}.csv"
        else:
            mapping_path = f"../tests/modelling/config/{mapping_name}.csv"
        path = os.path.join(get_project_root(), mapping_path)
        return pd.read_csv(path)
    return storage.read_parquet(
        config.MOUNT,
        "{folder}/{country}/{division}/"
        "{source}/{name}.parquet.gzip".format(
            folder=config.FOLDER_REPORTS_DATA,
            country=country,
            division=division,
            source="mappings",
            name=mapping_name
        ),
        object_type=object_type
    )


def save_mappings(
        mappings: DataFrame,
        country: str,
        division: str,
        mapping_name: str
):
    blob = "{folder}/{country}/{division}/{source}/{name}.parquet.gzip".format(
        folder=config.FOLDER_REPORTS_DATA,
        country=country,
        division=division,
        source="mappings",
        name=mapping_name
    )
    storage.upload_parquet(mappings, config.MOUNT, blob)
