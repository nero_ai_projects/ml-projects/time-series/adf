from typing import Dict, Callable, Any, Optional
import os
import logging
from pathlib import Path
from functools import wraps
import tempfile
import mlflow  # type: ignore
from dask.distributed import Client  # type: ignore
from sqlalchemy.orm import Session  # type: ignore
from adf import config
from adf.services import database


class Provider:
    """ This class is supposed to be used as a decorator to setup common services

    name: name to be used on logging and MLFlow experiment
    mlrun: flag to start a MLFlow run
    session: flag to provide a database session
    cluster: flag to provide a Dask cluster
    """

    def __init__(
        self, name: str, mlrun: bool = False,
        session: bool = False, cluster: bool = False
    ):
        self.name = name
        self.has_mlrun = mlrun
        self.has_session = session
        self.has_cluster = cluster
        self.handler: Optional[logging.FileHandler] = None
        self.mlrun: Optional[mlflow.ActiveRun] = None
        self.session: Optional[Session] = None
        self.cluster: Optional[Client] = None
        self.mlflow_exp_name = self.name
        if os.getenv("MLFLOW_TRACKING_URI") == "databricks":
            self.mlflow_exp_name = "{databricks_mlflow_path}{experiment_name}".format(
                databricks_mlflow_path=config.DATABRICKS_MLFLOW_PATH,
                experiment_name=self.name
            )

    def __call__(self, func: Callable) -> Callable:
        @wraps(func)
        def wrapper(*args, **kwargs) -> Any:
            try:
                self.pipeline_id = kwargs.get('pipeline_id', None)
                self.open()
                self.enhance(kwargs)
                result = func(*args, **kwargs)
                self._status = "FINISHED"
                return result
            except Exception as err:
                if self.has_session and self.session:
                    self.session.rollback()
                self.status = "FAILED"
                if self.mlrun:
                    mlflow.end_run(status="FAILED")
                raise err
            finally:
                self.close()
        return wrapper

    def open(self) -> None:
        self.temp_dir = tempfile.TemporaryDirectory()
        self.handler = logging.FileHandler(
            Path(self.temp_dir.name, f"{self.name}.log")
        )
        logger = logging.getLogger("adf")
        logger.addHandler(self.handler)

        if self.has_mlrun:
            mlflow.end_run()
            mlflow.set_experiment(self.mlflow_exp_name)
            self.mlrun = mlflow.start_run()

        if self.has_session:
            self.session = database.get_session()

        if self.has_cluster:
            if config.DASK_SCHEDULER_ADDRESS:
                self.cluster = Client(config.DASK_SCHEDULER_ADDRESS)

    def close(self) -> None:
        if self.has_cluster and self.cluster:
            self.cluster.close()

        if self.has_session and self.session:
            self.session.expunge_all()
            self.session.close()

        if self.has_mlrun:
            mlflow.log_artifact(
                str(Path(self.temp_dir.name, f"{self.name}.log"))
            )
            mlflow.end_run(status="FINISHED")

        assert isinstance(self.handler, logging.FileHandler)
        logger = logging.getLogger("adf")
        logger.removeHandler(self.handler)
        self.handler.close()
        self.temp_dir.cleanup()

    def enhance(self, kwargs: Dict) -> None:
        kwargs.update({
            "handler": self.handler,
            "mlrun": self.mlrun,
            "session": self.session,
            "cluster": self.cluster,
        })
