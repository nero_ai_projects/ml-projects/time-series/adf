from typing import List, Dict, Union, Any
from jsonschema import validate  # type: ignore


class BaseSchema:

    def __init__(self, config):
        self._config = config
        self._index = -1

    def get(self, key: str, default: Any = None) -> Any:
        return self._config.get(key, default)

    def __iter__(self) -> Any:
        return self

    def __len__(self) -> int:
        return len(self._config)

    def __next__(self) -> Any:
        self._index += 1
        if self._index >= len(self._config):
            self._index = -1
            raise StopIteration
        return self._dispatch(self._config[self._index])

    def __getstate__(self):
        return self.__dict__

    def __setstate__(self, value: Any):
        self.__dict__ = value

    def __getattr__(self, attr: str) -> Any:
        return self._dispatch(self._config[attr])

    def __getitem__(self, index: int) -> Any:
        return self._dispatch(self._config[index])

    def _dispatch(self, value: Any) -> Any:
        if isinstance(value, (dict, list)):
            return BaseSchema(value)
        return value


class Schema(BaseSchema):

    SCHEMA = None

    def __init__(self, config: Union[List, Dict]):
        super().__init__(config)
        validate(config, self.SCHEMA)

    def merge_config(self, config: Dict):
        self._config = {
            **config,
            **self._config,
        }
