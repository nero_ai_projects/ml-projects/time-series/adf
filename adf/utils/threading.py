from typing import List, Callable
from multiprocessing import Pool
import numpy as np  # type: ignore
import pandas as pd  # type: ignore
from adf.types import DataFrame


def multithreaded(func: Callable, args: List, threads: int) -> List:
    pool = Pool(threads)
    results = pool.map(func, args)
    pool.close()
    pool.join()
    return results


def split_df_in_chunks(df: DataFrame, column: str, nchunks: int) -> np.ndarray:
    unique = df[column].unique() if isinstance(df, pd.DataFrame) else df[column].unique().compute()
    nchunks = min(unique.size, nchunks)
    return np.array_split(unique, nchunks)
