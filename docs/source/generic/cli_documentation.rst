CLI documentation
:::::::::::::::::


This guide shows how to run the different modules available in the ``adf`` library.
What is important to have in mind is that any command can be executed in one of the following environments: "test",
"local", "dev", prod". Hence, you will have to specify your ENV variable each time a
command is run or to configure the default ENV variable in your terminal using the following command :

.. code-block:: bash

    export ENV=dev

Services
--------
The ``adf.services`` module contains different libraries. Some of them can be executed by user using a CLI (command line
interface).

Calendar
********
This module enables to fetch calendar/holidays data for a given country, province and
time period (between a start date and end date).
This request is launched with the data preparation process or by the orchestrator. It can also be launched on-demand.
To do so, the following command will have to be executed from the remote machine, updating ENV and arguments if
necessary:

.. code-block:: bash

    ENV="local" adf calendar holidays -c france -s 2014-01-01 -e 2020-12-31 -p None -st None --add_zones False

The arguments in the ``adf calendar holidays`` are as follows:

  -c, --country TEXT        The country for which you want to get holiday data [required]
  -s, --start_date TEXT     The start date of the period  [required]
  -e, --end_date TEXT       The end date of the period  [required]
  -p, --province TEXT       The province for which you want to get holiday data, default None
  -st, --state TEXT         The state for which you want to get holiday data, default None
  --add_zones TEXT          default None

Weather
*******
This module enables to fetch historical weather data for a given country, province and
time period; or forecasted weather data for the new week. This request is launched with the data preparation
process or by the orchestrator. It can also be launched on-demand. To do so, the following command will have to
be executed from the remote machine, updating ENV and arguments if necessary:

.. code-block:: bash

    ENV=local adf weather historical -c france -s 2014-01-01 -e 2019-12-31
    # or
    ENV=local adf weather forecast -c france

The arguments in the ``adf weather historical`` are as follows:

  -c, --country TEXT    Country to be queried  [required]
  -d, --division TEXT   Division to be queried (dairy, water, etc.) [required]
  -s, --start_date      Interval start date [%Y-%m-%d] [required]
  -e, --end_date        Interval end date [%Y-%m-%d] [required]

The arguments in the ``adf weather forecast`` are as follows:

  -c, --country TEXT    Country to be queried  [required]
  -d, --division TEXT   Division to be queried (dairy, water, etc.) [required]

ETL
---
The ``adf.etl`` module can be executed by the user using a dedicated CLI. It is parsing raw files received from
specific data sources in order to check that format, structure, dates etc. are compliant to build a relevant data
history. ETL is launched by the orchestrator but can also be launched on-demand. To do so, the following command
will have to be executed from the remote machine, updating ENV and arguments if necessary:

.. code-block:: bash

    ENV=local adf etl run -c france -s 2014-01-01 -e 2019-12-31

The arguments in the ``adf etl run`` are as follows:

  -c, --country TEXT   [required]
  -d, --division TEXT  [required]
  -s, --source TEXT    [optional]

Modelling
---------
The ``modelling`` contains all the libraries needed to execute the full modeling pipeline from data preparation to
forecasts.

Data Preparation
****************
The ``adf.modelling.dataprep`` module is specific to each CBU. It will prepare the data based on specific business
rules (data cleaning, replacement, filtering, aggregation, merges and so on). It is launched by the
orchestrator but can also be launched on-demand. To do so, the following command will have to be executed from
the remote machine, updating ENV and arguments if necessary:

.. code-block:: bash

    ENV=local adf modelling dataprep --country france --division waters --product_level fu --location_level national
    --aggregate_iri False --product_splits 14 --until_weekday 2

The arguments in the ``adf modelling dataprep`` are as follows:

  --country TEXT                  Country on which to perform dataprep [required]
  --division TEXT                 Division (waters, dairy) on which to perform dataprep [required]
  --product_level                 Product level to generate output (sku or fu) [sku|fu] [required]
  --location_level                Location level to generate output [national/regional/departmental/warehouse] \
                                  [required]

Preprocessing
*************
The ``adf.modelling.preprocessing`` module is generic. The specificities of each CBU will be handled using a
dedicated configuration file that will be used by this module to perform the expected processing. It will
generate a preprocessed data set with new generated features that will be used later for the training. It is
launched by the orchestrator but can also be launched on-demand. To do so, the following command will have to
be executed from the remote machine, updating ENV and arguments if necessary:

.. code-block:: bash

    ENV=local adf modelling preprocessing --config_path path/to/config/file --input_path path/to/input/data

The arguments in the ``adf modelling preprocessing`` are as follows:

  --config_path PATH    Path to preprocessing configuration file [required]
  --input_path PATH     Path to input file (.csv or .parquet.gzip) [required]
  --input_bucket TEXT   Bucket of the input file [optional]
  --input_blob TEXT     Blob of the input file [optional]

Training
********
The ``adf.modelling.training`` module is generic. The specificities of each CBU will be handled using a dedicated
configuration file that will be used by this module to perform the expected training with the specified features
in the built model. It will generate a machine learning model (stored in a pickle format). It is launched by the
orchestrator but can also be launched on-demand. To do so, the following command will have to be executed from the
remote machine, updating ENV and arguments if necessary:

.. code-block:: bash

    ENV=local adf modelling training --config_path path/to/config/file --preprocessing_id 005e81ee
    --filters_path path/to/filters/file --forecast

The arguments in the ``adf modelling training`` are as follows:

  --config_path PATH                path to training configuration file  [required]
  --preprocessing_id TEXT           id of preprocessing that contains data for training  [required]
  --filters_path PATH               path to filter file used on training/forecast [optional]
  --splits_path PATH                path used for splitting preprocessing data [optional]
  --forecast--no-forecast BOOL      whether to perform the forecast step or not [optional]
  --kpis, --no-kpis BOOL            whether to calculate performance KPIs for forecasting step [optional]
  --backtest, --no-backtest BOOL    whether to calculate Shap values on the backtest period [optional]


Forecast
********
The ``adf.modelling.forecast`` module is generic. The specificities of each CBU will be handled using a dedicated
configuration file that will be used to perform the expected forecasts on the specified prediction
and target dates and generate a results' table. It is launched by the orchestrator but can
also be launched on-demand. To do so, the following command will have to be executed from the remote machine,
updating ENV and arguments if necessary:

.. code-block:: bash

    ENV=local adf modelling forecast --config_path path/to/config/file --training_id 0f5e81re
    --input_mount /dbfs/mnt/exploration/advanced_forecasting/data
    --input_blob dev/modelling/preprocessing/005e81ee-492c-4b13-88ac-d1d1bc13eab7.parquet.gzip
    --filters_path path/to/filters/file

The arguments in the ``adf modelling training`` are as follows:

  --config_path PATH        path to forecast configuration file
  --training_id TEXT        id of training that contains model for forecast [required]
  --input_mount TEXT        mount of the input file  [required]
  --input_blob TEXT         blob of the input file  [required]
  --filters_path PATH       an optional path to filter file
  --kpis, --no-kpis BOOL    whether to calculate performance KPIs for forecasting step [optional]


Innovation
**********
TODO: add usage

This module is dedicated to the innovation model training and forecasting, the arguments to use are as follows:

For the training:

  --config_path PATH        path to innovation configuration file [required]
  --preprocessing_id TEXT   id of preprocessing that contains data for training [required]

For the forecasting:

  --config_path PATH        path to innovation configuration file [required]
  --training_id TEXT        id of training of the classification model [required]

IBP Export
**********
The ``adf.modelling.ibp_export`` module is generic and will export results into a .csv file in the IBP
tool. This file will contain predictions for all the products, locations and target dates for the specified
forecast IDs we specify. It will be sent to the AWS S3 mount ``/dbfs/mnt/exploration/advanced_forecasting/data``, in the following path:
``<ENV>/modelling/ibp_export/<Country>/<export_date>/``.
It is launched by the orchestrator but can also be launched on-demand. To do so, the following command will have
to be executed from the remote machine, updating ENV and arguments if necessary:

.. code-block:: bash

    ENV=local adf modelling ibp_export --forecast_ids id_1 --forecast_ids id_2 --forecast_ids id_3

The arguments in the ``adf modelling ibp_export`` are as follows:

  --forecast_ids TEXT               Id of forecast(s) results to be exported. This argument can be multiple, \
                                    meaning we can add as many ids as we want      [required]
  --pipeline_id TEXT                If IBP Export comes from a pipeline, please specify its ID [optional]
  --dataprep_id TEXT                If IBP Export doesn't come from a pipeline, please specify the \
                                    dataprep ID [optional]
  --prediction_date TEXT            If forecasts contain several prediction dates, please specify a prediction \
                                    date to keep [optional]
  --prerequired_forecast_ids  TEXT  Id of prerequired forecast(s) needed for disaggregation [optional]
  --backtest, --no-backtest BOOL    Whether to test a disaggregation on the backtest period [optional]


Flows
-----

The ``adf.flows`` module is generic and will enable the user to:

* Send a configuration file and an optional configuration filter to the `pipeline_configs`` table in the database so
  that it will be used by the modeling flow
* Run a calendar flow to launch a calendar data request job
* Run a weather flow to launch a weather data request job
* Run an ETL flow/job
* Run a promotion flow (data preparation of promos data + preprocessing of promos data)
* Run a complete modeling flow (panel data preparation + panel data preprocessing + merge of promos data preprocessing +
  training and forecasting jobs)

1. ``adf flows send_config`` is used to send a configuration file + an optional filters file into the
`pipeline_configs`` table in the database, so that it will be used by the other flows for a given CBU, modelling step,
time base, location and product location. It can be launched by both the orchestrator or on-demand. To do so, the
following command will have to be executed from the remote machine, updating ENV and arguments if necessary:

.. code-block:: bash

    ENV=local adf flows send_config --country <country> --division <division> --step preprocessing --algorithm_type day
    --config_path path/to/config/file --filters_path path/to/optional/filter/file --split_location 11111 --rotation low

The arguments in the ``adf flows send_config`` are as follows:

  --country TEXT                country to which the configuration file is related
  --division TEXT               division to which the configuration file is related
  --step TEXT                   ["preprocessing", "training", "forecast"] modeling step to which the configuration file
                                is related
  --algorithm_type              ["day", "week", "month"]
  --config_path TEXT            local path to configuration file that we need to send into the database
  --filters_path TEXT           local path to filters file that we need to send into the database
  --split_location INTEGER      code of location to which the configuration file is related
  --rotation                    ["high, "low"] product rotation to which the configuration file is related


2. ``adf flows run_weather_flow`` is used to query weather data of previous day for a given CBU. It is launched by
the orchestrator but can also be launched on-demand. To do so, the following command will have to be executed from the
remote machine, updating ENV and arguments if necessary:

.. code-block:: bash

    ENV=local adf flows run_weather_flow --country france --division waters

The arguments in the ``adf flows run_weather_flow`` are as follows:

  --country TEXT        country on which weather data have to be queried
  --division TEXT       division on which weather data have to be queried
  --run_type CHOICE     ["weather_core"] [optional]

3. ``adf flows run_calendar_flow`` is used to query calendar data for a given country. It is launched by
the orchestrator but can also be launched on-demand. To do so, the following command will have to be executed from the
remote machine, updating ENV and arguments if necessary:

.. code-block:: bash

    ENV=local adf flows run_calendar_flow --country france

The arguments in the ``adf flows run_calendar_flow`` are as follows:

  --country TEXT        country on which calendar data have to be queried
  --year_shift INTEGER  indicates the number of years to shift from current year to perform request, \
                        default 0 [optional]
  --depth INTEGER       number of months for which to extract data from current month, including current month, \
                        default 1 [optional]
  --by_zones BOOL       indicates whether extracts must be split by provinces or not, default True [optional]
  --run_type CHOICE     ["calendar_core"] [optional]

4. ``adf flows run_promo_flow`` is used to perform a specific data preparation on promotion date, perform a
preprocessing step with a specific configuration and aggregate results into another data preparation report that will
be used to merge the promotions with the core model dataprep (panel data). It is launched by the orchestrator but can
also be launched on-demand. To do so, the following command will have to be executed from the remote machine, updating
ENV and arguments if necessary:

.. code-block:: bash

  ENV=local adf flows run_promo_flow --country france --division waters --product level fu --location_level national

The arguments in the ``adf flows run_promo_flow`` are as follows:

  --country TEXT                [required]
  --division TEXT               [required]
  --product_level TEXT          ["sku", "fu", "brand"] [required]
  --location_level TEXT         ["national", "regional", "departmental", "warehouse"] [required]
  --pipeline_id TEXT            [optional]
  --from_step CHOICE            ["promo_preprocessing","finalize_promo","retailers_promo","uplifts_promo"] [optional]
  --building_block BOOL         [optional]
  --compute_market_shares BOOL  [optional]
  --run_type CHOICE             ["promo_core"] [optional]

5. ``adf flows run_plc_flow`` is used to query and perform ETL on PLC data for a given country, division and sales \
organisation if many in a single country.

The arguments in the ``adf flows run_plc_flow`` are as follows:

  --country TEXT                [required]
  --division TEXT               [required]
  --sales_org_cod TEXT          [required]
  --run_type CHOICE             ["plc_core"] [optional]

6. ``adf flows run_flow`` is used to run the global modeling pipeline on all data (data preparation on all data
except promotion data, preprocessing on these data, merge previous preprocessing with promotion data preprocessing,
training and forecasting). It is launched by the orchestrator but can also be launched on-demand. To do so, the
following command will have to be executed from the remote machine, updating ENV and arguments if necessary:

.. code-block:: bash

  ENV=local adf flows run_flow

The arguments in the ``adf flows run_flow`` are as follows:

  --country TEXT                    Country on which to run the modeling pipeline [required]
  --division TEXT                   Division (waters, dairy) on which to run the modeling pipeline [required]
  --product_level                   Product level to generate output (sku or fu) [sku|fu] [required]
  --location_level                  Location level to generate output [national/regional/departmental/warehouse] \
                                    [required]
  --run_type CHOICE                 ["core"] [optional]
  --from_step CHOICE                ["preprocessing", "training", "ibp_export"] [optional]
  --to_step CHOICE                  ["dataprep", "preprocessing", "training"] [optional]
  --algorithm_type CHOICE           ["day", "week", "month"] [optional]
  --inno BOOL                       If innovation training and forecast, False by default [optional]
  --backtest, --no-backtest BOOL    Whether to calculate Shap values on the backtest period [optional]

TODO: run_flows
