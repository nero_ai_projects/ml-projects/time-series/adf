
Configurations
::::::::::::::


For most of the steps described above, configuration files in ``.json`` format will have to be used.
The schemas of these files are developed in the ``adf.config`` module. All the configurations used in the
automated workflow for all the steps will be stored in the metadata PostGre database on AWS RDS, in the
``public.pipeline_configs`` table.

Some examples of these files are also available in the ``seeds/modelling_configs/`` repository for the modelling
part (preprocessing, training, forecast).
To get more details on the modeling steps configuration files, you can also report to the following `document
<https://docs.google.com/presentation/d/1Xc18xXYKivdIBf9tASWfEDWkOOTa16SZtCWxYh0-pZw/
edit?ts=5eb01843#slide=id.g751106a437_1_29>`_.


ETL
---
For ETL, configuration files are located in the ``seeds/data_sources/`` repository, with a dedicated space for
each CBU.
There is one data file per data source for each CBU, with the following fields:

* "country" TEXT: country
* "division" TEXT: division
* "source" TEXT: data source name
* "unique" BOOLEAN: is true if the data source does not depend on time. In this case, a new data extract will completely
  replace the previous one. If false, the data source is dependent to time and extracts will be received periodically
  and be added to previous data (period is specified in the "update_frequency" field)
* "parser" DICT: gives some details on how to parse the specific data source (columns to read, rename, format,
  fillna with given values, dropna, types etc.). Below is an example:

.. code-block:: json

    {
        "parser": {
            "threshold": 0.0,
            "read": {
                "sep": ";"
            },
            "columns": [
                {
                    "name": "MAT_COD",
                    "rename": "mat_cod",
                    "dtype": "object",
                    "numeric": {
                        "errors": "coerce",
                        "downcast": "integer"
                    },
                    "dropna": true,
                    "astype": "uint32"
                },
                {
                    "name": "SAL_ORG_COD",
                    "rename": "sales_organization_cod",
                    "dtype": "str"
                },
                {
                    "name": "SAL_CRE_DAT",
                    "rename": "order_creation_date",
                    "dfmt": "%Y-%m-%d %H:%M:%S",
                    "dtype": "object",
                    "datetime": {
                        "errors": "coerce",
                        "format": "%Y-%m-%d"
                    },
                    "dropna": false
                },
            ]
        }
    }

* The "threshold" field specifies a ratio of dropped rows above which an ETL error is sent
* The "read" field is a dictionary in which to specify the "reading" arguments that will be used by the parser to
  read the data source (separator, rows to skip etc.) using the methods from the ``adf.services.storage`` module.
* The "dfmt" is a specification of the raw format of the reference temporal field of the extract. It will only
  be specified for one temporal field used to check temporal gaps and overlaps in the history building. For data
  source that does not depend on time, it will not be specified.

* "landing_mount" TEXT: mount in which the data will be dumped for this data source
* "landing_folder" TEXT: folder in which the country/division/date landing folder will be created for this data source
* "landing_file_prefix" TEXT: prefix that each dumped file name should respect for this data source
* "update_frequency" TEXT: describes the frequency of dumping. Can be "irregular", "daily", "weekly", "monthly"
* "time_column" TEXT, null: specifies the column that will be used as a reference in gaps or overlaps checks in history
  building. This must be a temporal column (date, timestamp etc.). The "dfmt" field found in "parser" json block
  describes the format of this field. If the data source does not depend on time, this field is filled by null.


Preprocessing
-------------
In the preprocessing configuration files, we can identify different blocks.

1. The first block is there to specify metadata used to identify the run:

.. code-block:: json

    {
        "tags": {
            "country": "france",
            "division": "waters"
        }
    }

2. The second block will set the indexes of the data set for the whole pipeline.
All these columns should not be added in the ``features`` section of the configuration file, unless you explicitly want
to use them as features (eg. "fu_cod" with a labeling encoding). It is not necessary to add the time column in the
``features`` section either.

.. code-block:: json

    {
        "product": {
            "column": "fu_cod",
            "level": "fu"
        },
        "location": {
            "column": "national",
            "level": "national"
        },
        "time": {
            "column": "to_dt",
            "level": "week"
        },
        "customer": {
            "column": "nfa_cod",
            "level": "cus_level_1"
        }
    }

3. The ``na_value`` block is used to define whether to fill missing values with specified value or not. If these
values should not be replaced, you will have to put ``null``. If they are to be replaced, you have to indicate
an integer value:

.. code-block:: json

    {
        "na_value": null
    }

4. In the ``aggregation`` block, all the aggregation methods applied to the dataset should be described
explicitly even if not used (leave an empty list in that case):

.. code-block:: json

    {
        "aggregation": {
            "sum": [
                "ordered_volumes"
            ],
            "mean": [
                "rain_intensity",
                "rain_probability",
                "dew_point"
            ],
            "min": [],
            "max": [],
            "first": []
        }
    }

5. In the ``encoding`` block, all the features that will have to be encoded should be listed in either the "label" or
"target" encoding section. Label and target encoding can both be used, but not for the same features. In the example
below, "columnA" is encoded using "ordered_volumes" as target:

.. code-block:: json

    {
        "encoding": {
                "label": [
                    "fu_cod",
                    "national",
                    "mat_type_cod",
                    "flavour_cod",
                    "flavour_desc",
                    "mat_area_cod",
                    "mat_area_desc",
                    "mat_umbrella_cod",
                    "mat_umbrella_desc"
        ],
        "target": [
                {
                    "column": "columnA",
                    "source": "ordered_volumes"
                }
            ]
        }
    }

6. In the ``lags`` blocks, all the backward and forward lags to apply on specified features will be listed.
The "count" field is used to define the final step to lag to. The "step" argument is used to indicate the step
between each lag (see below chapter on preprocessing for details).

.. code-block:: json

    {
        "lags": [
            {
                "column": "ordered_volumes",
                "direction": "backward",
                "count": 11,
                "step": 1
            },
            {
                "column": "ordered_volumes",
                "direction": "forward",
                "count": 2,
                "step": 1
            }
        ]
    }


An optional field is available to handle COVID periods and implement lag jumps.

A pre-requisite is to create a feature to identify the COVID period in the dataprep. This feature must be binary: take 
the value 1 if the day is part of the COVID period, 0 otherwise. It can be named ``is_covid`` for example.

Then, in the preprocessing configuration file, a new field has to be added: ``condition`` in which you must specify:

* the name of the binary variable associated with the COVID (in the ``column`` field)
* the value for which a jump must be made (put the value ``0`` in the ``not_equals`` field)
* the duration of the COVID period in days (in the ``days_jump`` field)

An example is presented below.

.. code-block:: json

    {
        "lags": [
            {
                "column": "ordered_volumes",
                "direction": "backward",
                "count": 11,
                "step": 1,
                "condition": {
                    "column": "is_covid",
                    "not_equals": 0,
                    "days_jump": 35
                }
            }
        ]
    }

It is recommended to exclude the COVID period from the training using the ``exclude_train_intervals`` field.

7. In the ``generating_functions`` block, you will specify all the methods to apply on specific columns to generate
additional features. For more information on how to fill configuration for the different methods with arguments, please
check the ``preprocessing.json`` examples in the ``seeds/modelling_configs/``. The available methods are:

* ``apply_evol``
* ``apply_sum``
* ``apply_diff``
* ``apply_prod``
* ``apply_div``
* ``apply_sin``
* ``apply_cos``
* ``apply_log10``
* ``apply_clip``
* ``apply_iso``
* ``apply_rolling_mean``
* ``apply_rolling_quantiles``
* ``apply_columns_shift``


Training
--------
In the training configuration files, we can identify different blocks.

1. The ``method`` field is used to specify the approach. It can be filled with "propagative" (one model for all the
time horizons to perform forecasting on), or "non_propagative" (one model for each time horizon described in the
configuration file).

2. The ``models`` block will be used to describe the models to build. You can either use one model for all the time
horizons or several ones (one for each time horizon for instance). Hence, the ``models`` value will be a list of
dictionaries, each describing a model.
For each model, you will have to specify:

* the name,
* the algorithm type ("XGBoost", "CatBoost", or "RandomForest"),
* the time horizons (``prediction_steps``) list on which to apply it,
* the ``specific features`` of this model, that will be added to the ``common_features`` listed in another
  configuration block to create the whole set of features for the model,
* the hyper parameters of the model in ``parameters``,
* the ``id`` of the model can be left blank ("") as it will be generated by the adf modelling training execution.

.. code-block:: json

    {
        "models": [
            {
                "name": "week_1",
                "type": "XGBoost",
                "parameters": {
                    "n_estimators": 200,
                    "objective": "reg:tweedie",
                    "tweedie_variance_power": 1.7,
                    "njobs": -1,
                    "colsample_bytree": 1,
                    "learning_rate": 0.05,
                    "max_depth": 5
                },
                "prediction_steps": [
                    0
                ],
                "specific_features": [
                    "ordered_volumes_minus_52"
                    ],
                "id": ""
            },
            {
                "name": "week_2",
                "type": "XGBoost",
                "parameters": {
                    "n_estimators": 200,
                    "objective": "reg:tweedie",
                    "tweedie_variance_power": 1.7,
                    "njobs": -1,
                    "colsample_bytree": 1,
                    "learning_rate": 0.05,
                    "max_depth": 5
                },
                "prediction_steps": [
                    1
                ],
                "specific_features": [
                    "ordered_volumes_minus_4"
                    ],
                "id": ""
            }
        ]
    }

3. The ``learning`` block is used to define if we are performing a classical training, a grid search
or a randomized search.

If we only want to perform a classical training, the type of learning should be null:

.. code-block:: json

    {
        "learning": {
            "type": null,
            "parameters": {}
        }
    }

In case we want to perform a grid search, the type of learning should be "grid_search" or "random_search" and the grid
search or random search parameters must be specified. In addition, the "split" field is mandatory to specify the cross-
validation process. An example is given below for the random search case:

.. code-block:: json

    {
        "learning": {
            "type": "random_search",
            "parameters": {
                "param_distributions": {
                    "n_estimators": [200, 400, 600, 1000],
                    "learning_rate": [0.01, 0.03, 0.05, 0.1, 0.2, 0.3],
                    "max_depth": [3, 4, 5, 6, 7, 8],
                    "min_child_weight": [1, 3, 5, 7, 9],
                    "subsample": [0.5, 0.7, 1],
                    "colsample_bytree": [0.5, 0.7, 1],
                    "tweedie_variance_power": [1.1, 1.3, 1.5, 1.7, 1.9]
                },
                "n_iter": 60,
                "splits": 3
        }
    }

4. The ``target`` block is used to specify the target column of the model (on which to perform training and
forecasting).

.. code-block:: json

    {
        "target": "ordered_volumes"
    }

5. The ``train_dates`` is used to defined the training interval:

.. code-block:: json

    {
        "train_dates": [
            "2017-02-06",
            "2019-08-11"
        ]
    }

6. The ``common_features`` block is used to list the features that will be used by all the models listed in the
configuration file in the ``models`` block. Target column written in the ``target`` must also be present in this block.
However, the "to_dt" (date index) must not be present in the block. Product and location index should be present in the
block only if we want to use them a features for the model (encoded features).

7. The ``predict_from_dates`` blocks contains the list of the view_from dates on which forecasting will be performed
if forecasting is launched after training step.

.. code-block:: json

    {
        "predict_from_dates": [
            "2019-08-14",
            "2019-08-21"
        ]
    }

Forecast
--------
To get further details on how to read and update configuration files for each step, please refer to the following
`document
<https://docs.google.com/presentation/d/1Xc18xXYKivdIBf9tASWfEDWkOOTa16SZtCWxYh0-pZw/
edit?ts=5eb01843#slide=id.g751106a437_1_29>`_


Filters
-------
The filters configuration file is used to include or exclude some indexes among:

* products as "_p"
* locations as "_l"
* dates as "_t"
* customers as "_c"

An example is given below for a filtering on products:

.. code-block:: json

    [
        {
            "column": "__p",
            "type": "include",
            "values": [
                10197,
                10198,
                14610
            ]
        },
        {
            "column": "__p",
            "type": "exclude",
            "values": []
        }
    ]
