Database
::::::::


Metadata
--------

An important concept in the whole ADF pipeline is that each step of the workflow is identified by a unique ID to ensure
traceability. All the associated metadata for each step and run are stored in an AWS RDS PostGre database, in which one
can fin the following tables and information:

* ``alembic_version``: contains the latest version of the database structure
* ``business_units``: contains metadata for each CBU handled by ADF pipeline (id, creation date, country, division,
  sales organization etc.)
* ``data_sources``: contains metadata for each data source ingested by the ADF pipeline for all CBUs (id,
  business_unit_id, landing_mount etc.)
* ``pipeline_configs``: contains metadata for each configuration used by global modeling pipeline (id, step,
  configuration, rotation, country etc.)
* ``pipeline_reports``: contains metadata for each pipeline launched by the orchestrator (id, status, log etc.)
* ``weather_reports``: contains metadata for each weather data file that is extracted from API (id, country, type, date,
  rows etc.)
* ``etl_reports``: contains metadata of each ETL job that is executed (id, execution date, status, etc.)
* ``data_reports``: contains metadata of each data source version after ETL is executed (id, etl_id, status etc.)
* ``dataprep_reports``: contains metadata of each data preparation job that is executed (id, execution date, status,
  etc.)
* ``preprocessing_reports``: contains metadata of each preprocessing job that is executed (id, execution date, status,
  etc.)
* ``training_reports``: contains metadata of each training job that is executed (id, execution date, status, etc.)
* ``forecast_reports``: contains metadata of each forecasting job that is executed (id, execution date, status, etc.)
* ``ibp_reports``: contains metadata of each IBP export job that is executed (id, execution date, status, etc.)


Database diagram
----------------

.. sadisplay::
    :module: adf.services.database