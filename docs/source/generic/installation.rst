Installation guide
::::::::::::::::::


Local environment
-----------------
The project can be accessed via your local environment at first.

If you are using Linux, please follow the steps in the readme document found in the
`github repository <https://github.com/danone/onesource.daai.cor.advanced-demand-forecasting>`_

If you are using Windows, please clone the project and refer to the tutorial
`here <https://github.com/danone/daai.data-science-best-practices/blob/master/tutorials/virtual_env.md#setup>`_ to setup
a virtual environment in Windows.

Remote environment
------------------
Remotely, the project is hosted on a AWS ssh machine that connects to the data.
(explain in a bit more detail how it's structured)

The steps for the remote environment and configuring the AWS environment are also in the
`readme <https://github.com/danone/onesource.daai.cor.advanced-demand-forecasting>`_. But before cloning the project, make
sure you have a public key associated with your github account, otherwise, you can generate one using
`this link <https://docs.github.com/en/github/authenticating-to-github/
generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent>`_.

Once the key in generated, it should be copied into ``~/.ssh/authorized_keys``, then set ``~/.ssh/config`` to:

.. code-block:: python

    Host github.com
    Hostname ssh.github.com
    ForwardAgent yes
    Port 443.

Use the following command on your local machine to setup ssh-agent :

.. code-block:: python

    sh
    ssh-add ~/.ssh/[private_key_path]
    # or on Mac to keep the ssh-agent configuration
    ssh-add -K ~/.ssh/[private_key_path]

You can now follow the steps in the readme. You might encounter an error while running the ``make install``, make sure
you upgrade ``pip`` and ``setuptools`` to solve the issue.