Main Steps
::::::::::


Data preprocessing
------------------
The preprocessing is basically the feature engineering and is the last step
before building the models.

It takes as input:

1) A configuration file describing the data and the processes to do on it
2) An input file path

It first parses the configuration file to get the parameters needed for this
step.

Then it loads the data, does all feature engineering needed and
outputs the prepared data for ML trainings.

Dataframe setup
***************
This step replaces missing values by a ``na_value`` which is a parameter to fill
in the configuration file (default is Null).

Then defines the columns indexes: product x location x time.

After that, it keeps the features specified in the configuration file.

Aggregation
***********
This step takes the input table specified in the preprocessing command and
aggregates it depending on level of granularity:

* Daily     : the table does not change
* Weekly    : aggregation starts from monday
* Monthly   : aggregation starts on the first on each month

Many aggregation options are available and filled in the configuration file.
Sum, mean, minimum, maximum, first value mostly for numerical features and
most frequent value for categorical ones.

Encoding
********
The machine learning models we use only handle numerical values. Categorical
variables thus have to be encoded. These variables are specified in the
configuration file.

In this case, label encoding is performed using the ``LabelEncoder`` or ``OneHotEncoder``
function from scikit-learn. It transforms non-numerical labels to numerical ones.

A target encoder is also available if specified in the configuration.

Lagging
*******
This part of the preprocessing allows to create new lag features based on
the options in the configuration file.

Two types of lags can be generated, forward or backward.
The backward lag shifts the column value in the future using the step defined by
the user.
The forward lag shifts it in the past.

Three main parameters are to be defined in the configuration file to generate
these lags:

* The column name of the variable that we wish to lag.
* The step to use for the lagging. This parameter is that of the ``shift``
  function in pandas.
* The count number determines the number of lags to create for the specified
  variable.

Here is an example for a variable 'test', let's choose a step of 3 and a count
of 2 for a backward lag.
In this case, two lags will be created, the first one will have a shift of 3
and the other of 6. Two new variables will thus be created named ``test_minus_3``
and ``test_minus_6``. The 3 and 6 shifts are computed as the product of a enumerating
factor that goes from one to the count value (1*3, 2*3 in this example).

Had we had a step of 2 for instance with a count of 2, we would have two lags
with 2 and 4 shifts and so on.

The same logic applies to the forward lag, except that the shift is rendered
negative.

~~~~~~~~~~~~~~~~~~~~~~~
**Lag jumps for COVID**
~~~~~~~~~~~~~~~~~~~~~~~

One option to handle COVID period that is part of our training set is to implement lag jumps. 

The idea is to **skip days/weeks that are part of the COVID period** (the period has to be defined for each country).
This is an additional condition to add in the definition of lags that will make that: if the lag falls on a 
day/week that is part of the COVID period, then we will look further in the past to have a value that is outside 
this period. We will pretend that this COVID period does not exist in our data, we will **"jump over it"**.


In the example below, let's say that the COVID period goes from ``01/03/2020`` to ``31/03/2020``. If the lags fall in
this period, we will take the values just before the ``01/03/2020``.

For example, the ``lag minus 1`` for the ``01/04/2020`` originally corresponds to the value for the ``31/03/2020``
(the day before), but 31/03/2020 is part of the COVID period.
Therefore the ``lag minus 1`` will go for the last value that is not part of the COVID period which is ``29/02/2020``. 
The same logic applies for all the lags (minus 2, minus 3, ...)

.. figure:: ../_static/lag_jumps.png
  :width: 800
  :align: center


To include lag jumps in your model, here are the steps to follow:

* Create a feature ``is_covid`` in your dataprep. This feature must be binary: take the value 1 if the day is part of
  the COVID period, 0 otherwise.
* Adapt your preprocessing configuration file with the lag jump option, using the ``condition`` field (more details in
  the documentation dedicated to configurations)
* Exclude the COVID period in your training configuration file, using the ``exclude_train_intervals`` field


Feature generation from functions
*********************************
This part of the feature engineering takes two parameters, the dataframe and a
list of functions to apply on the data in order to generate new features.

So far, many functions are defined. Some are applied to two columns specified in
the configuration while others are applied to one column. Functions involving two
columns are the following:

* Sum, difference, product, evol (1 if first column higher than the second,
  otherwise -1) and evolution which is the difference in percentage
  between two columns (if x is the first and y the second, then the evolution
  would be:

.. math::
    \frac{ a - b }{y}  *  100

The functions that are applied to one column are:

* Cos, sin, log10, rolling means and quantiles among others.

Training
--------
This step consists in training a model on historical data that will be then
used in the next step to produce a forecast on new data.

It takes as mandatory input:

1) A configuration file describing the model and its hyper parameters
2) A preprocessing ID to reference the preprocessed data that'll be used during the training

The following options are available:

1) An optional path to filter file to include or exclude products used on training and forecast
2) An option to exclude some periods from the training data set
3) Whether to perform the forecast step right after the training or not

It first parses the configuration file for getting the parameters needed for the 
training step. Then it loads the data thanks to the preprocessing_id, splits it into
a train and test data set, trains a model on the train set then saves the model. If
the forecast mode is set, it also saves forecast results (see the Forecast section).

Approach
********
It has been decided to go for a non-propagative approach. It means that the model
does not use previous predictions as training data (i.e. in a propagative approach, 
the prediction at horizon week + 2 uses prediction at horizon week + 1 as training
data). Thus, the model only uses data that are known at the time the forecast is
launched, for any horizon. In order to avoid leakage, lag functions are applied
in most of the cases.

.. figure:: ../_static/non_propagative_approach.png
  :width: 800
  :align: center

Models presentation
*******************
Different models have been implemented in the studio: XGBoost, CatBoost and Prophet.

* The first two models are machine learning models. They are both implementations of
  gradient boosting on decision trees. The main difference between these two models is
  that CatBoost handles categorical features whereas with XGBoost these types of features have
  to be encoded. Another benefit of using the CatBoost is the possibility to have the confidence
  interval of the prediction.
* On the other hand, Prophet is an algorithm developed by Facebook to forecast time series
  data based on an additive model where non-linear trends are fit with yearly, weekly, and
  daily seasonality, plus holiday effects. It works best with time series that have strong
  seasonal effects and several seasons of historical data. It can also use additional features
  as regressors (such as weather, SASS, promotion, ...).

The chosen model and its hyper parameters have to be specified in the ``model`` section of
the training's configuration file.

Process
*******
As mentioned before, in this training part the first step is to split the data in two
subsets: one used to train the model (training set), and one to evaluate the model's
performance (test set). The default split ratio is 80% for the training set and 20% for
the test set.

During the phase of model design and optimization, the test set corresponds to
historical data for which the real orders are known. This allows to compute model
performance and optimize the model if needed. It may happen that the test set is empty 
(see below).

The set-up of train and test sets is done within the training configuration file in the
following way:

- The parameter ``train_dates`` is used to define the time boundaries of the train set.
  It is a list of two string elements, the first one corresponding to the start date and the
  second one to the end date. Example:

.. code-block:: python

    "train_dates": [
      "2014-04-14",
      "2018-09-12"
    ]

- For the test set, it consists in the remaining available data that goes beyond the
  training end date. Thus if all the available data is used for training (i.e. if the training
  end date is the last date in our training dataset), the test will be empty and the model will
  not be evaluated during the training step.

In order to improve the model's performance, hyper parameters' optimization is an important
leverage point. Two different methods have been implemented to do this, grid search and
random search:

* Grid search consists in giving several values for different parameters and for each
  combination, training a model and scoring on the test set. In this approach, every
  combination of hyper parameter values is tried which can be very long and costly.
* Randomized search consists in setting up a grid of hyper parameter values and selecting
  random combinations to train the model and score. This allows to explicitly control the
  number of parameter combinations that are attempted. The number of search iterations is
  set based on time or resources.

In both of these methods, the cross-validation used is the `Time series split 
<scikit-learn.org/stable/modules/generated/sklearn.model_selection.
TimeSeriesSplit.html>`_ which is a variation of the K-fold cross-validation adapted to
time series and the fact that observations in a time series are not independent.

The choice between these methods is once again defined in the training configuration
file in the ``learning`` section.

Model evaluation
****************
After training the model, we evaluate it in two steps if the grid search or
randomized search is used, otherwise it is evaluated on the test set at the end.
In both cases, the evaluation metric is the default one depending on the chosen objective
function (for France Waters for instance, we used a Tweedie regression with log-link), unless another metric is
specified in the configuration file. For example:

.. code-block:: json

    {
        "scoring": "neg_root_mean_squared_error"
    }


Forecasting
-----------
This step consists in producing forecasts for given dates and time horizons using a model
that has been previously trained on historical data.

It takes as mandatory input:

1) A configuration file describing the forecast scope
2) A training ID, to reference the model that will be used to forecast
3) The mount of the input file (preprocessed data)
4) The blob (path) of the input file (preprocessed data)
5) An optional path to filter file to include or exclude products, locations etc. used
   for the forecast

It first parses the configuration file to get the parameters needed for the forecast step
and gets the ML model to run that is referenced by the training id. After that, it loads the
data with the mount name and blob path, then generates forecasts which are saved at a
result forecast directory path.

Set-up
******
The forecast scope is defined through a configuration file that contains two main parameters:

- ``predict_from_dates`` : prediction dates from which to compute the forecast. It is a list
  of dates that should be in the following format ``"year-month-day"`` (e.g.: ``"2019-05-05"``).
  One requirement is that the prediction date(s) must be out of the training scope of the ML
  model used to forecast.

- ``time_step`` : time horizon of the forecast. It corresponds to the number of forecast
  steps to compute from the given prediction date.

Example: given that the time granularity set up in the preprocessing part is daily, 
the prediction date is ``"2019-01-01"`` and the time step is 3, it will compute 
forecasts for the following dates: ``"2019-01-02"``, ``"2019-01-03"``, ``"2019-01-04"``).

Process
*******
Once the prediction dates have been validated, the process starts to 
scope the panel data by filtering out data that is not available
(e.g.: if a forward lag of 2 weeks is applied, it is assumed we have data
for the next two weeks, but everything after that is dropped). 

Then, for each step (i.e.: defined by the ``time_step`` parameter), it 
reruns the preprocessing part, such as lag and feature generation.
Finally, it updates the panel data with newly generated forecasts for
the next iteration.

Once the forecasts have been generated for all the prediction dates, the
results are concatenated and formatted in order to create the final output 
table. The formatting consists in:

- renaming some columns (product code, date, location code)
- adding week and day gaps information (i.e.: adding columns describing the gap in days and
  in weeks between the base predicting date and the target date)
- adding metadata (i.e.: adding information about the forecast scope such as the report id,
  the execution date, the product, location and time levels, the country and the division)
- normalizing the columns (i.e.: ensuring columns conform to standard defined in configuration)

The last step consists in computing several metrics on the forecasts: error, 
accuracy and bias. They are all integrated in the final output table. 

Evaluation
**********
*Forecast Accuracy at Danone* is defined as follows:

.. figure:: ../_static/danone_forecast_accuracy_formula.jpg
  :width: 500
  :align: center

During the training phase and to ensure the model's stability, the predictions are made on numerous
dates depending on the time granularity set during the preprocessing. For example, if we are
predicting on a weekly level, the model will train on available data (from "2014-04-14" to "2018-09-12"
for instance) then predict on the following weeks until august of the next year. This will allow to test
the model on different periods and generate a forecast accuracy that would take all these predictions
into account. The final forecast accuracy is aggregated using all predictions made on the weeks until
the year after.

The output file generates per product, location and date a demand prediction along with the ground truth
which is the real demand. Forecast accuracy is then computed with these values using the formula above.

IBP Export
-----------
This step consists in producing the final results for them to be ingested by SAP IBP demand planning tool at Danone.

It takes as mandatory input a list of forecasts reports ids, and uses them to generate the final results.

Below are the main steps of this module:

1) It loads the forecasts reports based on provided ids and loads associated forecasts data
2) It disaggregates weekly forecasts into daily forecasts in case input forecasts granularity is not daily
3) It build the ibp_export reports and loads the data into blob storage container
