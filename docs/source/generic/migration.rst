Migration guide
::::::::::::::::::

Originally developed using `pandas <https://github.com/pandas-dev/pandas>`_, the algorithms were then parallelized using
`dask <https://github.com/dask/dask>`_ from
`PR #48 <https://github.com/danone/onesource.daai.cor.advanced-demand-forecasting/pull/48>`_
and then `koalas <https://github.com/databricks/koalas>`_
from `PR #359 <https://github.com/danone/onesource.daai.cor.advanced-demand-forecasting/pull/359>`_.

The syntax of both `dask` and `koalas` both provide compatibility with the `pandas` syntax, letting data scientists and
data engineers work with a common syntax
during both design and industralization phases of algorithms.

- `Dask comparison to Spark <https://docs.dask.org/en/stable/spark.html>`_
- `Pandas API on Spark vs PySpark DataFrame API <https://spark.apache.org/docs/3.2.0/api/python/user_guide/pandas_on_spark/faq.html#should-i-use-pyspark-s-dataframe-api-or-pandas-api-on-spark>`_

Migration from AWS to Azure DatabricksS
--------------------------------------

A few words about
- Snowflake, Parquet
- S3, ADLS

Databricks cluster configuration
-------------------------------

- ``spark.executor.memory``: amount of memory allocated for each executor  within the JVM memory heap
- ``spark.databricks.adaptive.autoOptimizeShuffle.enabled``: whether AQE should adjusts the shuffle partition number automatically

Migration from pandas to koalas
-------------------------------

`Koalas best practices <https://koalas.readthedocs.io/en/latest/user_guide/best_practices.html>`_

Syntax details:

- Load your input dataframe to Koalas early in your code if possible. Parallel collections ensure your Spark tasks will be correctly distributed in the cluster. Usage of ``ks.from_pandas`` is therefore discouraged when the Pandas dataframe is heavy. Use ``get_snowflake_df_spark()`` and ``storage.read_parquet(object_type = 'koalas')`` to read data.
- `ks.broadcast() <https://koalas.readthedocs.io/en/latest/reference/api/databricks.koalas.broadcast.html>`_, `pyspark.broadcast() <https://spark.apache.org/docs/3.1.1/api/python/reference/api/pyspark.sql.functions.broadcast.html>`_ are good hints to perform respectively ``df.merge()`` or Spark SQL joins.
- ``df.groupby.apply()`` takes a Pandas udf as an argument, there is a trick with the return type of the udf function, make sure *not to declare any annotation* instead of declaring ``pd.DataFrame``. In fact the koalas code will whether interpret full schema annotations such as ``pd.DataFrame[float, str]`` or infer a schema when *no annotation is provided*.
- try to replace the calls to ``df.groupby.apply()`` by ``df.groupby.agg()`` and ``df.merge()`` to only use Koalas dataframes.
- ``numpy`` instructions will likely cause runtime errors while running on Spark, please use native `pandas`-`koalas` compatible calls when possible.

- ``ks.set_option('compute.ops_on_diff_frames', True)`` is needed when performing Pandas-like legacy call if you perform addition on your dataframes, especially around ``df.apply()`` instructions. Use Python context manager to isolate these and mark the code for rewrite later.
- Use ``ks.set_option('compute.default_index_type', 'distributed')`` at the beginning of your Spark task, in the `def run()`.

General considerations
-------------------------------
- Perform most heavy dataprep in SQL with Snowflake if possible. The virtual datawarehouses in Snowflake already provide good computing resources and are as powerful as Spark without configuration management. If your input mainly of Snowflake tables and views, it is better to perform SQL queries. Basic SQL queries and `SQL Functions <https://docs.snowflake.com/en/sql-reference/intro-summary-operators-functions.html>`_ is likely more maintainable than Pandas code, and provide enough for most use cases.
- More unit tests are needed to avoid having to debug parts of code in Databricks. This will passively performs checks for types, expected output and dynamically generated columns but also available operators in Koalas and not in Pandas).
- Snowflake also introduced Snowpark lately allowing to run spark jobs on the Snowflake datawarehouses, and it is compatible with Pandas syntax.

Upgrading from Apache Spark 3.1 to 3.2
--------------------------------------

The project currently runs on Apache Spark 3.1.2 using Databricks Runtime 9.0.

Koalas supports Apache Spark 3.1 and is officially included to PySpark in Apache Spark 3.2
- https://pypi.org/project/koalas/
- https://spark.apache.org/docs/3.2.0/api/python/user_guide/pandas_on_spark/

Databricks released on October 21 a new runtime supporting Spark 3.2
- https://docs.microsoft.com/en-us/azure/databricks/release-notes/runtime/releases
- https://spark.apache.org/docs/3.2.0/api/python/reference/pyspark.pandas/frame.html


Method mapping

+------------+------------+-----------------+---------+----------+
| Pandas     | Koalas     | Pandas on Spark | PySpark | Comments |
+============+============+=================+=========+==========+
| `df.merge() <https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.merge.html>`_ | `df.merge() <https://koalas.readthedocs.io/en/latest/reference/api/databricks.koalas.DataFrame.merge.html>`_ | `df.merge() <https://spark.apache.org/docs/3.2.0/api/python/reference/pyspark.pandas/api/pyspark.pandas.DataFrame.merge.html>`_ | `df.join() <https://spark.apache.org/docs/3.2.0/api/python/reference/api/pyspark.sql.DataFrame.join.html>`_ | |   
+------------+------------+-----------------+---------+----------+
| `df.loc() <https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.loc.html>`_ | `df.loc() <https://koalas.readthedocs.io/en/latest/reference/api/databricks.koalas.DataFrame.loc.html`_ | `df.loc() <https://spark.apache.org/docs/3.2.0/api/python/reference/pyspark.pandas/api/pyspark.pandas.DataFrame.loc.html>`_ | `df.filter() <https://spark.apache.org/docs/3.2.0/api/python/reference/api/pyspark.sql.DataFrame.filter.html>`_ | |
+------------+------------+-----------------+---------+----------+
| `df.groupby.apply() <https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.core.groupby.GroupBy.apply.html>`_ | `df.groupby.apply() <https://koalas.readthedocs.io/en/latest/reference/api/databricks.koalas.groupby.GroupBy.apply.html>`_ | `df.groupby.apply() <https://spark.apache.org/docs/3.2.0/api/python/reference/pyspark.pandas/api/pyspark.pandas.groupby.GroupBy.apply.html>`_ | `df.groupBy() <https://spark.apache.org/docs/3.2.0/api/python/reference/api/pyspark.sql.DataFrame.groupBy.html>`_ | |
+------------+------------+-----------------+---------+----------+
| `df.pipe() <https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.pipe.html>`_ | `df.pipe() <https://koalas.readthedocs.io/en/latest/reference/api/databricks.koalas.DataFrame.pipe.html>`_ | `df.pipe() <https://spark.apache.org/docs/3.2.0/api/python/reference/pyspark.pandas/api/pyspark.pandas.DataFrame.pipe.html>`_ | `df.transform()`` <https://spark.apache.org/docs/latest/api/python/reference/api/pyspark.sql.DataFrame.transform.html>`_ | | 
+------------+------------+-----------------+---------+----------+
| | | | `df.select() <https://spark.apache.org/docs/3.2.0/api/python/reference/api/pyspark.sql.DataFrame.select.html>`_ | |
+------------+------------+-----------------+---------+----------+
| | | | `df.drop() <https://spark.apache.org/docs/latest/api/python/reference/api/pyspark.sql.DataFrame.drop.html>`_ | |
+------------+------------+-----------------+---------+----------+
