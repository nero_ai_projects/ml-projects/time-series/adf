Exploratory Data Analysis
=========================

.. toctree::
    :maxdepth: 4
    :glob:

    general_eda
    orders_eda