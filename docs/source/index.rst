Danone Advanced Demand Forecasting
==================================


General presentation
::::::::::::::::::::


What is the Advanced demand Forecasting project ?
-------------------------------------------------

    - Our ambition is to provide Demand Planners with machine learning algorithms to improve forecasts accuracy for
      short term predictions (W-0 to W-14)

    - The algorithms leverage historical data to forecast water or dairy sell-in volumes

    - The Machine Learning algorithms are built within ADF Studio, and forecasts are made available within IBP, the
      Demand Planners forecasting tool

    - ADF Studio is an in-house forecasting solution meant for Data Scientists. It is designed to be a highly evolutive
      and customizable tool. It consists in a pipeline including all necessary steps to generate a forecast: from data
      ingestion & pre-processing, machine learning model adjustments, model training and forecast generation at any
      granularity


How it started
--------------

    - The project started in the beginning of 2020, with the development of the core model for 2 pilot CBUs:
        - France for water
        - Spain for dairies


Main goal
---------

    - The main goal is to improve the forecast accuracies compared to demand planners forecasts
    - The second goal is to make the forecast understandable by the demand planners, so that they re-work them and add
      value to the machine proposition (building blocks, uncertainty, granularities...)

Main KPI
--------

Forecast accuracy

.. figure:: _static/danone_forecast_accuracy_formula.jpg
  :width: 500
  :align: center

Main steps
----------

    - ETL: Collect data on the mount where it is dropped, apply basic format transformation, manage versioning and drop
      the obtained file on a new mount

    - Data preparation: Apply all data preparation required to filter the data, match the sources together, create new
      calculated columns to generate additional value, and finally create an input table with only the interesting
      information for feature engineering, and at the granularity required for the model

    - Preprocessing and feature engineering: Apply generic preprocessing and feature engineering thanks to json files
      to decide on the model configuration



Table of Content
::::::::::::::::

.. toctree::
    :maxdepth: 8
    :glob:

    generic/installation
    generic/main_steps
    generic/database
    generic/configurations
    generic/tutorials/__init__
    generic/cli_documentation
    generic/migration
    modules

