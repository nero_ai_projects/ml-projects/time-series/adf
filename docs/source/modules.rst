Code and rollouts documentation
=======================================

Welcome to the ADF Project

This project allows you to generate sell-in forecasts from data provided by Danone's teams

Don't hesitate to post issues on our Github repository if you are confronting difficulties.

You'll find in this nomenclature the code modules documentation.

Please navigate in the adf/modelling/dataprep/{country} to find a country roll-out documentation, which will
describe the pipeline workflow and different business rules and features that are applied in the code.


.. toctree::
   :maxdepth: 8

   apidoc/adf
