# Databricks notebook source
import logging
from pyspark.sql import SparkSession
from pyspark.dbutils import DBUtils

from adf.flows.flow import run_calendar_flow


spark = SparkSession.builder.getOrCreate()
dbutils = DBUtils(spark)
displayHTML = locals().get("displayHTML")
logging.getLogger("py4j").setLevel(logging.ERROR)

# COMMAND ----------

country = dbutils.widgets.get("Country")

try:
    year_shift = dbutils.widgets.get("Year Shift")
except Exception:
    year_shift = 0
try:
    depth = dbutils.widgets.get("Months Depth")
except Exception:
    depth = 1
try:
    by_zones = dbutils.widgets.get("Split By Zones")
except Exception:
    by_zones = False

run_calendar_flow(
    country=country,
    year_shift=int(year_shift),
    depth=int(depth),
    by_zones=by_zones,
    run_type="calendar_core"
)
