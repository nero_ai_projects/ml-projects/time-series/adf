# Databricks notebook source

import os
import logging
import alembic.config
from adf.utils.imports import import_method
from adf.services.database.seeder import main


logging.getLogger("py4j").setLevel(logging.ERROR)
ADF_PATH, ADF_ERROR = None, None

# COMMAND ----------

# MAGIC %%bash --out ADF_PATH --err ADF_ERROR
# MAGIC pip show adf | grep "Location" | awk '{print $2}'

# COMMAND ----------

if ADF_ERROR:
    raise Exception(ADF_ERROR)

# COMMAND ----------

# MAGIC %cd $ADF_PATH

# COMMAND ----------

results = {}
for module in os.listdir(
    os.path.join(os.path.dirname(os.path.abspath(__name__)), "migrations/versions/")
):
    if module[-3:] == ".py":
        revision = import_method(f"migrations.versions.{module[:-3]}", "revision")
        down_revision = import_method(f"migrations.versions.{module[:-3]}", "down_revision")
        if down_revision not in results:
            results[down_revision] = []
        results[down_revision].append(revision)
if any([len(val) > 1 for val in results.values()]):
    raise Exception("2 migrations have the same down revision, please fix !")

# COMMAND ----------

alembicArgs = [
    '--raiseerr',
    'upgrade', 'head',
]
alembic.config.main(argv=alembicArgs)

# COMMAND ----------

main()
