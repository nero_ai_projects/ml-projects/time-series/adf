# Databricks notebook source
import logging
from sqlalchemy import desc
import datetime

from pyspark.sql import SparkSession
from pyspark.dbutils import DBUtils

from adf.services.database import get_session
import adf.services.database as db
from adf.config import (
    MOUNT,
    IBP_INTERFACE_MOUNT,
    IBP_EXPORT_FORECAST_ST_SUFFIX,
    IBP_EXPORT_SELLOUT_SUFFIX,
    load_config
)
from adf.services.storage import read_csv
from adf.modelling.ibp_export.tools.utils import (
    upload_ibp_export_to_snowflake,
    upload_sellout_export_to_snowflake
)

spark = SparkSession.builder.getOrCreate()
dbutils = DBUtils(spark)
logging.getLogger("py4j").setLevel(logging.ERROR)


# COMMAND ----------

sales_org = dbutils.widgets.get("Sales Organization")
today = datetime.datetime.now()
year, month, day = str(today.year).zfill(4), str(today.month).zfill(2), str(today.day).zfill(2)


# COMMAND ----------

session = get_session()

try:
    business_unit = (
        session
        .query(db.BusinessUnits)
        .filter(db.BusinessUnits.sales_org_cod == str(int(sales_org)).zfill(2))
        .first()
    )
    country, division, business_unit_id = (
        business_unit.country,
        business_unit.division,
        business_unit.id
    )
    ibp_report = (
        session
        .query(db.IBPReports)
        .filter(db.IBPReports.business_unit_id == business_unit_id)
        .filter(db.IBPReports.pipeline_run_type == "core")
        .filter(db.IBPReports.status == "success")
        .order_by(desc(db.IBPReports.updated_at))
        .first()
    )
    ibp_report_id = ibp_report.id
except Exception as e:
    session.rollback()
    raise Exception(e)

# Load ibp_export file of the day, from ADLS

output = load_config("ibp_export/output.json")
dtypes = {c["rename"]: c["dtype"] for c in output}

export_df = (
    read_csv(
        MOUNT,
        f"dev/modelling/ibp_export/"
        f"{country.capitalize()}/{today.strftime('%Y%m%d')}/"
        f"{sales_org}{IBP_EXPORT_FORECAST_ST_SUFFIX}.csv",
        infer_schema=False,
        sep=";"
    )
    .astype(dtypes, errors="ignore")
    .replace({"None": None})
)

# Write results into IBP_EXPORTS table in Snowflake

upload_ibp_export_to_snowflake(
    export_df, ibp_report
)

# Transfer .csv file from ADF ADLS container into ADF-IBP-INTERFACE ADLS container

# If we have several prediction dates (backtest), keep max only and delete prediction date notion
# to meet required format
export_df = export_df.loc[
    export_df["FORECASTED_FROM"] == export_df["FORECASTED_FROM"].max()
]
export_df = export_df.drop(columns=["FORECASTED_FROM"])

# Create target directory
dbutils.fs.mkdirs(f"{IBP_INTERFACE_MOUNT}/{country}/{division}/{year}/{month}/{day}")

# Write .csv flat file
export_df.to_csv(
    f"/dbfs{IBP_INTERFACE_MOUNT}/{country}/{division}/{year}/{month}/{day}/"
    f"{sales_org}{IBP_EXPORT_FORECAST_ST_SUFFIX}.csv",
    index=False,
    sep=";"
)

# COMMAND ----------

# Sellout module for France Waters

if sales_org == "0046":

    session = get_session()

    try:
        business_unit = (
            session
            .query(db.BusinessUnits)
            .filter(db.BusinessUnits.sales_org_cod == str(int(sales_org)).zfill(2))
            .first()
        )
        country, division, business_unit_id = (
            business_unit.country,
            business_unit.division,
            business_unit.id
        )
        sellout_ibp_report = (
            session
            .query(db.SelloutIBPReports)
            .filter(db.SelloutIBPReports.business_unit_id == business_unit_id)
            .filter(db.SelloutIBPReports.status == "success")
            .order_by(desc(db.SelloutIBPReports.updated_at))
            .first()
        )
        sellout_ibp_report_id = sellout_ibp_report.id

    except Exception as e:
        session.rollback()
        raise Exception(e)

    # Load ibp_export file of the day, from ADLS
    sellout_dtypes = {
        "sales_organization_cod": "str",
        "fu_cod": "str",
        "rfa_cod": "str",
        "to_dt": "datetime64[D]",
        "sellout": "float"
    }

    sellout_export_df = (
        read_csv(
            MOUNT,
            f"dev/modelling/ibp_export/"
            f"{country.capitalize()}/{today.strftime('%Y%m%d')}/"
            f"{sales_org}{IBP_EXPORT_SELLOUT_SUFFIX}.csv",
            infer_schema=False,
            sep=";"
        )
        .astype(sellout_dtypes, errors="ignore")
        .replace({"None": None})
    )

    # Write results into IBP_EXPORTS table in Snowflake

    upload_sellout_export_to_snowflake(
        sellout_export_df, sellout_ibp_report
    )

    # Transfer .csv file from ADF ADLS container into ADF-IBP-INTERFACE ADLS container

    # Write .csv flat file
    export_df.to_csv(
        f"/dbfs{IBP_INTERFACE_MOUNT}/{country}/{division}/{year}/{month}/{day}/"
        f"{sales_org}{IBP_EXPORT_SELLOUT_SUFFIX}.csv",
        index=False,
        sep=";"
    )
