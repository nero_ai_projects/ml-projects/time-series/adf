# Databricks notebook source

import logging
from pyspark.sql import SparkSession
from pyspark.dbutils import DBUtils
from datetime import timedelta, date
import pandas as pd
import numpy as np
import shap
from sqlalchemy import desc
from adf.services.database import get_session, PipelineReports, BusinessUnits
from adf.modelling.tools import find_all_features
from adf.enums import Indexes
from adf.flows.flow import run_multi_adf_flows, run_adf_flow
from adf.services import database as db
from adf.modelling.mllogs import load_model


spark = SparkSession.builder.getOrCreate()
dbutils = DBUtils(spark)
displayHTML = locals().get("displayHTML")
logging.getLogger("py4j").setLevel(logging.ERROR)

# COMMAND ----------

country = dbutils.widgets.get("Country")
division = dbutils.widgets.get("Division")
product_level = dbutils.widgets.get("Product Level")
location_level = dbutils.widgets.get("Location Level")
try:
    algorithm_type = dbutils.widgets.get("Algorithm Type")
except Exception:
    algorithm_type = None
try:
    pipeline_id = dbutils.widgets.get("Pipeline ID")
except Exception:
    pipeline_id = None
try:
    from_step = dbutils.widgets.get("From Step")
except Exception:
    from_step = None
try:
    to_step = dbutils.widgets.get("To Step")
except Exception:
    to_step = None

if not algorithm_type or algorithm_type == "auto":
    run_multi_adf_flows(country, division, product_level, location_level, "core")
else:
    run_adf_flow(
        country, division, product_level, location_level, "core", algorithm_type,
        pipeline_id=pipeline_id, from_step=from_step, to_step=to_step
    )

# COMMAND ----------

session = db.get_session()
bu = session.query(db.BusinessUnits) \
    .filter(db.BusinessUnits.country == country) \
    .filter(db.BusinessUnits.division == division) \
    .first()
ibp = session.query(db.IBPReports) \
    .filter(db.IBPReports.business_unit_id == bu.id) \
    .filter(db.IBPReports.pipeline_run_type == "core") \
    .filter(db.IBPReports.status == "success") \
    .order_by(desc(db.IBPReports.created_at)) \
    .first()

# COMMAND ----------

df = ibp.download(sep=";")

# COMMAND ----------

df["week"] = pd.to_datetime(df.TIME).dt.weekofyear
df.groupby("week").Total.sum()

# COMMAND ----------

shap.initjs()

# COMMAND ----------

sample_size = 10
products = []
customers = []

# COMMAND ----------


def load_latest_model_and_data(country, division):
    session = get_session()
    business_unit = session.query(BusinessUnits) \
        .filter(BusinessUnits.country == country) \
        .filter(BusinessUnits.division == division) \
        .first()
    pipeline_report = session.query(PipelineReports) \
        .filter(PipelineReports.run_type == "core") \
        .filter(PipelineReports.dataprep_report.any(business_unit_id=business_unit.id)) \
        .order_by(desc(PipelineReports.created_at)) \
        .first()
    training_reports = [
        training_report
        for training_report in pipeline_report.training_report
        if training_report.description["models"][0]["id"] != ""
    ]
    training_report = sorted(training_reports, key=lambda x: x.created_at, reverse=True)[0]
    preprocessing_report = sorted(
        pipeline_report.preprocessing_report, key=lambda x: x.created_at, reverse=True
    )[0]
    return pipeline_report, preprocessing_report, training_report


def get_force_plot(
    pipeline_report, training_report, preprocessing_report,
    df, horizon=0, model_type="CatBoost", sample_size=20
):
    if pipeline_report.algorithm_type == "week":
        end = date.today() + timedelta(
            days=7 * horizon
        )
        end = end - timedelta(days=end.weekday() - 1)
        start = end - timedelta(
            days=7 * sample_size
        )
        dates = pd.date_range(start, end, freq="W-MON")
        print(dates)
    elif pipeline_report.algorithm_type == "day":
        end = date.today() + timedelta(
            days=horizon
        )
        start = end - timedelta(
            days=sample_size
        )
        dates = pd.date_range(start, end, freq="D")
        print(dates)
    else:
        raise Exception(f"Not available for algorithm_type {pipeline_report.algorithm_type}")
    model = load_model(
        training_report.description['models'][horizon]['id'],
        preprocessing_report.id,
        model_type=model_type
    )
    common_features = training_report.description['common_features']
    specific_features = training_report.description['models'][horizon]['specific_features']
    cols = find_all_features(df, common_features + specific_features)
    data_query = "time_index in [{dates}]{customer_query}{product_query}".format(
        dates=', '.join(['"{}"'.format(value.strftime("%Y-%m-%d")) for value in dates]),
        customer_query=f" and customer_index in {customers}" if customers else "",
        product_query=f" and product_index in {products}" if products else ""
    )
    test_data = df[
        cols + Indexes
    ].sort_values("time_index").query(data_query)[cols + ["time_index"]]
    explainer = shap.TreeExplainer(model)
    shap_values = explainer.shap_values(test_data[model.feature_names_])
    new_shap = []
    curr = 0
    for val in df.loc[test_data.index].groupby("time_index")["product_index"].count().values:
        tmp_shap = np.zeros(len(shap_values[0]))
        for shap_val in shap_values[curr:curr + val]:
            tmp_shap = tmp_shap + shap_val
        new_shap += [list(tmp_shap)]
        curr = curr + val + 1
    new_shap = np.array(new_shap)
    force_plot = shap.force_plot(
        explainer.expected_value,
        new_shap,
        test_data.groupby(
            "time_index", as_index=False
        )[model.feature_names_].sum()[model.feature_names_],
        ordering_keys=test_data.groupby("time_index")[
            model.feature_names_
        ].sum().reset_index()["time_index"].apply(
            lambda x: x.strftime("%Y-%m-%d")
        ),
        ordering_keys_time_format="%Y-%m-%d"
    )
    return f"<head>{shap.getjs()}</head><body>{force_plot.html()}</body>"


# COMMAND ----------

pipeline_report, preprocessing_report, training_report = load_latest_model_and_data(
    country, division
)

# COMMAND ----------

df = preprocessing_report.download(object_type="koalas").to_pandas()
df = df.reset_index()
df[list(df.select_dtypes("category").columns)] = df[
    list(df.select_dtypes("category").columns)
].astype(int)


# COMMAND ----------

df["time_index"] = pd.to_datetime(df.time_index)

# COMMAND ----------

# Do not execute for some CBUs to avoid memory error
if (country, division) not in (("france", "dairy_reg"), ("dach", "dairy_de")):
    try:
        for horizon in range(len(training_report.description.get("models", []))):
            print(f"Model {horizon} :")
            displayHTML(
                get_force_plot(
                    pipeline_report, training_report, preprocessing_report,
                    df, sample_size=sample_size, horizon=horizon
                )
            )
    except Exception as err:
        print(f"Could not render shap values. Error is: {err}")
