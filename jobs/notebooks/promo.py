# Databricks notebook source

import logging
from pyspark.sql import SparkSession
from pyspark.dbutils import DBUtils

from adf.flows.flow import run_promo_flow


spark = SparkSession.builder.getOrCreate()
dbutils = DBUtils(spark)
displayHTML = locals().get("displayHTML")
logging.getLogger("py4j").setLevel(logging.ERROR)

# COMMAND ----------

country = dbutils.widgets.get("Country")
division = dbutils.widgets.get("Division")
product_level = dbutils.widgets.get("Product Level")
location_level = dbutils.widgets.get("Location Level")
try:
    pipeline_id = dbutils.widgets.get("Pipeline ID")
except Exception:
    pipeline_id = None
try:
    from_step = dbutils.widgets.get("From Step")
except Exception:
    from_step = None
try:
    building_block = dbutils.widgets.get("Building Block")
except Exception:
    building_block = True
try:
    compute_market_shares = dbutils.widgets.get("Compute Market Shares")
except Exception:
    compute_market_shares = True
try:
    cannib = dbutils.widgets.get("Cannib")
except Exception:
    cannib = True

run_promo_flow(
    country, division, product_level, location_level,
    pipeline_id=pipeline_id, from_step=from_step,
    building_block=building_block, compute_market_shares=compute_market_shares,
    run_type='promo_core', cannib=cannib
)
