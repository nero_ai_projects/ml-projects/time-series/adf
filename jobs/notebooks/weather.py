# Databricks notebook source
import logging
from pyspark.sql import SparkSession
from pyspark.dbutils import DBUtils

from adf.flows.flow import run_weather_flow


spark = SparkSession.builder.getOrCreate()
dbutils = DBUtils(spark)
displayHTML = locals().get("displayHTML")
logging.getLogger("py4j").setLevel(logging.ERROR)

# COMMAND ----------

country = dbutils.widgets.get("Country")
division = dbutils.widgets.get("Division")

run_weather_flow(
    country=country,
    division=division,
    run_type="weather_core"
)
