# Scripts

## Table of Contents

- [test.sh](#test.sh)
- [user.sh](#user.sh)
- [install.sh](#install.sh)
- [setup.sh](#setup.sh)
- [cleanup.sh](#cleanup.sh)

**make** commands should be launched from root folder.

## test.sh

This script is used to grant SSH access to a new user, it is meant to be launched from inside the instance with root.

```bash
$ make user
 > User: <username>
 > SSH: <public_ssh_key_value>
```

## user.sh

Grant SSH access to a new user, it is meant to be launched from inside the remote instance with root.

```bash
$ make user
 > User: <username>
 > SSH: <public_ssh_key_value>
```

The user can now SSH into the instance:

```bash
ssh <username>@<instance_dns>

# specifying key
ssh -i <key_path> <username>@<instance_dns>

# with ssh agent forwarding
ssh -A <key_path> <username>@<instance_dns>
```

## install.sh

Set up the virtualenv.

```bash
$ make install
```

## setup.sh

Set up docker services.

```bash
$ make setup
```

## cleanup.sh

Remove the virtualenv and docker related content.

```bash
$ make cleanup
```
