#!/bin/bash

read -p ">> Hard reset. Are you sure? " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
  then
    echo "Cancelled"
    exit 0
fi

poetry env remove python3.7

echo "Removing docker contents"
export COMPOSE_PROJECT_NAME=local
docker-compose -f dockerfiles/docker-compose.local.yml down --volumes --rmi local
