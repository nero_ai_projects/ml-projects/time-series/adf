#!/bin/bash

echo "Building virtualenv"
rm -rf venv
poetry env use python3.7
poetry run pip install --upgrade pip
poetry install
