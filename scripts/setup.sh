#!/bin/bash

if [[ $ENV != "local" ]]
  then
    echo "Do not run setup on any environment other than local!"
    exit 1
fi

cowsay "Starting docker services"
docker-compose -f dockerfiles/docker-compose.local.yml up -d

cowsay "Waiting for postgres"
sleep 2
until docker exec -it adf-postgres psql -h localhost -U postgres -c '\q'; do
  sleep 1
done

cowsay "Running ADF migrations"
poetry run alembic upgrade head

cowsay "Seeding ADF database"
poetry run python seeds/seeder.py