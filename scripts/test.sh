#!/bin/bash

export ENV=test
export MLFLOW_TRACKING_URI="postgresql+psycopg2://postgres:postgres@0.0.0.0:5433/mlflow"

echo "Starting test database"
docker run --name adf-test --rm -p 5433:5432 -d postgres:11.6

sleep 2
until docker exec -i adf-test psql -h localhost -U postgres -c '\q'; do
  sleep 1
done
echo "Database is up"

psql -h 0.0.0.0 -p 5433 -U postgres -d postgres -f ./dockerfiles/postgres/init-db.sql

echo "Removing data folder"
rm -rf data/

echo "Starting tests"
set -e
docker run --rm --name gitleaks -v $(pwd):/code/ zricethezav/gitleaks:v7.6.1 --path=/code/ --config-path=/code/gitleaks.toml --no-git
poetry run coverage run -m pytest -p no:warnings --flake8
poetry run mypy -p adf
set +e


echo "Stopping test database"
docker stop adf-test


echo "Removing MLFlow related directories"
rm -rf mlruns
rm -rf tests/modelling/mlruns
rm -rf tests/modelling/preprocessing/mlruns
rm -rf tests/modelling/training/mlruns
echo "Done"
