import json
from jinja2 import Environment, PackageLoader
import os
import requests
from importlib_metadata import version
from adf.config import ENV
from adf.errors import DatabricksAPIRequestError


DATABRICKS_HOST = os.environ["DATABRICKS_HOST"]
DATABRICKS_TOKEN = os.environ["DATABRICKS_TOKEN"]
API_VERSION = '/api/2.0'


def get_policy_mapping():
    try:
        api_command_policies_clusters_list = "/policies/clusters/list/"
        response = requests.get(
            url=f"{DATABRICKS_HOST}{API_VERSION}{api_command_policies_clusters_list}",
            headers={"Authorization": f"Bearer {DATABRICKS_TOKEN}"}
        )
        if response.status_code != 200:
            raise DatabricksAPIRequestError(response.content)
        policies = response.json()["policies"]
        return {
            policy["name"]: policy["policy_id"] for policy in policies
            if "job_only" in policy["name"]
        }
    except Exception as e:
        raise e


POLICY_MAPPING = get_policy_mapping()

JINJA_ENV = Environment(loader=PackageLoader('jobs', 'templates'))

JOB_PREFIX = f"ADF-JOB-{ENV.upper()}-"
ADF_VERSION = version("adf")

JOBS = [
    {
        "job_name": "MIGRATE",
        "min_workers": 0,
        "max_workers": 0,
        "driver_node_type": None,
        "node_type": "Standard_DS3_v2",
        "notebook_path": f"/Advanced forecasting/{ENV}/migrate",
        "adf_version": ADF_VERSION,
        "schedule": None,
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_single_node"],
        "availability": "SPOT_WITH_FALLBACK_AZURE",
    },
    {
        "job_name": "DACH-AT-CALENDAR",
        "min_workers": 0,
        "max_workers": 0,
        "driver_node_type": None,
        "node_type": "Standard_DS14_v2",
        "notebook_path": f"/Advanced forecasting/{ENV}/calendar",
        "notebook_params": {
            "Country": "austria",
            "Year Shift": 1,
            "Months Depth": 12,
            "Split By Zones": True
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 0 12 1 1 ?" if ENV == "prod" else "0 0 12 1 1 ?",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_single_node"],
        "availability": "SPOT_WITH_FALLBACK_AZURE"
    },
    {
        "job_name": "DACH-SI-CALENDAR",
        "min_workers": 0,
        "max_workers": 0,
        "driver_node_type": None,
        "node_type": "Standard_DS14_v2",
        "notebook_path": f"/Advanced forecasting/{ENV}/calendar",
        "notebook_params": {
            "Country": "slovenia",
            "Year Shift": 1,
            "Months Depth": 12,
            "Split By Zones": False
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 0 12 1 1 ?" if ENV == "prod" else "0 0 12 1 1 ?",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_single_node"],
        "availability": "SPOT_WITH_FALLBACK_AZURE"
    },
    {
        "job_name": "DACH-DAIRY-AT-PIPELINE",
        "min_workers": 1,
        "max_workers": 4,
        "driver_node_type": "Standard_E32_v3",
        "node_type": "Standard_E32_v3",
        "notebook_path": f"/Advanced forecasting/{ENV}/pipeline",
        "notebook_params": {
            "Country": "dach",
            "Division": "dairy_at",
            "Product Level": "sku",
            "Location Level": "departmental"
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 30 1 ? * 3" if ENV == "prod" else "0 30 1 ? * 3",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_multi_node_cpu_optimized_special_DS"],
        "availability": "SPOT_WITH_FALLBACK_AZURE",
    },
    {
        "job_name": "DACH-DE-CALENDAR",
        "min_workers": 0,
        "max_workers": 0,
        "driver_node_type": None,
        "node_type": "Standard_DS14_v2",
        "notebook_path": f"/Advanced forecasting/{ENV}/calendar",
        "notebook_params": {
            "Country": "germany",
            "Year Shift": 1,
            "Months Depth": 12,
            "Split By Zones": True
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 0 12 1 1 ?" if ENV == "prod" else "0 0 12 1 1 ?",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_single_node"],
        "availability": "SPOT_WITH_FALLBACK_AZURE"
    },
    {
        "job_name": "DACH-DAIRY-DE-PIPELINE",
        "min_workers": 1,
        "max_workers": 4,
        "driver_node_type": "Standard_E32_v3",
        "node_type": "Standard_E32_v3",
        "notebook_path": f"/Advanced forecasting/{ENV}/pipeline",
        "notebook_params": {
            "Country": "dach",
            "Division": "dairy_de",
            "Product Level": "sku",
            "Location Level": "departmental"
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 30 1 ? * 3" if ENV == "prod" else "0 30 1 ? * 3",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_multi_node_cpu_optimized_special_DS"],
        "availability": "SPOT_WITH_FALLBACK_AZURE",
    },
    {
        "job_name": "DACH-CH-CALENDAR",
        "min_workers": 0,
        "max_workers": 0,
        "driver_node_type": None,
        "node_type": "Standard_DS14_v2",
        "notebook_path": f"/Advanced forecasting/{ENV}/calendar",
        "notebook_params": {
            "Country": "switzerland",
            "Year Shift": 1,
            "Months Depth": 12,
            "Split By Zones": True
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 0 12 1 1 ?" if ENV == "prod" else "0 0 12 1 1 ?",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_single_node"],
        "availability": "SPOT_WITH_FALLBACK_AZURE"
    },
    {
        "job_name": "DACH-DAIRY-CH-PIPELINE",
        "min_workers": 1,
        "max_workers": 4,
        "driver_node_type": "Standard_E32_v3",
        "node_type": "Standard_E32_v3",
        "notebook_path": f"/Advanced forecasting/{ENV}/pipeline",
        "notebook_params": {
            "Country": "dach",
            "Division": "dairy_ch",
            "Product Level": "sku",
            "Location Level": "departmental"
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 30 1 ? * 3" if ENV == "prod" else "0 30 1 ? * 3",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_multi_node_cpu_optimized_special_DS"],
        "availability": "SPOT_WITH_FALLBACK_AZURE",
    },
    {
        "job_name": "FRANCE-CALENDAR",
        "min_workers": 0,
        "max_workers": 0,
        "driver_node_type": None,
        "node_type": "Standard_DS14_v2",
        "notebook_path": f"/Advanced forecasting/{ENV}/calendar",
        "notebook_params": {
            "Country": "france",
            "Year Shift": 1,
            "Months Depth": 12,
            "Split By Zones": False
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 0 12 1 1 ?" if ENV == "prod" else "0 0 12 1 1 ?",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_single_node"],
        "availability": "SPOT_WITH_FALLBACK_AZURE"
    },
    {
        "job_name": "FRANCE-WATERS-WEATHER",
        "min_workers": 0,
        "max_workers": 0,
        "driver_node_type": None,
        "node_type": "Standard_DS14_v2",
        "notebook_path": f"/Advanced forecasting/{ENV}/weather",
        "notebook_params": {
            "Country": "france",
            "Division": "waters",
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 00 1 * * ?" if ENV == "prod" else "0 00 1 * * ?",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_single_node"],
        "availability": "SPOT_WITH_FALLBACK_AZURE",
    },
    {
        "job_name": "FRANCE-WATERS-PIPELINE",
        "min_workers": 1,
        "max_workers": 4,
        "driver_node_type": "Standard_DS15_v2",
        "node_type": "Standard_DS15_v2",
        "notebook_path": f"/Advanced forecasting/{ENV}/pipeline",
        "notebook_params": {
            "Country": "france",
            "Division": "waters",
            "Product Level": "fu",
            "Location Level": "national"
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 20 1 ? * 1,6" if ENV == "prod" else "0 20 1 ? * 1,6",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_multi_node_cpu_optimized_special_DS"],
        "availability": "SPOT_WITH_FALLBACK_AZURE"
    },
    {
        "job_name": "FRANCE-DAIRY-REG-PIPELINE",
        "min_workers": 3,
        "max_workers": 15,
        "driver_node_type": "Standard_E96a_v4",
        "node_type": "Standard_D16s_v3",
        "notebook_path": f"/Advanced forecasting/{ENV}/pipeline",
        "notebook_params": {
            "Country": "france",
            "Division": "dairy_reg",
            "Product Level": "sku",
            "Location Level": "departmental"
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 30 2 ? * 2" if ENV == "prod" else "0 30 2 ? * 2",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_multi_node_cpu_optimized_special_DS"],
        "availability": "SPOT_WITH_FALLBACK_AZURE",
        "executor_cores": 16
    },
    {
        "job_name": "FRANCE-DAIRY-REG-PROMO",
        "min_workers": 3,
        "max_workers": 4,
        "driver_node_type": "Standard_E96a_v4",
        "node_type": "Standard_D64s_v3",
        "notebook_path": f"/Advanced forecasting/{ENV}/promo",
        "notebook_params": {
            "Country": "france",
            "Division": "dairy_reg",
            "Product Level": "sku",
            "Location Level": "departmental"
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 30 2 ? * 7" if ENV == "prod" else "0 30 2 ? * 7",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_multi_node_cpu_optimized_special_DS"],
        "availability": "ON_DEMAND_AZURE",
    },
    {
        "job_name": "FRANCE-DAIRY-VEG-PIPELINE",
        "min_workers": 3,
        "max_workers": 4,
        "driver_node_type": "Standard_E96a_v4",
        "node_type": "Standard_D64s_v3",
        "notebook_path": f"/Advanced forecasting/{ENV}/pipeline",
        "notebook_params": {
            "Country": "france",
            "Division": "dairy_veg",
            "Product Level": "sku",
            "Location Level": "departmental"
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 30 2 ? * 2" if ENV == "prod" else "0 30 2 ? * 2",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_multi_node_cpu_optimized_special_DS"],
        "availability": "SPOT_WITH_FALLBACK_AZURE"
    },
    {
        "job_name": "FRANCE-DAIRY-VEG-PROMO",
        "min_workers": 3,
        "max_workers": 4,
        "driver_node_type": "Standard_E96a_v4",
        "node_type": "Standard_D64s_v3",
        "notebook_path": f"/Advanced forecasting/{ENV}/promo",
        "notebook_params": {
            "Country": "france",
            "Division": "dairy_veg",
            "Product Level": "sku",
            "Location Level": "departmental"
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 30 2 ? * 7" if ENV == "prod" else "0 30 2 ? * 7",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_multi_node_cpu_optimized_special_DS"],
        "availability": "SPOT_WITH_FALLBACK_AZURE",
    },
    {
        "job_name": "POLAND-CALENDAR",
        "min_workers": 0,
        "max_workers": 0,
        "driver_node_type": None,
        "node_type": "Standard_DS14_v2",
        "notebook_path": f"/Advanced forecasting/{ENV}/calendar",
        "notebook_params": {
            "Country": "poland",
            "Year Shift": 1,
            "Months Depth": 12,
            "Split By Zones": False
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 0 12 1 1 ?" if ENV == "prod" else "0 0 12 1 1 ?",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_single_node"],
        "availability": "SPOT_WITH_FALLBACK_AZURE"
    },
    {
        "job_name": "POLAND-DAIRY-PIPELINE",
        "min_workers": 1,
        "max_workers": 4,
        "driver_node_type": "Standard_E32_v3",
        "node_type": "Standard_E32_v3",
        "notebook_path": f"/Advanced forecasting/{ENV}/pipeline",
        "notebook_params": {
            "Country": "poland",
            "Division": "dairy",
            "Product Level": "sku",
            "Location Level": "regional"
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 30 1 ? * 3-6" if ENV == "prod" else "0 30 1 ? * 3-6",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_multi_node_cpu_optimized_special_DS"],
        "availability": "SPOT_WITH_FALLBACK_AZURE",
    },
    {
        "job_name": "POLAND-WATERS-WEATHER",
        "min_workers": 0,
        "max_workers": 0,
        "driver_node_type": None,
        "node_type": "Standard_DS14_v2",
        "notebook_path": f"/Advanced forecasting/{ENV}/weather",
        "notebook_params": {
            "Country": "poland",
            "Division": "waters",
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 0 2 * * ?" if ENV == "prod" else "0 0 2 * * ?",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_single_node"],
        "availability": "SPOT_WITH_FALLBACK_AZURE",
    },
    {
        "job_name": "POLAND-WATERS-PIPELINE",
        "min_workers": 1,
        "max_workers": 3,
        "driver_node_type": "Standard_DS15_v2",
        "node_type": "Standard_DS15_v2",
        "notebook_path": f"/Advanced forecasting/{ENV}/pipeline",
        "notebook_params": {
            "Country": "poland",
            "Division": "waters",
            "Product Level": "fu",
            "Location Level": "national"
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 15 2 ? * 1,3-7" if ENV == "prod" else "0 15 2 ? * 1,3-7",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_multi_node_cpu_optimized_special_DS"],
        "availability": "SPOT_WITH_FALLBACK_AZURE",
    },
    {
        "job_name": "SPAIN-CALENDAR",
        "min_workers": 0,
        "max_workers": 0,
        "driver_node_type": None,
        "node_type": "Standard_DS14_v2",
        "notebook_path": f"/Advanced forecasting/{ENV}/calendar",
        "notebook_params": {
            "Country": "spain",
            "Year Shift": 1,
            "Months Depth": 12,
            "Split By Zones": True
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 0 12 1 1 ?" if ENV == "prod" else "0 0 12 1 1 ?",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_single_node"],
        "availability": "SPOT_WITH_FALLBACK_AZURE"
    },
    {
        "job_name": "SPAIN-DAIRY-PIPELINE",
        "min_workers": 4,
        "max_workers": 8,
        "driver_node_type": "Standard_E96a_v4",
        "node_type": "Standard_E32_v3",
        "notebook_path": f"/Advanced forecasting/{ENV}/pipeline",
        "notebook_params": {
            "Country": "spain",
            "Division": "dairy",
            "Product Level": "sku",
            "Location Level": "warehouse"
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 00 18 * * ?" if ENV == "prod" else "0 00 18 * * ?",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_multi_node_cpu_optimized_special_DS"],
        "availability": "SPOT_WITH_FALLBACK_AZURE",
    },
    {
        "job_name": "SPAIN-DAIRY-PROMO",
        "min_workers": 4,
        "max_workers": 8,
        "driver_node_type": "Standard_E96a_v4",
        "node_type": "Standard_E32_v3",
        "notebook_path": f"/Advanced forecasting/{ENV}/promo",
        "notebook_params": {
            "Country": "spain",
            "Division": "dairy",
            "Product Level": "sku",
            "Location Level": "warehouse",
            "Building Block": True,
            "Compute Market Shares": True
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 00 1 ? * 7" if ENV == "prod" else "0 00 1 ? * 7",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_multi_node_cpu_optimized_special_DS"],
        "availability": "SPOT_WITH_FALLBACK_AZURE",
    },
    {
        "job_name": "CANARY-DAIRY-PIPELINE",
        "min_workers": 1,
        "max_workers": 2,
        "driver_node_type": "Standard_DS15_v2",
        "node_type": "Standard_DS15_v2",
        "notebook_path": f"/Advanced forecasting/{ENV}/pipeline",
        "notebook_params": {
            "Country": "canary",
            "Division": "dairy",
            "Product Level": "sku",
            "Location Level": "warehouse"
        },
        "adf_version": ADF_VERSION,
        "schedule": "0 00 18 * * ?" if ENV == "prod" else "0 00 18 * * ?",
        "emails_on_start": [],
        "emails_on_success": [],
        "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
        "ENV": ENV,
        "policy_id": POLICY_MAPPING["job_only_multi_node_cpu_optimized_special_DS"],
        "availability": "SPOT_WITH_FALLBACK_AZURE",
    }
]

if ENV == "dev":
    JOBS += [
        {
            "job_name": "POLAND-DAIRY-MOVE-TO-PROD",
            "min_workers": 0,
            "max_workers": 0,
            "driver_node_type": None,
            "node_type": "Standard_DS3_v2",
            "notebook_path": f"/Advanced forecasting/{ENV}/move_to_prod",
            "notebook_params": {
                "Sales Organization": "0029"
            },
            "adf_version": ADF_VERSION,
            "schedule": "0 0 6 ? * 3-6" if ENV == "prod" else "0 0 6 ? * 3-6",
            "emails_on_start": [],
            "emails_on_success": [],
            "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
            "ENV": ENV,
            "policy_id": POLICY_MAPPING["job_only_single_node"],
            "availability": "SPOT_WITH_FALLBACK_AZURE",
        },
        {
            "job_name": "DACH-DAIRY-AT-MOVE-TO-PROD",
            "min_workers": 0,
            "max_workers": 0,
            "driver_node_type": None,
            "node_type": "Standard_DS3_v2",
            "notebook_path": f"/Advanced forecasting/{ENV}/move_to_prod",
            "notebook_params": {
                "Sales Organization": "0013"
            },
            "adf_version": ADF_VERSION,
            "schedule": "0 55 4 ? * 3" if ENV == "prod" else "0 55 4 ? * 3",
            "emails_on_start": [],
            "emails_on_success": [],
            "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
            "ENV": ENV,
            "policy_id": POLICY_MAPPING["job_only_single_node"],
            "availability": "SPOT_WITH_FALLBACK_AZURE",
        },
        {
            "job_name": "DACH-DAIRY-DE-MOVE-TO-PROD",
            "min_workers": 0,
            "max_workers": 0,
            "driver_node_type": None,
            "node_type": "Standard_DS3_v2",
            "notebook_path": f"/Advanced forecasting/{ENV}/move_to_prod",
            "notebook_params": {
                "Sales Organization": "0006"
            },
            "adf_version": ADF_VERSION,
            "schedule": "0 55 4 ? * 3" if ENV == "prod" else "0 55 4 ? * 3",
            "emails_on_start": [],
            "emails_on_success": [],
            "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
            "ENV": ENV,
            "policy_id": POLICY_MAPPING["job_only_single_node"],
            "availability": "SPOT_WITH_FALLBACK_AZURE",
        },
        {
            "job_name": "DACH-DAIRY-CH-MOVE-TO-PROD",
            "min_workers": 0,
            "max_workers": 0,
            "driver_node_type": None,
            "node_type": "Standard_DS3_v2",
            "notebook_path": f"/Advanced forecasting/{ENV}/move_to_prod",
            "notebook_params": {
                "Sales Organization": "0071"
            },
            "adf_version": ADF_VERSION,
            "schedule": "0 55 4 ? * 3" if ENV == "prod" else "0 55 4 ? * 3",
            "emails_on_start": [],
            "emails_on_success": [],
            "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
            "ENV": ENV,
            "policy_id": POLICY_MAPPING["job_only_single_node"],
            "availability": "SPOT_WITH_FALLBACK_AZURE",
        },
        {
            "job_name": "POLAND-WATERS-MOVE-TO-PROD",
            "min_workers": 0,
            "max_workers": 0,
            "driver_node_type": None,
            "node_type": "Standard_DS3_v2",
            "notebook_path": f"/Advanced forecasting/{ENV}/move_to_prod",
            "notebook_params": {
                "Sales Organization": "0042"
            },
            "adf_version": ADF_VERSION,
            "schedule": "0 55 4 ? * 1,3-7" if ENV == "prod" else "0 55 4 ? * 1,3-7",
            "emails_on_start": [],
            "emails_on_success": [],
            "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
            "ENV": ENV,
            "policy_id": POLICY_MAPPING["job_only_single_node"],
            "availability": "SPOT_WITH_FALLBACK_AZURE",
        },
        {
            "job_name": "FRANCE-DAIRY-REG-MOVE-TO-PROD",
            "min_workers": 0,
            "max_workers": 0,
            "driver_node_type": None,
            "node_type": "Standard_D16s_v3",
            "notebook_path": f"/Advanced forecasting/{ENV}/move_to_prod",
            "notebook_params": {
                "Sales Organization": "0024"
            },
            "adf_version": ADF_VERSION,
            "schedule": "0 30 17 ? * 2" if ENV == "prod" else "0 30 17 ? * 2",
            "emails_on_start": [],
            "emails_on_success": [],
            "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
            "ENV": ENV,
            "policy_id": POLICY_MAPPING["job_only_single_node"],
            "availability": "SPOT_WITH_FALLBACK_AZURE",
        },
        {
            "job_name": "FRANCE-DAIRY-VEG-MOVE-TO-PROD",
            "min_workers": 0,
            "max_workers": 0,
            "driver_node_type": None,
            "node_type": "Standard_DS3_v2",
            "notebook_path": f"/Advanced forecasting/{ENV}/move_to_prod",
            "notebook_params": {
                "Sales Organization": "0056"
            },
            "adf_version": ADF_VERSION,
            "schedule": "0 30 7 ? * 2" if ENV == "prod" else "0 30 7 ? * 2",
            "emails_on_start": [],
            "emails_on_success": [],
            "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
            "ENV": ENV,
            "policy_id": POLICY_MAPPING["job_only_single_node"],
            "availability": "SPOT_WITH_FALLBACK_AZURE",
        },
        {
            "job_name": "FRANCE-WATERS-MOVE-TO-PROD",
            "min_workers": 0,
            "max_workers": 0,
            "driver_node_type": None,
            "node_type": "Standard_DS3_v2",
            "notebook_path": f"/Advanced forecasting/{ENV}/move_to_prod",
            "notebook_params": {
                "Sales Organization": "0046"
            },
            "adf_version": ADF_VERSION,
            "schedule": "0 40 3 ? * 1,6" if ENV == "prod" else "0 40 3 ? * 1,6",
            "emails_on_start": [],
            "emails_on_success": [],
            "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
            "ENV": ENV,
            "policy_id": POLICY_MAPPING["job_only_single_node"],
            "availability": "SPOT_WITH_FALLBACK_AZURE",
        },
        {
            "job_name": "SPAIN-DAIRY-MOVE-TO-PROD",
            "min_workers": 0,
            "max_workers": 0,
            "driver_node_type": None,
            "node_type": "Standard_DS3_v2",
            "notebook_path": f"/Advanced forecasting/{ENV}/move_to_prod",
            "notebook_params": {
                "Sales Organization": "0036"
            },
            "adf_version": ADF_VERSION,
            "schedule": "0 00 4 * * ?" if ENV == "prod" else "0 00 4 * * ?",
            "emails_on_start": [],
            "emails_on_success": [],
            "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
            "ENV": ENV,
            "policy_id": POLICY_MAPPING["job_only_single_node"],
            "availability": "SPOT_WITH_FALLBACK_AZURE",
        },
        {
            "job_name": "CANARY-DAIRY-MOVE-TO-PROD",
            "min_workers": 0,
            "max_workers": 0,
            "driver_node_type": None,
            "node_type": "Standard_DS3_v2",
            "notebook_path": f"/Advanced forecasting/{ENV}/move_to_prod",
            "notebook_params": {
                "Sales Organization": "0035"
            },
            "adf_version": ADF_VERSION,
            "schedule": "0 00 22 * * ?" if ENV == "prod" else "0 00 22 * * ?",
            "emails_on_start": [],
            "emails_on_success": [],
            "emails_on_failure": ["danone-advanced-forecasting@danone.com"],
            "ENV": ENV,
            "policy_id": POLICY_MAPPING["job_only_single_node"],
            "availability": "SPOT_WITH_FALLBACK_AZURE",
        }
    ]


def main():
    api_command_list = '/jobs/list'
    api_command_delete = '/jobs/delete'
    api_command_create = '/jobs/create'
    api_command_update = '/jobs/update'
    list_url = f"{DATABRICKS_HOST}{API_VERSION}{api_command_list}"
    response = requests.get(
        url=list_url,
        headers={"Authorization": f"Bearer {DATABRICKS_TOKEN}"}
    )
    existing_jobs = [
        job
        for job in json.loads(response.text)["jobs"]
        if job["settings"]["name"].startswith(JOB_PREFIX)
    ]
    for job in existing_jobs:
        if job["settings"]["name"] not in [JOB_PREFIX + new_job["job_name"] for new_job in JOBS]:
            delete_url = f"{DATABRICKS_HOST}{API_VERSION}{api_command_delete}"
            requests.post(
                url=delete_url,
                data={"job_id": job["job_id"]},
                headers={"Authorization": f"Bearer {DATABRICKS_TOKEN}"}
            )
    for job in JOBS:
        template = JINJA_ENV.get_template('job.json')
        job["job_prefix"] = JOB_PREFIX
        rendered_data = template.render(
            **{
                key: json.dumps(value) if isinstance(value, (list, dict)) else value
                for key, value in job.items()
            }
        )
        if JOB_PREFIX + job["job_name"] not in [
            old_job["settings"]["name"] for old_job in existing_jobs
        ]:
            create_url = f"{DATABRICKS_HOST}{API_VERSION}{api_command_create}"
            create_request = requests.post(
                url=create_url,
                data=json.dumps(json.loads(rendered_data)),
                headers={"Authorization": f"Bearer {DATABRICKS_TOKEN}"}
            )
            print(create_request.text)
        else:
            update_url = f"{DATABRICKS_HOST}{API_VERSION}{api_command_update}"
            old_job = [
                old_job
                for old_job in existing_jobs
                if old_job["settings"]["name"] == JOB_PREFIX + job["job_name"]
            ][0]
            update_request = requests.post(
                url=update_url,
                data=json.dumps(
                    {"job_id": old_job["job_id"], "new_settings": json.loads(rendered_data)}
                ),
                headers={"Authorization": f"Bearer {DATABRICKS_TOKEN}"}
            )
            print(update_request.text)


if __name__ == "__main__":
    main()
