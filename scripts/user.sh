#!/bin/sh

read -p " > User: " username
read -p " > SSH: " ssh

echo "Creating user"
sudo adduser $username

sudo -i -u $username bash << EOF
echo "Creating .ssh folder"
mkdir ~/.ssh
chmod 700 .ssh

echo "Exporting key"
touch ~/.ssh/authorized_keys
chmod 600 ~/.ssh/authorized_keys
echo "$ssh" >> ~/.ssh/authorized_keys

echo "Setting ~/.ssh config"
touch ~/.ssh/config
chmod 600 ~/.ssh/config
echo """Host github.com
  Hostname ssh.github.com
  Port 443""" >> ~/.ssh/config

echo "Setting environment"
echo "export ENV=dev" >> ~/.bashrc

echo "Setting MlFlow"
echo "export MLFLOW_TRACKING_URI=http://localhost:5000" >> ~/.bashrc
EOF

echo "Bye!"
