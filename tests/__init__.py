from moto import mock_s3
from moto import mock_logs


__all__ = [
    "mock_s3",
    "mock_logs",
]
