import os
from tests.modelling.modelling_fixtures import ModellingFixtures
from adf.flows.flow import run_adf_flow
from unittest import mock


DIR = os.path.dirname(__file__)


class MockPrepare:
    mount: str = ''
    blob: str = ''


class MockPreprocess:
    id: str = "0"
    mount: str = ''
    blob: str = ''


class MockTraining:
    id: str = "0"


class MockForecast:
    id: str = "0"


class TestFlows(ModellingFixtures):

    @mock.patch('adf.flows.flow.get_prerequired_forecasts', return_value=None)
    @mock.patch('adf.flows.tasks.get_algorithm_types', return_value=(True, ["week"]))
    @mock.patch('adf.flows.tasks.prepare', return_value=(MockPrepare(), MockPrepare()))
    @mock.patch('adf.flows.tasks.preprocessing.run', return_value=MockPreprocess())
    @mock.patch('adf.flows.tasks.training.run', return_value=MockTraining())
    @mock.patch('adf.flows.tasks.forecasting.run', return_value=MockForecast())
    @mock.patch('adf.flows.tasks.export_to_ibp.run', return_value=True)
    def test_pipeline(
        self,
        mocked_get_prerequired_forecasts,
        mocked_get_algorithm_type,
        mocked_prepare,
        mocked_preprocessing_run,
        mocked_training_run,
        mocked_forecast_run,
        mocked_ibp_export
    ):
        country = "spain"
        division = "dairy"

        run_adf_flow(
            country=country,
            division=division,
            product_level="sku",
            location_level="warehouse",
            run_type="test",
            pipeline_id=None,
            from_step=None,
            to_step=None,
            algorithm_type="week"
        )
