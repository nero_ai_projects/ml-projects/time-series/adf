from contextlib import nullcontext
import os
import pytest
from freezegun import freeze_time
from datetime import datetime
import pandas as pd
from adf.flows.tools.scheduling import get_algorithm_types
from adf.flows.tools import scheduling


@pytest.mark.parametrize(
    "country,division,current_date,expected,raises",
    [
        ('spain', 'dairy', '2019-12-31', (True, ['week']), nullcontext()),
        ('spain', 'dairy', '2020-01-01', (True, ['day']), nullcontext()),
        ('spain', 'dairy', '2020-01-02', (False, "Unknown algorithm type"), nullcontext()),
        ('spain', 'dairy', '2020-01-13', (False, "No execution scheduled"), nullcontext()),

        ('france', 'dairy_veg', '2021-05-27', (
            False, "Don't execute algorithm, except on Mondays"
        ), nullcontext()),
        ('france', 'dairy_reg', '2021-05-31', (True, ['week']), nullcontext()),

        ('france', 'waters', '2021-05-28', (True, ['week']), nullcontext()),
        ('france', 'waters', '2021-05-30', (True, ['week']), nullcontext()),
        ('france', 'waters', '2021-05-31', (
            False, "Don't execute algorithm, except on Fridays and Sundays"
        ), nullcontext()),

        ('poland', 'dairy', '2020-01-04', (
            False, "Don't execute algorithm on non working days or mondays"
        ), nullcontext()),
        ('poland', 'dairy', '2020-01-06', (
            False, "Don't execute algorithm on non working days or mondays"
        ), nullcontext()),
        ('poland', 'dairy', '2020-01-07', (True, ['week']), nullcontext()),
        ('poland', 'dairy', '2020-01-09', (True, ['week']), nullcontext()),

        ('poland', 'waters', '2020-01-06', (
            False, "Don't execute algorithm, on Mondays"
        ), nullcontext()),
        ('poland', 'waters', '2020-01-07', (True, ['week']), nullcontext()),
        ('poland', 'waters', '2020-01-08', (True, ['week']), nullcontext()),

        ('dach', 'dairy_at', '2022-04-18', (
            False, "Don't execute algorithm, except on Tuesdays"
        ), nullcontext()),
        ('dach', 'dairy_at', '2022-04-19', (True, ['week']), nullcontext()),

        ('dach', 'dairy_de', '2022-04-18', (
            False, "Don't execute algorithm, except on Tuesdays"
        ), nullcontext()),
        ('dach', 'dairy_de', '2022-04-19', (True, ['week']), nullcontext()),

        ('dach', 'dairy_ch', '2022-04-18', (
            False, "Don't execute algorithm, except on Tuesdays"
        ), nullcontext()),
        ('dach', 'dairy_ch', '2022-04-19', (True, ['week']), nullcontext()),

        ('mexico', 'dairy', '2022-04-18', (True, ["week"]), nullcontext()),
        ('mexico', 'dairy', '2022-04-19', (
            False, "No algorithm to execute, except on Mondays"
        ), nullcontext()),
    ]
)
def test_get_algorithm_type(monkeypatch, country, division, current_date, expected, raises):
    with raises:
        with freeze_time(datetime.strptime(current_date, "%Y-%m-%d")):
            monkeypatch.setattr(
                scheduling, "get_snowflake_df",
                lambda x, y, z: pd.read_csv(
                    os.path.join(
                        os.path.dirname(os.path.abspath(__file__)),
                        "config",
                        "job_scheduling_calendar.csv"
                    )
                )
            )
            assert get_algorithm_types(country, division) == expected
