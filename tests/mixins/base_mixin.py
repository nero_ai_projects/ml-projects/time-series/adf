import shutil
import tempfile
from pathlib import Path


class BaseMixin(object):

    def setUp(self):
        super(BaseMixin, self).setUp()
        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        super(BaseMixin, self).tearDown()
        shutil.rmtree(self.test_dir)

    def get_filepath(self, filename: str) -> str:
        """ Returns a filepath in the test folder
        """
        return str(Path(self.test_dir, filename))
