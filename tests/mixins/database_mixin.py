import adf.services.database as db
from adf.services.database.seeder import seed_database
from sqlalchemy.orm import scoped_session, sessionmaker  # type: ignore
from sqlalchemy import MetaData
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base


class DatabaseMixin(object):

    def setUp(self):
        super(DatabaseMixin, self).setUp()
        self.engine = db.get_engine()
        self.session = db.get_session()
        self.run_migrations()
        seed_database(self.session)
        self.mlflow_engine = create_engine(
            "postgresql+psycopg2://postgres:postgres@0.0.0.0:5433/mlflow",
            pool_size=4
        )
        self.mlflow_session = scoped_session(sessionmaker(
            bind=self.mlflow_engine,
            autoflush=False,
            autocommit=False,
            expire_on_commit=False
        ))

    def tearDown(self):
        self.session.close()
        self.rollback_migrations()
        db.dispose()
        self.drop_mlflow_tables()
        self.mlflow_engine.dispose()
        self.mlflow_session.remove()

    def run_migrations(self):
        db.Base.metadata.create_all(self.engine)

    def rollback_migrations(self):
        db.Base.metadata.drop_all(self.engine)

    def drop_mlflow_tables(self):
        base = declarative_base()
        metadata = MetaData(self.mlflow_engine)
        base.metadata.drop_all(self.mlflow_engine, list(metadata.sorted_tables))
