from moto import mock_logs


@mock_logs
class LoggingMixin(object):

    def setUp(self):
        super(LoggingMixin, self).setUp()
        self.moto_logs = mock_logs()
        self.moto_logs.start()

    def tearDown(self):
        super(LoggingMixin, self).tearDown()
        self.moto_logs.stop()
