import os
import shutil

from adf import config
from tests.mixins.database_mixin import DatabaseMixin


class StorageMixin(DatabaseMixin):

    def setUp(self):
        super(StorageMixin, self).setUp()
        os.makedirs(config.MOUNT, exist_ok=True)

    def tearDown(self):
        super(StorageMixin, self).tearDown()
        shutil.rmtree(config.MOUNT)
