from unittest import TestCase
import pandas as pd
from adf.modelling.dataprep.utils import core


class TestCore(TestCase):
    def test_build_sub_panel_indexes(self):
        df = pd.DataFrame({'mat_cod': [1, 1, 3, 4, 5],
                           'sales_organization_cod': [1, 2, 2, 4, 5],
                           'rfa_cod': [1, 2, 3, 5, 5],
                           'to_dt': ['2020-01-01', '2020-01-02', '2020-01-03', '2020-01-04',
                                     '2020-01-05']},
                          columns=['mat_cod', 'rfa_cod', 'to_dt',
                                   'sales_organization_cod'])
        res = core.build_sub_panel_indexes(df, '2020-01-01', 'mat_cod', 'rfa_cod', 'to_dt',
                                           None, 'sales_organization_cod', 'pandas')
        self.assertEqual(res.shape, (64, 4))
        self.assertEqual(res.columns.tolist(), ['sales_organization_cod', 'mat_cod',
                                                'rfa_cod', 'to_dt'])
