from unittest import TestCase

from pyspark.sql import SparkSession
import databricks.koalas as ks
import pandas as pd
import datetime
from adf.modelling.dataprep.france.dairy import panel

builder = SparkSession.builder.appName("pyspark-shell")
builder = builder.config('spark.executor.memory', '2g')
builder = builder.config('spark.sql.execution.arrow.pyspark.enabled', 'true')
builder.getOrCreate()


ks.set_option('compute.default_index_type', 'distributed')
ks.set_option("compute.ops_on_diff_frames", True)

product_column = "mat_cod"
location_column = "rfa_cod"
division = "dairy_reg"


class TestPanel(TestCase):
    def test_format_to_4_digits(self):
        df = ks.DataFrame({'code': ['1', '2', '3', '45678']}, columns=['code'])
        res = panel.format_to_4_digits(df, 'code')
        self.assertEqual(res['code'].tolist(), ['0001', '0002', '0003', '45678'])

    def test_format_products(self):
        df = ks.DataFrame({'sales_organization_cod': ['1', '2', '3', '#', '(null)']},
                          columns=['sales_organization_cod'])
        res = panel.format_products(df)
        self.assertEqual(res['sales_organization_cod'].tolist(),
                         ['0001', '0002', '0003', '0NaN', '0NaN'])

    def test_process_products(self):
        products = ks.DataFrame({'mat_cod': [1, 1, 3, 4, 5],
                                 'mat_desc': ['...', '...', '...', '...', '...'],
                                 'mat_brand_desc': ['...', '...', '...', '...', '...'],
                                 'mat_volume_value': ['...', '...', '...', '...', '...'],
                                 'distribution_channel_cod': [1, 1, 2, 2, 3],
                                 'sales_organization_cod': [2, 2, 2, 3, 3]},
                                columns=['mat_cod', 'mat_desc', 'mat_brand_desc',
                                         'mat_volume_value', 'distribution_channel_cod',
                                         'sales_organization_cod'])

        res = panel.process_products(products, product_column)
        self.assertEqual(res['mat_cod'].tolist(), [1, 3, 4, 5])
        self.assertEqual(res.columns.tolist(), ['mat_cod', 'mat_desc', 'mat_brand_desc',
                                                'mat_volume_value', 'sales_organization_cod'])

    def test_filter_customers(self):
        customers = ks.DataFrame({'cus_cod': ['1', '2', '???', '4', '???'],
                                  'rfa_cod': ['???', '4', None, '5', '???']},
                                 columns=['cus_cod', 'rfa_cod'])
        res = panel.filter_customers(customers)
        self.assertEqual(res['cus_cod'].tolist(), [2.0, 4.0])
        self.assertEqual(res['rfa_cod'].tolist(), [4.0, 5.0])

    def test_cast_customers(self):
        customers = ks.DataFrame({'cus_cod': [2.0, 4.0], 'rfa_cod': [4.0, 5.0],
                                  'distribution_channel_cod': [1.0, 2.0]},
                                 columns=['cus_cod', 'rfa_cod',
                                          'distribution_channel_cod'])
        res = panel.cast_customers(customers)
        self.assertEqual(res['cus_cod'].tolist(), [2, 4])
        self.assertEqual(res['rfa_cod'].tolist(), [4, 5])
        self.assertEqual(res['distribution_channel_cod'].tolist(), [1, 2])

    def test_filter_sales_org_cod(self):
        customers = ks.DataFrame({'cus_cod': [2, 4, 0], 'rfa_cod': [4, 5, 0],
                                  'distribution_channel_cod': [1, 2, 0],
                                  'sales_organization_cod': [24, 24, 56]},
                                 columns=['cus_cod', 'rfa_cod',
                                          'distribution_channel_cod',
                                          'sales_organization_cod'])
        res = panel.filter_sales_org_cod(customers, division)
        self.assertEqual(res['sales_organization_cod'].tolist(), ['0024', '0024'])
        self.assertEqual(res['cus_cod'].tolist(), [2, 4])

    def test_exclude_specific_customer(self):
        customers = ks.DataFrame({'cus_cod': [2, 4, 0, 150033622, 150051476]},
                                 columns=['cus_cod'])
        res = panel.exclude_specific_customer(customers)
        self.assertEqual(res['cus_cod'].tolist(), [2, 4, 0])

    def test_prepare_customers(self):
        customers = ks.DataFrame({'cus_cod': [2, 4, 0], 'rfa_cod': [4, 5, 0],
                                  'distribution_channel_cod': [1, 2, 0],
                                  'sales_organization_cod': [24, 24, 56]},
                                 columns=['cus_cod', 'rfa_cod',
                                          'distribution_channel_cod',
                                          'sales_organization_cod'])
        panel.prepare_customers(customers, division)
        pass

    def test_remove_rejected_orders(self):
        orders = ks.DataFrame({'rejection_cod': ['#', '?'],
                               'other_col': [1, 2]},
                              columns=['rejection_cod', 'other_col'])
        res = panel.remove_rejected_orders(orders)
        self.assertEqual(res['other_col'].tolist(), [1])

    def test_filter_plant(self):
        orders = ks.DataFrame({'plant_cod': ['#', '2', '3', '4', '5', '6',
                                             '7', '76', '77', '80', '88',
                                             '102', '103', '515']},
                              columns=['plant_cod'])
        res = panel.filter_plant(orders)
        self.assertEqual(res['plant_cod'].tolist(), [77, 80, 102])

    def test_remove_returns(self):
        orders = ks.DataFrame({'sal_doc_typ_cod_short': [1, 2, 3],
                               'sal_doc_typ_cod': ['ZRab', 'abcd', 'defg']},
                              columns=['sal_doc_typ_cod_short', 'sal_doc_typ_cod'])
        res = panel.remove_returns(orders)
        self.assertEqual(res['sal_doc_typ_cod_short'].tolist(), ['ab', 'de'])

    def test_remove_unused_codes(self):
        orders = ks.DataFrame({'mat_cod': ['1', '2', '3', '4', '5', '6', '7', '8', '9']},
                              columns=['mat_cod'])
        fictive_codes = ks.DataFrame({'mat_cod': ['7', '8']}, columns=['mat_cod'])
        tiers_codes = ks.DataFrame({'mat_cod': ['8', '9', '10']}, columns=['mat_cod'])
        res = panel.remove_unused_codes(orders, fictive_codes, tiers_codes)
        self.assertEqual(res['mat_cod'].tolist(), ['1', '2', '3', '4', '5', '6'])

    def test_filter_orders_data(self):
        orders = ks.DataFrame({'mat_cod': ['1', '2', '3'],
                               'rejection_cod': ['#', '?', '?'],
                               'cus_cod': [2, 4, 150033622],
                               'plant_cod': ['1', '2', '3'],
                               'sal_doc_typ_cod_short': [1, 2, 3],
                               'sal_doc_typ_cod': ['ZRab', 'abcd', 'defg'],
                               'distribution_channel_cod': [1, 2, 0],
                               'material_availability_date': [1, 2, 3],
                               'order_creation_date': [1, 2, 3]},
                              columns=['mat_cod', 'rejection_cod', 'cus_cod', 'plant_cod',
                                       'sal_doc_typ_cod_short', 'sal_doc_typ_cod',
                                       'distribution_channel_cod', 'material_availability_date',
                                       'order_creation_date'])
        fictive_codes = ks.DataFrame({'mat_cod': ['7', '8']}, columns=['mat_cod'])
        tiers_codes = ks.DataFrame({'mat_cod': ['8', '9', '10']}, columns=['mat_cod'])

        panel.filter_orders_data(orders, fictive_codes, tiers_codes)
        pass

    def test_replace_skus(self):
        orders = ks.DataFrame({'mat_cod': ['1', '2', '3', '4', '5', '6', '7', '8', '9']},
                              columns=['mat_cod'])
        mapping = ks.DataFrame({'sku_code_origin': ['1', '2', '3', '4', '5'],
                                'sku_code_dest': ['11', '12', '13', '14', '15']},
                               columns=['sku_code_origin', 'sku_code_dest'])
        res = panel.replace_skus(orders, mapping)
        self.assertEqual(sorted(res['mat_cod'].tolist()), [6, 7, 8, 9, 11, 12, 13, 14, 15])

    def test_filter_pause_prod(self):
        pass

    def test_add_future_orders_until_weekday_of_n_previous_weeks(self):
        pass

    def test_process_orders_data(self):
        pass

    def test_merge_with_customers(self):
        pass

    def test_update_plant_cod(self):
        pass

    def test_aggregate_orders_ks(self):
        pass

    def test_correct_zdpl_orders(self):
        pass

    def test_update_holidays_datetime(self):
        holidays = pd.DataFrame({'to_dt': ['2012-01-01T00:00:00', '2012-01-02T01:00:00']},
                                columns=['to_dt'])
        panel.update_holidays_datetime(holidays)
        pass

    def test_build_end_date(self):
        ts = panel.build_end_date(6, '2021-12-31')
        self.assertEqual(ts.date(), datetime.date(2021, 12, 31))

        ts = panel.build_end_date(6)
        self.assertGreater(ts.date(), datetime.date.today())

    def test_merge_products(self):
        df = pd.DataFrame({'mat_cod': [1, 1, 3],
                           'sales_organization_cod': [1, 2, 3],
                           'other_column': [1, 2, 3]},
                          columns=['mat_cod', 'sales_organization_cod', 'other_column'])
        products = pd.DataFrame({'mat_cod': [1, 1],
                                 'sales_organization_cod': [1, 2],
                                 'another_column': [0, 0]},
                                columns=['mat_cod', 'sales_organization_cod', 'another_column'])
        res = panel.merge_products(df, products, product_column)
        self.assertEqual(res.shape, (3, 4))
        self.assertEqual(res.columns.tolist(), ['mat_cod', 'sales_organization_cod',
                                                'other_column', 'another_column'])

    def test_aggregate_panel(self):
        panel_df = pd.DataFrame({'mat_cod': [1, 1, 3, 3], 'rfa_cod': [1, 2, 3, 3],
                                 'to_dt': ['2018-01-01', '2018-01-02', '2018-01-03', '2018-01-03'],
                                 'sales_organization_cod': [1, 2, 3, 4],
                                 'ordered_units': [1.0, 2.0, 3.0, 4.0]},
                                columns=['mat_cod', 'rfa_cod', 'to_dt',
                                         'sales_organization_cod', 'ordered_units'])
        res = panel.aggregate_panel(panel_df, product_column, location_column)
        self.assertEqual(res.shape, (3, 4))
        self.assertEqual(res['mat_cod'].tolist(), [1, 1, 3])
        self.assertEqual(res['rfa_cod'].tolist(), [1, 2, 3])
        self.assertEqual(res['to_dt'].tolist(), ['2018-01-01', '2018-01-02', '2018-01-03'])
        self.assertEqual(res['ordered_units'].tolist(), [1.0, 2.0, 7.0])

    def test_build_product_panels(self):
        orders = pd.DataFrame({'mat_cod': [1, 1, 3, 4, 5],
                               'rfa_cod': [1, 2, 3, 5, 5],
                               'to_dt': ['2020-01-01', '2020-01-02', '2020-01-03', '2020-01-04',
                                         '2020-01-05'],
                               'sales_organization_cod': [1, 2, 2, 4, 5]},
                              columns=['mat_cod', 'rfa_cod', 'to_dt', 'sales_organization_cod'])
        products = pd.DataFrame({'mat_cod': [1, 1],
                                 'sales_organization_cod': [1, 2],
                                 'another_column': [0, 0]},
                                columns=['mat_cod', 'sales_organization_cod', 'another_column'])
        panel.build_product_panels(orders, products, product_column, location_column, '2021-12-31')
        pass

    def test_multiprocess_product_panel(self):
        pass

    def test_merge_promotions(self):
        pass
