from unittest import TestCase

from pyspark.sql import SparkSession
import databricks.koalas as ks
# import pandas as pd
# import numpy as np
# from adf.modelling.dataprep.france.dairy import promo, promo_uplifts, promo_means

builder = SparkSession.builder.appName("pyspark-shell")
builder = builder.config('spark.executor.memory', '2g')
builder = builder.config('spark.sql.execution.arrow.pyspark.enabled', 'true')
builder.getOrCreate()

ks.set_option('compute.default_index_type', 'distributed')
ks.set_option("compute.ops_on_diff_frames", True)

product_column = "mat_cod"
location_column = "country_cod"
division = "dairy_reg"


class TestPromo(TestCase):
    def test_unify_promotions(self):
        # promo.unify_promotions()
        pass

    def test_prepare_live_main_promo(self):
        # promo.live_main_promo()
        pass

    def test_drop_multiple_mecha_codes_for_same_promo(self):
        # promo.drop_multiple_mecha_codes_for_same_promo()
        pass

    def test_merge_live_promo(self):
        # promo.merge_live_promo()
        pass

    def test_clean_promo(self):
        # promo.clean_promo()
        pass

    def test_process_promo_cas(self):
        # promo.process_promo_cas()
        pass

    def test_resolve_date_inconsistencies(self):
        # promo.resolve_date_inconsistencies()
        pass

    def test_create_mapping_meca(self):
        # promo.create_mapping_meca()
        pass

    def test_map_mechanics(self):
        # promo.map_mechanics()
        pass

    def test_map_promo_type(self):
        # promo.map_promo_type()
        pass

    def test_add_promo_type_id(self):
        # promo.add_promo_type_id()
        pass

    def test_add_location_id(self):
        # promo.add_location_id()
        pass

    def test_aggregate_duplicate_promotions(self):
        # promo.aggregate_duplicate_promotions()
        pass

    def test_merge_promotion_meca_codes(self):
        # promo.merge_promotion_meca_codes()
        pass

    def test_correct_product_types(self):
        # promo.correct_product_types()
        pass

    def test_cast_customer_ids(self):
        # promo.cast_customer_ids()
        pass

    def test_shift_locations_start_sellin(self):
        # promo.shift_locations_start_sellin()
        pass

    def test_add_mapping_promo_lot_fdr(self):
        # promo.add_mapping_promo_lot_fdr()
        pass

    def test_build_uplifts_promo(self):
        # promo_uplifts.build_uplifts_promo()
        pass

    def test_build_promo_uplifts_data(self):
        # promo_uplifts.build_promo_data()
        pass

    def test_build_promo_means_data(self):
        # promo_means.build_promo_data()
        pass

    # TODO: add tests for functions in promo_uplifts and promo_means
