import os
from unittest import TestCase
from tests.mixins import BaseMixin
from tests.mixins import StorageMixin
from adf import utils


DIR = os.path.dirname(__file__)


class ModellingFixtures(BaseMixin, StorageMixin, TestCase):

    def setUp(self):
        super(ModellingFixtures, self).setUp()

    def get_config_path(self, name):
        return os.path.join(DIR, f"config/{name}.json")

    def get_data_path(self, name):
        path = os.path.join(DIR, f"config/{name}.parquet")
        return "/".join(path.split("/")[:-1]), path.split("/")[-1]

    def load_config(self, name):
        return utils.load_json(self.get_config_path(name))

    def load_data(self, name):
        return utils.load_data(self.get_data_path(name))
