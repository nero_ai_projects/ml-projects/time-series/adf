import os
import pandas as pd
from xgboost.core import Booster
from xgboost import XGBRegressor
from tests.modelling.modelling_fixtures import ModellingFixtures
import mock
from functools import partial

from adf.modelling import preprocessing
from adf.modelling import training
from adf.modelling import forecast as forecasting
from adf.modelling.ibp_export import main as exporting
from adf.utils.provider import Provider


class TestModelling(ModellingFixtures):

    @mock.patch("adf.services.storage.get_dbutils")
    def test_pipeline(self, mock_db_utils):
        # Preprocessing
        mock_db_utils.return_value.fs.mkdirs = partial(os.makedirs, exist_ok=True)
        input_mount, input_blob = self.get_data_path("preprocessing")
        preprocessing_report = Provider(
            "preprocessing", mlrun=True, session=True, cluster=False
        )(preprocessing.run)(
            self.load_config("preprocessing"),
            input_mount, input_blob
        )
        self.assertEqual(preprocessing_report.status, "success")
        self.assertIsInstance(preprocessing_report.download(), pd.DataFrame)
        # Training
        training_report = Provider(
            "training", mlrun=True, session=True, cluster=False
        )(training.run)(
            self.load_config("training"),
            preprocessing_report.id,
            self.load_config("filters")
        )
        self.assertEqual(training_report.status, "success")
        self.assertIsInstance(
            training_report.download(model_type="XGBoost"), (Booster, XGBRegressor)
        )

        # Second Training
        second_training_config = self.load_config("training")
        second_training_config["models"][0]["name"] = "week_0_b"
        second_training_config["models"][0]["parameters"]["n_estimators"] = 20
        second_training_config["models"][1]["name"] = "week_1_b"
        second_training_config["models"][1]["parameters"]["n_estimators"] = 20
        second_training_report = Provider(
            "training", mlrun=True, session=True, cluster=False
        )(training.run)(
            second_training_config,
            preprocessing_report.id,
            self.load_config("filters")
        )

        # Forecast with the two trained models
        forecast_config = self.load_config("forecast")

        forecast_report = Provider(
            "forecasting", mlrun=True, session=True, cluster=False
        )(forecasting.run)(
            forecast_config,
            second_training_report.id,
            second_training_report.preprocessing_report.mount,
            second_training_report.preprocessing_report.blob,
            self.load_config("filters"),
            True
        )
        self.assertEqual(forecast_report.status, "success")
        self.assertIsInstance(forecast_report.download(), pd.DataFrame)

        # Export data to IBP
        ibp_report = Provider(
            "ibp_export", mlrun=True, session=True, cluster=False
        )(exporting.run)(
            [str(forecast_report.id)]
        )
        self.assertEqual(ibp_report.status, "success")
        self.assertIsInstance(ibp_report.download(), pd.DataFrame)
