import pandas as pd
import unittest
import datetime
from numpy import testing
from unittest import TestCase
from adf.services.calendar import main as calendar

MODULE = 'adf.services.calendar.main'


class TestCalendar(TestCase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.country = "France"
        self.start_date = '2019-09-01'
        self.end_date = '2020-09-01'
        self.test_date = datetime.datetime(2020, 4, 13)
        self.test_zone = 'B'

    def test_retrieve_bank_holidays(self):
        bank_holidays = calendar.retrieve_bank_holidays(self.country, self.start_date,
                                                        self.end_date)
        self.assertEqual(len(bank_holidays.query('is_holiday == 1')), 11)
        self.assertEqual(bank_holidays.shape, (
            len(pd.date_range(self.start_date, self.end_date)),
            4
        ))
        self.assertFalse(bank_holidays['is_holiday'].isnull().any())

    def test_retrieve_french_school_holidays(self):
        school_holidays = calendar.retrieve_french_school_holidays(self.start_date, self.end_date)
        self.assertTrue(school_holidays.head(1).date.values == datetime.date(2019, 1, 1))
        self.assertTrue(school_holidays.tail(1).date.values == datetime.date(2020, 12, 31))
        testing.assert_array_equal(school_holidays["zones"].unique(), ["A", "B", "C"])
        testing.assert_array_equal(school_holidays["is_school_holiday"].unique(), [1, 0])


if __name__ == "__main__":
    unittest.main()
