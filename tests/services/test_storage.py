import os
import unittest
from unittest import TestCase
from functools import partial

import mock
import pandas as pd

from tests.mixins import BaseMixin
from tests.mixins import StorageMixin

from adf import config
from adf.services import storage


class TestStorage(StorageMixin, BaseMixin, TestCase):

    def setUp(self):
        super().setUp()
        self.df = pd.DataFrame([{"col": "row"}])

    def get_blob(self, filename: str) -> str:
        return "test/{}".format(filename)

    @mock.patch("adf.services.storage.get_dbutils")
    def test_upload_and_read_csv(self, mock_db_utils):
        mock_db_utils.return_value.fs.mkdirs = partial(os.makedirs, exist_ok=True)
        blob = self.get_blob("test.csv")
        storage.upload_csv(self.df, config.MOUNT, blob)
        downloaded_df = storage.read_csv(config.MOUNT, blob)
        self.assertTrue(self.df.equals(downloaded_df))

    @mock.patch("adf.services.storage.get_dbutils")
    def test_upload_and_read_parquet(self, mock_db_utils):
        mock_db_utils.return_value.fs.mkdirs = partial(os.makedirs, exist_ok=True)
        blob = self.get_blob("test.parquet")
        storage.upload_parquet(self.df, config.MOUNT, blob)
        downloaded_df = storage.read_parquet(config.MOUNT, blob)
        self.assertTrue(self.df.equals(downloaded_df))


if __name__ == "__main__":
    unittest.main()
