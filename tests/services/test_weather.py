import json
import pandas as pd
from datetime import datetime
import unittest
from unittest import mock
from unittest import TestCase
from tests.mixins import StorageMixin
from adf.services.weather import main as weather


MODULE = 'adf.services.weather.main'


class MockResponse:

    def __init__(self, payload, status_code):
        self.payload = payload
        self.status_code = status_code

    def json(self):
        return self.payload


class TestWeather(StorageMixin, TestCase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.country = "france"
        self.division = "waters"
        self.cities = [
            {
                "city": "paris",
                "latitude": 48.866667,
                "longitude": 2.333333,
            },
            {
                "city": "bordeaux",
                "latitude": 44.833333,
                "longitude": 0.566667
            },
        ]
        self.date = datetime(2020, 12, 13)

    @staticmethod
    def load_sample(filename):
        with open(f"tests/services/samples/{filename}") as stream:
            sample = json.load(stream)
        return sample

    @mock.patch(f"{MODULE}.utils.load_weather_apikeys")
    @mock.patch(f"{MODULE}.requests.get")
    @mock.patch(f"{MODULE}.get_cities")
    def test_fetch_forecast(self, mock_cities, mock_requests, mock_load_keys):
        mock_cities.return_value = self.cities
        payload = self.load_sample("weather_forecast.json")
        mock_requests.return_value = MockResponse(payload, 200)
        mock_load_keys.return_value = ["1", "2", "3"]

        client = weather.DarkSkyApi()
        cities = weather.get_cities(self.country)
        weather_df, _ = weather.process_cities(
            client,
            pd.DataFrame(columns=["city"]),
            cities,
            "forecast",
            self.date.date()
        )
        self.assertTrue(weather_df.shape[0] == 16)

    @mock.patch(f"{MODULE}.utils.load_weather_apikeys")
    @mock.patch(f"{MODULE}.requests.get")
    @mock.patch(f"{MODULE}.get_cities")
    def test_fetch_historical(self, mock_cities, mock_requests, mock_load_keys):
        mock_load_keys.return_value = ["1", "2", "3"]
        payload = self.load_sample("weather_historical.json")
        mock_requests.return_value = MockResponse(payload, 200)
        mock_cities.return_value = self.cities

        client = weather.DarkSkyApi()
        cities = weather.get_cities(self.country)
        weather_df, _ = weather.process_cities(
            client,
            pd.DataFrame(columns=["city"]),
            cities,
            "historical",
            self.date.date()
        )
        self.assertTrue(weather_df.shape[0] == 2)

    def test_date_range_fails(self):
        wrong_start_date = datetime(2021, 1, 1)
        right_end_date = datetime(2020, 1, 1)
        with self.assertRaises(weather.WeatherError):
            list(weather.date_range(wrong_start_date, right_end_date))


if __name__ == "__main__":
    unittest.main()
