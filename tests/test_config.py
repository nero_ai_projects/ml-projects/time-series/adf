from unittest import TestCase
from adf import config
from adf import utils


class TestConfig(TestCase):

    def test_env(self):
        self.assertEqual(config.ENV, "test")

    def test_get_database_uri(self):
        uri = utils.get_database_uri()
        self.assertEqual(uri, "postgresql+psycopg2://postgres@0.0.0.0:5433")
