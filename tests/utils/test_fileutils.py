import builtins
import io
from contextlib import nullcontext
import json
import pytest
from adf.utils.fileutils import load_json, save_json, read_config, display_config


@pytest.mark.parametrize(
    "data,expected,raises",
    [
        ('{"a": "b"}', {"a": "b"}, nullcontext()),
        ('[{"a": "b"}]', [{"a": "b"}], nullcontext()),
        ('ndnfbfhf', None, pytest.raises(json.decoder.JSONDecodeError))
    ]
)
def test_load_json(monkeypatch, data, expected, raises):
    monkeypatch.setattr(builtins, "open", lambda x, y: io.StringIO(data))
    with raises:
        assert load_json("any_file_path") == expected


@pytest.mark.parametrize(
    "data,expected,raises",
    [
        ('{"a": "b"}', {"a": "b"}, nullcontext()),
        ('[{"a": "b"}]', None, pytest.raises(AttributeError)),
        ('ndnfbfhf', None, pytest.raises(json.decoder.JSONDecodeError))
    ]
)
def test_read_config(monkeypatch, data, expected, raises):
    monkeypatch.setattr(builtins, "open", lambda x, y: io.StringIO(data))
    with raises:
        assert read_config("any_file_path") == expected


@pytest.mark.parametrize(
    "data,raises",
    [
        ({"a": "b"}, nullcontext()),
        ([{"a": "b"}], pytest.raises(AttributeError)),
        ('ndnfbfhf', pytest.raises(AttributeError))
    ]
)
def test_display_config(monkeypatch, data, raises):
    with raises:
        assert display_config(data) is None


@pytest.mark.parametrize(
    "data,raises",
    [
        ({"a": "b"}, nullcontext()),
        ([{"a": "b"}], nullcontext()),
        (object(), pytest.raises(TypeError))
    ]
)
def test_save_json(monkeypatch, data, raises):
    monkeypatch.setattr(builtins, "open", lambda x, y: io.StringIO())
    with raises:
        assert save_json("any_file_path", data) is None
