from unittest import TestCase
from adf.utils.imports import import_method


def any_method(value):
    return value


class TestConfig(TestCase):

    def test_import_method(self):
        return_value = "test"
        tested_result = import_method("tests.utils.test_imports", "any_method")(return_value)
        self.assertEqual(tested_result, return_value)
