import pytest
import pandas as pd
import numpy as np
from adf.utils.threading import split_df_in_chunks, multithreaded


testdata = [
    (
        pd.DataFrame({"col": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}),
        "col",
        4,
        [np.array([1, 2, 3]), np.array([4, 5, 6]), np.array([7, 8]), np.array([9, 10])]
    )
]


@pytest.mark.parametrize("df,column,nchunks,expected", testdata)
def test_split_df_in_chunks(df, column, nchunks, expected):
    for result_array, expected_array in zip(split_df_in_chunks(df, column, nchunks), expected):
        assert all(result_array == expected_array)


def any_method(a):
    return a + a


@pytest.mark.parametrize("func,args,threads,expected", [(any_method, [1, 2], 2, [2, 4])])
def test_multithreaded(func, args, threads, expected):
    assert multithreaded(func, args, threads) == expected
